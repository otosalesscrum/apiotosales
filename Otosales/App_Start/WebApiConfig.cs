﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using a2is.Framework.Security;
using GardaMobile.Framework.API;
using GardaMobile.Framework.API.Handlers;
using Otosales.Infrastructure;

namespace Otosales
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            //config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "StandardApi",
                routeTemplate: "api/{controller}/{action}"
            );
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{version}/{controller}/{action}"
            );

            // always place logging handler at the last order
            //config.MessageHandlers.Clear();
            if (Convert.ToBoolean(ConfigurationManager.AppSettings["IsLoggingEnabled"]))
                config.MessageHandlers.Add(new LoggingHandler());

            config.Services.Replace(typeof(IHttpControllerSelector), new VersioningHttpControllerSelector(config)); 
        }
    }
}
