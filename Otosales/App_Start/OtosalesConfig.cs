﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.App_Start
{
    public class OtosalesConfig
    {
        public const string PUSHAPI_APPLICATIONID = "";
        public const string APPLICATIONID = "ApplicationID";

        #region Connection String Config

        public const string CONNECTION_STRING_MOBILESURVEYDB = "MobileSurveyDB";
        public const string CONNECTION_STRING_AABDB = "AABDB";
        public const string CONNECTION_STRING_AABMobile = "AABMobile";
        public const string CONNECTION_STRING_CPSDB = "CPSDB";
        public const string CONNECTION_STRING_BEYONDDB = "BeyondDB";
        public const string CONNECTION_STRING_AABImageDB = "AABImage";
        public const string CONNECTION_STRING_XOOMDB = "XOOM";
        public const string CONNECTION_STRING_AntrianDB = "AntrianDB";
        public const string CONNECTION_STRING_GASI = "GASI";
        public const string CONNECTION_STRING_a2isAuthorizationDB = "a2isAuthorizationDB";
        public const string CONNECTION_STRING_SURVEYMANAGEMENTDB = "SurveyManagementDB";
        public const string CONNECTION_STRING_GARDAMOBILEDB = "GardaMobileDB";

        #endregion
        public static string APPDATA_PATH = "";

        public static void Init(HttpContext context)
        {
            OtosalesConfig.APPDATA_PATH = HttpContext.Current.Server.MapPath(string.Format("~/App_Data"));
        }
    }
}