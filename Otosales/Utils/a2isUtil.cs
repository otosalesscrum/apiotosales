﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Otosales.Repository;
using log4net;

namespace Otosales.Utils
{
    public class a2isUtil : OtosalesRepositoryBase
    {

        public static void multipleExecuteList(dynamic db, List<string> query)
        {

            foreach (string data in query)
            {
                db.Execute(data);
            }

        }

        public static void SetLoggingReference(string reference)
        {
            ThreadContext.Properties["lReference"] = reference;
        }
    }
}