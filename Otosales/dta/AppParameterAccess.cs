﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.dta
{
    public class AppParameterAccess
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();

        #region Query Get Application Key
        private static string queryGetApplicationKey = @"select COALESCE(OptionValue,'') AS ParamValue from [AsuransiAstra].[dbo].[AsuransiAstraOptions] where OptionName = @0;";
        #endregion

        public static string GetAppParamValue(string paramName)
        {
            string applicationValue = "";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB))
            {
                var result = db.FirstOrDefault<dynamic>(queryGetApplicationKey, paramName);

                applicationValue = (result == null) ? "" : result.ParamValue;
            }

            return applicationValue;
        }

        public static List<string> GetApplicationParametersValue(string ParamName)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            List<string> result = new List<string>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                query = @"SELECT ParamValue, ParamValue2 FROM dbo.ApplicationParameters
                        WHERE RowStatus = 1 AND ParamName = @0";
                List<dynamic> listVal = db.Fetch<dynamic>(query, ParamName);
                if (listVal.Count > 0)
                {
                    result.Add(listVal.First().ParamValue);
                    result.Add(listVal.First().ParamValue2);
                }
                return result;
            }
            catch (Exception e)
            {
                _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static string SafeTrim(string str)
        {
            return str != null ? str.Trim() : "";
        }

    }
}