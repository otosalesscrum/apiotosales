﻿using Otosales.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Otosales.Controllers.vWeb
{
    [AllowAnonymous]
    public class WebSAController : OtosalesBaseController
    {
        

        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public string Login(string userID, string password)
        //{
        //    try
        //    {
        //        LoginResult<UserPrivilegeWebSA> result = new LoginResult<UserPrivilegeWebSA>();
        //        string query = "SELECT SalesAdminID,Email,Name,Password,Validation,Expiry,Password_Expiry,Phone1,Phone2,Phone3,OfficePhone,Ext,Fax,EntryDate,LastUpdatedTime,RowStatus FROM SalesAdmin WITH(NOLOCK) WHERE SalesAdminID='" + userID + "' and RowStatus=1";
        //        DataTable dtUser = sqlConn.GetDataTable(query);
        //        if (dtUser != null)
        //        {
        //            if (dtUser.Rows.Count > 0)
        //            {
        //                SalesAdmin user = new SalesAdmin()
        //                {
        //                    SalesAdminID = dtUser.Rows[0]["SalesAdminID"] != null ? Convert.ToString(dtUser.Rows[0]["SalesAdminID"]) : null,
        //                    Email = dtUser.Rows[0]["Email"] != null ? Convert.ToString(dtUser.Rows[0]["Email"]) : null,
        //                    Name = dtUser.Rows[0]["Name"] != null ? Convert.ToString(dtUser.Rows[0]["Name"]) : null,
        //                    Password = dtUser.Rows[0]["Password"] != null ? Convert.ToString(dtUser.Rows[0]["Password"]) : null,
        //                    Validation = dtUser.Rows[0]["Validation"] != null ? Convert.ToString(dtUser.Rows[0]["Validation"]) : null,
        //                    Phone1 = dtUser.Rows[0]["Phone1"] != null ? Convert.ToString(dtUser.Rows[0]["Phone1"]) : null,
        //                    Phone2 = dtUser.Rows[0]["Phone2"] != null ? Convert.ToString(dtUser.Rows[0]["Phone2"]) : null,
        //                    Phone3 = dtUser.Rows[0]["Phone3"] != null ? Convert.ToString(dtUser.Rows[0]["Phone3"]) : null,
        //                    OfficePhone = dtUser.Rows[0]["OfficePhone"] != null ? Convert.ToString(dtUser.Rows[0]["OfficePhone"]) : null,
        //                    Ext = dtUser.Rows[0]["Ext"] != null ? Convert.ToString(dtUser.Rows[0]["Ext"]) : null,
        //                    Fax = dtUser.Rows[0]["Fax"] != null ? Convert.ToString(dtUser.Rows[0]["Fax"]) : null,
        //                    RowStatus = dtUser.Rows[0]["RowStatus"] != null ? Convert.ToBoolean((dtUser.Rows[0]["RowStatus"])) : false
        //                };
        //                if (dtUser.Rows[0]["Expiry"].ToString() != "")
        //                    user.Expiry = Convert.ToDateTime(dtUser.Rows[0]["Expiry"].ToString());
        //                if (dtUser.Rows[0]["Password_Expiry"].ToString() != "")
        //                    user.Password_Expiry = Convert.ToDateTime(dtUser.Rows[0]["Password_Expiry"].ToString());
        //                if (dtUser.Rows[0]["EntryDate"].ToString() != "")
        //                    user.EntryDate = Convert.ToDateTime(dtUser.Rows[0]["EntryDate"].ToString());
        //                if (dtUser.Rows[0]["LastUpdatedTime"].ToString() != "")
        //                    user.LastUpdatedTime = Convert.ToDateTime(dtUser.Rows[0]["LastUpdatedTime"].ToString());

        //                if (user.Password.ToString() == CryptographyService.EncryptPassword(password))
        //                {
        //                    result.IsAuthenticated = true;
        //                    result.Status = "OK";

        //                    UserPrivilegeWebSA userPrivilege = new UserPrivilegeWebSA();

        //                    DataTable dtRole = sqlConn.GetDataTable("SELECT SalesAdminID,BranchCode,LastUpdatedTime,RowStatus FROM SalesAdminBranch WITH(NOLOCK) WHERE SalesAdminID='" + userID + "' AND RowStatus=1");
        //                    foreach (DataRow f in dtRole.Rows)
        //                    {
        //                        SalesAdminBranch saBranch = new SalesAdminBranch();
        //                        saBranch.SalesAdminID = Convert.ToString(f["SalesAdminID"]);
        //                        saBranch.BranchCode = Convert.ToString(f["BranchCode"]);
        //                        saBranch.LastUpdatedTime = Convert.ToDateTime(f["LastUpdatedTime"].ToString());
        //                        saBranch.RowStatus = Convert.ToBoolean((f["RowStatus"]));
        //                        userPrivilege.BranchCollections.Add(saBranch);
        //                    }

        //                    userPrivilege.User = user;
        //                    result.UserInfo = userPrivilege;
        //                    result.Token = Guid.NewGuid().ToString();
        //                }
        //                else
        //                {
        //                    result.IsAuthenticated = false;
        //                    result.Status = "Your password is invalid";
        //                    result.UserInfo = null;
        //                }
        //            }
        //            else
        //            {
        //                result.IsAuthenticated = false;
        //                result.Status = "Your user ID is invalid";
        //                result.UserInfo = null;
        //            }
        //        }
        //        else
        //        {
        //            result.IsAuthenticated = false;

        //            result.Status = "Your user ID is invalid";
        //            result.UserInfo = null;
        //        }
        //        return JsonConvert.SerializeObject(result, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });
        //    }
        //    catch (Exception ex)
        //    {
        //        EventViewer.Write(DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error in MobileSalesServiceInternal - WebSAServiceInternal Method Login for user : " + userID + ". Error Message : " + ex.Message.ToString());
        //        return "";
        //    }
        //}
    }
}
