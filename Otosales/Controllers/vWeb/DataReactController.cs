﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Microsoft.Reporting.WinForms;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;

namespace Otosales.Controllers.vWeb
{

    public class DataReactController : ApiController
    {

        private static readonly a2isLogHelper _log = new a2isLogHelper();
        #region Rate Calculation Global Variable
        private string ProductCode = "";
        private string CityCode = "";
        private string type = "";
        private string UsageCode = "";
        private int Year = 0;

        private bool IsSRCCChecked = false;
        private bool IsFLDChecked = false;
        private bool IsETVChecked = false;
        private bool IsTSChecked = false;
        private bool IsTPLChecked = false;
        private bool IsPAPASSChecked = false;
        private bool IsPADRVRChecked = false;
        private bool IsTPLSIEnabled = false;
        private bool IsPASSEnabled = false;
        private bool IsPAPASSSIEnabled = false;
        private bool IsPADRVRSIEnabled = false;
        private bool IsACCESSChecked = false;
        private bool IsACCESSSIEnabled = false;


        private bool IsSRCCEnabled = false;
        private bool IsETVEnabled = false;
        private bool IsFLDEnabled = false;
        private bool IsTSEnabled = false;
        private bool IsPAPASSEnabled = false;
        private bool IsPADRVREnabled = false;
        private bool IsACCESSEnabled = false;
        private bool IsTPLEnabled = false;

        private string TPLCoverageID = "";
        private double TPLSI = 0;
        private double PADRVRSI = 0;
        private double PAPASSSI = 0;
        private int PASS = 0;
        private double AccessSI = 0;

        private double BasicPremi = 0;
        private double SumInsured = 0;
        private double SRCCPremi = 0;
        private double FLDPremi = 0;
        private double ETVPremi = 0;
        private double TSPremi = 0;
        private double ACCESSPremi = 0;
        private double TPLPremi = 0;
        private double PADRVRPremi = 0;
        private double PAPASSPremi = 0;
        private double LoadingPremi = 0;
        private double TotalPremi = 0;
        private double AdminFee = 0;

        private double VehiclePrice = 0;


        List<CalculatedPremi> CalculatedPremiItems = new List<CalculatedPremi>();
        List<CalculatedPremi> InterestCoverageItems = new List<CalculatedPremi>();
        List<Prd_Load_Mv> loadingMaster = new List<Prd_Load_Mv>();
        List<Prd_Agreed_Value> prdAgreedValueMaster = new List<Prd_Agreed_Value>();
        List<CoveragePeriod> coveragePeriodItems = new List<CoveragePeriod>();
        List<Prd_Interest_Coverage> prdInterestCoverage = new List<Prd_Interest_Coverage>();


        VehiclePriceTolerance vptModel;
        ProspectCustomer pcModel;
        List<OrderSimulationInterest> osiDeletedModels;
        List<OrderSimulationCoverage> oscDeletedModels;
        List<CoveragePeriodNonBasic> coveragePeriodNonBasicItems = new List<CoveragePeriodNonBasic>();


        #endregion

        #region rate Calculation Helper Function

        private void RefreshBundlingPremi()
        {
            string query = "";
            #region RefreshingBundlingPremi
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            #region Initialize SetCoverageTypeNonBasicItems
            SetCoverageTypeNonBasicItems();
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            if (CalculatedPremiItems.Count() > 0)
            {
                for (int i = CalculatedPremiItems.Count(); i > 0; i--)
                {
                    CalculatedPremi cp = CalculatedPremiItems[i - 1];
                    if ((cp.CoverageID.Equals("SRCC  ") || cp.CoverageID.Equals("SRCTLO") ||
                            cp.CoverageID.Equals("FLD   ") || cp.CoverageID.Equals("FLDTLO") ||
                            cp.CoverageID.Equals("ETV   ") || cp.CoverageID.Equals("ETVTLO")))
                    {
                        CalculatedPremiItems.RemoveAt(i - 1);
                    }
                }
            }

            if (IsSRCCChecked || IsFLDChecked || IsETVChecked)
            {
                foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                {
                    query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, item.Year * 12.0);
                    // HOTFIX 20151120 - BSY


                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC)
                    {
                        CalculatePremi("CASCO ",
                                   item.CoverageType == CoverageType.Comprehensive ? "SRCC  " : "SRCTLO",
                                   item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured,
                                   item.IsBundling);
                    }
                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD)
                    {
                        CalculatePremi("CASCO ",
                                item.CoverageType == CoverageType.Comprehensive ? "FLD   " : "FLDTLO",
                                item.Year, Convert.ToDouble(
                                agreedValue.Rate / 100) * SumInsured,
                                item.IsBundling);
                    }
                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                    {
                        CalculatePremi("CASCO ",
                                item.CoverageType == CoverageType.Comprehensive ? "ETV   " : "ETVTLO",
                                item.Year, Convert.ToDouble(
                                agreedValue.Rate / 100) * SumInsured,
                                item.IsBundling);
                    }
                }

                if (IsSRCCChecked)
                {
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.CoverageID.TrimEnd().Equals("SRCC") || cp.CoverageID.TrimEnd().Equals("SRCTLO"))
                        {
                            SRCCPremi += cp.Premium;
                        }
                        if (cp.CoverageID.TrimEnd().Equals("FLD") || cp.CoverageID.TrimEnd().Equals("FLDTLO"))
                        {
                            FLDPremi += cp.Premium;
                        }
                        if (cp.CoverageID.TrimEnd().Equals("ETV") || cp.CoverageID.TrimEnd().Equals("ETVTLO"))
                        {
                            ETVPremi += cp.Premium;
                        }
                    }
                }

            }
            if (AccessSI > 0)
            {
                CalculateACCESSPremi();
            }

            CalculateLoading();
            CalculateTotalPremi();
            #endregion
            #endregion
        }

        private void CalculateACCESSPremi()
        {

            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.InterestID.Equals("ACCESS"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            for (int i = 0; i < CalculatedPremiItems.Count(); i++)
            {
                CalculatedPremi cp = CalculatedPremiItems[i];
                if (cp.InterestID.Equals("CASCO "))
                {
                    double premi = 0;
                    double loadPremi = 0;
                    double sumInsured = 0;
                    string query = "SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, cp.Year * 12.0);
                    sumInsured = AccessSI * Convert.ToDouble(agreedValue.Rate / 100);
                    premi = sumInsured * cp.Rate / 100;

                    CalculatedPremi calculatedPremi = new CalculatedPremi();
                    calculatedPremi.InterestID = "ACCESS";
                    calculatedPremi.CoverageID = cp.CoverageID;
                    calculatedPremi.Year = cp.Year;
                    calculatedPremi.Rate = cp.Rate;
                    calculatedPremi.Premium = premi;
                    calculatedPremi.SumInsured = sumInsured;
                    calculatedPremi.LoadingRate = cp.LoadingRate;
                    calculatedPremi.Loading = loadPremi;
                    if (cp.CoverageID.Equals("ALLRIK") || cp.CoverageID.Equals("TLO   "))
                    {
                        calculatedPremi.LoadingRate = cp.LoadingRate;
                        loadPremi = cp.LoadingRate / 100 * premi;
                        calculatedPremi.Loading = loadPremi;
                    }
                    calculatedPremi.IsBundling = cp.IsBundling;
                    CalculatedPremiItems.Add(calculatedPremi);
                }
            }

            ACCESSPremi = 0;
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;

            foreach (CalculatedPremi cp in CalculatedPremiItems)
            {
                if (cp.InterestID.Equals("ACCESS") &&
                        (cp.CoverageID.Equals("ALLRIK") || cp.CoverageID.Equals("TLO   ")))
                {
                    ACCESSPremi += cp.Premium;
                    IsACCESSChecked = true;
                }
                else if (cp.CoverageID.Equals("SRCC  ") || cp.CoverageID.Equals("SRCTLO"))
                {
                    SRCCPremi += cp.Premium;
                    IsSRCCChecked = true;
                }
                else if (cp.CoverageID.Equals("FLD   ") || cp.CoverageID.Equals("FLDTLO"))
                {
                    FLDPremi += cp.Premium;
                    IsFLDChecked = true;
                }
                else if (cp.CoverageID.Equals("ETV   ") || cp.CoverageID.Equals("ETVTLO"))
                {
                    ETVPremi += cp.Premium;
                    IsETVChecked = true;
                }
                else if (cp.CoverageID.Equals("TRS   ") || cp.CoverageID.Equals("TRRTLO"))
                {
                    TSPremi += cp.Premium;
                    IsTSChecked = true;
                }
            }

            CalculateLoading();
            CalculateTotalPremi();
        }

        private void CalculateLoading()
        {
            LoadingPremi = 0;
            foreach (CalculatedPremi item in CalculatedPremiItems)
                LoadingPremi += item.Loading;
        }
        private string CheckSumInsured()
        {
            string message = "";
            if (vptModel != null)
            {
                switch (vptModel.ToleranceType)
                {
                    case 0:
                        if (SumInsured != VehiclePrice)
                        {
                            message = "The sum insured of your vehicle has exceeded the limit of market price";
                            SumInsured = VehiclePrice;
                        }
                        break;
                    case 1:
                        if (SumInsured > VehiclePrice +
                                (VehiclePrice * Convert.ToDouble(vptModel.TolerancePct / 100)) ||
                                SumInsured < VehiclePrice -
                                        (VehiclePrice * Convert.ToDouble(vptModel.TolerancePct / 100)))
                        {
                            message = "The sum insured of your vehicle has exceeded +/- " +
                                    Convert.ToInt32(vptModel.TolerancePct) + "% of market price";
                            SumInsured = VehiclePrice;
                        }
                        break;
                    case 2:
                        if (SumInsured > VehiclePrice + Convert.ToDouble(vptModel.ToleranceAmt) ||
                                SumInsured < VehiclePrice - Convert.ToDouble(vptModel.ToleranceAmt))
                        {
                            message = "The sum insured of your vehicle has exceeded +/- " +
                                    Util.ToThousand(vptModel.ToleranceAmt) +
                                    " of market price";
                            SumInsured = VehiclePrice;
                        }
                        break;
                }

            }

            return message;
        }

        private void CalculateTotalPremi()
        {
            TotalPremi = 0;
            foreach (CalculatedPremi item in CalculatedPremiItems)
                TotalPremi += item.Premium;

            // 003 begin
            if (TotalPremi != 0)
            {
                TotalPremi += AdminFee + LoadingPremi;
            }
            // 003 end
        }

        private void InitializePremi()
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            if (coveragePeriodItems.Count() > 0 && prdAgreedValueMaster.Count() > 0)
            {

                foreach (CoveragePeriod item in coveragePeriodItems)
                {
                    string query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, prdAgreedValueMaster[0].Product_Code, item.Year * 12.0);

                    CalculatePremi("CASCO ",
                            item.CoverageType == CoverageType.Comprehensive ? "ALLRIK" : "TLO   ",
                            item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, false);

                    List<CoveragePeriodNonBasic> cperiodNonBasicitem = new List<CoveragePeriodNonBasic>();
                    foreach (CoveragePeriodNonBasic cp in coveragePeriodNonBasicItems)
                    {
                        if (cp.Year.Equals(item.Year))
                        {
                            cperiodNonBasicitem.Add(cp);
                        }
                    }

                    foreach (CoveragePeriodNonBasic item2 in cperiodNonBasicitem)
                    {
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC)
                        {
                            CalculatePremi("CASCO ",
                                     item.CoverageType == CoverageType.Comprehensive
                                             ? "SRCC  "
                                             : "SRCTLO",
                                     item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, item2.IsBundling);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD)
                        {
                            CalculatePremi("CASCO ",
                                    item.CoverageType == CoverageType.Comprehensive
                                            ? "FLD   "
                                            : "FLDTLO",
                                    item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, item2.IsBundling);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                        {
                            CalculatePremi("CASCO ",
                                    item.CoverageType == CoverageType.Comprehensive
                                            ? "ETV   "
                                            : "ETVTLO",
                                    item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, item2.IsBundling);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                        {
                            CalculatePremi("CASCO ",
                                    item.CoverageType == CoverageType.Comprehensive
                                            ? "TRS   "
                                            : "TRRTLO",
                                    item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, false);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER && TPLSI > 0)
                        {
                            // CalculatePremi("TPLPER", "MVTPL1", item.Year, TPLSI, false);
                            // 004
                            CalculatePremi("TPLPER", TPLCoverageID,
                                    item.Year, TPLSI, false);
                            // 004
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR && PADRVRSI > 0)
                        {
                            CalculatePremi("PADRVR", "", item.Year, PADRVRSI, false);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS &&
                                PAPASSSI > 0 && PASS > 0)
                        {
                            CalculatePremi("PAPASS",
                                    "C" + PASS,
                                    item.Year, PAPASSSI, false);
                        }
                    }
                }

                RefreshAccessSI();
                RefreshPremi();
            }
        }
        private void RefreshPremi()
        {
            BasicPremi = 0;
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;
            TPLPremi = 0;
            PADRVRPremi = 0;
            PAPASSPremi = 0;
            ACCESSPremi = 0;

            foreach (CalculatedPremi x in CalculatedPremiItems)
            {
                if (x.InterestID.TrimEnd().Equals("CASCO") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    BasicPremi += x.Premium;
                }
                else if (x.InterestID.TrimEnd().Equals("ACCESS") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    ACCESSPremi += x.Premium;
                    IsACCESSChecked = true;
                    IsACCESSSIEnabled = true;
                }
                else if (x.CoverageID.TrimEnd().Equals("SRCC") || x.CoverageID.TrimEnd().Equals("SRCTLO"))
                {
                    SRCCPremi += x.Premium;
                    IsSRCCChecked = true;
                }
                else if (x.CoverageID.TrimEnd().Equals("FLD") || x.CoverageID.TrimEnd().Equals("FLDTLO"))
                {
                    FLDPremi += x.Premium;
                    IsFLDChecked = true;
                }
                else if (x.CoverageID.TrimEnd().Equals("ETV") || x.CoverageID.TrimEnd().Equals("ETVTLO"))
                {
                    ETVPremi += x.Premium;
                    IsETVChecked = true;
                }
                else if (x.CoverageID.TrimEnd().Equals("TRS") || x.CoverageID.TrimEnd().Equals("TRRTLO"))
                {
                    TSPremi += x.Premium;
                    IsTSChecked = true;
                    IsTSEnabled = true;
                    /** start 0040/URF/2017 */
                    //} else if (x.InterestID.equals("TPLPER") && (x.CoverageID.equals("MVTPL1"))) {
                }
                else if (x.InterestID.Equals("TPLPER") && (x.CoverageID.Contains("MVTP")))
                {
                    TPLPremi += x.Premium;
                    IsTPLChecked = true;
                    IsTPLEnabled = true;
                    IsTPLSIEnabled = true;
                }
                else if (x.InterestID.Equals("PADRVR"))
                {
                    PADRVRPremi += x.Premium;
                    IsPADRVRChecked = true;
                    IsPADRVRSIEnabled = true;
                }
                else if (x.InterestID.Equals("PAPASS"))
                {
                    PAPASSPremi += x.Premium;
                    IsPAPASSChecked = true;
                    IsPAPASSEnabled = true;
                    IsPAPASSSIEnabled = true;
                    IsPASSEnabled = true;

                }
            }
        }

        private void SetCoverageTypeNonBasicItems()
        {
            foreach (CoveragePeriod item in coveragePeriodItems)
            {
                foreach (CoveragePeriodNonBasic item2 in coveragePeriodNonBasicItems)
                {
                    if (item.Year == item2.Year)
                    {
                        item2.CoverageType = item.CoverageType;
                    }
                }
            }
        }
        private void RefreshPAPASS(bool chPAPASS)
        {
            // PAPASSSI = OtoSalesUtil.convertToDoubleMoney(etPAPASSSI.getText());
            PAPASSPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.InterestID.Equals("PAPASS"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            if (PAPASSSI > 0 && PASS > 0)
            {
                SetCoverageTypeNonBasicItems();
                if (chPAPASS)
                {
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS)
                        {
                            CalculatePremi("PAPASS", "C" + Convert.ToString(PASS),
                                    item.Year, PAPASSSI, false);
                        }
                    }
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.InterestID.Equals("PAPASS"))
                        {
                            PAPASSPremi += cp.Premium;
                        }
                    }
                }
            }
            //  PAPASSPremi = Util.ToThousand(Convert.ToDecimal(;
            CalculateTotalPremi();
            //etPAPASSSI.setText(OtoSalesUtil.convertThousand(PAPASSSI));
        }

        private void RefreshTPL(bool chTPL)
        {
            /** start 0040/URF/2017 */
            //        TPLSI = OtoSalesUtil.convertToDoubleMoney(etTPLSI.getText());
            //TPLSI = OtoSalesUtil.convertToDoubleMoney(spinnerTpl.getItemSelected());
            /** end 0040/URF/2017 */
            TPLPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                /** start 0040/URF/2017 */
                //            if (cp.InterestID.equals("TPLPER") && cp.CoverageID.equals("MVTPL1")) {
                //                CalculatedPremiItems.remove(i - 1);
                //            }
                if (cp.InterestID.Equals("TPLPER") && cp.CoverageID.Contains("MVTP"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
                /** end 0040/URF/2017 */
            }

            if (TPLSI > 0)
            {
                SetCoverageTypeNonBasicItems();
                if (chTPL)
                {
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER)
                        {
                            /** start 0040/URF/2017 */
                            //                         CalculatePremi("TPLPER", "MVTPL1", item.Year, TPLSI, false);
                            CalculatePremi("TPLPER", TPLCoverageID,
                                    item.Year, TPLSI, false);
                            /** end 0040/URF/2017 */
                        }
                    }
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.InterestID.Equals("TPLPER"))
                        {
                            TPLPremi += cp.Premium;
                        }
                    }
                }
            }

            //tvTPL.setText(OtoSalesUtil.convertThousand(TPLPremi));
            CalculateTotalPremi();
            //        etTPLSI.setText(OtoSalesUtil.convertThousand(TPLSI));
            //spinnerTpl.setSelection(spinnerTpl.getSelectedItemPosition());
        }


        private void RefreshPADRVR(bool chPADRVR)
        {
            // PADRVRSI = OtoSalesUtil.convertToDoubleMoney(etPADRVRSI.getText());
            PADRVRPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.InterestID.Equals("PADRVR"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            if (PADRVRSI > 0)
            {
                SetCoverageTypeNonBasicItems();
                if (chPADRVR)
                {
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR)
                        {
                            CalculatePremi("PADRVR", "", item.Year, PADRVRSI, false);
                        }
                    }
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.InterestID.Equals("PADRVR"))
                        {
                            PADRVRPremi += cp.Premium;
                        }
                    }
                }
            }

            //tvPADRVR.setText(OtoSalesUtil.convertThousand(PADRVRPremi));
            CalculateTotalPremi();
            //etPADRVRSI.setText(OtoSalesUtil.convertThousand(PADRVRSI));
        }


        private void RefreshAccessSI()
        {
            ACCESSPremi = 0;

            if (AccessSI > 0)
            {
                CalculateACCESSPremi();
            }
            else
            {
                for (int i = CalculatedPremiItems.Count(); i > 0; i--)
                {
                    CalculatedPremi cp = CalculatedPremiItems[i - 1];
                    if (cp.InterestID.Equals("ACCESS"))
                    {
                        CalculatedPremiItems.RemoveAt(i - 1);
                    }
                }
                ACCESSPremi = 0;
                //  tvAccessPremi.setText(OtoSalesUtil.convertThousand(ACCESSPremi));
            }

            RefreshBundlingPremi();
            RefreshTS();
            CalculateLoading();
            CalculateTotalPremi();
            //if (!etAccessSI.getText().equals(OtoSalesUtil.convertThousand(AccessSI)))
            //{
            //    etAccessSI.setText(OtoSalesUtil.convertThousand(AccessSI));
            //}
        }

        private void RefreshTS()
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            SetCoverageTypeNonBasicItems();

            TSPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.CoverageID.Equals("TRS   ") || cp.CoverageID.Equals("TRRTLO"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            if (IsTSChecked)
            {
                foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                {
                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                    {
                        string query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                        Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, item.Year * 12.0);

                        CalculatePremi("CASCO ",
                                item.CoverageType == CoverageType.Comprehensive ? "TRS   " : "TRRTLO",
                                item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, false);
                    }
                }

                foreach (CalculatedPremi cp in CalculatedPremiItems)
                {
                    if (cp.CoverageID.Equals("TRS   ") || cp.CoverageID.Equals("TRRTLO"))
                    {
                        TSPremi += cp.Premium;
                    }
                }
            }

            if (AccessSI > 0)
            {
                CalculateACCESSPremi();
            }

            CalculateLoading();
            CalculateTotalPremi();
        }

        private void CalculatePremi(String interestID, String coverageID, int year,
                                double sumInsured, bool isBundling)
        {
            double premi = 0;
            double loadPremi = 0;
            double rateLoad = 0;
            double rate = 0;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            string query = "SELECT * FROM Prd_Interest_Coverage WHERE Product_Code='" +
                ProductCode + "' AND Interest_Id='" + interestID + "'" + (string.IsNullOrEmpty(coverageID) ? " " : " AND Coverage_Id='" + coverageID + "'");
            List<Prd_Interest_Coverage> prdInterestCoverages = db.Fetch<Prd_Interest_Coverage>(query);
            Prd_Interest_Coverage interestCoverage = prdInterestCoverages[0];

            if (interestCoverage != null)
            {
                // 003 begin
                if ((coverageID.Equals("ALLRIK") || coverageID.Equals("TLO   ")) || coverageID.Contains("MVTP") ||
                        isBasicCoverExist(CalculatedPremiItems))
                {
                    if (!string.IsNullOrEmpty(interestCoverage.Scoring_Code.TrimEnd()))
                    {
                        List<Dtl_Rate_Factor> rateFactorItems;

                        try
                        {
                            rateFactorItems = db.Fetch<Dtl_Rate_Factor>(
                                    "SELECT * FROM Dtl_Rate_Factor WHERE Score_Code='" +
                                            interestCoverage.Scoring_Code +
                                            "' AND Insurance_type='000401' AND Coverage_id='" +
                                            interestCoverage.Coverage_Id + "' ORDER BY sequence_no ASC");
                        }
                        catch (Exception ex)
                        {
                            rateFactorItems = new List<Dtl_Rate_Factor>();
                        }

                        if (rateFactorItems.Count() > 0)
                        {
                            Dtl_Rate_Scoring rateScoring = new Dtl_Rate_Scoring();
                            try
                            {
                                rateScoring = GetRateScoring(rateFactorItems,
                                 interestCoverage.Scoring_Code, interestCoverage.Coverage_Id,
                                sumInsured, false, CityCode, type, UsageCode);
                            }
                            catch (Exception ex)
                            {
                                rateScoring = null;
                            }

                            if (rateScoring != null)
                            {
                                rate = Convert.ToDouble(rateScoring.Rate);
                            }
                        }
                    }
                    else
                    {
                        rate = Convert.ToDouble(interestCoverage.Rate);
                    }
                }
                // 003 end

                premi = rate / 100 * sumInsured;
                rateLoad = GetRateLoad(interestCoverage.Coverage_Id, year);
                loadPremi = rateLoad / 100 * premi;

                CalculatedPremi calculatedPremi = new CalculatedPremi();
                calculatedPremi.CoverageID = interestCoverage.Coverage_Id;
                calculatedPremi.InterestID = interestID;
                calculatedPremi.Premium = premi;
                calculatedPremi.Rate = rate;
                calculatedPremi.Year = year;
                calculatedPremi.Loading = loadPremi;
                calculatedPremi.LoadingRate = rateLoad;
                calculatedPremi.SumInsured = sumInsured;
                calculatedPremi.IsBundling = isBundling;

                CalculatedPremiItems.Add(calculatedPremi);
            }
        }

        private double GetRateLoad(String coverageID, int year)
        {
            double rateLoad = 0;
            int yearNow = DateTime.Now.Year;

            double yearLoad = yearNow - Convert.ToInt32(Year) + year - 1;
            //Kalau usia kenderaan melebihi age loading yang terakhir, maka ambil age terakhir,
            // lalu tambah 5% untuk kelebihan tiap tahunnya
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            string query = @"SELECT * FROM Prd_Load_Mv WHERE Product_Code=@0 AND Insurance_Code=@1 AND Coverage_Id=@2";

            List<Prd_Load_Mv> loadMVCoverage = db.Fetch<Prd_Load_Mv>(query, ProductCode, type, coverageID);

            if (loadMVCoverage != null)
            {
                if (loadMVCoverage.Count() > 0)
                {
                    Prd_Load_Mv maxLoadItem = db.FirstOrDefault<Prd_Load_Mv>(query + " Order By Age DESC", ProductCode, type, coverageID);
                    Prd_Load_Mv minLoadItem = db.FirstOrDefault<Prd_Load_Mv>(query + " Order By Age ASC", ProductCode, type, coverageID);
                    if (yearLoad < Convert.ToDouble(minLoadItem.Age))
                    {
                        rateLoad = 0;
                    }
                    else if (yearLoad > Convert.ToDouble(maxLoadItem.Age))
                    {
                        rateLoad = Convert.ToDouble(maxLoadItem.Load_Pct) + ((yearLoad - Convert.ToDouble(maxLoadItem.Age)) * 5);
                    }
                    else
                    {
                        Prd_Load_Mv loadMv = db.FirstOrDefault<Prd_Load_Mv>(query + " AND Age=@3", ProductCode, type, coverageID, yearLoad);
                        if (loadMv != null)
                        {
                            rateLoad = Convert.ToDouble(loadMv.Load_Pct);
                        }
                    }
                }
            }
            return rateLoad;
        }

        private bool isBasicCoverExist(List<CalculatedPremi> CalculatedPremiItems)
        {
            bool isExist = false;
            foreach (CalculatedPremi x in CalculatedPremiItems)
            {
                if (x.InterestID.Equals("CASCO ") &&
                        (x.CoverageID.Equals("ALLRIK") || x.CoverageID.Equals("TLO   ")))
                {
                    if (x.Rate != 0)
                    {
                        isExist = true;
                    }
                }
            }
            return isExist;
        }
        private Dtl_Rate_Scoring GetRateScoring(List<Dtl_Rate_Factor> rateFactorItems,
                                            String scoringCode,
                                            String coverageID,
                                            double sumInsured,
                                            bool isCheckBundling, string CityCode, string type, string UsageCode)
        {
            string query = "";
            Dtl_Rate_Scoring data = null;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    query = RateScoringQueryBuilder(rateFactorItems, scoringCode, coverageID, sumInsured, isCheckBundling, false, CityCode, type, UsageCode);
                    List<Dtl_Rate_Scoring> dataList = db.Fetch<Dtl_Rate_Scoring>(query);
                    // 003 begin
                    if (dataList.Count() == 0 && !isCheckBundling)
                    {
                        query = RateScoringQueryBuilder(rateFactorItems, scoringCode, coverageID, sumInsured, isCheckBundling, true, CityCode, type, UsageCode);
                        dataList = db.Fetch<Dtl_Rate_Scoring>(query);
                    }
                    // 003 end
                    data = dataList[0];
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return data;
        }


        private String RateScoringQueryBuilder(List<Dtl_Rate_Factor> rateFactorItems,
                                           String scoringCode,
                                           String coverageID,
                                           double sumInsured,
                                           bool isCheckBundling,
                                           bool undefinedVehicleType, string CityCode, string type, string UsageCode)
        {
            String query = "SELECT * FROM Dtl_Rate_Scoring WHERE " +
                    "Score_code='" + scoringCode + "' AND " +
                    "Insurance_Type='000401' AND " +
                    "Coverage_ID='" + coverageID + "' AND " +
                //                "BC_Up_To_SI >= " + to()._string(sumInsured) + " ";
                    "(BC_Up_To_SI >= " + Convert.ToString(sumInsured) + " OR BC_Up_To_SI = -1) "; // bsy edit for bcuptosi = -1

            if (!isCheckBundling)
            {
                query += "AND Rate > 0 ";
            }

            foreach (Dtl_Rate_Factor item in rateFactorItems)
            {
                String ratingValue;
                if (item.factor_code.Equals("GEOGRA"))
                {
                    ratingValue = CityCode;
                }
                else if (item.factor_code.Equals("MVUSAG"))
                {
                    ratingValue = UsageCode;
                }
                else if (item.factor_code.Equals("VHCTYP"))
                {
                    ratingValue = type;
                    if (undefinedVehicleType)
                    {
                        ratingValue = "ALL   ";
                    }
                }
                else
                {
                    ratingValue = CityCode;
                }
                query += "AND Rating_value" + Convert.ToString(item.sequence_no) + "='" + ratingValue + "' ";
            }
            query += "ORDER BY BC_Up_To_SI ASC";

            return query;
        }


        private Product getProductExternal(string ChannelSource, bool isVehicleNew)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                string pType = "Used";
                if (isVehicleNew)
                {
                    pType = "New";
                }

                string sqlQuery = "SELECT Product.* FROM ProductAgent " +
                        "JOIN Product ON Product.ProductCode = ProductAgent.ProductCode " +
                        "WHERE ProductAgent.ChannelSource='" + ChannelSource + "' " +
                        "AND ProductType='" + pType + "' ORDER BY ProductAgent.LastUpdatedTime DESC";


                return db.FirstOrDefault<Product>(sqlQuery);
            }
        }

        private void IncludeBundlingCoverage(int year, bool isBundling)
        {
            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                     CoverageTypeNonBasic.SRCC, year, isBundling));
            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                    CoverageTypeNonBasic.FLD, year, isBundling));
            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                    CoverageTypeNonBasic.ETV, year, isBundling));

            IsSRCCChecked = true;
            IsFLDChecked = true;
            IsETVChecked = true;
            IsTSEnabled = true;
        }

        #endregion

        [HttpPost]
        public IHttpActionResult SaveProspect(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string State = form.Get("State");
                string PCData = form.Get("ProspectCustomer");
                string FUData = form.Get("FollowUp");
                string FUHData = form.Get("FollowUpHistory");
                string ENData = form.Get("EmailNotification ");
                string OSData = form.Get("OrderSimulation");
                string OSMVData = form.Get("OrderSimulationMV");
                // string OSIData = form.Get("OrderSimulationInterest");
                string calculatedPremiItems = form.Get("calculatedPremiItems");
                string query = "";
                string CustIdEnd = "";
                string FollowUpNoEnd = "";
                string OrderNoEnd = "";

                FollowUp FUEnd = null;
                ProspectCustomer PCEnd = null;

                #region Prospect Tab
                if (State.Equals("PROSPECT") || State.Equals("PREMIUMSIMULATION"))
                {
                    string CustId = "";
                    string FollowUpNumber = "";
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT UPDATE PROSPECT CUSTOMER

                        ProspectCustomer PC = JsonConvert.DeserializeObject<ProspectCustomer>(PCData);
                        string CustID = System.Guid.NewGuid().ToString();
                        PC.CustID = string.IsNullOrEmpty(PC.CustID) ? CustID : PC.CustID;
                        query = @";IF EXISTS(SELECT * FROM ProspectCustomer WHERE CustId=@0)
BEGIN
UPDATE ProspectCustomer SET
       [Name]=@1
      ,[Phone1]=@2
      ,[Phone2]=@3
      ,[Email1]=@4
      ,[Email2]=@5
      ,[CustIDAAB]=@6
      ,[SalesOfficerID]=@7
      ,[DealerCode]=@8
      ,[SalesDealer]=@9
      ,[BranchCode]=@10
      ,[LastUpdatedTime]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCustomer ([CustID]
      ,[Name]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[CustIDAAB]
      ,[SalesOfficerID]
      ,[DealerCode]
      ,[SalesDealer]
      ,[BranchCode]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,GETDATE(),GETDATE(),1)
END";
                        PC.SalesOfficerID = PC.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", PC.SalesOfficerID) : PC.SalesOfficerID;

                        db.Execute(query, PC.CustID, PC.Name, PC.Phone1, PC.Phone2, PC.Email1, PC.Email2, PC.CustIDAAB, PC.SalesOfficerID, PC.DealerCode, PC.SalesDealer, PC.BranchCode);
                        #endregion


                        #region INSERT UPDATE FOLLOWUP
                        FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);


                        string FollowUpNo = System.Guid.NewGuid().ToString();
                        FU.FollowUpNo = string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo;
                        FU.CustID = string.IsNullOrEmpty(FU.CustID) ? CustID : FU.CustID;
                        FU.LastSeqNo = db.ExecuteScalar<int>(";IF EXISTS(SELECT * FROM FollowUpHistory Where FollowUpNo=@0)BEGIN SELECT ISNULL(MAX(SeqNo),0) From FollowUpHistory WHERE FollowUpNo=@0 END ELSE BEGIN SELECT 0 END", string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo);
                        query = @";IF EXISTS(SELECT * FROM FollowUp WHERE FollowUpNo=@0)
BEGIN
    UPDATE FollowUp SET [LastSeqNo]=@1
      ,[CustID]=@2
      ,[ProspectName]=@3
      ,[Phone1]=@4
      ,[Phone2]=@5
      ,[SalesOfficerID]=@6
      ,[EntryDate]=GETDATE()
      ,[FollowUpName]=@7
      ,[FollowUpStatus]=@8
      ,[FollowUpInfo]=@9
      ,[Remark]=@10
      ,[BranchCode]=@11
      ,[LastUpdatedTime]=GETDATE()
      ,[PolicyId]=@12
      ,[PolicyNo]=@13
      ,[TransactionNo]=@14
      ,[SalesAdminID]=@15
      ,[SendDocDate]=@16
      ,[IdentityCard]=@17
      ,[STNK]=@18
      ,[SPPAKB]=@19
      ,[BSTB1]=@20
      ,[BSTB2]=@21
      ,[BSTB3]=@22
      ,[BSTB4]=@23
      ,[CheckListSurvey1]=@24
      ,[CheckListSurvey2]=@25
      ,[CheckListSurvey3]=@26
      ,[CheckListSurvey4]=@27
      ,[PaymentReceipt1]=@28
      ,[PaymentReceipt2]=@29
      ,[PaymentReceipt3]=@30
      ,[PaymentReceipt4]=@31
      ,[PremiumCal1]=@32
      ,[PremiumCal2]=@33
      ,[PremiumCal3]=@34
      ,[PremiumCal4]=@35
      ,[FormA1]=@36
      ,[FormA2]=@37
      ,[FormA3]=@38
      ,[FormA4]=@39
      ,[FormB1]=@40
      ,[FormB2]=@41
      ,[FormB3]=@42
      ,[FormB4]=@43
      ,[FormC1]=@44
      ,[FormC2]=@45
      ,[FormC3]=@46
      ,[FormC4]=@47
      ,[FUBidStatus]=@48 WHERE [FollowUpNo]=@0
END
    ELSE
BEGIN
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[PolicyId]
      ,[PolicyNo]
      ,[TransactionNo]
      ,[SalesAdminID]
      ,[SendDocDate]
      ,[IdentityCard]
      ,[STNK]
      ,[SPPAKB]
      ,[BSTB1]
      ,[BSTB2]
      ,[BSTB3]
      ,[BSTB4]
      ,[CheckListSurvey1]
      ,[CheckListSurvey2]
      ,[CheckListSurvey3]
      ,[CheckListSurvey4]
      ,[PaymentReceipt1]
      ,[PaymentReceipt2]
      ,[PaymentReceipt3]
      ,[PaymentReceipt4]
      ,[PremiumCal1]
      ,[PremiumCal2]
      ,[PremiumCal3]
      ,[PremiumCal4]
      ,[FormA1]
      ,[FormA2]
      ,[FormA3]
      ,[FormA4]
      ,[FormB1]
      ,[FormB2]
      ,[FormB3]
      ,[FormB4]
      ,[FormC1]
      ,[FormC2]
      ,[FormC3]
      ,[FormC4]
      ,[FUBidStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38,@39,@40,@41,@42,@43,@44,@45,@46,@47,@48)
END";
                        FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;

                        db.Execute(query, FU.FollowUpNo
     , FU.LastSeqNo
     , FU.CustID
     , FU.ProspectName
     , FU.Phone1
     , FU.Phone2
     , FU.SalesOfficerID
     , FU.FollowUpName
     , FU.FollowUpStatus
     , FU.FollowUpInfo
     , FU.Remark
     , FU.BranchCode
     , FU.PolicyId
     , FU.PolicyNo
     , FU.TransactionNo
     , FU.SalesAdminID
     , FU.SendDocDate
     , FU.IdentityCard
     , FU.STNK
     , FU.SPPAKB
     , FU.BSTB1
     , FU.BSTB2
     , FU.BSTB3
     , FU.BSTB4
     , FU.CheckListSurvey1
     , FU.CheckListSurvey2
     , FU.CheckListSurvey3
     , FU.CheckListSurvey4
     , FU.PaymentReceipt1
     , FU.PaymentReceipt2
     , FU.PaymentReceipt3
     , FU.PaymentReceipt4
     , FU.PremiumCal1
     , FU.PremiumCal2
     , FU.PremiumCal3
     , FU.PremiumCal4
     , FU.FormA1
     , FU.FormA2
     , FU.FormA3
     , FU.FormA4
     , FU.FormB1
     , FU.FormB2
     , FU.FormB3
     , FU.FormB4
     , FU.FormC1
     , FU.FormC2
     , FU.FormC3
     , FU.FormC4
     , FU.FUBidStatus);
                        #endregion

                        PCEnd = PC;
                        FUEnd = FU;


                        //                        #region INSERT FOLLOWUP HISTORY
                        //                        FollowUpHistory FUH = new FollowUpHistory();
                        //                        FUH.BranchCode = FU.BranchCode;
                        //                        FollowUpNumber = FUH.FollowUpNo = string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo;
                        //                        FUH.SeqNo = db.ExecuteScalar<int>("SELECT LastSeqNo FROM FollowUp WHERE FollowUpNo=@0", FUH.FollowUpNo) + 1;
                        //                        CustId = FUH.CustID = string.IsNullOrEmpty(FU.CustID) ? CustID : FU.CustID;
                        //                        FUH.ProspectName = FU.ProspectName;
                        //                        FUH.Phone1 = FU.Phone1;
                        //                        FUH.Phone2 = FU.Phone2;
                        //                        FUH.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;
                        //                        FUH.FollowUpName = FU.FollowUpName;
                        //                        FUH.NextFollowUpDate = FU.NextFollowUpDate;
                        //                        FUH.LastFollowUpDate = FU.LastFollowUpDate;
                        //                        FUH.FollowUpStatus = FU.FollowUpStatus;
                        //                        FUH.FollowUpInfo = FU.FollowUpInfo;
                        //                        FUH.Remark = FU.Remark;
                        //                        FUH.BranchCode = FU.BranchCode;

                        //                        query = @"; IF NOT EXISTS(SELECT  * FROM FollowupHistory WHERE FollowUpNo=@0)
                        //BEGIN
                        //    INSERT INTO FollowUpHistory (
                        //       [FollowUpNo]
                        //      ,[SeqNo]
                        //      ,[CustID]
                        //      ,[ProspectName]
                        //      ,[Phone1]
                        //      ,[Phone2]
                        //      ,[SalesOfficerID]
                        //      ,[EntryDate]
                        //      ,[FollowUpName]
                        //      ,[NextFollowUpDate]
                        //      ,[LastFollowUpDate]
                        //      ,[FollowUpStatus]
                        //      ,[FollowUpInfo]
                        //      ,[Remark]
                        //      ,[BranchCode]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),1)
                        //END
                        //";
                        //                        db.Execute(query, FUH.FollowUpNo
                        //     , FUH.SeqNo
                        //     , FUH.CustID
                        //     , FUH.ProspectName
                        //     , FUH.Phone1
                        //     , FUH.Phone2
                        //     , FUH.SalesOfficerID
                        //     , FUH.FollowUpName
                        //     , FUH.NextFollowUpDate
                        //     , FUH.LastFollowUpDate
                        //     , FUH.FollowUpStatus
                        //     , FUH.FollowUpInfo
                        //     , FUH.Remark
                        //     , FUH.BranchCode);
                        //                        #endregion
                        #region SAVE IMAGE
                        query = @";
SELECT [ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                        List<dynamic> ImageData = db.Fetch<dynamic>(query, FU.FollowUpNo);
                        foreach (dynamic id in ImageData)
                        {
                            query = @"IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8)
END";
                            db.Execute(query, id.ImageID, id.DeviceID
      , id.SalesOfficerID
      , id.PathFile
      , id.EntryDate
      , id.LastUpdatedTime
      , id.RowStatus
      , id.Data
      , id.ThumbnailData);
                        }
                        #endregion
                    }
                    //return Json(new { status = true, message = "Success", CustId, FollowUpNumber });
                    CustIdEnd = CustId;
                    FollowUpNoEnd = FollowUpNumber;

                }
                #endregion

                #region Vehicle Tab
                if (State.Equals("VEHICLE") || State.Equals("PREMIUMSIMULATION"))
                {
                    string OrderNoReturn = null;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT ORDERSIMULATION
                        OrderSimulation OS = JsonConvert.DeserializeObject<OrderSimulation>(OSData);
                        string OrderNo = System.Guid.NewGuid().ToString();
                        OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                        OrderNoReturn = OS.OrderNo;
                        OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;
                        OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;
                        OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                        OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                        OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25)
END";
                        OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                        db.Execute(query, OS.OrderNo
      , OS.CustID
      , OS.FollowUpNo
      , OS.QuotationNo
      , OS.MultiYearF
      , OS.YearCoverage
      , OS.TLOPeriod
      , OS.ComprePeriod
      , OS.BranchCode
      , OS.SalesOfficerID
      , OS.PhoneSales
      , OS.DealerCode
      , OS.SalesDealer
      , OS.ProductTypeCode
      , OS.AdminFee
      , OS.TotalPremium
      , OS.Phone1
      , OS.Phone2
      , OS.Email1
      , OS.Email2
      , OS.SendStatus
      , OS.InsuranceType
      , OS.ApplyF
      , OS.SendF
      , OS.LastInterestNo
      , OS.LastCoverageNo);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION MV
                        OrderSimulationMV OSMV = JsonConvert.DeserializeObject<OrderSimulationMV>(OSMVData);
                        OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                        query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                        db.Execute(query, OSMV.OrderNo
      , OSMV.ObjectNo
      , OSMV.ProductTypeCode
      , OSMV.VehicleCode
      , OSMV.BrandCode
      , OSMV.ModelCode
      , OSMV.Series
      , OSMV.Type
      , OSMV.Sitting
      , OSMV.Year
      , OSMV.CityCode
      , OSMV.UsageCode
      , OSMV.SumInsured
      , OSMV.AccessSI
      , OSMV.RegistrationNumber
      , OSMV.EngineNumber
      , OSMV.ChasisNumber
      , OSMV.LastUpdatedTime
      , OSMV.RowStatus
      , OSMV.IsNew);
                        #endregion
                    }

                    //return Json(new { status = true, message = "Success", OrderNo = OrderNoReturn });
                    OrderNoEnd = OrderNoReturn;
                }
                #endregion

                #region Cover Tab
                if (State.Equals("COVER") || State.Equals("PREMIUMSIMULATION"))
                {
                    OrderSimulation OS = JsonConvert.DeserializeObject<OrderSimulation>(OSData);
                    OrderSimulationMV OSMV = JsonConvert.DeserializeObject<OrderSimulationMV>(OSMVData);

                    List<OrderSimulationInterest> OSI = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> OSC = new List<OrderSimulationCoverage>();
                    OS.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OS.OrderNo;
                    OSMV.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OSMV.OrderNo;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        int a = 1;
                        List<CalculatedPremi> calculatedPremiItem = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculatedPremiItems);
                        foreach (CalculatedPremi cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            foreach (OrderSimulationInterest item in OSI)
                            {
                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && Convert.ToInt32(item.Year.TrimEnd()) == cp.Year)
                                {
                                    isexist = true;
                                }
                                {

                                }
                            }
                            if (!isexist)
                            {
                                OrderSimulationInterest osi = new OrderSimulationInterest();
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                foreach (CalculatedPremi cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.Year == cp.Year)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;
                        foreach (OrderSimulationInterest item in OSI)
                        {
                            foreach (CalculatedPremi item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()) && item2.Year == Convert.ToInt32(item.Year))
                                {
                                    OrderSimulationCoverage oscItem = new OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }
                        #region INSERT UPDATE ORDERSIMULATION INTEREST
                        #region Insert
                        OrderSimulationInterest count = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);
                        if (count == null)
                        {
                            int i = 1;
                            foreach (OrderSimulationInterest data in OSI)
                            {
                                query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,DATEADD(YEAR,@6,GETDATE()),DATEADD(YEAR,@7,GETDATE()),GETDATE(),1)";
                                db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, Convert.ToInt32(data.Year) - 1, Convert.ToInt32(data.Year));
                                i++;
                            }
                        }
                        else
                        {

                            #region Update
                            List<OrderSimulationInterest> TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);

                            foreach (OrderSimulationInterest data in TempAllOSI)
                            {
                                bool isExist = false;
                                foreach (OrderSimulationInterest data2 in OSI)
                                {
                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) && data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                                    {
                                        isExist = true;
                                        query = @"UPDATE OrderSimulationInterest SET 
                                Premium=@5,
                                LastUpdatedTime=GETDATE()
                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year, data2.Premium);
                                    }
                                }

                                if (!isExist)
                                {
                                    //soft delete coverage

                                    query = @"UPDATE OrderSimulationCoverage SET 
                                RowStatus=0,
                                LastUpdatedTime=GETDATE()
                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 ";
                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo);


                                    //soft delete interest
                                    query = @"UPDATE OrderSimulationInterest SET 
                                RowStatus=0,
                                LastUpdatedTime=GETDATE()
                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                                }
                            }
                            //merge data from new collection to old collection
                            //insert new data if not exist in interestItems

                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                            foreach (OrderSimulationInterest data in OSI2)
                            {
                                bool isExist = false;
                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                                {
                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                                    {
                                        isExist = true;
                                    }
                                }
                                if (!isExist)
                                {
                                    //check if exist in deleted Collection
                                    //if exist, update rowstate from D to U
                                    OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                                    if (osiDeletedItem != null)
                                    {
                                        query = @"UPDATE OrderSimulationInterest SET 
                                RowStatus=1,
                                LastUpdatedTime=GETDATE()
                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                                    }
                                    else
                                    {
                                        int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                                        i++;
                                        query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,DATEADD(YEAR,@6,GETDATE()),DATEADD(YEAR,@7,GETDATE()),GETDATE(),1)";
                                        db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, Convert.ToInt32(data.Year) - 1, Convert.ToInt32(data.Year));

                                    }
                                }
                            }
                            #endregion
                        }
                        #endregion
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION COVERAGE

                        #region Insert
                        OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);
                        if (countC == null)
                        {
                            int i = 1;
                            foreach (OrderSimulationCoverage data in OSC)
                            {
                                query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,GETDATE(),GETDATE(),GETDATE(),1,@10)";
                                db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.IsBundling ? 1 : 0);
                                i++;
                            }
                        }
                        else
                        {

                            #region Update
                            List<OrderSimulationCoverage> TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                            foreach (OrderSimulationCoverage data in TempAllOSC)
                            {

                                OrderSimulationInterest osiItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2", data.OrderNo, data.ObjectNo, data.InterestNo);
                                if (osiItem != null)
                                {
                                    bool isExist = false;
                                    foreach (OrderSimulationCoverage item in OSC)
                                    {
                                        if (item.CoverageID.TrimEnd().Equals(data.CoverageID.TrimEnd()) && item.InterestNo.Equals(data.InterestNo) && item.CoverageNo.Equals(data.CoverageNo))
                                        {
                                            isExist = true;
                                            query = @"UPDATE OrderSimulationCoverage SET 
      [Rate]=@5
      ,[SumInsured]=@6
      ,[LoadingRate]=@7
      ,[Loading]=@8
      ,[Premium]=@9
      ,[BeginDate]=@10
      ,[EndDate]=@11
      ,[LastUpdatedTime]=GETDATE()
      ,[RowStatus]=1
      ,[IsBundling]=@12 WHERE [OrderNo]=@0 AND [ObjectNo]=@1 AND [InterestNo]=@2 AND [CoverageNo] =@3 AND [CoverageID]=@4";
                                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0);

                                        }
                                    }
                                    //delete data
                                    if (!isExist)
                                    {
                                        //soft delete coverage

                                        query = @"UPDATE OrderSimulationCoverage SET 
                                RowStatus=0,
                                LastUpdatedTime=GETDATE()
                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageId=@4";
                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID);


                                    }
                                }

                            }
                            //merge data from new collection to old collection
                            //insert new data if not exist in interestItems
                            TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);

                            foreach (OrderSimulationCoverage item in OSC)
                            {
                                bool isExist = false;
                                foreach (OrderSimulationCoverage data in TempAllOSC)
                                {
                                    if (data.InterestNo.Equals(item.InterestNo) &&
                           data.CoverageNo.Equals(item.CoverageNo) && data.CoverageID.TrimEnd().Equals(item.CoverageID.TrimEnd()))
                                    {
                                        isExist = true;
                                    }
                                }
                                if (isExist)
                                {
                                    OrderSimulationCoverage oscitem = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageID=@4", item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.CoverageID);
                                    if (oscitem != null)
                                    {
                                        query = @"UPDATE OrderSimulationCoverage SET 
                                RowStatus=1
                                ,Rate=@4
                                ,SumInsured=@5
                                ,LoadingRate=@6
                                ,Loading=@7
                                ,Premium=@8
                                ,IsBundling=@9     
                                ,LastUpdatedTime=GETDATE()                           
WHERE OrderNo=@0 AND ObjectNo = @1 AND InterestNo=@2 AND CoverageNo=@3";
                                        db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.Rate, item.SumInsured, item.LoadingRate, item.LoadingRate, item.Premium, item.IsBundling ? 1 : 0);
                                    }
                                }
                                else
                                {

                                    int i = db.ExecuteScalar<int>("SELECT max(CoverageNo) FROM OrderSimulationCoverage WHERE OrderNo=@0 ", item.OrderNo);
                                    i++;
                                    query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,GETDATE(),GETDATE(),GETDATE(),1,@10)";
                                    db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, i, item.CoverageID, item.Rate, item.SumInsured, item.LoadingRate, item.Loading, item.Premium, item.IsBundling ? 1 : 0);
                                    i++;
                                }


                            }
                            #endregion
                        }
                        #endregion
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);

                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion

                    }

                    return Json(new { status = true, message = "Success", OrderSimulation = OS, FollowUpNo = FollowUpNoEnd });
                }
                #endregion
                return Json(new { status = true, message = "Success", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getAccessoriesList(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<dynamic> result = db.Fetch<dynamic>(@"SELECT Description , MaxSI FROM Accessories WHERE RowStatus=1");
                return Json(new { status = true, message = "Success", data = result });

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult SendQuotationSMS(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Phone = form.Get("Phone");
            string OrderNo = form.Get("OrderNo");
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                OrderSimulation os = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE OrderNo=@0", OrderNo);
                OrderSimulationMV osmv = db.FirstOrDefault<OrderSimulationMV>("SELECT TOP 1 * FROM OrderSimulationMV WHERE OrderNo=@0", OrderNo);
                string vehicle = db.ExecuteScalar<string>("SELECT TOP 1 VB.Description+' '+VM.Description+' '+ @3+' TH '+@2 FROM  VehicleBrand VB  LEFT JOIN VehicleModel VM ON VM.BrandCode=@1 AND VM.ModelCode=@0 WHERE VB.BrandCode=@1 ", osmv.ModelCode, osmv.BrandCode, osmv.Year, osmv.Series);

                string coveragedesc = (os.ComprePeriod) > 0 ? "Comprehensive " + os.ComprePeriod + ((os.ComprePeriod == 1) ? " Year" : " Years") : "";
                coveragedesc = os.TLOPeriod == 0 ? coveragedesc : string.IsNullOrEmpty(coveragedesc) ? "" : coveragedesc + ", ";
                coveragedesc = coveragedesc + ((os.TLOPeriod) > 0 ? "TLO " + os.TLOPeriod + ((os.TLOPeriod == 1) ? " Year" : " Years") : "");
                string message = "SIMULASI: " + coveragedesc + " " + (os.ComprePeriod + os.TLOPeriod);
                message += " " + vehicle.ToUpper();
                message += " Premi Rp " + Util.ToThousand(os.TotalPremium);

                string statusMessage = MessageController.SendSMS(message, Phone);
                return Json(new { status = true, message = "Success" });

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult UploadImage(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string ext = form.Get("ext");
            string ImageType = form.Get("ImageType");
            string FollowUpNo = form.Get("FollowUpNo");
            string DeviceID = form.Get("DeviceID");
            string SalesOfficerID = form.Get("SalesOfficerID");
            string Data = form.Get("Data");
            string ThumbnailData = form.Get("ThumbnailData");
            FollowUpNo = string.IsNullOrEmpty(FollowUpNo) ? System.Guid.NewGuid().ToString() : FollowUpNo;
            byte[] rawData = new byte[Data.Length / 2];
            for (int i = 0; i < rawData.Length; i++)
            {
                rawData[i] = Convert.ToByte(Data.Substring(i * 2, 2), 16);
            }

            byte[] rawThumb = new byte[ThumbnailData.Length / 2];
            for (int i = 0; i < rawThumb.Length; i++)
            {
                rawThumb[i] = Convert.ToByte(ThumbnailData.Substring(i * 2, 2), 16);
            }

            try
            {
                string ImageID = System.Guid.NewGuid().ToString();
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string query = @";IF EXISTS(SELECT * FROM TempImageData Where FollowUpNo=@0 AND ImageType=@3 AND SalesOfficerID=@4)
                BEGIN
                UPDATE TempImageData SET ImageID=@1
                ,DeviceID=@2
                ,PathFile=@5
                ,LastUpdatedTime=GETDATE()
                ,rowstatus=1
                ,Data=Convert(varbinary(max),@6)
                ,ThumbnailData=Convert(varbinary(max),@7)
                 WHERE FollowUpNo=@0 AND ImageType=@3 AND SalesOfficerID=@4
                END
                ELSE
                BEGIN
                INSERT INTO TempImageData (FollowUpNo,ImageID,DeviceID,ImageType,SalesOfficerID,PathFile,EntryDate,LastUpdatedTime,RowStatus,Data,ThumbnailData) 
                VALUES(@0,@1,@2,@3,@4,@5,GETDATE(),GETDATE(),1,@6,@7)
                END";
                string ImageId = ImageID + ext;
                SalesOfficerID = SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", SalesOfficerID) : SalesOfficerID;

                db.Execute(query, FollowUpNo, ImageID, DeviceID, ImageType, SalesOfficerID, ImageId, rawData, rawThumb);

                return Json(new { status = true, message = "OK", data = ImageId, FollowUpNo = FollowUpNo });

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        private A2isMessagingAttachment GenerateReportFile(string orderNo, int yearCoverage, string fileName, int insuranceType)
        {

            A2isMessagingAttachment att = new A2isMessagingAttachment();
            string filePath = "";
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportPathOne = ConfigurationManager.AppSettings["ReportPathOne"].ToString();
            string ReportPathTinsOne = ConfigurationManager.AppSettings["ReportPathTinsOne"].ToString();
            string ReportPathLexusOne = ConfigurationManager.AppSettings["ReportPathLexusOne"].ToString();
            string ReportPathTwo = ConfigurationManager.AppSettings["ReportPathTwo"].ToString();
            string ReportPathTinsTwo = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusTwo = ConfigurationManager.AppSettings["ReportPathLexusTwo"].ToString();
            string ReportPathThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathTinsThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusThree = ConfigurationManager.AppSettings["ReportPathLexusThree"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;
            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";


            NetworkCredential NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);


            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;

                using (ReportViewer rView = new ReportViewer())
                {
                    rView.ProcessingMode = ProcessingMode.Remote;

                    ServerReport serverReport = rView.ServerReport;
                    serverReport.ReportServerUrl = new Uri(ReportServerURL);
                    switch (yearCoverage)
                    {
                        case 1:
                            switch (insuranceType)
                            {
                                case 1: serverReport.ReportPath = ReportPathOne; break;
                                case 2: serverReport.ReportPath = ReportPathTinsOne; break;
                                case 3: serverReport.ReportPath = ReportPathLexusOne; break;
                            }; break;
                        case 2:
                            switch (insuranceType)
                            {
                                case 1: serverReport.ReportPath = ReportPathTwo; break;
                                case 2: serverReport.ReportPath = ReportPathTinsTwo; break;
                                case 3: serverReport.ReportPath = ReportPathLexusTwo; break;
                            }; break;
                        case 3:
                            switch (insuranceType)
                            {
                                case 1: serverReport.ReportPath = ReportPathThree; break;
                                case 2: serverReport.ReportPath = ReportPathTinsThree; break;
                                case 3: serverReport.ReportPath = ReportPathLexusThree; break;
                            }; break;
                    }
                    serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;

                    ReportParameter[] parameters = new ReportParameter[1];
                    ReportParameter reportParam = new ReportParameter();
                    reportParam.Name = "OrderNo";
                    reportParam.Values.Add(orderNo);
                    parameters[0] = reportParam;
                    rView.ServerReport.SetParameters(parameters);
                    bytes = rView.ServerReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

                    filePath = ReportFiles + fileName + ".pdf"; //"QUOTATION-" + OrderNo + ".pdf";

                    if (File.Exists(filePath))
                        File.Delete(filePath);
                    // 1. Read lock information from file   2. If locked by us, delete the file
                    //using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Delete))
                    //{
                    //    File.Delete(filePath);
                    //}

                    //using (FileStream fs = File.Create(filePath)) { }

                    att.FileByte = bytes;
                    att.FileContentType = "application/pdf";
                    att.FileExtension = ".pdf";
                    att.AttachmentDescription = fileName;
                }

            }
            catch (Exception ex)
            {
                filePath = "";
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();

            }

            return att;
        }


        [HttpPost]
        public IHttpActionResult SendQuotationEmail(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            A2isCommonResult rslt = new A2isCommonResult();
            string Email = form.Get("Email");
            string OrderNo = form.Get("OrderNo");
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();
                string DSOrder = "select a.OrderNo, a.EntryDate,b.Name 'CustName',b.Phone1 + case when isnull(b.Phone2,'')='' then'' else '/' + b.Phone2 end 'CustPhone', ";
                DSOrder += " b.Email1 'CustEmail', so.Name 'SalesName', sr.Description 'SalesRole', so.Phone1 + case when isnull(so.Phone2,'')='' then'' else '/' + so.Phone2 end 'SalesPhone', so.Fax 'SalesFax',";
                DSOrder += " so.OfficePhone 'SalesOfficePhone', so.Ext 'SalesOfficePhoneExt', a.QuotationNo, ab.AccountName, ab.AccountNo, ab.Bank, ab.Description 'Branch', ab.City 'BrachCity',";
                DSOrder += " d.Sequence 'Region', cast(isnull(a.TLOPeriod,0)+isnull(a.ComprePeriod,0) as varchar(10)) + ' Tahun' 'Period',";
                DSOrder += " a.MultiYearF, case when isnull(a.TLOPeriod,0)>0 and isnull(a.ComprePeriod,0)=0 then 'TLO'";
                DSOrder += " when  isnull(a.TLOPeriod,0)=0 and isnull(a.ComprePeriod,0)>0 then 'Comprehensive'";
                DSOrder += " else 'Kombinasi' end 'Pertanggungan', CAST(DAY(GETDATE()) AS VARCHAR(2)) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS [ExecTime], a.InsuranceType";
                DSOrder += " from OrderSimulation a";
                DSOrder += " inner join ProspectCustomer b on a.CustID=b.CustID";
                DSOrder += " inner join SalesOfficer so on a.SalesOfficerID=so.SalesOfficerID";
                DSOrder += " inner join SysUserRole sr on so.Role=sr.Role";
                DSOrder += " inner join AABBranch ab on so.BranchCode=ab.BranchCode";
                DSOrder += " inner join OrderSimulationMV c on a.OrderNo=c.OrderNo";
                DSOrder += " inner join Region d on c.CityCode=d.RegionCode";
                DSOrder += " where a.OrderNo=@0";

                dynamic dtOrder = db.FirstOrDefault<dynamic>(DSOrder, OrderNo);

                string DSVehicle = "select  c.Description + ' ' + d.Description + ' ' + e.Series + ' ' + b.Year 'VehicleDescription',";
                DSVehicle += " a.RegistrationNumber, a.Year, f.Description 'Usage', a.sitting, a.SumInsured, o.YearCoverage";
                DSVehicle += " from OrderSimulation o ";
                DSVehicle += " inner join OrderSimulationMV a on a.OrderNo=o.OrderNo";
                DSVehicle += " inner join Vehicle b on a.VehicleCode=b.VehicleCode and a.BrandCode=b.BrandCode and a.ProductTypeCode=b.ProductTypeCode";
                DSVehicle += " and a.ModelCode=b.ModelCode and a.Series=b.Series and a.Type=b.Type and a.Sitting=b.Sitting and a.Year=b.Year";
                DSVehicle += " inner join VehicleBrand c on a.BrandCode=c.BrandCode";
                DSVehicle += " inner join VehicleModel d on a.BrandCode=d.BrandCode and a.ModelCode=d.ModelCode";
                DSVehicle += " inner join VehicleSeries e on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series";
                DSVehicle += " inner join Dtl_Ins_Factor f on a.UsageCode=f.insurance_code and f.Factor_Code='MVUSAG'";
                DSVehicle += " where a.OrderNo=@0";

                dynamic dtVehicle = db.FirstOrDefault<dynamic>(DSVehicle, OrderNo);

                string NamaPelanggan = dtOrder.CustName;
                string VehicleDescription = dtVehicle.VehicleDescription;
                string JenisPertanggungan = dtOrder.Pertanggungan;
                string PeriodePertanggungan = dtOrder.Period;
                string NamaSales = dtOrder.SalesName;
                string NomorSales = dtOrder.SalesPhone;
                string JabatanSales = dtOrder.SalesRole;
                int yearCoverage = Convert.ToInt32(dtVehicle.YearCoverage);
                int insuranceType = Convert.ToInt32(dtOrder.InsuranceType);
                string fileName = EmailSubject + " - " + NamaPelanggan; // +" - " + VehicleDescription;
                List<A2isMessagingAttachment> attFile = new List<A2isMessagingAttachment>();
                attFile.Add(GenerateReportFile(OrderNo, yearCoverage, fileName, insuranceType));

                if (!string.IsNullOrEmpty(fileName))
                {
                    string EmailBody = @"<html><body>Yth, Bapak/Ibu @NamaPelanggan
<br /><br />
Berikut kami lampirkan penawaran untuk asuransi kendaraan bermotor Anda:
<ul>
	<li>@VehicleDescription - @JenisPertanggungan @PeriodePertanggungan</li>
</ul>
Jika anda membutuhkan informasi lebih lanjut, silahkan menghubungi saya (@NamaSales, @NomorSales) atau melalui Garda Akses 24 Jam di 1 500 112.
<br />
Atas perhatian dan kerjasamanya, kami ucapkan terimakasih.
<br /><br />
<font color=#336699>Terimakasih,
<br /><br /><br />
<b>@NamaSales</b>
<br />
@JabatanSales
<hr color=#00CCFF>
<b>M</b> @NomorSales
<br />
<b>asuransiastra</b>.com
</font></body></html>";
                    string emailText = EmailBody.Replace("@NamaPelanggan", NamaPelanggan).Replace("@VehicleDescription", VehicleDescription)
                        .Replace("@JenisPertanggungan", JenisPertanggungan).Replace("@PeriodePertanggungan", PeriodePertanggungan)
                        .Replace("@NamaSales", NamaSales).Replace("@NomorSales", NomorSales).Replace("@JabatanSales", JabatanSales);
                    rslt = MessageController.SendEmail(Email, emailText, EmailSubject, attFile);

                }
                if (rslt.ResultCode)
                {
                    string query = @"UPDATE ORDERSIMULATION SET SendStatus=1, SendDate=GETDATE() WHERE OrderNo=@0";
                    db.Execute(query, OrderNo);
                    bool isInfoChanged = false;
                    FollowUp FU = db.FirstOrDefault<FollowUp>("SELECT * FROM FollowUp WHERE FollowUpNo=(SELECT TOP 1 FollowUpNo From OrderSimulation WHERE OrderNo=@0)", OrderNo);
                    if (FU.FollowUpStatus < 4 || FU.FollowUpStatus == 9)
                    {

                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
BEGIN
SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
END
ELSE
BEGIN
SELECT 0 AS SeqNo
END", FU.FollowUpNo);
                        if (FU.FollowUpInfo != 3)
                        {
                            isInfoChanged = true;
                            lastseqno++;
                        }
                        FU.LastSeqNo = lastseqno;
                        FU.FollowUpStatus = 2;
                        FU.FollowUpInfo = 3;
                        query = @"UPDATE FollowUp SET 
                           LastSeqNo=@1,
                           FollowUpStatus=@2,
                           FollowUpInfo=@3,
                           NextFollowUpDate=GETDATE(),
                           LastFollowUpDate=GETDATE()
                           WHERE FollowUpNo=@0
                           ";
                        db.Execute(query, FU.FollowUpNo, FU.LastSeqNo, FU.FollowUpStatus, FU.FollowUpInfo);
                        if (isInfoChanged)
                        {
                            #region INSERT FOLLOW UP HISTORY
                            query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),0)
";

                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                            #endregion
                        }
                    }
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + rslt.ResultDesc });

                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



        [HttpPost]
        public IHttpActionResult getVehicleBrand()
        {
            var query = @"SELECT BrandCode, ProductTypeCode, Description, RowStatus FROM VehicleBrand WHERE RowStatus = 1 ORDER BY Description";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleYear(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();

                if (brandCode == null || brandCode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                var query = @"SELECT DISTINCT Year FROM Vehicle WHERE BrandCode=@0 ORDER BY Year DESC";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, brandCode);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleType(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();
                string year = form.Get("year").Trim();

                if (year == null || year == "" || brandCode == null || brandCode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                        SELECT DISTINCT a.* FROM VehicleModel a 
                        INNER JOIN Vehicle b ON a.BrandCode=b.BrandCode 
                        AND a.ProductTypeCode=b.ProductTypeCode 
                        AND a.ModelCode=b.ModelCode WHERE b.BrandCode=@0 AND a.RowStatus = 1 
                        AND b.Year=@1 ORDER BY a.Description
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, brandCode, year);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleSeries(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();
                string modelCode = form.Get("modelCode").Trim();
                string year = form.Get("year").Trim();

                if (year == null || year == "" || brandCode == null || brandCode == "" || modelCode == null || modelCode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                       SELECT DISTINCT a.BrandCode, a.ProductTypeCode,  a.ModelCode, a.Series, a.RowStatus  
                       FROM Vehicle a  JOIN VehicleSeries b  ON a.BrandCode = b.BrandCode  
                       and a.ModelCode = b.ModelCode  and a.ProductTypeCode = b.ProductTypeCode 
                       WHERE a.BrandCode=@0 AND a.ModelCode=@1 AND a.Year=@2 AND a.RowStatus = 1
                       ORDER BY a.Series
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, brandCode, modelCode, year);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();
                string modelCode = form.Get("modelCode").Trim();
                string year = form.Get("year").Trim();
                string series = form.Get("series").Trim();

                if (year == null || year == "" || brandCode == null || brandCode == "" ||
                    modelCode == null || modelCode == "" || series == null || series == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                       SELECT VehicleCode, BrandCode, ProductTypeCode, ModelCode, 
                       Series, Type, Sitting, Year, CityCode, Price 
                       FROM Vehicle WHERE Year = @0
                       And BrandCode = @1 And ModelCode = @2
                       And Series = @3 ORDER BY VehicleCode
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, year, brandCode, modelCode, series);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleUsage()
        {
            string query = @"
 SELECT Factor_Code,insurance_code,Description,status,Entryusr,Entrydt,UpdateUsr,UpdateDt,LastUpdatedTime,RowStatus into #TempDtlInsFact  FROM Dtl_Ins_Factor WITH(NOLOCK) WHERE (Factor_Code in ('GEOGRA') OR (Factor_Code = 'MVUSAG' and insurance_code like 'U%') OR (Factor_Code = 'VHCTYP' and insurance_code like 'T0%'))  ORDER BY LastUpdatedTime, Factor_Code, insurance_code ASC
  SELECT * FROM #TempDtlInsFact WHERE Factor_Code='MVUSAG' AND RowStatus = 1 ORDER BY Description
                ";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleRegion()
        {
            string query = @"
                   SELECT RegionCode, Description, Sequence FROM Region WHERE RowStatus = 1 ORDER BY Description
                ";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getDealerName()
        {
            string query = @";SELECT DealerCode, Description, RowStatus, CustID FROM Dealer WHERE RowStatus = 1 ORDER BY Description
                ";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getSalesmanDealerName(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string dealercode = form.Get("dealercode").Trim();

                if (dealercode == null || dealercode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                      SELECT * FROM SalesmanDealer where DealerCode = @0 AND RowStatus = 1 ORDER BY Description ASC
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, dealercode);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getProductType(FormDataCollection form)
        {

            string Channel = form.Get("Channel").Trim();
            string query = @"
                  SELECT ProductTypeCode, Description, InsuranceType, RowStatus FROM ProductType WHERE RowStatus = 1
                ";
            query = query + ((Channel.Equals("EXT")) ? " AND InsuranceType=1" : "");
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getProductCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string productypecode = form.Get("producttypecode").Trim();
                string insurancetype = form.Get("insurancetype").Trim();
                string Channel = form.Get("Channel").Trim();
                string ChannelSource = form.Get("ChannelSource").Trim();
                string isVehicleNew = form.Get("isVehicleNew").Trim();
                List<Product> result = new List<Product>();
                if (productypecode == null || productypecode == "" || insurancetype == null)
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                if ((Channel.Equals("EXT")))
                {
                    result.Add(getProductExternal(ChannelSource, Convert.ToBoolean(isVehicleNew)));
                }
                else
                {
                    string query = @"
                  SELECT ProductCode, Description, ProductTypeCode, InsuranceType, RowStatus, ValidFrom, ValidTo FROM Product 
                  WHERE ProductTypeCode=@0
                  AND InsuranceType=@1 AND RowStatus = 1
                ";

                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        result = db.Fetch<Product>(query, productypecode, insurancetype);

                    }
                }
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpSummaryAO(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string salesofficerid = form.Get("salesofficerid").Trim();
                string year = form.Get("year").Trim();
                string month = form.Get("month").Trim();

                if (salesofficerid == null || salesofficerid == "" || year == null || year == "" || month == null || month == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                int number;

                if (!(Int32.TryParse(year, out number) && Int32.TryParse(month, out number)))
                {
                    return Json(new { status = false, message = "year or month must number", data = "null" });
                }

                string query = @"
                  SELECT SalesOfficerID, SalesName, BranchCode, BranchDesc, Region, Period, RowStatus, 
                  TotalProspect, TotalNeedFu, TotalPotential, TotalCallLater, TotalCollectDoc, 
                  TotalNotDeal, TotalSendToSA, TotalPolicyCreated, TotalBackToAo, TotalDeal, Frequency
                  FROM FollowUpSummary
                  WHERE SalesOfficerID = @0 
                  and YEAR(Period) = @1 
                  and MONTH(Period) = @2
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, salesofficerid, year, month);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getBranchList(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string search = "%" + form.Get("search").Trim() + "%";

                string query = @"
                 SELECT * FROM Branch WHERE Address <> '' 
                 AND (Name like @0 OR City like @0) ORDER BY Name ASC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, search);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult getBasicCover(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                List<BasicCover> BasicCover = new List<BasicCover>();

                string Channel = form.Get("Channel").Trim();
                if ((Channel.Equals("EXT")))
                {
                    BasicCover bc = new BasicCover();
                    bc.Id = 1;
                    bc.ComprePeriod = 1;
                    bc.TLOPeriod = 0;
                    bc.Description = "Comprehensive";
                    BasicCover.Add(bc);

                    bc = new BasicCover();
                    bc.Id = 2;
                    bc.ComprePeriod = 0;
                    bc.TLOPeriod = 1;
                    bc.Description = "TLO";
                    BasicCover.Add(bc);
                }
                else
                {
                    BasicCover bc = new BasicCover();
                    bc.Id = 1;
                    bc.ComprePeriod = 1;
                    bc.TLOPeriod = 0;
                    bc.Description = "Comprehensive 1 Year";
                    BasicCover.Add(bc);

                    bc = new BasicCover();
                    bc.Id = 2;
                    bc.ComprePeriod = 0;
                    bc.TLOPeriod = 1;
                    bc.Description = "TLO 1 Year";
                    BasicCover.Add(bc);


                    bc = new BasicCover();
                    bc.Id = 3;
                    bc.ComprePeriod = 2;
                    bc.TLOPeriod = 0;
                    bc.Description = "Comprehensive 2 Years";
                    BasicCover.Add(bc);

                    bc = new BasicCover();
                    bc.Id = 4;
                    bc.ComprePeriod = 0;
                    bc.TLOPeriod = 2;
                    bc.Description = "TLO 2 Years";
                    BasicCover.Add(bc);

                    bc = new BasicCover();
                    bc.Id = 5;
                    bc.ComprePeriod = 3;
                    bc.TLOPeriod = 0;
                    bc.Description = "Comprehensive 3 Years";
                    BasicCover.Add(bc);

                    bc = new BasicCover();
                    bc.Id = 6;
                    bc.ComprePeriod = 0;
                    bc.TLOPeriod = 3;
                    bc.Description = "TLO 3 Years";
                    BasicCover.Add(bc);

                    bc = new BasicCover();
                    bc.Id = 7;
                    bc.ComprePeriod = 1;
                    bc.TLOPeriod = 1;
                    bc.Description = "Comprehensive 1 Year, TLO 1 Year";
                    BasicCover.Add(bc);

                    bc = new BasicCover();
                    bc.Id = 8;
                    bc.ComprePeriod = 1;
                    bc.TLOPeriod = 2;
                    bc.Description = "Comprehensive 1 Year, TLO 2 Years";
                    BasicCover.Add(bc);


                    bc = new BasicCover();
                    bc.Id = 9;
                    bc.ComprePeriod = 2;
                    bc.TLOPeriod = 1;
                    bc.Description = "Comprehensive 2 Years, TLO 1 Year";
                    BasicCover.Add(bc);

                }

                return Json(new { status = true, message = "OK", BasicCover }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



        [HttpPost]
        public IHttpActionResult getServicePointList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string search = "%" + form.Get("search").Trim() + "%";

                string query = @"
                 SELECT * FROM GardaCenter WHERE GardaCenterName 
                 like @0 OR City like @0 AND RowStatus = 1 Order by GardaCenterName ASC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, search);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



        [HttpPost]
        public IHttpActionResult getDashboard(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string StateDashboard = form.Get("StateDashboard").Trim();
                string Role = form.Get("userRole").Trim(); //NATIONALMGR/BRANCHMGR
                string periodView = form.Get("periodView").Trim();// Monthly/Yearly
                string SalesOfficerID = form.Get("SalesOfficerID").Trim();
                string param = form.Get("param").Trim();
                string Channel = form.Get("Channel").Trim();
                string ChannelSource = form.Get("ChannelSource").Trim();

                var dbs = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                SalesOfficerID = SalesOfficerID.Contains("@") ? dbs.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", SalesOfficerID) : SalesOfficerID;

                string query = "";
                //GetAABHEAD
                if (StateDashboard.Equals("REGIONALMGR"))
                {
                    if (Role.Equals("REGIONALMGR"))
                    {
                        query = @"SELECT DISTINCT ah.Type, ar.RegionDescription as Description,ah.Param,
                                    ah.Value, ah.LastUpdatedTime, ah.RowStatus FROM AABHead ah
                                    JOIN AABRegion ar ON ah.Param = ar.RegionCode
                                    WHERE ah.Type = 'Region' AND  Value = @0 And ah.RowStatus=1 ";
                    }
                    else if (Role.Equals("NATIONALMGR") || Role.Equals("BRANCHSUPPORT"))
                    {
                        query = "SELECT DISTINCT ah.Type, ar.RegionDescription as Description, ah.Param," +
                            " ah.Value, ah.LastUpdatedTime, ah.RowStatus FROM AABHead ah" +
                            " JOIN AABRegion ar ON ah.Param = ar.RegionCode" +
                         " WHERE ah.Type = 'Region' And ah.RowStatus=1  ";

                        if (Channel.Equals("EXT"))
                        {
                            query += "AND ar.NationalCode = @1";
                        }
                        else
                        {
                            query += "AND ar.NationalCode = 'AAB'";
                        }
                    }
                }
                else if (StateDashboard.Equals("NATIONALMGR") || StateDashboard.Equals("BRANCHSUPPORT"))
                {
                    //            sqlQuery= "SELECT * FROM AABHead WHERE Type = 'Region'";
                    // 0261/URF/2015
                    query = "SELECT DISTINCT ah.* FROM AABHead ah JOIN AABRegion ar ON ah.Param = ar.RegionCode WHERE ah.Type = 'Region' AND ah.RowStatus=1 ";
                    if (Channel.Equals("EXT"))
                    {
                        query += "AND ar.NationalCode = @1";
                    }
                    else
                    {
                        query += "AND ar.NationalCode = 'AAB'";
                    }
                }
                else if (StateDashboard.Equals("BRANCHMGR") || StateDashboard.Equals("SALESSECHEAD"))
                {
                    query = @"SELECT DISTINCT ah.*,ar.Description FROM AABHead ah JOIN AABBranch ar ON ah.Param = ar.BranchCode WHERE ah.Type = 'Branch' AND ah.Value =@0 And ah.RowStatus=1 ";

                }
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<AABHead> aabHeads = new List<AABHead>();
                if (StateDashboard.Equals(Role) || StateDashboard.Equals("REGIONALMGR") || (StateDashboard.Equals("BRANCHMGR") && Role.Equals("SALESSECHEAD")))
                {
                    aabHeads = string.IsNullOrEmpty(query) ? new List<AABHead>() : db.Fetch<AABHead>(query, SalesOfficerID, ChannelSource);

                }//  param = !string.IsNullOrEmpty(param) ? param : aabHeads[0].Param;
                if (StateDashboard.Equals("NATIONALMGR"))
                {
                    query = "SELECT DISTINCT ar.RegionDescription as Region,SUM(TotalProspect) TotalProspect,SUM(TotalNeedFu) TotalNeedFu," +
                                                  " SUM(TotalPotential) TotalPotential,SUM (TotalCallLater) TotalCallLater," +
                                                  " SUM(TotalCollectDoc) TotalCollectDoc,SUM(TotalNotDeal) TotalNotDeal," +
                                                  " SUM(TotalSendToSA) TotalSendToSA, SUM(TotalPolicyCreated) TotalPolicyCreated," +
                                                  " SUM(TotalBackToAo) TotalBackToAo,SUM(TotalDeal) AS TotalDeal FROM FollowUpSummary fu" +
                                                  " JOIN AABRegion ar ON fu.Region = ar.RegionCode" +
                                                  " WHERE";
                    if (Channel.Equals("EXT"))
                    {
                        query += " ar.NationalCode = '" + ChannelSource + "'";
                    }
                    else
                    {
                        query += " ar.NationalCode = 'AAB'";
                    }


                    switch (periodView)
                    {
                        case "Monthly":
                            query += " AND Period= CONVERT(char,YEAR(GETDATE()))+'-'+CONVERT(char,MONTH(GETDATE()))+'-01 00:00:00.000' " +
                                        " AND Frequency = 'Monthly' GROUP BY ar.RegionDescription";
                            break;
                        case "Yearly":
                            query += " AND Period= CONVERT(char,YEAR(GETDATE()))+'-01-01 00:00:00.000'" +
                                        " AND Frequency = 'Yearly' GROUP BY ar.RegionDescription";
                            break;
                        default:
                            break;
                    }
                }
                else if (StateDashboard.Equals("REGIONALMGR"))
                {
                    query = "SELECT DISTINCT BranchCode,BranchDesc,SUM(TotalProspect) TotalProspect,SUM(TotalNeedFu) TotalNeedFu, SUM(TotalPotential) TotalPotential,SUM (TotalCallLater) TotalCallLater," +
                    "SUM(TotalCollectDoc) TotalCollectDoc,SUM(TotalNotDeal) TotalNotDeal,SUM(TotalSendToSA) TotalSendToSA, SUM(TotalPolicyCreated) TotalPolicyCreated,SUM(TotalBackToAo) TotalBackToAo,SUM(TotalDeal) AS TotalDeal " +
                    "FROM FollowUpSummary WHERE Region=@0";

                    switch (periodView)
                    {
                        case "Monthly":
                            query += " AND Period= CONVERT(char,YEAR(GETDATE()))+'-'+CONVERT(char,MONTH(GETDATE()))+'-01 00:00:00.000' " +
                                        " AND Frequency = 'Monthly' GROUP BY BranchCode,BranchDesc";
                            break;
                        case "Yearly":
                            query += " AND Period= CONVERT(char,YEAR(GETDATE()))+'-01-01 00:00:00.000'" +
                                        " AND Frequency = 'Yearly' GROUP BY BranchCode,BranchDesc";
                            break;
                        default:
                            query += " GROUP BY BranchCode,BranchDesc";
                            break;
                    }
                }
                else if (StateDashboard.Equals("BRANCHMGR") || StateDashboard.Equals("SALESSECHEAD"))
                {
                    query = "SELECT DISTINCT * FROM FollowUpSummary WHERE BranchCode =@0";
                    switch (periodView)
                    {
                        case "Monthly":
                            query += " AND Period= CONVERT(char,YEAR(GETDATE()))+'-'+CONVERT(char,MONTH(GETDATE()))+'-01 00:00:00.000' " +
                                        " AND Frequency = 'Monthly'";
                            break;
                        case "Yearly":
                            query += " AND Period= CONVERT(char,YEAR(GETDATE()))+'-01-01 00:00:00.000'" +
                                        " AND Frequency = 'Yearly'";
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    query = "SELECT DISTINCT * FROM FollowUpSummary WHERE SalesOfficerID ='" + SalesOfficerID + "' AND Period =CONVERT(char,YEAR(GETDATE()))+'-'+CONVERT(char,MONTH(GETDATE()))+'-01 00:00:00.000'";

                }


                List<dynamic> fuModel = new List<dynamic>();
                if (aabHeads.Count() > 0)
                {
                    foreach (AABHead aab in aabHeads)
                    {
                        var fuMod = db.Fetch<dynamic>(query, aab.Param);
                        fuModel.Add(fuMod);

                    }
                }
                else
                {
                    AABHead ah = new AABHead();
                    aabHeads.Add(ah);
                    List<dynamic> fuMod = db.Fetch<dynamic>(query, param);
                    if (fuMod.Count() == 0)
                    {
                        if (StateDashboard.Equals("SALESOFFICER"))
                        {
                            fuMod = db.Fetch<dynamic>(@"SELECT TOP 1 [SalesOfficerID]
      ,[SalesName]
      ,[BranchCode]
      ,[BranchDesc]
      ,[Region]
      ,[Period]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,0[TotalProspect]
      ,0[TotalNeedFu]
      ,0[TotalPotential]
      ,0[TotalCallLater]
      ,0[TotalCollectDoc]
      ,0[TotalNotDeal]
      ,0[TotalSendToSA]
      ,0[TotalPolicyCreated]
      ,0[TotalBackToAo]
      ,0[TotalDeal]
      ,[Frequency]
      ,[National]
  FROM [AABMobile].[dbo].[FollowUpSummary] WHERE SalesOfficerID=@0 ORDER BY PERIOD DESC", SalesOfficerID);
                        }
                    }
                    fuModel.Add(fuMod.Count() > 0 ? fuMod : null);
                }

                return Json(new { status = true, message = "OK", aabHeads, fuModel }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        private void InitializeCoverageItems(string OrderNo)
        {
            string query = "select a.InterestID, b.CoverageID, a.Year, b.Rate, b.Premium, ";
            query += "b.SumInsured, b.Loading, b.LoadingRate, b.IsBundling ";
            query += "From orderSimulationinterest a inner join orderSimulationcoverage b on " +
                    "a.orderno=b.orderno and a.interestno=b.interestno and b.RowStatus=1 ";
            query += "where a.RowStatus=1 AND a.orderno=@0";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                CalculatedPremiItems = db.Fetch<CalculatedPremi>(query, OrderNo);
            }

            InterestCoverageItems = CalculatedPremiItems;
            BasicPremi = ACCESSPremi = AccessSI = SRCCPremi = FLDPremi = ETVPremi = TSPremi =
                    TPLPremi = TPLSI = PADRVRPremi = PADRVRSI = PAPASSPremi = PAPASSSI = PASS = 0;

            if (CalculatedPremiItems.Count() > 0)
            {
                foreach (CalculatedPremi item in CalculatedPremiItems)
                {
                    if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO") &&
                            (item.CoverageID.ToUpper().Equals("ALLRIK") || item.CoverageID.ToUpper().TrimEnd().Equals("TLO")))
                    {
                        BasicPremi += item.Premium;
                    }
                    if (item.InterestID.ToUpper().Equals("ACCESS") &&
                            (item.CoverageID.ToUpper().Equals("ALLRIK") || item.CoverageID.ToUpper().TrimEnd().Equals("TLO")))
                    {
                        IsACCESSChecked = true;
                        IsACCESSSIEnabled = true;
                        ACCESSPremi += item.Premium;
                        if (item.Year == 1)
                            AccessSI = item.SumInsured;
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("SRCC") || item.CoverageID.ToUpper().Equals("SRCTLO")))
                    {
                        IsSRCCChecked = true;
                        IsTSEnabled = true;
                        SRCCPremi += item.Premium;
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.SRCC, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("FLD") || item.CoverageID.ToUpper().Equals("FLDTLO")))
                    {
                        IsFLDChecked = true;
                        IsTSEnabled = true;
                        FLDPremi += item.Premium;
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.FLD, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("ETV") || item.CoverageID.ToUpper().Equals("ETVTLO")))
                    {
                        IsETVChecked = true;
                        IsTSEnabled = true;
                        ETVPremi += item.Premium;
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.ETV, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("TRS") || item.CoverageID.ToUpper().Equals("TRRTLO")))
                    {
                        IsTSEnabled = true;
                        IsTSChecked = true;
                        TSPremi += item.Premium;
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.TS, item.Year, item.IsBundling));
                        }
                    }
                    //if(item.InterestID.equals("TPLPER") && item.CoverageID.equals("MVTPL1")) {
                    /** start 0040/URF/2017 */
                    if (item.InterestID.ToUpper().Equals("TPLPER") && item.CoverageID.ToUpper().Contains("MVTP"))
                    {
                        IsTPLChecked = true;
                        IsTPLEnabled = true;
                        IsTPLSIEnabled = true;
                        TPLPremi += item.Premium;
                        if (item.Year == 1)
                        {
                            TPLSI = item.SumInsured;
                        }
                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.TPLPER, item.Year, item.IsBundling));
                    } /** end 0040/URF/2017 */
                    if (item.InterestID.ToUpper().Equals("PADRVR"))
                    {
                        IsPADRVRChecked = true;
                        IsPADRVRSIEnabled = true;
                        PADRVRPremi += item.Premium;
                        if (item.Year == 1)
                        {
                            PADRVRSI = item.SumInsured;
                        }

                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.PADRVR, item.Year, item.IsBundling));
                    }
                    if (item.InterestID.ToUpper().Equals("PAPASS"))
                    {
                        IsPAPASSChecked = true;
                        IsPAPASSSIEnabled = true;
                        IsPASSEnabled = true;

                        PAPASSPremi += item.Premium;
                        if (item.Year == 1)
                        {
                            PAPASSSI = item.SumInsured;
                        }
                        PASS = Convert.ToInt32(item.CoverageID.Replace("C", "").TrimEnd());
                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.PAPASS, item.Year, item.IsBundling));
                    }
                }
            }
        }

        [HttpPost]
        public IHttpActionResult rateCalculationNonBasic(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            string Alert = "";
            try
            {
                string chItem = form.Get("chItem");
                ProductCode = form.Get("ProductCode");
                SumInsured = Convert.ToDouble(form.Get("SumInsured"));
                CoverageTypeNonBasic cType = chItem.Equals("SRCC") ? CoverageTypeNonBasic.SRCC : (chItem.Equals("FLD") ? CoverageTypeNonBasic.FLD : (chItem.Equals("ETV")) ? CoverageTypeNonBasic.ETV : (chItem.Equals("TS")) ? CoverageTypeNonBasic.TS : (chItem.Equals("TPL")) ? CoverageTypeNonBasic.TPLPER : (chItem.Equals("PADRVR")) ? CoverageTypeNonBasic.PADRVR : CoverageTypeNonBasic.PAPASS);
                IsTSChecked = chItem.Equals("TS") ? true : false;
                IsTSEnabled = chItem.Equals("TS") ? true : false;
                IsPADRVRChecked = chItem.Equals("PADRVR") ? true : false;
                IsPADRVRSIEnabled = chItem.Equals("PADRVR") ? true : false;
                IsPAPASSChecked = chItem.Equals("PAPASS") ? true : false;
                IsPAPASSSIEnabled = chItem.Equals("PAPASS") ? true : false;
                IsTPLChecked = chItem.Equals("TPL") ? true : false;
                IsTPLSIEnabled = chItem.Equals("TPL") ? true : false;
                IsSRCCChecked = chItem.Equals("SRCC") ? true : false;
                IsETVChecked = chItem.Equals("ETV") ? true : false;
                IsFLDChecked = chItem.Equals("FLD") ? true : false;
                bool ch = false; // checked item that thrown
                bool chPAPASS = (chItem.Equals("PAPASS") ? true : false);
                bool chPADRVR = (chItem.Equals("PADRVR") ? true : false);
                bool chTPL = (chItem.Equals("TPL") ? true : false);


                UsageCode = form.Get("UsageCode");
                type = form.Get("type");
                CityCode = form.Get("CityCode");

                TPLCoverageID = form.Get("TPLCoverageID");
                PADRVRSI = Convert.ToDouble(form.Get("PADRVRSI").Equals("null") ? "0" : form.Get("PADRVRSI"));
                PAPASSSI = Convert.ToDouble(form.Get("PAPASSSI").Equals("null") ? "0" : form.Get("PAPASSSI"));
                PASS = Convert.ToInt32(form.Get("PASS").Equals("null") ? "0" : form.Get("PASS"));
                TPLSI = Convert.ToDouble(form.Get("TPLSI"));
                AccessSI = Convert.ToDouble(form.Get("AccessSI").Equals("null") ? "0" : form.Get("AccessSI"));


                bool isExtentedBundling = (form.Get("isExtentedBundling").Equals("true") ? true : false);
                bool chOne = (form.Get("chOne").Equals("1") ? true : false);
                bool chTwo = (form.Get("chTwo").Equals("1") ? true : false);
                bool chThree = (form.Get("chThree").Equals("1") ? true : false);



                bool ischOneEnabled = (form.Get("ischOneEnabled").Equals("1") ? true : false);
                bool ischTwoEnabled = (form.Get("ischTwoEnabled").Equals("1") ? true : false);
                bool ischThreeEnabled = (form.Get("ischThreeEnabled").Equals("1") ? true : false);
                string coveragePeriodNonBasic = form.Get("coveragePeriodNonBasicItems");
                string coveragePeriod = form.Get("coveragePeriodItems");
                string calculatedPremi = form.Get("CalculatedPremiItems");
                coveragePeriodNonBasicItems = JsonConvert.DeserializeObject<List<CoveragePeriodNonBasic>>(coveragePeriodNonBasic);
                coveragePeriodItems = JsonConvert.DeserializeObject<List<CoveragePeriod>>(coveragePeriod);
                CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculatedPremi);

                getAdminFee();
                List<CoveragePeriodNonBasic> removeperiodnonbasic = new List<CoveragePeriodNonBasic>();
                foreach (CoveragePeriodNonBasic cpnb in coveragePeriodNonBasicItems)
                {
                    if (cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC || cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD || cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                    {
                        IsSRCCChecked = true;
                        IsFLDChecked = true;
                        IsETVChecked = true;
                    }
                    if (cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER)
                    {
                        bool tplexist = false;
                        foreach (CalculatedPremi cp in CalculatedPremiItems)
                        {
                            if (cp.InterestID.Equals("TPLPER") && cp.Year == cpnb.Year)
                            {
                                tplexist = true;
                            }
                        }
                        if (!tplexist)
                        {
                            removeperiodnonbasic.Add(cpnb);
                        }
                    }
                }
                //Removeperiodnonbasic
                foreach (CoveragePeriodNonBasic item in removeperiodnonbasic)
                {
                    coveragePeriodNonBasicItems.Remove(item);
                }
                bool isValid = false;
                if (chItem.Equals("TS"))
                {
                    bool isBundlingOne = false;
                    bool isBundlingTwo = false;
                    bool isBundlingThree = false;

                    bool isCheckedOne = false;
                    bool isCheckedTwo = false;
                    bool isCheckedThree = false;
                    List<CoveragePeriodNonBasic> cbasic = new List<CoveragePeriodNonBasic>();
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC))
                        {
                            cbasic.Add(item);
                        }
                    }
                    foreach (CoveragePeriodNonBasic bundlingItem in cbasic)
                    {
                        if (bundlingItem.Year == 1)
                        {
                            isCheckedOne = true;
                            if (bundlingItem.IsBundling)
                            {
                                isBundlingOne = true;
                            }
                        }
                        else if (bundlingItem.Year == 2)
                        {
                            isCheckedTwo = true;
                            if (bundlingItem.IsBundling)
                            {
                                isBundlingTwo = true;
                            }
                        }
                        else if (bundlingItem.Year == 3)
                        {
                            isCheckedThree = true;
                            if (bundlingItem.IsBundling)
                            {
                                isBundlingThree = true;
                            }
                        }
                    }
                    if ((isBundlingOne && isBundlingTwo && isBundlingThree &&
                     (!chOne && (chTwo || chThree))) ||
                     (isBundlingTwo && isBundlingThree && !chTwo && chThree) ||
                     (!chOne && ((chTwo && ischTwoEnabled && !isBundlingTwo) ||
                     (chThree && ischThreeEnabled && !isBundlingThree))) ||
                     !chTwo && (chThree && ischThreeEnabled && !isBundlingThree))
                    {
                        return Json(new { status = true, message = "OK", Alert = "Extended warranties for the previous year must be selected." }, Util.jsonSerializerSetting());
                    }
                    else if ((chOne && !isCheckedOne) || (chTwo && !isCheckedTwo) ||
                            (chThree && !isCheckedThree))
                    {
                        return Json(new
                        {
                            status = true,
                            message = "OK",
                            Alert = "Extended warranties TS can only be selected in the same " +
                                "period with Strike, Riot, Commotion (SRCC), Flood & Windstorm, " +
                                "dan Earthquake, Tsunami, Volcanic Eruption."
                        }, Util.jsonSerializerSetting());

                    }
                    else
                    {
                        isValid = true;
                    }
                    IsTSChecked = (chOne == false && chTwo == false && chThree == false) ? false : IsTSChecked;
                }
                else if (chItem.Equals("TPL"))
                {
                    //validasi TPL
                    bool isFail = false;
                    foreach (CoveragePeriod item in coveragePeriodItems)
                    {
                        if (chOne && item.Year == 1 && item.CoverageType == CoverageType.TLO)
                        {
                            isFail = true;
                            break;
                        }
                        else if (chTwo && item.Year == 2 && item.CoverageType == CoverageType.TLO)
                        {
                            isFail = true;
                            break;
                        }
                        else if (chThree && item.Year == 3 && item.CoverageType == CoverageType.TLO)
                        {
                            isFail = true;
                            break;
                        }
                    }
                    if (isFail)
                    {
                        return Json(new
                        {
                            status = true,
                            message = "OK",
                            Alert = "Extended warranties TPL can only be selected in the same " +
                                    "period with the Comprehensive"
                        }, Util.jsonSerializerSetting());

                    }
                    else if ((!chOne && ((chTwo && ischTwoEnabled) ||
                          (chThree && ischThreeEnabled))) ||
                          !chTwo && (chThree && ischThreeEnabled))
                    {
                        return Json(new { status = true, message = "OK", Alert = "Extended warranties for the previous year must be selected." }, Util.jsonSerializerSetting());

                    }
                    else
                    {
                        isValid = true;
                    }

                    IsTPLChecked = (chOne == false && chTwo == false && chThree == false) ? false : IsTPLChecked;
                }
                else if ((!chOne && ((chTwo && ischTwoEnabled) ||
                      (chThree && ischThreeEnabled))) ||
                      !chTwo && (chThree && ischThreeEnabled))
                {
                    return Json(new { status = true, message = "OK", Alert = "Extended warranties for the previous year must be selected." }, Util.jsonSerializerSetting());

                }
                else
                {
                    isValid = true;
                }
                if (isValid)
                {
                    List<int> checkedItems = new List<int>();
                    List<int> uncheckedItems = new List<int>();

                    bool isExist = false;
                    if (chOne && ischOneEnabled)
                    {
                        //check if already added to CoveragePeriodNonBasicItems
                        foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                        {
                            if (item.CoverageTypeNonBasic.Equals(cType) && item.Year == 1)
                            {
                                isExist = true;
                            }
                        }
                        if (!isExist)
                        {
                            if (isExtentedBundling)
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.SRCC, 1, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.FLD, 1, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.ETV, 1, false));
                            }
                            else
                            {
                                coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(cType, 1, false));
                                if (chItem.Equals("PAPASS"))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR))
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (!exists)
                                    {
                                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                                CoverageTypeNonBasic.PADRVR, 1, false));
                                        checkedItems.Add(1);
                                    }
                                }
                            }
                        }
                    }

                    isExist = false;
                    if (chTwo && ischTwoEnabled)
                    {
                        foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                        {
                            if (item.CoverageTypeNonBasic.Equals(cType) && item.Year == 2)
                            {

                                isExist = true;
                            }
                        }

                        if (!isExist)
                        {
                            if (isExtentedBundling)
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.SRCC, 2, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.FLD, 2, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.ETV, 2, false));
                            }
                            else
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(cType, 2, false));
                                if (chItem.Equals("PAPASS"))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR) && item.Year == 2)
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (!exists)
                                    {
                                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                                CoverageTypeNonBasic.PADRVR, 2, false));
                                        checkedItems.Add(2);
                                    }

                                }
                            }
                        }
                    }
                    isExist = false;
                    if (chThree && ischThreeEnabled)
                    {
                        foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                        {
                            if (item.CoverageTypeNonBasic.Equals(cType) && item.Year == 3)
                            {
                                isExist = true;
                            }
                        }
                        if (!isExist)
                        {
                            if (isExtentedBundling)
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.SRCC, 3, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.FLD, 3, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.ETV, 3, false));
                            }
                            else
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(cType, 3, false));
                                if (chItem.Equals("PAPASS"))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR) && item.Year == 3)
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (!exists)
                                    {
                                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                                CoverageTypeNonBasic.PADRVR, 3, false));
                                        checkedItems.Add(3);
                                    }

                                }
                            }

                        }
                    }

                    if (chOne || chTwo || chThree)
                    {
                        if (isExtentedBundling)
                        {
                            IsSRCCChecked = true;
                            IsFLDChecked = true;
                            IsETVChecked = true;
                            IsTSEnabled = true;
                        }
                        else
                        {
                            ch = true;
                            if (chItem.Equals("TPL"))
                            {

                                IsTPLEnabled = true;
                            }
                            else if (chItem.Equals("PADRVR"))
                            {
                                IsPADRVRSIEnabled = true;
                            }
                            else if (chItem.Equals("PAPASS"))
                            {
                                IsPAPASSSIEnabled = true;
                                IsPASSEnabled = true;
                                IsPAPASSEnabled = true;
                                IsPADRVRChecked = true;
                                IsPADRVRSIEnabled = true;
                            }
                        }
                    }
                    else
                    {
                        if (isExtentedBundling)
                        {
                            IsSRCCChecked = false;
                            IsFLDChecked = false;
                            IsETVChecked = false;
                            IsTSChecked = false;
                            IsTSEnabled = false;
                        }
                        else
                        {
                            ch = false;
                            if (chItem.Equals("TPL"))
                            {
                                IsTPLSIEnabled = false;
                                IsTPLChecked = false;
                                IsTPLEnabled = false;
                                IsTPLChecked = false;
                                TPLSI = 0;
                                TPLPremi = 0;
                            }
                            else if (chItem.Equals("PADRVR"))
                            {
                                IsPADRVRChecked = false;
                                IsPAPASSChecked = false;
                                IsPAPASSSIEnabled = false;
                                IsPASSEnabled = false;
                                IsPAPASSSIEnabled = false;
                                IsPADRVRSIEnabled = false;
                                PADRVRSI = 0;
                                PAPASSSI = 0;
                                PASS = 0;
                            }
                            else if (chItem.Equals("PAPASS"))
                            {
                                IsPAPASSSIEnabled = false;
                                IsPAPASSChecked = false;
                                IsPASSEnabled = false;
                                PASS = 0;
                                PAPASSSI = 0;
                                foreach (CoveragePeriodNonBasic cpnb in coveragePeriodNonBasicItems)
                                {
                                    if (cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR)
                                    {
                                        IsPADRVRChecked = true;
                                        IsPADRVRSIEnabled = true;
                                    }
                                }
                            }
                        }
                    }
                    for (int i = coveragePeriodNonBasicItems.Count(); i > 0; i--)
                    {
                        CoveragePeriodNonBasic cp = coveragePeriodNonBasicItems[i - 1];
                        if (isExtentedBundling)
                        {
                            if (((cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.FLD)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.ETV))
                                    && cp.Year == 1 && !chOne)
                                    || ((cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.FLD)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.ETV))
                                    && cp.Year == 2 && !chTwo)
                                    || ((cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.FLD)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.ETV))
                                    && cp.Year == 3 && !chThree))
                            {
                                coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                bool exists = false;
                                foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                {
                                    if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.TS) && item.Year == cp.Year)
                                    {
                                        exists = true;
                                    }
                                }
                                if (exists)
                                {
                                    if (!uncheckedItems.Contains(cp.Year))
                                    {
                                        uncheckedItems.Add(cp.Year);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((cp.CoverageTypeNonBasic.Equals(cType) && cp.Year == 1 && !chOne)
                                    || (cp.CoverageTypeNonBasic.Equals(cType) && cp.Year == 2 && !chTwo)
                                    || (cp.CoverageTypeNonBasic.Equals(cType) && cp.Year == 3 && !chThree))
                            {
                                coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                if (cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PAPASS) && item.Year == cp.Year)
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (exists)
                                    {
                                        uncheckedItems.Add(cp.Year);
                                    }
                                }
                            }
                        }
                    }

                    if (chItem.Equals("SRCC") || chItem.Equals("chFLD") || chItem.Equals("ETV"))
                    {
                        RefreshBundlingPremi();
                    }
                    else if (chItem.Equals("TS"))
                    {
                        RefreshTS();
                    }
                    else if (chItem.Equals("TPL"))
                    {
                        RefreshTPL(chTPL);
                    }
                    else if (chItem.Equals("PADRVR"))
                    {
                        RefreshPADRVR(chPADRVR);
                    }
                    else if (chItem.Equals("PAPASS"))
                    {
                        RefreshPAPASS(chPAPASS);
                    }
                    checkedItems.Sort();
                    uncheckedItems.Sort();

                    string message = "";
                    if (checkedItems.Count() > 0)
                    {
                        message = "Extended warranties PA Driver automatically selected for the year "; // 001
                        //auto checked
                        foreach (int i in checkedItems)
                        {
                            message += i + ",";
                        }
                        message = message.Substring(0, message.Length - 1);
                        message += ".";
                    }

                    //auto unchecked
                    if (uncheckedItems.Count() > 0)
                    {
                        if (chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV"))
                        {
                            message = "Extended warranties TS are not automatically selected for " +
                                    "the year"; // 001
                        }
                        else if (chItem.Equals("PADRVR"))
                        {
                            message = "Passenger PA Extended warranties are not automatically " +
                                    "selected for the year "; // 001
                        }

                        foreach (int i in uncheckedItems)
                        {
                            message += i + ",";
                        }

                        //delete dependency control base on each depend
                        int isChecked = 0;
                        for (int i = coveragePeriodNonBasicItems.Count(); i > 0; i--)
                        {
                            CoveragePeriodNonBasic cp = coveragePeriodNonBasicItems[i - 1];
                            int counter = 0;
                            foreach (int j in uncheckedItems)
                            {
                                if (chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV") &&
                                   cp.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                                {
                                    if (cp.Year == j)
                                    {
                                        coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                        counter++;
                                    }
                                }
                                else if (chItem.Equals("PADRVR") &&
                                      cp.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS)
                                {
                                    if (cp.Year == j)
                                    {
                                        coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                        counter++;
                                    }
                                }
                            }
                            if (counter == 0)
                            {
                                isChecked++;
                            }
                        }

                        if ((chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV")) && isChecked == 0)
                        {
                            IsTSChecked = false;
                        }
                        else if (chItem.Equals("PADRVR") && isChecked == 0)
                        {

                            IsPAPASSChecked = false;
                            IsPASSEnabled = false;
                            IsPAPASSSIEnabled = false;
                            PASS = 0;
                            PAPASSSI = 0;
                        }
                        message = message.Substring(0, message.Length - 1);
                        message += ".";
                    }
                    if (!message.Equals(""))
                    {
                        if ((chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV")))
                        {
                            RefreshTS();
                        }
                        else if (chItem.Equals("PADRVR"))
                        {
                            RefreshPAPASS(chPAPASS);
                        }
                        else if (chItem.Equals("PAPASS"))
                        {
                            RefreshPADRVR(chPADRVR);
                        }

                        String finalMessage = message;
                        String[] messageSplit = message.Split(',');
                        if (messageSplit.Length == 2)
                        {
                            finalMessage = messageSplit[0] + " dan " + messageSplit[1];
                        }
                        else if (messageSplit.Length == 3)
                        {
                            finalMessage = messageSplit[0] + ", " + messageSplit[1] + ", dan " +
                                    messageSplit[2];
                        }

                        Alert = finalMessage;
                    }




                }
                //recalculate bundling cover and accessory

                foreach (CalculatedPremi cp in CalculatedPremiItems)
                {
                    if (cp.CoverageID.TrimEnd().Equals("SRCC") || cp.CoverageID.TrimEnd().Equals("SRCTLO"))
                    {
                        SRCCPremi += cp.Premium;
                    }
                    if (cp.CoverageID.TrimEnd().Equals("FLD") || cp.CoverageID.TrimEnd().Equals("FLDTLO"))
                    {
                        FLDPremi += cp.Premium;
                    }
                    if (cp.CoverageID.TrimEnd().Equals("ETV") || cp.CoverageID.TrimEnd().Equals("ETVTLO"))
                    {
                        ETVPremi += cp.Premium;
                    }
                    if (cp.CoverageID.TrimEnd().Equals("ALLRIK") && cp.InterestID.TrimEnd().Equals("ACCESS"))
                    {
                        ACCESSPremi += cp.Premium;
                    }


                    //Load previous non basic Items
                    RefreshPremi();
                    CalculateLoading();
                    CalculateTotalPremi();
                }

                return Json(new
                {
                    status = true,
                    message = "OK",
                    CalculatedPremiItems,
                    coveragePeriodItems,
                    coveragePeriodNonBasicItems,
                    IsTPLEnabled,
                    IsTPLChecked,
                    IsTPLSIEnabled,
                    IsSRCCChecked,
                    IsFLDChecked,
                    IsETVChecked,
                    IsTSChecked,
                    IsTSEnabled,
                    IsPADRVRChecked,
                    IsPADRVRSIEnabled,
                    IsPASSEnabled,
                    IsPAPASSSIEnabled,
                    IsPAPASSEnabled,
                    IsPAPASSChecked,
                    SRCCPremi,
                    FLDPremi,
                    ETVPremi,
                    TSPremi,
                    PADRVRPremi,
                    PAPASSPremi,
                    TPLPremi,
                    ACCESSPremi,
                    //   AdminFee,
                    TotalPremi,
                    Alert
                }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



        private void getAdminFee()
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string query = @"SELECT * FROM PRD_POLICY_FEE WHERE Product_Code=@0 AND Order_Type=1 AND Lower_Limit<= @1 ORDER BY Lower_Limit DESC";
                    AdminFee = Convert.ToDouble(db.FirstOrDefault<PRD_POLICY_FEE>(query, ProductCode, SumInsured).Policy_Fee1);
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        private void resetCoverage()
        {

        }

        [HttpPost]
        public IHttpActionResult rateCalculation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            string Alert = "";
            try
            {

                string OrderNo = form.Get("OrderNo");
                ProductCode = form.Get("ProductCode");
                string productTypeCode = form.Get("productTypeCode");
                int comprePeriod = Convert.ToInt32(form.Get("comprePeriod"));
                type = form.Get("type");
                string BrandCode = form.Get("BrandCode");
                string VehicleCode = form.Get("VehicleCode");
                CityCode = form.Get("CityCode");
                string ModelCode = form.Get("ModelCode");
                string Series = form.Get("Series");
                UsageCode = form.Get("UsageCode");
                Year = Convert.ToInt32(form.Get("Year"));
                int TLOPeriod = Convert.ToInt32(form.Get("TLOPeriod"));
                int IsNew = Convert.ToInt32(form.Get("IsNew"));
                PADRVRSI = Convert.ToInt32(form.Get("PADRVRSI"));
                PAPASSSI = Convert.ToInt32(form.Get("PAPASSSI"));
                PASS = Convert.ToInt32(form.Get("PASS"));
                AccessSI = Convert.ToInt32(form.Get("AccessSI"));
                SumInsured = Convert.ToInt64(form.Get("SumInsured"));


                //string Channel = form.Get("Channel");
                //string ChannelSource = form.Get("ChannelSource");
                //if (Channel.Equals("EXT"))
                //{
                //    ProductCode = getProductExternal(ChannelSource, Convert.ToBoolean(IsNew));
                //}
                if (string.IsNullOrEmpty(productTypeCode) || string.IsNullOrEmpty(ProductCode) || (comprePeriod + TLOPeriod == 0) || SumInsured == 0)
                {
                    return Json(new { status = false, message = "Param Is Not Valid Or Not Completed" });

                }
                string CalculatedPremi = form.Get("CalculatedPremi");
                TPLCoverageID = form.Get("TPLCoverageID");
                IsSRCCChecked = string.IsNullOrEmpty(form.Get("IsSRCCChecked")) ? false : form.Get("IsSRCCChecked").Equals("1") ? true : false;
                IsFLDChecked = string.IsNullOrEmpty(form.Get("IsFLDChecked")) ? false : form.Get("IsFLDChecked").Equals("1") ? true : false;
                IsETVChecked = string.IsNullOrEmpty(form.Get("IsETVChecked")) ? false : form.Get("IsETVChecked").Equals("1") ? true : false;
                IsTSChecked = string.IsNullOrEmpty(form.Get("IsTSChecked")) ? false : form.Get("IsTSChecked").Equals("1") ? true : false;
                CalculatedPremi = form.Get("CalculatedPremi");
                string coveragePeriodNonBasicItem = form.Get("coveragePeriodNonBasicItems");
                string coveragePeriodItem = form.Get("coveragePeriodItems");
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                CityCode = form.Get("CityCode");
                type = form.Get("type");
                UsageCode = form.Get("UsageCode");
                if (!string.IsNullOrEmpty(CalculatedPremi))
                {
                    CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(CalculatedPremi);
                }
                if (!string.IsNullOrEmpty(coveragePeriodNonBasicItem))
                {
                    coveragePeriodNonBasicItems = JsonConvert.DeserializeObject<List<CoveragePeriodNonBasic>>(coveragePeriodNonBasicItem);
                }
                if (!string.IsNullOrEmpty(coveragePeriodItem))
                {
                    coveragePeriodItems = JsonConvert.DeserializeObject<List<CoveragePeriod>>(coveragePeriodItem);
                }
                string query = "";
                if (PADRVRSI != 0)
                {
                    bool chPADRVR = true;
                    getAdminFee();
                    RefreshPADRVR(chPADRVR);
                    RecallingCoverageNonBasic();
                    Alert = calculateAgainPremi();
                }
                else if (PAPASSSI != 0)
                {
                    bool chPAPASS = true;
                    getAdminFee();
                    RefreshPAPASS(chPAPASS);
                    RecallingCoverageNonBasic();
                    Alert = calculateAgainPremi();
                }
                else if (AccessSI != 0)
                {

                    getAdminFee();
                    RefreshAccessSI();
                    //CalculatedPremiItems.AddRange(cpremi);
                    RecallingCoverageNonBasic();

                    // Alert = calculateAgainPremi();
                }
                else if (!string.IsNullOrEmpty(TPLCoverageID))
                {
                    TPLSI = db.ExecuteScalar<double>("SELECT TOP 1 BC_Up_To_SI FROM Mapping_Cover_Progressive WHERE Coverage_ID=@0 ", TPLCoverageID);

                    getAdminFee();
                    RefreshTPL(true);

                    RecallingCoverageNonBasic();
                    Alert = calculateAgainPremi();
                }
                else
                {
                    #region reset coverage


                    #region Initialize Vehicle Price Tolerance
                    query = @"SELECT Price FROM Vehicle WHERE VehicleCode=@0 AND BrandCode=@1 AND ModelCode=@2 AND Series=@3 AND Type=@4 AND Year=@5";
                    VehiclePrice = Convert.ToDouble(db.FirstOrDefault<Vehicle>(query, VehicleCode, BrandCode, ModelCode, Series, type, Year).Price);
                    query = @"SELECT * FROM VehiclePriceTolerance WHERE IsNew=@0 AND IsATPM=1 AND FormType=1 AND VehicleCategory=1 AND MaxSI>= @1 ORDER BY MaxSI ASC";
                    vptModel = db.FirstOrDefault<VehiclePriceTolerance>(query, IsNew, VehiclePrice);
                    #endregion

                    #region Initialize CoveragePeriode

                    int year = 0;
                    if (comprePeriod > 0)
                    {
                        for (int i = 0; i < comprePeriod; i++)
                        {
                            year++;
                            CoveragePeriod cp = new CoveragePeriod();
                            cp.CoverageType = CoverageType.Comprehensive;
                            cp.Year = year;
                            coveragePeriodItems.Add(cp);
                        }
                    }
                    if (TLOPeriod > 0)
                    {
                        for (int i = 0; i < TLOPeriod; i++)
                        {
                            year++;
                            CoveragePeriod cp = new CoveragePeriod();
                            cp.CoverageType = CoverageType.TLO;
                            cp.Year = year;
                            coveragePeriodItems.Add(cp);
                        }
                    }
                    #endregion

                    #region Initialize Product Setting
                    if (string.IsNullOrEmpty(productTypeCode) &&
                   string.IsNullOrEmpty(ProductCode) &&
                   (comprePeriod + TLOPeriod == 0) && SumInsured < 0 &&
                   string.IsNullOrEmpty(CityCode) &&
                   string.IsNullOrEmpty(UsageCode))
                    {
                        return Json(new { status = false, message = "Data Not Completed" });

                    }

                    if (coveragePeriodNonBasicItems != null && coveragePeriodNonBasicItems.Count() > 0)
                    {
                        RecallingCoverageNonBasic();

                    }
                    else
                    {
                        query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0";
                        prdAgreedValueMaster = db.Fetch<Prd_Agreed_Value>(query, ProductCode);

                        getAdminFee();

                        query = @"SELECT * FROM Prd_Interest_Coverage WHERE Product_Code=@0";
                        prdInterestCoverage = db.Fetch<Prd_Interest_Coverage>(query, ProductCode);
                        foreach (Prd_Interest_Coverage prd in prdInterestCoverage)
                        {
                            if (prd.Coverage_Id.Trim().Equals("SRCC") ||
                                    prd.Coverage_Id.Trim().Equals("SRCTLO"))
                            {
                                IsSRCCEnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Equals("FLD") ||
                                  prd.Coverage_Id.Trim().Equals("FLDTLO"))
                            {
                                IsFLDEnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Equals("ETV") ||
                                  prd.Coverage_Id.Trim().Equals("ETVTLO"))
                            {
                                IsETVEnabled = true;
                            }
                            if (prd.Coverage_Id.Trim().Equals("TRS") ||
                                 prd.Coverage_Id.Trim().Equals("TRRTLO"))
                            {
                                IsTSEnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Contains("MVTP") &&
                                  comprePeriod > 0)
                            {
                                IsTPLEnabled = true;
                            }
                            else if (prd.Interest_ID.Trim().Equals("PADRVR"))
                            {
                                IsPADRVREnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Equals("C1"))
                            {
                                IsPAPASSEnabled = true;
                            }
                        }
                        IsACCESSEnabled = true;

                        if (coveragePeriodNonBasicItems.Count() > 0)
                        {

                        }
                        else
                        {
                            query = "SELECT a.Coverage_Id, a.Scoring_Code, b.Auto_Apply, 1 Year  FROM " +
                        "Prd_Interest_Coverage a INNER JOIN B2B_Interest_Coverage b ON " +
                        "a.Coverage_Id=b.Coverage_Id AND a.Interest_ID=b.Interest_Id AND " +
                        "a.Product_Code=b.Product_Code WHERE a.Product_Code=@0 AND b.Vehicle_Type='ALL   ' AND b.Interest_Id='CASCO ' AND " +
                                    "Auto_Apply IN (@@autoapply)";
                            string autoapply = "";
                            if (comprePeriod != 0 &&
                                TLOPeriod != 0)
                            {
                                autoapply = "1, 2";
                            }
                            else if (comprePeriod != 0)
                            {
                                autoapply = "1";
                            }
                            else if (TLOPeriod != 0)
                            {
                                autoapply = "2";
                            }
                            query = query.Replace("@@autoapply", autoapply);
                            List<CoverageBundling> coverageBundlingCheckItems = db.Fetch<CoverageBundling>(query.Replace("@1", autoapply), ProductCode);
                            List<CoverageBundling> coverageBundlingItems = new List<CoverageBundling>();


                            foreach (CoveragePeriod item in coveragePeriodItems)
                            {
                                foreach (CoverageBundling item2 in coverageBundlingCheckItems)
                                {
                                    if ((item.CoverageType == CoverageType.Comprehensive &&
                                            item2.Auto_Apply == 1) ||
                                            (item.CoverageType == CoverageType.TLO && item2.Auto_Apply == 2))
                                    {
                                        bool exist = false;
                                        foreach (CoverageBundling check in coverageBundlingItems)
                                        {
                                            if (check.Year.Equals(item.Year))
                                            {
                                                exist = true;
                                            }
                                        }
                                        if (!exist)
                                        {
                                            CoverageBundling cb = new CoverageBundling();
                                            cb.Coverage_Id = item2.Coverage_Id;
                                            cb.Scoring_Code = item2.Scoring_Code;
                                            cb.Auto_Apply = item2.Auto_Apply;
                                            cb.Year = item.Year;
                                            coverageBundlingItems.Add(cb);
                                        }
                                        break;
                                    }
                                }
                            }

                            foreach (CoverageBundling item in coverageBundlingItems)
                            {
                                if (!string.IsNullOrEmpty(item.Scoring_Code.TrimEnd()))
                                {
                                    query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, item.Year * 12.0);
                                    // HOTFIX 20151120 - BSY
                                    query = "SELECT * FROM Dtl_Rate_Factor WHERE Insurance_type='000401' AND " +
                                            "Score_Code='" + item.Scoring_Code + "' AND Coverage_id='" +
                                            item.Coverage_Id + "'";
                                    List<Dtl_Rate_Factor> rateFactor;

                                    try
                                    {
                                        rateFactor = db.Fetch<Dtl_Rate_Factor>(query);
                                    }
                                    catch (Exception ex)
                                    {
                                        rateFactor = new List<Dtl_Rate_Factor>();
                                    }

                                    if (rateFactor.Count() > 0)
                                    {
                                        //get scoringDtl_Rate_Scorin
                                        Dtl_Rate_Scoring rateScoring = new Dtl_Rate_Scoring();
                                        try
                                        {
                                            rateScoring = GetRateScoring(rateFactor,
                                               item.Scoring_Code, item.Coverage_Id,
                                               Convert.ToDouble(agreedValue.Rate) / 100 * SumInsured, true, CityCode, type, UsageCode);

                                        }
                                        catch (Exception ex)
                                        {
                                            //handle mapping exiasting
                                            rateScoring = null;
                                            IsSRCCEnabled = false;
                                            IsETVEnabled = false;
                                            IsFLDEnabled = false;
                                            IsTPLEnabled = false;
                                            IsPADRVREnabled = false;
                                            IsPASSEnabled = false;
                                            IsPAPASSEnabled = false;
                                            IsTSEnabled = false;
                                        }

                                        if (rateScoring != null)
                                        {
                                            if (rateScoring.Rate > 0)
                                            {
                                                IncludeBundlingCoverage(item.Year, true);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    IncludeBundlingCoverage(item.Year, true);

                                }
                            }
                        }
                    }

                    #endregion

                    #region Refresh Bundling Premi
                    RefreshBundlingPremi();
                    #endregion


                    //if (!string.IsNullOrEmpty(OrderNo))
                    //{
                    InitializeCoverageItems(OrderNo);
                    if (CalculatedPremiItems.Count() == 0)
                    {

                        #region Initialize Premi
                        InitializePremi();
                        #endregion
                    }
                    //}
                    //else
                    //{

                    //    #region Initialize Premi
                    //    InitializePremi();
                    //    #endregion
                    //}

                    CalculateLoading();
                    CalculateTotalPremi();
                    Alert = CheckSumInsured();

                    #endregion
                }
                return Json(new
                {
                    status = true,
                    message = "OK",
                    ProductCode,
                    CalculatedPremiItems,
                    coveragePeriodItems,
                    coveragePeriodNonBasicItems,
                    IsTPLEnabled,
                    IsTPLChecked,
                    IsTPLSIEnabled,
                    IsSRCCChecked,
                    IsSRCCEnabled,
                    IsFLDChecked,
                    IsFLDEnabled,
                    IsETVChecked,
                    IsETVEnabled,
                    IsTSChecked,
                    IsTSEnabled,
                    IsPADRVRChecked,
                    IsPADRVREnabled,
                    IsPADRVRSIEnabled,
                    IsPASSEnabled,
                    IsPAPASSSIEnabled,
                    IsPAPASSEnabled,
                    IsPAPASSChecked,
                    IsACCESSChecked,
                    IsACCESSEnabled,
                    IsACCESSSIEnabled,
                    SRCCPremi,
                    FLDPremi,
                    ETVPremi,
                    TSPremi,
                    PADRVRPremi,
                    PAPASSPremi,
                    TPLPremi,
                    ACCESSPremi,
                    AdminFee,
                    TotalPremi,
                    Alert
                }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        private void RecallingCoverageNonBasic()
        {
            foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
            {
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC)
                {
                    IsSRCCChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD)
                {
                    IsFLDChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                {
                    IsETVChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                {
                    IsTSChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER)
                {
                    IsTPLChecked = true;
                    IsTPLSIEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR)
                {
                    IsPADRVRChecked = true;
                    IsPADRVRSIEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS)
                {
                    IsPAPASSChecked = true;
                    IsPAPASSSIEnabled = true;
                    IsPASSEnabled = true;
                }
            }
        }


        [HttpPost]
        public IHttpActionResult getWorkshopList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string search = "%" + form.Get("search").Trim() + "%";

                string query = @"
                 SELECT * FROM Workshop WHERE WorkshopName 
                 like @0 OR City like @0 Order by WorkshopName ASC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, search);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string salesofficerid = form.Get("salesofficerid").Trim();
                string search = "%" + form.Get("search").Trim() + "%";
                string indexOrderByItem = form.Get("indexOrderByItem").Trim();
                int indexOrderByTypeItem = Int32.Parse(form.Get("indexOrderByTypeItem").Trim());
                bool chFollowUpMonthlyChecked = bool.Parse(form.Get("chFollowUpMonthlyChecked").Trim());
                bool chFollowUpDateChecked = bool.Parse(form.Get("chFollowUpDateChecked").Trim());
                bool chNeedFUChecked = bool.Parse(form.Get("chNeedFUChecked").Trim());
                bool chPotentialChecked = bool.Parse(form.Get("chPotentialChecked").Trim());
                bool chCallLaterChecked = bool.Parse(form.Get("chCallLaterChecked").Trim());
                bool chCollectDocChecked = bool.Parse(form.Get("chCollectDocChecked").Trim());
                bool chNotDealChecked = bool.Parse(form.Get("chNotDealChecked").Trim());
                bool chSentToSAChecked = bool.Parse(form.Get("chSentToSAChecked").Trim());
                bool chBackToAOChecked = bool.Parse(form.Get("chBackToAOChecked").Trim());
                bool chPolicyCreatedChecked = bool.Parse(form.Get("chPolicyCreatedChecked").Trim());
                string dateFollowUp = form.Get("getFollowUpDate");
                DateTime getFollowUpDate = DateTime.Now;
                if (!string.IsNullOrEmpty(dateFollowUp))
                {
                    getFollowUpDate = Convert.ToDateTime(dateFollowUp);
                }

                if (salesofficerid == null || salesofficerid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                //string query2 = @"
                //           SELECT fu.* FROM FollowUp fu 
                //JOIN OrderSimulation os ON os.FollowUpNo = fu.FollowUpNo 
                //JOIN HistoryPenawaran hp ON hp.OrderNo = os.OrderNo 
                //WHERE fu.SalesOfficerID=@0  AND fu.FollowUpStatus=9 AND fu.FollowUpName like @1
                //Union SELECT a.* FROM FollowUp a 
                //INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode 
                //WHERE a.SalesOfficerID=@0 AND a.FollowUpName like @1
                //                ";

                string query = @";SELECT a.* FROM FollowUp a INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode 
WHERE a.SalesOfficerID=@0 ";

                if (chFollowUpDateChecked)
                {
                    query = string.Concat(query, "AND (NextFollowUpdate <=@2  OR NextFollowUpDate IS NULL) ");
                }
                else if (chFollowUpMonthlyChecked)
                {
                    string querytemp = @"AND ((MONTH(a.NextFollowUpdate) = MONTH(GETDATE()) 
AND YEAR(a.NextFollowUpdate) = YEAR(GETDATE())) 
OR a.NextFollowUpdate IS NULL) ";
                    query = string.Concat(query, querytemp);
                }


                List<string> list = new List<string>();

                if (chNeedFUChecked)
                {
                    list.Add("FollowUpStatus=1 ");
                }
                if (chPotentialChecked)
                {
                    list.Add("FollowUpStatus=2 ");
                }
                if (chCallLaterChecked)
                {
                    list.Add("FollowUpStatus=3 ");
                }
                if (chCollectDocChecked)
                {
                    list.Add("FollowUpStatus=4 ");
                }
                if (chNotDealChecked)
                {
                    list.Add("FollowUpStatus=5 ");
                }
                if (chSentToSAChecked)
                {
                    list.Add("FollowUpStatus=6 ");
                }
                if (chPolicyCreatedChecked)
                {
                    list.Add("FollowUpStatus=7 ");
                }
                if (chBackToAOChecked)
                {
                    list.Add("FollowUpStatus=8 ");
                }

                if (list.Count == 1)
                {
                    query = string.Concat(query, "AND " + list[0]);
                }
                else if (list.Count > 1)
                {
                    query = string.Concat(query, "AND (");
                    for (int i = 0; i < list.Count; i++)
                    {
                        query = string.Concat(query, list[i]);
                        if (list.Count != i + 1)
                        {
                            query = string.Concat(query, " OR ");
                        }
                    }
                    query = string.Concat(query, ") ");
                }

                if (!string.IsNullOrEmpty(form.Get("search")))
                {
                    query = string.Concat(query, "AND ProspectName like @1 ");
                }

                query = string.Concat(query, "ORDER BY ");

                if (indexOrderByItem.Equals("Status"))
                {
                    query = string.Concat(query, "b.SeqNo ASC, a.EntryDate DESC");
                }
                if (indexOrderByItem.Equals("Name"))
                {
                    query = string.Concat(query, "a.ProspectName");
                }
                if (indexOrderByItem.Equals("EntryDate"))
                {
                    query = string.Concat(query, "a.EntryDate");
                }
                if (indexOrderByItem.Equals("LastFollowUpdate"))
                {
                    query = string.Concat(query, "a.LastFollowUpdate");
                }
                if (indexOrderByItem.Equals("NextFollowUpdate"))
                {
                    query = string.Concat(query, "a.NextFollowUpdate");
                }

                if (!indexOrderByItem.Equals("Status"))
                {
                    if (indexOrderByTypeItem == 0)
                    {
                        query = string.Concat(query, " ASC");
                    }
                    else
                    {
                        query = string.Concat(query, " DESC");
                    }
                }

                //                string querytempaja = @"
                //Union SELECT a.* FROM FollowUp a 
                //                INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode 
                //                WHERE a.SalesOfficerID=@0 AND a.FollowUpName like @1 ORDER BY EntryDate DESC";
                //                query = string.Concat(query, querytempaja);

                string queryprospectLeadList = @";SELECT fu.* FROM FollowUp fu 
JOIN OrderSimulation os ON os.FollowUpNo = fu.FollowUpNo 
JOIN Quotation.dbo.HistoryPenawaran hp ON hp.OrderNo = os.OrderNo 
WHERE fu.SalesOfficerID=@0 AND fu.FollowUpStatus=9 AND fu.RowStatus = 1
ORDER BY EntryDate DESC";



                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    salesofficerid = salesofficerid.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", salesofficerid) : salesofficerid;

                    var result = db.Fetch<dynamic>(query, salesofficerid, search, getFollowUpDate);
                    var prospectLeadList = db.Fetch<dynamic>(queryprospectLeadList, salesofficerid, search);

                    List<dynamic> historyPenawaranList = new List<dynamic>();
                    List<dynamic> penawaranList = new List<dynamic>();


                    for (int fi = 0; fi < prospectLeadList.Count; fi++)
                    {
                        string queryhistoryPenawaranList = @";SELECT TOP 1 hp.* FROM Quotation.dbo.HistoryPenawaran hp 
INNER JOIN OrderSimulation os ON hp.OrderNo = os.OrderNo WHERE os.FollowUpNo = @0 ORDER BY LastUpdatedTime DESC";
                        historyPenawaranList.Add(db.FirstOrDefault<dynamic>(queryhistoryPenawaranList, prospectLeadList[fi].FollowUpNo));

                        string querypenawaranList = @";SELECT * FROM Quotation.dbo.Penawaran WHERE policyID = @0";
                        penawaranList.Add(db.FirstOrDefault<dynamic>(querypenawaranList, historyPenawaranList[fi].PolicyID));
                    }

                    return Json(new { status = true, message = "OK", data = prospectLeadList.Concat(result), historyPenawaranList = historyPenawaranList, penawaranList = penawaranList }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpStatus(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupstatus = form.Get("followupstatus").Trim();
                string followupstatusactivity = form.Get("followupstatusactivity");

                if (string.IsNullOrEmpty(followupstatus))
                {
                    return Json(new { status = false, message = "Param is null", data = "No Data" }, Util.jsonSerializerSetting());
                }
                string query = @"";
                if (!followupstatus.Equals("6") && !followupstatus.Equals("7") && !followupstatus.Equals("8"))
                {
                    if (!followupstatus.Equals("1"))
                    {
                        query += "SELECT * FROM FollowUpStatus WHERE StatusCode NOT IN (1, 7, 8) AND RowStatus = 1 ORDER BY StatusCode";
                    }
                    else
                    {
                        query += "SELECT * FROM FollowUpStatus WHERE StatusCode NOT IN (7,8) AND RowStatus = 1 ORDER BY StatusCode";
                    }
                }
                else if (followupstatus.Equals("8"))
                {
                    query += "SELECT * FROM FollowUpStatus WHERE StatusCode IN (6,8) AND RowStatus = 1 ORDER BY StatusCode";
                }
                else
                {
                    query += "SELECT * FROM FollowUpStatus WHERE StatusCode =@0 AND RowStatus = 1 ORDER BY StatusCode";
                }

                if (!string.IsNullOrEmpty(followupstatusactivity))
                {
                    if (followupstatusactivity.Equals("6"))
                    {
                        query = @";SELECT * FROM FollowUpStatus WHERE StatusCode=6 AND RowStatus = 1 ORDER BY StatusCode";
                    }
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM FollowUpStatus WITH(NOLOCK) WHERE LastUpdatedTime = @0", "19000101");


                    query = query + " ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";
                    var result = db.Fetch<dynamic>(query, followupstatus, totalcount, 100);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpHistory(FormDataCollection form)
        {
            try
            {
                string followupno = form.Get("followupno").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  select * from FollowUpHistory where FollowUpNo=@0 and SeqNo > 0 order by SeqNo DESC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult sendToSA(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno");
                string orderno = form.Get("orderno");
                string quotationno = form.Get("quotationno");
                string SalesOfficerID = form.Get("salesofficerid");

                if (string.IsNullOrEmpty(followupno) || string.IsNullOrEmpty(orderno) || string.IsNullOrEmpty(quotationno) || string.IsNullOrEmpty(SalesOfficerID))
                {
                    return Json(new { status = false, message = "Param is null" }, Util.jsonSerializerSetting());
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    SalesOfficerID = SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", SalesOfficerID) : SalesOfficerID;

                    string query = @"SELECT * FROM OrderSimulation WHERE FollowUpNo =@0";

                    string quotationNoBefore = "";

                    // ClearSendF
                    List<OrderSimulation> OS = db.Fetch<OrderSimulation>(query, followupno);
                    for (var i = 0; i < OS.Count; i++)
                    {
                        OrderSimulation item = OS[i];
                        if (!item.OrderNo.Equals(orderno))
                        {
                            quotationNoBefore = item.QuotationNo;
                            db.Execute("UPDATE OrderSimulation SET ApplyF=0, SendF=0 WHERE OrderNo=@0", item.OrderNo);
                        }
                    }


                    // SendToSA
                    OrderSimulation OSModel = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE OrderNo=@0", orderno);
                    if (OSModel != null)
                    {
                        db.Execute("UPDATE OrderSimulation SET ApplyF=1, SendF=1, SendDate=GETDATE() WHERE OrderNo=@0", OSModel.OrderNo);
                    }

                    // InsertEmailNotification

                    EmailNotification enModel = new EmailNotification();
                    enModel.EmailNotificationNo = System.Guid.NewGuid().ToString();
                    string sqlQuery = @"SELECT COUNT(*) FROM EmailNotification WHERE ParameterValue=@0";

                    try
                    {
                        if (!string.IsNullOrEmpty(quotationNoBefore) && !quotationNoBefore.Equals(quotationno))
                        {
                            enModel.NotificationType = 4;
                            enModel.ParameterName = "FollowUpNo||QuotationNoBefore";
                            enModel.ParameterValue = followupno + "||" + quotationNoBefore;

                        }
                        else
                        {
                            enModel.NotificationType = ((db.ExecuteScalar<int>(sqlQuery, followupno) > 0) ? 2 : 1);
                            enModel.ParameterName = "FollowUpNo";
                            enModel.ParameterValue = followupno;
                        }
                        enModel.EmailSendStatus = 0;
                        enModel.UserID = SalesOfficerID;
                        enModel.RowStatus = true;
                        enModel.LastUpdatedTime = new DateTime();

                        string insertEnModel = @"INSERT INTO [dbo].[EmailNotification]
           ([EmailNotificationNo]
           ,[NotificationType]
           ,[ParameterName]
           ,[ParameterValue]
           ,[EmailSendStatus]
           ,[UserID]
           ,[EntryDate]
           ,[LastUpdatedTime]
           ,[RowStatus])
     VALUES
           (@0,@1,@2,@3,@4,@5,GETDATE(),GETDATE(),1)";

                        db.Execute(insertEnModel, enModel.EmailNotificationNo, enModel.NotificationType, enModel.ParameterName, enModel.ParameterValue, enModel.EmailSendStatus, SalesOfficerID);

                        return Json(new { status = true, message = "Success" }, Util.jsonSerializerSetting());

                    }
                    catch (Exception e)
                    {
                        _log.Error("Function " + actionName + " Error : " + e.ToString());
                        return Json(new { status = false, message = e.Message });
                    }

                }

                return Json(new { status = true, message = "OK" }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult getProspectCustomer(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string custid = form.Get("custid").Trim();

                if (custid == null || custid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }


                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    #region PROSPECT CUSTOMER
                    string query = @"
                  SELECT TOP 1 * FROM ProspectCustomer WHERE CustID=@0
                ";
                    ProspectCustomer ProspectCustomer = db.FirstOrDefault<ProspectCustomer>(query, custid);
                    #endregion


                    return Json(new { status = true, message = "OK", ProspectCustomer }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult getProspectCustomerEdit(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string custid = form.Get("custid").Trim();

                if (custid == null || custid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }


                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    #region PROSPECT CUSTOMER
                    FollowUp FollowUp = new FollowUp();
                    ProspectCustomer ProspectCustomer = new ProspectCustomer();
                    OrderSimulation OrderSimulation = new OrderSimulation();
                    List<OrderSimulationMV> OrderSimulationMV = new List<OrderSimulationMV>();
                    List<OrderSimulationInterest> OrderSimulationInterest = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> OrderSimulationCoverage = new List<OrderSimulationCoverage>();
                    List<Dealer> Dealer = new List<Dealer>();
                    List<SalesmanDealer> SalesmanDealer = new List<SalesmanDealer>();
                    string query = @"
                  SELECT TOP 1 * FROM ProspectCustomer WHERE CustID=@0
                ";
                    ProspectCustomer = db.FirstOrDefault<ProspectCustomer>(query, custid);
                    #endregion


                    #region FOLLOW UP
                    query = @"
                  SELECT TOP 1 * FROM FollowUp WHERE CustID=@0
                ";
                    FollowUp = db.FirstOrDefault<FollowUp>(query, custid);
                    #endregion


                    #region ORDERSIMULATION
                    query = @"
					SELECT * FROM OrderSimulation WHERE CustId=@0 AND ApplyF=1
                ";
                    OrderSimulation = db.FirstOrDefault<OrderSimulation>(query, custid);
                    #endregion
                    if (OrderSimulation != null)
                    {

                        #region ORDERSIMULATIONMV
                        query = @"SELECT b.* FROM OrderSimulation a INNER JOIN OrderSimulationMV b ON 
                                a.OrderNo=b.OrderNo WHERE a.RowStatus =1 AND 
                                b.RowStatus = 1 AND a.FollowUpNo=@0 ";
                        OrderSimulationMV = db.Fetch<OrderSimulationMV>(query, FollowUp.FollowUpNo);
                        foreach (OrderSimulationMV osmv in OrderSimulationMV)
                        {
                            if (OrderSimulation.ComprePeriod + OrderSimulation.TLOPeriod == 0)
                            {
                                osmv.ProductTypeCode = null;
                                OrderSimulation.ProductCode = null;
                            }
                        }
                        #endregion


                        #region ORDERSIMULATION INTEREST
                        query = @"SELECT * FROM OrderSimulationInterest WHERE RowStatus=1 AND OrderNo=@0
                ";
                        OrderSimulationInterest = null;
                        if (OrderSimulation != null)
                        {
                            OrderSimulationInterest = db.Fetch<OrderSimulationInterest>(query, OrderSimulation.OrderNo);
                        }
                        #endregion


                        #region ORDERSIMULATION COVERAGE
                        query = @"SELECT * FROM OrderSimulationCoverage WHERE RowStatus=1 AND OrderNo=@0
                ";
                        OrderSimulationCoverage = null;

                        if (OrderSimulation != null)
                        {
                            OrderSimulationCoverage = db.Fetch<OrderSimulationCoverage>(query, OrderSimulation.OrderNo);
                        }
                        #endregion


                        #region DEALER
                        query = @"SELECT * FROM Dealer WHERE RowStatus=1 Order By Description 
                ";
                        Dealer = db.Fetch<Dealer>(query);
                        #endregion


                        #region SALESMAN DEALER
                        query = @"SELECT * FROM SalesmanDealer WHERE DealerCode=@0 AND RowStatus=1  Order By Description
                ";
                        SalesmanDealer = null;
                        if (OrderSimulation != null)
                        {
                            SalesmanDealer = db.Fetch<SalesmanDealer>(query, OrderSimulation.DealerCode);
                        }
                        #endregion

                    }
                    #region LOAD IMAGE
                    List<dynamic> imageData = new List<dynamic>();
                    if (FollowUp != null)
                    {
                        query = @"SELECT 
 ''''+  ISNULL([IdentityCard]+''',',''',')         
    +''''+ ISNULL([STNK]+''',',''',')
    +''''+ ISNULL([SPPAKB]+''',',''',')
    +''''+ ISNULL([BSTB1]+''',',''',')
    +''''+ ISNULL([BSTB2]+''',',''',')
    +''''+ ISNULL([BSTB3]+''',',''',')
    +''''+ ISNULL([BSTB4]+''',',''',')
    +''''+ ISNULL([CheckListSurvey1]+''',',''',')
    +''''+ ISNULL([CheckListSurvey2]+''',',''',')
    +''''+ ISNULL([CheckListSurvey3]+''',',''',')
    +''''+ ISNULL([CheckListSurvey4]+''',',''',')
    +''''+ ISNULL([PaymentReceipt1]+''',',''',')
    +''''+ ISNULL([PaymentReceipt2]+''',',''',')
    +''''+ ISNULL([PaymentReceipt3]+''',',''',')
    +''''+ ISNULL([PaymentReceipt4]+''',',''',')
    +''''+ ISNULL([PremiumCal1]+''',',''',')
    +''''+ ISNULL([PremiumCal2]+''',',''',')
    +''''+ ISNULL([PremiumCal3]+''',',''',')
    +''''+ ISNULL([PremiumCal4]+''',',''',')
    +''''+ ISNULL([FormA1]+''',',''',')
    +''''+ ISNULL([FormA2]+''',',''',')
    +''''+ ISNULL([FormA3]+''',',''',')
    +''''+ ISNULL([FormA4]+''',',''',')
    +''''+ ISNULL([FormB1]+''',',''',')
    +''''+ ISNULL([FormB2]+''',',''',')
    +''''+ ISNULL([FormB3]+''',',''',')
    +''''+ ISNULL([FormB4]+''',',''',')
    +''''+ ISNULL([FormC1]+''',',''',')
    +''''+ ISNULL([FormC2]+''',',''',')
    +''''+ ISNULL([FormC3]+''',',''',')
    +''''+ ISNULL([FormC4]+'''','''') 
  FROM [AABMobile].[dbo].[FollowUp] WHERE FollowUpNo=@0";
                        string listimage = db.ExecuteScalar<string>(query, FollowUp.FollowUpNo);


                        query = @"SELECT * FROM ImageData WHERE PathFile IN (@@Param) AND RowStatus=1";
                        query = query.Replace("@@Param", string.IsNullOrEmpty(listimage) ? "''" : listimage);
                        imageData = db.Fetch<dynamic>(query);

                    }

                    #endregion

                    //IF Prospect Lead
                    string preferredtime = "";
                    if (FollowUp.FollowUpStatus.Equals("9"))
                    {
                        #region Prefered Time
                        preferredtime = db.ExecuteScalar<string>(@"
					SELECT p.followUpDateTime FROM Penawaran p 
                                JOIN HistoryPenawaran hp ON hp.PolicyID = p.policyID 
                                JOIN OrderSimulation os ON os.OrderNo = hp.OrderNo 
                                AND os.FollowUpNo =@0", FollowUp.FollowUpNo);
                        #endregion
                    }

                    return Json(new { status = true, message = "OK", ProspectCustomer, FollowUp, OrderSimulation, OrderSimulationMV, OrderSimulationInterest, OrderSimulationCoverage, Dealer, SalesmanDealer, imageData, preferredtime }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getSummaryResult(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  SELECT 
                     a.Name, a.Phone1, a.Email1, 
                     e.Description + ' ' + f.Description + ' ' + g.Series + ' ' + d.Year AS 'Vehicle', 
                     CASE 
                     WHEN c.ComprePeriod > 0 AND c.TLOPeriod = 0 
	                    THEN 'Comprehensive ' + CAST(c.ComprePeriod AS VARCHAR) + 
		                    CASE WHEN c.ComprePeriod > 1 THEN ' years' 
		                    ELSE ' year' 
		                    END 
                    WHEN c.ComprePeriod = 0 AND c.TLOPeriod > 0 
	                    THEN 'TLO ' + CAST(c.TLOPeriod AS VARCHAR) + 
		                    CASE WHEN c.TLOPeriod > 1 THEN ' years' 
		                    ELSE ' year' 
		                    END 
                    WHEN c.ComprePeriod > 0 AND c.TLOPeriod > 0 
	                    THEN 'Comprehensive ' + CAST(c.ComprePeriod AS VARCHAR) + 
		                    CASE 
			                    WHEN c.ComprePeriod > 1 THEN ' years' 
			                    ELSE ' year' 
			                    END + ' + TLO ' + CAST(c.TLOPeriod AS VARCHAR) + 
		                    CASE 
			                    WHEN c.TLOPeriod > 1 THEN ' years' 
			                    ELSE ' year' 
			                    END 
                    END AS 'CoverageType', 
                    c.TotalPremium, d.SumInsured, d.AccessSI, h.Description AS 'DealerDesc', 
                    i.Description AS 'SalesmanDesc'
                    FROM ProspectCustomer a 
                    INNER JOIN FollowUp b ON a.custid=b.custid 
                    INNER JOIN OrderSimulation c ON b.followupno=c.followupno AND c.applyf=1 
                    INNER JOIN OrderSimulationMV d ON c.OrderNo = d.OrderNo 
                    INNER JOIN VehicleBrand e ON d.BrandCode = e.BrandCode 
                    AND d.ProductTypeCode = e.ProductTypeCode 
                    INNER JOIN VehicleModel f ON f.BrandCode = e.BrandCode 
                    AND f.ProductTypeCode = e.ProductTypeCode AND f.ModelCode = d.ModelCode 
                    INNER JOIN VehicleSeries g ON f.BrandCode = g.BrandCode 
                    AND f.ProductTypeCode = g.ProductTypeCode AND f.ModelCode = g.ModelCode 
                    AND d.Series = g.Series LEFT JOIN Dealer h ON a.DealerCode=h.DealerCode 
                    LEFT JOIN SalesmanDealer i ON a.SalesDealer=i.SalesmanCode AND h.DealerCode=i.DealerCode
                    WHERE b.FollowUpNo=@0
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getOrderDetailHeader(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();
                string branchcode = form.Get("branchcode").Trim();
                string channelsource = form.Get("channelsource").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  SELECT 
                    os.OrderNo, 
                    os.InsuranceType, 
                    pt.Description AS 'InsuranceTypeDesc', 
                    aab.City, 
                    os.EntryDate, 
                    os.QuotationNo, 
                    pc.Name AS 'ProspectName', 
                    pc.Phone1, 
                    pc.Email1, 
                    vb.Description + ' ' + vm.Description + ' ' + vs.Series AS 'Vehicle', 
                    osmv.RegistrationNumber, 
                    os.YearCoverage, 
                    osmv.AccessSI, 
                    osmv.Year, 
                    dif.Description AS 'Usage', 
                    osmv.Sitting, 
                    r.Sequence AS 'Region', 
                    os.TotalPremium, 
                    ao.Name AS 'AccountOfficerName', 
                    ao.Phone1 AS 'AOPhone', 
                    os.AdminFee, 
                    ao2.Name AS 'BranchHead', 
                    aab.Bank, 
                    aab.AccountNo, 
                    aab.AccountName, 
                    ao.Fax, 
                    ao.OfficePhone, 
                    os.TLOPeriod, 
                    os.ComprePeriod, 
                    case 
	                    when os.ComprePeriod > 0 and os.TLOPeriod = 0 
		                    then 'Comprehensive ' + CAST(os.ComprePeriod AS VARCHAR) + ' Tahun' 
	                    when os.ComprePeriod = 0 and os.TLOPeriod > 0 
		                    then 'TLO ' + CAST(os.TLOPeriod AS VARCHAR) + ' Tahun' 
	                    when os.ComprePeriod > 0 and os.TLOPeriod > 0 
		                    then 'Comprehensive ' + CAST(os.ComprePeriod AS VARCHAR) + ' Tahun\nTLO ' 
			                    + CAST(os.TLOPeriod AS VARCHAR) + ' Tahun' 
	                    end AS 'CoverageType' 
                    FROM OrderSimulation os 
                    INNER JOIN OrderSimulationMV osmv ON os.OrderNo=osmv.OrderNo 
                    INNER JOIN VehicleBrand vb on osmv.BrandCode=vb.BrandCode 
                    AND osmv.ProductTypeCode=vb.ProductTypeCode 
                    INNER JOIN VehicleModel vm on vm.BrandCode=vb.BrandCode 
                    AND vm.ProductTypeCode=vb.ProductTypeCode AND vm.ModelCode=osmv.ModelCode 
                    INNER JOIN VehicleSeries vs on vm.BrandCode = vs.BrandCode 
                    AND vm.ProductTypeCode=vs.ProductTypeCode AND vm.ModelCode=vs.ModelCode 
                    AND osmv.Series=vs.Series 
                    INNER JOIN ProspectCustomer pc ON os.CustID=pc.CustID 
                    INNER JOIN SalesOfficer ao ON os.SalesOfficerID=ao.SalesOfficerID 
                    INNER JOIN ProductType pt ON os.InsuranceType=pt.InsuranceType 
                    INNER JOIN AABBranch aab ON os.BranchCode=aab.BranchCode 
                    INNER JOIN Dtl_Ins_Factor dif ON dif.Factor_Code='MVUSAG' 
                    AND dif.insurance_code=osmv.UsageCode 
                    INNER JOIN Region r ON r.RegionCode=osmv.CityCode 
                    INNER JOIN AABHead aah ON aah.Type='Branch' AND aah.Param=os.BranchCode 
                    INNER JOIN SalesOfficer ao2 ON ao2.SalesOfficerID=aah.Value WHERE os.FollowUpNo=@0 AND os.ApplyF=1
                ";

                string query2 = @"SELECT * FROM SysParam WHERE Type='ReportQuotation'";

                string query3 = @"SELECT A.* FROM Branch A INNER JOIN AABBranch B
 ON A.Name LIKE '%'+
 (SELECT TOP 1 [Description] FROM AABBRANCH WHERE BRANCHCODE = @0)+'%'
 WHERE B.BranchCode = @0";

                string query4 = @"SELECT * FROM SysParam WHERE Param='QuotSign'+@0";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);
                    var result2 = db.Fetch<dynamic>(query2);
                    var result3 = db.FirstOrDefault<dynamic>(query3, branchcode);
                    var result4 = db.FirstOrDefault<dynamic>(query4, channelsource);

                    return Json(new { status = true, message = "OK", data = result, spModels = result2, BranchData = result3, param = result4 }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getOrderDetailCoverage(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string orderno = form.Get("orderno").Trim();

                if (orderno == null || orderno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  SELECT  
                    osi.InterestID, osc.CoverageID, osi.Year, osc.Rate, 
                    osc.SumInsured, osc.LoadingRate, osc.Loading, osc.Premium, 
                    osc.IsBundling 
                    FROM OrderSimulation os 
                    INNER JOIN OrderSimulationMV osmv ON os.OrderNo=osmv.OrderNo
                    INNER JOIN OrderSimulationInterest osi ON osmv.OrderNo=osi.OrderNo 
                    AND osmv.ObjectNo=osi.ObjectNo
                    INNER JOIN OrderSimulationCoverage osc ON osc.OrderNo=osi.OrderNo 
                    AND osc.ObjectNo=osi.ObjectNo AND osc.InterestNo=osi.InterestNo 
                    WHERE os.OrderNo=@0 AND osi.RowStatus = 1 AND osc.RowStatus = 1
                    ORDER BY osi.Year
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, orderno);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getQuotationHistory(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  select 
                    a.CustID, a.OrderNo, a.QuotationNo,  
                    a.EntryDate, a.TotalPremium, 
                    c.Description + ' ' + d.Description + ' ' + e.Series Vehicle, 
                    b.Year VehicleYear,  b.SumInsured, b.AccessSI, 
                    a.ApplyF, a.SendF, a.SendStatus, 
                    case 
	                    when a.ComprePeriod > 0 and a.TLOPeriod = 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod = 0 and a.TLOPeriod > 0 
		                    then 'TLO ' + CAST(a.TLOPeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod > 0 and a.TLOPeriod > 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)
			                    + 'Th TLO ' + CAST( a.TLOPeriod AS VARCHAR)+ 'Th' 
                    end AS 'CoverageType' 
                    from OrderSimulation a 
                    inner join OrderSimulationMV b on a.OrderNo = b.OrderNo 
                    inner join VehicleBrand c on b.BrandCode = c.BrandCode 
                    AND b.ProductTypeCode = c.ProductTypeCode 
                    inner join VehicleModel d on d.BrandCode = c.BrandCode 
                    AND d.ProductTypeCode = c.ProductTypeCode AND d.ModelCode = b.ModelCode 
                    inner join VehicleSeries e on d.BrandCode = e.BrandCode 
                    AND d.ProductTypeCode = e.ProductTypeCode AND d.ModelCode = e.ModelCode 
                    AND b.Series = e.Series 
                    where a.FollowUpNo=@0
                    ORDER BY a.EntryDate DESC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveFollowUpDetails(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string FUData = form.Get("FollowUp").Replace(":0\",", ":01\",");
                FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);

                string FULasData = form.Get("FollowUpLast").Replace(":0\",", ":01\",");
                FollowUp FULast = JsonConvert.DeserializeObject<FollowUp>(FULasData);

                bool isInfoChanged = bool.Parse(form.Get("isInfoChanged").Trim());

                bool isSentToSA = false;

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    #region UPDATE FOLLOW UP
                    string queryupdatefollowup = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE() WHERE [FollowUpNo]=@0";

                    db.Execute(queryupdatefollowup, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate);
                    #endregion

                    if (isInfoChanged)
                    {
                        if (FU.FollowUpStatus == 9)
                        {
                            string query = @"SELECT TOP 1 * FROM HistoryPenawaran 
WHERE OrderNo = (SELECT OrderNo FROM OrderSimulation 
WHERE FollowUpNo =@0 ORDER BY EntryDate";

                            HistoryPenawaran hp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);
                            hp.IsAbleExpired = 0;

                            bool responsestatus = false;
                            using (var dbagent = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                            {
                                int bidExist = dbagent.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", hp.HistoryPenawaranID);

                                if (bidExist == 1)
                                {
                                    // permanently set data to agent
                                    db.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);
                                    responsestatus = true;
                                }

                                if (responsestatus)
                                {
                                    //#region UPDATE FOLLOW UP
                                    //query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE() WHERE [FollowUpNo]=@0";

                                    //db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate);
                                    //#endregion

                                    int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                    #region INSERT FOLLOW UP HISTORY
                                    query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),0)
";

                                    db.Execute(query, FULast.FollowUpNo, lastseqno, FULast.CustID, FULast.ProspectName, FULast.Phone1, FULast.Phone2, FULast.SalesOfficerID, FULast.FollowUpName, FULast.NextFollowUpDate, FULast.LastFollowUpDate, FULast.FollowUpStatus, FULast.FollowUpInfo, FULast.Remark, FULast.BranchCode);
                                    #endregion


                                    db.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);

                                }
                            }
                            query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0";

                            return Json(new { status = true, message = "succes", data = db.FirstOrDefault<dynamic>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                        }
                        else
                        {
                            //#region UPDATE FOLLOW UP
                            //string query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE() WHERE [FollowUpNo]=@0";

                            //db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate);
                            //#endregion
                            string query = "";

                            int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                            #region INSERT FOLLOW UP HISTORY
                            query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),0)
";

                            db.Execute(query, FULast.FollowUpNo, lastseqno, FULast.CustID, FULast.ProspectName, FULast.Phone1, FULast.Phone2, FULast.SalesOfficerID, FULast.FollowUpName, FULast.NextFollowUpDate, FULast.LastFollowUpDate, FULast.FollowUpStatus, FULast.FollowUpInfo, FULast.Remark, FULast.BranchCode);
                            #endregion

                            query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0";

                            return Json(new { status = true, message = "succes", data = db.FirstOrDefault<dynamic>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                        }
                    }
                    else
                    {
                        string query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0";
                        return Json(new { status = true, message = "succes", data = db.FirstOrDefault<dynamic>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                    }

                }


            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveFollowUpStatusDetails(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string FUData = form.Get("FollowUp");
                FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);

                string FULasData = form.Get("FollowUpLast");
                FollowUp FULast = JsonConvert.DeserializeObject<FollowUp>(FULasData);

                bool isInfoChanged = false;

                bool isSentToSA = bool.Parse(form.Get("isSentToSA").Trim());
                string STATE = form.Get("STATE");

                #region MGO BID
                if (STATE.Equals("MGOBID"))
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;
                        FULast.SalesOfficerID = FULast.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FULast.SalesOfficerID) : FULast.SalesOfficerID;

                        string query = @";SELECT TOP 1 * FROM Quotation.dbo.HistoryPenawaran 
WHERE OrderNo = (SELECT TOP 1 OrderNo FROM OrderSimulation 
WHERE FollowUpNo =@0 ORDER BY EntryDate DESC)";

                        HistoryPenawaran hp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);
                        hp.IsAbleExpired = 0;

                        bool responsestatus = false;

                        // check all mgo bid if expired bid still exist
                        Util.CheckAllMGOBidExpiry();

                        using (var dbagent = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                        {
                            // check data not expired
                            int bidExist = dbagent.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", hp.HistoryPenawaranID);

                            if (bidExist == 1)
                            {
                                // permanently set data to agent
                                dbagent.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);
                                responsestatus = true;
                            }

                            if (responsestatus)
                            {

                                #region execUpdateFollowUp
                                #region UPDATE FOLLOW UP
                                if (FU.FollowUpInfo != FULast.FollowUpInfo)
                                {
                                    isInfoChanged = true;


                                }
                                query = @";UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6 WHERE [FollowUpNo]=@0";

                                db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1);

                                #endregion

                                if (isInfoChanged)
                                {
                                    #region INSERT FOLLOW UP HISTORY
                                    query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),0)
";
                                    int lastseqnofuh = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                    db.Execute(query, FU.FollowUpNo, lastseqnofuh, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                                    #endregion
                                }
                                string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                                #endregion

                                int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                db.Execute("UPDATE Quotation.dbo.HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);

                                query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                                return Json(new { status = true, message = message, data = db.FirstOrDefault<FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());

                            }
                            else
                            {
                                query = @";SELECT * FROM OrderSimulation  WHERE FollowUpNo = @0";
                                List<OrderSimulation> orderSimulation = db.Fetch<OrderSimulation>(query, FU.FollowUpNo);

                                query = @";SELECT * FROM Quotation.dbo.HistoryPenawaran hp 
JOIN OrderSimulation os ON os.OrderNo = hp.OrderNo 
JOIN FollowUp fu On fu.FollowUpNo = os.FollowUpNo  WHERE fu.FollowUpNo = @0";

                                HistoryPenawaran hptemp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);

                                foreach (OrderSimulation os in orderSimulation)
                                {
                                    string queryDeleted = @";UPDATE OrderSimulationCoverage SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationInterest SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationMV SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulation SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);

                                }

                                string queryDelete = @";UPDATE FollowUpHistory SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE FollowUp SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE ProspectCustomer SET RowStatus=0 WHERE CustID=@0";
                                db.Execute(queryDelete, FU.CustID);
                                queryDelete = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE HistoryPenawaranID=@0";
                                db.Execute(queryDelete, hptemp.HistoryPenawaranID);

                                int hpCount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM HistoryPenawaran WHERE PolicyID = @0", hp.PolicyID);
                                if (hpCount == 0)
                                {
                                    queryDelete = @";UPDATE Quotation.dbo.Penawaran SET RowStatus=0 WHERE PolicyID=@0";
                                    db.Execute(queryDelete, hptemp.HistoryPenawaranID);
                                }

                                return Json(new { status = true, message = "Unable to process your request. This prospect has been expired" }, Util.jsonSerializerSetting());
                            }

                        }
                    }

                }
                #endregion

                else
                {
                    # region execUpdateFollowUp
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;
                        FULast.SalesOfficerID = FULast.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FULast.SalesOfficerID) : FULast.SalesOfficerID;


                        #region UPDATE FOLLOW UP
                        if (FU.FollowUpInfo != FULast.FollowUpInfo)
                        {
                            isInfoChanged = true;


                        }
                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
BEGIN
SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
END
ELSE
BEGIN
SELECT 0 AS SeqNo
END", FU.FollowUpNo) + 1;

                        string query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6 WHERE [FollowUpNo]=@0";

                        db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, lastseqno);

                        #endregion

                        if (isInfoChanged)
                        {
                            #region INSERT FOLLOW UP HISTORY
                            query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),0)
";

                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FULast.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                            #endregion
                        }
                        string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                        query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                        return Json(new { status = true, message = message, data = db.FirstOrDefault<FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                    }
                    #endregion
                }

        
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpDetailsStatusInfo(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string statuscode = form.Get("statuscode").Trim();
                string followupstatusactivity = form.Get("followupstatusactivity");

                string query = @";SELECT * FROM FollowUpStatusInfo WHERE StatusCode=@0 AND RowStatus = 1 ORDER BY InfoCode";

                if (!string.IsNullOrEmpty(followupstatusactivity))
                {
                    if (followupstatusactivity.Equals("6"))
                    {
                        query = @";SELECT * FROM FollowUpStatusInfo WHERE StatusCode=6 AND InfoCode=27 AND RowStatus = 1 ORDER BY InfoCode";

                    }
                    else
                    {
                        query = @";SELECT * FROM FollowUpStatusInfo WHERE StatusCode=@1 AND RowStatus = 1 ORDER BY InfoCode";
                    }
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, statuscode, followupstatusactivity);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getStatusInfoDescription(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string infocode = form.Get("infocode").Trim();

                string query = @"
                    SELECT Description FROM FollowUpStatusInfo WHERE InfoCode=@0
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, infocode);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getStatusInfoDescriptionAll()
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string query = @"
                    SELECT InfoCode, Description FROM FollowUpStatusInfo
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult isValidatedSendToSA(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                string query = @"
                    SELECT DISTINCT a.* FROM OrderSimulation a 
INNER JOIN OrderSimulationMV b ON a.OrderNo=b.OrderNo 
INNER JOIN OrderSimulationInterest c ON b.OrderNo=c.OrderNo 
AND b.ObjectNo=c.ObjectNo
INNER JOIN OrderSimulationCoverage d ON c.OrderNo=d.OrderNo 
AND c.ObjectNo=d.ObjectNo AND c.InterestNo=d.InterestNo
WHERE a.FollowUpNo=@0
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    List<OrderSimulation> OS = db.Fetch<OrderSimulation>(query, followupno);

                    if (OS.Count == 0)
                    {
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = "There is no quotation for this prospect. Please make quotation first." }, Util.jsonSerializerSetting());
                    }

                    if (OS[0].ProductCode == null)
                    {
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = "Information not complete, please check mandatory field" }, Util.jsonSerializerSetting());
                    }

                    return Json(new { status = true, message = "OK", vaildToSA = true, toast = "Valid BY SQL QUERY" }, Util.jsonSerializerSetting());

                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult isValidatedSendToSArev(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string query = @";SELECT DISTINCT a.* FROM OrderSimulation a 
  INNER JOIN OrderSimulationMV b ON a.OrderNo=b.OrderNo 
  INNER JOIN OrderSimulationInterest c ON b.OrderNo=c.OrderNo AND 
  b.ObjectNo=c.ObjectNo
  INNER JOIN OrderSimulationCoverage d ON c.OrderNo=d.OrderNo AND 
  c.ObjectNo=d.ObjectNo AND c.InterestNo=d.InterestNo WHERE a.FollowUpNo=@0";

                    string queryFollowUp = @";select * from followup where followupno = @0";
                    FollowUp fuModel = db.FirstOrDefault<FollowUp>(queryFollowUp, followupno);

                    List<OrderSimulation> osModels = db.Fetch<OrderSimulation>(query, followupno);
                    string message = "";
                    if (osModels.Count == 0)
                    {
                        message = "There is no quotation for this prospect. Please make quotation first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(fuModel.IdentityCard))
                    {
                        message = "Please complete photo of KTP/KITAS/SIM first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(fuModel.STNK))
                    {
                        message = "Please complete photo of STNK or BSTB first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(fuModel.SPPAKB))
                    {
                        message = "Please complete photo of SPPAKB first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (fuModel.FollowUpStatus == 6 && fuModel.FollowUpInfo == 28)
                    {
                        message = "You could not send document of this follow up because the policy status on follow up is already awaiting approval.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(osModels[0].ProductCode))
                    {
                        message = "Information not complete, please check mandatory field";
                        return Json(new { status = true, message = "DIALOG", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }

                    return Json(new { status = true, message = "OK", vaildToSA = true, toast = "VALID" }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }

        }

        [HttpPost]
        public IHttpActionResult editQuotation(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();
                string orderNo = form.Get("orderNo").Trim();

                if (followupno == null || followupno == "" || orderNo == null || orderNo == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  select 
                    a.CustID, a.OrderNo, a.QuotationNo,  
                    a.EntryDate, a.TotalPremium, 
                    c.Description + ' ' + d.Description + ' ' + e.Series Vehicle, 
                    b.Year VehicleYear,  b.SumInsured, b.AccessSI, 
                    a.ApplyF, a.SendF, a.SendStatus, 
                    case 
	                    when a.ComprePeriod > 0 and a.TLOPeriod = 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod = 0 and a.TLOPeriod > 0 
		                    then 'TLO ' + CAST(a.TLOPeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod > 0 and a.TLOPeriod > 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)
			                    + 'Th TLO ' + CAST( a.TLOPeriod AS VARCHAR)+ 'Th' 
                    end AS 'CoverageType' 
                    from OrderSimulation a 
                    inner join OrderSimulationMV b on a.OrderNo = b.OrderNo 
                    inner join VehicleBrand c on b.BrandCode = c.BrandCode 
                    AND b.ProductTypeCode = c.ProductTypeCode 
                    inner join VehicleModel d on d.BrandCode = c.BrandCode 
                    AND d.ProductTypeCode = c.ProductTypeCode AND d.ModelCode = b.ModelCode 
                    inner join VehicleSeries e on d.BrandCode = e.BrandCode 
                    AND d.ProductTypeCode = e.ProductTypeCode AND d.ModelCode = e.ModelCode 
                    AND b.Series = e.Series 
                    where a.FollowUpNo=@0
                    ORDER BY a.EntryDate DESC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var quotationhistory = db.Fetch<dynamic>(query, followupno);
                    foreach (dynamic item in quotationhistory)
                    {
                        bool ApplyF = false;
                        if (item.OrderNo.Equals(orderNo))
                        {
                            ApplyF = true;
                        }
                        db.Execute("UPDATE OrderSimulation SET ApplyF=@1 WHERE OrderNo=@0", item.OrderNo, ApplyF);
                    }

                    #region ORDERSIMULATION
                    query = @"
					SELECT * FROM OrderSimulation WHERE OrderNo=@0
                ";
                    OrderSimulation OrderSimulation = db.FirstOrDefault<OrderSimulation>(query, orderNo);
                    #endregion


                    #region ORDERSIMULATIONMV
                    query = @"SELECT b.* FROM OrderSimulation a INNER JOIN OrderSimulationMV b ON 
                                a.OrderNo=b.OrderNo WHERE a.RowStatus =1 AND 
                                b.RowStatus = 1 AND a.orderNo=@0";
                    List<OrderSimulationMV> OrderSimulationMV = db.Fetch<OrderSimulationMV>(query, orderNo);
                    foreach (OrderSimulationMV osmv in OrderSimulationMV)
                    {
                        if (OrderSimulation.ComprePeriod + OrderSimulation.TLOPeriod == 0)
                        {
                            osmv.ProductTypeCode = null;
                            OrderSimulation.ProductCode = null;
                        }
                    }
                    #endregion


                    #region ORDERSIMULATION INTEREST
                    query = @"SELECT * FROM OrderSimulationInterest WHERE RowStatus=1 AND OrderNo=@0
                ";
                    List<OrderSimulationInterest> OrderSimulationInterest = db.Fetch<OrderSimulationInterest>(query, orderNo);
                    #endregion


                    #region ORDERSIMULATION COVERAGE
                    query = @"SELECT * FROM OrderSimulationCoverage WHERE RowStatus=1 AND OrderNo=@0
                ";
                    List<OrderSimulationCoverage> OrderSimulationCoverage = db.Fetch<OrderSimulationCoverage>(query, orderNo);
                    #endregion




                    return Json(new
                    {
                        status = true,
                        message = "OK",
                        OrderSimulation,
                        OrderSimulationMV,
                        OrderSimulationInterest,
                        OrderSimulationCoverage
                    }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult addCopyQuotation(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string state = form.Get("state").Trim();
                string orderNo = form.Get("orderNo").Trim();
                string followUpNo = form.Get("followUpNo").Trim();
                string query = "";


                if (state == null || state == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    ProspectCustomer prospectCustomer = new ProspectCustomer();
                    FollowUp followUp = new FollowUp();
                    OrderSimulation orderSimulation = new OrderSimulation();
                    OrderSimulationMV orderSimulationMV = new OrderSimulationMV();
                    List<OrderSimulationInterest> orderSimulationInterest = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> orderSimulationCoverage = new List<OrderSimulationCoverage>();


                    if (state.Equals("ADD"))
                    {
                        OrderSimulation oldOS = new OrderSimulation();
                        query = "SELECT * FROM OrderSimulation WHERE orderNo=@0";
                        OrderSimulation nowOS = db.FirstOrDefault<OrderSimulation>(query, orderNo);
                        if (nowOS != null)
                        {
                            oldOS = nowOS;
                        }
                        else
                        {
                            query = "SELECT * FROM OrderSimulation WHERE RowStatus=0 AND FollowUpNo=@0 AND ApplyF=1";
                            oldOS = db.FirstOrDefault<OrderSimulation>(query, followUpNo);
                        }
                        query = "UPDATE OrderSimulation SET ApplyF='false' WHERE OrderNo=@0";
                        db.Execute(query, oldOS.OrderNo);
                        query = "SELECT * FROM ProspectCustomer WHERE CustId=@0";
                        prospectCustomer = db.FirstOrDefault<ProspectCustomer>(query, oldOS.CustID);
                        query = "SELECT * FROM FollowUp WHERE CustId=@0";
                        followUp = db.FirstOrDefault<FollowUp>(query, oldOS.CustID);

                        prospectCustomer.SalesOfficerID = prospectCustomer.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", prospectCustomer.SalesOfficerID) : prospectCustomer.SalesOfficerID;
                        followUp.SalesOfficerID = followUp.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", followUp.SalesOfficerID) : followUp.SalesOfficerID;
                        orderSimulation.SalesOfficerID = orderSimulation.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", orderSimulation.SalesOfficerID) : orderSimulation.SalesOfficerID;


                    }
                    else if (state.Equals("COPY"))
                    {

                        query = "SELECT * FROM OrderSimulation WHERE OrderNo=@0";
                        orderSimulation = db.FirstOrDefault<OrderSimulation>(query, orderNo);

                        query = "UPDATE OrderSimulation SET ApplyF='false' WHERE OrderNo=@0";
                        db.Execute(query, orderSimulation.OrderNo);


                        query = @"SELECT osmv.* FROM OrderSimulationMV osmv 
                                    Inner Join OrderSimulation os ON os.OrderNo=osmv.OrderNo WHERE os.OrderNo=@0";
                        orderSimulationMV = db.FirstOrDefault<OrderSimulationMV>(query, orderNo);

                        query = @"SELECT * FROM OrderSimulationInterest WHERE RowStatus=0 AND OrderNo=@0
                ";
                        orderSimulationInterest = db.Fetch<OrderSimulationInterest>(query, orderNo);



                        query = @"SELECT * FROM OrderSimulationCoverage WHERE RowStatus=0 AND OrderNo=@0
                ";
                        orderSimulationCoverage = db.Fetch<OrderSimulationCoverage>(query, orderNo);


                        query = "SELECT * FROM ProspectCustomer WHERE CustId=@0";
                        prospectCustomer = db.FirstOrDefault<ProspectCustomer>(query, orderSimulation.CustID);

                        query = "SELECT * FROM FollowUp WHERE CustId=@0";
                        followUp = db.FirstOrDefault<FollowUp>(query, orderSimulation.CustID);


                        OrderSimulation newOSModel = orderSimulation;
                        OrderSimulationMV newOSMVModel = orderSimulationMV;
                        List<OrderSimulationInterest> newOSIModel = orderSimulationInterest;
                        List<OrderSimulationCoverage> newOSCModel = orderSimulationCoverage;

                        prospectCustomer.SalesOfficerID = prospectCustomer.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", prospectCustomer.SalesOfficerID) : prospectCustomer.SalesOfficerID;
                        followUp.SalesOfficerID = followUp.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", followUp.SalesOfficerID) : followUp.SalesOfficerID;

                        newOSModel.OrderNo = System.Guid.NewGuid().ToString();
                        if (!string.IsNullOrEmpty(newOSModel.CustID))
                        {

                            newOSModel.CustID = prospectCustomer.CustID;
                            newOSModel.QuotationNo = null;
                            newOSModel.LastUpdatedTime = null;
                            newOSModel.ApplyF = true;
                            newOSModel.SendF = true;
                            newOSModel.SendStatus = 0;
                            newOSModel.SendDate = null;
                            newOSModel.SalesOfficerID = newOSModel.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", newOSModel.SalesOfficerID) : newOSModel.SalesOfficerID;

                            query = @";INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25)";
                            db.Execute(query, newOSModel.OrderNo
          , newOSModel.CustID
          , newOSModel.FollowUpNo
          , newOSModel.QuotationNo
          , newOSModel.MultiYearF
          , newOSModel.YearCoverage
          , newOSModel.TLOPeriod
          , newOSModel.ComprePeriod
          , newOSModel.BranchCode
          , newOSModel.SalesOfficerID
          , newOSModel.PhoneSales
          , newOSModel.DealerCode
          , newOSModel.SalesDealer
          , newOSModel.ProductTypeCode
          , newOSModel.AdminFee
          , newOSModel.TotalPremium
          , newOSModel.Phone1
          , newOSModel.Phone2
          , newOSModel.Email1
          , newOSModel.Email2
          , newOSModel.SendStatus
          , newOSModel.InsuranceType
          , newOSModel.ApplyF
          , newOSModel.SendF
          , newOSModel.LastInterestNo
          , newOSModel.LastCoverageNo);

                            //Copy OSMV
                            newOSMVModel.OrderNo = newOSModel.OrderNo;
                            newOSMVModel.LastUpdatedTime = null;
                            newOSMVModel.FieldChanged = "";
                            query = @" INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)";
                            db.Execute(query, newOSMVModel.OrderNo
      , newOSMVModel.ObjectNo
      , newOSMVModel.ProductTypeCode
      , newOSMVModel.VehicleCode
      , newOSMVModel.BrandCode
      , newOSMVModel.ModelCode
      , newOSMVModel.Series
      , newOSMVModel.Type
      , newOSMVModel.Sitting
      , newOSMVModel.Year
      , newOSMVModel.CityCode
      , newOSMVModel.UsageCode
      , newOSMVModel.SumInsured
      , newOSMVModel.AccessSI
      , newOSMVModel.RegistrationNumber
      , newOSMVModel.EngineNumber
      , newOSMVModel.ChasisNumber
      , newOSMVModel.LastUpdatedTime
      , newOSMVModel.RowStatus
      , newOSMVModel.IsNew);

                            //Copy OrderSimulationInterest
                            foreach (OrderSimulationInterest osimodel in newOSIModel)
                            {
                                osimodel.OrderNo = newOSModel.OrderNo;
                                query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                                db.Execute(query, osimodel.OrderNo, osimodel.ObjectNo, osimodel.InterestNo, osimodel.InterestID, osimodel.Year, osimodel.Premium, osimodel.PeriodFrom, osimodel.PeriodTo);

                            }
                            foreach (OrderSimulationCoverage oscmodel in newOSCModel)
                            {

                                oscmodel.OrderNo = newOSModel.OrderNo;
                                query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12)";
                                db.Execute(query, oscmodel.OrderNo, oscmodel.ObjectNo, oscmodel.InterestNo, oscmodel.CoverageNo, oscmodel.CoverageID, oscmodel.Rate, oscmodel.SumInsured, oscmodel.LoadingRate, oscmodel.Loading, oscmodel.Premium, oscmodel.BeginDate, oscmodel.EndDate, oscmodel.IsBundling ? 1 : 0);

                            }

                        }



                    }
                    return Json(new { status = true, message = "OK", orderSimulation, orderSimulationMV, orderSimulationInterest, orderSimulationCoverage, prospectCustomer, followUp }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult getTPLSIList(FormDataCollection form)
        {

            string ProductCode = form.Get("ProductCode").Trim();

            if (ProductCode == null || ProductCode == "")
            {
                return Json(new { status = false, message = "Parameter is null", data = "null" });
            }
            var query = @"SELECT DISTINCT Product_Code AS ProductCode,Coverage_Id AS CoverageId,BC_Up_To_SI AS TSI FROM Mapping_Cover_Progressive WHERE Product_Code =@0";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query, ProductCode);
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        private string calculateAgainPremi()
        {
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;
            PADRVRPremi = 0;
            PAPASSPremi = 0;

            foreach (CalculatedPremi cp in CalculatedPremiItems)
            {
                if (cp.CoverageID.TrimEnd().Equals("SRCC") || cp.CoverageID.TrimEnd().Equals("SRCTLO"))
                {
                    IsSRCCChecked = true;
                    SRCCPremi += cp.Premium;
                }
                if (cp.CoverageID.TrimEnd().Equals("FLD") || cp.CoverageID.TrimEnd().Equals("FLDTLO"))
                {
                    IsFLDChecked = true;
                    FLDPremi += cp.Premium;
                }
                if (cp.CoverageID.TrimEnd().Equals("ETV") || cp.CoverageID.TrimEnd().Equals("ETVTLO"))
                {
                    IsETVChecked = true;
                    ETVPremi += cp.Premium;
                }
            }


            //Load previous non basic Items
            RefreshPremi();
            CalculateLoading();
            CalculateTotalPremi();
            return CheckSumInsured();
        }

        [HttpPost]
        public IHttpActionResult saveTokenPushNotif(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string username = form.Get("username").Trim();
                string token = form.Get("token");



                if (string.IsNullOrEmpty(token))
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }
                var query = @";IF EXISTS(SELECT * FROM TokenPushNotifReact WHERE Username = @0)
BEGIN
UPDATE TokenPushNotifReact SET
       TokenPushNotif=@1
      ,LastUpdatedTime=GETDATE() WHERE Username=@0
END
    ELSE
BEGIN
INSERT INTO TokenPushNotifReact (Username
      ,TokenPushNotif
      ,EntryDate
      ,LastUpdatedTime
      ) VALUES(@0,@1,GETDATE(),GETDATE())
END";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    username = username.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", username) : username;

                    db.Execute(query, username, token);

                    return Json(new { status = true, message = "OK" }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }


        }

        [HttpPost]
        public IHttpActionResult SendReportEmail(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Email = form.Get("Email");
            Email = Email.Replace("null", "");
            string reportType = form.Get("ReportType");
            string salesOfficerID = form.Get("salesOfficerID");
            DateTime periodFrom = Convert.ToDateTime(form.Get("period"));
            DateTime periodTo = periodFrom.AddMonths(1).AddDays(1 - periodFrom.Day).Date;
            string userName = "";
            string subject = "";
            string emailsubject = "";
            string filename = "";
            string filePath = "";
            string displayName = "";
            string emailText = "";
            string UserNameNotificationDisplay = ConfigurationManager.AppSettings["UserNameNotificationDisplay"].ToString();
            List<A2isMessagingAttachment> attFiles = new List<A2isMessagingAttachment>();

            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                salesOfficerID = salesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", salesOfficerID) : salesOfficerID;

                userName = db.ExecuteScalar<string>("SELECT Name FROM SalesOfficer WHERE SalesOfficerID=@0 ", salesOfficerID);
                string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();
                if (reportType == "REPORTOFFICER")
                {
                    emailsubject = ConfigurationManager.AppSettings["EmailSubjectSummary"].ToString();
                    subject = emailsubject + " - " + userName + " - " + periodFrom.ToString("dd/MM/yyyy") + " - " + periodTo.AddDays(-1).ToString("dd/MM/yyyy");
                    filename = emailsubject + " - " + userName + " - " + periodFrom.ToString("dd-MM-yyyy") + " - " + periodTo.AddDays(-1).ToString("dd-MM-yyyy");
                    attFiles.Add(GenerateReportSummaryFile(salesOfficerID, periodFrom, periodTo, filename, "ReportOfficer"));
                    displayName = UserNameNotificationDisplay;
                    emailText = "Terlampir Prospect Summary milik " + userName + ". <br /><br />Terimakasih, semoga membantu kita semua dalam bekerja.";
                }
                else if (reportType == "REPORTMANAGER")
                {
                    emailsubject = ConfigurationManager.AppSettings["EmailSubjectDashboard"].ToString();
                    subject = emailsubject + " - " + userName + " - " + periodFrom.ToString("MMMM yyyy") + " - " + DateTime.Now.ToString("dd/MM/yyyy");
                    filename = "[" + emailsubject + "] [" + periodFrom.ToString("yyyy-MM") + "] [" + DateTime.Now.ToString("yyyy-MM-dd") + "]";
                    attFiles.Add(GenerateReportSummaryFile(salesOfficerID, periodFrom, periodTo, filename, "ReportManager"));
                    displayName = UserNameNotificationDisplay;
                    emailText = "Terlampir Report Dashboard Sales milik " + userName + ". <br /><br />Terimakasih, semoga membantu kita semua dalam bekerja.";
                }
                else if (reportType == "REPORTMANAGERYTD")
                {

                    emailsubject = ConfigurationManager.AppSettings["EmailSubjectDashboard"].ToString();
                    subject = emailsubject + " - " + userName + " - " + periodFrom.ToString("yyyy") + " - " + DateTime.Now.ToString("dd/MM/yyyy");
                    filename = "[" + emailsubject + "] [" + periodFrom.ToString("yyyy") + "] [" + DateTime.Now.ToString("yyyy-MM-dd") + "]";
                    attFiles.Add(GenerateReportSummaryFile(salesOfficerID, periodFrom, periodTo, filename, "ReportManagerYTD"));
                    displayName = UserNameNotificationDisplay;
                    emailText = "Terlampir Report Dashboard Sales milik " + userName + ". <br /><br />Terimakasih, semoga membantu kita semua dalam bekerja.";
                }

                A2isCommonResult rslt = MessageController.SendEmail(Email, emailText, subject, attFiles);

                if (rslt.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + rslt.ResultDesc });

                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        private A2isMessagingAttachment GenerateReportSummaryFile(string salesOfficerID, DateTime periodFrom, DateTime periodTo, string fileName, string reportType)
        {
            string filePath = "";
            A2isMessagingAttachment att = new A2isMessagingAttachment();
            string ReportSummaryPath = ConfigurationManager.AppSettings["ReportSummaryPath"].ToString();
            string ReportDashboardPath = ConfigurationManager.AppSettings["ReportDashboardPath"].ToString();
            string ReportDashboardYTDPath = ConfigurationManager.AppSettings["ReportDashboardYTDPath"].ToString();
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";


            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;

            NetworkCredential NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);


            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;

                int totalParam = 0;
                string reportPath = "";
                string paramNameID = "";

                if (reportType == "ReportOfficer")
                {
                    totalParam = 3;
                    reportPath = ReportSummaryPath;
                    paramNameID = "SalesOfficerID";
                }
                else if (reportType == "ReportManager")
                {
                    totalParam = 2;
                    reportPath = ReportDashboardPath;
                    paramNameID = "UserID";
                }
                else if (reportType == "ReportManagerYTD")
                {
                    totalParam = 2;
                    reportPath = ReportDashboardYTDPath;
                    paramNameID = "UserID";
                }

                using (ReportViewer rView = new ReportViewer())
                {
                    rView.ProcessingMode = ProcessingMode.Remote;

                    ServerReport serverReport = rView.ServerReport;
                    serverReport.ReportServerUrl = new Uri(ReportServerURL);
                    serverReport.ReportPath = reportPath;
                    serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;

                    //ReportParameter[] parameters = new ReportParameter[3];
                    ReportParameter[] reportParam = new ReportParameter[totalParam];
                    reportParam[0] = new ReportParameter();
                    reportParam[0].Name = paramNameID;
                    reportParam[0].Values.Add(salesOfficerID);

                    reportParam[1] = new ReportParameter();
                    reportParam[1].Name = "PeriodFrom";
                    reportParam[1].Values.Add(periodFrom.ToString());

                    if (reportType == "ReportOfficer")
                    {
                        reportParam[2] = new ReportParameter();
                        reportParam[2].Name = "PeriodTo";
                        reportParam[2].Values.Add(periodTo.ToString());
                    }

                    rView.ServerReport.SetParameters(reportParam);
                    bytes = rView.ServerReport.Render("EXCEL", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

                    filePath = ReportFiles + fileName + ".xls"; //"QUOTATION-" + OrderNo + ".pdf";

                    if (File.Exists(filePath))
                        File.Delete(filePath);
                    // 1. Read lock information from file   2. If locked by us, delete the file
                    //using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Delete))
                    //{
                    //    File.Delete(filePath);
                    //}

                    //using (FileStream fs = File.Create(filePath)) { }

                    att.FileByte = bytes;
                    att.FileContentType = "application/xls";
                    att.FileExtension = ".xls";
                    att.AttachmentDescription = fileName;
                }


            }
            catch (Exception ex)
            {
                filePath = "";
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();

            }

            return att;

        }

        [HttpPost]
        public IHttpActionResult ResetVehicleCode(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string vehicleCode = form.Get("vehicleCode");
            try
            {
                string query = @"SELECT a.VehicleCode, a.BrandCode, a.ProductTypeCode, a.ModelCode,  a.Series, 
                              a.Type, a.Sitting, a.Year, a.CityCode, a.Price, 
                              a.LastUpdatedTime, a.RowStatus FROM Vehicle a 
                              LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                              a.BrandCode=b.BrandCode 
                              AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                              AND a.Series=b.Series AND a.Type=b.Type 
                              AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                              AND a.Year > b.Year WHERE b.Year IS NULL And UPPER(a.VehicleCode) = UPPER('" +
                        vehicleCode + "') ORDER BY a.VehicleCode";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                Vehicle vehicle = db.FirstOrDefault<Vehicle>(query);
                if (vehicle != null)
                {
                    return Json(new { status = true, message = "OK", data = vehicle });
                }
                else
                {
                    return Json(new { status = false, message = "Data Not Available" });
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult CheckPhoneExist(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Phone = form.Get("Phone");
            string CustId = form.Get("CustId");
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                bool phoneexist = Convert.ToBoolean(db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM ProspectCustomer WHERE Phone1=@0 AND CustID !=@1", Phone, CustId));
                string message = phoneexist ? "Phone number already exist. Are you sure to continue?" : "";
                int code = phoneexist ? 001 : 002;
                return Json(new { status = true, message = message, code = code });

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



    }
}
