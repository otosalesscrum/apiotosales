﻿using Otosales.Infrastructure;
using Otosales.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Configuration;
using System.Web.Http;

namespace Otosales.Controllers.vWeb
{
    public class DataController : OtosalesBaseController
    {
        [HttpPost]
        public IHttpActionResult GetDb()
        {
            string fileName = WebConfigurationManager.AppSettings["MobileDBFileName"];
            string localFilePath = Settings.Default.MobileDBPath + @"\" + fileName;
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StreamContent(new FileStream(localFilePath, FileMode.Open, FileAccess.Read));
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;
            return base.ResponseMessage(response);
        }
    }
}
