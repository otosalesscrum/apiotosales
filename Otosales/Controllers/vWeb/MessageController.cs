﻿using a2is.Framework.Monitoring;
using A2isMessaging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Otosales.Controllers.vWeb
{
    public class MessageController : ApiController
    {

        private static readonly a2isLogHelper _log = new a2isLogHelper();

        public static A2isCommonResult SendEmail(string CustomerMail, string body, string subject,List<A2isMessagingAttachment> att)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try{
            CultureInfo ID_Culture = new CultureInfo("id-ID");
           


            A2isMessageParam mParam = new A2isMessageParam();
            mParam.MessageFrom = "admin-noreply@asuransiastra.com";
            mParam.ModuleRequest = "SendEmail";
            mParam.ApplicationRequest = "Otosales";
            mParam.MessageSchedule = DateTime.Now.ToString(A2isMessaging.A2isCommonStatic.FormatDateTimeUI);
            mParam.MessageType = "MAIL";
            mParam.MessageDisplayName = "Garda Mobile Otosales";
            mParam.MessageTo = CustomerMail.Replace(" ","");
            mParam.MessageSubject = subject;
            mParam.MessageBody = body;
            mParam.MessagePriority = 0;
            foreach (A2isMessagingAttachment item in att)
            {

                mParam.MessageAttachment.Add(item);
            }

       
                A2isCommonResult result = Messaging.ProcessQueuing(mParam, Convert.ToBoolean(ConfigurationManager.AppSettings["isDirectSendEmail"]));
                return result;
            }
            catch (Exception Ex)
            {
                _log.Error(string.Format("Function {0} Error : {1}", CurrentMethod, Ex));
                return new A2isCommonResult { ResultCode = false, ResultDesc = Ex.ToString() };
            }

        }
   
        public static string SendSMS(string message, string phoneNumber)
        {
            #region InfoBip
            string maskingName = ConfigurationManager.AppSettings["a2isMessagingMaskingName"].ToString();
            if (string.IsNullOrEmpty(maskingName))
                maskingName = "ASURANSIASTRA";

            A2isMessageParam mParam = new A2isMessageParam();
            mParam.ApplicationRequest = "Otosales";
            mParam.ModuleRequest = "InfoQuotation";
            mParam.MessageSchedule = DateTime.Now.ToString(DateTime.Now.ToString(A2isMessaging.A2isCommonStatic.FormatDateTimeUI));
            mParam.MessageType = "SMS";
            mParam.MessageFrom = maskingName; //"GARDA OTO"; // sender SMS, harus ada di tabel a2is messaging masking
            mParam.MessageTo = phoneNumber; //no tujuan SMS
            mParam.MessageBody = message;
            A2isCommonResult mResult = Messaging.ProcessQueuing(mParam, false);
            return mResult.ResultCode ? mResult.ResultObject : (mResult.ResultExMessage != null ? mResult.ResultExMessage.Substring(0, 50) : "ResultExMessage null");

            #endregion
        }
    }
}
