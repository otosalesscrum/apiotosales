﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.dta;
using Otosales.Infrastructure;
using Otosales.Models;
using Otosales.Models.v0213URF2019;
using Otosales.Models.vWeb2;
using Otosales.Repository.v0213URF2019;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Otosales.Controllers.v0213URF2019
{
    [AutoLoggingReference]
    public class DataReactController : OtosalesBaseController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();
        #region Properties
        IMobileRepository mobileRepository;

        public DataReactController()
        {
            mobileRepository = new MobileRepository();
        }
        public DataReactController(IMobileRepository repository)
        {
            mobileRepository = repository;
        }
        #endregion
        [HttpPost]
        public IHttpActionResult sendToSARenewalNonRenNot(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string PolicyNo = form.Get("PolicyNo");
            string FollowUpNo = form.Get("FollowUpNo");
            string RemarkToSA = form.Get("RemarksToSA");
            string SalesOfficerID = form.Get("SalesOfficerID");
            int isPTSB = Convert.ToInt32(form.Get("isPTSB"));
            #endregion

            //Generate GUID 
            string custID = System.Guid.NewGuid().ToString();
            string followUpID = String.IsNullOrEmpty(FollowUpNo) ? System.Guid.NewGuid().ToString() : FollowUpNo;
            string orderID = System.Guid.NewGuid().ToString();

            if (string.IsNullOrEmpty(PolicyNo) || string.IsNullOrEmpty(FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            try
            {
                #region Check Double Insured
                Otosales.Models.vWeb2.CheckDoubleInsuredResult dblins = MobileRepository.GetCheckDoubleInsuredDataKotor(PolicyNo);
                if (dblins.IsDoubleInsured)
                {
                    return Json(new { status = false, message = "Double Insured.", data = dblins });
                }
                #endregion
                MobileRepository.GenerateOrderSimulation(SalesOfficerID, custID, followUpID, orderID, PolicyNo, RemarkToSA, isPTSB);

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", orderID });
            }
            catch (Exception e)
            {
                MobileRepository.ClearFailedData(custID, followUpID, orderID);
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveFollowUpStatusDetails(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string FUData = form.Get("FollowUp");
                string Editor = form.Get("editor");
                FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);

                string FULasData = form.Get("FollowUpLast");
                FollowUp FULast = JsonConvert.DeserializeObject<FollowUp>(FULasData);

                bool isInfoChanged = false;

                bool isSentToSA = bool.Parse(form.Get("isSentToSA").Trim());
                string STATE = form.Get("STATE");
                string TRName = form.Get("TRName");

                #region MGO BID
                if (STATE.Equals("MGOBID"))
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        string query = @";SELECT TOP 1 * FROM Quotation.dbo.HistoryPenawaran 
WHERE OrderNo = (SELECT TOP 1 OrderNo FROM OrderSimulation 
WHERE FollowUpNo =@0 ORDER BY EntryDate DESC)";

                        HistoryPenawaran hp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);
                        hp.IsAbleExpired = 0;

                        bool responsestatus = false;

                        // check all mgo bid if expired bid still exist
                        Util.CheckAllMGOBidExpiry();

                        using (var dbagent = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                        {
                            // check data not expired
                            int bidExist = dbagent.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", hp.HistoryPenawaranID);

                            if (bidExist == 1)
                            {
                                // permanently set data to agent
                                dbagent.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);
                                responsestatus = true;
                            }

                            if (responsestatus)
                            {

                                #region execUpdateFollowUp
                                #region UPDATE FOLLOW UP
                                if (FU.FollowUpInfo != FULast.FollowUpInfo)
                                {
                                    isInfoChanged = true;


                                }
                                query = @";UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6,[Receiver]=@7 WHERE [FollowUpNo]=@0";

                                db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1, FU.Receiver);

                                #endregion

                                if (isInfoChanged)
                                {
                                    #region INSERT FOLLOW UP HISTORY
                                    query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,GETDATE(),@10,@11,@12,@13,GETDATE(),0)
";
                                    int lastseqnofuh = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                    db.Execute(query, FU.FollowUpNo, lastseqnofuh, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                                    #endregion
                                    MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);

                                }
                                string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                                #endregion

                                int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                db.Execute("UPDATE Quotation.dbo.HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);

                                query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                                MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = message, data = db.FirstOrDefault<FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());

                            }
                            else
                            {
                                query = @";SELECT * FROM OrderSimulation  WHERE FollowUpNo = @0";
                                List<OrderSimulation> orderSimulation = db.Fetch<OrderSimulation>(query, FU.FollowUpNo);

                                query = @";SELECT * FROM Quotation.dbo.HistoryPenawaran hp 
JOIN OrderSimulation os ON os.OrderNo = hp.OrderNo 
JOIN FollowUp fu On fu.FollowUpNo = os.FollowUpNo  WHERE fu.FollowUpNo = @0";

                                HistoryPenawaran hptemp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);

                                foreach (OrderSimulation os in orderSimulation)
                                {
                                    string queryDeleted = @";UPDATE OrderSimulationCoverage SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationInterest SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationMV SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulation SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);

                                }

                                string queryDelete = @";UPDATE FollowUpHistory SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE FollowUp SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE ProspectCustomer SET RowStatus=0 WHERE CustID=@0";
                                db.Execute(queryDelete, FU.CustID);
                                queryDelete = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE HistoryPenawaranID=@0";
                                db.Execute(queryDelete, hptemp.HistoryPenawaranID);

                                int hpCount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM HistoryPenawaran WHERE PolicyID = @0", hp.PolicyID);
                                if (hpCount == 0)
                                {
                                    queryDelete = @";UPDATE Quotation.dbo.Penawaran SET RowStatus=0 WHERE PolicyID=@0";
                                    db.Execute(queryDelete, hptemp.HistoryPenawaranID);
                                }

                                MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = "Unable to process your request. This prospect has been expired" }, Util.jsonSerializerSetting());
                            }

                        }
                    }

                }
                #endregion

                else
                {
                    # region execUpdateFollowUp
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        #region UPDATE FOLLOW UP
                        if (FU.FollowUpInfo != FULast.FollowUpInfo)
                        {
                            isInfoChanged = true;
                        }
                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
BEGIN
SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
END
ELSE
BEGIN
SELECT 0 AS SeqNo
END", FU.FollowUpNo) + 1;

                        string query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6, [FollowUpNotes]=@7,[PICWA]=@8,[Receiver]=@9 WHERE [FollowUpNo]=@0";

                        db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, lastseqno, FU.FollowUpNotes, FU.PICWA, FU.Receiver);

                        if (FU.FollowUpStatus == 5) {
                            string PolicyOrderNo = "";
                            query = @"SELECT COALESCE(PolicyOrderNo,'') FROM dbo.OrderSimulation WHERE FollowUpNo = @0 AND CustID = @1 AND ApplyF = 1 AND RowStatus = 1";
                            PolicyOrderNo = db.ExecuteScalar<string>(query, FU.FollowUpNo, FU.CustID);
                            if (!string.IsNullOrEmpty(PolicyOrderNo)) {
                                query = "UPDATE dbo.Mst_Order set Order_Status = '6' WHERE Order_No = @0";
                                var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                                aabdb.Execute(query, PolicyOrderNo);
                            }
                        }
                        #endregion

                        if (isInfoChanged)
                        {
                            #region INSERT FOLLOW UP HISTORY
                            query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,GETDATE(),@9,@10,@11,@12,GETDATE(),0)
";

                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FULast.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                            #endregion
                            MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);

                        }
                        string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                        query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                        MobileRepository.InsertMstOrderMobile(FU);
                        return Json(new { status = true, message = message, data = db.FirstOrDefault<FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                    }
                    #endregion
                }



            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult fetchDetailApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            OtosalesAPIResult response = new OtosalesAPIResult();
            try
            {
                string OrderID = form.Get("OrderID");
                string OrderNo = form.Get("OrderNo");
                string ApprovalType = form.Get("ApprovalType");
                string Role = form.Get("Role");
                response.ResponseCode = "1";
                response.Message = "Success!";
                switch (ApprovalType)
                {
                    case "COMSAMD":
                        {
                            response = MobileRepository.detailApprovalComission(OrderID, Role, OrderNo);
                        }
                        break;
                    case "KOMISI":
                        {
                            response = MobileRepository.detailApprovalComission(OrderID, Role, OrderNo);
                        }
                        break;
                    case "ADJUST":
                        {
                            response = MobileRepository.detailApprovalAdjustment(OrderID, OrderNo);
                        }
                        break;
                    case "NEXTLMT":
                        {
                            response = MobileRepository.detailApprovalTsiLimit(OrderID, OrderNo);
                        }
                        break;
                    default:
                        {
                            response.Status = false;
                            response.Message = "Unknown Order Type";
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = ex.Message;
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult GetApplicationParameter(FormDataCollection form) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string ParamName = form.Get("ParamName");
            #endregion

            if (string.IsNullOrEmpty(ParamName))
                return Json(new { status = false, message = "Param can't be null" });
            try
            {
                List<string> data = mobileRepository.GetApplicationParametersValue(ParamName);
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success",data});
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult logErrorUI(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            OtosalesAPIResult response = new OtosalesAPIResult();
            try
            {
                string ErrorInfo = form.Get("ErrorInfo");
                string UserLogin = form.Get("UserLogin");
                response.ResponseCode = "1";
                response.Message = "Success!";

                response.Status = MobileRepository.loggingOtosalesUI(ErrorInfo, UserLogin);

                return Json(response, Util.jsonSerializerSetting());

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = ex.Message;
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult GetPaymentInfo(FormDataCollection form) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                if (string.IsNullOrEmpty(CustID) && string.IsNullOrEmpty(FollowUpNo))
                    return Json(new { status = false, message = "Param can't be null" });

                Otosales.Models.vWeb2.PaymentInfo res = MobileRepository.GetPaymentInfo(CustID, FollowUpNo);
                return Json(new { status = true, message = "Update Success.", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult ReactiveVA(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string VANumber = form.Get("VANumber");
                string PolicyOrderNo = form.Get("PolicyOrderNo");
                if (string.IsNullOrEmpty(VANumber))
                    return Json(new { status = false, message = "Param can't be null" });

                bool res = MobileRepository.ReactiveVA(VANumber, PolicyOrderNo);
                return Json(new { status = res, message = "Reactive Success."});
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
        [HttpPost]
        public IHttpActionResult getTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                #endregion
                Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
                try
                {
                    result = MobileRepository.GetTaksListDetail(CustID, FollowUpNo);
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateTaskDetail(UpdateTaskDetailParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string CustID = param.CustID;
            string FollowUpNo = param.FollowUpNo;
            PersonalData PersonalData = param.PersonalData;
            CompanyData CompanyData = param.CompanyData;
            VehicleData VehicleData = param.VehicleData;
            PolicyAddress policyAddress = param.policyAddress;
            SurveySchedule surveySchedule = param.surveySchedule;
            List<ImageDataTaskDetail> imageData = param.imageData;
            OrderSimulationModel OSData = param.OSData;
            OrderSimulationMVModel OSMVData = param.OSMVData;
            CalculatePremiModel calculatedPremiItems = param.calculatedPremiItems;
            string FollowUpStatus = param.FollowUpStatus;
            Remarks Remarks = param.Remarks;
            #endregion

            if (string.IsNullOrEmpty(param.CustID) || string.IsNullOrEmpty(param.FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            string OrderNo = "";
            try
            {
                OrderNo = MobileRepository.UpdateTaskListDetail(CustID, FollowUpNo, PersonalData,
                    CompanyData, VehicleData, policyAddress, surveySchedule, imageData, OSData, OSMVData,
                    calculatedPremiItems, FollowUpStatus, Remarks);
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", data = new { OrderNo, PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo() } });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult CheckDoubleInsured(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string ChasisNo = form.Get("ChasisNo");
                    string EngineNo = form.Get("EngineNo");
                    string OrderNo = form.Get("OrderNo");
                    if (string.IsNullOrEmpty(ChasisNo) && string.IsNullOrEmpty(EngineNo))
                    {
                        return BadRequest("Input Required");
                    }

                    CheckDoubleInsuredResult res = MobileRepository.GetCheckDoubleInsured(ChasisNo, EngineNo, OrderNo);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

    }
}
