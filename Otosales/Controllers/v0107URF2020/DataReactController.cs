﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Models;
using PremiumCalculation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using Otosales.Repository.v0107URF2020;
using System.Text.RegularExpressions;
using A2isMessaging;
using System.Globalization;

namespace Otosales.Controllers.v0107URF2020
{
    public class DataReactController : ApiController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();
        #region Properties
        IMobileRepository mobileRepository;
        IAABRepository aabRepository;

        public DataReactController()
        {
            mobileRepository = new MobileRepository();
            aabRepository = new AABRepository();
        }
        public DataReactController(IMobileRepository repository)
        {
            mobileRepository = repository;
        }
        public DataReactController(IAABRepository repository)
        {
            aabRepository = repository;
        }

        #endregion
        #region Rate Calculation Global Variable
        private string ProductCode = "";
        private string CityCode = "";
        private string type = "";
        private string UsageCode = "";
        private int Year = 0;

        private bool IsSRCCChecked = false;
        private bool IsFLDChecked = false;
        private bool IsETVChecked = false;
        private bool IsTSChecked = false;
        private bool IsTPLChecked = false;
        private bool IsPAPASSChecked = false;
        private bool IsPADRVRChecked = false;
        private bool IsTPLSIEnabled = false;
        private bool IsPASSEnabled = false;
        private bool IsPAPASSSIEnabled = false;
        private bool IsPADRVRSIEnabled = false;
        private bool IsACCESSChecked = false;
        private bool IsACCESSSIEnabled = false;


        private bool IsSRCCEnabled = false;
        private bool IsETVEnabled = false;
        private bool IsFLDEnabled = false;
        private bool IsTSEnabled = false;
        private bool IsPAPASSEnabled = false;
        private bool IsPADRVREnabled = false;
        private bool IsACCESSEnabled = false;
        private bool IsTPLEnabled = false;

        private string TPLCoverageID = "";
        private double TPLSI = 0;
        private double PADRVRSI = 0;
        private double PAPASSSI = 0;
        private int PASS = 0;
        private double AccessSI = 0;

        private double BasicPremi = 0;
        private double SumInsured = 0;
        private double SRCCPremi = 0;
        private double FLDPremi = 0;
        private double ETVPremi = 0;
        private double TSPremi = 0;
        private double ACCESSPremi = 0;
        private double TPLPremi = 0;
        private double PADRVRPremi = 0;
        private double PAPASSPremi = 0;
        private double LoadingPremi = 0;
        private double TotalPremi = 0;
        private double GrossPremi = 0;
        private double AdminFee = 0;
        private double VehiclePrice = 0;


        List<Otosales.Models.CalculatedPremi> CalculatedPremiItems = new List<Otosales.Models.CalculatedPremi>();
        List<Otosales.Models.CalculatedPremi> InterestCoverageItems = new List<Otosales.Models.CalculatedPremi>();
        List<Otosales.Models.Prd_Load_Mv> loadingMaster = new List<Otosales.Models.Prd_Load_Mv>();
        List<Otosales.Models.Prd_Agreed_Value> prdAgreedValueMaster = new List<Otosales.Models.Prd_Agreed_Value>();
        List<Otosales.Models.CoveragePeriod> coveragePeriodItems = new List<Otosales.Models.CoveragePeriod>();
        List<Otosales.Models.Prd_Interest_Coverage> prdInterestCoverage = new List<Otosales.Models.Prd_Interest_Coverage>();


        Otosales.Models.VehiclePriceTolerance vptModel;
        Otosales.Models.ProspectCustomer pcModel;
        List<Otosales.Models.OrderSimulationInterest> osiDeletedModels;
        List<Otosales.Models.OrderSimulationCoverage> oscDeletedModels;
        List<Otosales.Models.CoveragePeriodNonBasic> coveragePeriodNonBasicItems = new List<Otosales.Models.CoveragePeriodNonBasic>();


        #endregion

        [HttpPost]
        public IHttpActionResult getProductCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string insurancetype = form.Get("insurancetype").Trim();
                string salesofficerid = form.Get("salesofficerid").Trim();
                int isRenew = string.IsNullOrEmpty(form.Get("isRenewal")) ? 0 : Convert.ToInt32(form.Get("isRenewal"));
                string isMVGodig = form.Get("isMvGodig");
                string isNew = form.Get("isNew");
                string searchParam = form.Get("searchParam");

                List<Models.v0107URF2020.Product> result = new List<Models.v0107URF2020.Product>();
                if (string.IsNullOrEmpty(insurancetype))
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                bool flagIsNew = false;
                if (!string.IsNullOrEmpty(isNew))
                {
                    flagIsNew = Boolean.Parse(isNew);
                }

                result = mobileRepository.GetProduct(insurancetype, salesofficerid, searchParam, isRenew, flagIsNew, flagIsMvGodig);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult BasicPremiCalculation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            #region declare param
            List<DetailScoring> dtlScoring = new List<DetailScoring>();
            List<decimal> RenDiscountPct = new List<decimal>();
            List<decimal> DiscountPct = new List<decimal>();
            int comprePeriod = Convert.ToInt32(form.Get("comprePeriod"));
            int TLOPeriod = Convert.ToInt32(form.Get("TLOPeriod"));
            string vPeriodFrom = form.Get("PeriodFrom");
            string vPeriodTo = form.Get("PeriodTo");
            // string vPeriodFromCover = form.Get("PeriodFromCover");
            // string vPeriodToCover = form.Get("PeriodToCover");
            string vtype = form.Get("vtype");
            string vcitycode = form.Get("vcitycode");
            string vusagecode = form.Get("vusagecode");
            string vyear = form.Get("vyear");
            string vsitting = form.Get("vsitting");
            ProductCode = form.Get("vProductCode");
            string vMouID = form.Get("vMouID");
            decimal vTSInterest = Convert.ToDecimal(form.Get("vTSInterest"));
            decimal vPrimarySI = Convert.ToDecimal(form.Get("vPrimarySI"));
            string vCoverageId = form.Get("vCoverageId");
            string vInterestId = "CASCO";//form.Get("vInterestId");
            string vModel = form.Get("vModel");
            string vBrand = form.Get("vBrand");
            string vIsNew = form.Get("isNew");
            string pQuotationNo = form.Get("pQuotationNo");
            string Ndays = form.Get("Ndays");
            string OldPolicyNo = form.Get("OldPolicyNo");
            //int calcmethod = Convert.ToInt16(form.Get("calcMethod"));
            if (string.IsNullOrEmpty(Ndays) || string.IsNullOrEmpty(vCoverageId) || string.IsNullOrEmpty(ProductCode) || string.IsNullOrEmpty(vusagecode) || string.IsNullOrEmpty(vcitycode) || string.IsNullOrEmpty(vPeriodFrom)
                || string.IsNullOrEmpty(vPeriodTo))
            {
                return Json(new
                {
                    status = false,
                    message = "Param Not Completed"
                });
            }
            DateTime PeriodFrom = DateTime.ParseExact(vPeriodFrom, "yyyy-MM-dd", null);
            DateTime PeriodTo = DateTime.ParseExact(vPeriodTo, "yyyy-MM-dd", null);
            DateTime PeriodFromCover = PeriodFrom;
            DateTime PeriodToCover = PeriodTo;
            List<CalculatedPremi> result = new List<CalculatedPremi>();
            #region set vehicleyear to year now if not choose vehicle
            if (Convert.ToInt16(vyear) == 0)
            {
                vyear = Convert.ToString(DateTime.Now.Year);
            }

            #endregion
            #endregion

            try
            {
                #region check product code is valid in mapping segment
                if (Repository.vWeb2.MobileRepository.isValidMappingSegment(ProductCode))
                {
                    var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    #region Initialize Enable Coverage
                    InitializeEnableCoverage();
                    #endregion
                    #region initialize Param

                    List<CoverageParam> CoverageList = new List<CoverageParam>();
                    if (comprePeriod == 0 && TLOPeriod == 0)
                    {
                        CoverageList.Add(new CoverageParam { CoverageId = vCoverageId, PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });
                    }
                    else
                    {
                        if (comprePeriod > 0 && TLOPeriod > 0)
                        {
                            CoverageList.Add(new CoverageParam { CoverageId = "ALLRIK", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodFromCover.AddYears(comprePeriod) });
                            CoverageList.Add(new CoverageParam { CoverageId = "TLO", PeriodFromCover = PeriodFromCover.AddYears(comprePeriod), PeriodToCover = PeriodFromCover.AddYears(comprePeriod + TLOPeriod) });

                        }
                        else if (comprePeriod > 0)
                        {
                            CoverageList.Add(new CoverageParam { CoverageId = "ALLRIK", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });

                        }
                        else if (TLOPeriod > 0)
                        {
                            CoverageList.Add(new CoverageParam { CoverageId = "TLO", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });

                        }
                    }
                    #endregion
                    bool isProductSupported = true;
                    CalculatedPremiItems = mobileRepository.CalculateBasicPremi(CoverageList, ProductCode, vMouID, vcitycode, vusagecode, vBrand, vtype, vModel, vIsNew, vsitting, vyear, PeriodFrom, PeriodTo, Ndays, vTSInterest, vPrimarySI, vInterestId, OldPolicyNo, out RenDiscountPct, out isProductSupported);
                    if (!isProductSupported)
                    {
                        return Json(new
                        {
                            status = true,
                            message = "Product Is Not Supported!"
                        });

                    }
                    #endregion

                    #region recalculate extended
                    DetailScoring ds = new DetailScoring();
                    if (!string.IsNullOrEmpty(vcitycode))
                    {
                        ds.FactorCode = "GEOGRA";
                        ds.InsuranceCode = vcitycode;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vusagecode))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "MVUSAG";
                        ds.InsuranceCode = vusagecode;
                        dtlScoring.Add(ds);
                    }
                    ds = new DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    ds.InsuranceCode = string.IsNullOrEmpty(vtype) ? "ALL   " : vtype;
                    dtlScoring.Add(ds);
                    if (!string.IsNullOrEmpty(vBrand))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "VBRAND";
                        ds.InsuranceCode = vBrand;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vIsNew))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "NEWVHC";
                        ds.InsuranceCode = vIsNew;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vModel))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "VMODEL";
                        ds.InsuranceCode = vModel;
                        dtlScoring.Add(ds);
                    }

                    if (!string.IsNullOrEmpty(OldPolicyNo))
                    {
                        List<CalculatedPremi> recalcpremi = db.Fetch<CalculatedPremi>(@";SELECT a.InterestID,b.coverageid,case when coverageid in('TRS','TRRTLO') then osmv.SumInsured else b.SumInsured end SumInsured,b.begindate AS PeriodFrom,b.enddate AS PeriodTo FROM OrderSimulation os inner join orderSimulationinterest a on a.orderno=os.orderno 
									 inner join orderSimulationcoverage b on a.orderno=b.orderno and a.interestno=b.interestno and b.RowStatus=1  and b.orderno=os.orderno
									 inner join OrderSimulationMV osmv on osmv.orderno=os.orderno 
                                     where a.RowStatus=1 AND os.oldpolicyno=@0 and (a.interestid='CASCO' AND b.coverageid in('TRS','TRRTLO') OR a.InterestID in('TPLPER','PADRVR','PAPASS')OR (a.InterestID='ACCESS' AND b.coverageid in('ALLRIK','TLO') AND b.BeginDate=CONVERT(char,YEAR(os.PeriodFrom))+'-'+CONVERT(char,MONTH(os.PeriodFrom))+'-'+CONVERT(char,DAY(os.PeriodFrom))+' 00:00:00.000' ))order by a.interestid desc", OldPolicyNo);


                        foreach (CalculatedPremi item in recalcpremi)
                        {
                            if (item.InterestID.Equals("ACCESS"))
                            {
                                List<CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(JsonConvert.SerializeObject(CalculatedPremiItems));
                                List<CalculatedPremi> cItems = mobileRepository.CalculateACCESS(vTSInterest, Convert.ToDecimal(item.SumInsured), ProductCode, vMouID, PeriodFrom, PeriodTo, "ACCESS", vtype, vyear, vsitting, pQuotationNo, calculateaccess, dtlScoring, OldPolicyNo, out RenDiscountPct, out DiscountPct);
                                CalculatedPremiItems.AddRange(cItems);

                            }
                        }
                    }
                    #endregion
                    #region calculate final item

                    RefreshPremi();
                    SumInsured = Convert.ToDouble(vTSInterest);
                    CalculateLoading();

                    decimal NoClaimBonus = Repository.vWeb2.MobileRepository.CalculateNCB(GrossPremi, RenDiscountPct);
                    decimal DiscountPremi = Repository.vWeb2.MobileRepository.CalculateDiscountPremi(GrossPremi, DiscountPct);
                    CalculateTotalPremi();

                    #endregion

                    return Json(new
                    {
                        status = true,
                        message = "OK",
                        ProductCode,
                        CalculatedPremiItems,
                        //coveragePeriodItems,
                        //coveragePeriodNonBasicItems,
                        IsTPLEnabled,
                        IsTPLChecked,
                        IsTPLSIEnabled,
                        IsSRCCChecked,
                        IsSRCCEnabled,
                        IsFLDChecked,
                        IsFLDEnabled,
                        IsETVChecked,
                        IsETVEnabled,
                        IsTSChecked,
                        IsTSEnabled,
                        IsPADRVRChecked,
                        IsPADRVREnabled,
                        IsPADRVRSIEnabled,
                        IsPASSEnabled,
                        IsPAPASSSIEnabled,
                        IsPAPASSEnabled,
                        IsPAPASSChecked,
                        IsACCESSChecked,
                        IsACCESSEnabled,
                        IsACCESSSIEnabled,
                        SRCCPremi,
                        FLDPremi,
                        ETVPremi,
                        TSPremi,
                        PADRVRPremi,
                        PAPASSPremi,
                        TPLPremi,
                        ACCESSPremi,
                        AdminFee,
                        TotalPremi,
                        GrossPremi,
                        NoClaimBonus,
                        DiscountPremi
                        //Alert
                    }, Util.jsonSerializerSetting());
                }
                else
                {
                    return Json(new { status = false, message = "Product Has Not Mapping Segment", data = "null" });

                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult PremiumCalculation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + "Function Begin: ");
            try
            {
                #region declare param
                List<decimal> RenDiscountPct = new List<decimal>();
                List<decimal> DiscountPct = new List<decimal>();
                string OrderNo = form.Get("OrderNo");
                ProductCode = form.Get("vProductCode");
                string MouID = form.Get("vMouID");
                decimal vTSInterest = Convert.ToDecimal(form.Get("vTSInterest"));
                #region load existing cover
                if (!string.IsNullOrEmpty(OrderNo))
                {
                    InitializeCoverageItems(OrderNo, MouID, out RenDiscountPct, out DiscountPct);
                }
                else
                {
                    #endregion
                    string chItem = form.Get("chItem");
                    string vPeriodFrom = form.Get("PeriodFrom");
                    string vPeriodTo = form.Get("PeriodTo");
                    vPeriodFrom = vPeriodFrom.Equals("null") ? null : vPeriodFrom;
                    vPeriodTo = vPeriodTo.Equals("null") ? null : vPeriodTo;
                    CoverageTypeNonBasic cType = chItem.Equals("SRCC") ? CoverageTypeNonBasic.SRCC : (chItem.Equals("FLD") ? CoverageTypeNonBasic.FLD : (chItem.Equals("ETV")) ? CoverageTypeNonBasic.ETV : (chItem.Equals("TS")) ? CoverageTypeNonBasic.TS : (chItem.Equals("TPLPER")) ? CoverageTypeNonBasic.TPLPER : (chItem.Equals("PADRVR")) ? CoverageTypeNonBasic.PADRVR : (chItem.Equals("PAPASS")) ? CoverageTypeNonBasic.PAPASS : CoverageTypeNonBasic.ACCESS);
                    string vtype = form.Get("vtype");
                    string vcitycode = form.Get("vcitycode");
                    string vusagecode = form.Get("vusagecode");
                    string vyear = form.Get("vyear");
                    vyear = string.IsNullOrEmpty(vyear) ? "0" : vyear;
                    string vsitting = form.Get("vsitting");
                    string vMouID = form.Get("vMouID");
                    string vCoverageId = form.Get("vCoverageId");
                    string pBasicCoverageId = vCoverageId;
                    string TPLCoverageId = form.Get("TPLCoverageId");
                    string vInterestId = (cType.Equals(CoverageTypeNonBasic.ACCESS) || cType.Equals(CoverageTypeNonBasic.TPLPER) || cType.Equals(CoverageTypeNonBasic.PADRVR) || cType.Equals(CoverageTypeNonBasic.PAPASS)) ? cType.ToString() : "CASCO";
                    string vModel = form.Get("vModel");
                    string vBrand = form.Get("vBrand");
                    string vIsNew = form.Get("isNew");
                    string pQuotationNo = form.Get("pQuotationNo");
                    string Ndays = form.Get("Ndays");
                    int ComprePeriod = Convert.ToInt16(form.Get("ComprePeriod"));
                    int TLOPeriod = Convert.ToInt16(form.Get("TLOPeriod"));
                    string OldPolicyNo = form.Get("OldPolicyNo");
                    decimal vPrimarySI = Convert.ToDecimal(form.Get("vPrimarySI"));

                    DateTime PeriodFrom = string.IsNullOrEmpty(vPeriodFrom) ? DateTime.MinValue : Convert.ToDateTime(vPeriodFrom);
                    DateTime PeriodTo = string.IsNullOrEmpty(vPeriodTo) ? DateTime.MinValue : Convert.ToDateTime(vPeriodTo);
                    DateTime PeriodFromCover = PeriodFrom;
                    DateTime PeriodToCover = PeriodTo;
                    string calculateditems = form.Get("CalculatedPremiItems");
                    bool isBasicCover = false;
                    if (string.IsNullOrEmpty(Ndays) || string.IsNullOrEmpty(vCoverageId) || string.IsNullOrEmpty(ProductCode) || string.IsNullOrEmpty(vusagecode) || string.IsNullOrEmpty(vcitycode)
                        || string.IsNullOrEmpty(form.Get("vTSInterest")) || string.IsNullOrEmpty(form.Get("vPrimarySI")))
                    {
                        return Json(new
                        {
                            status = true,
                            message = "Param Not Completed"
                        });
                    }
                    if (!string.IsNullOrEmpty(calculateditems))
                    {
                        decimal primarySI = 0;
                        CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculateditems);//new List<CalculatedPremi>();

                        #region remove current cover
                        List<CalculatedPremi> removeitem = new List<CalculatedPremi>();
                        if ((new List<string>() { "SRCC", "FLD", "ETV" }.Contains(cType.ToString())))
                        {
                            CalculatedPremiItems.RemoveAll(x => ((x.InterestID.Equals("CASCO") || x.InterestID.Equals("ACCESS")) && (!x.CoverageID.TrimEnd().Equals("ALLRIK") && !x.CoverageID.TrimEnd().Equals("TLO"))));

                        }
                        foreach (CalculatedPremi item in CalculatedPremiItems)
                        {
                            if ((item.InterestID.Equals(cType.ToString())) || (cType.Equals(CoverageTypeNonBasic.TS) && (item.CoverageID.TrimEnd().Equals("TRS") || item.CoverageID.TrimEnd().Equals("TRRTLO"))))
                            {
                                removeitem.Add(item);
                            }
                            //get periodfrom periodto cover
                            if (item.CoverageID.Equals("ALLRIK") || item.CoverageID.Equals("TLO"))
                            {
                                if (item.PeriodFrom < PeriodFrom)
                                {
                                    PeriodFrom = item.PeriodFrom;
                                }
                                if (item.PeriodTo > PeriodTo)
                                {
                                    PeriodTo = item.PeriodTo;
                                }
                            }

                            if (item.InterestID.Equals("ACCESS") && (item.CoverageID.Equals("ALLRIK") || item.CoverageID.Equals("TLO")))
                            {
                                if (AccessSI < item.SumInsured)
                                {
                                    AccessSI = item.SumInsured;
                                }

                            }


                        }
                        foreach (CalculatedPremi remove in removeitem)
                        {
                            CalculatedPremiItems.Remove(remove);

                        }

                        #endregion
                    }
                    #region set vehicleyear to year now if not choose vehicle

                    if (Convert.ToInt16(vyear) == 0)
                    {
                        vyear = Convert.ToString(DateTime.Now.Year);
                    }
                    #endregion

                    #endregion

                    #region Check if Basic Cover Has Applied
                    if (string.IsNullOrEmpty(OrderNo))
                    {
                        bool isBasicCoverApplied = true;
                        isBasicCoverApplied = CalculatedPremiItems.Where(x => new List<string>() { "CASCO" }.Contains(x.InterestID.TrimEnd())).Count() == 0 ? false : true;

                        if (!isBasicCoverApplied)
                        {
                            return Json(new
                            {
                                status = true,
                                message = "Please Apply Basic Cover first!"
                            });
                        }
                    }
                    #endregion
                    #region Check TS Able To Checked
                    bool isableTS = true;
                    if (chItem.Equals("TS"))
                    {
                        isableTS = CalculatedPremiItems.Where(x => new List<string>() { "SRCC", "FLD", "ETV", "SRCTLO", "FLDTLO", "ETVTLO" }.Contains(x.CoverageID.TrimEnd())).Count() == 0 ? false : true;
                    }
                    if (!isableTS)
                    {
                        return Json(new
                        {
                            status = true,
                            message = "Please Apply SRCC/FLD/ETV first!"
                        });
                    }
                    #endregion
                    var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                    if (!vInterestId.Equals("CASCO") && !vInterestId.Equals("ACCESS"))
                    {// HANDLE TPL AND PRODUCT LEXUS
                        vInterestId = Repository.vWeb2.MobileRepository.GetSpecialInterestId(ProductCode, cType, vInterestId);
                        vCoverageId = Repository.vWeb2.MobileRepository.GetSpecialCoverageId(ProductCode, cType, vInterestId, TPLCoverageId);
                    }
                    else if (cType.Equals(CoverageTypeNonBasic.SRCC) || cType.Equals(CoverageTypeNonBasic.FLD) || cType.Equals(CoverageTypeNonBasic.ETV))
                    {
                        string coverageiep = Repository.vWeb2.MobileRepository.isProductIEP(ProductCode);
                        vCoverageId = vCoverageId.Equals("TLO") ? cType.ToString() + vCoverageId : cType.ToString();
                        vCoverageId = string.IsNullOrEmpty(coverageiep) ? vCoverageId : coverageiep;

                    }
                    else if (!vInterestId.Equals("ACCESS"))
                    {
                        vCoverageId = cType.Equals(CoverageTypeNonBasic.TS) ? vCoverageId.Equals("TLO") ? "TRRTLO" : "TRS" : cType.ToString();
                    }

                    #region Get rate scoring
                    List<DetailScoring> dtlScoring = new List<DetailScoring>();
                    DetailScoring ds = new DetailScoring();
                    if (!string.IsNullOrEmpty(vcitycode))
                    {
                        ds.FactorCode = "GEOGRA";
                        ds.InsuranceCode = vcitycode;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vusagecode))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "MVUSAG";
                        ds.InsuranceCode = vusagecode;
                        dtlScoring.Add(ds);
                    }
                    ds = new DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    ds.InsuranceCode = string.IsNullOrEmpty(vtype) ? "ALL" : vtype;
                    dtlScoring.Add(ds);
                    if (!string.IsNullOrEmpty(vBrand))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "VBRAND";
                        ds.InsuranceCode = vBrand;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vIsNew))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "NEWVHC";
                        ds.InsuranceCode = vIsNew;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vModel))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "VMODEL";
                        ds.InsuranceCode = vModel;
                        dtlScoring.Add(ds);
                    }
                    #endregion
                    if (cType.Equals(CoverageTypeNonBasic.ACCESS) && PeriodFrom != DateTime.MinValue)
                    {
                        List<CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculateditems);
                        List<CalculatedPremi> cItems = mobileRepository.CalculateACCESS(vPrimarySI, vTSInterest, ProductCode, vMouID, PeriodFrom, PeriodTo, vInterestId, vtype, vyear, vsitting, pQuotationNo, calculateaccess, dtlScoring, OldPolicyNo, out RenDiscountPct, out DiscountPct);
                        CalculatedPremiItems.AddRange(cItems);
                    }
                    #region handle uncheck item
                    else if (PeriodFrom == DateTime.MinValue)
                    {
                        var RenDiscount = Repository.vWeb2.RetailRepository.GetRenewalDiscount(OldPolicyNo);
                        foreach (WTCommission ren in RenDiscount)
                        {
                            if (ren.CommissionID.Contains("RD"))
                            {
                                RenDiscountPct.Add(ren.Percentage);
                            }
                        }
                        if (!(new List<string>() { "SRCC", "FLD", "TS", "ETV" }.Contains(cType.ToString())) && CalculatedPremiItems.Select(x => x.InterestID.Equals(vInterestId)).Count() > 0)
                        {
                            CalculatedPremiItems.RemoveAll(x => x.InterestID.Trim().Equals(vInterestId));
                            //handle PAPASS when uncheck PADRVR
                            if (cType.Equals(CoverageTypeNonBasic.PADRVR) && CalculatedPremiItems.Select(x => x.InterestID.Trim()).Contains(CoverageTypeNonBasic.PAPASS.ToString()))
                            {
                                CalculatedPremiItems.RemoveAll(x => x.InterestID.Contains(CoverageTypeNonBasic.PAPASS.ToString()));
                            }
                        }
                        else if (cType.Equals(CoverageTypeNonBasic.TS) && pBasicCoverageId.Equals("ALLRIK,TLO"))
                        {
                            CalculatedPremiItems.RemoveAll(x => x.InterestID.TrimEnd().Equals("CASCO") && (x.CoverageID.TrimEnd().Equals("TRS") || x.CoverageID.TrimEnd().Equals("TRRTLO")));
                        }
                        else
                        {
                            var PRDType = Repository.vWeb2.RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: "CASCO", CoverageID: pBasicCoverageId);
                            string ConfigName = "EXTENDED_COVER_GROUP_COMPRE";
                            if (PRDType[0].TLOFlag != null)
                            {
                                if (PRDType[0].TLOFlag)
                                {
                                    ConfigName = "EXTENDED_COVER_GROUP_TLO";
                                }
                            }
                            List<string> CoverGrp = Repository.vWeb2.RetailRepository.GetGeneralConfig(ConfigName, "PRODUCT", ProductCode, vCoverageId);
                            if (CoverGrp.Count == 0)
                                CoverGrp.Add(vCoverageId);

                            //handle FLD, SRCC, ETV
                            foreach (var c in CoverGrp)
                            {
                                CalculatedPremiItems.RemoveAll(x => c.Equals(x.CoverageID.Trim()));
                            }

                        }

                    }
                    #endregion
                    else
                    {
                        List<CoverageParam> cpList = new List<CoverageParam>();
                        if (TLOPeriod > 0 && ComprePeriod > 0 && (new List<string>() { "SRCC", "FLD", "TS", "ETV" }.Contains(cType.ToString())))
                        {// HANDLING KOMBINASI BASIC COVER
                         //Handling TS Apply like Bundling
                            if (cType.Equals(CoverageTypeNonBasic.TS))
                            {
                                foreach (CalculatedPremi item in CalculatedPremiItems)
                                {
                                    if (item.InterestID.Equals("CASCO") && item.CoverageID.Contains("SRC"))
                                    {
                                        cpList.Add(new CoverageParam { CoverageId = (item.CoverageID.Contains("TLO") ? "TRRTLO" : vCoverageId), PeriodFromCover = item.PeriodFrom, PeriodToCover = item.PeriodTo });

                                    }
                                }

                            }
                        }
                        else
                        {
                            cpList.Add(new CoverageParam { CoverageId = vCoverageId, PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });
                        }

                        foreach (CoverageParam cp in cpList)
                        {

                            int calcmethod = PeriodTo.Date < PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                            CalculatedPremiItems = mobileRepository.calculatepremicalculation(ProductCode, vInterestId, cp, PeriodFrom, PeriodTo, vtype, vyear
                                                                                             , vsitting, dtlScoring, Ndays, vTSInterest, vMouID, OldPolicyNo, CalculatedPremiItems, calcmethod, out RenDiscountPct, out DiscountPct);
                        }
                    }
                    #region recalculate access if srcc ts applied lastly
                    if (new List<string>() { "SRCC", "FLD", "TS", "ETV" }.Contains(cType.ToString()) && PeriodFrom != DateTime.MinValue)
                    {
                        CalculatedPremiItems = mobileRepository.RecalculateAccess(vTSInterest, vCoverageId, CalculatedPremiItems, ProductCode, vMouID, PeriodFrom, PeriodTo,
                                                           vtype, vyear, vsitting, pQuotationNo, dtlScoring, OldPolicyNo, out RenDiscountPct, out DiscountPct);
                    }
                    #endregion
                }
                #region calculate final item

                InitializeEnableCoverage();
                SumInsured = Convert.ToDouble(vTSInterest);
                RefreshPremi();
                getAdminFee();
                CalculateLoading();
                CalculateTotalPremi();
                decimal NoClaimBonus = Repository.vWeb2.MobileRepository.CalculateNCB(GrossPremi, RenDiscountPct);
                decimal DiscountPremi = Repository.vWeb2.MobileRepository.CalculateDiscountPremi(GrossPremi, DiscountPct);
                #endregion
                if (CalculatedPremiItems != null)
                {
                    _log.Debug(actionName + "Function OK! ");
                    return Json(new
                    {
                        status = true,
                        message = "OK",
                        ProductCode,
                        CalculatedPremiItems,
                        //coveragePeriodItems,
                        //coveragePeriodNonBasicItems,
                        IsTPLEnabled,
                        IsTPLChecked,
                        IsTPLSIEnabled,
                        IsSRCCChecked,
                        IsSRCCEnabled,
                        IsFLDChecked,
                        IsFLDEnabled,
                        IsETVChecked,
                        IsETVEnabled,
                        IsTSChecked,
                        IsTSEnabled,
                        IsPADRVRChecked,
                        IsPADRVREnabled,
                        IsPADRVRSIEnabled,
                        IsPASSEnabled,
                        IsPAPASSSIEnabled,
                        IsPAPASSEnabled,
                        IsPAPASSChecked,
                        IsACCESSChecked,
                        IsACCESSEnabled,
                        IsACCESSSIEnabled,
                        SRCCPremi,
                        FLDPremi,
                        ETVPremi,
                        TSPremi,
                        PADRVRPremi,
                        PAPASSPremi,
                        TPLPremi,
                        ACCESSPremi,
                        AdminFee,
                        TotalPremi,
                        GrossPremi,
                        NoClaimBonus,
                        DiscountPremi
                        //Alert
                    }, Util.jsonSerializerSetting());
                }
                _log.Error(actionName + "Something Error!");
                return Json(new { status = false, message = "Error", data = "Something Error!" });
            }
            catch (Exception e)
            {
                _log.Error(actionName + "Function Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult getOrderDetailCoverage(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                double AdminFee = Convert.ToDouble(form.Get("AdminFee").Trim());
                string vtype = form.Get("vtype");
                string vcitycode = form.Get("vcitycode");
                string vusagecode = form.Get("vusagecode");
                string vyear = form.Get("vyear");
                string vsitting = form.Get("vsitting");
                string vModel = form.Get("vModel");
                string vBrand = form.Get("vBrand");
                string vIsNew = form.Get("isNew");
                string Ndays = form.Get("Ndays");
                ProductCode = form.Get("vProductCode");
                string vMouID = form.Get("vMouID");
                CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(form.Get("calculatedPremi"));
                DateTime PeriodFrom = Convert.ToDateTime(form.Get("PeriodFrom"));
                DateTime PeriodTo = Convert.ToDateTime(form.Get("PeriodTo"));
                #region detail scoring
                List<DetailScoring> dtlScoring = new List<DetailScoring>();
                DetailScoring ds = new DetailScoring();
                if (!string.IsNullOrEmpty(vcitycode))
                {
                    ds.FactorCode = "GEOGRA";
                    ds.InsuranceCode = vcitycode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(vusagecode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "MVUSAG";
                    ds.InsuranceCode = vusagecode;
                    dtlScoring.Add(ds);
                }
                ds = new DetailScoring();
                ds.FactorCode = "VHCTYP";
                ds.InsuranceCode = string.IsNullOrEmpty(vtype) ? "ALL   " : vtype;
                dtlScoring.Add(ds);
                if (!string.IsNullOrEmpty(vBrand))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "VBRAND";
                    ds.InsuranceCode = vBrand;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(vIsNew))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "NEWVHC";
                    ds.InsuranceCode = vIsNew;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(vModel))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "VMODEL";
                    ds.InsuranceCode = vModel;
                    dtlScoring.Add(ds);
                }
                #endregion
                double disc = 0;
                DetailCoverageSummary result = mobileRepository.GetDetailCoverageSummary(ProductCode, vMouID, dtlScoring, CalculatedPremiItems, AdminFee, vyear, UsageCode, Convert.ToInt16(Ndays), PeriodFrom, PeriodTo,
                                                                                          vtype, vsitting, "", out disc);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult getTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                #endregion
                Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
                try
                {
                    result = mobileRepository.GetTaksListDetail(CustID, FollowUpNo);
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateTaskDetail(Models.v0213URF2019.UpdateTaskDetailParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string CustID = param.CustID;
            string FollowUpNo = param.FollowUpNo;
            Models.vWeb2.PersonalData PersonalData = param.PersonalData;
            Models.vWeb2.CompanyData CompanyData = param.CompanyData;
            Models.vWeb2.VehicleData VehicleData = param.VehicleData;
            Models.vWeb2.PolicyAddress policyAddress = param.policyAddress;
            Models.vWeb2.SurveySchedule surveySchedule = param.surveySchedule;
            List<Models.vWeb2.ImageDataTaskDetail> imageData = param.imageData;
            Models.vWeb2.OrderSimulationModel OSData = param.OSData;
            Models.vWeb2.OrderSimulationMVModel OSMVData = param.OSMVData;
            Models.vWeb2.CalculatePremiModel calculatedPremiItems = param.calculatedPremiItems;
            string FollowUpStatus = param.FollowUpStatus;
            Models.vWeb2.Remarks Remarks = param.Remarks;
            #endregion

            if (string.IsNullOrEmpty(param.CustID) || string.IsNullOrEmpty(param.FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            string OrderNo = "";
            try
            {
                OrderNo = mobileRepository.UpdateTaskListDetail(CustID, FollowUpNo, PersonalData,
                    CompanyData, VehicleData, policyAddress, surveySchedule, imageData, OSData, OSMVData,
                    calculatedPremiItems, FollowUpStatus, Remarks);
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", data = new { OrderNo, PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo() } });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveProspect(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string State = form.Get("State");
                string PCData = form.Get("ProspectCustomer");
                string FUData = form.Get("FollowUp");
                string FUHData = form.Get("FollowUpHistory");
                string ENData = form.Get("EmailNotification ");
                string OSData = form.Get("OrderSimulation");
                string OSMVData = form.Get("OrderSimulationMV");
                // string OSIData = form.Get("OrderSimulationInterest");
                string calculatedPremiItems = form.Get("calculatedPremiItems");
                bool IsMvGodig = bool.Parse(form.Get("isMvGodig"));
                string GuidTempPenawaran = form.Get("GuidTempPenawaran");
                string PremiumModelData = form.Get("PremiumModel");

                string query = "";
                string CustIdEnd = "";
                string FollowUpNoEnd = "";
                string OrderNoEnd = "";


                Models.v0219URF2019.FollowUp FUEnd = null;
                Models.v0219URF2019.ProspectCustomer PCEnd = null;

                #region Prospect Tab
                if (State.Equals("PROSPECT") || State.Equals("PREMIUMSIMULATION"))
                {
                    string CustId = "";
                    string FollowUpNumber = "";
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? Repository.v0219URF2019.MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT UPDATE PROSPECT CUSTOMER

                        Models.v0219URF2019.ProspectCustomer PC = JsonConvert.DeserializeObject<Models.v0219URF2019.ProspectCustomer>(PCData);
                        string CustID = System.Guid.NewGuid().ToString();
                        string FollowUpNo = System.Guid.NewGuid().ToString();
                        PC.CustID = string.IsNullOrEmpty(PC.CustID) ? CustID : PC.CustID;
                        query = @";IF EXISTS(SELECT * FROM ProspectCustomer WHERE CustId=@0)
BEGIN
UPDATE ProspectCustomer SET
       [Name]=@1
      ,[Phone1]=@2
      ,[Phone2]=@3
      ,[Email1]=@4
      ,[Email2]=@5
      ,[CustIDAAB]=@6
      ,[SalesOfficerID]=@7
      ,[DealerCode]=@8
      ,[SalesDealer]=@9
      ,[BranchCode]=@10
      ,[isCompany]=@11
      ,[LastUpdatedTime]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCustomer ([CustID]
      ,[Name]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[CustIDAAB]
      ,[SalesOfficerID]
      ,[DealerCode]
      ,[SalesDealer]
      ,[BranchCode]
      ,[isCompany]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1)
END";
                        // PC.SalesOfficerID = PC.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", PC.SalesOfficerID) : PC.SalesOfficerID;

                        db.Execute(query, PC.CustID, PC.Name, PC.Phone1, PC.Phone2, PC.Email1, PC.Email2, PC.CustIDAAB, PC.SalesOfficerID, PC.DealerCode, PC.SalesDealer, PC.BranchCode, PC.isCompany);
                        #endregion

                        #region INSERT UPDATE FOLLOWUP
                        Models.v0219URF2019.FollowUp FU = JsonConvert.DeserializeObject<Models.v0219URF2019.FollowUp>(FUData);

                        FU.FollowUpNo = string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo;
                        FU.CustID = string.IsNullOrEmpty(FU.CustID) ? CustID : FU.CustID;
                        FU.LastSeqNo = db.ExecuteScalar<int>(";IF EXISTS(SELECT * FROM FollowUpHistory Where FollowUpNo=@0)BEGIN SELECT ISNULL(MAX(SeqNo),0) From FollowUpHistory WHERE FollowUpNo=@0 END ELSE BEGIN SELECT 0 END", string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo);
                        query = @";IF EXISTS(SELECT * FROM FollowUp WHERE FollowUpNo=@0)
BEGIN
    UPDATE FollowUp SET [LastSeqNo]=@1
      ,[CustID]=@2
      ,[ProspectName]=@3
      ,[Phone1]=@4
      ,[Phone2]=@5
      ,[SalesOfficerID]=@6
      ,[EntryDate]=GETDATE()
      ,[FollowUpName]=@7
      ,[FollowUpStatus]=@8
      ,[FollowUpInfo]=@9
      ,[Remark]=@10
      ,[BranchCode]=@11
      ,[LastUpdatedTime]=GETDATE()
      ,[PolicyId]=@12
      ,[PolicyNo]=@13
      ,[TransactionNo]=@14
      ,[SalesAdminID]=@15
      ,[SendDocDate]=@16
      ,[IdentityCard]=@17
      ,[STNK]=@18
      ,[SPPAKB]=@19
      ,[BSTB1]=@20
      ,[BSTB2]=@21
      ,[BSTB3]=@22
      ,[BSTB4]=@23
      ,[CheckListSurvey1]=@24
      ,[CheckListSurvey2]=@25
      ,[CheckListSurvey3]=@26
      ,[CheckListSurvey4]=@27
      ,[PaymentReceipt1]=@28
      ,[PaymentReceipt2]=@29
      ,[PaymentReceipt3]=@30
      ,[PaymentReceipt4]=@31
      ,[PremiumCal1]=@32
      ,[PremiumCal2]=@33
      ,[PremiumCal3]=@34
      ,[PremiumCal4]=@35
      ,[FormA1]=@36
      ,[FormA2]=@37
      ,[FormA3]=@38
      ,[FormA4]=@39
      ,[FormB1]=@40
      ,[FormB2]=@41
      ,[FormB3]=@42
      ,[FormB4]=@43
      ,[FormC1]=@44
      ,[FormC2]=@45
      ,[FormC3]=@46
      ,[FormC4]=@47
      ,[FUBidStatus]=@48
      ,[BuktiBayar]=@49 WHERE [FollowUpNo]=@0
END
    ELSE
BEGIN
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[PolicyId]
      ,[PolicyNo]
      ,[TransactionNo]
      ,[SalesAdminID]
      ,[SendDocDate]
      ,[IdentityCard]
      ,[STNK]
      ,[SPPAKB]
      ,[BSTB1]
      ,[BSTB2]
      ,[BSTB3]
      ,[BSTB4]
      ,[CheckListSurvey1]
      ,[CheckListSurvey2]
      ,[CheckListSurvey3]
      ,[CheckListSurvey4]
      ,[PaymentReceipt1]
      ,[PaymentReceipt2]
      ,[PaymentReceipt3]
      ,[PaymentReceipt4]
      ,[PremiumCal1]
      ,[PremiumCal2]
      ,[PremiumCal3]
      ,[PremiumCal4]
      ,[FormA1]
      ,[FormA2]
      ,[FormA3]
      ,[FormA4]
      ,[FormB1]
      ,[FormB2]
      ,[FormB3]
      ,[FormB4]
      ,[FormC1]
      ,[FormC2]
      ,[FormC3]
      ,[FormC4]
      ,[FUBidStatus]
      ,[BuktiBayar]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38,@39,@40,@41,@42,@43,@44,@45,@46,@47,@48,@49)
END";
                        // FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;

                        db.Execute(query, FU.FollowUpNo
     , FU.LastSeqNo
     , FU.CustID
     , FU.ProspectName
     , FU.Phone1
     , FU.Phone2
     , FU.SalesOfficerID
     , FU.FollowUpName
     , FU.FollowUpStatus
     , FU.FollowUpInfo
     , FU.Remark
     , FU.BranchCode
     , FU.PolicyId
     , FU.PolicyNo
     , FU.TransactionNo
     , FU.SalesAdminID
     , FU.SendDocDate
     , FU.IdentityCard
     , FU.STNK
     , FU.SPPAKB
     , FU.BSTB1
     , FU.BSTB2
     , FU.BSTB3
     , FU.BSTB4
     , FU.CheckListSurvey1
     , FU.CheckListSurvey2
     , FU.CheckListSurvey3
     , FU.CheckListSurvey4
     , FU.PaymentReceipt1
     , FU.PaymentReceipt2
     , FU.PaymentReceipt3
     , FU.PaymentReceipt4
     , FU.PremiumCal1
     , FU.PremiumCal2
     , FU.PremiumCal3
     , FU.PremiumCal4
     , FU.FormA1
     , FU.FormA2
     , FU.FormA3
     , FU.FormA4
     , FU.FormB1
     , FU.FormB2
     , FU.FormB3
     , FU.FormB4
     , FU.FormC1
     , FU.FormC2
     , FU.FormC3
     , FU.FormC4
     , FU.FUBidStatus
     , FU.BuktiBayar);
                        #endregion


                        if (PC.isCompany)
                        {
                            #region INSERT UPDATE COMPANY DATA
                            db.Execute(@";IF EXISTS(SELECT * FROM ProspectCompany WHERE CustId=@0)
BEGIN
UPDATE ProspectCompany SET
       [CompanyName]=@1
      ,[FollowUpNo]=@2
      ,[PICPhoneNo]=@3
      ,[Email]=@4
      ,[RowStatus]=1
      ,[ModifiedDate]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName]
      ,[FollowUpNo]
      ,[PICPhoneNo]
      ,[Email]
      ,[ModifiedDate]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,GETDATE(),1)
END", PC.CustID, PC.Name, FU.FollowUpNo, PC.Phone1, PC.Email1);
                            #endregion
                        }

                        PCEnd = PC;
                        FUEnd = FU;

                        #region SAVE IMAGE
                        mobileRepository.SaveImageProspect(FU.FollowUpNo);
                        #endregion


                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "PROSPECT", FU.CustID, FU.FollowUpNo, "", GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, FU.FollowUpNo, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion


                    }
                    //return Json(new { status = true, message = "Success", CustId, FollowUpNumber });
                    CustIdEnd = FUEnd.CustID;
                    FollowUpNoEnd = FUEnd.FollowUpNo;

                }
                #endregion


                #region Vehicle Tab
                if (State.Equals("VEHICLE") || State.Equals("PREMIUMSIMULATION"))
                {
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? Repository.v0219URF2019.MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    string OrderNoReturn = null;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT ORDERSIMULATION
                        Models.v0219URF2019.OrderSimulation OS = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulation>(OSData);

                        FollowUpNoEnd = OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;

                        CustIdEnd = OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;

                        string OrderNo = System.Guid.NewGuid().ToString();
                        OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                        // 
                        query = @";Select OrderNo from OrderSimulation where FollowUpNo=@0 and CustId = @1 and ApplyF = 1";
                        string OrderNoCheck = db.FirstOrDefault<string>(query, FollowUpNoEnd, CustIdEnd);
                        OS.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OS.OrderNo : OrderNoCheck;
                        //
                        OrderNoReturn = OS.OrderNo;
                        OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                        OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                        OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PeriodFrom]
      ,[PeriodTo]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27)
END";
                        // OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                        db.Execute(query, OS.OrderNo
      , OS.CustID
      , OS.FollowUpNo
      , OS.QuotationNo
      , OS.MultiYearF
      , OS.YearCoverage
      , OS.TLOPeriod
      , OS.ComprePeriod
      , OS.BranchCode
      , OS.SalesOfficerID
      , OS.PhoneSales
      , OS.DealerCode
      , OS.SalesDealer
      , OS.ProductTypeCode
      , OS.AdminFee
      , OS.TotalPremium
      , OS.Phone1
      , OS.Phone2
      , OS.Email1
      , OS.Email2
      , OS.SendStatus
      , OS.InsuranceType
      , OS.ApplyF
      , OS.SendF
      , OS.LastInterestNo
      , OS.LastCoverageNo
      , OS.PeriodFrom
      , OS.PeriodTo);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION MV
                        Models.v0219URF2019.OrderSimulationMV OSMV = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulationMV>(OSMVData);
                        OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                        OSMV.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OSMV.OrderNo : OrderNoCheck;

                        query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                        db.Execute(query, OSMV.OrderNo
      , OSMV.ObjectNo
      , OSMV.ProductTypeCode
      , OSMV.VehicleCode
      , OSMV.BrandCode
      , OSMV.ModelCode
      , OSMV.Series
      , OSMV.Type
      , OSMV.Sitting
      , OSMV.Year
      , OSMV.CityCode
      , OSMV.UsageCode
      , OSMV.SumInsured
      , OSMV.AccessSI
      , OSMV.RegistrationNumber
      , OSMV.EngineNumber
      , OSMV.ChasisNumber
      //, OSMV.LastUpdatedTime
      //, OSMV.RowStatus
      , OSMV.IsNew);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION SURVEY
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulationSurvey WHERE OrderNo=@0)
BEGIN
INSERT INTO ordersimulationsurvey (OrderNo,CityCode,LocationCode,SurveyAddress,SurveyPostalCode,IsNeedSurvey,IsNSASkipSurvey,RowStatus,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
VALUES(@0,'','','','',@1,0,1,'OtosalesAPI',GETDATE(),NULL,NULL)
END
ELSE
BEGIN
UPDATE ordersimulationsurvey SET IsNeedSurvey=@1,ModifiedBy='OtosalesAPI',ModifiedDate=GETDATE() WHERE OrderNo=@0
END";
                        int isneedsurvey = OSMV.Year.Equals(DateTime.Now.Year.ToString()) ? 0 : 1;
                        db.Execute(query, OS.OrderNo, isneedsurvey);
                        #endregion

                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "VEHICLE", OS.CustID, OS.FollowUpNo, OS.OrderNo, GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, OS.FollowUpNo, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion
                    }

                    //return Json(new { status = true, message = "Success", OrderNo = OrderNoReturn });
                    OrderNoEnd = OrderNoReturn;
                }
                #endregion

                #region Cover Tab
                if (State.Equals("COVER") || State.Equals("PREMIUMSIMULATION"))
                {
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? Repository.v0219URF2019.MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    Models.v0219URF2019.OrderSimulation OS = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulation>(OSData);
                    Models.v0219URF2019.OrderSimulationMV OSMV = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulationMV>(OSMVData);
                    List<Models.v0219URF2019.OrderSimulationInterest> OSI = new List<Models.v0219URF2019.OrderSimulationInterest>();
                    List<Models.v0219URF2019.OrderSimulationCoverage> OSC = new List<Models.v0219URF2019.OrderSimulationCoverage>();
                    OS.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OS.OrderNo;
                    OSMV.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OSMV.OrderNo;

                    bool isexistSFE = false;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        // 
                        query = @";Select OrderNo from OrderSimulation where FollowUpNo=@0 and CustId = @1 and ApplyF = 1";
                        string OrderNoCheck = db.FirstOrDefault<string>(query, FollowUpNoEnd, CustIdEnd);
                        OS.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OS.OrderNo : OrderNoCheck;
                        OSMV.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OSMV.OrderNo : OrderNoCheck;
                        //

                        int a = 1;
                        int year = 1;
                        List<Models.v0219URF2019.CalculatedPremi> calculatedPremiItem = JsonConvert.DeserializeObject<List<Models.v0219URF2019.CalculatedPremi>>(calculatedPremiItems);

                        DateTime? pt = null;
                        foreach (Models.v0219URF2019.CalculatedPremi cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (Models.v0219URF2019.OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (Models.v0219URF2019.CalculatedPremi cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                        pt = cpitem.PeriodTo;
                                    }
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                        pf = cpitem.PeriodFrom;
                                    }
                                }
                                Models.v0219URF2019.OrderSimulationInterest osi = new Models.v0219URF2019.OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (Models.v0219URF2019.CalculatedPremi cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;

                        foreach (Models.v0219URF2019.OrderSimulationInterest item in OSI)
                        {
                            foreach (Models.v0219URF2019.CalculatedPremi item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    Models.v0219URF2019.OrderSimulationCoverage oscItem = new Models.v0219URF2019.OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }

                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert

                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);


                        int i = 1;
                        foreach (Models.v0219URF2019.OrderSimulationInterest data in OSI)
                        {
                            query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                            i++;
                        }
                        #endregion
                        #endregion

                        #endregion

                        #region DELETE UPDATE ORDERSIMULATION COVERAGE

                        #region Insert

                        i = 1;
                        foreach (Models.v0219URF2019.OrderSimulationCoverage data in OSC)
                        {
                            query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                            i++;

                        }
                        #endregion
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = OS.PeriodFrom ?? DateTime.Now;
                            DateTime end = OS.PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,PeriodFrom=@12
                        ,PeriodTo=@13
                        ,MouID=@14
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo, OS.PeriodFrom, OS.PeriodTo, OS.MouID);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion

                        OrderNoEnd = string.IsNullOrEmpty(OrderNoEnd) ? OS.OrderNo : OrderNoEnd;
                        FollowUpNoEnd = string.IsNullOrEmpty(FollowUpNoEnd) ? OS.FollowUpNo : FollowUpNoEnd;
                        CustIdEnd = string.IsNullOrEmpty(CustIdEnd) ? OS.CustID : CustIdEnd;
                        if (!Repository.v0219URF2019.MobileRepository.IsAllowedIEP(OS.ProductCode, isexistSFE))
                        {
                            return Json(new { status = false, message = "Basic Cover Not Applied", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd });
                        }

                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "COVER", CustIdEnd, FollowUpNoEnd, OS.OrderNo, GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, FollowUpNoEnd, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion

                    }

                    //    return Json(new { status = true, message = "Success", OrderSimulation = OS, FollowUpNo = FollowUpNoEnd });
                }
                return Json(new { status = true, message = "Success", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd, GuidTempPenawaran = GuidTempPenawaran });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult DownloadQuotation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string OrderNo = form.Get("OrderNo");

                Models.v0219URF2019.DownloadQuotationResult result = mobileRepository.GetByteReport(OrderNo);
                return Json(new { status = true, message = "success", data = result });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
                throw e;
            }
        }

        [HttpPost]
        public IHttpActionResult SendQuotationEmail(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Email = form.Get("Email");
            string OrderNo = form.Get("OrderNo");
            Email = Regex.Replace(Email, @"\t|\n|\r", "");
            try
            {
                A2isCommonResult res = mobileRepository.SendQuotationEmail(OrderNo, Email);
                if (res.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + res.ResultDesc });
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult SendViaWA(Models.v0219URF2019.SendViaWAParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(param.OrderNo) && string.IsNullOrEmpty(param.WAMessage) && string.IsNullOrEmpty(param.OrderNo))
                {
                    return BadRequest("Input Required");
                }
                bool status = mobileRepository.SendViaWA(param);
                if (status)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send WA" });
                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult SendViaEmail(Models.vWeb2.SendViaEmailParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(param.EmailTo) && string.IsNullOrEmpty(param.Body) && string.IsNullOrEmpty(param.OrderNo))
                {
                    return BadRequest("Input Required");
                }
                A2isCommonResult data = mobileRepository.SendViaEmail(param.EmailTo, param.EmailCC, param.EmailBCC, param.Body, param.Subject, param.ListAtt, param.OrderNo, param.IsAddAtt);
                if (data.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + data.ResultDesc });

                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult sendToSARenewalNonRenNot(Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string PolicyNo = param.PolicyNo;
            string FollowUpNo = param.FollowUpNo;
            string RemarkToSA = param.RemarksToSA;
            string SalesOfficerID = param.SalesOfficerID;
            int isPTSB = Convert.ToInt32(param.isPTSB);
            #endregion

            //Generate GUID 
            string custID = System.Guid.NewGuid().ToString();
            string followUpID = String.IsNullOrEmpty(FollowUpNo) ? System.Guid.NewGuid().ToString() : FollowUpNo;
            string orderID = System.Guid.NewGuid().ToString();

            if (string.IsNullOrEmpty(PolicyNo) || string.IsNullOrEmpty(FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            try
            {
                #region Check Double Insured
                Otosales.Models.vWeb2.CheckDoubleInsuredResult dblins = Repository.v0008URF2020.MobileRepository.GetCheckDoubleInsuredDataKotor(PolicyNo);
                if (dblins.IsDoubleInsured)
                {
                    return Json(new { status = false, message = "Double Insured.", data = dblins });
                }
                #endregion
                mobileRepository.GenerateOrderSimulation(SalesOfficerID, custID, followUpID, orderID, PolicyNo, RemarkToSA, isPTSB);

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", orderID });
            }
            catch (Exception e)
            {
                mobileRepository.ClearFailedData(custID, followUpID, orderID, PolicyNo);
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult GetSlotTimeBooking(FormDataCollection form)
        {
            _log.Debug("Begin");
            if (form == null) return BadRequest("Bad Request");
            string zipcode = form.Get("ZipCode");
            string cityid = form.Get("CityId");
            string branchID = form.Get("BranchID");
            string date = form.Get("AvailableDate");

            DateTime d;
            if (!DateTime.TryParse(date, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out d)) return BadRequest("Incorrect Date format");

            if (string.IsNullOrEmpty(cityid) || string.IsNullOrEmpty(date)) return BadRequest("Bad Request");
            try
            {
                dynamic res = null;
                if (!string.IsNullOrEmpty(zipcode))
                {
                    // 0181/URF/2018
                    IEnumerable<dynamic> data = null;
                    data = mobileRepository.GetSurveyScheduleSurveyManagement(cityid, zipcode, d);
                    // 0181/URF/2018
                    _log.Debug("OK");
                    res = data;
                }
                else if (!string.IsNullOrEmpty(branchID))
                {
                    List<string> TimeCodeSurveyDalam = mobileRepository.getSurveyCategoryInternalSurvey(branchID, d);
                    List<dynamic> times = mobileRepository.GetScheduleSurveyDalam(TimeCodeSurveyDalam);
                    res = times;
                }
                return Json(new { status = true, message = "Fetch success", data = res });
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }

        }

        [HttpPost]
        public IHttpActionResult GetSurveyDays(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string BranchID = form.Get("BranchID");
                List<string> data = mobileRepository.GetSurveyDays(BranchID);
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
                throw e;
            }
        }


        [HttpPost]
        public IHttpActionResult GetEnableDisableSalesmanDealer(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string ProductCode = form.Get("ProductCode");
                    string BranchCode = form.Get("BranchCode");
                    string MouID = form.Get("MouID");
                    if (string.IsNullOrEmpty(ProductCode) && string.IsNullOrEmpty(BranchCode) && string.IsNullOrEmpty(MouID))
                    {
                        return BadRequest("Input Required");
                    }

                    dynamic res = aabRepository.GetEnableDisableSalesmanDealer(ProductCode, BranchCode, MouID);
                    return Json(new { status = true, message = "success", IsDealerEnable = res.IsDealerEnable, IsSalesmanInfoEnable = res.IsSalesmanInfoEnable });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString(), spesific = false });
                }
            }
        }

        #region Private
        private void RefreshPremi()
        {
            BasicPremi = 0;
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;
            TPLPremi = 0;
            PADRVRPremi = 0;
            PAPASSPremi = 0;
            ACCESSPremi = 0;
            LoadingPremi = 0;

            foreach (CalculatedPremi x in CalculatedPremiItems)
            {
                GrossPremi += Convert.ToDouble(x.GrossPremium);
                if (x.InterestID.TrimEnd().Equals("CASCO") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    BasicPremi += Convert.ToDouble(x.Premium);
                }
                else if (x.InterestID.TrimEnd().Equals("ACCESS") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    ACCESSPremi += Convert.ToDouble(x.Net3);
                    if (ACCESSPremi > 0)
                    {
                        IsACCESSChecked = true;
                    }
                    IsACCESSSIEnabled = true;
                }
                else if (x.CoverageID.TrimEnd().Equals("SRCC") || x.CoverageID.TrimEnd().Equals("SRCTLO") || x.CoverageID.TrimEnd().Equals("SRCCTS"))
                {
                    SRCCPremi += Convert.ToDouble(x.Net3);
                    if (SRCCPremi > 0)
                    {
                        IsSRCCChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("FLD") || x.CoverageID.TrimEnd().Equals("FLDTLO"))
                {
                    FLDPremi += Convert.ToDouble(x.Net3);
                    if (FLDPremi > 0)
                    {
                        IsFLDChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("ETV") || x.CoverageID.TrimEnd().Equals("ETVTLO") || x.CoverageID.TrimEnd().Equals("EQK"))
                {
                    ETVPremi += Convert.ToDouble(x.Net3);
                    if (ETVPremi > 0)
                    {
                        IsETVChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("TRS") || x.CoverageID.TrimEnd().Equals("TRRTLO"))
                {
                    TSPremi += Convert.ToDouble(x.Net3);

                    if (TSPremi > 0)
                    {
                        IsTSChecked = true;
                    }
                    IsTSEnabled = true;
                    /** start 0040/URF/2017 */
                    //} else if (x.InterestID.equals("TPLPER") && (x.CoverageID.equals("MVTPL1"))) {
                }
                else if (x.InterestID.Equals("TPLPER") && (x.CoverageID.Contains("MVTP")))
                {
                    TPLPremi += Convert.ToDouble(x.Net3);

                    if (TPLPremi > 0 || x.Rate == -1)
                    {
                        IsTPLChecked = true;
                    }
                    IsTPLEnabled = true;
                    IsTPLSIEnabled = true;
                }
                else if (x.InterestID.Equals("PADRVR") || x.InterestID.Equals("PADDR1"))
                {
                    PADRVRPremi += Convert.ToDouble(x.Net3);
                    if (PADRVRPremi > 0)
                    {
                        IsPADRVRChecked = true;
                    }
                    IsPADRVRSIEnabled = true;
                }
                else if (x.InterestID.Equals("PAPASS") || x.InterestID.Equals("PA24AV"))
                {
                    PAPASSPremi += Convert.ToDouble(x.Net3);

                    if (PAPASSPremi > 0)
                    {
                        IsPADRVRChecked = true;
                        IsPAPASSChecked = true;
                    }
                    IsPAPASSEnabled = true;
                    IsPAPASSSIEnabled = true;
                    IsPASSEnabled = true;

                }

            }
        }
        private void InitializeEnableCoverage()
        {
            try
            {
                #region initialize enable cover

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string query = @"SELECT * FROM Prd_Interest_Coverage WHERE Product_Code=@0";
                prdInterestCoverage = db.Fetch<Prd_Interest_Coverage>(query, ProductCode);
                foreach (Prd_Interest_Coverage prd in prdInterestCoverage)
                {
                    if (prd.Coverage_Id.Trim().Equals("SRCC") ||
                            prd.Coverage_Id.Trim().Equals("SRCTLO") ||
                            prd.Coverage_Id.Trim().Equals("SRCCTS"))
                    {
                        IsSRCCEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Equals("FLD") ||
                          prd.Coverage_Id.Trim().Equals("FLDTLO"))
                    {
                        IsFLDEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Equals("ETV") ||
                          prd.Coverage_Id.Trim().Equals("ETVTLO") ||
                            prd.Coverage_Id.Trim().Equals("EQK"))
                    {
                        IsETVEnabled = true;
                    }
                    if (prd.Coverage_Id.Trim().Equals("TRS") ||
                         prd.Coverage_Id.Trim().Equals("TRRTLO"))
                    {
                        IsTSEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Contains("MVTP"))
                    {
                        IsTPLEnabled = true;
                    }
                    else if (prd.Interest_ID.Trim().Equals("PADRVR") || prd.Interest_ID.Trim().Equals("PADDR1"))
                    {
                        IsPADRVREnabled = true;
                    }
                    else if (prd.Interest_ID.Equals("PAPASS") || prd.Interest_ID.Equals("PA24AV"))
                    {
                        IsPAPASSEnabled = true;
                    }
                }
                IsACCESSEnabled = true;

                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        private void CalculateLoading()
        {
            LoadingPremi = 0;
            foreach (CalculatedPremi item in CalculatedPremiItems)
                LoadingPremi += item.Loading;
        }
        private void CalculateTotalPremi()
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    TotalPremi = 0;
                    GrossPremi = 0;

                    IsTPLEnabled = false;
                    foreach (CalculatedPremi item in CalculatedPremiItems)
                    {
                        TotalPremi += Convert.ToDouble(item.Net3);
                        GrossPremi += Convert.ToDouble(item.GrossPremium);
                        //CalculatePremiPerCoverage(item.InterestID, item.CoverageID, Convert.ToDouble(item.Premium));

                        if (item.CoverageID.TrimEnd().Equals("ALLRIK"))
                        {
                            IsTPLEnabled = true;
                        }
                    }


                    string query = @"select top 1 Policy_Fee1 from prd_policy_fee where product_code=@0 AND Order_Type=1 AND Lower_limit<=@1  AND Status=1 order by Lower_Limit desc";
                    AdminFee = db.ExecuteScalar<double>(query, ProductCode, TotalPremi);

                    // 003 begin
                    if (TotalPremi != 0)
                    {
                        TotalPremi += AdminFee; //+ LoadingPremi;
                    }
                    // 003 end
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        private void InitializeCoverageItems(string OrderNo, string MouID, out List<decimal> RPTRenDiscount, out List<decimal> Discount)
        {
            List<decimal> RDP = new List<decimal>();
            List<decimal> DP = new List<decimal>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            CalculatedPremiItems = Repository.vWeb2.MobileRepository.LoadCalculatedpremibyOrderNo(OrderNo.TrimEnd(), "");


            InterestCoverageItems = CalculatedPremiItems;
            BasicPremi = ACCESSPremi = AccessSI = SRCCPremi = FLDPremi = ETVPremi = TSPremi =
                    TPLPremi = TPLSI = PADRVRPremi = PADRVRSI = PAPASSPremi = PAPASSSI = PASS = 0;

            if (CalculatedPremiItems.Count() > 0)
            {
                foreach (CalculatedPremi item in CalculatedPremiItems)
                {
                    if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO") &&
                            (item.CoverageID.ToUpper().Equals("ALLRIK") || item.CoverageID.ToUpper().TrimEnd().Equals("TLO")))
                    {
                        BasicPremi += item.Premium;
                    }
                    if (item.InterestID.ToUpper().Equals("ACCESS") &&
                            (item.CoverageID.ToUpper().Equals("ALLRIK") || item.CoverageID.ToUpper().TrimEnd().Equals("TLO")))
                    {
                        IsACCESSSIEnabled = true;
                        ACCESSPremi += item.Premium;
                        if (ACCESSPremi > 0)
                        {
                            IsACCESSChecked = true;
                        }
                        if (item.Year == 1)
                            AccessSI = item.SumInsured;
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("SRCC") || item.CoverageID.ToUpper().Equals("SRCTLO")))
                    {
                        IsSRCCEnabled = true;
                        SRCCPremi += item.Premium;
                        if (SRCCPremi > 0)
                        {
                            IsSRCCChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.SRCC, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("FLD") || item.CoverageID.ToUpper().Equals("FLDTLO")))
                    {
                        IsFLDEnabled = true;
                        FLDPremi += item.Premium;
                        if (FLDPremi > 0)
                        {
                            IsFLDChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.FLD, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("ETV") || item.CoverageID.ToUpper().Equals("ETVTLO")))
                    {
                        IsETVEnabled = true;
                        ETVPremi += item.Premium;
                        if (ETVPremi > 0)
                        {
                            IsETVChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.ETV, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("TRS") || item.CoverageID.ToUpper().Equals("TRRTLO")))
                    {
                        IsTSEnabled = true;
                        TSPremi += item.Premium;
                        if (TSPremi > 0)
                        {
                            IsTSChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.TS, item.Year, item.IsBundling));
                        }
                    }
                    //if(item.InterestID.equals("TPLPER") && item.CoverageID.equals("MVTPL1")) {
                    /** start 0040/URF/2017 */
                    if (item.InterestID.ToUpper().Equals("TPLPER") && item.CoverageID.ToUpper().Contains("MVTP"))
                    {
                        IsTPLEnabled = true;
                        IsTPLSIEnabled = true;
                        TPLPremi += item.Premium;
                        if (TPLPremi > 0)
                        {
                            IsTPLChecked = true;
                        }
                        if (item.Year == 1)
                        {
                            TPLSI = item.SumInsured;
                        }
                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.TPLPER, item.Year, item.IsBundling));
                    } /** end 0040/URF/2017 */
                    if (item.InterestID.ToUpper().Equals("PADRVR") || item.InterestID.ToUpper().Equals("PADDR1"))
                    {
                        IsPADRVRChecked = true;
                        IsPADRVRSIEnabled = true;
                        PADRVRPremi += item.Premium;
                        if (PADRVRPremi > 0)
                        {
                            IsPADRVRChecked = true;
                        }
                        if (item.Year == 1)
                        {
                            PADRVRSI = item.SumInsured;
                        }

                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.PADRVR, item.Year, item.IsBundling));
                    }
                    if (item.InterestID.ToUpper().Equals("PAPASS") || item.InterestID.ToUpper().Equals("PA24AV"))
                    {
                        IsPAPASSSIEnabled = true;
                        IsPASSEnabled = true;

                        PAPASSPremi += item.Premium;
                        if (PAPASSPremi > 0)
                        {
                            IsPAPASSChecked = true;
                        }
                        if (item.Year == 1)
                        {
                            PAPASSSI = item.SumInsured;
                        }
                        PASS = db.ExecuteScalar<int>("SELECT sitting FROM ordersimulationmv WHERE orderno=@0", OrderNo) - 1;
                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.PAPASS, item.Year, item.IsBundling));
                    }
                }
            }
            #region calculate discount renewal
            string OldPolicyNo = db.ExecuteScalar<string>("SELECT OldPolicyNo FROM Ordersimulation WHERE OrderNo=@0", OrderNo);
            if (!string.IsNullOrWhiteSpace(OldPolicyNo))
            {
                var RenDiscount = Repository.vWeb2.RetailRepository.GetRenewalDiscount(OldPolicyNo);
                var Disc = Repository.vWeb2.RetailRepository.GetWTDiscount(ProductCode, MouID);
                foreach (WTCommission ren in RenDiscount)
                {
                    if (ren.CommissionID.Contains("RD"))
                    {
                        RDP.Add(ren.Percentage);
                    }
                }
                foreach (WTCommission D in Disc)
                {
                    DP.Add(D.Percentage);
                }
            }
            Discount = DP;
            RPTRenDiscount = RDP;
            #endregion
        }

        private void getAdminFee()
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    string query = @"select top 1 Policy_Fee1+Stamp_Duty1 from prd_policy_fee where product_code=@0 AND Order_Type=1 AND Lower_limit<=@1  AND Status=1 order by Lower_Limit desc";
                    AdminFee = Convert.ToDouble(db.FirstOrDefault<PRD_POLICY_FEE>(query, ProductCode, SumInsured).Policy_Fee1);
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        #endregion
    }
}
