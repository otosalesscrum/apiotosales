﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using Otosales.Repository.vWeb2;
using PremiumCalculation.Manager;
using PremiumCalculation.Model;
using PremiumCalculation.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;

namespace Otosales.Controllers.vWeb2
{
    /**
     * Rev No   : 001
     * Rev Date : 2016-09-28
     * Dev      : BSY
     * Remark   : Mengubah validasi dalam query penawaran reserved, 
     *            agar hanya agent yang pertama kali bid yang mendapatkan prospect
     */

    [AutoLoggingReference]
    public class SynchronizationController : OtosalesBaseController
    {

        private CalculatePremiRepository cpr = new CalculatePremiRepository();
        #region Declare

        OtosalesAPIResult response;
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        private List<string> ExcludedColumn
        {
            get
            {
                return new List<string>()
                {
                    "FieldChanged", "PrimaryChanged", "RowState"
                };
            }
        }

        #endregion

        #region Sync Methods

        [HttpPost]
        public IHttpActionResult Sync(FormDataCollection form)
        {
            string tableName = form.Get("objectType");
            string data = form.Get("data");

            return Json(SyncData(tableName, data, Util.CONNECTION_STRING_AABMOBILE), Util.jsonSerializerSetting());
        }

        //FollowUp
        [HttpPost]
        public IHttpActionResult DirectSync(FormDataCollection form)
        {
            string data = form.Get("data");
            data = data.Replace("//''", "\\\"");

            ProspectData prospectData = JsonConvert.DeserializeObject<ProspectData>(data);
            ProspectDataResult responseData = new ProspectDataResult();

            if (prospectData.ProspectCustomer != null)
            {
                responseData.ProspectCustomer = SyncData("ProspectCustomer", JsonConvert.SerializeObject(prospectData.ProspectCustomer, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.FollowUp != null)
            {
                responseData.FollowUp = SyncData("FollowUp", JsonConvert.SerializeObject(prospectData.FollowUp, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.FollowUpHistory != null)
            {
                responseData.FollowUpHistory = SyncData("FollowUpHistory", JsonConvert.SerializeObject(prospectData.FollowUpHistory, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulation != null)
            {
                responseData.OrderSimulation = SyncData("OrderSimulation", JsonConvert.SerializeObject(prospectData.OrderSimulation, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulationMV != null)
            {
                responseData.OrderSimulationMV = SyncData("OrderSimulationMV", JsonConvert.SerializeObject(prospectData.OrderSimulationMV, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulationInterest != null)
            {
                responseData.OrderSimulationInterest = SyncData("OrderSimulationInterest", JsonConvert.SerializeObject(prospectData.OrderSimulationInterest, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulationCoverage != null)
            {
                responseData.OrderSimulationCoverage = SyncData("OrderSimulationCoverage", JsonConvert.SerializeObject(prospectData.OrderSimulationCoverage, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.EmailQuotation != null)
            {
                responseData.EmailQuotation = SyncData("EmailQuotation", JsonConvert.SerializeObject(prospectData.EmailQuotation, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;

            }

            if (prospectData.EmailNotification != null)
            {
                responseData.EmailNotification = SyncData("EmailNotification", JsonConvert.SerializeObject(prospectData.EmailNotification, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            return Json(responseData, Util.jsonSerializerSetting());
        }

        private SyncDataResponse SyncData(string tableName, string data, String connection)
        {
            SyncDataResponse response = new SyncDataResponse();
            response.Status = new List<bool>();

            Util.SaveSyncDebug(tableName, data);

            if (connection.Equals(Util.CONNECTION_STRING_AABAGENT))
            {
                Util.CheckAllMGOBidExpiry();
            }

            data = data.Replace("//''", "\\\"");

            List<JObject> items = JsonConvert.DeserializeObject<List<JObject>>(data, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

            Type type = tableName.GetCT();
            PropertyInfo[] propertyInfos = type.GetPI();
            string[] primaryKeys = type.GetPK();

            for (int i = 0; i < items.Count; i++)
            {
                string rowState = items[i].GetValue("RowState").ToString();
                switch (rowState)
                {
                    case "C": response.Status.Add(InsertData(tableName, items[i], propertyInfos, connection)); break;
                    case "U": response.Status.Add(UpdateData(tableName, items[i], propertyInfos, primaryKeys, connection)); break;
                    case "D": response.Status.Add(DeleteData(tableName, items[i], propertyInfos, primaryKeys, connection)); break;
                }
            }
            response.TimeStamp = DateTime.Now;

            return response;
        }

        #endregion

        #region Create Update Delete

        private bool InsertData(string tableName, JObject jObject, PropertyInfo[] propertyInfos, string connection)
        {
            try
            {
                string sqlCommand = "INSERT INTO " + tableName + " ({1}) VALUES ({2})";
                string column = "";
                string value = "";
                JToken _value = null;

                foreach (PropertyInfo info in propertyInfos)
                {
                    if (!(ExcludedColumn.Where(o => o.Contains(info.Name)).Count() > 0))
                    {
                        _value = jObject.GetValue(info.Name);
                        column += "," + info.Name;

                        if (info.Name == "LastUpdatedTime")
                        {
                            value += ",getdate()";
                        }
                        else
                        {
                            if (_value == null)
                                value += ",null";
                            else
                            {
                                if (_value.Type.ToString() == "Date")
                                {
                                    value += ",'" + DateTime.Parse(_value.ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";
                                }
                                else if (_value.Type.ToString().ToLower() == "float" || _value.Type.ToString().ToLower() == "double")
                                {
                                    Double dv = Double.Parse(_value.ToString(), System.Globalization.NumberStyles.Float);
                                    value += ",'" + dv.ToString().Replace(",", ".") + "'";
                                }
                                else
                                {
                                    value += ",'" + _value.ToString().Replace("'", "''").Replace("@", "@@") + "'";
                                }
                            }
                        }
                    }
                }

                column = column.Substring(1);
                value = value.Substring(1);

                sqlCommand = sqlCommand.Replace("{1}", column).Replace("{2}", value);

                using (var db = new a2isDBHelper.Database(connection))
                {
                    db.Execute(sqlCommand);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "SynchronizationController", "InsertData", ex.Message.ToString());
                return true;
            }

            return true;
        }

        private bool UpdateData(string tableName, JObject jObject, PropertyInfo[] propertyInfos, string[] primaryKeys, string connection)
        {
            try
            {
                List<string> fieldChangedList = new List<string>();
                List<string[]> primaryChangedList = new List<string[]>();
                string setValue = "";
                string setFilter = "";
                JToken _value = null;

                using (var db = new a2isDBHelper.Database(connection))
                {
                    foreach (string keyItem in primaryKeys)
                    {
                        //kalau PK berubah, ambil PK dari PrimaryChanged
                        if (primaryChangedList.Where(o => o[0].Contains(keyItem)).Count() > 0)
                            setFilter += " AND " + keyItem + "='" + primaryChangedList.Where(o => o[0] == keyItem).FirstOrDefault()[1].ToString() + "'";
                        else
                        {
                            _value = jObject.GetValue(keyItem);
                            setFilter += " AND " + keyItem + "=" + (_value == null ? "null" : "'" + _value.ToString() + "'");
                        }
                    }
                    setFilter = setFilter.Substring(5);

                    //validate if data not exist, then call InsertData
                    int serverCount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM " + tableName + " WHERE " + setFilter);
                    if (serverCount > 0)
                    {
                        string sqlCommand = "UPDATE " + tableName + " SET {1} WHERE {2}";

                        //  List<string[]> -> PK yang berubah, index[0] -> Nama field,   index[1] -> Value
                        if (jObject.GetValue("PrimaryChanged") != null && jObject.GetValue("PrimaryChanged").ToString() != "")
                            primaryChangedList = JsonConvert.DeserializeObject<List<string[]>>(jObject.GetValue("PrimaryChanged").ToString(), BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

                        //  List<string>  -> Nama Field yang berubah 
                        if (jObject.GetValue("FieldChanged") != null && jObject.GetValue("FieldChanged").ToString() != "" && jObject.GetValue("FieldChanged").ToString() != "[]")
                        {
                            fieldChangedList = JsonConvert.DeserializeObject<List<string>>(jObject.GetValue("FieldChanged").ToString(), BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

                            _value = null;
                            foreach (string fieldChangedItem in fieldChangedList)
                            {
                                if (!(ExcludedColumn.Where(o => o.Contains(fieldChangedItem)).Count() > 0))
                                {
                                    if (fieldChangedItem == "LastUpdatedTime")
                                    {
                                        setValue += "," + fieldChangedItem + "=getdate()";
                                    }
                                    else
                                    {
                                        _value = jObject.GetValue(fieldChangedItem);
                                        string value = null;

                                        if (_value == null)
                                        {
                                            value = "null";
                                        }
                                        else
                                        {
                                            if (_value.Type.ToString() == "Date")
                                            {
                                                value = "'" + DateTime.Parse(_value.ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";
                                            }
                                            else if (_value.Type.ToString().ToLower() == "float" || _value.Type.ToString().ToLower() == "double")
                                            {
                                                //value += ",'" + _value.ToString().Replace(",", ".") + "'";
                                                Double dv = Double.Parse(_value.ToString(), System.Globalization.NumberStyles.Float);
                                                value = "'" + dv.ToString().Replace(",", ".") + "'";
                                            }
                                            else
                                            {
                                                value = "'" + _value.ToString().Replace("'", "''").Replace("@", "@@") + "'";
                                            }
                                        }
                                        setValue += "," + fieldChangedItem + "= " + value;
                                    }
                                }
                            }

                            setValue = setValue.Substring(1);
                            sqlCommand = sqlCommand.Replace("{1}", setValue).Replace("{2}", setFilter);
                            db.Execute(sqlCommand);
                        }
                    }
                    else
                    {
                        return InsertData(tableName, jObject, propertyInfos, connection);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "SynchronizationController", "UpdateData", ex.Message.ToString());
                return false;
            }

            return true;
        }

        private bool DeleteData(string tableName, JObject jObject, PropertyInfo[] propertyInfos, string[] primaryKeys, string connection)
        {
            try
            {
                string sqlCommand = "UPDATE " + tableName + " SET RowStatus=0, LastUpdatedTime=GETDATE() WHERE {1}";
                string setFilter = "";

                List<string[]> primaryChangedList = new List<string[]>();

                //  List<string[]> -> PK yang berubah, index[0] -> Nama field,   index[1] -> Value
                if (jObject.GetValue("PrimaryChanged").ToString() != "")
                    primaryChangedList = JsonConvert.DeserializeObject<List<string[]>>(jObject.GetValue("PrimaryChanged").ToString(), BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

                object _value = null;
                foreach (string keyItem in primaryKeys)
                {
                    //kalau PK berubah, ambil PK dari PrimaryChanged
                    if (primaryChangedList.Where(o => o[0].Contains(keyItem)).Count() > 0)
                        setFilter += " AND " + keyItem + "='" + primaryChangedList.Where(o => o[0] == keyItem).FirstOrDefault()[1].ToString() + "'";
                    else
                    {
                        _value = jObject.GetValue(keyItem);
                        setFilter += " AND " + keyItem + "=" + (_value == null ? "null" : "'" + _value + "'");
                    }
                }

                setFilter = setFilter.Substring(5);

                sqlCommand = sqlCommand.Replace("{1}", setFilter);

                using (var db = new a2isDBHelper.Database(connection))
                {
                    db.Execute(sqlCommand);
                }

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "SynchronizationController", "DeleteData", ex.Message.ToString());
                return false;
            }

            return true;
        }

        #endregion

        #region MGO Bid

        //CheckExpiredForFollowUp
        [HttpPost]
        public IHttpActionResult CheckMGOBidExpiry(FormDataCollection form)
        {
            string ids = form.Get("listHistoryPenawaranID");

            List<int> data = new List<int>();

            // check all mgo bid data first
            Util.CheckAllMGOBidExpiry();

            List<int> idList = JsonConvert.DeserializeObject<List<int>>(ids);
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
            {
                for (int i = 0; i < idList.Count; i++)
                {
                    string query = "SELECT OrderNo, ExpiredDate FROM HistoryPenawaran WHERE HistoryPenawaranID=@0 AND IsAbleExpired = 1";
                    HistoryPenawaran historyPenawaran = db.FirstOrDefault<HistoryPenawaran>(query, idList[i]);

                    if (historyPenawaran != null)
                    {
                        if (DateTime.Compare(Convert.ToDateTime(historyPenawaran.ExpiredDate), DateTime.Now) < 0)
                        {
                            //return true if its expired - EKI
                            data.Add(1);
                            Util.SoftDeleteProspect(historyPenawaran.OrderNo);
                        }
                        else
                        {
                            data.Add(0);
                        }
                    }
                    else
                    {
                        data.Add(0);
                    }
                }
            }

            return Json(data, Util.jsonSerializerSetting());
        }

        //CheckUpdateBidStatus
        [HttpPost]
        public IHttpActionResult GetItMGOBid(FormDataCollection form)
        {
            string PolicyID = form.Get("PolicyID");
            string OrderNo = System.Guid.NewGuid().ToString();
            string SalesOfficerID = form.Get("SalesOfficerID");
            string UserPhone = form.Get("UserPhone");
            string BranchCode = form.Get("BranchCode");

            response = new OtosalesAPIResult();

            int isSendBid;
            int limitPenawaranAgent;
            int totalBidAgent;
            DateTime reserveDate;
            var db2 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
            {

                reserveDate = db.ExecuteScalar<DateTime>("INSERT INTO PenawaranReserved(policyID, SalesOfficerID, CreatedDate) OUTPUT INSERTED.CreatedDate VALUES(@0, @1, getdate())", PolicyID, SalesOfficerID);

                //using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                //{
                isSendBid = db.ExecuteScalar<int>("SELECT IsSendBid FROM AABMobile.dbo.SalesOfficer WHERE SalesOfficerID = @0", SalesOfficerID);
                limitPenawaranAgent = db.ExecuteScalar<int>("SELECT Value FROM AABMobile.dbo.SysParam WHERE Param = 'LimitPenawaranAgent' AND Type = 'MGOBid'");

                string query = "SELECT COUNT(*) as TotalBid";
                query += " FROM HistoryPenawaran hp";
                query += " LEFT JOIN AABMobile.dbo.OrderSimulation os ON os.OrderNo = hp.OrderNo ";
                query += " AND hp.SalesOfficerID = os.SalesOFficerID";
                query += " LEFT JOIN AABMobile.dbo.FollowUp fu ON fu.FollowUpNo = os.FollowUpNo";
                query += " WHERE hp.SalesOfficerID = @0 AND os.RowStatus=1 AND fu.RowStatus=1 ";
                query += " AND (fu.FollowUpStatus IN (1,2,3,4,9) OR (os.OrderNo is null and hp.IsAbleExpired = 1))";
                query += " AND (hp.IsAbleExpired = 0 OR (hp.IsAbleExpired = 1 AND getdate() < hp.ExpiredDate))";
                totalBidAgent = db.ExecuteScalar<int>(query, SalesOfficerID);
                //}

                if (isSendBid > 0)
                {
                    if (limitPenawaranAgent > totalBidAgent)
                    {
                        // 001 begin
                        int getReserveCount = db.ExecuteScalar<int>(@"
                                                select count(*) from PenawaranReserved
                                                where policyid = @0
                                                and PenawaranReservedID = (select min(PenawaranReservedID) from PenawaranReserved where policyid = @0)
                                                and SalesOfficerID = @1
                                            ", PolicyID, SalesOfficerID);
                        // 001 end
                        if (getReserveCount == 1)
                        {
                            int isAvailable = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Penawaran WHERE PolicyID = @0 AND BidStatus = 0", PolicyID);
                            if (isAvailable == 1)
                            {
                                // update bid status to 1
                                db.Execute("UPDATE Penawaran SET BidStatus = 1 WHERE policyID = @0", PolicyID);

                                // generate history penawaran
                                db.Execute(";EXECUTE sp_GenerateHistoryPenawaran @0, @1, @2", PolicyID, OrderNo, SalesOfficerID);

                                query = "SELECT policyID, policyCode, policyGeneratedDate, agentCode, agentEmail, agentFullName, customerName, customerEmail, customerPhone, ";
                                query += "policeNumber, productCode, protectionType, vehicleCode, vehicleType, vehicleDescription, vehicleYear, usageCode, usageText, ";
                                query += "areaCode, totalSumInsured, baseRate, isTJH, tjhRate, tjhValue, isPaDriver, paDriverRate, paDriverValue, isPaPass, ";
                                query += "paPassRate, paPassQuantity, paPassValue, isAutoApply, srccRate, etvRate, fldRate, isCheckSrcc, autoApplyRate, isTRS, ";
                                query += "trsRate, isAcc, accRate, accValue, loadingRate, administrationPrice, basicPremi, accPremi, totalPremiKendaraanPlusAksesoris, ";
                                query += "premiLoading, totalPremiDasarPlusLoading, premiAutoApply, premiTRS, premiTJH, premiPADriver, premiPAPass, totalPremiPerluasan, ";
                                query += "totalPremiDasarPlusPerluasan, totalAllPremi, PDFFile, infoModified, BidStatus, LastUpdatedTime, Domisili, followUpDateTime, isHotProspect, fuByCSOBranch ";
                                query += "FROM Penawaran WHERE policyID = @0 ORDER BY isHotProspect DESC, infoModified DESC, policyID ASC";
                                Penawaran penawaran = db.First<Penawaran>(query, PolicyID);

                                HistoryPenawaran historyPenawaran = db.First<HistoryPenawaran>("SELECT TOP 1 PolicyID, OrderNo, ExpiredDate, LastUpdatedTime, SalesOfficerID, IsAbleExpired, HistoryPenawaranID FROM HistoryPenawaran WHERE PolicyID = @0 ORDER BY LastUpdatedTime DESC", PolicyID);

                                //OTOSALES REACT JS
                                Penawaran actualPenawaran = penawaran;
                                bool isTLO = false;
                                if (actualPenawaran.protectionType.Trim().Equals("TLO"))
                                {
                                    isTLO = true;
                                }
                                else if (!actualPenawaran.protectionType.Trim().Equals("ALLRIK"))
                                {
                                    response.Status = false;
                                    response.Message = "Fatal error occurred! " +
                                            "Invalid data detected on database. Please try again!";
                                    return Json(response, Util.jsonSerializerSetting());
                                }
                                actualPenawaran.isAcc = actualPenawaran.isAcc == null
                                                        ? Convert.ToInt16(0)
                                                        : actualPenawaran.isAcc;
                                decimal accessSI = (Convert.ToDecimal(actualPenawaran.isAcc) > 0 && (actualPenawaran.accValue > 0))
                                                  ? actualPenawaran.accValue
                                                  : 0;

                                actualPenawaran.administrationPrice =
                                        actualPenawaran.administrationPrice == null
                                        ? 0
                                        : actualPenawaran.administrationPrice;
                                actualPenawaran.totalAllPremi = actualPenawaran.totalAllPremi == null
                                                                ? 0
                                                                : actualPenawaran.totalAllPremi;
                                actualPenawaran.totalSumInsured = actualPenawaran.totalSumInsured == null
                                                                  ? 0
                                                                  : actualPenawaran.totalSumInsured;
                                actualPenawaran.paPassValue = actualPenawaran.paPassValue == null
                                                              ? 0
                                                              : actualPenawaran.paPassValue;
                                actualPenawaran.paPassQuantity = actualPenawaran.paPassQuantity == null
                                                                 ? 0
                                                                 : actualPenawaran.paPassQuantity;
                                // create ProspectCustomer record
                                ProspectCustomer prospectCustomer = new ProspectCustomer();
                                prospectCustomer.CustID = System.Guid.NewGuid().ToString();
                                prospectCustomer.Name = actualPenawaran.customerName;
                                prospectCustomer.Phone1 = actualPenawaran.customerPhone;
                                prospectCustomer.Email1 = actualPenawaran.customerEmail;

                                prospectCustomer.SalesOfficerID = SalesOfficerID;
                                prospectCustomer.BranchCode = BranchCode;

                                prospectCustomer.EntryDate = new DateTime();
                                prospectCustomer.RowStatus = true;
                                prospectCustomer.FieldChanged = prospectCustomer.PrimaryChanged = "";

                                // create FollowUp and FollowUpHistory record
                                FollowUp followUp = new FollowUp();
                                FollowUpHistory followUpHistory = new FollowUpHistory();

                                followUp.FollowUpNo = followUpHistory.FollowUpNo = System.Guid.NewGuid().ToString();
                                followUp.LastSeqNo = followUpHistory.SeqNo = 0;
                                followUp.CustID = followUpHistory.CustID = prospectCustomer.CustID;
                                followUp.ProspectName = followUpHistory.ProspectName =
                                        prospectCustomer.Name;
                                followUp.Phone1 = followUpHistory.Phone1 = prospectCustomer.Phone1;
                                followUp.Phone2 = followUpHistory.Phone2 = prospectCustomer.Phone2;
                                followUp.SalesOfficerID = followUpHistory.SalesOfficerID =
                                        prospectCustomer.SalesOfficerID;
                                followUp.EntryDate = followUpHistory.EntryDate =
                                        prospectCustomer.EntryDate;
                                followUp.FollowUpName = followUpHistory.FollowUpName =
                                        prospectCustomer.Name;
                                followUp.FollowUpStatus = followUpHistory.FollowUpStatus = 1;
                                followUp.FollowUpInfo = followUpHistory.FollowUpInfo = 34;
                                followUp.Remark = followUpHistory.Remark = "";
                                followUp.BranchCode = followUpHistory.BranchCode =
                                        prospectCustomer.BranchCode;
                                followUp.FUBidStatus = Convert.ToString(actualPenawaran.BidStatus);

                                followUp.FieldChanged = followUp.PrimaryChanged =
                                        followUpHistory.FieldChanged = followUpHistory.PrimaryChanged = "";
                                followUp.RowStatus = followUpHistory.RowStatus = true;

                                // create OrderSimulation record

                                String newOrderNo = OrderNo;
                                OrderSimulation orderSimulation = new OrderSimulation();
                                orderSimulation.OrderNo = newOrderNo;
                                orderSimulation.CustID = prospectCustomer.CustID;
                                orderSimulation.FollowUpNo = followUp.FollowUpNo;
                                orderSimulation.QuotationNo = penawaran.policyCode;
                                orderSimulation.TLOPeriod = isTLO ? 1 : 0;
                                orderSimulation.ComprePeriod = isTLO ? 0 : 1;
                                orderSimulation.YearCoverage = orderSimulation.TLOPeriod +
                                        orderSimulation.ComprePeriod;
                                orderSimulation.MultiYearF = orderSimulation.YearCoverage > 1;
                                orderSimulation.BranchCode = prospectCustomer.BranchCode;
                                orderSimulation.SalesOfficerID = prospectCustomer.SalesOfficerID;
                                orderSimulation.PhoneSales = UserPhone;
                                orderSimulation.DealerCode = prospectCustomer.DealerCode;
                                orderSimulation.SalesDealer = prospectCustomer.SalesDealer;

                                orderSimulation.ProductCode = actualPenawaran.productCode;
                                // get insurance product detail from Product table
                                Otosales.Models.Product product = db2.FirstOrDefault<Otosales.Models.Product>("SELECT * FROM Product WHERE ProductCode = @0 ", actualPenawaran.productCode);
                                orderSimulation.ProductTypeCode = product.ProductTypeCode;
                                orderSimulation.InsuranceType = product.InsuranceType;

                                orderSimulation.AdminFee = actualPenawaran.administrationPrice;
                                orderSimulation.TotalPremium = actualPenawaran.totalAllPremi;

                                orderSimulation.Phone1 = prospectCustomer.Phone1;
                                orderSimulation.Phone2 = prospectCustomer.Phone2;
                                orderSimulation.Email1 = prospectCustomer.Email1;
                                orderSimulation.Email2 = prospectCustomer.Email2;
                                orderSimulation.SendStatus = 0;
                                orderSimulation.EntryDate = prospectCustomer.EntryDate;
                                orderSimulation.ApplyF = true;
                                orderSimulation.SendF = false;
                                orderSimulation.LastInterestNo = orderSimulation.LastCoverageNo = 0;

                                orderSimulation.RowStatus = true;
                                orderSimulation.FieldChanged = orderSimulation.PrimaryChanged = "";
                                 orderSimulation.PeriodFrom = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
                                 orderSimulation.PeriodTo = new DateTime(DateTime.Now.AddYears(1).Year, DateTime.Now.AddYears(1).Month, DateTime.Now.AddYears(1).Day, 0, 0, 0);
                                // create OrderSimulationMV
                                OrderSimulationMV orderSimulationMV = new OrderSimulationMV();
                                orderSimulationMV.OrderNo = orderSimulation.OrderNo;
                                orderSimulationMV.ObjectNo = 1;
                                orderSimulationMV.ProductTypeCode = orderSimulation.ProductTypeCode;

                                orderSimulationMV.VehicleCode = actualPenawaran.vehicleCode;
                                // get vehicle data from Vehicle table
                                Vehicle vehicle = db2.FirstOrDefault<Vehicle>("SELECT * FROM Vehicle WHERE VehicleCode = @0 AND Year = @1 AND RowStatus = 1"
                                    , actualPenawaran.vehicleCode, actualPenawaran.vehicleYear);
                                orderSimulationMV.BrandCode = vehicle.BrandCode;
                                orderSimulationMV.ModelCode = vehicle.ModelCode;
                                orderSimulationMV.Series = vehicle.Series;
                                orderSimulationMV.Type = actualPenawaran.vehicleType;
                                orderSimulationMV.Sitting =
                                        vehicle.Sitting == null ? 0 : vehicle.Sitting;
                                orderSimulationMV.Year = actualPenawaran.vehicleYear;
                                orderSimulationMV.CityCode = actualPenawaran.areaCode;


                                orderSimulationMV.UsageCode = actualPenawaran.usageCode;
                                orderSimulationMV.SumInsured = actualPenawaran.totalSumInsured;
                                if (Convert.ToInt16(actualPenawaran.isAcc) == 1)
                                {
                                    orderSimulationMV.AccessSI = actualPenawaran.accValue;
                                }
                                orderSimulationMV.RegistrationNumber = actualPenawaran.policeNumber;
                                orderSimulationMV.IsNew =  (Convert.ToInt16(orderSimulationMV.Year)==DateTime.Now.Year)?true:false;

                                orderSimulationMV.RowStatus = true;
                                orderSimulationMV.FieldChanged = orderSimulationMV.PrimaryChanged = "";
                                var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);

                               // create OrderSimulationInterest List and OrderSimulationCoverage List
                                // define productcode based on cartype new or used car 0089/URF/2019
                                string cartype = orderSimulationMV.IsNew ? "newVehicle" : "usedVehicle";
                                orderSimulation.ProductCode=actualPenawaran.productCode = db.ExecuteScalar<string>("SELECT optionValue FROM AppOptions WHERE optionName=@0",cartype);
                                string Ndays = AABDB.ExecuteScalar<string>("SELECT CASE WHEN mp.day_calculation_method =360 THEN 360 ELSE 366 END [NDays] FROM Mst_Product mp WHERE mp.Product_Code=@0", orderSimulation.ProductCode);
                              
                                List<OrderSimulationInterest> interestList = new List<OrderSimulationInterest>();
                                List<OrderSimulationCoverage> coverageList = new List<OrderSimulationCoverage>();
                                CalculatedPremi calculatedPremi = new CalculatedPremi();
                                List<CalculatedPremi> calculatedPremiList = new List<CalculatedPremi>();
                                //actualPenawaran.loadingRate *= 100;
                                // for basic premi
                                #region calculate basic premi
                                string vehiclecode = db2.ExecuteScalar<string>(@"select top 1 vehiclecode from Vehicle v where   
                                                V.BrandCode = @0 
                                                AND V.ModelCode = @1
                                                AND v.year = @2 
                                                AND v.Series =@3
                                                AND v.RowStatus=1",orderSimulationMV.BrandCode,orderSimulationMV.ModelCode,orderSimulationMV.Year,orderSimulationMV.Series);
                                decimal vehicleprice = AABDB.ExecuteScalar<decimal>(@"select top 1 Price from [dbo].[Mst_Vehicle_Price] where vehicle_code=@0 and year=@1 and geo_area_id=@2 order by effective_date desc",vehiclecode, orderSimulationMV.Year, orderSimulationMV.CityCode);
                                List<decimal> RenDiscountPct = new List<decimal>(); 
                                List<decimal> DiscountPct = new List<decimal>();
                                bool isProductSupported = true;
                                calculatedPremi.InterestID = "CASCO ";
                                calculatedPremi.CoverageID = actualPenawaran.protectionType;
                                CoverageParam cp = new CoverageParam();
                                cp.CoverageId = actualPenawaran.protectionType;
                                cp.PeriodFromCover = orderSimulation.PeriodFrom??DateTime.Now;
                                cp.PeriodToCover = orderSimulation.PeriodTo ?? DateTime.Now.AddYears(1) ;
                                List<CoverageParam> cplist= new List<CoverageParam>();
                                cplist.Add(cp);
                                calculatedPremiList = MobileRepository.CalculateBasicPremi(cplist,orderSimulation.ProductCode,orderSimulationMV.CityCode,orderSimulationMV.UsageCode,orderSimulationMV.BrandCode,orderSimulationMV.Type,
                                                                                           orderSimulationMV.ModelCode,Convert.ToInt16(orderSimulationMV.IsNew).ToString(),Convert.ToString(orderSimulationMV.Sitting),orderSimulationMV.Year,
                                                                                           cp.PeriodFromCover,cp.PeriodToCover,Ndays,orderSimulationMV.SumInsured,vehicleprice,"CASCO","",out RenDiscountPct,out isProductSupported);
#endregion
                          //      addInterestItemToList(interestList, coverageList,
                          //              orderSimulation.OrderNo, calculatedPremi, accessSI, orderSimulation, Ndays);
                                // process TPL if selected
                                #region Get rate scoring
                                List<DetailScoring> dtlScoring = new List<DetailScoring>();
                                DetailScoring ds = new DetailScoring();
                                if (!string.IsNullOrEmpty(orderSimulationMV.CityCode))
                                {
                                    ds.FactorCode = "GEOGRA";
                                    ds.InsuranceCode = orderSimulationMV.CityCode;
                                    dtlScoring.Add(ds);
                                }
                                if (!string.IsNullOrEmpty(orderSimulationMV.UsageCode))
                                {
                                    ds = new DetailScoring();
                                    ds.FactorCode = "MVUSAG";
                                    ds.InsuranceCode = orderSimulationMV.UsageCode;
                                    dtlScoring.Add(ds);
                                }
                                ds = new DetailScoring();
                                ds.FactorCode = "VHCTYP";
                                ds.InsuranceCode = !string.IsNullOrEmpty(orderSimulationMV.BrandCode) ? string.IsNullOrEmpty(orderSimulationMV.Type) ? "ALL   " : orderSimulationMV.Type : "";
                                dtlScoring.Add(ds);
                                if (!string.IsNullOrEmpty(orderSimulationMV.BrandCode))
                                {

                                    ds = new DetailScoring();
                                    ds.FactorCode = "VBRAND";
                                    ds.InsuranceCode = orderSimulationMV.BrandCode;
                                    dtlScoring.Add(ds);
                                }
                                if (!string.IsNullOrEmpty(Convert.ToInt16(orderSimulationMV.IsNew).ToString()))
                                {

                                    ds = new DetailScoring();
                                    ds.FactorCode = "NEWVHC";
                                    ds.InsuranceCode = Convert.ToInt16(orderSimulationMV.IsNew).ToString();
                                    dtlScoring.Add(ds);
                                }
                                if (!string.IsNullOrEmpty(orderSimulationMV.ModelCode))
                                {
                                    ds = new DetailScoring();
                                    ds.FactorCode = "VMODEL";
                                    ds.InsuranceCode = orderSimulationMV.ModelCode;
                                    dtlScoring.Add(ds);
                                }

                                #endregion
                                if (Convert.ToInt16(actualPenawaran.isTJH) == 1)
                                {
                                    calculatedPremi = new CalculatedPremi();
                                   // calculatedPremi.InterestID = "TPLPER";
                                    cp.CoverageId = AABDB.ExecuteScalar<string>(@"SELECT DISTINCT Coverage_Id AS CoverageId FROM Mapping_Cover_Progressive WHERE Product_Code =@0 and BC_Up_To_SI=@1", orderSimulation.ProductCode, actualPenawaran.tjhValue);
                                    cp.CoverageId = string.IsNullOrEmpty(cp.CoverageId) ? "MVTPL1" : cp.CoverageId;
                                    calculatedPremi.SumInsured = Convert.ToDouble(actualPenawaran.tjhValue);
                                    calculatedPremiList = MobileRepository.calculatepremicalculation(orderSimulation.ProductCode,"TPLPER",cp,cp.PeriodFromCover,cp.PeriodToCover,orderSimulationMV.Type,
                                                                                                     orderSimulationMV.Year,Convert.ToString(orderSimulationMV.Sitting),dtlScoring,Ndays,actualPenawaran.tjhValue,orderSimulation.ProductCode,
                                                                                                     "", calculatedPremiList, 0, out RenDiscountPct, out DiscountPct);
                                    //addInterestItemToList(interestList, coverageList,
                                    //       orderSimulation.OrderNo, calculatedPremi, accessSI, orderSimulation, Ndays);
                                }

                                if (Convert.ToInt16(actualPenawaran.isAutoApply)==0&&Convert.ToInt16(actualPenawaran.isCheckSrcc) == 1)
                                {
                                    // add SRCC data automate with FLD and ETV
                                    calculatedPremi = new CalculatedPremi();
                                    cp.CoverageId = isTLO ? "SRCTLO" : "SRCC  ";
                                    foreach (DetailScoring dtl in dtlScoring)
                                    {
                                        if (dtl.FactorCode.Equals("VHCTYP"))
                                        {
                                            dtl.InsuranceCode = "ALL";
                                        }
                                    }
                                    calculatedPremiList = MobileRepository.calculatepremicalculation(orderSimulation.ProductCode, "CASCO", cp, cp.PeriodFromCover, cp.PeriodToCover, orderSimulationMV.Type,
                                                                                                     orderSimulationMV.Year, Convert.ToString(orderSimulationMV.Sitting), dtlScoring, Ndays, actualPenawaran.totalSumInsured, orderSimulation.ProductCode,
                                                                                                     "", calculatedPremiList, 0, out RenDiscountPct, out DiscountPct);
                                
                                   }
                                // process terrorism cover if selected
                                if (Convert.ToInt16(actualPenawaran.isTRS) == 1)
                                {
                                    calculatedPremi = new CalculatedPremi();
                                    cp.CoverageId = isTLO ? "TRRTLO" : "TRS   ";
                                    calculatedPremiList = MobileRepository.calculatepremicalculation(orderSimulation.ProductCode, "CASCO", cp, cp.PeriodFromCover, cp.PeriodToCover, orderSimulationMV.Type,
                                                                                                     orderSimulationMV.Year, Convert.ToString(orderSimulationMV.Sitting), dtlScoring, Ndays, actualPenawaran.totalSumInsured, orderSimulation.ProductCode,
                                                                                                     "", calculatedPremiList, 0, out RenDiscountPct, out DiscountPct);
                                }
                                // process driver cover if selected
                                if (Convert.ToInt16(actualPenawaran.isPaDriver) == 1)
                                {
                                    calculatedPremi = new CalculatedPremi();
                                    cp.CoverageId = "PAD1  ";
                                    calculatedPremi.SumInsured = Convert.ToDouble(actualPenawaran.tjhValue);
                                    calculatedPremiList = MobileRepository.calculatepremicalculation(orderSimulation.ProductCode, "PADRVR", cp, cp.PeriodFromCover, cp.PeriodToCover, orderSimulationMV.Type,
                                                                                                     orderSimulationMV.Year, Convert.ToString(orderSimulationMV.Sitting), dtlScoring, Ndays, actualPenawaran.paDriverValue, orderSimulation.ProductCode,
                                                                                                     "", calculatedPremiList, 0, out RenDiscountPct, out DiscountPct);
                                }
                                // process passenger cover if selected
                                if (Convert.ToInt16(actualPenawaran.isPaPass) == 1 && actualPenawaran.paPassQuantity > 0 &&
                                        Convert.ToDouble(actualPenawaran.paPassValue) > 0.0)
                                {
                                    calculatedPremi = new CalculatedPremi();
                                    cp.CoverageId = "PAP1  ";
                                    calculatedPremi.SumInsured = Convert.ToDouble(actualPenawaran.tjhValue);
                                    calculatedPremiList = MobileRepository.calculatepremicalculation(orderSimulation.ProductCode, "PAPASS", cp, cp.PeriodFromCover, cp.PeriodToCover, orderSimulationMV.Type,
                                                                                                     orderSimulationMV.Year, Convert.ToString(orderSimulationMV.Sitting), dtlScoring, Ndays, actualPenawaran.paPassValue, orderSimulation.ProductCode,
                                                                                                     "", calculatedPremiList, 0, out RenDiscountPct, out DiscountPct);
                                }
                                // auto add accessories cover
                                if (accessSI > 0 )
                                {
                                    List<CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(JsonConvert.SerializeObject(calculatedPremiList));
                                    List<CalculatedPremi> cItems = MobileRepository.CalculateACCESS(vehicleprice, accessSI,orderSimulation.ProductCode, cp.PeriodFromCover, cp.PeriodToCover, "ACCESS", orderSimulationMV.Type, orderSimulationMV.Year,
                                                                                    Convert.ToString(orderSimulationMV.Sitting), "", calculateaccess, dtlScoring, "", out RenDiscountPct, out DiscountPct);
                                    calculatedPremiList.AddRange(cItems);
                                }
                                //add calculatedpremi item to interest and coverage
                                foreach(CalculatedPremi cpitem in calculatedPremiList){
                                    addInterestItemToList(interestList, coverageList,
                                            orderSimulation.OrderNo, cpitem, accessSI,orderSimulation,Ndays);
                                }
                                // update LastInterestNo and LastCoverageNo in OrderSimulation
                                orderSimulation.LastInterestNo = interestList.Count();
                                orderSimulation.LastCoverageNo = coverageList.Count();

                                #region INSERT TO DATABASE
                                //PROSPECT CUSTOMER
                                query = @";INSERT INTO ProspectCustomer ([CustID]
      ,[Name]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[CustIDAAB]
      ,[SalesOfficerID]
      ,[DealerCode]
      ,[SalesDealer]
      ,[BranchCode]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,GETDATE(),GETDATE(),1)";

                                //FOLLOW UP
                                db2.Execute(query, prospectCustomer.CustID, prospectCustomer.Name, prospectCustomer.Phone1, prospectCustomer.Phone2, prospectCustomer.Email1, prospectCustomer.Email2, prospectCustomer.CustIDAAB, prospectCustomer.SalesOfficerID, prospectCustomer.DealerCode, prospectCustomer.SalesDealer, prospectCustomer.BranchCode);
                                query = @";INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[PolicyId]
      ,[PolicyNo]
      ,[TransactionNo]
      ,[SalesAdminID]
      ,[SendDocDate]
      ,[IdentityCard]
      ,[STNK]
      ,[SPPAKB]
      ,[BSTB1]
      ,[BSTB2]
      ,[BSTB3]
      ,[BSTB4]
      ,[CheckListSurvey1]
      ,[CheckListSurvey2]
      ,[CheckListSurvey3]
      ,[CheckListSurvey4]
      ,[PaymentReceipt1]
      ,[PaymentReceipt2]
      ,[PaymentReceipt3]
      ,[PaymentReceipt4]
      ,[PremiumCal1]
      ,[PremiumCal2]
      ,[PremiumCal3]
      ,[PremiumCal4]
      ,[FormA1]
      ,[FormA2]
      ,[FormA3]
      ,[FormA4]
      ,[FormB1]
      ,[FormB2]
      ,[FormB3]
      ,[FormB4]
      ,[FormC1]
      ,[FormC2]
      ,[FormC3]
      ,[FormC4]
      ,[FUBidStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38,@39,@40,@41,@42,@43,@44,@45,@46,@47,@48)";
                                db2.Execute(query, followUp.FollowUpNo
          , followUp.LastSeqNo
          , followUp.CustID
          , followUp.ProspectName
          , followUp.Phone1
          , followUp.Phone2
          , followUp.SalesOfficerID
          , followUp.FollowUpName
          , followUp.FollowUpStatus
          , followUp.FollowUpInfo
          , followUp.Remark
          , followUp.BranchCode
          , followUp.PolicyId
          , followUp.PolicyNo
          , followUp.TransactionNo
          , followUp.SalesAdminID
          , followUp.SendDocDate
          , followUp.IdentityCard
          , followUp.STNK
          , followUp.SPPAKB
          , followUp.BSTB1
          , followUp.BSTB2
          , followUp.BSTB3
          , followUp.BSTB4
          , followUp.CheckListSurvey1
          , followUp.CheckListSurvey2
          , followUp.CheckListSurvey3
          , followUp.CheckListSurvey4
          , followUp.PaymentReceipt1
          , followUp.PaymentReceipt2
          , followUp.PaymentReceipt3
          , followUp.PaymentReceipt4
          , followUp.PremiumCal1
          , followUp.PremiumCal2
          , followUp.PremiumCal3
          , followUp.PremiumCal4
          , followUp.FormA1
          , followUp.FormA2
          , followUp.FormA3
          , followUp.FormA4
          , followUp.FormB1
          , followUp.FormB2
          , followUp.FormB3
          , followUp.FormB4
          , followUp.FormC1
          , followUp.FormC2
          , followUp.FormC3
          , followUp.FormC4
          , followUp.FUBidStatus);
                                //FOLLOW UP HISTORY
                                query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[CustID]
      ,[SeqNo]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),1)
";
                                db2.Execute(query, followUpHistory.FollowUpNo
             , followUpHistory.CustID
             , followUpHistory.SeqNo
             , followUpHistory.ProspectName
             , followUpHistory.Phone1
             , followUpHistory.Phone2
             , followUpHistory.SalesOfficerID
             , followUpHistory.FollowUpName
             , followUpHistory.NextFollowUpDate
             , followUpHistory.LastFollowUpDate
             , followUpHistory.FollowUpStatus
             , followUpHistory.FollowUpInfo
             , followUpHistory.Remark
             , followUpHistory.BranchCode);
                                //ORDERSIMULATION
                                query = @";INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[ProductCode]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26)";
                                db2.Execute(query, orderSimulation.OrderNo
               , orderSimulation.CustID
               , orderSimulation.FollowUpNo
               , orderSimulation.QuotationNo
               , orderSimulation.MultiYearF
               , orderSimulation.YearCoverage
               , orderSimulation.TLOPeriod
               , orderSimulation.ComprePeriod
               , orderSimulation.BranchCode
               , orderSimulation.SalesOfficerID
               , orderSimulation.PhoneSales
               , orderSimulation.DealerCode
               , orderSimulation.SalesDealer
               , orderSimulation.ProductTypeCode
               , orderSimulation.AdminFee
               , orderSimulation.TotalPremium
               , orderSimulation.Phone1
               , orderSimulation.Phone2
               , orderSimulation.Email1
               , orderSimulation.Email2
               , orderSimulation.SendStatus
               , orderSimulation.InsuranceType
               , orderSimulation.ApplyF
               , orderSimulation.SendF
               , orderSimulation.LastInterestNo
               , orderSimulation.LastCoverageNo
               , orderSimulation.ProductCode);
                                //ORDERSIMULATION MV
                                query = @" INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[ObjectNo]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
";
                                db2.Execute(query, orderSimulationMV.OrderNo
              , orderSimulationMV.ProductTypeCode
              , orderSimulationMV.VehicleCode
              , orderSimulationMV.ObjectNo
              , orderSimulationMV.BrandCode
              , orderSimulationMV.ModelCode
              , orderSimulationMV.Series
              , orderSimulationMV.Type
              , orderSimulationMV.Sitting
              , orderSimulationMV.Year
              , orderSimulationMV.CityCode
              , orderSimulationMV.UsageCode
              , orderSimulationMV.SumInsured
              , orderSimulationMV.AccessSI
              , orderSimulationMV.RegistrationNumber
              , orderSimulationMV.EngineNumber
              , orderSimulationMV.ChasisNumber
              , orderSimulationMV.LastUpdatedTime
              , orderSimulationMV.RowStatus
              , orderSimulationMV.IsNew);
                                //ORDERSIMULATION INTEREST
                                foreach (OrderSimulationInterest osi in interestList)
                                {
                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1,@8)";
                                    db2.Execute(query, osi.OrderNo, osi.ObjectNo, osi.InterestNo, osi.InterestID, osi.Year, osi.Premium, osi.PeriodFrom, osi.PeriodTo, osi.DeductibleCode);

                                }
                                //ORDERSIMULATION COVERAGE
                                foreach (OrderSimulationCoverage osc in coverageList)
                                {
                                    query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22)";
                                    db2.Execute(query, osc.OrderNo, osc.ObjectNo, osc.InterestNo, osc.CoverageNo, osc.CoverageID, osc.Rate, osc.SumInsured, osc.LoadingRate, osc.Loading, osc.Premium, osc.BeginDate, osc.EndDate, osc.IsBundling ? 1 : 0,
                                                       osc.EntryPct, osc.Ndays, osc.ExcessRate, osc.CalcMethod, osc.CoverPremium, osc.GrossPremium, osc.Net1, osc.Net2, osc.Net3, osc.DeductibleCode);
                                }
                                #endregion
                                response.Data = new { Penawaran = penawaran, HistoryPenawaran = historyPenawaran };
                                response.Message = "'Get It' success";
                                response.Status = true;
                            }
                            else
                            {
                                response.Status = false;
                                response.Message = "This prospect has been taken by another mitra.";
                            }
                        }
                        else
                        {
                            response.Status = false;
                            response.Message = "This prospect has been taken by another mitra.";
                        }
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "Request cannot be processed because you are already taking the maximum number of prospects.";
                    }
                }
                else
                {
                    response.Status = false;
                    response.Message = "You are not authorized to perform this action.";
                }

                db.Execute("DELETE PenawaranReserved WHERE PolicyID = @0", PolicyID); // 001
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        private void addInterestItemToList(List<OrderSimulationInterest> interestList,
                                       List<OrderSimulationCoverage> coverageList,
                                       String orderNo,
                                       CalculatedPremi calculatedPremi,
                                       decimal accessSI, OrderSimulation orderSimulation, string Ndays)
        {
            // prepare OrderSimulationInterest data
            OrderSimulationInterest orderSimulationInterest = null;
            int interestIndex = 0;
            // find and reuse matched Interest item from the list, if any
            for (; interestIndex < interestList.Count(); interestIndex++)
            {
                OrderSimulationInterest interestItem = interestList[interestIndex];
                if (interestItem.OrderNo.Equals(orderNo) &&
                    interestItem.InterestID.Equals(calculatedPremi.InterestID) &&
                    interestItem.Year.Equals(calculatedPremi.Year.ToString()))
                {
                    orderSimulationInterest = interestItem;
                    break;
                }
            }

            if (orderSimulationInterest == null)
            {
                orderSimulationInterest = new OrderSimulationInterest();
                orderSimulationInterest.OrderNo = orderNo;
                orderSimulationInterest.ObjectNo = 1;
                orderSimulationInterest.InterestNo = interestList.Count() + 1;
                orderSimulationInterest.InterestID = calculatedPremi.InterestID;
                orderSimulationInterest.Year = calculatedPremi.Year.ToString();
                orderSimulationInterest.Premium = Convert.ToDecimal(calculatedPremi.Premium);
                DateTime dt = DateTime.Now;
                orderSimulationInterest.PeriodFrom = dt.AddYears(calculatedPremi.Year - 1);
                orderSimulationInterest.PeriodTo = dt.AddYears(calculatedPremi.Year);

                orderSimulationInterest.RowStatus = true;
                orderSimulationInterest.FieldChanged = orderSimulationInterest.PrimaryChanged = "";
                orderSimulationInterest.DeductibleCode = calculatedPremi.DeductibleCode;
                interestList.Add(orderSimulationInterest);
            }
            else
            {
                orderSimulationInterest.Premium += Convert.ToDecimal(calculatedPremi.Premium);
            }

            // prepare OrderSimulationCoverage data
            OrderSimulationCoverage orderSimulationCoverage = new OrderSimulationCoverage();
            orderSimulationCoverage.OrderNo = orderNo;
            orderSimulationCoverage.ObjectNo = orderSimulationInterest.ObjectNo;
            orderSimulationCoverage.InterestNo = orderSimulationInterest.InterestNo;
            orderSimulationCoverage.CoverageNo = coverageList.Count() + 1;
            orderSimulationCoverage.CoverageID = calculatedPremi.CoverageID;
            orderSimulationCoverage.Rate = Convert.ToDecimal(calculatedPremi.Rate);
            orderSimulationCoverage.SumInsured = Convert.ToDecimal(calculatedPremi.SumInsured);
            orderSimulationCoverage.LoadingRate = Convert.ToDecimal(calculatedPremi.LoadingRate);
            orderSimulationCoverage.Loading = Convert.ToDecimal(calculatedPremi.Loading);
            orderSimulationCoverage.Premium = Convert.ToDecimal(calculatedPremi.Premium);
            orderSimulationCoverage.BeginDate = orderSimulationInterest.PeriodFrom;
            orderSimulationCoverage.EndDate = orderSimulationInterest.PeriodTo;
            orderSimulationCoverage.IsBundling = calculatedPremi.IsBundling;

            orderSimulationCoverage.RowStatus = true;
            orderSimulationCoverage.FieldChanged = orderSimulationCoverage.PrimaryChanged = "";
            orderSimulationCoverage.CalcMethod = calculatedPremi.CalcMethod;
            orderSimulationCoverage.CoverPremium = calculatedPremi.CoverPremium;
            orderSimulationCoverage.Net1 = calculatedPremi.Net1;
            orderSimulationCoverage.Net2 = calculatedPremi.Net2;
            orderSimulationCoverage.Net3 = calculatedPremi.Net3;
            orderSimulationCoverage.EntryPct = calculatedPremi.EntryPrecentage;
            orderSimulationCoverage.Ndays = calculatedPremi.Ndays;
            orderSimulationCoverage.ExcessRate = calculatedPremi.ExcessRate;
            orderSimulationCoverage.DeductibleCode = calculatedPremi.DeductibleCode;
            orderSimulationCoverage.GrossPremium = calculatedPremi.GrossPremium;

            coverageList.Add(orderSimulationCoverage);

         
        }

        // UpdateIsAbleExpiredFollowUp
        [HttpPost]
        public IHttpActionResult UpdateIsAbleExpiredFollowUp(FormDataCollection form)
        {
            response = new OtosalesAPIResult();
            int historyPenawaranId = int.Parse(form.Get("HistoryPenawaranID"));

            // check all mgo bid if expired bid still exist
            Util.CheckAllMGOBidExpiry();

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
            {
                // check data not expired
                int bidExist = db.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", historyPenawaranId);

                if (bidExist == 1)
                {
                    // permanently set data to agent
                    db.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", historyPenawaranId);
                    response.Status = true;
                }
                else
                {
                    response.Status = false;
                }
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        #endregion

        #region OrderApproval

        //ApprovalReviseRejectOrder
        [HttpPost]
        public IHttpActionResult ProcessOrderApproval(FormDataCollection form)
        {
            response = new OtosalesAPIResult();
            int OrderId = int.Parse(form.Get("OrderId"));
            string OrderNo = form.Get("OrderNo");
            string userID = form.Get("userID");
            string submitOrder = form.Get("submitOrder");
            string remarksOrder = form.Get("remarksOrder");
            string EmailSubject = "";
            string EmailBody = "";
            List<A2isMessagingAttachment> attFiles = new List<A2isMessagingAttachment>();

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                // set userid that approve/revise/reject order in Order_Mobile_Notification's table
                int notificationId = db.ExecuteScalar<int>("SELECT TOP 1 Notification_ID FROM Order_Mobile_Notification WHERE Order_ID = @0 ORDER BY EntryDt DESC", OrderId);
                db.Execute("UPDATE Order_Mobile_Notification SET User_Id = @0, UpdateDt = GETDATE(), UpdateUsr = @1 WHERE Notification_ID = @2", userID, userID, notificationId);

                Mst_Order_Mobile order = db.First<Mst_Order_Mobile>("SELECT TOP 1 Approval_Status, isSO FROM Mst_Order_Mobile WHERE Order_No like '%" + OrderNo + "%' AND Order_ID = @0 ORDER BY EntryDt DESC", OrderId);
                dynamic mstOrder = db.FirstOrDefault<dynamic>(@"SELECT TOP 1 mom.Email_SA AS Email, mo.Policy_No AS PolicyNo, mo.Name_on_policy AS CustomerName, mu.User_Name AS SAName FROM Mst_Order mo
															  INNER JOIN Mst_User mu ON mu.User_Id = @1
															  INNER JOIN Mst_Order_Mobile mom on mom.Order_No=mo.Order_No
                                                              INNER JOIN Mst_Salesman ms ON ms.Salesman_Id = mo.Salesman_Id Where mo.Order_No=@0 ", OrderNo, userID);
                // get order status if any
                List<string> orderStatus = db.Fetch<string>("SELECT Order_Status FROM Mst_Order WHERE Order_No like '%" + OrderNo + "%' AND Order_Status in ('6','9','10','11')");

                if (order.Approval_Status.Equals(0)) // if order not approved yet
                {
                    if (orderStatus.Count < 1) // if order status not in (6,9,10,11)
                    {
                        switch (submitOrder)
                        {
                            case "Approve":
                                // approve order
                                db.Execute("UPDATE Mst_Order_Mobile SET Approval_Status = 1, UpdateDt = GETDATE(), UpdateUsr = @0 WHERE Order_ID = @1", userID, OrderId);

                                response.Status = true;
                                response.Message = "Order approved";
                                break;

                            case "Revise":
                                // revise order
                                db.Execute("UPDATE Mst_Order_Mobile SET Approval_Status = 2, Remarks = @0, UpdateDt = GETDATE(), UpdateUsr = @1 WHERE Order_ID = @2", remarksOrder, userID, OrderId);

                                // activate email notification
                                int notificationID = db.ExecuteScalar<int>("SELECT Top 1 Notification_ID from Order_Mobile_Notification where Order_ID = @0 order by (CASE WHEN UpdateDt is null THEN EntryDt ELSE UpdateDt END)", OrderId);
                                db.Execute("UPDATE Order_Mobile_Notification SET Email_Status = 0 WHERE Notification_ID = @0", notificationId);


                                response.Status = true;
                                response.Message = "Order sent back to SA";
                                break;

                            case "Reject":
                                // reject order
                                db.Execute("UPDATE Mst_Order_Mobile SET Approval_Status = 2, UpdateDt = GETDATE(), UpdateUsr = @0 WHERE Order_ID = @1", userID, OrderId);

                                response.Status = true;
                                response.Message = "Order rejected";
                                break;

                            default:
                                response.Status = false;
                                response.Message = "Failed to approve/revise/reject";
                                break;
                        }
                    }
                    else
                    {
                        string username = db.ExecuteScalar<string>("SELECT TOP 1 us.UserName as Name FROM AAB.dbo.Approval ms INNER JOIN a2isAuthorizationDB.General.Users us ON us.UserID = ms.UpdateUsr WHERE ms.Reference_No like '%" + OrderNo + "%' ORDER BY (CASE WHEN UpdateDt is null THEN EntryDt ELSE UpdateDt END) DESC");

                        switch (int.Parse(orderStatus[0]))
                        {
                            case 9:
                                response.Status = false;
                                response.Message = "This policy has been approved by " + username;
                                break;

                            case 10:
                                response.Status = false;
                                response.Message = "This policy has been approved by " + username;
                                break;

                            case 11:
                                response.Status = false;
                                response.Message = "This policy has been approved by " + username;
                                break;

                            case 6:
                                response.Status = false;
                                response.Message = "This policy has been rejected by " + username;
                                break;
                        }
                    }
                }
                else
                {
                    string username = db.ExecuteScalar<string>("SELECT TOP 1 us.UserName as Name, us.UserID FROM AAB.dbo.Mst_Order_Mobile ms INNER JOIN a2isAuthorizationDB.General.Users us ON us.UserID = ms.UpdateUsr WHERE ms.Order_No like '%" + OrderNo + "%' ORDER BY (CASE WHEN UpdateDt is null THEN EntryDt ELSE UpdateDt END) DESC");

                    switch (order.Approval_Status)
                    {
                        case 1:
                            response.Status = false;
                            response.Message = "This policy has been approved by " + username;
                            break;
                        case 2:
                            if (order.isSO.Equals(1))
                            {
                                response.Status = false;
                                response.Message = "This policy has been revised by " + username;
                            }
                            else if (order.isSO.Equals(0))
                            {
                                response.Status = false;
                                response.Message = "This policy has been rejected by " + username;
                            }
                            else
                            {
                                response.Status = false;
                                response.Message = "Check database, wrong SO status for this order";
                            }
                            break;
                        default:
                            response.Status = false;
                            response.Message = "Check database, wrong approval status for this order";
                            break;
                    }
                }
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        #endregion
    }
}
