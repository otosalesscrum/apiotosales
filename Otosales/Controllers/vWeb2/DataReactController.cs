﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Microsoft.Reporting.WinForms;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;
using Otosales.dta;
using Otosales.Repository.vWeb2;
using PremiumCalculation.Model;
using PremiumCalculation.BussinessInterface;
using PremiumCalculation.Manager;
using System.Web;
using System.Text.RegularExpressions;

namespace Otosales.Controllers.vWeb2
{
    [AutoLoggingReference]
    public class DataReactController : OtosalesBaseController
    {
        private PremiumCalculationManager pm = new PremiumCalculationManager();
        private static readonly a2isLogHelper _log = new a2isLogHelper();

        #region Properties
        IMobileRepository mobileRepository;
        IAABRepository aabRepository;
        public DataReactController()
        {
            mobileRepository = new MobileRepository();
            aabRepository = new AABRepository();
        }
        public DataReactController(IMobileRepository repository)
        {
            mobileRepository = repository;
        }
        public DataReactController(IAABRepository repository)
        {
            aabRepository = repository;
        }
        #endregion
        #region Rate Calculation Global Variable
        private string ProductCode = "";
        private string CityCode = "";
        private string type = "";
        private string UsageCode = "";
        private int Year = 0;

        private bool IsSRCCChecked = false;
        private bool IsFLDChecked = false;
        private bool IsETVChecked = false;
        private bool IsTSChecked = false;
        private bool IsTPLChecked = false;
        private bool IsPAPASSChecked = false;
        private bool IsPADRVRChecked = false;
        private bool IsTPLSIEnabled = false;
        private bool IsPASSEnabled = false;
        private bool IsPAPASSSIEnabled = false;
        private bool IsPADRVRSIEnabled = false;
        private bool IsACCESSChecked = false;
        private bool IsACCESSSIEnabled = false;


        private bool IsSRCCEnabled = false;
        private bool IsETVEnabled = false;
        private bool IsFLDEnabled = false;
        private bool IsTSEnabled = false;
        private bool IsPAPASSEnabled = false;
        private bool IsPADRVREnabled = false;
        private bool IsACCESSEnabled = false;
        private bool IsTPLEnabled = false;

        private string TPLCoverageID = "";
        private double TPLSI = 0;
        private double PADRVRSI = 0;
        private double PAPASSSI = 0;
        private int PASS = 0;
        private double AccessSI = 0;

        private double BasicPremi = 0;
        private double SumInsured = 0;
        private double SRCCPremi = 0;
        private double FLDPremi = 0;
        private double ETVPremi = 0;
        private double TSPremi = 0;
        private double ACCESSPremi = 0;
        private double TPLPremi = 0;
        private double PADRVRPremi = 0;
        private double PAPASSPremi = 0;
        private double LoadingPremi = 0;
        private double TotalPremi = 0;
        private double GrossPremi = 0;
        private double AdminFee = 0;
        private double VehiclePrice = 0;


        List<CalculatedPremi> CalculatedPremiItems = new List<CalculatedPremi>();
        List<CalculatedPremi> InterestCoverageItems = new List<CalculatedPremi>();
        List<Prd_Load_Mv> loadingMaster = new List<Prd_Load_Mv>();
        List<Prd_Agreed_Value> prdAgreedValueMaster = new List<Prd_Agreed_Value>();
        List<CoveragePeriod> coveragePeriodItems = new List<CoveragePeriod>();
        List<Prd_Interest_Coverage> prdInterestCoverage = new List<Prd_Interest_Coverage>();


        VehiclePriceTolerance vptModel;
        ProspectCustomer pcModel;
        List<OrderSimulationInterest> osiDeletedModels;
        List<OrderSimulationCoverage> oscDeletedModels;
        List<CoveragePeriodNonBasic> coveragePeriodNonBasicItems = new List<CoveragePeriodNonBasic>();


        #endregion

        #region rate Calculation Helper Function

        private void RefreshBundlingPremi()
        {
            string query = "";
            #region RefreshingBundlingPremi
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            #region Initialize SetCoverageTypeNonBasicItems
            SetCoverageTypeNonBasicItems();
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            if (CalculatedPremiItems.Count() > 0)
            {
                for (int i = CalculatedPremiItems.Count(); i > 0; i--)
                {
                    CalculatedPremi cp = CalculatedPremiItems[i - 1];
                    if ((cp.CoverageID.Equals("SRCC  ") || cp.CoverageID.Equals("SRCTLO") ||
                            cp.CoverageID.Equals("FLD   ") || cp.CoverageID.Equals("FLDTLO") ||
                            cp.CoverageID.Equals("ETV   ") || cp.CoverageID.Equals("ETVTLO")))
                    {
                        CalculatedPremiItems.RemoveAt(i - 1);
                    }
                }
            }

            if (IsSRCCChecked || IsFLDChecked || IsETVChecked)
            {
                foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                {
                    query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, item.Year * 12.0);
                    // HOTFIX 20151120 - BSY


                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC)
                    {
                        CalculatePremi("CASCO ",
                                   item.CoverageType == CoverageType.Comprehensive ? "SRCC  " : "SRCTLO",
                                   item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured,
                                   item.IsBundling);
                    }
                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD)
                    {
                        CalculatePremi("CASCO ",
                                item.CoverageType == CoverageType.Comprehensive ? "FLD   " : "FLDTLO",
                                item.Year, Convert.ToDouble(
                                agreedValue.Rate / 100) * SumInsured,
                                item.IsBundling);
                    }
                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                    {
                        CalculatePremi("CASCO ",
                                item.CoverageType == CoverageType.Comprehensive ? "ETV   " : "ETVTLO",
                                item.Year, Convert.ToDouble(
                                agreedValue.Rate / 100) * SumInsured,
                                item.IsBundling);
                    }
                }

                if (IsSRCCChecked)
                {
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.CoverageID.TrimEnd().Equals("SRCC") || cp.CoverageID.TrimEnd().Equals("SRCTLO"))
                        {
                            SRCCPremi += cp.Premium;
                        }
                        if (cp.CoverageID.TrimEnd().Equals("FLD") || cp.CoverageID.TrimEnd().Equals("FLDTLO"))
                        {
                            FLDPremi += cp.Premium;
                        }
                        if (cp.CoverageID.TrimEnd().Equals("ETV") || cp.CoverageID.TrimEnd().Equals("ETVTLO"))
                        {
                            ETVPremi += cp.Premium;
                        }
                    }
                }

            }
            if (AccessSI > 0)
            {
                CalculateACCESSPremi();
            }

            CalculateLoading();
            CalculateTotalPremi();
            #endregion
            #endregion
        }

        private void CalculateACCESSPremi()
        {

            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.InterestID.Equals("ACCESS"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            for (int i = 0; i < CalculatedPremiItems.Count(); i++)
            {
                CalculatedPremi cp = CalculatedPremiItems[i];
                if (cp.InterestID.Equals("CASCO "))
                {
                    double premi = 0;
                    double loadPremi = 0;
                    double sumInsured = 0;
                    string query = "SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, cp.Year * 12.0);
                    sumInsured = AccessSI * Convert.ToDouble(agreedValue.Rate / 100);
                    premi = sumInsured * cp.Rate / 100;

                    CalculatedPremi calculatedPremi = new CalculatedPremi();
                    calculatedPremi.InterestID = "ACCESS";
                    calculatedPremi.CoverageID = cp.CoverageID;
                    calculatedPremi.Year = cp.Year;
                    calculatedPremi.Rate = cp.Rate;
                    calculatedPremi.Premium = premi;
                    calculatedPremi.SumInsured = sumInsured;
                    calculatedPremi.LoadingRate = cp.LoadingRate;
                    calculatedPremi.Loading = loadPremi;
                    calculatedPremi.PeriodFrom = DateTime.Now;
                    calculatedPremi.PeriodTo = DateTime.Now;

                    if (cp.CoverageID.Equals("ALLRIK") || cp.CoverageID.Equals("TLO   "))
                    {
                        calculatedPremi.LoadingRate = cp.LoadingRate;
                        loadPremi = cp.LoadingRate / 100 * premi;
                        calculatedPremi.Loading = loadPremi;
                    }
                    calculatedPremi.IsBundling = cp.IsBundling;
                    CalculatedPremiItems.Add(calculatedPremi);
                }
            }

            ACCESSPremi = 0;
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;

            foreach (CalculatedPremi cp in CalculatedPremiItems)
            {
                if (cp.InterestID.Equals("ACCESS") &&
                        (cp.CoverageID.Equals("ALLRIK") || cp.CoverageID.Equals("TLO   ")))
                {
                    ACCESSPremi += cp.Premium;
                    IsACCESSChecked = true;
                }
                else if (cp.CoverageID.Equals("SRCC  ") || cp.CoverageID.Equals("SRCTLO"))
                {
                    SRCCPremi += cp.Premium;
                    IsSRCCChecked = true;
                }
                else if (cp.CoverageID.Equals("FLD   ") || cp.CoverageID.Equals("FLDTLO"))
                {
                    FLDPremi += cp.Premium;
                    IsFLDChecked = true;
                }
                else if (cp.CoverageID.Equals("ETV   ") || cp.CoverageID.Equals("ETVTLO"))
                {
                    ETVPremi += cp.Premium;
                    IsETVChecked = true;
                }
                else if (cp.CoverageID.Equals("TRS   ") || cp.CoverageID.Equals("TRRTLO"))
                {
                    TSPremi += cp.Premium;
                    IsTSChecked = true;
                }
            }

            CalculateLoading();
            CalculateTotalPremi();
        }

        private void CalculateLoading()
        {
            LoadingPremi = 0;
            foreach (CalculatedPremi item in CalculatedPremiItems)
                LoadingPremi += item.Loading;
        }
        private string CheckSumInsured()
        {
            string message = "";
            if (vptModel != null)
            {
                switch (vptModel.ToleranceType)
                {
                    case 0:
                        if (SumInsured != VehiclePrice)
                        {
                            message = "The sum insured of your vehicle has exceeded the limit of market price";
                            SumInsured = VehiclePrice;
                        }
                        break;
                    case 1:
                        if (SumInsured > VehiclePrice +
                                (VehiclePrice * Convert.ToDouble(vptModel.TolerancePct / 100)) ||
                                SumInsured < VehiclePrice -
                                        (VehiclePrice * Convert.ToDouble(vptModel.TolerancePct / 100)))
                        {
                            message = "The sum insured of your vehicle has exceeded +/- " +
                                    Convert.ToInt32(vptModel.TolerancePct) + "% of market price";
                            SumInsured = VehiclePrice;
                        }
                        break;
                    case 2:
                        if (SumInsured > VehiclePrice + Convert.ToDouble(vptModel.ToleranceAmt) ||
                                SumInsured < VehiclePrice - Convert.ToDouble(vptModel.ToleranceAmt))
                        {
                            message = "The sum insured of your vehicle has exceeded +/- " +
                                    Util.ToThousand(vptModel.ToleranceAmt) +
                                    " of market price";
                            SumInsured = VehiclePrice;
                        }
                        break;
                }

            }

            return message;
        }

        private void CalculateTotalPremi()
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    TotalPremi = 0;
                    GrossPremi = 0;

                    IsTPLEnabled = false;
                    foreach (CalculatedPremi item in CalculatedPremiItems)
                    {
                        TotalPremi += Convert.ToDouble(item.Net3);
                        GrossPremi += Convert.ToDouble(item.GrossPremium);
                        //CalculatePremiPerCoverage(item.InterestID, item.CoverageID, Convert.ToDouble(item.Premium));
                       
                        if (item.CoverageID.TrimEnd().Equals("ALLRIK"))
                        {
                            IsTPLEnabled = true;
                        }
                    }


                    string query = @"select top 1 Policy_Fee1 from prd_policy_fee where product_code=@0 AND Order_Type=1 AND Lower_limit<=@1  AND Status=1 order by Lower_Limit desc";
                    AdminFee = db.ExecuteScalar<double>(query, ProductCode, TotalPremi);

                    // 003 begin
                    if (TotalPremi != 0)
                    {
                        TotalPremi += AdminFee; //+ LoadingPremi;
                    }
                    // 003 end
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
                   }

        private void InitializePremi()
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            if (coveragePeriodItems.Count() > 0 && prdAgreedValueMaster.Count() > 0)
            {

                foreach (CoveragePeriod item in coveragePeriodItems)
                {
                    string query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, prdAgreedValueMaster[0].Product_Code, item.Year * 12.0);

                    CalculatePremi("CASCO ",
                            item.CoverageType == CoverageType.Comprehensive ? "ALLRIK" : "TLO   ",
                            item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, false);

                    List<CoveragePeriodNonBasic> cperiodNonBasicitem = new List<CoveragePeriodNonBasic>();
                    foreach (CoveragePeriodNonBasic cp in coveragePeriodNonBasicItems)
                    {
                        if (cp.Year.Equals(item.Year))
                        {
                            cperiodNonBasicitem.Add(cp);
                        }
                    }

                    foreach (CoveragePeriodNonBasic item2 in cperiodNonBasicitem)
                    {
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC)
                        {
                            CalculatePremi("CASCO ",
                                     item.CoverageType == CoverageType.Comprehensive
                                             ? "SRCC  "
                                             : "SRCTLO",
                                     item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, item2.IsBundling);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD)
                        {
                            CalculatePremi("CASCO ",
                                    item.CoverageType == CoverageType.Comprehensive
                                            ? "FLD   "
                                            : "FLDTLO",
                                    item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, item2.IsBundling);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                        {
                            CalculatePremi("CASCO ",
                                    item.CoverageType == CoverageType.Comprehensive
                                            ? "ETV   "
                                            : "ETVTLO",
                                    item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, item2.IsBundling);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                        {
                            CalculatePremi("CASCO ",
                                    item.CoverageType == CoverageType.Comprehensive
                                            ? "TRS   "
                                            : "TRRTLO",
                                    item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, false);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER && TPLSI > 0)
                        {
                            // CalculatePremi("TPLPER", "MVTPL1", item.Year, TPLSI, false);
                            // 004
                            CalculatePremi("TPLPER", TPLCoverageID,
                                    item.Year, TPLSI, false);
                            // 004
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR && PADRVRSI > 0)
                        {
                            CalculatePremi("PADRVR", "", item.Year, PADRVRSI, false);
                        }
                        if (item2.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS &&
                                PAPASSSI > 0 && PASS > 0)
                        {
                            CalculatePremi("PAPASS",
                                    "C" + PASS,
                                    item.Year, PAPASSSI, false);
                        }
                    }
                }

                RefreshAccessSI();
                RefreshPremi();
            }
        }
        private void RefreshPremi()
        {   
            BasicPremi = 0;
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;
            TPLPremi = 0;
            PADRVRPremi = 0;
            PAPASSPremi = 0;
            ACCESSPremi = 0;
            LoadingPremi = 0;

            foreach (CalculatedPremi x in CalculatedPremiItems)
            {
                GrossPremi += Convert.ToDouble(x.GrossPremium);
                if (x.InterestID.TrimEnd().Equals("CASCO") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    BasicPremi += Convert.ToDouble(x.Premium);
                }
                else if (x.InterestID.TrimEnd().Equals("ACCESS") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    ACCESSPremi += Convert.ToDouble(x.Net3);
                    if (ACCESSPremi > 0) {
                        IsACCESSChecked = true;
                    }
                    IsACCESSSIEnabled = true;
                }
                else if (x.CoverageID.TrimEnd().Equals("SRCC") || x.CoverageID.TrimEnd().Equals("SRCTLO") || x.CoverageID.TrimEnd().Equals("SRCCTS"))
                {
                    SRCCPremi += Convert.ToDouble(x.Net3);
                    if (SRCCPremi > 0) {
                        IsSRCCChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("FLD") || x.CoverageID.TrimEnd().Equals("FLDTLO"))
                {
                    FLDPremi += Convert.ToDouble(x.Net3);
                    if (FLDPremi > 0)
                    { 
                    IsFLDChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("ETV") || x.CoverageID.TrimEnd().Equals("ETVTLO") || x.CoverageID.TrimEnd().Equals("EQK"))
                {
                    ETVPremi += Convert.ToDouble(x.Net3);
                    if (ETVPremi > 0)
                    { 
                    IsETVChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("TRS") || x.CoverageID.TrimEnd().Equals("TRRTLO"))
                {
                    TSPremi += Convert.ToDouble(x.Net3);

                    if (TSPremi > 0)
                    {
                        IsTSChecked = true;
                    }
                    IsTSEnabled = true;
                    /** start 0040/URF/2017 */
                    //} else if (x.InterestID.equals("TPLPER") && (x.CoverageID.equals("MVTPL1"))) {
                }
                else if (x.InterestID.Equals("TPLPER") && (x.CoverageID.Contains("MVTP")))
                {
                    TPLPremi += Convert.ToDouble(x.Net3);

                    if (TPLPremi > 0||x.Rate==-1)
                    {
                        IsTPLChecked = true;
                    }
                    IsTPLEnabled = true;
                    IsTPLSIEnabled = true;
                }
                else if (x.InterestID.Equals("PADRVR") || x.InterestID.Equals("PADDR1"))
                {
                    PADRVRPremi += Convert.ToDouble(x.Net3);
                    if (PADRVRPremi > 0)
                    {
                        IsPADRVRChecked = true;
                    }
                    IsPADRVRSIEnabled = true;
                }
                else if (x.InterestID.Equals("PAPASS") || x.InterestID.Equals("PA24AV"))
                {
                    PAPASSPremi +=Convert.ToDouble(x.Net3);

                    if (PAPASSPremi > 0)
                    {
                        IsPADRVRChecked = true;
                        IsPAPASSChecked = true;
                    }
                    IsPAPASSEnabled = true;
                    IsPAPASSSIEnabled = true;
                    IsPASSEnabled = true;

                }
                
            }
        }

        private void SetCoverageTypeNonBasicItems()
        {
            foreach (CoveragePeriod item in coveragePeriodItems)
            {
                foreach (CoveragePeriodNonBasic item2 in coveragePeriodNonBasicItems)
                {
                    if (item.Year == item2.Year)
                    {
                        item2.CoverageType = item.CoverageType;
                    }
                }
            }
        }
        private void RefreshPAPASS(bool chPAPASS)
        {
            // PAPASSSI = OtoSalesUtil.convertToDoubleMoney(etPAPASSSI.getText());
            PAPASSPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.InterestID.Equals("PAPASS"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            if (PAPASSSI > 0 && PASS > 0)
            {
                SetCoverageTypeNonBasicItems();
                if (chPAPASS)
                {
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS)
                        {
                            CalculatePremi("PAPASS", "C" + Convert.ToString(PASS),
                                    item.Year, PAPASSSI, false);
                        }
                    }
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.InterestID.Equals("PAPASS"))
                        {
                            PAPASSPremi += cp.Premium;
                        }
                    }
                }
            }
            //  PAPASSPremi = Util.ToThousand(Convert.ToDecimal(;
            CalculateTotalPremi();
            //etPAPASSSI.setText(OtoSalesUtil.convertThousand(PAPASSSI));
        }

        private void RefreshTPL(bool chTPL)
        {
            /** start 0040/URF/2017 */
            //        TPLSI = OtoSalesUtil.convertToDoubleMoney(etTPLSI.getText());
            //TPLSI = OtoSalesUtil.convertToDoubleMoney(spinnerTpl.getItemSelected());
            /** end 0040/URF/2017 */
            TPLPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                /** start 0040/URF/2017 */
                //            if (cp.InterestID.equals("TPLPER") && cp.CoverageID.equals("MVTPL1")) {
                //                CalculatedPremiItems.remove(i - 1);
                //            }
                if (cp.InterestID.Equals("TPLPER") && cp.CoverageID.Contains("MVTP"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
                /** end 0040/URF/2017 */
            }

            if (TPLSI > 0)
            {
                SetCoverageTypeNonBasicItems();
                if (chTPL)
                {
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER)
                        {
                            /** start 0040/URF/2017 */
                            //                         CalculatePremi("TPLPER", "MVTPL1", item.Year, TPLSI, false);
                            CalculatePremi("TPLPER", TPLCoverageID,
                                    item.Year, TPLSI, false);
                            /** end 0040/URF/2017 */
                        }
                    }
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.InterestID.Equals("TPLPER"))
                        {
                            TPLPremi += cp.Premium;
                        }
                    }
                }
            }

            //tvTPL.setText(OtoSalesUtil.convertThousand(TPLPremi));
            CalculateTotalPremi();
            //        etTPLSI.setText(OtoSalesUtil.convertThousand(TPLSI));
            //spinnerTpl.setSelection(spinnerTpl.getSelectedItemPosition());
        }


        private void RefreshPADRVR(bool chPADRVR)
        {
            // PADRVRSI = OtoSalesUtil.convertToDoubleMoney(etPADRVRSI.getText());
            PADRVRPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.InterestID.Equals("PADRVR"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            if (PADRVRSI > 0)
            {
                SetCoverageTypeNonBasicItems();
                if (chPADRVR)
                {
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR)
                        {
                            CalculatePremi("PADRVR", "", item.Year, PADRVRSI, false);
                        }
                    }
                    foreach (CalculatedPremi cp in CalculatedPremiItems)
                    {
                        if (cp.InterestID.Equals("PADRVR"))
                        {
                            PADRVRPremi += cp.Premium;
                        }
                    }
                }
            }

            //tvPADRVR.setText(OtoSalesUtil.convertThousand(PADRVRPremi));
            CalculateTotalPremi();
            //etPADRVRSI.setText(OtoSalesUtil.convertThousand(PADRVRSI));
        }


        private void RefreshAccessSI()
        {
            ACCESSPremi = 0;

            if (AccessSI > 0)
            {
                CalculateACCESSPremi();
            }
            else
            {
                for (int i = CalculatedPremiItems.Count(); i > 0; i--)
                {
                    CalculatedPremi cp = CalculatedPremiItems[i - 1];
                    if (cp.InterestID.Equals("ACCESS"))
                    {
                        CalculatedPremiItems.RemoveAt(i - 1);
                    }
                }
                ACCESSPremi = 0;
                //  tvAccessPremi.setText(OtoSalesUtil.convertThousand(ACCESSPremi));
            }

            RefreshBundlingPremi();
            RefreshTS();
            CalculateLoading();
            CalculateTotalPremi();
            //if (!etAccessSI.getText().equals(OtoSalesUtil.convertThousand(AccessSI)))
            //{
            //    etAccessSI.setText(OtoSalesUtil.convertThousand(AccessSI));
            //}
        }

        private void RefreshTS()
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            SetCoverageTypeNonBasicItems();

            TSPremi = 0;

            for (int i = CalculatedPremiItems.Count(); i > 0; i--)
            {
                CalculatedPremi cp = CalculatedPremiItems[i - 1];
                if (cp.CoverageID.Equals("TRS   ") || cp.CoverageID.Equals("TRRTLO"))
                {
                    CalculatedPremiItems.RemoveAt(i - 1);
                }
            }

            if (IsTSChecked)
            {
                foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                {
                    if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                    {
                        string query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                        Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, item.Year * 12.0);

                        CalculatePremi("CASCO ",
                                item.CoverageType == CoverageType.Comprehensive ? "TRS   " : "TRRTLO",
                                item.Year, Convert.ToDouble(agreedValue.Rate / 100) * SumInsured, false);
                    }
                }

                foreach (CalculatedPremi cp in CalculatedPremiItems)
                {
                    if (cp.CoverageID.Equals("TRS   ") || cp.CoverageID.Equals("TRRTLO"))
                    {
                        TSPremi += cp.Premium;
                    }
                }
            }

            if (AccessSI > 0)
            {
                CalculateACCESSPremi();
            }

            CalculateLoading();
            CalculateTotalPremi();
        }

        private void CalculatePremi(String interestID, String coverageID, int year,
                                double sumInsured, bool isBundling)
        {
            double premi = 0;
            double loadPremi = 0;
            double rateLoad = 0;
            double rate = 0;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            string query = "SELECT * FROM Prd_Interest_Coverage WHERE Product_Code='" +
                ProductCode + "' AND Interest_Id='" + interestID + "'" + (string.IsNullOrEmpty(coverageID) ? " " : " AND Coverage_Id='" + coverageID + "'");
            List<Prd_Interest_Coverage> prdInterestCoverages = db.Fetch<Prd_Interest_Coverage>(query);
            Prd_Interest_Coverage interestCoverage = prdInterestCoverages[0];

            if (interestCoverage != null)
            {
                // 003 begin
                if ((coverageID.Equals("ALLRIK") || coverageID.Equals("TLO   ")) || coverageID.Contains("MVTP") ||
                        isBasicCoverExist(CalculatedPremiItems))
                {
                    if (!string.IsNullOrEmpty(interestCoverage.Scoring_Code.TrimEnd()))
                    {
                        List<Dtl_Rate_Factor> rateFactorItems;

                        try
                        {
                            rateFactorItems = db.Fetch<Dtl_Rate_Factor>(
                                    "SELECT * FROM Dtl_Rate_Factor WHERE Score_Code='" +
                                            interestCoverage.Scoring_Code +
                                            "' AND Insurance_type='000401' AND Coverage_id='" +
                                            interestCoverage.Coverage_Id + "' ORDER BY sequence_no ASC");
                        }
                        catch (Exception ex)
                        {
                            rateFactorItems = new List<Dtl_Rate_Factor>();
                        }

                        if (rateFactorItems.Count() > 0)
                        {
                            Dtl_Rate_Scoring rateScoring = new Dtl_Rate_Scoring();
                            try
                            {
                                rateScoring = GetRateScoring(rateFactorItems,
                                 interestCoverage.Scoring_Code, interestCoverage.Coverage_Id,
                                sumInsured, false, CityCode, type, UsageCode);
                            }
                            catch (Exception ex)
                            {
                                rateScoring = null;
                            }

                            if (rateScoring != null)
                            {
                                rate = Convert.ToDouble(rateScoring.Rate);
                            }
                        }
                    }
                    else
                    {
                        rate = Convert.ToDouble(interestCoverage.Rate);
                    }
                }
                // 003 end

                premi = rate / 100 * sumInsured;
                rateLoad = GetRateLoad(interestCoverage.Coverage_Id, year);
                loadPremi = rateLoad / 100 * premi;

                CalculatedPremi calculatedPremi = new CalculatedPremi();
                calculatedPremi.CoverageID = interestCoverage.Coverage_Id;
                calculatedPremi.InterestID = interestID;
                calculatedPremi.Premium = premi;
                calculatedPremi.Rate = rate;
                calculatedPremi.Year = year;
                calculatedPremi.Loading = loadPremi;
                calculatedPremi.LoadingRate = rateLoad;
                calculatedPremi.SumInsured = sumInsured;
                calculatedPremi.IsBundling = isBundling;

                calculatedPremi.PeriodFrom = DateTime.Now;
                calculatedPremi.PeriodTo = DateTime.Now;
                CalculatedPremiItems.Add(calculatedPremi);
            }
        }

        private double GetRateLoad(String coverageID, int year)
        {
            double rateLoad = 0;
            int yearNow = DateTime.Now.Year;

            double yearLoad = yearNow - Convert.ToInt32(Year) + year - 1;
            //Kalau usia kenderaan melebihi age loading yang terakhir, maka ambil age terakhir,
            // lalu tambah 5% untuk kelebihan tiap tahunnya
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            string query = @"SELECT * FROM Prd_Load_Mv WHERE Product_Code=@0 AND Insurance_Code=@1 AND Coverage_Id=@2";

            List<Prd_Load_Mv> loadMVCoverage = db.Fetch<Prd_Load_Mv>(query, ProductCode, type, coverageID);

            if (loadMVCoverage != null)
            {
                if (loadMVCoverage.Count() > 0)
                {
                    Prd_Load_Mv maxLoadItem = db.FirstOrDefault<Prd_Load_Mv>(query + " Order By Age DESC", ProductCode, type, coverageID);
                    Prd_Load_Mv minLoadItem = db.FirstOrDefault<Prd_Load_Mv>(query + " Order By Age ASC", ProductCode, type, coverageID);
                    if (yearLoad < Convert.ToDouble(minLoadItem.Age))
                    {
                        rateLoad = 0;
                    }
                    else if (yearLoad > Convert.ToDouble(maxLoadItem.Age))
                    {
                        rateLoad = Convert.ToDouble(maxLoadItem.Load_Pct) + ((yearLoad - Convert.ToDouble(maxLoadItem.Age)) * 5);
                    }
                    else
                    {
                        Prd_Load_Mv loadMv = db.FirstOrDefault<Prd_Load_Mv>(query + " AND Age=@3", ProductCode, type, coverageID, yearLoad);
                        if (loadMv != null)
                        {
                            rateLoad = Convert.ToDouble(loadMv.Load_Pct);
                        }
                    }
                }
            }
            return rateLoad;
        }

        private bool isBasicCoverExist(List<CalculatedPremi> CalculatedPremiItems)
        {
            bool isExist = false;
            foreach (CalculatedPremi x in CalculatedPremiItems)
            {
                if (x.InterestID.Equals("CASCO ") &&
                        (x.CoverageID.Equals("ALLRIK") || x.CoverageID.Equals("TLO   ")))
                {
                    if (x.Rate != 0)
                    {
                        isExist = true;
                    }
                }
            }
            return isExist;
        }
        private Dtl_Rate_Scoring GetRateScoring(List<Dtl_Rate_Factor> rateFactorItems,
                                            String scoringCode,
                                            String coverageID,
                                            double sumInsured,
                                            bool isCheckBundling, string CityCode, string type, string UsageCode)
        {
            string query = "";
            Dtl_Rate_Scoring data = null;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    query = RateScoringQueryBuilder(rateFactorItems, scoringCode, coverageID, sumInsured, isCheckBundling, false, CityCode, type, UsageCode);
                    List<Dtl_Rate_Scoring> dataList = db.Fetch<Dtl_Rate_Scoring>(query);
                    // 003 begin
                    if (dataList.Count() == 0 && !isCheckBundling)
                    {
                        query = RateScoringQueryBuilder(rateFactorItems, scoringCode, coverageID, sumInsured, isCheckBundling, true, CityCode, type, UsageCode);
                        dataList = db.Fetch<Dtl_Rate_Scoring>(query);
                    }
                    // 003 end
                    data = dataList[0];
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return data;
        }


        private String RateScoringQueryBuilder(List<Dtl_Rate_Factor> rateFactorItems,
                                           String scoringCode,
                                           String coverageID,
                                           double sumInsured,
                                           bool isCheckBundling,
                                           bool undefinedVehicleType, string CityCode, string type, string UsageCode)
        {
            String query = "SELECT * FROM Dtl_Rate_Scoring WHERE " +
                    "Score_code='" + scoringCode + "' AND " +
                    "Insurance_Type='000401' AND " +
                    "Coverage_ID='" + coverageID + "' AND " +
                    //                "BC_Up_To_SI >= " + to()._string(sumInsured) + " ";
                    "(BC_Up_To_SI >= " + Convert.ToString(sumInsured) + " OR BC_Up_To_SI = -1) "; // bsy edit for bcuptosi = -1

            if (!isCheckBundling)
            {
                query += "AND Rate > 0 ";
            }

            foreach (Dtl_Rate_Factor item in rateFactorItems)
            {
                String ratingValue;
                if (item.factor_code.Equals("GEOGRA"))
                {
                    ratingValue = CityCode;
                }
                else if (item.factor_code.Equals("MVUSAG"))
                {
                    ratingValue = UsageCode;
                }
                else if (item.factor_code.Equals("VHCTYP"))
                {
                    ratingValue = type;
                    if (undefinedVehicleType)
                    {
                        ratingValue = "ALL   ";
                    }
                }
                else
                {
                    ratingValue = CityCode;
                }
                query += "AND Rating_value" + Convert.ToString(item.sequence_no) + "='" + ratingValue + "' ";
            }
            query += "ORDER BY BC_Up_To_SI ASC";

            return query;
        }


        private Otosales.Models.Product getProductExternal(string ChannelSource, bool isVehicleNew)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                string pType = "Used";
                if (isVehicleNew)
                {
                    pType = "New";
                }

                string sqlQuery = "SELECT Product.* FROM ProductAgent " +
                        "JOIN Product ON Product.ProductCode = ProductAgent.ProductCode " +
                        "WHERE ProductAgent.ChannelSource='" + ChannelSource + "' " +
                        "AND ProductType='" + pType + "' ORDER BY ProductAgent.LastUpdatedTime DESC";


                return db.FirstOrDefault<Otosales.Models.Product>(sqlQuery);
            }
        }

        private void IncludeBundlingCoverage(int year, bool isBundling)
        {
            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                     CoverageTypeNonBasic.SRCC, year, isBundling));
            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                    CoverageTypeNonBasic.FLD, year, isBundling));
            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                    CoverageTypeNonBasic.ETV, year, isBundling));

            IsSRCCChecked = true;
            IsFLDChecked = true;
            IsETVChecked = true;
            IsTSEnabled = true;
        }

        #endregion

        [HttpPost]
        public IHttpActionResult SaveProspect(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string State = form.Get("State");
                string PCData = form.Get("ProspectCustomer");
                string FUData = form.Get("FollowUp");
                string FUHData = form.Get("FollowUpHistory");
                string ENData = form.Get("EmailNotification ");
                string OSData = form.Get("OrderSimulation");
                string OSMVData = form.Get("OrderSimulationMV");
                // string OSIData = form.Get("OrderSimulationInterest");
                string calculatedPremiItems = form.Get("calculatedPremiItems");
                string query = "";
                string CustIdEnd = "";
                string FollowUpNoEnd = "";
                string OrderNoEnd = "";

                FollowUp FUEnd = null;
                ProspectCustomer PCEnd = null;

                #region Prospect Tab
                if (State.Equals("PROSPECT") || State.Equals("PREMIUMSIMULATION"))
                {
                    string CustId = "";
                    string FollowUpNumber = "";
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT UPDATE PROSPECT CUSTOMER

                        ProspectCustomer PC = JsonConvert.DeserializeObject<ProspectCustomer>(PCData);
                        string CustID = System.Guid.NewGuid().ToString();
                        string FollowUpNo = System.Guid.NewGuid().ToString();
                        PC.CustID = string.IsNullOrEmpty(PC.CustID) ? CustID : PC.CustID;
                        query = @";IF EXISTS(SELECT * FROM ProspectCustomer WHERE CustId=@0)
BEGIN
UPDATE ProspectCustomer SET
       [Name]=@1
      ,[Phone1]=@2
      ,[Phone2]=@3
      ,[Email1]=@4
      ,[Email2]=@5
      ,[CustIDAAB]=@6
      ,[SalesOfficerID]=@7
      ,[DealerCode]=@8
      ,[SalesDealer]=@9
      ,[BranchCode]=@10
      ,[isCompany]=@11
      ,[LastUpdatedTime]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCustomer ([CustID]
      ,[Name]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[CustIDAAB]
      ,[SalesOfficerID]
      ,[DealerCode]
      ,[SalesDealer]
      ,[BranchCode]
      ,[isCompany]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1)
END";
                        // PC.SalesOfficerID = PC.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", PC.SalesOfficerID) : PC.SalesOfficerID;

                        db.Execute(query, PC.CustID, PC.Name, PC.Phone1, PC.Phone2, PC.Email1, PC.Email2, PC.CustIDAAB, PC.SalesOfficerID, PC.DealerCode, PC.SalesDealer, PC.BranchCode, PC.isCompany);
                        #endregion

                        #region INSERT UPDATE FOLLOWUP
                        FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);

                        FU.FollowUpNo = string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo;
                        FU.CustID = string.IsNullOrEmpty(FU.CustID) ? CustID : FU.CustID;
                        FU.LastSeqNo = db.ExecuteScalar<int>(";IF EXISTS(SELECT * FROM FollowUpHistory Where FollowUpNo=@0)BEGIN SELECT ISNULL(MAX(SeqNo),0) From FollowUpHistory WHERE FollowUpNo=@0 END ELSE BEGIN SELECT 0 END", string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo);
                        query = @";IF EXISTS(SELECT * FROM FollowUp WHERE FollowUpNo=@0)
BEGIN
    UPDATE FollowUp SET [LastSeqNo]=@1
      ,[CustID]=@2
      ,[ProspectName]=@3
      ,[Phone1]=@4
      ,[Phone2]=@5
      ,[SalesOfficerID]=@6
      ,[EntryDate]=GETDATE()
      ,[FollowUpName]=@7
      ,[FollowUpStatus]=@8
      ,[FollowUpInfo]=@9
      ,[Remark]=@10
      ,[BranchCode]=@11
      ,[LastUpdatedTime]=GETDATE()
      ,[PolicyId]=@12
      ,[PolicyNo]=@13
      ,[TransactionNo]=@14
      ,[SalesAdminID]=@15
      ,[SendDocDate]=@16
      ,[IdentityCard]=@17
      ,[STNK]=@18
      ,[SPPAKB]=@19
      ,[BSTB1]=@20
      ,[BSTB2]=@21
      ,[BSTB3]=@22
      ,[BSTB4]=@23
      ,[CheckListSurvey1]=@24
      ,[CheckListSurvey2]=@25
      ,[CheckListSurvey3]=@26
      ,[CheckListSurvey4]=@27
      ,[PaymentReceipt1]=@28
      ,[PaymentReceipt2]=@29
      ,[PaymentReceipt3]=@30
      ,[PaymentReceipt4]=@31
      ,[PremiumCal1]=@32
      ,[PremiumCal2]=@33
      ,[PremiumCal3]=@34
      ,[PremiumCal4]=@35
      ,[FormA1]=@36
      ,[FormA2]=@37
      ,[FormA3]=@38
      ,[FormA4]=@39
      ,[FormB1]=@40
      ,[FormB2]=@41
      ,[FormB3]=@42
      ,[FormB4]=@43
      ,[FormC1]=@44
      ,[FormC2]=@45
      ,[FormC3]=@46
      ,[FormC4]=@47
      ,[FUBidStatus]=@48
      ,[BuktiBayar]=@49 WHERE [FollowUpNo]=@0
END
    ELSE
BEGIN
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[PolicyId]
      ,[PolicyNo]
      ,[TransactionNo]
      ,[SalesAdminID]
      ,[SendDocDate]
      ,[IdentityCard]
      ,[STNK]
      ,[SPPAKB]
      ,[BSTB1]
      ,[BSTB2]
      ,[BSTB3]
      ,[BSTB4]
      ,[CheckListSurvey1]
      ,[CheckListSurvey2]
      ,[CheckListSurvey3]
      ,[CheckListSurvey4]
      ,[PaymentReceipt1]
      ,[PaymentReceipt2]
      ,[PaymentReceipt3]
      ,[PaymentReceipt4]
      ,[PremiumCal1]
      ,[PremiumCal2]
      ,[PremiumCal3]
      ,[PremiumCal4]
      ,[FormA1]
      ,[FormA2]
      ,[FormA3]
      ,[FormA4]
      ,[FormB1]
      ,[FormB2]
      ,[FormB3]
      ,[FormB4]
      ,[FormC1]
      ,[FormC2]
      ,[FormC3]
      ,[FormC4]
      ,[FUBidStatus]
      ,[BuktiBayar]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38,@39,@40,@41,@42,@43,@44,@45,@46,@47,@48,@49)
END";
                        // FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;

                        db.Execute(query, FU.FollowUpNo
     , FU.LastSeqNo
     , FU.CustID
     , FU.ProspectName
     , FU.Phone1
     , FU.Phone2
     , FU.SalesOfficerID
     , FU.FollowUpName
     , FU.FollowUpStatus
     , FU.FollowUpInfo
     , FU.Remark
     , FU.BranchCode
     , FU.PolicyId
     , FU.PolicyNo
     , FU.TransactionNo
     , FU.SalesAdminID
     , FU.SendDocDate
     , FU.IdentityCard
     , FU.STNK
     , FU.SPPAKB
     , FU.BSTB1
     , FU.BSTB2
     , FU.BSTB3
     , FU.BSTB4
     , FU.CheckListSurvey1
     , FU.CheckListSurvey2
     , FU.CheckListSurvey3
     , FU.CheckListSurvey4
     , FU.PaymentReceipt1
     , FU.PaymentReceipt2
     , FU.PaymentReceipt3
     , FU.PaymentReceipt4
     , FU.PremiumCal1
     , FU.PremiumCal2
     , FU.PremiumCal3
     , FU.PremiumCal4
     , FU.FormA1
     , FU.FormA2
     , FU.FormA3
     , FU.FormA4
     , FU.FormB1
     , FU.FormB2
     , FU.FormB3
     , FU.FormB4
     , FU.FormC1
     , FU.FormC2
     , FU.FormC3
     , FU.FormC4
     , FU.FUBidStatus
     , FU.BuktiBayar);
                        #endregion


                        if (PC.isCompany)
                        {
                            #region INSERT UPDATE COMPANY DATA
                            db.Execute(@";IF EXISTS(SELECT * FROM ProspectCompany WHERE CustId=@0)
BEGIN
UPDATE ProspectCompany SET
       [CompanyName]=@1
      ,[FollowUpNo]=@2
      ,[PICPhoneNo]=@3
      ,[Email]=@4
      ,[RowStatus]=1
      ,[ModifiedDate]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName]
      ,[FollowUpNo]
      ,[PICPhoneNo]
      ,[Email]
      ,[ModifiedDate]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,GETDATE(),1)
END", PC.CustID, PC.Name, FU.FollowUpNo, PC.Phone1, PC.Email1);
                            #endregion
                        }

                        PCEnd = PC;
                        FUEnd = FU;


                        //                        #region INSERT FOLLOWUP HISTORY
                        //                        FollowUpHistory FUH = new FollowUpHistory();
                        //                        FUH.BranchCode = FU.BranchCode;
                        //                        FollowUpNumber = FUH.FollowUpNo = string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo;
                        //                        FUH.SeqNo = db.ExecuteScalar<int>("SELECT LastSeqNo FROM FollowUp WHERE FollowUpNo=@0", FUH.FollowUpNo) + 1;
                        //                        CustId = FUH.CustID = string.IsNullOrEmpty(FU.CustID) ? CustID : FU.CustID;
                        //                        FUH.ProspectName = FU.ProspectName;
                        //                        FUH.Phone1 = FU.Phone1;
                        //                        FUH.Phone2 = FU.Phone2;
                        //                        FUH.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;
                        //                        FUH.FollowUpName = FU.FollowUpName;
                        //                        FUH.NextFollowUpDate = FU.NextFollowUpDate;
                        //                        FUH.LastFollowUpDate = FU.LastFollowUpDate;
                        //                        FUH.FollowUpStatus = FU.FollowUpStatus;
                        //                        FUH.FollowUpInfo = FU.FollowUpInfo;
                        //                        FUH.Remark = FU.Remark;
                        //                        FUH.BranchCode = FU.BranchCode;

                        //                        query = @"; IF NOT EXISTS(SELECT  * FROM FollowupHistory WHERE FollowUpNo=@0)
                        //BEGIN
                        //    INSERT INTO FollowUpHistory (
                        //       [FollowUpNo]
                        //      ,[SeqNo]
                        //      ,[CustID]
                        //      ,[ProspectName]
                        //      ,[Phone1]
                        //      ,[Phone2]
                        //      ,[SalesOfficerID]
                        //      ,[EntryDate]
                        //      ,[FollowUpName]
                        //      ,[NextFollowUpDate]
                        //      ,[LastFollowUpDate]
                        //      ,[FollowUpStatus]
                        //      ,[FollowUpInfo]
                        //      ,[Remark]
                        //      ,[BranchCode]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),1)
                        //END
                        //";
                        //                        db.Execute(query, FUH.FollowUpNo
                        //     , FUH.SeqNo
                        //     , FUH.CustID
                        //     , FUH.ProspectName
                        //     , FUH.Phone1
                        //     , FUH.Phone2
                        //     , FUH.SalesOfficerID
                        //     , FUH.FollowUpName
                        //     , FUH.NextFollowUpDate
                        //     , FUH.LastFollowUpDate
                        //     , FUH.FollowUpStatus
                        //     , FUH.FollowUpInfo
                        //     , FUH.Remark
                        //     , FUH.BranchCode);
                        //                        #endregion
                        #region SAVE IMAGE
                        query = @";
SELECT [ImageID]
      ,[FollowUpNo]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                        List<dynamic> ImageData = db.Fetch<dynamic>(query, FU.FollowUpNo);
                        foreach (dynamic id in ImageData)
                        {
                            query = @"IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,1,@6,@7)
END";
                            db.Execute(query, id.ImageID, id.DeviceID
      , id.SalesOfficerID
      , id.PathFile
      , id.EntryDate
      , id.LastUpdatedTime
      , id.Data
      , id.ThumbnailData);
                            db.Execute("UPDATE dbo.TempImageData SET RowStatus=0 WHERE FollowUpNo=@0 AND PathFile=@1", id.FollowUpNo, id.PathFile);
                        }
                        #endregion
                    }
                    //return Json(new { status = true, message = "Success", CustId, FollowUpNumber });
                    CustIdEnd = FUEnd.CustID;
                    FollowUpNoEnd = FUEnd.FollowUpNo;

                }
                #endregion

                #region Vehicle Tab
                if (State.Equals("VEHICLE") || State.Equals("PREMIUMSIMULATION"))
                {
                    string OrderNoReturn = null;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT ORDERSIMULATION
                        OrderSimulation OS = JsonConvert.DeserializeObject<OrderSimulation>(OSData);
                        string OrderNo = System.Guid.NewGuid().ToString();
                        OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                        OrderNoReturn = OS.OrderNo;
                        FollowUpNoEnd=OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;

                        CustIdEnd = OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;
                        OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                        OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                        OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PeriodFrom]
      ,[PeriodTo]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27)
END";
                        // OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                        db.Execute(query, OS.OrderNo
      , OS.CustID
      , OS.FollowUpNo
      , OS.QuotationNo
      , OS.MultiYearF
      , OS.YearCoverage
      , OS.TLOPeriod
      , OS.ComprePeriod
      , OS.BranchCode
      , OS.SalesOfficerID
      , OS.PhoneSales
      , OS.DealerCode
      , OS.SalesDealer
      , OS.ProductTypeCode
      , OS.AdminFee
      , OS.TotalPremium
      , OS.Phone1
      , OS.Phone2
      , OS.Email1
      , OS.Email2
      , OS.SendStatus
      , OS.InsuranceType
      , OS.ApplyF
      , OS.SendF
      , OS.LastInterestNo
      , OS.LastCoverageNo
      , OS.PeriodFrom
      , OS.PeriodTo);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION MV
                        OrderSimulationMV OSMV = JsonConvert.DeserializeObject<OrderSimulationMV>(OSMVData);
                        OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                        query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                        db.Execute(query, OSMV.OrderNo
      , OSMV.ObjectNo
      , OSMV.ProductTypeCode
      , OSMV.VehicleCode
      , OSMV.BrandCode
      , OSMV.ModelCode
      , OSMV.Series
      , OSMV.Type
      , OSMV.Sitting
      , OSMV.Year
      , OSMV.CityCode
      , OSMV.UsageCode
      , OSMV.SumInsured
      , OSMV.AccessSI
      , OSMV.RegistrationNumber
      , OSMV.EngineNumber
      , OSMV.ChasisNumber
      //, OSMV.LastUpdatedTime
      //, OSMV.RowStatus
      , OSMV.IsNew);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION SURVEY
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulationSurvey WHERE OrderNo=@0)
BEGIN
INSERT INTO ordersimulationsurvey (OrderNo,CityCode,LocationCode,SurveyAddress,SurveyPostalCode,IsNeedSurvey,IsNSASkipSurvey,RowStatus,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
VALUES(@0,'','','','',@1,0,1,'OtosalesAPI',GETDATE(),NULL,NULL)
END
ELSE
BEGIN
UPDATE ordersimulationsurvey SET IsNeedSurvey=@1,ModifiedBy='OtosalesAPI',ModifiedDate=GETDATE() WHERE OrderNo=@0
END";
                        int isneedsurvey= OSMV.Year.Equals(DateTime.Now.Year.ToString())?0:1;
                        db.Execute(query,OS.OrderNo,isneedsurvey);
                        #endregion
                    }

                    //return Json(new { status = true, message = "Success", OrderNo = OrderNoReturn });
                    OrderNoEnd = OrderNoReturn;
                }
                #endregion

                #region Cover Tab
                if (State.Equals("COVER") || State.Equals("PREMIUMSIMULATION"))
                {
                    OrderSimulation OS = JsonConvert.DeserializeObject<OrderSimulation>(OSData);
                    OrderSimulationMV OSMV = JsonConvert.DeserializeObject<OrderSimulationMV>(OSMVData);
                    List<OrderSimulationInterest> OSI = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> OSC = new List<OrderSimulationCoverage>();
                    OS.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OS.OrderNo;
                    OSMV.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OSMV.OrderNo;

                    bool isexistSFE = false;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        int a = 1;
                        int year = 1;
                        List<CalculatedPremi> calculatedPremiItem = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculatedPremiItems);

                        DateTime? pt = null;
                        foreach (CalculatedPremi cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (CalculatedPremi cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                        pt = cpitem.PeriodTo;
                                    }
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                        pf = cpitem.PeriodFrom;
                                    }
                                }
                                OrderSimulationInterest osi = new OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (CalculatedPremi cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;

                        foreach (OrderSimulationInterest item in OSI)
                        {
                            foreach (CalculatedPremi item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    OrderSimulationCoverage oscItem = new OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }
                      
                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert
                     
                            db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);
                            db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);
                        

                        int i = 1;
                        foreach (OrderSimulationInterest data in OSI)
                        {
                            query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                            i++;
                        }

                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationInterest> TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationInterest data in TempAllOSI)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in OSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) && data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                        query = @"UPDATE OrderSimulationInterest SET 
                        //                                Premium=@5,
                        //                                LastUpdatedTime=GETDATE()
                        //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4 AND Deductible_Code=@6";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year, data2.Premium, data.DeductibleCode);
                        //                                    }
                        //                                }

                        //                                if (!isExist)
                        //                                {
                        //                                    //soft delete coverage

                        //                                    query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 ";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo);


                        //                                    //soft delete interest
                        //                                    query = @"DELETE OrderSimulationInterest " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                #endregion

                        #region DELETE UPDATE ORDERSIMULATION COVERAGE

                        #region Insert
                        //  OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        i = 1;
                        foreach (OrderSimulationCoverage data in OSC)
                        {
                            query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                            i++;

                        }
                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationCoverage> TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationCoverage data in TempAllOSC)
                        //                            {

                        //                                OrderSimulationInterest osiItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2", data.OrderNo, data.ObjectNo, data.InterestNo);
                        //                                if (osiItem != null)
                        //                                {
                        //                                    bool isExist = false;
                        //                                    foreach (OrderSimulationCoverage item in OSC)
                        //                                    {
                        //                                        if (item.CoverageID.TrimEnd().Equals(data.CoverageID.TrimEnd()) && item.InterestNo.Equals(data.InterestNo) && item.CoverageNo.Equals(data.CoverageNo))
                        //                                        {
                        //                                            isExist = true;
                        //                                            query = @"UPDATE OrderSimulationCoverage SET 
                        //      [Rate]=@5
                        //      ,[SumInsured]=@6
                        //      ,[LoadingRate]=@7
                        //      ,[Loading]=@8
                        //      ,[Premium]=@9
                        //      ,[BeginDate]=@10
                        //      ,[EndDate]=@11
                        //      ,[LastUpdatedTime]=GETDATE()
                        //      ,[RowStatus]=1
                        //      ,[IsBundling]=@12
                        //      ,[Entry_Pct]=@13
                        //      ,[Ndays]=@14
                        //      ,[Excess_Rate]=@15
                        //      ,[Calc_Method]=@16
                        //      ,[Cover_Premium]=@17
                        //      ,[Gross_Premium]=@18
                        //      ,[Max_si]=@19
                        //      ,[Net1]=@20
                        //      ,[Net2]=@21
                        //      ,[Net3]=@22
                        //      ,[Deductible_Code]=@23 WHERE [OrderNo]=@0 AND [ObjectNo]=@1 AND [InterestNo]=@2 AND [CoverageNo] =@3 AND [CoverageID]=@4";
                        //                                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                        //                                            data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);

                        //                                        }
                        //                                    }
                        //                                    //delete data
                        //                                    if (!isExist)
                        //                                    {
                        //                                        //soft delete coverage

                        //                                        query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageId=@4";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID);


                        //                                    }
                        //                                }

                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems
                        //                            TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationCoverage item in OSC)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationCoverage data in TempAllOSC)
                        //                                {
                        //                                    if (data.InterestNo.Equals(item.InterestNo) &&
                        //                           data.CoverageNo.Equals(item.CoverageNo) && data.CoverageID.TrimEnd().Equals(item.CoverageID.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (isExist)
                        //                                {
                        //                                    OrderSimulationCoverage oscitem = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageID=@4", item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.CoverageID);
                        //                                    if (oscitem != null)
                        //                                    {
                        //                                        query = @"UPDATE OrderSimulationCoverage SET 
                        //                                RowStatus=1
                        //                                ,Rate=@4
                        //                                ,SumInsured=@5
                        //                                ,LoadingRate=@6
                        //                                ,Loading=@7
                        //                                ,Premium=@8
                        //                                ,IsBundling=@9     
                        //                                ,LastUpdatedTime=GETDATE()  
                        //                                ,[Entry_Pct]=@10
                        //                                ,[Ndays]=@11
                        //                                ,[Excess_Rate]=@12
                        //                                ,[Calc_Method]=@13
                        //                                ,[Cover_Premium]=@14
                        //                                ,[Gross_Premium]=@15
                        //                                ,[Max_si]=@16
                        //                                ,[Net1]=@17
                        //                                ,[Net2]=@18
                        //                                ,[Net3]=@19
                        //                                ,[Deductible_Code]=@20                      
                        //WHERE OrderNo=@0 AND ObjectNo = @1 AND InterestNo=@2 AND CoverageNo=@3";
                        //                                        db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.Rate, item.SumInsured, item.LoadingRate, item.LoadingRate, item.Premium, item.IsBundling ? 1 : 0,
                        //                                             item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
                        //                                    }
                        //                                }
                        //                                else
                        //                                {

                        //                                    int i = db.ExecuteScalar<int>("SELECT max(CoverageNo) FROM OrderSimulationCoverage WHERE OrderNo=@0 ", item.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[CoverageNo]
                        //      ,[CoverageID]
                        //      ,[Rate]
                        //      ,[SumInsured]
                        //      ,[LoadingRate]
                        //      ,[Loading]
                        //      ,[Premium]
                        //      ,[BeginDate]
                        //      ,[EndDate]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]
                        //      ,[IsBundling]    
                        //      ,[Entry_Pct]
                        //      ,[Ndays]
                        //      ,[Excess_Rate]
                        //      ,[Calc_Method]
                        //      ,[Cover_Premium]
                        //      ,[Gross_Premium]
                        //      ,[Max_si]
                        //      ,[Net1]
                        //      ,[Net2]
                        //      ,[Net3]
                        //      ,[Deductible_Code])) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                        //                                    db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, i, item.CoverageID, item.Rate, item.SumInsured, item.LoadingRate, item.Loading, item.Premium, item.BeginDate, item.EndDate, item.IsBundling ? 1 : 0,
                        //                                                      item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
                        //                                    i++;
                        //                                }


                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = OS.PeriodFrom ?? DateTime.Now;
                            DateTime end = OS.PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,PeriodFrom=@12
                        ,PeriodTo=@13
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo, OS.PeriodFrom, OS.PeriodTo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion

                        OrderNoEnd = string.IsNullOrEmpty(OrderNoEnd) ? OS.OrderNo : OrderNoEnd;
                        FollowUpNoEnd = string.IsNullOrEmpty(FollowUpNoEnd) ? OS.FollowUpNo : FollowUpNoEnd;
                        CustIdEnd = string.IsNullOrEmpty(CustIdEnd) ? OS.CustID : CustIdEnd;
                        if (!MobileRepository.IsAllowedIEP(OS.ProductCode, isexistSFE))
                        {
                            return Json(new { status = false, message = "Basic Cover Not Applied", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd });
                        }
                    }

                //    return Json(new { status = true, message = "Success", OrderSimulation = OS, FollowUpNo = FollowUpNoEnd });
                }
                return Json(new { status = true, message = "Success", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getAccessoriesList(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<dynamic> result = db.Fetch<dynamic>(@"SELECT Description , MaxSI FROM Accessories WHERE RowStatus=1");
                return Json(new { status = true, message = "Success", data = result });

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult UploadImage(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string ext = form.Get("ext");
            string ImageType = form.Get("ImageType");
            string FollowUpNo = form.Get("FollowUpNo");
            string DeviceID = form.Get("DeviceID");
            string SalesOfficerID = form.Get("SalesOfficerID");
            string Data = form.Get("Data");
            string ThumbnailData = form.Get("ThumbnailData");
            FollowUpNo = string.IsNullOrEmpty(FollowUpNo) ? System.Guid.NewGuid().ToString() : FollowUpNo;
            byte[] rawData = new byte[Data.Length / 2];
            for (int i = 0; i < rawData.Length; i++)
            {
                rawData[i] = Convert.ToByte(Data.Substring(i * 2, 2), 16);
            }
            //_log.Debug("checkpoint1 " + DateTime.Now);

            //byte[] rawThumb = new byte[ThumbnailData.Length / 2];
            //for (int i = 0; i < rawThumb.Length; i++)
            //{
            //    rawThumb[i] = Convert.ToByte(ThumbnailData.Substring(i * 2, 2), 16);
            //}
            //_log.Debug("checkpoint2 " + DateTime.Now);

            try
            {
                string ImageID = MobileRepository.GenerateGuid(GenGuid.ImageID);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string query = @";IF EXISTS(SELECT * FROM TempImageData Where FollowUpNo=@0 AND ImageType=@3 AND SalesOfficerID=@4)
                BEGIN
                UPDATE TempImageData SET ImageID=@1
                ,DeviceID=@2
                ,PathFile=@5
                ,LastUpdatedTime=GETDATE()
                ,rowstatus=1
                ,Data=Convert(varbinary(max),@6)
                ,ThumbnailData=Convert(varbinary(max),@7)
                 WHERE FollowUpNo=@0 AND ImageType=@3 AND SalesOfficerID=@4
                END
                ELSE
                BEGIN
                INSERT INTO TempImageData (FollowUpNo,ImageID,DeviceID,ImageType,SalesOfficerID,PathFile,EntryDate,LastUpdatedTime,RowStatus,Data,ThumbnailData) 
                VALUES(@0,@1,@2,@3,@4,@5,GETDATE(),GETDATE(),1,@6,@7)
                END";
                string ImageId = ImageID + ext;
                // SalesOfficerID = SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", SalesOfficerID) : SalesOfficerID;

                //db.Execute(query, FollowUpNo, ImageID, DeviceID, ImageType, SalesOfficerID, ImageId, rawData, rawThumb);
                db.Execute(query, FollowUpNo, ImageID, DeviceID, ImageType, SalesOfficerID, ImageId, rawData, rawData);
                _log.Debug("checkpoint3 " + DateTime.Now);
                return Json(new { status = true, message = "OK", data = ImageId, FollowUpNo = FollowUpNo });

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.ToString(), data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleBrand()
        {
            var query = @"SELECT BrandCode, ProductTypeCode, Description, RowStatus FROM VehicleBrand WHERE RowStatus = 1 ORDER BY Description";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleYear(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();

                if (brandCode == null || brandCode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                var query = @"SELECT DISTINCT Year FROM Vehicle WHERE BrandCode=@0 ORDER BY Year DESC";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, brandCode);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleType(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();
                string year = form.Get("year").Trim();

                if (year == null || year == "" || brandCode == null || brandCode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                        SELECT DISTINCT a.* FROM VehicleModel a 
                        INNER JOIN Vehicle b ON a.BrandCode=b.BrandCode 
                        AND a.ProductTypeCode=b.ProductTypeCode 
                        AND a.ModelCode=b.ModelCode WHERE b.BrandCode=@0 AND a.RowStatus = 1 
                        AND b.Year=@1 ORDER BY a.Description
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, brandCode, year);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        [HttpPost]
        public IHttpActionResult getVehicleTypeAll(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
               

                string query = @"
					   SELECT * FROM VehicleType where categorycode=1 and vehicletypecode not in('T00009') AND RowStatus=1 ORDER BY Description
                     
                    ";
                
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleSeries(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();
                string modelCode = form.Get("modelCode").Trim();
                string year = form.Get("year").Trim();

                if (year == null || year == "" || brandCode == null || brandCode == "" || modelCode == null || modelCode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                       SELECT DISTINCT a.BrandCode, a.ProductTypeCode,  a.ModelCode, a.Series, a.RowStatus  
                       FROM Vehicle a  JOIN VehicleSeries b  ON a.BrandCode = b.BrandCode  
                       and a.ModelCode = b.ModelCode  and a.ProductTypeCode = b.ProductTypeCode 
                       WHERE a.BrandCode=@0 AND a.ModelCode=@1 AND a.Year=@2 AND a.RowStatus = 1
                       ORDER BY a.Series
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, brandCode, modelCode, year);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string brandCode = form.Get("brandCode").Trim();
                string modelCode = form.Get("modelCode").Trim();
                string year = form.Get("year").Trim();
                string series = form.Get("series").Trim();

                if (year == null || year == "" || brandCode == null || brandCode == "" ||
                    modelCode == null || modelCode == "" || series == null || series == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                       SELECT VehicleCode, BrandCode, ProductTypeCode, ModelCode, 
                       Series, Type, Sitting, Year, CityCode, Price 
                       FROM Vehicle WHERE Year = @0
                       And BrandCode = @1 And ModelCode = @2
                       And Series = @3 ORDER BY VehicleCode
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, year, brandCode, modelCode, series);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleUsage()
        {
            string query = @"
 SELECT Factor_Code,insurance_code,Description,status,Entryusr,Entrydt,UpdateUsr,UpdateDt,LastUpdatedTime,RowStatus into #TempDtlInsFact  FROM Dtl_Ins_Factor WITH(NOLOCK) WHERE (Factor_Code in ('GEOGRA') OR (Factor_Code = 'MVUSAG' and insurance_code like 'U%') OR (Factor_Code = 'VHCTYP' and insurance_code like 'T0%'))  ORDER BY LastUpdatedTime, Factor_Code, insurance_code ASC
  SELECT * FROM #TempDtlInsFact WHERE Factor_Code='MVUSAG' AND RowStatus = 1 ORDER BY Description
                ";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getVehicleRegion()
        {
            string query = @"
                   SELECT RegionCode, Description, Sequence FROM Region WHERE RowStatus = 1 ORDER BY Description
                ";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getDealerName()
        {
            string query = @";SELECT DealerCode, Description, RowStatus, CustID FROM Dealer WHERE RowStatus = 1 ORDER BY Description
                ";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                var result = db.Fetch<dynamic>(query);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult getSalesmanDealerName(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string dealercode = form.Get("dealercode").Trim();

                if (dealercode == null || dealercode == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                var result = mobileRepository.getSalesmanDealerName(dealercode);
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult getProductType(FormDataCollection form)
        {

           // string Channel = form.Get("Channel").Trim();

            string salesOfficerId = form.Get("salesOfficerId").Trim();
            string query = @"
                  SELECT ProductTypeCode, Description, InsuranceType, RowStatus FROM ProductType WHERE RowStatus = 1
                ";
         //   query = query + ((Channel.Equals("EXT")) ? " AND InsuranceType=1" : "");
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                using (var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    bool isAllowedSharia = Convert.ToBoolean(aabdb.ExecuteScalar<int>(@"select Count(*) from mst_salesman s WITH (NOLOCK)
inner join Mst_Branch b on s.Branch_id = b.Branch_id
where s.status = 1 and user_id_otosales = @0 and b.biz_type=2",salesOfficerId));
                    query = string.Concat(query, (isAllowedSharia ? " Order By SeqNo ASC" : " AND SeqNo NOT IN(2) Order By SeqNo ASC"));
                    var result = db.Fetch<dynamic>(query);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

                }
            }
        }

        [HttpPost]
        public IHttpActionResult getProductCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string insurancetype = form.Get("insurancetype").Trim();
                string salesofficerid = form.Get("salesofficerid").Trim();
                int isRenew = string.IsNullOrEmpty(form.Get("isRenewal")) ? 0 : Convert.ToInt32(form.Get("isRenewal"));
                List<Otosales.Models.Product> result = new List<Otosales.Models.Product>();
                if (string.IsNullOrEmpty(insurancetype))
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }
                result = mobileRepository.GetProduct(insurancetype, salesofficerid,isRenew);
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpSummaryAO(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string salesofficerid = form.Get("salesofficerid").Trim();
                string year = form.Get("year").Trim();
                string month = form.Get("month").Trim();

                if (salesofficerid == null || salesofficerid == "" || year == null || year == "" || month == null || month == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                int number;

                if (!(Int32.TryParse(year, out number) && Int32.TryParse(month, out number)))
                {
                    return Json(new { status = false, message = "year or month must number", data = "null" });
                }

                string query = @"
                  SELECT SalesOfficerID, SalesName, BranchCode, BranchDesc, Region, Period, RowStatus, 
                  TotalProspect, TotalNeedFu, TotalPotential, TotalCallLater, TotalCollectDoc, 
                  TotalNotDeal, TotalSendToSA, TotalPolicyCreated, TotalBackToAo, TotalDeal, Frequency
                  FROM FollowUpSummary
                  WHERE SalesOfficerID = @0 
                  and YEAR(Period) = @1 
                  and MONTH(Period) = @2
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, salesofficerid, year, month);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getBranchList(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string search = "%" + form.Get("search").Trim() + "%";
                var result = mobileRepository.getBranchList(search);
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }


        [HttpPost]
        public IHttpActionResult getBasicCover(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                List<BasicCover> BasicCover = new List<BasicCover>();

                //string Channel = form.Get("Channel").Trim();
                string vyear = form.Get("vyear").Trim();
                int year = string.IsNullOrEmpty(vyear) ?DateTime.Now.Year : Convert.ToInt32(vyear);
                bool isBasic = Convert.ToBoolean(form.Get("isBasic"));
                BasicCover = MobileRepository.GetBasicCover(year, isBasic);
               
                return Json(new { status = true, message = "OK", BasicCover }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



        [HttpPost]
        public IHttpActionResult getServicePointList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string search = "%" + form.Get("search").Trim() + "%";

                string query = @"
                 SELECT * FROM GardaCenter WHERE GardaCenterName 
                 like @0 OR City like @0 AND RowStatus = 1 Order by GardaCenterName ASC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, search);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



        [HttpPost]
        public IHttpActionResult getDashboard(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string StateDashboard = form.Get("StateDashboard").Trim();
                string Role = form.Get("userRole").Trim(); //NATIONALMGR/BRANCHMGR
                string periodView = "MONTHLY";//form.Get("periodView").Trim();// Monthly/Yearly
                string param = form.Get("param").Trim();

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                //SalesOfficerID = SalesOfficerID.Contains("@") ? dbs.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", SalesOfficerID) : SalesOfficerID;

                List<DashboardProspect> dpList = db.Fetch<DashboardProspect>(";EXEC [dbo].[usp_GetDashboardProspectOtosales] @0,@1,@2", Role, param, periodView);
                List<DashboardSubordinate> dsList = db.Fetch<DashboardSubordinate>(";EXEC [dbo].[usp_GetDashboardSubordinateOtosales] @0,@1,@2", Role, StateDashboard, param);
                List<DashboardStatus> dpsList = db.Fetch<DashboardStatus>(";EXEC [dbo].[usp_GetDashboardSubordinateOtosales] @0,@1,@2", Role, StateDashboard, param);

                return Json(new { status = true, message = "OK", dashboardProspect = dpList, dashboardSubordinate = dsList, dashboardStatus = dpsList }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getNewDashboard(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                /*
                 * State - Keterangan untuk melakukan action getdata
                 * 1 - State National Manager [NATIONAL]
                 * 2 - State Region Manager [REGIONAL]
                 * 3 - State Agency Manager []
                 * 4 - State Branch Manager [BRANCH]
                 * 5 - State Sales Section Head []
                 * 6 - State Area Unit Manager []
                 * 7 - State AO, MGO []
                 */

                /*
                 * Parameter - Parameter untuk getdashboard otosales
                 *  StateDashboard=NATIONAL/REGIONAL/BRANCH/
                    Role=UserLogin.RoleCode
                    PeriodView=MONTHLY/YEARLY
                    UserID=UserLogin.ID
                    Channel=UserLogin.Channel
                    ChannelSource=UserLogin.ChannelSource
                    DetailID=ResponseAPIDashboard.details.selectedid
                 */

                #region Set Parameter
                string StateDashboard = form.Get("StateDashboard").Trim(); //NATIONAL/BRANCH/REGION/
                string Role = form.Get("Role").Trim(); //NATIONALMGR/BRANCHMGR
                string PeriodView = form.Get("PeriodView").Trim();
                string UserID = form.Get("UserID").Trim();
                string DetailID = form.Get("DetailID").Trim();
                #endregion

                DashboardResult result = DashboardRepository.GenerateDashboard(Role,UserID, PeriodView, StateDashboard, DetailID);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        private void InitializeCoverageItems(string OrderNo, out List<decimal> RPTRenDiscount, out List<decimal> Discount)
        {
            List<decimal> RDP = new List<decimal>();
            List<decimal> DP = new List<decimal>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            
                CalculatedPremiItems = MobileRepository.LoadCalculatedpremibyOrderNo(OrderNo.TrimEnd(),"");
            

            InterestCoverageItems = CalculatedPremiItems;
            BasicPremi = ACCESSPremi = AccessSI = SRCCPremi = FLDPremi = ETVPremi = TSPremi =
                    TPLPremi = TPLSI = PADRVRPremi = PADRVRSI = PAPASSPremi = PAPASSSI = PASS = 0;

            if (CalculatedPremiItems.Count() > 0)
            {
                foreach (CalculatedPremi item in CalculatedPremiItems)
                {
                    if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO") &&
                            (item.CoverageID.ToUpper().Equals("ALLRIK") || item.CoverageID.ToUpper().TrimEnd().Equals("TLO")))
                    {
                        BasicPremi += item.Premium;
                    }
                    if (item.InterestID.ToUpper().Equals("ACCESS") &&
                            (item.CoverageID.ToUpper().Equals("ALLRIK") || item.CoverageID.ToUpper().TrimEnd().Equals("TLO")))
                    {
                        IsACCESSSIEnabled = true;
                        ACCESSPremi += item.Premium;
                        if (ACCESSPremi > 0)
                        {
                            IsACCESSChecked = true;
                        }
                        if (item.Year == 1)
                            AccessSI = item.SumInsured;
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("SRCC") || item.CoverageID.ToUpper().Equals("SRCTLO")))
                    {
                        IsSRCCEnabled = true;
                        SRCCPremi += item.Premium;
                        if (SRCCPremi > 0)
                        {
                            IsSRCCChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.SRCC, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("FLD") || item.CoverageID.ToUpper().Equals("FLDTLO")))
                    {
                        IsFLDEnabled = true;
                        FLDPremi += item.Premium;
                        if (FLDPremi > 0)
                        {
                            IsFLDChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.FLD, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("ETV") || item.CoverageID.ToUpper().Equals("ETVTLO")))
                    {
                        IsETVEnabled = true;
                        ETVPremi += item.Premium;
                        if (ETVPremi > 0)
                        {
                            IsETVChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.ETV, item.Year, item.IsBundling));
                        }
                    }
                    if ((item.CoverageID.ToUpper().TrimEnd().Equals("TRS") || item.CoverageID.ToUpper().Equals("TRRTLO")))
                    {
                        IsTSEnabled = true;
                        TSPremi += item.Premium;
                        if (TSPremi > 0)
                        {
                            IsTSChecked = true;
                        }
                        if (item.InterestID.ToUpper().TrimEnd().Equals("CASCO"))
                        {
                            coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                    CoverageTypeNonBasic.TS, item.Year, item.IsBundling));
                        }
                    }
                    //if(item.InterestID.equals("TPLPER") && item.CoverageID.equals("MVTPL1")) {
                    /** start 0040/URF/2017 */
                    if (item.InterestID.ToUpper().Equals("TPLPER") && item.CoverageID.ToUpper().Contains("MVTP"))
                    {
                        IsTPLEnabled = true;
                        IsTPLSIEnabled = true;
                        TPLPremi += item.Premium;
                        if (TPLPremi > 0)
                        {
                            IsTPLChecked = true;
                        }
                        if (item.Year == 1)
                        {
                            TPLSI = item.SumInsured;
                        }
                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.TPLPER, item.Year, item.IsBundling));
                    } /** end 0040/URF/2017 */
                    if (item.InterestID.ToUpper().Equals("PADRVR") || item.InterestID.ToUpper().Equals("PADDR1"))
                    {
                        IsPADRVRChecked = true;
                        IsPADRVRSIEnabled = true;
                        PADRVRPremi += item.Premium;
                        if (PADRVRPremi > 0)
                        {
                            IsPADRVRChecked = true;
                        }
                        if (item.Year == 1)
                        {
                            PADRVRSI = item.SumInsured;
                        }

                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.PADRVR, item.Year, item.IsBundling));
                    }
                    if (item.InterestID.ToUpper().Equals("PAPASS") || item.InterestID.ToUpper().Equals("PA24AV"))
                    {
                        IsPAPASSSIEnabled = true;
                        IsPASSEnabled = true;

                        PAPASSPremi += item.Premium;
                        if (PAPASSPremi > 0)
                        {
                            IsPAPASSChecked = true;
                        }
                        if (item.Year == 1)
                        {
                            PAPASSSI = item.SumInsured;
                        }
                        PASS = db.ExecuteScalar<int>("SELECT sitting FROM ordersimulationmv WHERE orderno=@0",OrderNo)-1;
                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                CoverageTypeNonBasic.PAPASS, item.Year, item.IsBundling));
                    }
                }
            }
            #region calculate discount renewal
            string OldPolicyNo = db.ExecuteScalar<string>("SELECT OldPolicyNo FROM Ordersimulation WHERE OrderNo=@0",OrderNo);
            if (!string.IsNullOrWhiteSpace(OldPolicyNo))
            {
                var RenDiscount = RetailRepository.GetRenewalDiscount(OldPolicyNo);
                var Disc = RetailRepository.GetWTDiscount(ProductCode, ProductCode);// New
                foreach (WTCommission ren in RenDiscount)
                {
                    if (ren.CommissionID.Contains("RD"))
                    {
                        RDP.Add(ren.Percentage);
                    }
                } 
                foreach (WTCommission D in Disc)
                {
                        DP.Add(D.Percentage);
                }
            }
            Discount = DP;
            RPTRenDiscount = RDP;
            #endregion
        }

        [HttpPost]
        public IHttpActionResult rateCalculationNonBasic(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            string Alert = "";
            try
            {
                string chItem = form.Get("chItem");
                ProductCode = form.Get("ProductCode");
                SumInsured = Convert.ToDouble(form.Get("SumInsured"));
                CoverageTypeNonBasic cType = chItem.Equals("SRCC") ? CoverageTypeNonBasic.SRCC : (chItem.Equals("FLD") ? CoverageTypeNonBasic.FLD : (chItem.Equals("ETV")) ? CoverageTypeNonBasic.ETV : (chItem.Equals("TS")) ? CoverageTypeNonBasic.TS : (chItem.Equals("TPLPER")) ? CoverageTypeNonBasic.TPLPER : (chItem.Equals("PADRVR")) ? CoverageTypeNonBasic.PADRVR : CoverageTypeNonBasic.PAPASS);
                IsTSChecked = chItem.Equals("TS") ? true : false;
                IsTSEnabled = chItem.Equals("TS") ? true : false;
                IsPADRVRChecked = chItem.Equals("PADRVR") ? true : false;
                IsPADRVRSIEnabled = chItem.Equals("PADRVR") ? true : false;
                IsPAPASSChecked = chItem.Equals("PAPASS") ? true : false;
                IsPAPASSSIEnabled = chItem.Equals("PAPASS") ? true : false;
                IsTPLChecked = chItem.Equals("TPL") ? true : false;
                IsTPLSIEnabled = chItem.Equals("TPL") ? true : false;
                IsSRCCChecked = chItem.Equals("SRCC") ? true : false;
                IsETVChecked = chItem.Equals("ETV") ? true : false;
                IsFLDChecked = chItem.Equals("FLD") ? true : false;
                bool ch = false; // checked item that thrown
                bool chPAPASS = (chItem.Equals("PAPASS") ? true : false);
                bool chPADRVR = (chItem.Equals("PADRVR") ? true : false);
                bool chTPL = (chItem.Equals("TPL") ? true : false);


                UsageCode = form.Get("UsageCode");
                type = form.Get("type");
                CityCode = form.Get("CityCode");

                TPLCoverageID = form.Get("TPLCoverageID");
                PADRVRSI = Convert.ToDouble(form.Get("PADRVRSI").Equals("null") ? "0" : form.Get("PADRVRSI"));
                PAPASSSI = Convert.ToDouble(form.Get("PAPASSSI").Equals("null") ? "0" : form.Get("PAPASSSI"));
                PASS = Convert.ToInt32(form.Get("PASS").Equals("null") ? "0" : form.Get("PASS"));
                TPLSI = Convert.ToDouble(form.Get("TPLSI"));
                AccessSI = Convert.ToDouble(form.Get("AccessSI").Equals("null") ? "0" : form.Get("AccessSI"));


                bool isExtentedBundling = (form.Get("isExtentedBundling").Equals("true") ? true : false);
                bool chOne = (form.Get("chOne").Equals("1") ? true : false);
                bool chTwo = (form.Get("chTwo").Equals("1") ? true : false);
                bool chThree = (form.Get("chThree").Equals("1") ? true : false);



                bool ischOneEnabled = (form.Get("ischOneEnabled").Equals("1") ? true : false);
                bool ischTwoEnabled = (form.Get("ischTwoEnabled").Equals("1") ? true : false);
                bool ischThreeEnabled = (form.Get("ischThreeEnabled").Equals("1") ? true : false);
                string coveragePeriodNonBasic = form.Get("coveragePeriodNonBasicItems");
                string coveragePeriod = form.Get("coveragePeriodItems");
                string calculatedPremi = form.Get("CalculatedPremiItems");
                coveragePeriodNonBasicItems = JsonConvert.DeserializeObject<List<CoveragePeriodNonBasic>>(coveragePeriodNonBasic);
                coveragePeriodItems = JsonConvert.DeserializeObject<List<CoveragePeriod>>(coveragePeriod);
                CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculatedPremi);

                getAdminFee();
                List<CoveragePeriodNonBasic> removeperiodnonbasic = new List<CoveragePeriodNonBasic>();
                foreach (CoveragePeriodNonBasic cpnb in coveragePeriodNonBasicItems)
                {
                    if (cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC || cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD || cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                    {
                        IsSRCCChecked = true;
                        IsFLDChecked = true;
                        IsETVChecked = true;
                    }
                    if (cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER)
                    {
                        bool tplexist = false;
                        foreach (CalculatedPremi cp in CalculatedPremiItems)
                        {
                            if (cp.InterestID.Equals("TPLPER") && cp.Year == cpnb.Year)
                            {
                                tplexist = true;
                            }
                        }
                        if (!tplexist)
                        {
                            removeperiodnonbasic.Add(cpnb);
                        }
                    }
                }
                //Removeperiodnonbasic
                foreach (CoveragePeriodNonBasic item in removeperiodnonbasic)
                {
                    coveragePeriodNonBasicItems.Remove(item);
                }
                bool isValid = false;
                if (chItem.Equals("TS"))
                {
                    bool isBundlingOne = false;
                    bool isBundlingTwo = false;
                    bool isBundlingThree = false;

                    bool isCheckedOne = false;
                    bool isCheckedTwo = false;
                    bool isCheckedThree = false;
                    List<CoveragePeriodNonBasic> cbasic = new List<CoveragePeriodNonBasic>();
                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                    {
                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC))
                        {
                            cbasic.Add(item);
                        }
                    }
                    foreach (CoveragePeriodNonBasic bundlingItem in cbasic)
                    {
                        if (bundlingItem.Year == 1)
                        {
                            isCheckedOne = true;
                            if (bundlingItem.IsBundling)
                            {
                                isBundlingOne = true;
                            }
                        }
                        else if (bundlingItem.Year == 2)
                        {
                            isCheckedTwo = true;
                            if (bundlingItem.IsBundling)
                            {
                                isBundlingTwo = true;
                            }
                        }
                        else if (bundlingItem.Year == 3)
                        {
                            isCheckedThree = true;
                            if (bundlingItem.IsBundling)
                            {
                                isBundlingThree = true;
                            }
                        }
                    }
                    if ((isBundlingOne && isBundlingTwo && isBundlingThree &&
                     (!chOne && (chTwo || chThree))) ||
                     (isBundlingTwo && isBundlingThree && !chTwo && chThree) ||
                     (!chOne && ((chTwo && ischTwoEnabled && !isBundlingTwo) ||
                     (chThree && ischThreeEnabled && !isBundlingThree))) ||
                     !chTwo && (chThree && ischThreeEnabled && !isBundlingThree))
                    {
                        return Json(new { status = true, message = "OK", Alert = "Extended warranties for the previous year must be selected." }, Util.jsonSerializerSetting());
                    }
                    else if ((chOne && !isCheckedOne) || (chTwo && !isCheckedTwo) ||
                            (chThree && !isCheckedThree))
                    {
                        return Json(new
                        {
                            status = true,
                            message = "OK",
                            Alert = "Extended warranties TS can only be selected in the same " +
                                "period with Strike, Riot, Commotion (SRCC), Flood & Windstorm, " +
                                "dan Earthquake, Tsunami, Volcanic Eruption."
                        }, Util.jsonSerializerSetting());

                    }
                    else
                    {
                        isValid = true;
                    }
                    IsTSChecked = (chOne == false && chTwo == false && chThree == false) ? false : IsTSChecked;
                }
                else if (chItem.Equals("TPL"))
                {
                    //validasi TPL
                    bool isFail = false;
                    foreach (CoveragePeriod item in coveragePeriodItems)
                    {
                        if (chOne && item.Year == 1 && item.CoverageType == CoverageType.TLO)
                        {
                            isFail = true;
                            break;
                        }
                        else if (chTwo && item.Year == 2 && item.CoverageType == CoverageType.TLO)
                        {
                            isFail = true;
                            break;
                        }
                        else if (chThree && item.Year == 3 && item.CoverageType == CoverageType.TLO)
                        {
                            isFail = true;
                            break;
                        }
                    }
                    if (isFail)
                    {
                        return Json(new
                        {
                            status = true,
                            message = "OK",
                            Alert = "Extended warranties TPL can only be selected in the same " +
                                    "period with the Comprehensive"
                        }, Util.jsonSerializerSetting());

                    }
                    else if ((!chOne && ((chTwo && ischTwoEnabled) ||
                          (chThree && ischThreeEnabled))) ||
                          !chTwo && (chThree && ischThreeEnabled))
                    {
                        return Json(new { status = true, message = "OK", Alert = "Extended warranties for the previous year must be selected." }, Util.jsonSerializerSetting());

                    }
                    else
                    {
                        isValid = true;
                    }

                    IsTPLChecked = (chOne == false && chTwo == false && chThree == false) ? false : IsTPLChecked;
                }
                else if ((!chOne && ((chTwo && ischTwoEnabled) ||
                      (chThree && ischThreeEnabled))) ||
                      !chTwo && (chThree && ischThreeEnabled))
                {
                    return Json(new { status = true, message = "OK", Alert = "Extended warranties for the previous year must be selected." }, Util.jsonSerializerSetting());

                }
                else
                {
                    isValid = true;
                }
                if (isValid)
                {
                    List<int> checkedItems = new List<int>();
                    List<int> uncheckedItems = new List<int>();

                    bool isExist = false;
                    if (chOne && ischOneEnabled)
                    {
                        //check if already added to CoveragePeriodNonBasicItems
                        foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                        {
                            if (item.CoverageTypeNonBasic.Equals(cType) && item.Year == 1)
                            {
                                isExist = true;
                            }
                        }
                        if (!isExist)
                        {
                            if (isExtentedBundling)
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.SRCC, 1, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.FLD, 1, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.ETV, 1, false));
                            }
                            else
                            {
                                coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(cType, 1, false));
                                if (chItem.Equals("PAPASS"))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR))
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (!exists)
                                    {
                                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                                CoverageTypeNonBasic.PADRVR, 1, false));
                                        checkedItems.Add(1);
                                    }
                                }
                            }
                        }
                    }

                    isExist = false;
                    if (chTwo && ischTwoEnabled)
                    {
                        foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                        {
                            if (item.CoverageTypeNonBasic.Equals(cType) && item.Year == 2)
                            {

                                isExist = true;
                            }
                        }

                        if (!isExist)
                        {
                            if (isExtentedBundling)
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.SRCC, 2, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.FLD, 2, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.ETV, 2, false));
                            }
                            else
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(cType, 2, false));
                                if (chItem.Equals("PAPASS"))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR) && item.Year == 2)
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (!exists)
                                    {
                                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                                CoverageTypeNonBasic.PADRVR, 2, false));
                                        checkedItems.Add(2);
                                    }

                                }
                            }
                        }
                    }
                    isExist = false;
                    if (chThree && ischThreeEnabled)
                    {
                        foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                        {
                            if (item.CoverageTypeNonBasic.Equals(cType) && item.Year == 3)
                            {
                                isExist = true;
                            }
                        }
                        if (!isExist)
                        {
                            if (isExtentedBundling)
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.SRCC, 3, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.FLD, 3, false));
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(CoverageTypeNonBasic.ETV, 3, false));
                            }
                            else
                            {
                                coveragePeriodNonBasicItems.Add(
                                        new CoveragePeriodNonBasic(cType, 3, false));
                                if (chItem.Equals("PAPASS"))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR) && item.Year == 3)
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (!exists)
                                    {
                                        coveragePeriodNonBasicItems.Add(new CoveragePeriodNonBasic(
                                                CoverageTypeNonBasic.PADRVR, 3, false));
                                        checkedItems.Add(3);
                                    }

                                }
                            }

                        }
                    }

                    if (chOne || chTwo || chThree)
                    {
                        if (isExtentedBundling)
                        {
                            IsSRCCChecked = true;
                            IsFLDChecked = true;
                            IsETVChecked = true;
                            IsTSEnabled = true;
                        }
                        else
                        {
                            ch = true;
                            if (chItem.Equals("TPL"))
                            {

                                IsTPLEnabled = true;
                            }
                            else if (chItem.Equals("PADRVR"))
                            {
                                IsPADRVRSIEnabled = true;
                            }
                            else if (chItem.Equals("PAPASS"))
                            {
                                IsPAPASSSIEnabled = true;
                                IsPASSEnabled = true;
                                IsPAPASSEnabled = true;
                                IsPADRVRChecked = true;
                                IsPADRVRSIEnabled = true;
                            }
                        }
                    }
                    else
                    {
                        if (isExtentedBundling)
                        {
                            IsSRCCChecked = false;
                            IsFLDChecked = false;
                            IsETVChecked = false;
                            IsTSChecked = false;
                            IsTSEnabled = false;
                        }
                        else
                        {
                            ch = false;
                            if (chItem.Equals("TPL"))
                            {
                                IsTPLSIEnabled = false;
                                IsTPLChecked = false;
                                IsTPLEnabled = false;
                                IsTPLChecked = false;
                                TPLSI = 0;
                                TPLPremi = 0;
                            }
                            else if (chItem.Equals("PADRVR"))
                            {
                                IsPADRVRChecked = false;
                                IsPAPASSChecked = false;
                                IsPAPASSSIEnabled = false;
                                IsPASSEnabled = false;
                                IsPAPASSSIEnabled = false;
                                IsPADRVRSIEnabled = false;
                                PADRVRSI = 0;
                                PAPASSSI = 0;
                                PASS = 0;
                            }
                            else if (chItem.Equals("PAPASS"))
                            {
                                IsPAPASSSIEnabled = false;
                                IsPAPASSChecked = false;
                                IsPASSEnabled = false;
                                PASS = 0;
                                PAPASSSI = 0;
                                foreach (CoveragePeriodNonBasic cpnb in coveragePeriodNonBasicItems)
                                {
                                    if (cpnb.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR)
                                    {
                                        IsPADRVRChecked = true;
                                        IsPADRVRSIEnabled = true;
                                    }
                                }
                            }
                        }
                    }
                    for (int i = coveragePeriodNonBasicItems.Count(); i > 0; i--)
                    {
                        CoveragePeriodNonBasic cp = coveragePeriodNonBasicItems[i - 1];
                        if (isExtentedBundling)
                        {
                            if (((cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.FLD)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.ETV))
                                    && cp.Year == 1 && !chOne)
                                    || ((cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.FLD)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.ETV))
                                    && cp.Year == 2 && !chTwo)
                                    || ((cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.SRCC)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.FLD)
                                    || cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.ETV))
                                    && cp.Year == 3 && !chThree))
                            {
                                coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                bool exists = false;
                                foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                {
                                    if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.TS) && item.Year == cp.Year)
                                    {
                                        exists = true;
                                    }
                                }
                                if (exists)
                                {
                                    if (!uncheckedItems.Contains(cp.Year))
                                    {
                                        uncheckedItems.Add(cp.Year);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if ((cp.CoverageTypeNonBasic.Equals(cType) && cp.Year == 1 && !chOne)
                                    || (cp.CoverageTypeNonBasic.Equals(cType) && cp.Year == 2 && !chTwo)
                                    || (cp.CoverageTypeNonBasic.Equals(cType) && cp.Year == 3 && !chThree))
                            {
                                coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                if (cp.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PADRVR))
                                {
                                    bool exists = false;
                                    foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
                                    {
                                        if (item.CoverageTypeNonBasic.Equals(CoverageTypeNonBasic.PAPASS) && item.Year == cp.Year)
                                        {
                                            exists = true;
                                        }
                                    }
                                    if (exists)
                                    {
                                        uncheckedItems.Add(cp.Year);
                                    }
                                }
                            }
                        }
                    }

                    if (chItem.Equals("SRCC") || chItem.Equals("chFLD") || chItem.Equals("ETV"))
                    {
                        RefreshBundlingPremi();
                    }
                    else if (chItem.Equals("TS"))
                    {
                        RefreshTS();
                    }
                    else if (chItem.Equals("TPL"))
                    {
                        RefreshTPL(chTPL);
                    }
                    else if (chItem.Equals("PADRVR"))
                    {
                        RefreshPADRVR(chPADRVR);
                    }
                    else if (chItem.Equals("PAPASS"))
                    {
                        RefreshPAPASS(chPAPASS);
                    }
                    checkedItems.Sort();
                    uncheckedItems.Sort();

                    string message = "";
                    if (checkedItems.Count() > 0)
                    {
                        message = "Extended warranties PA Driver automatically selected for the year "; // 001
                        //auto checked
                        foreach (int i in checkedItems)
                        {
                            message += i + ",";
                        }
                        message = message.Substring(0, message.Length - 1);
                        message += ".";
                    }

                    //auto unchecked
                    if (uncheckedItems.Count() > 0)
                    {
                        if (chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV"))
                        {
                            message = "Extended warranties TS are not automatically selected for " +
                                    "the year"; // 001
                        }
                        else if (chItem.Equals("PADRVR"))
                        {
                            message = "Passenger PA Extended warranties are not automatically " +
                                    "selected for the year "; // 001
                        }

                        foreach (int i in uncheckedItems)
                        {
                            message += i + ",";
                        }

                        //delete dependency control base on each depend
                        int isChecked = 0;
                        for (int i = coveragePeriodNonBasicItems.Count(); i > 0; i--)
                        {
                            CoveragePeriodNonBasic cp = coveragePeriodNonBasicItems[i - 1];
                            int counter = 0;
                            foreach (int j in uncheckedItems)
                            {
                                if (chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV") &&
                                   cp.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                                {
                                    if (cp.Year == j)
                                    {
                                        coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                        counter++;
                                    }
                                }
                                else if (chItem.Equals("PADRVR") &&
                                      cp.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS)
                                {
                                    if (cp.Year == j)
                                    {
                                        coveragePeriodNonBasicItems.RemoveAt(i - 1);
                                        counter++;
                                    }
                                }
                            }
                            if (counter == 0)
                            {
                                isChecked++;
                            }
                        }

                        if ((chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV")) && isChecked == 0)
                        {
                            IsTSChecked = false;
                        }
                        else if (chItem.Equals("PADRVR") && isChecked == 0)
                        {

                            IsPAPASSChecked = false;
                            IsPASSEnabled = false;
                            IsPAPASSSIEnabled = false;
                            PASS = 0;
                            PAPASSSI = 0;
                        }
                        message = message.Substring(0, message.Length - 1);
                        message += ".";
                    }
                    if (!message.Equals(""))
                    {
                        if ((chItem.Equals("SRCC") || chItem.Equals("FLD") || chItem.Equals("ETV")))
                        {
                            RefreshTS();
                        }
                        else if (chItem.Equals("PADRVR"))
                        {
                            RefreshPAPASS(chPAPASS);
                        }
                        else if (chItem.Equals("PAPASS"))
                        {
                            RefreshPADRVR(chPADRVR);
                        }

                        String finalMessage = message;
                        String[] messageSplit = message.Split(',');
                        if (messageSplit.Length == 2)
                        {
                            finalMessage = messageSplit[0] + " dan " + messageSplit[1];
                        }
                        else if (messageSplit.Length == 3)
                        {
                            finalMessage = messageSplit[0] + ", " + messageSplit[1] + ", dan " +
                                    messageSplit[2];
                        }

                        Alert = finalMessage;
                    }




                }
                //recalculate bundling cover and accessory

                foreach (CalculatedPremi cp in CalculatedPremiItems)
                {
                    if (cp.CoverageID.TrimEnd().Equals("SRCC") || cp.CoverageID.TrimEnd().Equals("SRCTLO"))
                    {
                        SRCCPremi += cp.Premium;
                    }
                    if (cp.CoverageID.TrimEnd().Equals("FLD") || cp.CoverageID.TrimEnd().Equals("FLDTLO"))
                    {
                        FLDPremi += cp.Premium;
                    }
                    if (cp.CoverageID.TrimEnd().Equals("ETV") || cp.CoverageID.TrimEnd().Equals("ETVTLO"))
                    {
                        ETVPremi += cp.Premium;
                    }
                    if (cp.CoverageID.TrimEnd().Equals("ALLRIK") && cp.InterestID.TrimEnd().Equals("ACCESS"))
                    {
                        ACCESSPremi += cp.Premium;
                    }


                    //Load previous non basic Items
                    RefreshPremi();
                    CalculateLoading();
                    CalculateTotalPremi();
                }

                return Json(new
                {
                    status = true,
                    message = "OK",
                    CalculatedPremiItems,
                    coveragePeriodItems,
                    coveragePeriodNonBasicItems,
                    IsTPLEnabled,
                    IsTPLChecked,
                    IsTPLSIEnabled,
                    IsSRCCChecked,
                    IsFLDChecked,
                    IsETVChecked,
                    IsTSChecked,
                    IsTSEnabled,
                    IsPADRVRChecked,
                    IsPADRVRSIEnabled,
                    IsPASSEnabled,
                    IsPAPASSSIEnabled,
                    IsPAPASSEnabled,
                    IsPAPASSChecked,
                    SRCCPremi,
                    FLDPremi,
                    ETVPremi,
                    TSPremi,
                    PADRVRPremi,
                    PAPASSPremi,
                    TPLPremi,
                    ACCESSPremi,
                    //   AdminFee,
                    TotalPremi,
                    Alert
                }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }



        private void getAdminFee()
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    string query = @"select top 1 Policy_Fee1+Stamp_Duty1 from prd_policy_fee where product_code=@0 AND Order_Type=1 AND Lower_limit<=@1  AND Status=1 order by Lower_Limit desc";
                    AdminFee = Convert.ToDouble(db.FirstOrDefault<PRD_POLICY_FEE>(query, ProductCode, SumInsured).Policy_Fee1);
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        [HttpPost]
        public IHttpActionResult rateCalculation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            string Alert = "";
            try
            {

                string OrderNo = form.Get("OrderNo");
                OrderNo = string.IsNullOrEmpty(OrderNo) ? "" : (OrderNo.Equals("null") ? "" : OrderNo);
                ProductCode = form.Get("ProductCode");
                string productTypeCode = form.Get("productTypeCode");
                int comprePeriod = Convert.ToInt32(form.Get("comprePeriod"));
                type = form.Get("type");
                string BrandCode = form.Get("BrandCode");
                string VehicleCode = form.Get("VehicleCode");
                CityCode = form.Get("CityCode");
                string ModelCode = form.Get("ModelCode");
                string Series = form.Get("Series");
                UsageCode = form.Get("UsageCode");
                Year = Convert.ToInt32(form.Get("Year"));
                int TLOPeriod = Convert.ToInt32(form.Get("TLOPeriod"));
                int IsNew = Convert.ToInt32(form.Get("IsNew"));
                PADRVRSI = Convert.ToInt32(form.Get("PADRVRSI"));
                PAPASSSI = Convert.ToInt32(form.Get("PAPASSSI"));
                PASS = Convert.ToInt32(form.Get("PASS"));
                AccessSI = Convert.ToInt32(form.Get("AccessSI"));
                SumInsured = Convert.ToInt64(form.Get("SumInsured"));


                //string Channel = form.Get("Channel");
                //string ChannelSource = form.Get("ChannelSource");
                //if (Channel.Equals("EXT"))
                //{
                //    ProductCode = getProductExternal(ChannelSource, Convert.ToBoolean(IsNew));
                //}
                if (string.IsNullOrEmpty(productTypeCode) || string.IsNullOrEmpty(ProductCode) || (comprePeriod + TLOPeriod == 0) || SumInsured == 0)
                {
                    return Json(new { status = false, message = "Param Is Not Valid Or Not Completed" });

                }
                string CalculatedPremi = form.Get("CalculatedPremi");
                TPLCoverageID = form.Get("TPLCoverageID");
                IsSRCCChecked = string.IsNullOrEmpty(form.Get("IsSRCCChecked")) ? false : form.Get("IsSRCCChecked").Equals("1") ? true : false;
                IsFLDChecked = string.IsNullOrEmpty(form.Get("IsFLDChecked")) ? false : form.Get("IsFLDChecked").Equals("1") ? true : false;
                IsETVChecked = string.IsNullOrEmpty(form.Get("IsETVChecked")) ? false : form.Get("IsETVChecked").Equals("1") ? true : false;
                IsTSChecked = string.IsNullOrEmpty(form.Get("IsTSChecked")) ? false : form.Get("IsTSChecked").Equals("1") ? true : false;
                CalculatedPremi = form.Get("CalculatedPremi");
                string coveragePeriodNonBasicItem = form.Get("coveragePeriodNonBasicItems");
                string coveragePeriodItem = form.Get("coveragePeriodItems");
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                CityCode = form.Get("CityCode");
                type = form.Get("type");
                UsageCode = form.Get("UsageCode");
                if (!string.IsNullOrEmpty(CalculatedPremi))
                {
                    CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(CalculatedPremi);
                }
                if (!string.IsNullOrEmpty(coveragePeriodNonBasicItem))
                {
                    coveragePeriodNonBasicItems = JsonConvert.DeserializeObject<List<CoveragePeriodNonBasic>>(coveragePeriodNonBasicItem);
                }
                if (!string.IsNullOrEmpty(coveragePeriodItem))
                {
                    coveragePeriodItems = JsonConvert.DeserializeObject<List<CoveragePeriod>>(coveragePeriodItem);
                }
                string query = "";
                if (PADRVRSI != 0)
                {
                    bool chPADRVR = true;
                    getAdminFee();
                    RefreshPADRVR(chPADRVR);
                    RecallingCoverageNonBasic();
                    Alert = calculateAgainPremi();
                }
                else if (PAPASSSI != 0)
                {
                    bool chPAPASS = true;
                    getAdminFee();
                    RefreshPAPASS(chPAPASS);
                    RecallingCoverageNonBasic();
                    Alert = calculateAgainPremi();
                }
                else if (AccessSI != 0)
                {

                    getAdminFee();
                    RefreshAccessSI();
                    //CalculatedPremiItems.AddRange(cpremi);
                    RecallingCoverageNonBasic();

                    // Alert = calculateAgainPremi();
                }
                else if (!string.IsNullOrEmpty(TPLCoverageID))
                {
                    TPLSI = db.ExecuteScalar<double>("SELECT TOP 1 BC_Up_To_SI FROM Mapping_Cover_Progressive WHERE Coverage_ID=@0 ", TPLCoverageID);

                    getAdminFee();
                    RefreshTPL(true);

                    RecallingCoverageNonBasic();
                    Alert = calculateAgainPremi();
                }
                else
                {
                    #region reset coverage


                    #region Initialize Vehicle Price Tolerance
                    query = @"SELECT Price FROM Vehicle WHERE VehicleCode=@0 AND BrandCode=@1 AND ModelCode=@2 AND Series=@3 AND Type=@4 AND Year=@5";
                    VehiclePrice = Convert.ToDouble(db.FirstOrDefault<Vehicle>(query, VehicleCode, BrandCode, ModelCode, Series, type, Year).Price);
                    query = @"SELECT * FROM VehiclePriceTolerance WHERE IsNew=@0 AND IsATPM=1 AND FormType=1 AND VehicleCategory=1 AND MaxSI>= @1 ORDER BY MaxSI ASC";
                    vptModel = db.FirstOrDefault<VehiclePriceTolerance>(query, IsNew, VehiclePrice);
                    #endregion

                    #region Initialize CoveragePeriode

                    int year = 0;
                    if (comprePeriod > 0)
                    {
                        for (int i = 0; i < comprePeriod; i++)
                        {
                            year++;
                            CoveragePeriod cp = new CoveragePeriod();
                            cp.CoverageType = CoverageType.Comprehensive;
                            cp.Year = year;
                            coveragePeriodItems.Add(cp);
                        }
                    }
                    if (TLOPeriod > 0)
                    {
                        for (int i = 0; i < TLOPeriod; i++)
                        {
                            year++;
                            CoveragePeriod cp = new CoveragePeriod();
                            cp.CoverageType = CoverageType.TLO;
                            cp.Year = year;
                            coveragePeriodItems.Add(cp);
                        }
                    }
                    #endregion

                    #region Initialize Product Setting
                    if (string.IsNullOrEmpty(productTypeCode) &&
                   string.IsNullOrEmpty(ProductCode) &&
                   (comprePeriod + TLOPeriod == 0) && SumInsured < 0 &&
                   string.IsNullOrEmpty(CityCode) &&
                   string.IsNullOrEmpty(UsageCode))
                    {
                        return Json(new { status = false, message = "Data Not Completed" });

                    }

                    if (coveragePeriodNonBasicItems != null && coveragePeriodNonBasicItems.Count() > 0)
                    {
                        RecallingCoverageNonBasic();

                    }
                    else
                    {
                        query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0";
                        prdAgreedValueMaster = db.Fetch<Prd_Agreed_Value>(query, ProductCode);

                        getAdminFee();

                        query = @"SELECT * FROM Prd_Interest_Coverage WHERE Product_Code=@0";
                        prdInterestCoverage = db.Fetch<Prd_Interest_Coverage>(query, ProductCode);
                        foreach (Prd_Interest_Coverage prd in prdInterestCoverage)
                        {
                            if (prd.Coverage_Id.Trim().Equals("SRCC") ||
                                    prd.Coverage_Id.Trim().Equals("SRCTLO"))
                            {
                                IsSRCCEnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Equals("FLD") ||
                                  prd.Coverage_Id.Trim().Equals("FLDTLO"))
                            {
                                IsFLDEnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Equals("ETV") ||
                                  prd.Coverage_Id.Trim().Equals("ETVTLO"))
                            {
                                IsETVEnabled = true;
                            }
                            if (prd.Coverage_Id.Trim().Equals("TRS") ||
                                 prd.Coverage_Id.Trim().Equals("TRRTLO"))
                            {
                                IsTSEnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Contains("MVTP") &&
                                  comprePeriod > 0)
                            {
                                IsTPLEnabled = true;
                            }
                            else if (prd.Interest_ID.Trim().Equals("PADRVR"))
                            {
                                IsPADRVREnabled = true;
                            }
                            else if (prd.Coverage_Id.Trim().Equals("C1"))
                            {
                                IsPAPASSEnabled = true;
                            }
                        }
                        IsACCESSEnabled = true;

                        if (coveragePeriodNonBasicItems.Count() > 0)
                        {

                        }
                        else
                        {
                            query = "SELECT a.Coverage_Id, a.Scoring_Code, b.Auto_Apply, 1 Year  FROM " +
                        "Prd_Interest_Coverage a INNER JOIN B2B_Interest_Coverage b ON " +
                        "a.Coverage_Id=b.Coverage_Id AND a.Interest_ID=b.Interest_Id AND " +
                        "a.Product_Code=b.Product_Code WHERE a.Product_Code=@0 AND b.Vehicle_Type='ALL   ' AND b.Interest_Id='CASCO ' AND " +
                                    "Auto_Apply IN (@@autoapply)";
                            string autoapply = "";
                            if (comprePeriod != 0 &&
                                TLOPeriod != 0)
                            {
                                autoapply = "1, 2";
                            }
                            else if (comprePeriod != 0)
                            {
                                autoapply = "1";
                            }
                            else if (TLOPeriod != 0)
                            {
                                autoapply = "2";
                            }
                            query = query.Replace("@@autoapply", autoapply);
                            List<CoverageBundling> coverageBundlingCheckItems = db.Fetch<CoverageBundling>(query.Replace("@1", autoapply), ProductCode);
                            List<CoverageBundling> coverageBundlingItems = new List<CoverageBundling>();


                            foreach (CoveragePeriod item in coveragePeriodItems)
                            {
                                foreach (CoverageBundling item2 in coverageBundlingCheckItems)
                                {
                                    if ((item.CoverageType == CoverageType.Comprehensive &&
                                            item2.Auto_Apply == 1) ||
                                            (item.CoverageType == CoverageType.TLO && item2.Auto_Apply == 2))
                                    {
                                        bool exist = false;
                                        foreach (CoverageBundling check in coverageBundlingItems)
                                        {
                                            if (check.Year.Equals(item.Year))
                                            {
                                                exist = true;
                                            }
                                        }
                                        if (!exist)
                                        {
                                            CoverageBundling cb = new CoverageBundling();
                                            cb.Coverage_Id = item2.Coverage_Id;
                                            cb.Scoring_Code = item2.Scoring_Code;
                                            cb.Auto_Apply = item2.Auto_Apply;
                                            cb.Year = item.Year;
                                            coverageBundlingItems.Add(cb);
                                        }
                                        break;
                                    }
                                }
                            }

                            foreach (CoverageBundling item in coverageBundlingItems)
                            {
                                if (!string.IsNullOrEmpty(item.Scoring_Code.TrimEnd()))
                                {
                                    query = @"SELECT * FROM Prd_Agreed_Value WHERE Product_Code=@0 AND UPTO=@1";
                                    Prd_Agreed_Value agreedValue = db.FirstOrDefault<Prd_Agreed_Value>(query, ProductCode, item.Year * 12.0);
                                    // HOTFIX 20151120 - BSY
                                    query = "SELECT * FROM Dtl_Rate_Factor WHERE Insurance_type='000401' AND " +
                                            "Score_Code='" + item.Scoring_Code + "' AND Coverage_id='" +
                                            item.Coverage_Id + "'";
                                    List<Dtl_Rate_Factor> rateFactor;

                                    try
                                    {
                                        rateFactor = db.Fetch<Dtl_Rate_Factor>(query);
                                    }
                                    catch (Exception ex)
                                    {
                                        rateFactor = new List<Dtl_Rate_Factor>();
                                    }

                                    if (rateFactor.Count() > 0)
                                    {
                                        //get scoringDtl_Rate_Scorin
                                        Dtl_Rate_Scoring rateScoring = new Dtl_Rate_Scoring();
                                        try
                                        {
                                            rateScoring = GetRateScoring(rateFactor,
                                               item.Scoring_Code, item.Coverage_Id,
                                               Convert.ToDouble(agreedValue.Rate) / 100 * SumInsured, true, CityCode, type, UsageCode);

                                        }
                                        catch (Exception ex)
                                        {
                                            //handle mapping exiasting
                                            rateScoring = null;
                                            IsSRCCEnabled = false;
                                            IsETVEnabled = false;
                                            IsFLDEnabled = false;
                                            IsTPLEnabled = false;
                                            IsPADRVREnabled = false;
                                            IsPASSEnabled = false;
                                            IsPAPASSEnabled = false;
                                            IsTSEnabled = false;
                                        }

                                        if (rateScoring != null)
                                        {
                                            if (rateScoring.Rate > 0)
                                            {
                                                IncludeBundlingCoverage(item.Year, true);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    IncludeBundlingCoverage(item.Year, true);

                                }
                            }
                        }
                    }

                    #endregion

                    #region Refresh Bundling Premi
                    RefreshBundlingPremi();
                    #endregion


                    //if (!string.IsNullOrEmpty(OrderNo))
                    //{
                   // InitializeCoverageItems(OrderNo,out RPTRenDiscount);
                    if (CalculatedPremiItems.Count() == 0)
                    {

                        #region Initialize Premi
                        InitializePremi();
                        #endregion
                    }
                    //}
                    //else
                    //{

                    //    #region Initialize Premi
                    //    InitializePremi();
                    //    #endregion
                    //}

                    CalculateLoading();
                    CalculateTotalPremi();
                    // Alert = CheckSumInsured(); --0089 URF 2019

                    #endregion
                }
                return Json(new
                {
                    status = true,
                    message = "OK",
                    ProductCode,
                    CalculatedPremiItems,
                    coveragePeriodItems,
                    coveragePeriodNonBasicItems,
                    IsTPLEnabled,
                    IsTPLChecked,
                    IsTPLSIEnabled,
                    IsSRCCChecked,
                    IsSRCCEnabled,
                    IsFLDChecked,
                    IsFLDEnabled,
                    IsETVChecked,
                    IsETVEnabled,
                    IsTSChecked,
                    IsTSEnabled,
                    IsPADRVRChecked,
                    IsPADRVREnabled,
                    IsPADRVRSIEnabled,
                    IsPASSEnabled,
                    IsPAPASSSIEnabled,
                    IsPAPASSEnabled,
                    IsPAPASSChecked,
                    IsACCESSChecked,
                    IsACCESSEnabled,
                    IsACCESSSIEnabled,
                    SRCCPremi,
                    FLDPremi,
                    ETVPremi,
                    TSPremi,
                    PADRVRPremi,
                    PAPASSPremi,
                    TPLPremi,
                    ACCESSPremi,
                    AdminFee,
                    TotalPremi,
                    Alert
                }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult BasicPremiCalculation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            #region declare param
            List<DetailScoring> dtlScoring = new List<DetailScoring>();
            List<decimal> RenDiscountPct = new List<decimal>();
            List<decimal> DiscountPct = new List<decimal>();
            int comprePeriod = Convert.ToInt32(form.Get("comprePeriod"));
            int TLOPeriod = Convert.ToInt32(form.Get("TLOPeriod"));
            string vPeriodFrom = form.Get("PeriodFrom");
            string vPeriodTo = form.Get("PeriodTo");
            // string vPeriodFromCover = form.Get("PeriodFromCover");
            // string vPeriodToCover = form.Get("PeriodToCover");
            string vtype = form.Get("vtype");
            string vcitycode = form.Get("vcitycode");
            string vusagecode = form.Get("vusagecode");
            string vyear = form.Get("vyear");
            string vsitting = form.Get("vsitting");
            ProductCode = form.Get("vProductCode");
            string vMouID = ProductCode;//form.Get("vMouID");
            decimal vTSInterest = Convert.ToDecimal(form.Get("vTSInterest"));
            decimal vPrimarySI = Convert.ToDecimal(form.Get("vPrimarySI"));
            string vCoverageId = form.Get("vCoverageId");
            string vInterestId = "CASCO";//form.Get("vInterestId");
            string vModel = form.Get("vModel");
            string vBrand = form.Get("vBrand");
            string vIsNew = form.Get("isNew");
            string pQuotationNo = form.Get("pQuotationNo");
            string Ndays = form.Get("Ndays");
            string OldPolicyNo = form.Get("OldPolicyNo");
                //int calcmethod = Convert.ToInt16(form.Get("calcMethod"));
            if (string.IsNullOrEmpty(Ndays)|| string.IsNullOrEmpty(vCoverageId) || string.IsNullOrEmpty(ProductCode)  || string.IsNullOrEmpty(vusagecode) || string.IsNullOrEmpty(vcitycode) || string.IsNullOrEmpty(vPeriodFrom)
                || string.IsNullOrEmpty(vPeriodTo))
            {
                return Json(new
             {
                 status = false,
                 message = "Param Not Completed"
             });
            }
            DateTime PeriodFrom = DateTime.ParseExact(vPeriodFrom, "yyyy-MM-dd", null);
            DateTime PeriodTo = DateTime.ParseExact(vPeriodTo, "yyyy-MM-dd", null);
            DateTime PeriodFromCover = PeriodFrom;
            DateTime PeriodToCover = PeriodTo;
            List<CalculatedPremi> result = new List<CalculatedPremi>();
            #region set vehicleyear to year now if not choose vehicle
                if (Convert.ToInt16(vyear) == 0)
                {
                    vyear = Convert.ToString(DateTime.Now.Year);
                }
         
            #endregion
            #endregion

            try
            {
                #region check product code is valid in mapping segment
                if (MobileRepository.isValidMappingSegment(ProductCode)) { 
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                #region Initialize Enable Coverage
                InitializeEnableCoverage();
                #endregion
                #region initialize Param

                List<CoverageParam> CoverageList = new List<CoverageParam>();
                if (comprePeriod == 0 && TLOPeriod == 0)
                {
                    CoverageList.Add(new CoverageParam{CoverageId=vCoverageId,PeriodFromCover=PeriodFromCover,PeriodToCover=PeriodToCover});
                }
                else
                {
                    if (comprePeriod > 0 && TLOPeriod > 0)
                    {
                        CoverageList.Add(new CoverageParam { CoverageId = "ALLRIK", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodFromCover.AddYears(comprePeriod) });
                        CoverageList.Add(new CoverageParam { CoverageId = "TLO", PeriodFromCover = PeriodFromCover.AddYears(comprePeriod), PeriodToCover = PeriodFromCover.AddYears(comprePeriod+TLOPeriod) });
               
                    }
                    else if (comprePeriod > 0)
                    {
                        CoverageList.Add(new CoverageParam { CoverageId = "ALLRIK", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });
               
                    }
                    else if (TLOPeriod > 0)
                    {
                        CoverageList.Add(new CoverageParam { CoverageId = "TLO", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });
               
                    }
                }
                #endregion
                bool isProductSupported = true;
                CalculatedPremiItems = MobileRepository.CalculateBasicPremi(CoverageList, ProductCode, vcitycode, vusagecode, vBrand, vtype, vModel, vIsNew, vsitting, vyear, PeriodFrom, PeriodTo, Ndays, vTSInterest, vPrimarySI, vInterestId, OldPolicyNo, out RenDiscountPct, out isProductSupported);
                if (!isProductSupported)
                {
                    return Json(new
              {
                  status = true,
                  message = "Product Is Not Supported!"
              });

                }
                #endregion

                #region recalculate extended
                DetailScoring ds = new DetailScoring();
                if (!string.IsNullOrEmpty(vcitycode))
                {
                    ds.FactorCode = "GEOGRA";
                    ds.InsuranceCode = vcitycode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(vusagecode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "MVUSAG";
                    ds.InsuranceCode = vusagecode;
                    dtlScoring.Add(ds);
                }
                ds = new DetailScoring();
                ds.FactorCode = "VHCTYP";
                ds.InsuranceCode = string.IsNullOrEmpty(vtype) ? "ALL   " : vtype;
                dtlScoring.Add(ds);
                if (!string.IsNullOrEmpty(vBrand))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "VBRAND";
                    ds.InsuranceCode = vBrand;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(vIsNew))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "NEWVHC";
                    ds.InsuranceCode = vIsNew;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(vModel))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "VMODEL";
                    ds.InsuranceCode = vModel;
                    dtlScoring.Add(ds);
                }

                if (!string.IsNullOrEmpty(OldPolicyNo)) { 
                 List<CalculatedPremi> recalcpremi = db.Fetch<CalculatedPremi>(@";SELECT a.InterestID,b.coverageid,case when coverageid in('TRS','TRRTLO') then osmv.SumInsured else b.SumInsured end SumInsured,b.begindate AS PeriodFrom,b.enddate AS PeriodTo FROM OrderSimulation os inner join orderSimulationinterest a on a.orderno=os.orderno 
									 inner join orderSimulationcoverage b on a.orderno=b.orderno and a.interestno=b.interestno and b.RowStatus=1  and b.orderno=os.orderno
									 inner join OrderSimulationMV osmv on osmv.orderno=os.orderno 
                                     where a.RowStatus=1 AND os.oldpolicyno=@0 and (a.interestid='CASCO' AND b.coverageid in('TRS','TRRTLO') OR a.InterestID in('TPLPER','PADRVR','PAPASS')OR (a.InterestID='ACCESS' AND b.coverageid in('ALLRIK','TLO') AND b.BeginDate=CONVERT(char,YEAR(os.PeriodFrom))+'-'+CONVERT(char,MONTH(os.PeriodFrom))+'-'+CONVERT(char,DAY(os.PeriodFrom))+' 00:00:00.000' ))order by a.interestid desc", OldPolicyNo);
          
          
                foreach (CalculatedPremi item in recalcpremi)
                {
                    if (item.InterestID.Equals("ACCESS"))
                    {
                        List<CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(JsonConvert.SerializeObject(CalculatedPremiItems));
                        List<CalculatedPremi> cItems = MobileRepository.CalculateACCESS(vTSInterest, Convert.ToDecimal(item.SumInsured), ProductCode, PeriodFrom, PeriodTo, "ACCESS", vtype, vyear, vsitting, pQuotationNo, calculateaccess, dtlScoring, OldPolicyNo, out RenDiscountPct,out DiscountPct);
                        CalculatedPremiItems.AddRange(cItems);

                    }
                }
                }
                #endregion
                #region calculate final item

                RefreshPremi();
                SumInsured = Convert.ToDouble(vTSInterest);
                CalculateLoading();

                decimal NoClaimBonus = MobileRepository.CalculateNCB(GrossPremi, RenDiscountPct);
                decimal DiscountPremi = MobileRepository.CalculateDiscountPremi(GrossPremi, DiscountPct);
                CalculateTotalPremi();
                  
                #endregion

                return Json(new
                {
                    status = true,
                    message = "OK",
                    ProductCode,
                    CalculatedPremiItems,
                    //coveragePeriodItems,
                    //coveragePeriodNonBasicItems,
                    IsTPLEnabled,
                    IsTPLChecked,
                    IsTPLSIEnabled,
                    IsSRCCChecked,
                    IsSRCCEnabled,
                    IsFLDChecked,
                    IsFLDEnabled,
                    IsETVChecked,
                    IsETVEnabled,
                    IsTSChecked,
                    IsTSEnabled,
                    IsPADRVRChecked,
                    IsPADRVREnabled,
                    IsPADRVRSIEnabled,
                    IsPASSEnabled,
                    IsPAPASSSIEnabled,
                    IsPAPASSEnabled,
                    IsPAPASSChecked,
                    IsACCESSChecked,
                    IsACCESSEnabled,
                    IsACCESSSIEnabled,
                    SRCCPremi,
                    FLDPremi,
                    ETVPremi,
                    TSPremi,
                    PADRVRPremi,
                    PAPASSPremi,
                    TPLPremi,
                    ACCESSPremi,
                    AdminFee,
                    TotalPremi,
                    GrossPremi,
                    NoClaimBonus,
                    DiscountPremi
                    //Alert
                }, Util.jsonSerializerSetting());
                }
                else
                {
                    return Json(new { status = false, message = "Product Has Not Mapping Segment", data = "null" });
            
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        private void InitializeEnableCoverage()
        {
            try
            {
                #region initialize enable cover

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string query = @"SELECT * FROM Prd_Interest_Coverage WHERE Product_Code=@0";
                prdInterestCoverage = db.Fetch<Prd_Interest_Coverage>(query, ProductCode);
                foreach (Prd_Interest_Coverage prd in prdInterestCoverage)
                {
                    if (prd.Coverage_Id.Trim().Equals("SRCC") ||
                            prd.Coverage_Id.Trim().Equals("SRCTLO") ||
                            prd.Coverage_Id.Trim().Equals("SRCCTS"))
                    {
                        IsSRCCEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Equals("FLD") ||
                          prd.Coverage_Id.Trim().Equals("FLDTLO"))
                    {
                        IsFLDEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Equals("ETV") ||
                          prd.Coverage_Id.Trim().Equals("ETVTLO") ||
                            prd.Coverage_Id.Trim().Equals("EQK"))
                    {
                        IsETVEnabled = true;
                    }
                    if (prd.Coverage_Id.Trim().Equals("TRS") ||
                         prd.Coverage_Id.Trim().Equals("TRRTLO"))
                    {
                        IsTSEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Contains("MVTP"))
                    {
                        IsTPLEnabled = true;
                    }
                    else if (prd.Interest_ID.Trim().Equals("PADRVR") || prd.Interest_ID.Trim().Equals("PADDR1"))
                    {
                        IsPADRVREnabled = true;
                    }
                    else if (prd.Interest_ID.Equals("PAPASS") || prd.Interest_ID.Equals("PA24AV"))
                    {
                        IsPAPASSEnabled = true;
                    }
                }
                IsACCESSEnabled = true;

                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        [HttpPost]
        public IHttpActionResult PremiumCalculation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + "Function Begin: ");
            try
            {
                #region declare param
                List<decimal> RenDiscountPct = new List<decimal>();
                List<decimal> DiscountPct = new List<decimal>();
                string OrderNo = form.Get("OrderNo");
                ProductCode = form.Get("vProductCode");
                decimal vTSInterest = Convert.ToDecimal(form.Get("vTSInterest"));
                #region load existing cover
                if (!string.IsNullOrEmpty(OrderNo))
                {
                    InitializeCoverageItems(OrderNo, out RenDiscountPct, out DiscountPct);
                }
                else
                {
                #endregion
                string chItem = form.Get("chItem");
                string vPeriodFrom = form.Get("PeriodFrom");
                string vPeriodTo = form.Get("PeriodTo");
                vPeriodFrom = vPeriodFrom.Equals("null") ? null : vPeriodFrom;
                vPeriodTo = vPeriodTo.Equals("null") ? null : vPeriodTo;
                CoverageTypeNonBasic cType = chItem.Equals("SRCC") ? CoverageTypeNonBasic.SRCC : (chItem.Equals("FLD") ? CoverageTypeNonBasic.FLD : (chItem.Equals("ETV")) ? CoverageTypeNonBasic.ETV : (chItem.Equals("TS")) ? CoverageTypeNonBasic.TS : (chItem.Equals("TPLPER")) ? CoverageTypeNonBasic.TPLPER : (chItem.Equals("PADRVR")) ? CoverageTypeNonBasic.PADRVR : (chItem.Equals("PAPASS")) ? CoverageTypeNonBasic.PAPASS : CoverageTypeNonBasic.ACCESS);
                string vtype = form.Get("vtype");
                string vcitycode = form.Get("vcitycode");
                string vusagecode = form.Get("vusagecode");
                string vyear = form.Get("vyear");
                vyear = string.IsNullOrEmpty(vyear) ? "0" : vyear;
                string vsitting = form.Get("vsitting");
                string vMouID = ProductCode;//form.Get("vMouID");
                string vCoverageId = form.Get("vCoverageId");
                string pBasicCoverageId = vCoverageId;
                string TPLCoverageId = form.Get("TPLCoverageId");
                string vInterestId = (cType.Equals(CoverageTypeNonBasic.ACCESS) || cType.Equals(CoverageTypeNonBasic.TPLPER) || cType.Equals(CoverageTypeNonBasic.PADRVR) || cType.Equals(CoverageTypeNonBasic.PAPASS)) ? cType.ToString() : "CASCO";
                string vModel = form.Get("vModel");
                string vBrand = form.Get("vBrand");
                string vIsNew = form.Get("isNew");
                string pQuotationNo = form.Get("pQuotationNo");
                string Ndays = form.Get("Ndays");
                int ComprePeriod = Convert.ToInt16(form.Get("ComprePeriod"));
                int TLOPeriod = Convert.ToInt16(form.Get("TLOPeriod"));
                string OldPolicyNo = form.Get("OldPolicyNo");
                decimal vPrimarySI = Convert.ToDecimal(form.Get("vPrimarySI"));
                 
                    DateTime PeriodFrom = string.IsNullOrEmpty(vPeriodFrom) ? DateTime.MinValue : Convert.ToDateTime(vPeriodFrom);
                    DateTime PeriodTo = string.IsNullOrEmpty(vPeriodTo) ? DateTime.MinValue : Convert.ToDateTime(vPeriodTo);
                    DateTime PeriodFromCover = PeriodFrom;
                    DateTime PeriodToCover = PeriodTo;
                    string calculateditems = form.Get("CalculatedPremiItems");
                    bool isBasicCover = false;
                    if (string.IsNullOrEmpty(Ndays) || string.IsNullOrEmpty(vCoverageId) || string.IsNullOrEmpty(ProductCode) || string.IsNullOrEmpty(vusagecode) || string.IsNullOrEmpty(vcitycode)
                        || string.IsNullOrEmpty(form.Get("vTSInterest")) || string.IsNullOrEmpty(form.Get("vPrimarySI")))
                    {
                        return Json(new
                        {
                            status = true,
                            message = "Param Not Completed"
                        });
                    }
                    if (!string.IsNullOrEmpty(calculateditems))
                    {
                        decimal primarySI = 0;
                        CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculateditems);//new List<CalculatedPremi>();

                        #region remove current cover
                        List<CalculatedPremi> removeitem = new List<CalculatedPremi>();
                        if ((new List<string>() { "SRCC", "FLD", "ETV" }.Contains(cType.ToString())))
                        {
                            CalculatedPremiItems.RemoveAll(x => ((x.InterestID.Equals("CASCO") || x.InterestID.Equals("ACCESS"))&&(!x.CoverageID.TrimEnd().Equals("ALLRIK") && !x.CoverageID.TrimEnd().Equals("TLO"))));

                        }
                        foreach (CalculatedPremi item in CalculatedPremiItems)
                        {
                            if ((item.InterestID.Equals(cType.ToString())) || (cType.Equals(CoverageTypeNonBasic.TS) && (item.CoverageID.TrimEnd().Equals("TRS") || item.CoverageID.TrimEnd().Equals("TRRTLO"))))
                            {
                                removeitem.Add(item);
                            }
                            //get periodfrom periodto cover
                            if (item.CoverageID.Equals("ALLRIK") || item.CoverageID.Equals("TLO"))
                            {
                                if (item.PeriodFrom < PeriodFrom)
                                {
                                    PeriodFrom = item.PeriodFrom;
                                }
                                if (item.PeriodTo > PeriodTo)
                                {
                                    PeriodTo = item.PeriodTo;
                                }
                            }

                            if (item.InterestID.Equals("ACCESS") && (item.CoverageID.Equals("ALLRIK") || item.CoverageID.Equals("TLO")))
                            {
                                if (AccessSI < item.SumInsured)
                                {
                                    AccessSI = item.SumInsured;
                                }

                            }


                        }
                        foreach (CalculatedPremi remove in removeitem)
                        {
                            CalculatedPremiItems.Remove(remove);

                        }

                        #endregion
                    }
                    #region set vehicleyear to year now if not choose vehicle
                 
                        if (Convert.ToInt16(vyear) == 0)
                        {
                            vyear = Convert.ToString(DateTime.Now.Year);
                        }
                    #endregion
     
                #endregion 
                    
                #region Check if Basic Cover Has Applied
                    if (string.IsNullOrEmpty(OrderNo)) { 
                    bool isBasicCoverApplied = true;
                    isBasicCoverApplied = CalculatedPremiItems.Where(x => new List<string>() { "CASCO" }.Contains(x.InterestID.TrimEnd())).Count() == 0 ? false : true;
                    
                    if (!isBasicCoverApplied)
                    {
                        return Json(new
                        {
                            status = true,
                            message = "Please Apply Basic Cover first!"
                        });
                    }
                    }
                    #endregion
                #region Check TS Able To Checked
                    bool isableTS = true;
                    if (chItem.Equals("TS"))
                    {
                        isableTS = CalculatedPremiItems.Where(x => new List<string>() { "SRCC", "FLD", "ETV", "SRCTLO", "FLDTLO", "ETVTLO" }.Contains(x.CoverageID.TrimEnd())).Count() == 0 ? false : true;
                    }
                    if (!isableTS)
                    {
                        return Json(new
                        {
                            status = true,
                            message = "Please Apply SRCC/FLD/ETV first!"
                        });
                    }
                    #endregion
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                if (!vInterestId.Equals("CASCO") && !vInterestId.Equals("ACCESS"))
                {// HANDLE TPL AND PRODUCT LEXUS
                    vInterestId = MobileRepository.GetSpecialInterestId(ProductCode, cType, vInterestId);
                    vCoverageId = MobileRepository.GetSpecialCoverageId(ProductCode, cType, vInterestId, TPLCoverageId);
                }
                else if (cType.Equals(CoverageTypeNonBasic.SRCC) || cType.Equals(CoverageTypeNonBasic.FLD) || cType.Equals(CoverageTypeNonBasic.ETV))
                {
                    string coverageiep = MobileRepository.isProductIEP(ProductCode);
                    vCoverageId = vCoverageId.Equals("TLO") ? cType.ToString() + vCoverageId : cType.ToString();
                    vCoverageId = string.IsNullOrEmpty(coverageiep) ? vCoverageId : coverageiep;

                }
                else if (!vInterestId.Equals("ACCESS"))
                {
                    vCoverageId = cType.Equals(CoverageTypeNonBasic.TS) ? vCoverageId.Equals("TLO") ? "TRRTLO" : "TRS" : cType.ToString();
                }

                #region Get rate scoring
                    List<DetailScoring> dtlScoring = new List<DetailScoring>();//db.Fetch<DetailScoring>("SELECT drf.factor_code AS FactorCode FROM Prd_Interest_Coverage pic INNER JOIN Dtl_Rate_Factor drf ON drf.Coverage_id=@1 AND drf.Score_Code=pic.Scoring_Code  where pic.Product_Code =@0 AND pic.Coverage_Id=@1 AND pic.Interest_Id=@2", vProductCode, vCoverageId, vInterestId);
                    //foreach (DetailScoring item in dtlScoring)
                    //{
                    //    if (item.FactorCode.Equals("GEOGRA"))
                    //    {
                    //        item.InsuranceCode = vcitycode;
                    //    }
                    //    else if (item.FactorCode.Equals("MVUSAG"))
                    //    {
                    //        item.InsuranceCode = vusagecode;
                    //    }
                    //    else if (item.FactorCode.Equals("VHCTYP"))
                    //    {
                    //        item.InsuranceCode = vtype;
                    //        if (string.IsNullOrEmpty(vtype))
                    //        {
                    //            item.InsuranceCode = "ALL   ";
                    //        }
                    //    }

                    //}
                    DetailScoring ds = new DetailScoring();
                    if (!string.IsNullOrEmpty(vcitycode))
                    {
                        ds.FactorCode = "GEOGRA";
                        ds.InsuranceCode = vcitycode;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vusagecode))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "MVUSAG";
                        ds.InsuranceCode = vusagecode;
                        dtlScoring.Add(ds);
                    }
                    ds = new DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    ds.InsuranceCode = string.IsNullOrEmpty(vtype) ? "ALL" : vtype;
                    dtlScoring.Add(ds);
                    if (!string.IsNullOrEmpty(vBrand))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "VBRAND";
                        ds.InsuranceCode = vBrand;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vIsNew))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "NEWVHC";
                        ds.InsuranceCode = vIsNew;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vModel))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "VMODEL";
                        ds.InsuranceCode = vModel;
                        dtlScoring.Add(ds);
                    }
                    #endregion
                if (cType.Equals(CoverageTypeNonBasic.ACCESS) && PeriodFrom != DateTime.MinValue)
                {
                    List<CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculateditems);
                    List<CalculatedPremi> cItems = MobileRepository.CalculateACCESS(vPrimarySI, vTSInterest, ProductCode, PeriodFrom, PeriodTo, vInterestId, vtype, vyear, vsitting, pQuotationNo, calculateaccess, dtlScoring, OldPolicyNo, out RenDiscountPct, out DiscountPct);
                    CalculatedPremiItems.AddRange(cItems);
                }
                #region handle uncheck item
                    else if (PeriodFrom == DateTime.MinValue)
                    {
                        var RenDiscount = RetailRepository.GetRenewalDiscount(OldPolicyNo);
                        foreach (WTCommission ren in RenDiscount)
                        {
                            if (ren.CommissionID.Contains("RD"))
                            {
                                RenDiscountPct.Add(ren.Percentage);
                            }
                        }
                        if (!(new List<string>() { "SRCC", "FLD", "TS", "ETV" }.Contains(cType.ToString())) && CalculatedPremiItems.Select(x => x.InterestID.Equals(vInterestId)).Count() > 0)
                        {
                            CalculatedPremiItems.RemoveAll(x => x.InterestID.Trim().Equals(vInterestId));
                            //handle PAPASS when uncheck PADRVR
                            if (cType.Equals(CoverageTypeNonBasic.PADRVR) && CalculatedPremiItems.Select(x => x.InterestID.Trim()).Contains(CoverageTypeNonBasic.PAPASS.ToString()))
                            {
                                CalculatedPremiItems.RemoveAll(x => x.InterestID.Contains(CoverageTypeNonBasic.PAPASS.ToString()));
                            }
                        }
                        else if (cType.Equals(CoverageTypeNonBasic.TS) && pBasicCoverageId.Equals("ALLRIK,TLO"))
                        {
                            CalculatedPremiItems.RemoveAll(x => x.InterestID.TrimEnd().Equals("CASCO")&&(x.CoverageID.TrimEnd().Equals("TRS") || x.CoverageID.TrimEnd().Equals("TRRTLO")));
                        }
                        else
                        {
                            var PRDType = RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: "CASCO", CoverageID: pBasicCoverageId);
                            string ConfigName = "EXTENDED_COVER_GROUP_COMPRE";
                            if (PRDType[0].TLOFlag != null)
                            {
                                if (PRDType[0].TLOFlag)
                                {
                                    ConfigName = "EXTENDED_COVER_GROUP_TLO";
                                }
                            }
                            List<string> CoverGrp = RetailRepository.GetGeneralConfig(ConfigName, "PRODUCT", ProductCode, vCoverageId);
                            if (CoverGrp.Count == 0)
                                CoverGrp.Add(vCoverageId);

                            //handle FLD, SRCC, ETV
                            foreach (var c in CoverGrp)
                            {
                                CalculatedPremiItems.RemoveAll(x => c.Equals(x.CoverageID.Trim()));
                            }

                        }

                    }
                    #endregion
                else
                {
                    List<CoverageParam> cpList = new List<CoverageParam>();
                    if (TLOPeriod > 0 && ComprePeriod > 0 && (new List<string>() { "SRCC", "FLD", "TS", "ETV" }.Contains(cType.ToString())))
                    {// HANDLING KOMBINASI BASIC COVER
                        //Handling TS Apply like Bundling
                        if (cType.Equals(CoverageTypeNonBasic.TS))
                        {
                            foreach (CalculatedPremi item in CalculatedPremiItems)
                            {
                                if (item.InterestID.Equals("CASCO")&&item.CoverageID.Contains("SRC"))
                                {
                                    cpList.Add(new CoverageParam { CoverageId = (item.CoverageID.Contains("TLO") ? "TRRTLO" : vCoverageId), PeriodFromCover = item.PeriodFrom, PeriodToCover = item.PeriodTo });

                                }
                            }
                
                        }
                    }
                    else
                    {
                        cpList.Add(new CoverageParam { CoverageId = vCoverageId, PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });
                    }

                    foreach (CoverageParam cp in cpList)
                    {

                        int calcmethod = PeriodTo.Date < PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                        CalculatedPremiItems = MobileRepository.calculatepremicalculation(ProductCode, vInterestId, cp, PeriodFrom, PeriodTo, vtype, vyear
                                                                                         , vsitting, dtlScoring, Ndays, vTSInterest, vMouID, OldPolicyNo, CalculatedPremiItems, calcmethod, out RenDiscountPct, out DiscountPct);
                    }
                }
                #region recalculate access if srcc ts applied lastly
                    if (new List<string>() { "SRCC", "FLD", "TS", "ETV" }.Contains(cType.ToString()) && PeriodFrom != DateTime.MinValue)
                    {
                        CalculatedPremiItems = MobileRepository.RecalculateAccess(vTSInterest, vCoverageId, CalculatedPremiItems, ProductCode, PeriodFrom, PeriodTo,
                                                           vtype, vyear, vsitting, pQuotationNo, dtlScoring, OldPolicyNo, out RenDiscountPct, out  DiscountPct);
                    }
                    #endregion
                }
                #region calculate final item

                InitializeEnableCoverage();
                SumInsured = Convert.ToDouble(vTSInterest);
                RefreshPremi();
                getAdminFee();
                CalculateLoading();
                CalculateTotalPremi();
                decimal NoClaimBonus = MobileRepository.CalculateNCB(GrossPremi, RenDiscountPct);
                decimal DiscountPremi = MobileRepository.CalculateDiscountPremi(GrossPremi, DiscountPct);
                #endregion
                if (CalculatedPremiItems != null)
                {
                    _log.Debug(actionName + "Function OK! ");
                    return Json(new
                    {
                        status = true,
                        message = "OK",
                        ProductCode,
                        CalculatedPremiItems,
                        //coveragePeriodItems,
                        //coveragePeriodNonBasicItems,
                        IsTPLEnabled,
                        IsTPLChecked,
                        IsTPLSIEnabled,
                        IsSRCCChecked,
                        IsSRCCEnabled,
                        IsFLDChecked,
                        IsFLDEnabled,
                        IsETVChecked,
                        IsETVEnabled,
                        IsTSChecked,
                        IsTSEnabled,
                        IsPADRVRChecked,
                        IsPADRVREnabled,
                        IsPADRVRSIEnabled,
                        IsPASSEnabled,
                        IsPAPASSSIEnabled,
                        IsPAPASSEnabled,
                        IsPAPASSChecked,
                        IsACCESSChecked,
                        IsACCESSEnabled,
                        IsACCESSSIEnabled,
                        SRCCPremi,
                        FLDPremi,
                        ETVPremi,
                        TSPremi,
                        PADRVRPremi,
                        PAPASSPremi,
                        TPLPremi,
                        ACCESSPremi,
                        AdminFee,
                        TotalPremi,
                        GrossPremi,
                        NoClaimBonus,
                        DiscountPremi
                        //Alert
                    }, Util.jsonSerializerSetting());
                }
                _log.Error(actionName + "Something Error!");
                return Json(new { status = false, message = "Error", data = "Something Error!" });
            }
            catch (Exception e)
            {
                _log.Error(actionName + "Function Error : " + e.ToString());
                return Json(new { status = false, message = "Error", data = e.ToString() });
            }
        }

        private void CalculatePremiPerCoverage(string InterestID, string CoverageID, double Premium)
        {
            if (InterestID.Equals("TPLPER") && CoverageID.Contains("MVTP"))
            {
                IsTPLEnabled = true;
                TPLPremi += Premium;
                if (Premium > 0)
                {
                    IsTPLChecked = true;
                }
            }
            if (CoverageID.TrimEnd().Equals("SRCC") || CoverageID.TrimEnd().Equals("SRCTLO"))
            {
                IsSRCCEnabled = true;
                SRCCPremi += Premium;
                if (Premium > 0)
                {
                    IsSRCCChecked = true;
                }
            }
            if (CoverageID.TrimEnd().Equals("FLD") || CoverageID.TrimEnd().Equals("FLDTLO"))
            {
                IsFLDEnabled = true;
                FLDPremi += Premium;
                if (Premium > 0)
                {
                    IsFLDChecked = true;
                }
            }
            if (CoverageID.TrimEnd().Equals("ETV") || CoverageID.TrimEnd().Equals("ETVTLO"))
            {
                IsETVEnabled = true;
                ETVPremi += Premium;
                if (Premium > 0)
                {
                    IsETVChecked = true;
                }
            }
            if (CoverageID.TrimEnd().Equals("TRS   ") || CoverageID.TrimEnd().Equals("TRRTLO"))
            {
                TSPremi += Premium;
                IsTSEnabled = true;
                if (Premium > 0)
                {
                    IsTSChecked = true;
                }
            }
            if (CoverageID.Trim().Contains("PAD"))
            {
                PADRVRPremi += Premium;
                IsPADRVREnabled = true;
                IsPADRVRSIEnabled = true;
                if (Premium > 0)
                {
                    IsPADRVRChecked = true;
                }
            }
            if (CoverageID.Trim().Contains("PAP"))
            {
                PAPASSPremi += Premium;
                IsPAPASSEnabled = true;
                IsPAPASSSIEnabled = true;
                if (Premium > 0)
                {
                    IsPAPASSChecked = true;
                }
            }
            if (InterestID.ToUpper().Equals("ACCESS") &&
                       (CoverageID.ToUpper().Equals("ALLRIK") || CoverageID.ToUpper().TrimEnd().Equals("TLO")))
            {
                IsACCESSChecked = true;
                IsACCESSEnabled = true;
                IsACCESSSIEnabled = true;
                ACCESSPremi += Premium;
            }

        }

        private void RecallingCoverageNonBasic()
        {
            foreach (CoveragePeriodNonBasic item in coveragePeriodNonBasicItems)
            {
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.SRCC)
                {
                    IsSRCCChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.FLD)
                {
                    IsFLDChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.ETV)
                {
                    IsETVChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TS)
                {
                    IsTSChecked = true;
                    IsTSEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.TPLPER)
                {
                    IsTPLChecked = true;
                    IsTPLSIEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PADRVR)
                {
                    IsPADRVRChecked = true;
                    IsPADRVRSIEnabled = true;
                }
                if (item.CoverageTypeNonBasic == CoverageTypeNonBasic.PAPASS)
                {
                    IsPAPASSChecked = true;
                    IsPAPASSSIEnabled = true;
                    IsPASSEnabled = true;
                }
            }
        }
        
        [HttpPost]
        public IHttpActionResult getWorkshopList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string search = "%" + form.Get("search").Trim() + "%";

                string query = @"
                 SELECT * FROM Workshop WHERE WorkshopName 
                 like @0 OR City like @0 Order by WorkshopName ASC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, search);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        [HttpPost]
        public IHttpActionResult getvehicleprice(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string vehiclecode = form.Get("vehiclecode").Trim();
                string Year = form.Get("Year").Trim();
                string citycode = form.Get("citycode").Trim();
                if (string.IsNullOrEmpty(vehiclecode) || string.IsNullOrEmpty(citycode) || string.IsNullOrEmpty(Year))
                {
                    return Json(new { status = true, message = "Parameter not completed" }, Util.jsonSerializerSetting());
               
                }
                string query = @"
							  select top 1 Price from [dbo].[Mst_Vehicle_Price] where vehicle_code=@0 and year=@1 and geo_area_id=@2 order by effective_date desc
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    var result = db.ExecuteScalar<double>(query, vehiclecode,Year,citycode);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult getTaskListold(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                Int16 currentPage = Convert.ToInt16(form.Get("currentPage"));
                Int16 pageSize =  Convert.ToInt16(form.Get("pageSize"));
                Int16 currentPageLead = currentPage; // Convert.ToInt16(form.Get("currentPageLead"));
                Int16 pageSizeLead = pageSize;// Convert.ToInt16(form.Get("pageSizeLead"));
                bool chIsSearchByChasNo = bool.Parse(form.Get("chIsSearchByChasNo").Trim());
                bool chIsSearchByEngNo = bool.Parse(form.Get("chIsSearchByEngNo").Trim());
                bool chIsSearchByPolicyNo = bool.Parse(form.Get("chIsSearchByPolicyNo").Trim());
                bool chIsSearchByProsName = bool.Parse(form.Get("chIsSearchByProsName").Trim());
                string salesofficerid = form.Get("salesofficerid").Trim();
                string search = form.Get("search").Trim();
                string indexOrderByItem = form.Get("indexOrderByItem").Trim();
                int indexOrderByTypeItem = Int32.Parse(form.Get("indexOrderByTypeItem").Trim());
                bool chFollowUpMonthlyChecked = bool.Parse(form.Get("chFollowUpMonthlyChecked").Trim());
                bool chFollowUpDateChecked = bool.Parse(form.Get("chFollowUpDateChecked").Trim());
                bool chNeedFUChecked = bool.Parse(form.Get("chNeedFUChecked").Trim());
                bool chPotentialChecked = bool.Parse(form.Get("chPotentialChecked").Trim());
                bool chCallLaterChecked = bool.Parse(form.Get("chCallLaterChecked").Trim());
                bool chCollectDocChecked = bool.Parse(form.Get("chCollectDocChecked").Trim());
                bool chNotDealChecked = bool.Parse(form.Get("chNotDealChecked").Trim());
                bool chSentToSAChecked = bool.Parse(form.Get("chSentToSAChecked").Trim());
                bool chBackToAOChecked = bool.Parse(form.Get("chBackToAOChecked").Trim());
                bool chPolicyCreatedChecked = bool.Parse(form.Get("chPolicyCreatedChecked").Trim());
                bool chOrderRejectedChecked = bool.Parse(form.Get("chOrderRejectedChecked").Trim());
                bool chNewChecked = bool.Parse(form.Get("chNewChecked").Trim());
                bool chRenewableChecked = bool.Parse(form.Get("chRenewableChecked").Trim());
                string dateFollowUp = form.Get("getFollowUpDate");
                DateTime getFollowUpDate = DateTime.Now;
                if (!string.IsNullOrEmpty(dateFollowUp))
                {
                    getFollowUpDate = Convert.ToDateTime(dateFollowUp);
                }

                if (salesofficerid == null || salesofficerid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string queryRenewalNonRenNot = @"select  1 AS IsRenewalNonRenNot,'Renewable' AS TaskStatus, 'Need FU' AS FollowUpDesc, 'Data baru diinput' AS FollowUpReason,
                '18 October 2019' AS ExpiredDate,''CustID,'' FollowUpNo,NULL AS LastFollowUpDate, NULL AS NextFollowUpDate, 0 AS LastSeqNo, ERP.policy_no AS PolicyNo,mc.Name AS ProspectName
				,p.EntryDt AS EntryDate from [BeyondReport].[AAB].[dbo].[Excluded_Renewal_Policy] ERP 
                INNER JOIN [BeyondReport].[AAB].[dbo].Policy p ON p.policy_no = ERP.policy_no 
                INNER JOIN [BeyondReport].[AAB].[dbo].mst_customer mc ON mc.cust_id = p.policy_holder_code                                 ";

                string query = @"SELECT 0 AS IsRenewalNonRenNot,CASE WHEN IsRenewal=1 THEN 'Renewable' ELSE 'New' END AS TaskStatus,b.Description AS FollowUpDesc,c.Description AS FollowUpReason,FORMAT(DATEADD(DAY,1,a.lastfollowupdate),'dd MMMM yyyy') AS ExpiredDate, a.CustID,a.FollowUpNo,a.LastFollowUpDate,a.NextFollowUpDate,a.LastSeqNo,(SELECT TOP 1 CASE WHEN a.FollowUpStatus=13 OR a.IsRenewal=1 THEN PolicyNo ELSE '' END FROM OrderSimulation WHERE FollowUpNo=a.FollowUpNo)AS PolicyNo,case when pc.isCompany=1 then pco.CompanyName else a.ProspectName end as ProspectName,a.EntryDate FROM FollowUp a INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode LEFT JOIN FollowUpStatusInfo c ON a.FollowUpInfo=c.InfoCode LEFT JOIN ProspectCustomer pc ON pc.Custid=a.custid  LEFT JOIN ProspectCompany pco ON pco.Custid=a.custid 
                                 WHERE  ";
                query = string.Concat(query, string.IsNullOrEmpty(search) ? " a.SalesOfficerID=@0 AND " : "");

                if (chFollowUpDateChecked)
                {
                    query = string.Concat(query, " (NextFollowUpdate <=@2  OR NextFollowUpDate IS NULL) ");
                }
                else if (chFollowUpMonthlyChecked)
                {
                    string querytemp = @" ((MONTH(a.NextFollowUpdate) = MONTH(GETDATE()) 
                                            AND YEAR(a.NextFollowUpdate) = YEAR(GETDATE())) 
                                            OR a.NextFollowUpdate IS NULL) ";
                    query = string.Concat(query, querytemp);
                }


                List<string> list = new List<string>();

                if (chNeedFUChecked)
                {
                    list.Add("FollowUpStatus=1 ");
                }
                if (chPotentialChecked)
                {
                    list.Add("FollowUpStatus=2 ");
                }
                if (chCallLaterChecked)
                {
                    list.Add("FollowUpStatus=3 ");
                }
                if (chCollectDocChecked)
                {
                    list.Add("FollowUpInfo in(56) ");
                }
                if (chNotDealChecked)
                {
                    list.Add("FollowUpStatus=5 ");
                }
                if (chSentToSAChecked)
                {
                    list.Add("FollowUpInfo=61 ");
                }
                if (chPolicyCreatedChecked)
                {
                    list.Add("FollowUpInfo IN (50,51,52,53,54,55) ");
                }
                if (chBackToAOChecked)
                {
                    list.Add("FollowUpInfo = 60 ");
                }
                if (chOrderRejectedChecked)
                {
                    list.Add("FollowUpStatus = 14 ");
                }
             

                if (list.Count == 1)
                {
                    query = string.Concat(query, "AND " + list[0]);
                }
                else if (list.Count > 1)
                {

                    query = string.Concat(query, " AND ( ");

                    for (int i = 0; i < list.Count; i++)
                    {
                        query = string.Concat(query, list[i]);
                        if (list.Count != i + 1)
                        {
                            query = string.Concat(query, " OR ");
                        }
                    }
                    query = string.Concat(query, ") ");
                }
                if (chNewChecked)
                {
                    query = string.Concat(query, " AND (isRenewal=0 OR isRenewal IS NULL " + (chRenewableChecked ? "" : ")"));
                }
                if (chRenewableChecked)
                {
                    query = chNewChecked ? string.Concat(query, " OR ") : string.Concat(query, " AND (");
                    query = string.Concat(query, "isRenewal=1 )");
                }
                if (!chNewChecked && !chRenewableChecked)
                {
                    query = string.Concat(query, "AND (isRenewal != 1 AND isRenewal !=0 AND isRenewal IS NOT NULL)");
                }
           
                if (!string.IsNullOrEmpty(search))
                {
                    queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " WHERE ");
                    if (chIsSearchByChasNo)
                    {
                        query = string.Concat(query, " AND a.FollowUpNo IN(SELECT FollowUpNo FROM OrderSimulation os INNER JOIN OrderSimulationMV osmv ON osmv.OrderNo=os.OrderNo WHERE osmv.ChasisNumber = @1 ) ");
                        queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " Chasis_Number = @1 ");
                    
                    }
                    else if (chIsSearchByEngNo)
                    {
                        query = string.Concat(query, " AND a.FollowUpNo IN(SELECT FollowUpNo FROM OrderSimulation os INNER JOIN OrderSimulationMV osmv ON osmv.OrderNo=os.OrderNo WHERE osmv.EngineNumber = @1 ) ");
                        queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " Engine_Number = @1 ");
                    }
                    else if (chIsSearchByPolicyNo)
                    {
                        query = string.Concat(query, " AND  a.FollowUpNo IN(SELECT FollowUpNo FROM OrderSimulation  WHERE PolicyNo = @1 )  ");
                        queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " p.Policy_No = @1 ");
                
                    }
                    else if (chIsSearchByProsName)
                    {

                        search = "%" + search + "%";
                        query = string.Concat(query, " AND (ProspectName like @1 OR a.FollowUpNo in (SELECT FollowUpNo FROM ProspectCompany WHERE CompanyName like @1)) ");
                        queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " mc.Name LIKE @1 ");
                
                    }
                }
                query = string.Concat(query, " AND a.followupno not in(select fu.followupno from ordersimulation os INNER JOIN FollowUp fu ON fu.Followupno=os.followupno AND fu.FollowUpStatus=1 INNER JOIN Quotation.dbo.HistoryPenawaran hp ON hp.OrderNo=os.OrderNo ) ");// Handle Active ProspectLead
                queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " AND erp.RowStatus=0");//Handle Taken RenewalRenNote
                if (string.IsNullOrEmpty(search))
                {
                    queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " AND erp.entrydt<'1995-01-01'");//Handle Not Show RenewalRenNote
                
                }
                //Combine with renewalrennot
              //  query = "SELECT z.* FROM (SELECT x.* FROM(" + query + " AND a.followupno not in(select fu.followupno from ordersimulation os INNER JOIN FollowUp fu ON fu.Followupno=os.followupno AND fu.FollowUpStatus=1 INNER JOIN Quotation.dbo.HistoryPenawaran hp ON hp.OrderNo=os.OrderNo ))x UNION ALL SELECT y.* FROM(" + queryRenewalNonRenNot + " AND erp.RowStatus=0 )y)z";
               // query = string.Concat(query, string.IsNullOrEmpty(search) ? " WHERE IsRenewalNonRenNot=0 " : "");
                query = string.Concat(query, " ORDER BY ");
                queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, " ORDER BY ");

                if (indexOrderByItem.Equals("Status"))
                {
                    query = string.Concat(query, "  b.Description DESC, a.EntryDate DESC ");
                    queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, "   p.EntryDt DESC ");
                }
                if (indexOrderByItem.Equals("Name"))
                {
                    query = string.Concat(query, " a.ProspectName");
                    queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, "mc.Name");
                }
                if (indexOrderByItem.Equals("EntryDate"))
                {
                    query = string.Concat(query, "a.EntryDate");
                    queryRenewalNonRenNot = string.Concat(queryRenewalNonRenNot, "p.EntryDt");
                }
                if (indexOrderByItem.Equals("LastFollowUpdate"))
                {
                    query = string.Concat(query, "a.LastFollowUpdate");
                }
                if (indexOrderByItem.Equals("NextFollowUpdate"))
                {
                    query = string.Concat(query, "a.NextFollowUpdate");
                }


                if (!indexOrderByItem.Equals("Status"))
                {
                    if (indexOrderByTypeItem == 0)
                    {
                        query = string.Concat(query, " ASC");
                    }
                    else
                    {
                        query = string.Concat(query, " DESC");
                    }
                }

                //                string querytempaja = @"
                //Union SELECT a.* FROM FollowUp a 
                //                INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode 
                //                WHERE a.SalesOfficerID=@0 AND a.FollowUpName like @1 ORDER BY EntryDate DESC";
                //                query = string.Concat(query, querytempaja);

                string queryprospectLeadList = @";SELECT p.IsHotProspect, 0 AS IsRenewalNonRenNot,CASE WHEN IsRenewal=1 THEN 'Renewable' ELSE 'New' END AS TaskStatus,b.Description AS FollowUpDesc,c.Description AS FollowUpReason,hp.ExpiredDate AS ExpiredDate, a.CustID,a.FollowUpNo,a.LastFollowUpDate,a.NextFollowUpDate,a.LastSeqNo,(SELECT TOP 1 CASE WHEN a.FollowUpStatus=13 OR a.IsRenewal=1 THEN PolicyNo ELSE '' END  FROM OrderSimulation WHERE FollowUpNo=a.FollowUpNo)AS PolicyNo, a.ProspectName,a.EntryDate FROM FollowUp a 
JOIN OrderSimulation os ON os.FollowUpNo = a.FollowUpNo INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode LEFT JOIN FollowUpStatusInfo c ON a.FollowUpInfo=c.InfoCode 
JOIN Quotation.dbo.HistoryPenawaran hp ON hp.OrderNo = os.OrderNo 
INNER JOIN Quotation.dbo.Penawaran p ON p.PolicyId=hp.policyid
WHERE a.SalesOfficerID=@0 AND a.FollowUpStatus=1 AND a.RowStatus = 1
ORDER BY os.EntryDate DESC";



                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    //salesofficerid = salesofficerid.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", salesofficerid) : salesofficerid;

                    //var result = db.Fetch<dynamic>(query, salesofficerid, search, getFollowUpDate);

                    var result = db.Page<dynamic>(currentPage, pageSize, query, salesofficerid, search, getFollowUpDate);
                
                    var prospectLeadListPage = db.Page<dynamic>(currentPageLead,pageSizeLead, queryprospectLeadList, salesofficerid, search);

                    var RenewalNonRenNotList = db.Page<dynamic>(currentPage, pageSize, queryRenewalNonRenNot, salesofficerid, search, getFollowUpDate);

                    var prospectLeadList = db.Fetch<dynamic>( queryprospectLeadList, salesofficerid, search);

                    List<dynamic> historyPenawaranList = new List<dynamic>();
                    List<dynamic> penawaranList = new List<dynamic>();


//                    for (int fi = 0; fi < prospectLeadList.Count; fi++)
//                    {
//                        string queryhistoryPenawaranList = @";SELECT TOP 1 hp.* FROM Quotation.dbo.HistoryPenawaran hp 
//                                                             INNER JOIN OrderSimulation os ON hp.OrderNo = os.OrderNo WHERE os.FollowUpNo = @0 ORDER BY LastUpdatedTime DESC";
//                        historyPenawaranList.Add(db.FirstOrDefault<dynamic>(queryhistoryPenawaranList, prospectLeadList[fi].FollowUpNo));

//                        string querypenawaranList = @";SELECT * FROM Quotation.dbo.Penawaran WHERE policyID = @0";
//                        penawaranList.Add(db.FirstOrDefault<dynamic>(querypenawaranList, historyPenawaranList[fi].PolicyID));
//                    }
                    return Json(new { status = true, message = "OK", data = result, prospectLeadList = prospectLeadListPage,RenewalNonRenNotList=RenewalNonRenNotList, historyPenawaranList = historyPenawaranList, penawaranList = penawaranList }, Util.jsonSerializerSetting());
                    //return Json(new { status = true, message = "OK", data = prospectLeadList.Concat(result), historyPenawaranList=historyPenawaranList, penawaranList = penawaranList }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        [HttpPost]
        public IHttpActionResult getFollowUpStatus(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            List<FollowUpStatus> result = new List<FollowUpStatus>();
            try
            {
                string followupstatus = form.Get("followupstatus").Trim();
                string followupstatusinfo = form.Get("followupstatusinfo").Trim();
                string isRenewal = form.Get("isRenewal").Trim();
                string followupno = form.Get("followupno").Trim();

                if (string.IsNullOrEmpty(followupstatus))
                {
                    return Json(new { status = false, message = "Param is null", data = "No Data" }, Util.jsonSerializerSetting());
                }
                string query = @";exec [usp_GetFollowUpStatusOtosales] @0,@1,@2,@3";



                //if (!string.IsNullOrEmpty(followupstatusactivity))
                //{
                //    if (followupstatusactivity.Equals("6"))
                //    {
                //        query = @";SELECT * FROM FollowUpStatus WHERE StatusCode=6 AND RowStatus = 1 ORDER BY StatusCode";
                //    }
                //}

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    #region check empty next status option
                    string query2 = @";SELECT COUNT(part) from AsuransiAstra.GODigital.Fn_SplitString((SELECT " + (followupstatus.Equals("14") ? "ParamValue" : "ParamValue2") + " FROM ApplicationParameters WHERE ParamName='FUSTATUSEMPTYSTATUSOPTION' AND ParamType='CNF'),',') where part=@0";
                    int exist = db.ExecuteScalar<int>(query2, (followupstatus.Equals("14") ? followupstatus : followupstatusinfo));
                    query = exist == 0 ? query : "";
                    #endregion

                    //  int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM FollowUpStatus WITH(NOLOCK) WHERE LastUpdatedTime = @0", "19000101");

                    if (!string.IsNullOrEmpty(query))
                    {
                        // query = query + " ASC OFFSET @2 ROWS FETCH NEXT @2 ROWS ONLY";
                        result = db.Fetch<FollowUpStatus>(query, followupstatus, followupstatusinfo, isRenewal,followupno); //totalcount, 100);

                    }

                    //#region add action send quotation
                    //query2 = @";SELECT COUNT(part) from AsuransiAstra.GODigital.Fn_SplitString((SELECT " + (followupstatus.Equals("14") ? "ParamValue" : "ParamValue2") + " FROM ApplicationParameters WHERE ParamName='FUSTATUSUNABLESENDQUOTATION' AND ParamType='CNF'),',') where part=@0";
                    //exist = db.ExecuteScalar<int>(query2, (followupstatus.Equals("14") ? followupstatus : followupstatusinfo));

                    //FollowUpStatus fus = new FollowUpStatus();
                    //fus.Description = "SendQuotation";
                    //if (exist == 0)
                    //{
                    //    result.Add(fus);
                    //}
                    //#endregion
                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpHistory(FormDataCollection form)
        {
            try
            {
                string followupno = form.Get("followupno").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  select * from FollowUpHistory where FollowUpNo=@0 and SeqNo > 0 order by SeqNo DESC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult sendToSA(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno");
                string orderno = form.Get("orderno");
                string quotationno = form.Get("quotationno");
                string SalesOfficerID = form.Get("salesofficerid");

                if (string.IsNullOrEmpty(followupno) || string.IsNullOrEmpty(orderno) || string.IsNullOrEmpty(quotationno) || string.IsNullOrEmpty(SalesOfficerID))
                {
                    return Json(new { status = false, message = "Param is null" }, Util.jsonSerializerSetting());
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    //SalesOfficerID = SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", SalesOfficerID) : SalesOfficerID;

                    string query = @"SELECT * FROM OrderSimulation WHERE FollowUpNo =@0";

                    string quotationNoBefore = "";

                    // ClearSendF
                    List<OrderSimulation> OS = db.Fetch<OrderSimulation>(query, followupno);
                    for (var i = 0; i < OS.Count; i++)
                    {
                        OrderSimulation item = OS[i];
                        if (!item.OrderNo.Equals(orderno))
                        {
                            quotationNoBefore = item.QuotationNo;
                            db.Execute("UPDATE OrderSimulation SET ApplyF=0, SendF=0 WHERE OrderNo=@0", item.OrderNo);
                        }
                    }


                    // SendToSA
                    OrderSimulation OSModel = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE OrderNo=@0", orderno);
                    if (OSModel != null)
                    {
                        db.Execute("UPDATE OrderSimulation SET ApplyF=1, SendF=1, SendDate=GETDATE() WHERE OrderNo=@0", OSModel.OrderNo);
                    }

                    // InsertEmailNotification

                    EmailNotification enModel = new EmailNotification();
                    enModel.EmailNotificationNo = System.Guid.NewGuid().ToString();
                    string sqlQuery = @"SELECT COUNT(*) FROM EmailNotification WHERE ParameterValue=@0";

                    try
                    {
                        if (!string.IsNullOrEmpty(quotationNoBefore) && !quotationNoBefore.Equals(quotationno))
                        {
                            enModel.NotificationType = 4;
                            enModel.ParameterName = "FollowUpNo||QuotationNoBefore";
                            enModel.ParameterValue = followupno + "||" + quotationNoBefore;

                        }
                        else
                        {
                            enModel.NotificationType = ((db.ExecuteScalar<int>(sqlQuery, followupno) > 0) ? 2 : 1);
                            enModel.ParameterName = "FollowUpNo";
                            enModel.ParameterValue = followupno;
                        }
                        enModel.EmailSendStatus = 0;
                        enModel.UserID = SalesOfficerID;
                        enModel.RowStatus = true;
                        enModel.LastUpdatedTime = new DateTime();

                        string insertEnModel = @"INSERT INTO [dbo].[EmailNotification]
           ([EmailNotificationNo]
           ,[NotificationType]
           ,[ParameterName]
           ,[ParameterValue]
           ,[EmailSendStatus]
           ,[UserID]
           ,[EntryDate]
           ,[LastUpdatedTime]
           ,[RowStatus])
     VALUES
           (@0,@1,@2,@3,@4,@5,GETDATE(),GETDATE(),1)";

                        db.Execute(insertEnModel, enModel.EmailNotificationNo, enModel.NotificationType, enModel.ParameterName, enModel.ParameterValue, enModel.EmailSendStatus, SalesOfficerID);

                        return Json(new { status = true, message = "Success" }, Util.jsonSerializerSetting());

                    }
                    catch (Exception e)
                    {
                        _log.Error("Function " + actionName + " Error : " + e.ToString());
                        return Json(new { status = false, message = e.Message });
                    }

                }

                return Json(new { status = true, message = "OK" }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        [HttpPost]
        public IHttpActionResult sendToSARenewalNonRenNot(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string PolicyNo = form.Get("PolicyNo");
            string FollowUpNo = form.Get("FollowUpNo");
            string RemarkToSA = form.Get("RemarksToSA");
            string SalesOfficerID = form.Get("SalesOfficerID");
            #endregion

            //Generate GUID 
            string custID = System.Guid.NewGuid().ToString();
            string followUpID = String.IsNullOrEmpty(FollowUpNo) ? System.Guid.NewGuid().ToString() : FollowUpNo;
            string orderID = System.Guid.NewGuid().ToString();

            if (string.IsNullOrEmpty(PolicyNo) || string.IsNullOrEmpty(FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            try
            {
                #region Check Double Insured
                //CheckDoubleInsuredResult cdir = AABRepository.GetCheckDoubleInsuredDirtyData(PolicyNo);
                //if (cdir.IsDoubleInsured)
                //{
                //    return Json(new { status = false, message = "Double Insured", cdir = cdir });
                //}
                #endregion
                MobileRepository.GenerateOrderSimulation(SalesOfficerID, custID, followUpID, orderID, PolicyNo, RemarkToSA);
                
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", orderID });
            }
            catch (Exception e)
            {
                MobileRepository.ClearFailedData(custID, followUpID, orderID);
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
        [HttpPost]
        public IHttpActionResult getProspectCustomer(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string custid = form.Get("custid").Trim();

                if (custid == null || custid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }


                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    #region PROSPECT CUSTOMER
                    string query = @"
                  SELECT TOP 1 * FROM ProspectCustomer WHERE CustID=@0
                ";
                    ProspectCustomer ProspectCustomer = db.FirstOrDefault<ProspectCustomer>(query, custid);
                    #endregion


                    return Json(new { status = true, message = "OK", ProspectCustomer }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult getProspectCustomerEdit(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string custid = form.Get("custid").Trim();

                if (custid == null || custid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }


                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    #region PROSPECT CUSTOMER
                    FollowUp FollowUp = new FollowUp();
                    ProspectCustomer ProspectCustomer = new ProspectCustomer();
                    ProspectCompany ProspectCompany = new ProspectCompany();
                    OrderSimulation OrderSimulation = new OrderSimulation();
                    List<OrderSimulationMV> OrderSimulationMV = new List<OrderSimulationMV>();
                    List<OrderSimulationInterest> OrderSimulationInterest = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> OrderSimulationCoverage = new List<OrderSimulationCoverage>();
                    List<Dealer> Dealer = new List<Dealer>();
                    List<SalesmanDealer> SalesmanDealer = new List<SalesmanDealer>();
                    SurveySchedule SurveySchedule = new SurveySchedule();
                    string query = @"
                  SELECT TOP 1 * FROM ProspectCustomer WHERE CustID=@0
                ";
                    ProspectCustomer = db.FirstOrDefault<ProspectCustomer>(query, custid);
                    #endregion


                    #region FOLLOW UP
                    query = @"
                  SELECT TOP 1 * FROM FollowUp WHERE CustID=@0
                ";
                    FollowUp = db.FirstOrDefault<FollowUp>(query, custid);
                    #endregion
                    query = @" SELECT oss.IsNeedSurvey, oss.IsNSASkipSurvey from ordersimulation os INNER JOIN ordersimulationsurvey oss ON os.OrderNo=oss.OrderNo WHERE os.FollowUpNo=@0";

                    List<dynamic> OrderSimulationSurvey = db.Fetch<dynamic>(query,FollowUp.FollowUpNo);
                    query = @"SELECT * FROM OrderSimulation WHERE CustId=@0 AND ApplyF=1                                ";
                    OrderSimulation = db.FirstOrDefault<OrderSimulation>(query, custid);
                    query = @"SELECT * FROM ProspectCompany WHERE CustId=@0";
                    ProspectCompany = db.FirstOrDefault<ProspectCompany>(query,custid);

                    List<string> PhonePriorityList = MobileRepository.GetPhoneNumberList(OrderSimulation.OrderNo, FollowUp.IsRenewal);
                    //#region ORDERSIMULATION
                    //                    query = @"
                    //					SELECT * FROM OrderSimulation WHERE CustId=@0 AND ApplyF=1
                    //                ";
                    //                    OrderSimulation = db.FirstOrDefault<OrderSimulation>(query, custid);
                    //                    #endregion
                    //                    if (OrderSimulation != null)
                    //                    {

                    //                        #region ORDERSIMULATIONMV
                    //                        query = @"SELECT b.* FROM OrderSimulation a INNER JOIN OrderSimulationMV b ON 
                    //                                a.OrderNo=b.OrderNo WHERE a.RowStatus =1 AND 
                    //                                b.RowStatus = 1 AND a.FollowUpNo=@0 ";
                    //                        OrderSimulationMV = db.Fetch<OrderSimulationMV>(query, FollowUp.FollowUpNo);
                    //                        foreach (OrderSimulationMV osmv in OrderSimulationMV)
                    //                        {
                    //                            if (OrderSimulation.ComprePeriod + OrderSimulation.TLOPeriod == 0)
                    //                            {
                    //                                osmv.ProductTypeCode = null;
                    //                                OrderSimulation.ProductCode = null;
                    //                            }
                    //                        }
                    //                        #endregion


                    //                        #region ORDERSIMULATION INTEREST
                    //                        query = @"SELECT * FROM OrderSimulationInterest WHERE RowStatus=1 AND OrderNo=@0
                    //                ";
                    //                        OrderSimulationInterest = null;
                    //                        if (OrderSimulation != null)
                    //                        {
                    //                            OrderSimulationInterest = db.Fetch<OrderSimulationInterest>(query, OrderSimulation.OrderNo);
                    //                        }
                    //                        #endregion


                    //                        #region ORDERSIMULATION COVERAGE
                    //                        query = @"SELECT * FROM OrderSimulationCoverage WHERE RowStatus=1 AND OrderNo=@0
                    //                ";
                    //                        OrderSimulationCoverage = null;

                    //                        if (OrderSimulation != null)
                    //                        {
                    //                            OrderSimulationCoverage = db.Fetch<OrderSimulationCoverage>(query, OrderSimulation.OrderNo);
                    //                        }
                    //                        #endregion


                    //                        #region DEALER
                    //                        query = @"SELECT * FROM Dealer WHERE RowStatus=1 Order By Description 
                    //                ";
                    //                        Dealer = db.Fetch<Dealer>(query);
                    //                        #endregion


                    //                        #region SALESMAN DEALER
                    //                        query = @"SELECT * FROM SalesmanDealer WHERE DealerCode=@0 AND RowStatus=1  Order By Description
                    //                ";
                    //                        SalesmanDealer = null;
                    //                        if (OrderSimulation != null)
                    //                        {
                    //                            SalesmanDealer = db.Fetch<SalesmanDealer>(query, OrderSimulation.DealerCode);
                    //                        }
                    //                        #endregion

                    //                    }
                    //                    #region LOAD IMAGE
                    //                    List<dynamic> imageData = new List<dynamic>();
                    //                    if (FollowUp != null)
                    //                    {
                    //                        query = @"SELECT 
                    // ''''+  ISNULL([IdentityCard]+''',',''',')         
                    //    +''''+ ISNULL([STNK]+''',',''',')
                    //    +''''+ ISNULL([SPPAKB]+''',',''',')
                    //    +''''+ ISNULL([BSTB1]+''',',''',')
                    //    +''''+ ISNULL([BSTB2]+''',',''',')
                    //    +''''+ ISNULL([BSTB3]+''',',''',')
                    //    +''''+ ISNULL([BSTB4]+''',',''',')
                    //    +''''+ ISNULL([CheckListSurvey1]+''',',''',')
                    //    +''''+ ISNULL([CheckListSurvey2]+''',',''',')
                    //    +''''+ ISNULL([CheckListSurvey3]+''',',''',')
                    //    +''''+ ISNULL([CheckListSurvey4]+''',',''',')
                    //    +''''+ ISNULL([PaymentReceipt1]+''',',''',')
                    //    +''''+ ISNULL([PaymentReceipt2]+''',',''',')
                    //    +''''+ ISNULL([PaymentReceipt3]+''',',''',')
                    //    +''''+ ISNULL([PaymentReceipt4]+''',',''',')
                    //    +''''+ ISNULL([PremiumCal1]+''',',''',')
                    //    +''''+ ISNULL([PremiumCal2]+''',',''',')
                    //    +''''+ ISNULL([PremiumCal3]+''',',''',')
                    //    +''''+ ISNULL([PremiumCal4]+''',',''',')
                    //    +''''+ ISNULL([FormA1]+''',',''',')
                    //    +''''+ ISNULL([FormA2]+''',',''',')
                    //    +''''+ ISNULL([FormA3]+''',',''',')
                    //    +''''+ ISNULL([FormA4]+''',',''',')
                    //    +''''+ ISNULL([FormB1]+''',',''',')
                    //    +''''+ ISNULL([FormB2]+''',',''',')
                    //    +''''+ ISNULL([FormB3]+''',',''',')
                    //    +''''+ ISNULL([FormB4]+''',',''',')
                    //    +''''+ ISNULL([FormC1]+''',',''',')
                    //    +''''+ ISNULL([FormC2]+''',',''',')
                    //    +''''+ ISNULL([FormC3]+''',',''',')
                    //    +''''+ ISNULL([FormC4]+'''','''') 
                    //  FROM [AABMobile].[dbo].[FollowUp] WHERE FollowUpNo=@0";
                    //                        string listimage = db.ExecuteScalar<string>(query, FollowUp.FollowUpNo);


                    //                        query = @"SELECT * FROM ImageData WHERE PathFile IN (@@Param) AND RowStatus=1";
                    //                        query = query.Replace("@@Param", string.IsNullOrEmpty(listimage) ? "''" : listimage);
                    //                        imageData = db.Fetch<dynamic>(query);

                    //                    }

                    //                    #endregion

                    //                    //IF Prospect Lead
                    //                    string preferredtime = "";
                    //                    if (FollowUp.FollowUpStatus.Equals("9"))
                    //                    {
                    //                        #region Prefered Time
                    //                        preferredtime = db.ExecuteScalar<string>(@"
                    //					SELECT p.followUpDateTime FROM Penawaran p 
                    //                                JOIN HistoryPenawaran hp ON hp.PolicyID = p.policyID 
                    //                                JOIN OrderSimulation os ON os.OrderNo = hp.OrderNo 
                    //                                AND os.FollowUpNo =@0", FollowUp.FollowUpNo);
                    //                        #endregion
                    //                    }

                    query = @"SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc, CAST(COALESCE(IsManualSurvey,0) AS BIT) IsManualSurvey
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                    SurveySchedule = db.Fetch<Otosales.Models.vWeb2.SurveySchedule>(query, custid, FollowUp.FollowUpNo).FirstOrDefault();

                    return Json(new { status = true, message = "OK", OrderSimulation, ProspectCustomer, FollowUp, OrderSimulationSurvey, PhonePriorityList, ProspectCompany, SurveySchedule }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getSummaryResult(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  SELECT 
                     a.Name, a.Phone1, a.Email1, 
                     e.Description + ' ' + f.Description + ' ' + d.Year + ' ' + g.Series  AS 'Vehicle', 
                     CASE 
                     WHEN c.ComprePeriod > 0 AND c.TLOPeriod = 0 
	                    THEN 'Comprehensive ' + CAST(c.ComprePeriod AS VARCHAR) + 
		                    CASE WHEN c.ComprePeriod > 1 THEN ' Years' 
		                    ELSE ' Year' 
		                    END 
                    WHEN c.ComprePeriod = 0 AND c.TLOPeriod > 0 
	                    THEN 'TLO ' + CAST(c.TLOPeriod AS VARCHAR) + 
		                    CASE WHEN c.TLOPeriod > 1 THEN ' Years' 
		                    ELSE ' Year' 
		                    END 
					WHEN c.ComprePeriod = 0 AND c.TLOPeriod = 0 
	                    THEN '' + 
		                    CASE WHEN (SELECT COUNT(*) FROM OrderSimulationCoverage WHERE OrderNo=c.OrderNo AND CoverageId='ALLRIK')>0 THEN 'Comprehensive Others' 
		                    ELSE 'TLO Others' 
		                    END 
                    WHEN c.ComprePeriod > 0 AND c.TLOPeriod > 0 
	                    THEN 'Comprehensive ' + CAST(c.ComprePeriod AS VARCHAR) + 
		                    CASE 
			                    WHEN c.ComprePeriod > 1 THEN ' Years' 
			                    ELSE ' Year' 
			                    END + ' ,TLO ' + CAST(c.TLOPeriod AS VARCHAR) + 
		                    CASE 
			                    WHEN c.TLOPeriod > 1 THEN ' Years' 
			                    ELSE ' Year' 
			                    END 
                    END AS 'CoverageType', 
                    c.TotalPremium, d.SumInsured, d.AccessSI, h.Description AS 'DealerDesc', 
                    i.Description AS 'SalesmanDesc'
                    FROM ProspectCustomer a 
                    INNER JOIN FollowUp b ON a.custid=b.custid 
                    INNER JOIN OrderSimulation c ON b.followupno=c.followupno AND c.applyf=1 
                    INNER JOIN OrderSimulationMV d ON c.OrderNo = d.OrderNo 
                    INNER JOIN VehicleBrand e ON d.BrandCode = e.BrandCode 
                    AND d.ProductTypeCode = e.ProductTypeCode 
                    INNER JOIN VehicleModel f ON f.BrandCode = e.BrandCode 
                    AND f.ProductTypeCode = e.ProductTypeCode AND f.ModelCode = d.ModelCode 
                    INNER JOIN VehicleSeries g ON f.BrandCode = g.BrandCode 
                    AND f.ProductTypeCode = g.ProductTypeCode AND f.ModelCode = g.ModelCode 
                    AND d.Series = g.Series LEFT JOIN Dealer h ON a.DealerCode=h.DealerCode 
                    LEFT JOIN SalesmanDealer i ON a.SalesDealer=i.SalesmanCode AND h.DealerCode=i.DealerCode
                    WHERE b.FollowUpNo=@0
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getOrderDetailHeader(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();
                string branchcode = form.Get("branchcode").Trim();
                string channelsource = form.Get("channelsource").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  SELECT 
                    os.OrderNo, 
                    os.InsuranceType, 
                    pt.Description AS 'InsuranceTypeDesc', 
                    aab.City, 
                    os.EntryDate, 
                    os.QuotationNo, 
                    pc.Name AS 'ProspectName', 
                    pc.Phone1, 
                    pc.Email1, 
                    vb.Description + ' ' + vm.Description + ' ' + vs.Series AS 'Vehicle', 
                    osmv.RegistrationNumber, 
                    os.YearCoverage, 
                    osmv.AccessSI, 
                    osmv.Year, 
                    dif.Description AS 'Usage', 
                    osmv.Sitting, 
                    r.Sequence AS 'Region', 
                    os.TotalPremium, 
                    ao.Name AS 'AccountOfficerName', 
                    ao.Phone1 AS 'AOPhone', 
                    os.AdminFee, 
                   -- ao2.Name AS 'BranchHead', // TODO tanya korel untuk apa?
                    aab.Bank, 
                    aab.AccountNo, 
                    aab.AccountName, 
                    ao.Fax, 
                    ao.OfficePhone, 
                    os.TLOPeriod, 
                    os.ComprePeriod, 
                    case 
	                    when os.ComprePeriod > 0 and os.TLOPeriod = 0 
		                    then 'Comprehensive ' + CAST(os.ComprePeriod AS VARCHAR) + ' Tahun' 
	                    when os.ComprePeriod = 0 and os.TLOPeriod > 0 
		                    then 'TLO ' + CAST(os.TLOPeriod AS VARCHAR) + ' Tahun' 
	                    when os.ComprePeriod > 0 and os.TLOPeriod > 0 
		                    then 'Comprehensive ' + CAST(os.ComprePeriod AS VARCHAR) + ' Tahun\nTLO ' 
			                    + CAST(os.TLOPeriod AS VARCHAR) + ' Tahun' 
	                    end AS 'CoverageType' 
                    FROM OrderSimulation os 
                    INNER JOIN OrderSimulationMV osmv ON os.OrderNo=osmv.OrderNo 
                    INNER JOIN VehicleBrand vb on osmv.BrandCode=vb.BrandCode 
                    AND osmv.ProductTypeCode=vb.ProductTypeCode 
                    INNER JOIN VehicleModel vm on vm.BrandCode=vb.BrandCode 
                    AND vm.ProductTypeCode=vb.ProductTypeCode AND vm.ModelCode=osmv.ModelCode 
                    INNER JOIN VehicleSeries vs on vm.BrandCode = vs.BrandCode 
                    AND vm.ProductTypeCode=vs.ProductTypeCode AND vm.ModelCode=vs.ModelCode 
                    AND osmv.Series=vs.Series 
                    INNER JOIN ProspectCustomer pc ON os.CustID=pc.CustID 
                    INNER JOIN SalesOfficer ao ON os.SalesOfficerID=ao.SalesOfficerID 
                    INNER JOIN ProductType pt ON os.InsuranceType=pt.InsuranceType 
                    INNER JOIN AABBranch aab ON os.BranchCode=aab.BranchCode 
                    INNER JOIN Dtl_Ins_Factor dif ON dif.Factor_Code='MVUSAG' 
                    AND dif.insurance_code=osmv.UsageCode 
                    INNER JOIN Region r ON r.RegionCode=osmv.CityCode 
                    --INNER JOIN AABHead aah ON aah.Type='Branch' AND aah.Param=os.BranchCode //TODO
                    --INNER JOIN SalesOfficer ao2 ON ao2.SalesOfficerID=aah.Value  //TODO
                    WHERE os.FollowUpNo=@0 AND os.ApplyF=1
                ";

                string query2 = @"SELECT * FROM SysParam WHERE Type='ReportQuotation'";

                string query3 = @"SELECT A.* FROM Branch A INNER JOIN AABBranch B
 ON A.Name LIKE '%'+
 (SELECT TOP 1 [Description] FROM AABBRANCH WHERE BRANCHCODE = @0)+'%'
 WHERE B.BranchCode = @0";

                string query4 = @"SELECT * FROM SysParam WHERE Param='QuotSign'+@0";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);
                    var result2 = db.Fetch<dynamic>(query2);
                    var result3 = db.FirstOrDefault<dynamic>(query3, branchcode);
                    var result4 = db.FirstOrDefault<dynamic>(query4, channelsource);

                    return Json(new { status = true, message = "OK", data = result, spModels = result2, BranchData = result3, param = result4 }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getOrderDetailCoverage(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                double AdminFee = Convert.ToDouble(form.Get("AdminFee").Trim());
                string vtype = form.Get("vtype");
                string vcitycode = form.Get("vcitycode");
                string vusagecode = form.Get("vusagecode");
                string vyear = form.Get("vyear");
                string vsitting = form.Get("vsitting");
                string vModel = form.Get("vModel");
                string vBrand = form.Get("vBrand");
                string vIsNew = form.Get("isNew");
                string Ndays = form.Get("Ndays");
                ProductCode = form.Get("vProductCode");
                CalculatedPremiItems = JsonConvert.DeserializeObject<List<CalculatedPremi>>(form.Get("calculatedPremi"));
                DateTime PeriodFrom = Convert.ToDateTime(form.Get("PeriodFrom"));
                DateTime PeriodTo = Convert.ToDateTime(form.Get("PeriodTo"));
                #region detail scoring
                List<DetailScoring> dtlScoring = new List<DetailScoring>();
                DetailScoring ds = new DetailScoring();
                    if (!string.IsNullOrEmpty(vcitycode))
                    {
                        ds.FactorCode = "GEOGRA";
                        ds.InsuranceCode = vcitycode;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vusagecode))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "MVUSAG";
                        ds.InsuranceCode = vusagecode;
                        dtlScoring.Add(ds);
                    }
                    ds = new DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    ds.InsuranceCode = string.IsNullOrEmpty(vtype) ? "ALL   " : vtype;
                    dtlScoring.Add(ds);
                    if (!string.IsNullOrEmpty(vBrand))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "VBRAND";
                        ds.InsuranceCode = vBrand;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vIsNew))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "NEWVHC";
                        ds.InsuranceCode = vIsNew;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vModel))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "VMODEL";
                        ds.InsuranceCode = vModel;
                        dtlScoring.Add(ds);
                    }
                    #endregion
               //if (orderno == null || orderno == "")
                //{
                //    return Json(new { status = false, message = "Parameter is null", data = "null" });
                //}

//                string query = @"
//                  SELECT  
//                    osi.InterestID, osc.CoverageID, osi.Year, osc.Rate, 
//                    osc.SumInsured, osc.LoadingRate, osc.Loading, osc.Premium, 
//                    osc.IsBundling 
//                    FROM OrderSimulation os 
//                    INNER JOIN OrderSimulationMV osmv ON os.OrderNo=osmv.OrderNo
//                    INNER JOIN OrderSimulationInterest osi ON osmv.OrderNo=osi.OrderNo 
//                    AND osmv.ObjectNo=osi.ObjectNo
//                    INNER JOIN OrderSimulationCoverage osc ON osc.OrderNo=osi.OrderNo 
//                    AND osc.ObjectNo=osi.ObjectNo AND osc.InterestNo=osi.InterestNo 
//                    WHERE os.OrderNo=@0 AND osi.RowStatus = 1 AND osc.RowStatus = 1
//                    ORDER BY osi.Year
//                ";
                //initialize row table
               DetailCoverageSummary result = MobileRepository.GetDetailCoverageSummary(ProductCode, dtlScoring, CalculatedPremiItems,AdminFee,vyear,UsageCode,Convert.ToInt16(Ndays),PeriodFrom,PeriodTo,
                                                                                         vtype, vsitting,""   );

                    //using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    //{
                    //    var result = db.Fetch<dynamic>(query, orderno);

                        return Json(new { status = true, message = "OK", data =result}, Util.jsonSerializerSetting());
                    //}
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getQuotationHistory(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                if (followupno == null || followupno == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @"
                  select 
                    a.CustID, a.OrderNo, a.QuotationNo,  
                    a.EntryDate, a.TotalPremium, 
                    c.Description + ' ' + d.Description + ' ' + e.Series Vehicle, 
                    b.Year VehicleYear,  b.SumInsured, b.AccessSI, 
                    a.ApplyF, a.SendF, a.SendStatus, 
                    case 
	                    when a.ComprePeriod > 0 and a.TLOPeriod = 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod = 0 and a.TLOPeriod > 0 
		                    then 'TLO ' + CAST(a.TLOPeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod > 0 and a.TLOPeriod > 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)
			                    + 'Th TLO ' + CAST( a.TLOPeriod AS VARCHAR)+ 'Th' 
                    end AS 'CoverageType' 
                    from OrderSimulation a 
                    inner join OrderSimulationMV b on a.OrderNo = b.OrderNo 
                    inner join VehicleBrand c on b.BrandCode = c.BrandCode 
                    AND b.ProductTypeCode = c.ProductTypeCode 
                    inner join VehicleModel d on d.BrandCode = c.BrandCode 
                    AND d.ProductTypeCode = c.ProductTypeCode AND d.ModelCode = b.ModelCode 
                    inner join VehicleSeries e on d.BrandCode = e.BrandCode 
                    AND d.ProductTypeCode = e.ProductTypeCode AND d.ModelCode = e.ModelCode 
                    AND b.Series = e.Series 
                    where a.FollowUpNo=@0
                    ORDER BY a.EntryDate DESC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, followupno);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        //        [HttpPost]
        //        public IHttpActionResult SaveFollowUpDetails(FormDataCollection form)
        //        {
        //            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
        //            _log.Debug(actionName + " Function start : ");
        //            try
        //            {
        //                string FUData = form.Get("FollowUp").Replace(":0\",", ":01\",");
        //                FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);

        //                string FULasData = form.Get("FollowUpLast").Replace(":0\",", ":01\",");
        //                FollowUp FULast = JsonConvert.DeserializeObject<FollowUp>(FULasData);

        //                bool isInfoChanged = bool.Parse(form.Get("isInfoChanged").Trim());

        //               // if (validateDataChangeStatus(FU)) {
        //                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
        //                {
        //                    #region UPDATE FOLLOW UP
        //                    string queryupdatefollowup = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[FollowUpNotes]=@6,[PICWA]=@7,[LastUpdatedTime]=GETDATE() WHERE [FollowUpNo]=@0";

        //                    db.Execute(queryupdatefollowup, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, FU.FollowUpNotes, FU.PICWA);
        //                    #endregion
        //                    #region SEND EMAIL PIC WA
        //                    if (!string.IsNullOrEmpty(FU.PICWA)) { 
        //                    string subject = ConfigurationManager.AppSettings["EmailSubjectPengirimanNoWA"].ToString();
        //                    string emailto = ConfigurationManager.AppSettings["EmailToSendPICWA"].ToString();
        //                    string emailcc = ConfigurationManager.AppSettings["EmailCCSendPICWA"].ToString();
        //                    string content = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='CONTENT-EMAILPICWA' AND ParamType='TMP'");
        //                    OrderSimulation os = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE FollowUpNo=@0 AND CustId=@1",FU.FollowUpNo,FU.CustID);
        //                    content = content.Replace("@PICWA", FU.PICWA).Replace("@CustomerName", FU.ProspectName).Replace("@ProductType", "").Replace("@TotalPremi", Util.ToThousand(os.TotalPremium))
        //                                     .Replace("@MetodePembayaran", "").Replace("@PeriodTo","").Replace("@TRName", "");
        //                    subject = subject.Replace("@CustomerName", FU.ProspectName).Replace("@NoPolisLama",os.PolicyNo);
        //                    A2isCommonResult a = MessageController.SendEmail(emailto, emailcc, "", content, subject, null);
        //                    }
        //                    #endregion
        //                    if (isInfoChanged)
        //                    {
        //                        if (FU.FollowUpStatus == 9)
        //                        {
        //                            string query = @"SELECT TOP 1 * FROM HistoryPenawaran 
        //                                            WHERE OrderNo = (SELECT OrderNo FROM OrderSimulation 
        //                                            WHERE FollowUpNo =@0 ORDER BY EntryDate";

        //                            HistoryPenawaran hp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);
        //                            hp.IsAbleExpired = 0;

        //                            bool responsestatus = false;
        //                            using (var dbagent = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
        //                            {
        //                                int bidExist = dbagent.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", hp.HistoryPenawaranID);

        //                                if (bidExist == 1)
        //                                {
        //                                    // permanently set data to agent
        //                                    db.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);
        //                                    responsestatus = true;
        //                                }

        //                                if (responsestatus)
        //                                {
        //                                    //#region UPDATE FOLLOW UP
        //                                    //query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE() WHERE [FollowUpNo]=@0";

        //                                    //db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate);
        //                                    //#endregion

        //                                    int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

        //                                    #region INSERT FOLLOW UP HISTORY
        //                                    query = @";
        //    INSERT INTO FollowUpHistory (
        //       [FollowUpNo]
        //      ,[SeqNo]
        //      ,[CustID]
        //      ,[ProspectName]
        //      ,[Phone1]
        //      ,[Phone2]
        //      ,[SalesOfficerID]
        //      ,[EntryDate]
        //      ,[FollowUpName]
        //      ,[NextFollowUpDate]
        //      ,[LastFollowUpDate]
        //      ,[FollowUpStatus]
        //      ,[FollowUpInfo]
        //      ,[Remark]
        //      ,[BranchCode]
        //      ,[FollowUpNotes]
        //      ,[LastUpdatedTime]
        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,@14,GETDATE(),0)
        //";

        //                                    db.Execute(query, FULast.FollowUpNo, lastseqno, FULast.CustID, FULast.ProspectName, FULast.Phone1, FULast.Phone2, FULast.SalesOfficerID, FULast.FollowUpName, FULast.NextFollowUpDate, FULast.LastFollowUpDate, FULast.FollowUpStatus, FULast.FollowUpInfo, FULast.Remark, FULast.BranchCode, FULast.FollowUpNotes);
        //                                    #endregion

        //                                    db.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);

        //                                }
        //                            }
        //                            query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0";

        //                            return Json(new { status = true, message = "succes", data = db.FirstOrDefault<dynamic>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
        //                        }
        //                        else
        //                        {
        //                            //#region UPDATE FOLLOW UP
        //                            //string query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE() WHERE [FollowUpNo]=@0";

        //                            //db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate);
        //                            //#endregion
        //                            string query = "";

        //                            int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

        //                            #region INSERT FOLLOW UP HISTORY
        //                            query = @";
        //    INSERT INTO FollowUpHistory (
        //       [FollowUpNo]
        //      ,[SeqNo]
        //      ,[CustID]
        //      ,[ProspectName]
        //      ,[Phone1]
        //      ,[Phone2]
        //      ,[SalesOfficerID]
        //      ,[EntryDate]
        //      ,[FollowUpName]
        //      ,[NextFollowUpDate]
        //      ,[LastFollowUpDate]
        //      ,[FollowUpStatus]
        //      ,[FollowUpInfo]
        //      ,[Remark]
        //      ,[BranchCode]
        //      ,[FollowUpNotes]
        //      ,[LastUpdatedTime]
        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,@14,GETDATE(),0)
        //";

        //                            db.Execute(query, FULast.FollowUpNo, lastseqno, FULast.CustID, FULast.ProspectName, FULast.Phone1, FULast.Phone2, FULast.SalesOfficerID, FULast.FollowUpName, FULast.NextFollowUpDate, FULast.LastFollowUpDate, FULast.FollowUpStatus, FULast.FollowUpInfo, FULast.Remark, FULast.BranchCode, FULast.FollowUpNotes);
        //                            #endregion

        //                            query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0";

        //                            return Json(new { status = true, message = "succes",isvalid=true, data = db.FirstOrDefault<dynamic>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
        //                        }
        //                    }
        //                    else
        //                    {
        //                        string query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0";
        //                        return Json(new { status = true, message = "succes", isvalid = true, data = db.FirstOrDefault<dynamic>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
        //                    }

        //                }

        //            //    }
        //            //    else
        //            //    {
        //            //        return Json(new { status = true, message = "Dokumen Anda Belum Lengkap", isvalid = false });

        //            //}
        //            }
        //            catch (Exception e)
        //            {
        //                _log.Error("Function " + actionName + " Error : " + e.ToString());
        //                return Json(new { status = false, message = e.Message, data = "null" });
        //            }
        //        }


        private bool validateDataChangeStatus(FollowUp FU)
        {
            bool valid = true;
            if (string.IsNullOrEmpty(FU.IdentityCard))
            {
                valid = false;
            }
            return valid;
        }

        [HttpPost]
        public IHttpActionResult SaveFollowUpStatusDetails(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string FUData = form.Get("FollowUp");
                string Editor = form.Get("editor");
                FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);

                string FULasData = form.Get("FollowUpLast");
                FollowUp FULast = JsonConvert.DeserializeObject<FollowUp>(FULasData);

                bool isInfoChanged = false;

                bool isSentToSA = bool.Parse(form.Get("isSentToSA").Trim());
                string STATE = form.Get("STATE");
                string TRName = form.Get("TRName");

                #region MGO BID
                if (STATE.Equals("MGOBID"))
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        // FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;
                        //FULast.SalesOfficerID = FULast.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FULast.SalesOfficerID) : FULast.SalesOfficerID;

                        string query = @";SELECT TOP 1 * FROM Quotation.dbo.HistoryPenawaran 
WHERE OrderNo = (SELECT TOP 1 OrderNo FROM OrderSimulation 
WHERE FollowUpNo =@0 ORDER BY EntryDate DESC)";

                        HistoryPenawaran hp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);
                        hp.IsAbleExpired = 0;

                        bool responsestatus = false;

                        // check all mgo bid if expired bid still exist
                        Util.CheckAllMGOBidExpiry();

                        using (var dbagent = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                        {
                            // check data not expired
                            int bidExist = dbagent.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", hp.HistoryPenawaranID);

                            if (bidExist == 1)
                            {
                                // permanently set data to agent
                                dbagent.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);
                                responsestatus = true;
                            }

                            if (responsestatus)
                            {

                                #region execUpdateFollowUp
                                #region UPDATE FOLLOW UP
                                if (FU.FollowUpInfo != FULast.FollowUpInfo)
                                {
                                    isInfoChanged = true;


                                }
                                query = @";UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6,[Receiver]=@7,[FollowUpNotes]=@8 WHERE [FollowUpNo]=@0";

                                db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1,FU.Receiver,FU.FollowUpNotes);

                                #endregion

                                if (isInfoChanged)
                                {
                                    #region INSERT FOLLOW UP HISTORY
                                    query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]  
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,GETDATE(),@10,@11,@12,@13,GETDATE(),0)
";
                                    int lastseqnofuh = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                    db.Execute(query, FU.FollowUpNo, lastseqnofuh, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                                    #endregion
                                    MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);
                       
                                }
                                string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                                #endregion

                                int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                db.Execute("UPDATE Quotation.dbo.HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);

                                query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                                MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = message, data = db.FirstOrDefault<FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());

                            }
                            else
                            {
                                query = @";SELECT * FROM OrderSimulation  WHERE FollowUpNo = @0";
                                List<OrderSimulation> orderSimulation = db.Fetch<OrderSimulation>(query, FU.FollowUpNo);

                                query = @";SELECT * FROM Quotation.dbo.HistoryPenawaran hp 
JOIN OrderSimulation os ON os.OrderNo = hp.OrderNo 
JOIN FollowUp fu On fu.FollowUpNo = os.FollowUpNo  WHERE fu.FollowUpNo = @0";

                                HistoryPenawaran hptemp = db.FirstOrDefault<HistoryPenawaran>(query, FU.FollowUpNo);

                                foreach (OrderSimulation os in orderSimulation)
                                {
                                    string queryDeleted = @";UPDATE OrderSimulationCoverage SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationInterest SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationMV SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulation SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);

                                }

                                string queryDelete = @";UPDATE FollowUpHistory SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE FollowUp SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE ProspectCustomer SET RowStatus=0 WHERE CustID=@0";
                                db.Execute(queryDelete, FU.CustID);
                                queryDelete = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE HistoryPenawaranID=@0";
                                db.Execute(queryDelete, hptemp.HistoryPenawaranID);

                                int hpCount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM HistoryPenawaran WHERE PolicyID = @0", hp.PolicyID);
                                if (hpCount == 0)
                                {
                                    queryDelete = @";UPDATE Quotation.dbo.Penawaran SET RowStatus=0 WHERE PolicyID=@0";
                                    db.Execute(queryDelete, hptemp.HistoryPenawaranID);
                                }

                                MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = "Unable to process your request. This prospect has been expired" }, Util.jsonSerializerSetting());
                            }

                        }
                    }

                }
                #endregion

                else
                {
                    # region execUpdateFollowUp
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        // FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;
                        // FULast.SalesOfficerID = FULast.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FULast.SalesOfficerID) : FULast.SalesOfficerID;


                        #region UPDATE FOLLOW UP
                        if (FU.FollowUpInfo != FULast.FollowUpInfo)
                        {
                            isInfoChanged = true;


                        }
                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
BEGIN
SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
END
ELSE
BEGIN
SELECT 0 AS SeqNo
END", FU.FollowUpNo) + 1;

                        string query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6, [FollowUpNotes]=@7,[PICWA]=@8,[Receiver]=@9 WHERE [FollowUpNo]=@0";

                        db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, lastseqno, FU.FollowUpNotes, FU.PICWA,FU.Receiver);

                        #endregion

                        if (isInfoChanged)
                        {
                            #region INSERT FOLLOW UP HISTORY
                            query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,GETDATE(),@9,@10,@11,@12,GETDATE(),0)
";

                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FULast.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                            #endregion
                            MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);
                       
                        }
                        string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                        query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                        //#region SEND EMAIL PIC WA
                        //if (!string.IsNullOrEmpty(FU.PICWA))
                        //{
                        //    string subject = ConfigurationManager.AppSettings["EmailSubjectPengirimanNoWA"].ToString();
                        //    string emailto = ConfigurationManager.AppSettings["EmailToSendPICWA"].ToString();
                        //    string emailcc = ConfigurationManager.AppSettings["EmailCCSendPICWA"].ToString();
                        //    string content = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='CONTENT-EMAILPICWA' AND ParamType='TMP'");
                        //    OrderSimulation os = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE FollowUpNo=@0 AND CustId=@1", FU.FollowUpNo, FU.CustID);
                        //    string producttype = db.ExecuteScalar<string>("SELECT Description FROM ProductType WHERE InsuranceType=@0", os.InsuranceType);
                        //    List<string> Prefix = db.Fetch<string>("select ParamValue from applicationparameters WHERE ParamName in ('PREFIXVAPERMATA','PREFIXVAMANDIRI','PREFIXVABCA') and ParamType='CNF'");

                        //    string MetodePembayaran = "Rekening Virtual Account Bank Permata ";
                        //    if (Prefix.Count == 3)
                        //    {
                        //        MetodePembayaran = string.Concat(MetodePembayaran, "[" + Prefix[0] + "]" + os.VANumber);
                        //        if (Convert.ToBoolean(FU.IsRenewal))
                        //        {
                        //            MetodePembayaran = string.Concat(MetodePembayaran, ", Rekening Virtual Account Bank Mandiri [" + Prefix[1] + "]" + os.VANumber);
                        //            MetodePembayaran = string.Concat(MetodePembayaran, ", Rekening Virtual Account Bank BCA [" + Prefix[2] + "]" + os.VANumber);
                        //        }

                        //    }

                        //    IFormatProvider culture = new CultureInfo("en-US", true);
                        //    string periods = "01/01/1900";
                        //    if (os.PeriodTo != null)
                        //    {
                        //        string det = Convert.ToString(os.PeriodTo);
                        //        DateTime period = os.PeriodTo ?? DateTime.Now;
                        //        periods = period.ToString("dd/MM/yyyy");
                        //    }
                        //    content = content.Replace("@PICWA", FU.PICWA).Replace("@CustomerName", FU.ProspectName).Replace("@ProductType", producttype).Replace("@TotalPremi", "Rp. " + Util.ToThousand(os.TotalPremium))
                        //                 .Replace("@MetodePembayaran", MetodePembayaran).Replace("@PeriodTo", periods).Replace("@TRName", "");
                        //    subject = subject.Replace("@CustomerName", FU.ProspectName).Replace("@NoPolisLama", os.PolicyNo);
                        //    List<A2isMessagingAttachment> att = new List<A2isMessagingAttachment>();
                        //    A2isCommonResult a = MessageController.SendEmail(emailto, emailcc, "", content, subject, att);
                        //}
                        //#endregion
                        MobileRepository.InsertMstOrderMobile(FU);
                        return Json(new { status = true, message = message, data = db.FirstOrDefault<FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                    }
                    #endregion
                }



            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpDetailsStatusInfo(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string statuscode = form.Get("statuscode").Trim();
                string statuscodebefore = form.Get("statuscodebefore").Trim();
                string statusinfocodebefore = form.Get("statusinfocodebefore").Trim();
                string IsRenewal = form.Get("IsRenewal").Trim();
                string NeedSurvey = form.Get("NeedSurvey").Trim();
                string NeedNSA = form.Get("NeedNSA").Trim();
                string followUpNo = form.Get("followUpNo").Trim();
                List<dynamic> result = new List<dynamic>();

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    result = db.Fetch<dynamic>(";exec [usp_GetFollowUpDetailsStatusInfoOtosales] @0,@1,@2,@3,@4,@5,@6", statuscode, statuscodebefore, statusinfocodebefore, IsRenewal, NeedSurvey, NeedNSA,followUpNo);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getStatusInfoDescription(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string infocode = form.Get("infocode").Trim();

                string query = @"
                    SELECT Description FROM FollowUpStatusInfo WHERE InfoCode=@0
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, infocode);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getStatusInfoDescriptionAll()
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string query = @"
                    SELECT InfoCode, Description FROM FollowUpStatusInfo
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult isValidatedSendToSA(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                string query = @"
                    SELECT DISTINCT a.* FROM OrderSimulation a 
INNER JOIN OrderSimulationMV b ON a.OrderNo=b.OrderNo 
INNER JOIN OrderSimulationInterest c ON b.OrderNo=c.OrderNo 
AND b.ObjectNo=c.ObjectNo
INNER JOIN OrderSimulationCoverage d ON c.OrderNo=d.OrderNo 
AND c.ObjectNo=d.ObjectNo AND c.InterestNo=d.InterestNo
WHERE a.FollowUpNo=@0
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    List<OrderSimulation> OS = db.Fetch<OrderSimulation>(query, followupno);

                    if (OS.Count == 0)
                    {
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = "There is no quotation for this prospect. Please make quotation first." }, Util.jsonSerializerSetting());
                    }

                    if (OS[0].ProductCode == null)
                    {
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = "Information not complete, please check mandatory field" }, Util.jsonSerializerSetting());
                    }

                    return Json(new { status = true, message = "OK", vaildToSA = true, toast = "Valid BY SQL QUERY" }, Util.jsonSerializerSetting());

                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult isValidatedSendToSArev(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string query = @";SELECT DISTINCT a.* FROM OrderSimulation a 
  INNER JOIN OrderSimulationMV b ON a.OrderNo=b.OrderNo 
  INNER JOIN OrderSimulationInterest c ON b.OrderNo=c.OrderNo AND 
  b.ObjectNo=c.ObjectNo
  INNER JOIN OrderSimulationCoverage d ON c.OrderNo=d.OrderNo AND 
  c.ObjectNo=d.ObjectNo AND c.InterestNo=d.InterestNo WHERE a.FollowUpNo=@0";

                    string queryFollowUp = @";select * from followup where followupno = @0";
                    FollowUp fuModel = db.FirstOrDefault<FollowUp>(queryFollowUp, followupno);

                    List<OrderSimulation> osModels = db.Fetch<OrderSimulation>(query, followupno);
                    string message = "";
                    if (osModels.Count == 0)
                    {
                        message = "There is no quotation for this prospect. Please make quotation first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(fuModel.IdentityCard))
                    {
                        message = "Please complete photo of KTP/KITAS/SIM first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(fuModel.STNK))
                    {
                        message = "Please complete photo of STNK or BSTB first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(fuModel.SPPAKB))
                    {
                        message = "Please complete photo of SPPAKB first.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (fuModel.FollowUpStatus == 6 && fuModel.FollowUpInfo == 28)
                    {
                        message = "You could not send document of this follow up because the policy status on follow up is already awaiting approval.";
                        return Json(new { status = true, message = "OK", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }
                    else if (string.IsNullOrEmpty(osModels[0].ProductCode))
                    {
                        message = "Information not complete, please check mandatory field";
                        return Json(new { status = true, message = "DIALOG", vaildToSA = false, toast = message }, Util.jsonSerializerSetting());
                    }

                    return Json(new { status = true, message = "OK", vaildToSA = true, toast = "VALID" }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }

        }

        [HttpPost]
        public IHttpActionResult isValidatedSaveStatus(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string followupno = form.Get("followupno").Trim();
                string followupstatusinfo = form.Get("followupstatusinfo").Trim();

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string queryFollowUp = @";select * from followup where followupno = @0";
                    bool isvalid = true;
                    FollowUp fuModel = db.FirstOrDefault<FollowUp>(queryFollowUp, followupno);
                    OrderSimulationMV osmv = db.FirstOrDefault<OrderSimulationMV>("SELECT osmv.* FROM OrdersimulationMV osmv INNER JOIN Ordersimulation os ON os.OrderNo=osmv.OrderNo WHERE os.FollowUpNo=@0", fuModel.FollowUpNo);
                    OrderSimulationSurvey oss = db.FirstOrDefault<OrderSimulationSurvey>("SELECT * FROM OrdersimulationSurvey WHERE OrderNo=@0", osmv.OrderNo);
                    if (followupstatusinfo.Equals("58") || followupstatusinfo.Equals("61")) //TODO Memastikan validasi benar
                    {
                        if (string.IsNullOrEmpty(fuModel.IdentityCard) || string.IsNullOrEmpty(fuModel.SPPAKB) || string.IsNullOrEmpty(fuModel.STNK) || string.IsNullOrEmpty(osmv.ChasisNumber) || string.IsNullOrEmpty(osmv.EngineNumber))
                        {
                            isvalid = false;
                        }
                        if (followupstatusinfo.Equals("58"))//SEND TO SURVEYOR
                        {
                            if (string.IsNullOrEmpty(osmv.ColorOnBPKB)
                                || string.IsNullOrEmpty(oss.CityCode) //|| oss.IsNeedSurvey == null || oss.IsNSASkipSurvey == null 
                                || string.IsNullOrEmpty(oss.LocationCode) || string.IsNullOrEmpty(oss.SurveyAddress)
                                || string.IsNullOrEmpty(oss.SurveyPostalCode) || string.IsNullOrEmpty(oss.ScheduleTimeID) || oss.SurveyDate == null)
                            {
                                isvalid = false;
                            }
                        }


                    }
                    if (isvalid)
                    {
                        return Json(new { status = true, message = "OK", isvalid = isvalid, toast = "" }, Util.jsonSerializerSetting());

                    }
                    else
                    {
                        return Json(new { status = true, message = "OK", isvalid = isvalid, toast = "Harap melengkapi data yang wajib diisi !" }, Util.jsonSerializerSetting());

                    }
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }

        }


        [HttpPost]
        public IHttpActionResult getTPLSIList(FormDataCollection form)
        {

            string ProductCode = form.Get("ProductCode").Trim();

            if (ProductCode == null || ProductCode == "")
            {
                return Json(new { status = false, message = "Parameter is null", data = "null" });
            }
            var query = @"SELECT DISTINCT Product_Code AS ProductCode,Coverage_Id AS CoverageId,BC_Up_To_SI AS TSI FROM Mapping_Cover_Progressive WHERE Product_Code =@0";

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                var result = db.Fetch<dynamic>(query, ProductCode);
                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
        }

        private string calculateAgainPremi()
        {
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;
            PADRVRPremi = 0;
            PAPASSPremi = 0;

            foreach (CalculatedPremi cp in CalculatedPremiItems)
            {
                if (cp.CoverageID.TrimEnd().Equals("SRCC") || cp.CoverageID.TrimEnd().Equals("SRCTLO"))
                {
                    IsSRCCChecked = true;
                    SRCCPremi += cp.Premium;
                }
                if (cp.CoverageID.TrimEnd().Equals("FLD") || cp.CoverageID.TrimEnd().Equals("FLDTLO"))
                {
                    IsFLDChecked = true;
                    FLDPremi += cp.Premium;
                }
                if (cp.CoverageID.TrimEnd().Equals("ETV") || cp.CoverageID.TrimEnd().Equals("ETVTLO"))
                {
                    IsETVChecked = true;
                    ETVPremi += cp.Premium;
                }
            }


            //Load previous non basic Items
            RefreshPremi();
            CalculateLoading();
            CalculateTotalPremi();
            return "";// CheckSumInsured(); --0089 URF 2019
        }

        [HttpPost] // Not Use
        public IHttpActionResult saveTokenPushNotif(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string username = form.Get("username").Trim();
                string token = form.Get("token");



                if (string.IsNullOrEmpty(token))
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }
                var query = @";IF EXISTS(SELECT * FROM TokenPushNotifReact WHERE Username = @0)
BEGIN
UPDATE TokenPushNotifReact SET
       TokenPushNotif=@1
      ,LastUpdatedTime=GETDATE() WHERE Username=@0
END
    ELSE
BEGIN
INSERT INTO TokenPushNotifReact (Username
      ,TokenPushNotif
      ,EntryDate
      ,LastUpdatedTime
      ) VALUES(@0,@1,GETDATE(),GETDATE())
END";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    // username = username.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", username) : username;

                    db.Execute(query, username, token);

                    return Json(new { status = true, message = "OK" }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }


        }

        [HttpPost]
        public IHttpActionResult SendReportEmail(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Email = form.Get("Email");
            Email = Email.Replace("null", "");
            string reportType = form.Get("ReportType");
            string salesOfficerID = form.Get("salesOfficerID");
            DateTime periodFrom = Convert.ToDateTime(form.Get("period"));
            DateTime periodTo = periodFrom.AddMonths(1).AddDays(1 - periodFrom.Day).Date;
            string userName = "";
            string subject = "";
            string emailsubject = "";
            string filename = "";
            string filePath = "";
            string displayName = "";
            string emailText = "";
            string UserNameNotificationDisplay = ConfigurationManager.AppSettings["UserNameNotificationDisplay"].ToString();
            List<A2isMessagingAttachment> attFiles = new List<A2isMessagingAttachment>();

            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                //salesOfficerID = salesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", salesOfficerID) : salesOfficerID;

                userName = db.ExecuteScalar<string>("SELECT Name FROM SalesOfficer WHERE SalesOfficerID=@0 ", salesOfficerID);
                string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();
                if (reportType == "REPORTOFFICER")
                {
                    emailsubject = ConfigurationManager.AppSettings["EmailSubjectSummary"].ToString();
                    subject = emailsubject + " - " + userName + " - " + periodFrom.ToString("dd/MM/yyyy") + " - " + periodTo.AddDays(-1).ToString("dd/MM/yyyy");
                    filename = emailsubject + " - " + userName + " - " + periodFrom.ToString("dd-MM-yyyy") + " - " + periodTo.AddDays(-1).ToString("dd-MM-yyyy");
                    attFiles.Add(GenerateReportSummaryFile(salesOfficerID, periodFrom, periodTo, filename, "ReportOfficer"));
                    displayName = UserNameNotificationDisplay;
                    emailText = "Terlampir Prospect Summary milik " + userName + ". <br /><br />Terimakasih, semoga membantu kita semua dalam bekerja.";
                }
                else if (reportType == "REPORTMANAGER")
                {
                    emailsubject = ConfigurationManager.AppSettings["EmailSubjectDashboard"].ToString();
                    subject = emailsubject + " - " + userName + " - " + periodFrom.ToString("MMMM yyyy") + " - " + DateTime.Now.ToString("dd/MM/yyyy");
                    filename = "[" + emailsubject + "] [" + periodFrom.ToString("yyyy-MM") + "] [" + DateTime.Now.ToString("yyyy-MM-dd") + "]";
                    attFiles.Add(GenerateReportSummaryFile(salesOfficerID, periodFrom, periodTo, filename, "ReportManager"));
                    displayName = UserNameNotificationDisplay;
                    emailText = "Terlampir Report Dashboard Sales milik " + userName + ". <br /><br />Terimakasih, semoga membantu kita semua dalam bekerja.";
                }
                else if (reportType == "REPORTMANAGERYTD")
                {

                    emailsubject = ConfigurationManager.AppSettings["EmailSubjectDashboard"].ToString();
                    subject = emailsubject + " - " + userName + " - " + periodFrom.ToString("yyyy") + " - " + DateTime.Now.ToString("dd/MM/yyyy");
                    filename = "[" + emailsubject + "] [" + periodFrom.ToString("yyyy") + "] [" + DateTime.Now.ToString("yyyy-MM-dd") + "]";
                    attFiles.Add(GenerateReportSummaryFile(salesOfficerID, periodFrom, periodTo, filename, "ReportManagerYTD"));
                    displayName = UserNameNotificationDisplay;
                    emailText = "Terlampir Report Dashboard Sales milik " + userName + ". <br /><br />Terimakasih, semoga membantu kita semua dalam bekerja.";
                }

                A2isCommonResult rslt = MessageController.SendEmail(Email, "", "", emailText, subject, attFiles);

                if (rslt.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + rslt.ResultDesc });

                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        private A2isMessagingAttachment GenerateReportSummaryFile(string salesOfficerID, DateTime periodFrom, DateTime periodTo, string fileName, string reportType)
        {
            string filePath = "";
            A2isMessagingAttachment att = new A2isMessagingAttachment();
            string ReportSummaryPath = ConfigurationManager.AppSettings["ReportSummaryPath"].ToString();
            string ReportDashboardPath = ConfigurationManager.AppSettings["ReportDashboardPath"].ToString();
            string ReportDashboardYTDPath = ConfigurationManager.AppSettings["ReportDashboardYTDPath"].ToString();
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";


            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;

            NetworkCredential NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);


            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;

                int totalParam = 0;
                string reportPath = "";
                string paramNameID = "";

                if (reportType == "ReportOfficer")
                {
                    totalParam = 3;
                    reportPath = ReportSummaryPath;
                    paramNameID = "SalesOfficerID";
                }
                else if (reportType == "ReportManager")
                {
                    totalParam = 2;
                    reportPath = ReportDashboardPath;
                    paramNameID = "UserID";
                }
                else if (reportType == "ReportManagerYTD")
                {
                    totalParam = 2;
                    reportPath = ReportDashboardYTDPath;
                    paramNameID = "UserID";
                }

                using (ReportViewer rView = new ReportViewer())
                {
                    rView.ProcessingMode = ProcessingMode.Remote;

                    ServerReport serverReport = rView.ServerReport;
                    serverReport.ReportServerUrl = new Uri(ReportServerURL);
                    serverReport.ReportPath = reportPath;
                    serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;

                    //ReportParameter[] parameters = new ReportParameter[3];
                    ReportParameter[] reportParam = new ReportParameter[totalParam];
                    reportParam[0] = new ReportParameter();
                    reportParam[0].Name = paramNameID;
                    reportParam[0].Values.Add(salesOfficerID);

                    reportParam[1] = new ReportParameter();
                    reportParam[1].Name = "PeriodFrom";
                    reportParam[1].Values.Add(periodFrom.ToString());

                    if (reportType == "ReportOfficer")
                    {
                        reportParam[2] = new ReportParameter();
                        reportParam[2].Name = "PeriodTo";
                        reportParam[2].Values.Add(periodTo.ToString());
                    }

                    rView.ServerReport.SetParameters(reportParam);
                    bytes = rView.ServerReport.Render("EXCEL", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

                    filePath = ReportFiles + fileName + ".xls"; //"QUOTATION-" + OrderNo + ".pdf";

                    if (File.Exists(filePath))
                        File.Delete(filePath);
                    // 1. Read lock information from file   2. If locked by us, delete the file
                    //using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Delete))
                    //{
                    //    File.Delete(filePath);
                    //}

                    //using (FileStream fs = File.Create(filePath)) { }

                    att.FileByte = bytes;
                    att.FileContentType = "application/xls";
                    att.FileExtension = ".xls";
                    att.AttachmentDescription = fileName;
                }


            }
            catch (Exception ex)
            {
                filePath = "";
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();

            }

            return att;

        }

        [HttpPost]
        public IHttpActionResult ResetVehicleCode(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string vehicleCode = form.Get("vehicleCode");
            try
            {
                string query = @"SELECT c.Description + ' ' + d.Description + ' ' + a.Series + ' ' + a.Year AS VehicleDescription,a.VehicleCode, a.BrandCode, a.ProductTypeCode, a.ModelCode,  a.Series, 
                              a.Type, a.Sitting, a.Year, a.CityCode, a.Price, 
                              a.LastUpdatedTime, a.RowStatus FROM Vehicle a 
							  LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
							  AND c.ProductTypeCode = a.ProductTypeCode 
							  LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
						      AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
                              LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                              a.BrandCode=b.BrandCode 
                              AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                              AND a.Series=b.Series AND a.Type=b.Type 
                              AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                              AND a.Year > b.Year WHERE b.Year IS NULL And UPPER(a.VehicleCode) = UPPER('" +
                        vehicleCode + "') ORDER BY a.VehicleCode";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                Vehicle vehicle = db.FirstOrDefault<Vehicle>(query);
                if (vehicle != null)
                {
                    return Json(new { status = true, message = "OK", data = vehicle });
                }
                else
                {
                    return Json(new { status = false, message = "Data Not Available" });
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult CheckPhoneExist(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Phone = form.Get("Phone");
            string CustId = form.Get("CustId");
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                bool phoneexist = Convert.ToBoolean(db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM ProspectCustomer WHERE Phone1=@0 AND CustID !=@1", Phone, CustId));
                string message = phoneexist ? "Phone number already exist. Are you sure to continue?" : "";
                int code = phoneexist ? 001 : 002;
                return Json(new { status = true, message = message, code = code });

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        #region 0089 URF 2019
        //GetPostalCode
        [HttpPost]
        public IHttpActionResult GetPostalCode(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                var search = " ";
                var citycode = " ";
                if (form == null) return BadRequest("Bad Request");
                else
                {
                    search = string.IsNullOrEmpty(form.Get("search")) ? "" : form.Get("search");
                    citycode = string.IsNullOrEmpty(form.Get("citycode")) ? "" : form.Get("citycode");
                    search = "%" + search + "%";
                    var dataPostal = MobileRepository.GetPostalCode(search,citycode);
                  
                        _log.Debug("OK");
                        return Json(new { status = true, message = "Fetch success", data = dataPostal });
                    
                }
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }
        [HttpPost]
        public IHttpActionResult getFollowUpNotes(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {

                string InfoCode = string.IsNullOrEmpty(form.Get("InfoCode")) ? "" : form.Get("InfoCode");
                List<FollowUpNotes> result = new List<FollowUpNotes>();
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    int exist = db.ExecuteScalar<int>(@";SELECT COUNT(part) from AsuransiAstra.GODigital.Fn_SplitString((SELECT ParamValue FROM ApplicationParameters WHERE ParamName='FUSTATUSNOTESEMPTY' AND ParamType='CNF'),',') where part=@0", InfoCode);
                    if (exist == 0)
                    {
                        result = db.Fetch<FollowUpNotes>("SELECT * FROM FollowUpStatusNotes WHERE Rowstatus=1");
                    }

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }

                //  var result = db.Fetch<dynamic>(query, statuscode, followupstatusactivity);


            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskDetailDataKotor(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {

                string PolicyNo = string.IsNullOrEmpty(form.Get("PolicyNo")) ? "" : form.Get("PolicyNo");
                
                List<string> temp = new List<string>();
                List<dynamic> listImageAAB = new List<dynamic>();
                dynamic keyNumber;
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    temp = db.Fetch<string>(";exec sp_execute_renewal_info @@PolicyNo = @0", PolicyNo);
                    string result = "";
                    foreach(string s in temp){
                        result = result + s;
                    }
                    result = result.Replace("<!--br-->","");

                    keyNumber = db.Query<dynamic>(@"SELECT policy_holder_code AS CustIDAAB, order_no AS OrderNo FROM Policy WHERE policy_no =@0", PolicyNo).First();
                    if(keyNumber != null)
                    {
                        string qGetDataImageAAB = @"SELECT dt1.Reference_Type AS ImageType
                        FROM
                        (SELECT DI.Reference_No, DI.Reference_Type, MAX(DI.Image_Id) AS ImageId
                        FROM dbo.Dtl_Image DI
                        WHERE DI.Reference_No in ({1})
                        AND DI.Reference_Type in ({0})
                        GROUP BY DI.Reference_No, DI.Reference_Type) dt1
                        INNER JOIN dbo.Mst_Image MI ON dt1.ImageId = MI.Image_Id";
                        listImageAAB = db.Fetch<dynamic>(String.Format(qGetDataImageAAB,"'KTP','STNK'", "'" + Convert.ToString(keyNumber.CustIDAAB) + "','" + Convert.ToString(keyNumber.OrderNo) + "'"));
                    }
                    using (var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        string FollowUpNo = MobileRepository.GenerateGuid(GenGuid.FollowUpNo);
                        return Json(new { status = true, message = "OK", data = result, FollowUpNo, listImageAAB }, Util.jsonSerializerSetting());
                    }
                }

                //  var result = db.Fetch<dynamic>(query, statuscode, followupstatusactivity);


            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
        [HttpPost]
        public IHttpActionResult getUpliner(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {

                string SalesOfficerId = string.IsNullOrEmpty(form.Get("SalesOfficerId")) ? "" : form.Get("SalesOfficerId");
                List<Upliner> result = new List<Upliner>();
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {

                    result = db.Fetch<Upliner>(@"SELECT                                     
                                                c.Cust_Id AS UplinerCustomerID,
                                a.Upliner_Client_Code AS UplinerCode ,
                                RTRIM(d.Name) AS UplinerName,
                                e.Cust_Id AS LeaderCustomerID,
                                a.Leader_Client_Code AS LeaderCode ,                  
                                RTRIM(f.Name) AS LeaderName                    
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT' AND b.Cust_Id=@0 OR a.Client_Code=@0
", SalesOfficerId);


                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }

                //  var result = db.Fetch<dynamic>(query, statuscode, followupstatusactivity);


            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }
    
        //Vehicle       
        [HttpPost]
        public IHttpActionResult GetVehicle(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                var search = " ";
                var dataVehicle = new List<dynamic>();
                if (form == null) return BadRequest("Bad Request");
                else
                {
                    search = string.IsNullOrEmpty(form.Get("search")) ? "null" : form.Get("search");
                    string istlo = form.Get("istlo") == null ? "ALLRIK" : form.Get("istlo");
                
                    dataVehicle = MobileRepository.GetVehicleFullTextTerm(search, istlo);
                    return Json(new { status = true, message = "Fetch success", data = dataVehicle });
                    }
                
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = e.ToString() });
            }
        }



        #region RMF 0089
        [HttpPost]
        public IHttpActionResult getTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                #endregion
                TaskListDetail result = new TaskListDetail();
                try
                {
                    result = MobileRepository.GetTaksListDetail(CustID, FollowUpNo);
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string CustID = form.Get("CustID");
            string FollowUpNo = form.Get("FollowUpNo");
            string PersonalData = form.Get("PersonalData");
            string CompanyData = form.Get("CompanyData");
            string VehicleData = form.Get("VehicleData");
            string policyAddress = form.Get("PolicyAddress");
            string surveySchedule = form.Get("SurveySchedule");
            string imageData = form.Get("ImageData");
            string OSData = form.Get("OSData");
            string OSMVData = form.Get("OSMVData");
            string calculatedPremiItems = form.Get("calculatedPremiItems");
            string FollowUpStatus = form.Get("FollowUpStatus");
            string Remarks = form.Get("Remarks");
            #endregion

            if (string.IsNullOrEmpty(CustID) || string.IsNullOrEmpty(FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            string OrderNo = "";
            try
            {
                OrderNo = MobileRepository.UpdateTaskListDetail(CustID, FollowUpNo, PersonalData,
                    CompanyData, VehicleData, policyAddress, surveySchedule, imageData, OSData, OSMVData,
                    calculatedPremiItems, FollowUpStatus, Remarks);
                PaymentInfo pf = MobileRepository.GetPaymentInfo(CustID, FollowUpNo);
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", data = new {OrderNo, PaymentInfo = pf} });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
        

        [HttpPost]
        public IHttpActionResult GetBank(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string param = form.Get("ParamSearch");
            #endregion
            List<dynamic> result = new List<dynamic>();
            try
            {
                result = AABRepository.GetBank(param);
                return Json(new { status = true, message = "Success", data = result });
                _log.Debug(actionName + " Function end.");
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetPolicySentTo(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string param = form.Get("ParamSearch");
                #endregion

                List<PolicySentTo> list = MobileRepository.GetPolicySentTo();

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = list });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetSurveyCity(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null)
                return BadRequest("Input Required");
            else if (String.IsNullOrEmpty(form.Get("pageType")))
                return BadRequest("Input Required");
            else
            {

                try
                {
                    string type = form.Get("pageType").Trim(); //Branch
                    int filterType = Int32.Parse(form.Get("filterType")); //0
                    List<dynamic> filterLocation = MobileRepository.GetSurveyCity();

                    return Json(new { status = true, message = "success", filterLocation, SessionTimeOutF = false });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = e.ToString(), SessionTimeOutF = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetSurveyLocation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    GetSurveyLocationResult res = MobileRepository.GetSurveyLocation(form);
                    return Json(new { status = true, message = "success", data = res.data, spesific = res.spesific });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetSurveyZipCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug("Begin");
            string CityID = form.Get("CityID");

            if (String.IsNullOrEmpty(CityID))
            {
                return Json(new { status = false, message = "error", data = "CityID Parameter Required", SessionTimeOutF = false });
            }
            else
            {
                try
                {
                    dynamic res = MobileRepository.GetSurveyZipCode(CityID);
                    return Json(new { status = true, message = "success", data = res, SessionTimeOutF = false });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult DownloadQuotation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string OrderNo = form.Get("OrderNo");

                DownloadQuotationResult result = MobileRepository.GetByteReport(OrderNo);
                return Json(new { status = true, message = "success", data = result });
            }
            catch (Exception e)
            {
                return Json(new { status = false, message = "Error", data = e.ToString() });
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                throw e;
            }
        }

        [HttpPost]
        public IHttpActionResult addCopyQuotation(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string state = form.Get("state").Trim();
                string orderNo = form.Get("orderNo").Trim();
                string followUpNo = form.Get("followUpNo").Trim();
                string query = "";
                CopyQuotationModel result = new CopyQuotationModel();


                if (state == null || state == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }
                else
                {
                    result = MobileRepository.getAddCopyQuotation(state, orderNo, followUpNo);
                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }

            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult editQuotation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string custid = form.Get("custid").Trim();
                string followupno = form.Get("followupno").Trim();
                string orderNo = form.Get("orderNo").Trim();
                TaskListDetail result = new TaskListDetail();
                if (followupno == null || followupno == "" || orderNo == null || orderNo == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }
                else
                {
                    result = MobileRepository.getEditQuotation(custid, followupno, orderNo);
                    return Json(new
                    {
                        status = true,
                        message = "OK",
                        //OrderSimulation,
                        //OrderSimulationMV,
                        //OrderSimulationInterest,
                        //OrderSimulationCoverage
                        data = result
                    }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetSegmentCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string productCode = form.Get("ProductCode");
                    List<dynamic> res = aabRepository.GetSegmentCode(productCode);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetSurveyDocument(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string surveyNo = form.Get("SurveyNo");
                    Dictionary<string, List<SurveyDocument>> res = AABRepository.GetSurveyDocument(surveyNo);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetScheduleTime(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string BranchID = form.Get("BranchID");
                    string SurveyDate = form.Get("SurveyDate");
                    string PostalCodeID = form.Get("PostalCodeID");
                    string Language = form.Get("Language");
                    List<ScheduleTimeList> res = MobileRepository.GetScheduleTimeList(BranchID, SurveyDate, PostalCodeID, Language);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetSlotTimeBooking(FormDataCollection form)
        {
            _log.Debug("Begin");
            if (form == null) return BadRequest("Bad Request");
            string zipcode = form.Get("ZipCode");
            string cityid = form.Get("CityId");
            string branchID = form.Get("BranchID");
            string date = form.Get("AvailableDate");

            DateTime d;
            if (!DateTime.TryParse(date, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out d)) return BadRequest("Incorrect Date format");

            if (string.IsNullOrEmpty(cityid) || string.IsNullOrEmpty(date)) return BadRequest("Bad Request");
            try
            {
                dynamic res = null;
                if (!string.IsNullOrEmpty(zipcode))
                {
                    // 0181/URF/2018
                    IEnumerable<dynamic> data = null;
                    data = MobileRepository.GetSurveyScheduleSurveyManagement(cityid, zipcode, d);
                    // 0181/URF/2018
                    _log.Debug("OK");
                    res = data;
                }
                else if(!string.IsNullOrEmpty(branchID)){
                    string SurveyLocation = "";
                    a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB);
                    SurveyLocation = db.SingleOrDefault<string>(@"SELECT type.NAME TNAME from Branch branch inner join BranchType type on type.ID = branch.TYPE  WHERE branch.ID=@0 AND branch.isDeleted = 0 and branch.TYPE in(2,5,6) order by branch.NAME asc", branchID);

                    SurveyLocation = SurveyLocation.Replace(" ", "").ToUpper();
                    List<string> TimeCodeSurveyDalam = db.Query<string>(@"
                select part from GODigital.Fn_SplitString((select optionValue from Asuransiastra.dbo.AsuransiAstraOptions
                where optionName = 'GODIG-'+'" + SurveyLocation + "'+'-SURVEY-TIME'),',') ").ToList();

                    List<dynamic> times = GetScheduleSurveyDalam(TimeCodeSurveyDalam);
                    res = times;
                }
                return Json(new { status = true, message = "Fetch success", data = res });
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetPolicyDeliveryName(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string PolicySentToID = form.Get("PolicySentToID");
                    string ParamSearch = form.Get("ParamSearch");
                    List<dynamic> res = mobileRepository.GetPolicyDeliveryName(PolicySentToID, ParamSearch);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetEnableDisableSalesmanDealer(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string ProductCode = form.Get("ProductCode");
                    string BranchCode = form.Get("BranchCode");
                    if (string.IsNullOrEmpty(ProductCode) && string.IsNullOrEmpty(BranchCode))
                    {
                        return BadRequest("Input Required");
                    }

                    bool res = AABRepository.GetEnableDisableSalesmanDealer(ProductCode, BranchCode);
                    return Json(new { status = true, message = "success", IsSalesmanDealerEnable = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult CheckNSA(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string OrderNo = form.Get("OrderNo");
                    if (string.IsNullOrEmpty(OrderNo))
                    {
                        return BadRequest("Input Required");
                    }

                    List<dynamic> res = MobileRepository.GetCheckNSA(OrderNo);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }
        [HttpPost]
        public IHttpActionResult RetrieveExtSI(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string InterestId = form.Get("InterestId");
                    if (string.IsNullOrEmpty(InterestId))
                    {
                        return BadRequest("Input Required");
                    }

                    List<double> res = MobileRepository.RetrieveExtSI(InterestId);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }
        [HttpPost]
        public IHttpActionResult CheckDoubleInsured(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string ChasisNo = form.Get("ChasisNo");
                    string EngineNo = form.Get("EngineNo");
                    string OrderNo = form.Get("OrderNo");
                    if (string.IsNullOrEmpty(ChasisNo) && string.IsNullOrEmpty(EngineNo))
                    {
                        return BadRequest("Input Required");
                    }

                    CheckDoubleInsuredResult res = AABRepository.GetCheckDoubleInsured(ChasisNo, EngineNo, OrderNo);
                    return Json(new { status = true, message = "success", data = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }
        [HttpPost]
        public IHttpActionResult UpdateOrder(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string OrderNo = form.Get("OrderNo");
                    string DeviceID = form.Get("DeviceID");
                    string SalesOfficerID = form.Get("SalesOfficerID");
                    if (string.IsNullOrEmpty(OrderNo) && string.IsNullOrEmpty(DeviceID) && string.IsNullOrEmpty(SalesOfficerID)) {
                        return BadRequest("Input Required");
                    }
                    TaskListDetail result = MobileRepository.GetUpdateOrder(OrderNo, DeviceID, SalesOfficerID);
                    return Json(new { status = true, message = "success", data = result });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult CheckSurveyResult(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string OrderNo = form.Get("OrderNo");
                    bool result = MobileRepository.GetCheckSurveyResult(OrderNo);
                    return Json(new { status = true, message = "success", isChange = result });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetBankInformation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string SalesmanCode = form.Get("SalesmanCode");
                    BankInformationResult result = mobileRepository.GetBankInformation(SalesmanCode);
                    return Json(new { status = true, message = "success", data = result });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult SendQuotationEmail(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Email = form.Get("Email");
            string OrderNo = form.Get("OrderNo");
            Email = Regex.Replace(Email, @"\t|\n|\r", "");
            try
            {
                A2isCommonResult res = MobileRepository.SendQuotationEmail(OrderNo, Email);
                if (res.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + res.ResultDesc });
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult SendQuotationSMS(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Phone = form.Get("Phone");
            string OrderNo = form.Get("OrderNo");
            Phone = Regex.Replace(Phone, @"\t|\n|\r", "");
            try
            {
                A2isCommonResult statusMessage = MobileRepository.SendQuotationSMS(OrderNo,Phone);
                if (statusMessage.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = statusMessage.ResultDesc });
                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetMappingVehiclePlateGeoArea(FormDataCollection form) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string VehiclePlateCode = form.Get("VehiclePlateCode");
                List<dynamic> data = mobileRepository.GetMappingVehiclePlateGeoArea(VehiclePlateCode);
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
                throw e;
            }
        }

        private static dynamic GetScheduleSurveyDalam(List<string> TimeCodeSurveyDalam)
        {
            dynamic result = null;

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAAKSES))
            {
                string queryGetScheduleSurveyDalam = @"
SELECT 
        CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
             WHEN tc.TimeCategoryCode = 'TC2' THEN 40
             WHEN tc.TimeCategoryCode = 'TC3' THEN 41
             WHEN tc.TimeCategoryCode = 'TC4' THEN 42
        END ScheduleTime ,
        TC.TimeCategoryDescription ScheduleTimeTextID ,
        CASE WHEN tc.TimeCategoryCode = 'TC1'
             THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
             WHEN tc.TimeCategoryCode = 'TC2'
             THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
             WHEN tc.TimeCategoryCode = 'TC3'
             THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
             WHEN tc.TimeCategoryCode = 'TC4'
             THEN REPLACE(TC.TimeCategoryDescription, 'Malam', 'Night')
        END ScheduleTimeTextEN
FROM    GA.TimeCategory TC WHERE  timecategorycode in (@0)";
                result = db.Fetch<dynamic>(queryGetScheduleSurveyDalam, TimeCodeSurveyDalam.Count > 0 ? TimeCodeSurveyDalam : new List<string>(new string[] { "0" }));

                return result;
            }
        }

        [HttpPost]
        public IHttpActionResult TesRMF(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string FollowUpNo = "7eccb4cc-8f23-4ebc-bd6d-8ee2f0f93d1b";
                string qGetBktByr = @"SELECT BuktiBayar, Data, PolicyOrderNo, CoreImage_ID, ImageID
                                    FROM dbo.FollowUp f
                                    INNER JOIN dbo.ImageData id ON f.BuktiBayar = id.PathFile
                                    INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
                                    WHERE f.FollowUpNo = @0 AND
                                    (CoreImage_ID = '' OR CoreImage_ID IS NULL)
                                    AND f.RowStatus = 1";
                List<dynamic> bktbyr = db.Fetch<dynamic>(qGetBktByr, FollowUpNo);
                if (bktbyr.Count > 0)
                {
                    string outImageID = null;
                    string referenceImageType = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage("BuktiBayar", false);
                    if (!string.IsNullOrEmpty(bktbyr.First().PolicyOrderNo))
                    {
                        if (Otosales.Logic.ImageLogic.UploadImage(bktbyr.First().Data, bktbyr.First().BuktiBayar, referenceImageType, 1, "OTOSL", bktbyr.First().PolicyOrderNo, out outImageID) && outImageID != null)
                        {
                            Otosales.Logic.ImageLogic.UpdateImageData(bktbyr.First().ImageID, outImageID);
                        }
                    }
                    Otosales.Logic.ImageLogic.DeleteUnusedImages(FollowUpNo);
                }

                return Json(new { status = true, message = "Success"});
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Error" });
                throw e;
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                int currentPage = Convert.ToInt16(form.Get("currentPage"));
                int pageSize = Convert.ToInt16(form.Get("pageSize"));
                int currentPageLead = currentPage; // Convert.ToInt16(form.Get("currentPageLead"));
                int pageSizeLead = pageSize;// Convert.ToInt16(form.Get("pageSizeLead"));
                bool chIsSearchByChasNo = bool.Parse(form.Get("chIsSearchByChasNo").Trim());
                bool chIsSearchByEngNo = bool.Parse(form.Get("chIsSearchByEngNo").Trim());
                bool chIsSearchByPolicyNo = bool.Parse(form.Get("chIsSearchByPolicyNo").Trim());
                bool chIsSearchByProsName = bool.Parse(form.Get("chIsSearchByProsName").Trim());
                string salesofficerid = form.Get("salesofficerid").Trim();
                string search = form.Get("search").Trim();
                string indexOrderByItem = form.Get("indexOrderByItem").Trim();
                int indexOrderByTypeItem = Int32.Parse(form.Get("indexOrderByTypeItem").Trim());
                bool chFollowUpMonthlyChecked = bool.Parse(form.Get("chFollowUpMonthlyChecked").Trim());
                bool chFollowUpDateChecked = bool.Parse(form.Get("chFollowUpDateChecked").Trim());
                bool chNeedFUChecked = bool.Parse(form.Get("chNeedFUChecked").Trim());
                bool chPotentialChecked = bool.Parse(form.Get("chPotentialChecked").Trim());
                bool chCallLaterChecked = bool.Parse(form.Get("chCallLaterChecked").Trim());
                bool chCollectDocChecked = bool.Parse(form.Get("chCollectDocChecked").Trim());
                bool chNotDealChecked = bool.Parse(form.Get("chNotDealChecked").Trim());
                bool chSentToSAChecked = bool.Parse(form.Get("chSentToSAChecked").Trim());
                bool chBackToAOChecked = bool.Parse(form.Get("chBackToAOChecked").Trim());
                bool chPolicyCreatedChecked = bool.Parse(form.Get("chPolicyCreatedChecked").Trim());
                bool chOrderRejectedChecked = bool.Parse(form.Get("chOrderRejectedChecked").Trim());
                bool chNewChecked = bool.Parse(form.Get("chNewChecked").Trim());
                bool chRenewableChecked = bool.Parse(form.Get("chRenewableChecked").Trim());
                string dateFollowUp = form.Get("getFollowUpDate");
                int allowSearchOthers = Int32.Parse(form.Get("allowSearchOthers").Trim());

                DateTime getFollowUpDate = DateTime.Now;
                if (!string.IsNullOrEmpty(dateFollowUp))
                {
                    getFollowUpDate = Convert.ToDateTime(dateFollowUp);
                }

                if (salesofficerid == null || salesofficerid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @";SELECT * FROM (
SELECT DISTINCT * 
        FROM  (SELECT 0 
                      AS 
                            IsRenewalNonRenNot, 
                      CASE 
                        WHEN IsRenewalDigital = 1 THEN 'Renewable Digital'
                        ELSE 
						CASE WHEN IsRenewal=1 THEN 'Renewal'
						ELSE 'New'END 
                      END 
                      AS 
                            TaskStatus, 
                      b.description 
                      AS 
                            FollowUpDesc, 
                      c.description 
                      AS 
FollowUpReason, 
CASE 
    WHEN os.OldPolicyNo IS NOT NULL THEN (SELECT TOP 1 Period_To FROM BEYONDREPORT.AAB.dbo.Policy Where Policy_No = os.OldPolicyNo)
    ELSE ''
END ExpiredDate, 
a.CustID, 
a.FollowUpNo, 
a.LastFollowUpDate, 
a.NextFollowUpDate, 
a.LastSeqNo, 
CASE 
    WHEN a.followupstatus = 13 
        OR a.isrenewal = 1 THEN os.PolicyNo
    ELSE '' 
END PolicyNo, 
CASE pcm.isCompany
	WHEN 1 THEN pcs.CompanyName
	ELSE a.ProspectName 
END ProspectName, 
a.EntryDate,
a.IsRenewal,
p.isHotProspect,
a.followupstatus
FROM   followup a WITH (NOLOCK) 
INNER JOIN followupstatus b WITH (NOLOCK) 
  ON a.followupstatus = b.statuscode AND a.RowStatus = 1
LEFT JOIN followupstatusinfo c WITH (NOLOCK) 
 ON a.followupinfo = c.infocode 
LEFT JOIN dbo.OrderSimulation os WITH (NOLOCK)
 ON os.FollowUpNo = a.FollowUpNo AND os.ApplyF = 1 AND os.RowStatus = 1
 LEFT JOIN dbo.OrderSimulationMV omv WITH (NOLOCK)
 ON omv.OrderNo = os.OrderNo
 LEFT JOIN dbo.ProspectCustomer pcm 
 ON pcm.CustID = a.CustID
 LEFT JOIN dbo.ProspectCompany pcs
 ON pcs.CustID = a.CustID
 LEFT JOIN Quotation.dbo.HistoryPenawaran hp WITH (NOLOCK)
 ON hp.OrderNo = os.OrderNo 
 LEFT JOIN Quotation.dbo.Penawaran p WITH (NOLOCK)
 ON p.policyID = hp.PolicyID AND a.FollowUpStatus = 1 AND a.RowStatus = 1
WHERE  #salesofficerid
#nextfollowupdate 
 #followup  
 #chasisno #engineno
 #name
 #policyno) x
#datakotor
) res
#isrenewal
ORDER  BY #orderby";
                string qsalesofficerid = "";
                string qdatakotor = "";
                qsalesofficerid = string.IsNullOrEmpty(search) ? " a.salesofficerid = @0 AND " : "";
                qdatakotor = !string.IsNullOrEmpty(search) ? @"UNION ALL 
SELECT *
FROM  (SELECT 1                   AS IsRenewalNonRenNot, 
'Renewable'         AS TaskStatus, 
'Need FU'           AS FollowUpDesc, 
'Data baru diinput' AS FollowUpReason, 
p.Period_To   AS ExpiredDate, 
''                  CustID, 
''                  FollowUpNo, 
NULL                AS LastFollowUpDate, 
NULL                AS NextFollowUpDate, 
0                   AS LastSeqNo, 
ERP.policy_no       AS PolicyNo, 
p.Name_On_Policy    AS ProspectName, 
p.entrydt           AS EntryDate,
1                   AS IsRenewal,
NULL                isHotProspect,
1                   AS followupstatus
FROM [BeyondReport].[AAB].[dbo].[excluded_renewal_policy] ERP 
INNER JOIN [BeyondReport].[AAB].[dbo].policy p 
  ON p.policy_no = ERP.policy_no AND RowStatus=0
 AND p.Status = 'A' AND p.RENEWAL_STATUS = 0
 AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) #2name
     #2chasisno #2engineno #2policyno)y
" : "";
                string qnextfollowupdate = "";
                if (chFollowUpDateChecked)
                {
                    qnextfollowupdate = @" ( nextfollowupdate <= @2 OR nextfollowupdate IS NULL ) ";
                }
                else if (chFollowUpMonthlyChecked)
                {
                    qnextfollowupdate = @" ((MONTH(a.EntryDate) = MONTH(GETDATE()) 
                                            AND YEAR(a.EntryDate) = YEAR(GETDATE()))) ";
                }

                List<int> listStatus = new List<int>();
                List<int> listInfo = new List<int>();

                if (chNeedFUChecked)
                {
                    listStatus.Add(1);
                }
                if (chPotentialChecked)
                {
                    listStatus.Add(2);
                }
                if (chCallLaterChecked)
                {
                    listStatus.Add(3);
                }
                if (chCollectDocChecked)
                {
                    listInfo.Add(56);
                }
                if (chNotDealChecked)
                {
                    listStatus.Add(5);
                }
                if (chSentToSAChecked)
                {
                    listInfo.Add(61);
                }
                if (chPolicyCreatedChecked)
                {
                    listInfo.Add(50);
                    listInfo.Add(51);
                    listInfo.Add(52);
                    listInfo.Add(53);
                    listInfo.Add(54);
                    listInfo.Add(55);
                }
                if (chBackToAOChecked)
                {
                    listInfo.Add(60);
                }
                if (chOrderRejectedChecked)
                {
                    listStatus.Add(14);
                }

                string qfollowup = "";
                qfollowup = @" AND ( followupstatus IN(@3) OR followupinfo IN(@4)) ";
                string qisrenewal = "";
                qisrenewal = @"WHERE {0} {1} ";
                string snew = "";              
                string srenew = "";

                if (chNewChecked)
                {
                    snew = "isrenewal = 0 OR isrenewal IS NULL";
                }
                if (chRenewableChecked)
                {
                    if (chNewChecked)
                    {
                        srenew = "OR isrenewal = 1";
                    }
                    else {
                        srenew = "isrenewal = 1";
                    }
                }
                if (!chNewChecked && !chRenewableChecked)
                {
                    snew = "isrenewal = 2";
                    srenew = "";
                }
                qisrenewal = string.Format(qisrenewal, snew, srenew);
                string qchasisno = "";
                string qengineno = "";
                string qchasisno2 = "";
                string qengineno2 = "";
                string qpolicyno = "";
                string qpolicyno2 = "";
                string qname = "";
                string qname2 = "";
                if (!string.IsNullOrEmpty(search))
                {
                    if (chIsSearchByChasNo)
                    {
                        qchasisno = " AND omv.ChasisNumber = @1 ";
                        qchasisno2 = " AND ERP.Chasis_Number = @1 ";
                    }
                    else if (chIsSearchByEngNo)
                    {
                        qengineno = " AND omv.EngineNumber = @1 ";
                        qengineno2 = " AND ERP.Engine_Number = @1 ";
                    }
                    else if (chIsSearchByPolicyNo)
                    {
                        qpolicyno = "AND os.PolicyNo = @1 ";
                        qpolicyno2 = " AND ERP.Policy_No = @1 ";
                    }
                    else if (chIsSearchByProsName)
                    {
                        //Flag form UI to Search Others ProsName - 0213/URF/2019
                        if (allowSearchOthers == 0) //
                        {
                            qsalesofficerid = " a.salesofficerid = @0 AND ";
                        }

                        search = '%' + search + '%';
                        qname = " AND (pcm.Name LIKE @1 OR pcs.CompanyName LIKE @1)";
                        qname2 = " WHERE Name_On_Policy LIKE @1 ";
                    }
                }

                string qorderby = "";
                if (indexOrderByItem.Equals("Status"))
                {
                    qorderby = @" isHotProspect DESC, IsRenewalNonRenNot ASC , COALESCE(isrenewal,0) ASC, COALESCE(NextFollowUpDate,DATEADD(YEAR,5,GETDATE())) ASC   , (CASE WHEN isrenewal = 1 THEN followupstatus END) ASC, (CASE WHEN isrenewal = 1 AND followupstatus=1 THEN ExpiredDate END) ASC, (CASE WHEN (isrenewal = 0 OR isrenewal IS NULL AND nextfollowupdate is null) OR (isrenewal = 1 AND followupstatus!=1)  THEN EntryDate END) DESC   ";
                }
                if (indexOrderByItem.Equals("Name"))
                {
                    qorderby = "ProspectName";
                }
                if (indexOrderByItem.Equals("EntryDate"))
                {
                    qorderby = "EntryDate";
                }
                if (indexOrderByItem.Equals("LastFollowUpdate"))
                {
                    qorderby = "LastFollowUpdate";
                }
                if (indexOrderByItem.Equals("NextFollowUpdate"))
                {
                    qorderby = "NextFollowUpdate";
                }


                if (!indexOrderByItem.Equals("Status"))
                {
                    if (indexOrderByTypeItem == 0)
                    {
                        qorderby = string.Concat(qorderby, " ASC");
                    }
                    else
                    {
                        qorderby = string.Concat(qorderby, " DESC");
                    }
                }
                query = query.Replace("#datakotor", qdatakotor);
                query = query.Replace("#salesofficerid", qsalesofficerid);
                query = query.Replace("#nextfollowupdate", qnextfollowupdate);
                query = query.Replace("#followup", qfollowup);
                query = query.Replace("#isrenewal", qisrenewal);
                query = query.Replace("#chasisno", qchasisno);
                query = query.Replace("#engineno", qengineno);
                query = query.Replace("#2chasisno", qchasisno2);
                query = query.Replace("#2engineno", qengineno2);
                query = query.Replace("#policyno", qpolicyno);
                query = query.Replace("#2policyno", qpolicyno2);
                query = query.Replace("#name", qname);
                query = query.Replace("#2name", qname2);
                query = query.Replace("#orderby", qorderby);

                //                string querytempaja = @"
                //Union SELECT a.* FROM FollowUp a 
                //                INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode 
                //                WHERE a.SalesOfficerID=@0 AND a.FollowUpName like @1 ORDER BY EntryDate DESC";
                //                query = string.Concat(query, querytempaja);

                string queryprospectLeadList = @";SELECT p.IsHotProspect, 0 AS IsRenewalNonRenNot,CASE WHEN IsRenewal=1 THEN 'Renewable' ELSE 'New' END AS TaskStatus,b.Description AS FollowUpDesc,c.Description AS FollowUpReason,hp.ExpiredDate AS ExpiredDate, a.CustID,a.FollowUpNo,a.LastFollowUpDate,a.NextFollowUpDate,a.LastSeqNo,(SELECT TOP 1 CASE WHEN a.FollowUpStatus=13 OR a.IsRenewal=1 THEN PolicyNo ELSE '' END  FROM OrderSimulation WHERE FollowUpNo=a.FollowUpNo)AS PolicyNo, a.ProspectName,a.EntryDate FROM FollowUp a 
JOIN OrderSimulation os ON os.FollowUpNo = a.FollowUpNo INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode LEFT JOIN FollowUpStatusInfo c ON a.FollowUpInfo=c.InfoCode 
JOIN Quotation.dbo.HistoryPenawaran hp ON hp.OrderNo = os.OrderNo 
INNER JOIN Quotation.dbo.Penawaran p ON p.PolicyId=hp.policyid
WHERE a.SalesOfficerID=@0 AND a.FollowUpStatus=1 AND a.RowStatus = 1
ORDER BY os.EntryDate DESC";

                string queryCountProspectLeadList = @"SELECT COUNT(*) FROM FollowUp a 
JOIN OrderSimulation os ON os.FollowUpNo = a.FollowUpNo INNER JOIN FollowUpStatus b ON a.FollowUpStatus=b.StatusCode LEFT JOIN FollowUpStatusInfo c ON a.FollowUpInfo=c.InfoCode 
JOIN Quotation.dbo.HistoryPenawaran hp ON hp.OrderNo = os.OrderNo 
INNER JOIN Quotation.dbo.Penawaran p ON p.PolicyId=hp.policyid
WHERE a.SalesOfficerID=@0 AND a.FollowUpStatus=1 AND a.RowStatus = 1";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    //salesofficerid = salesofficerid.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", salesofficerid) : salesofficerid;

                    //var result = db.Fetch<dynamic>(query, salesofficerid, search, getFollowUpDate);
                    dynamic result = null;
                    result = db.Page<dynamic>(currentPage, pageSize, query, salesofficerid, search, getFollowUpDate
                        , listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 });

        //                List<dynamic> list = db.Fetch<dynamic>(query, salesofficerid, search, getFollowUpDate
        //, listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 });
        //                PageModel res = new PageModel();
        //                res.CurrentPage = 1;
        //                res.TotalPages = 1;
        //                res.TotalItems = list.Count;
        //                res.ItemsPerPage = list.Count;
        //                res.Items = list;
        //                result = res;


                    #region unused
                    //int countLead = db.ExecuteScalar<int>(queryCountProspectLeadList, salesofficerid);
                    
                    //if (countLead < pageSizeLead)
                    //{
                    //    pageSizeLead = countLead;
                    //}
                    //dynamic prospectLeadListPage = null;
                    //if (pageSizeLead > 0) {
                    //    prospectLeadListPage = db.Page<dynamic>(currentPageLead, pageSizeLead, queryprospectLeadList, salesofficerid, search);                    
                    //}

                    //var prospectLeadList = db.Fetch<dynamic>(queryprospectLeadList, salesofficerid, search);

                    //List<dynamic> historyPenawaranList = new List<dynamic>();
                    //List<dynamic> penawaranList = new List<dynamic>();


//                    for (int fi = 0; fi < prospectLeadList.Count; fi++)
//                    {
//                        string queryhistoryPenawaranList = @";SELECT TOP 1 hp.* FROM Quotation.dbo.HistoryPenawaran hp 
//                                                             INNER JOIN OrderSimulation os ON hp.OrderNo = os.OrderNo WHERE os.FollowUpNo = @0 ORDER BY LastUpdatedTime DESC";
//                        historyPenawaranList.Add(db.FirstOrDefault<dynamic>(queryhistoryPenawaranList, prospectLeadList[fi].FollowUpNo));

//                        string querypenawaranList = @";SELECT * FROM Quotation.dbo.Penawaran WHERE policyID = @0";
//                        penawaranList.Add(db.FirstOrDefault<dynamic>(querypenawaranList, historyPenawaranList[fi].PolicyID));
//                    }
                    #endregion
                    return Json(new { status = true, message = "OK", data = result, /*prospectLeadList = prospectLeadListPage, historyPenawaranList = historyPenawaranList, penawaranList = penawaranList*/ }, Util.jsonSerializerSetting());
                    //return Json(new { status = true, message = "OK", data = prospectLeadList.Concat(result), historyPenawaranList=historyPenawaranList, penawaranList = penawaranList }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetSurveyDays(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string BranchID = form.Get("BranchID");
                List<string> data = MobileRepository.GetSurveyDays(BranchID);
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Error" });
                throw e;
            }
        }

        #endregion
        #endregion

        #region 0198urf2019
        [HttpPost]
        public IHttpActionResult GetSendEmailTemplate(FormDataCollection form) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetEmailTemplate(OrderNo,0);
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }        
        }
        [HttpPost]
        public IHttpActionResult GetButtonEmailTemplate(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetEmailTemplate(OrderNo, 1);
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult SendViaEmail(SendViaEmailParam param)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                if (string.IsNullOrEmpty(param.EmailTo) && string.IsNullOrEmpty(param.Body) && string.IsNullOrEmpty(param.OrderNo))
                {
                    return BadRequest("Input Required");
                }
                A2isCommonResult data = MobileRepository.SendViaEmail(param.EmailTo, param.EmailCC, param.EmailBCC, param.Body, param.Subject, param.ListAtt, param.OrderNo, param.IsAddAtt);
                if (data.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + data.ResultDesc });

                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult SendViaEmailFormData()
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                string EmailTo = System.Web.HttpContext.Current.Request.Form.Get("EmailTo");
                string EmailCC = System.Web.HttpContext.Current.Request.Form.Get("EmailCC");
                string EmailBCC = System.Web.HttpContext.Current.Request.Form.Get("EmailBCC");
                string Body = System.Web.HttpContext.Current.Request.Form.Get("Body");
                string Subject = System.Web.HttpContext.Current.Request.Form.Get("Subject");
                string OrderNo = System.Web.HttpContext.Current.Request.Form.Get("OrderNo");
                var files = HttpContext.Current.Request.Files;
                List<HttpPostedFile> ListRawAtt = new List<HttpPostedFile>();

                for (int i = 0; i < files.Count; i++)
                {
                    string key = files.AllKeys[i];
                    if (key.Contains("Attachment"))
                    {
                        ListRawAtt.Add(files[i]);
                    }
                }
                //HttpPostedFile Attachment1 = HttpContext.Current.Request.Files["Attachment1"];
                //HttpPostedFile Attachment2 = HttpContext.Current.Request.Files["Attachment2"];
                //HttpPostedFile Attachment3 = HttpContext.Current.Request.Files["Attachment3"];
                //HttpPostedFile Attachment4 = HttpContext.Current.Request.Files["Attachment4"];
                //HttpPostedFile Attachment5 = HttpContext.Current.Request.Files["Attachment5"];

                //ListRawAtt.Add(Attachment1); ListRawAtt.Add(Attachment2); ListRawAtt.Add(Attachment3); ListRawAtt.Add(Attachment4); ListRawAtt.Add(Attachment5);
                List<A2isMessagingAttachment> ListAtt = new List<A2isMessagingAttachment>();
                foreach (HttpPostedFile item in ListRawAtt)
                {
                    A2isMessagingAttachment mdl = new A2isMessagingAttachment();
                    mdl.AttachmentDescription = Path.GetFileName(item.FileName);
                    using (var binaryReader = new BinaryReader(item.InputStream))
                    {
                        mdl.FileByte = binaryReader.ReadBytes(item.ContentLength);
                    }
                    mdl.FileContentType = item.ContentType;
                    mdl.FileExtension = System.IO.Path.GetExtension(item.FileName);
                    mdl.Seq = 0;
                }

                A2isCommonResult data = MobileRepository.SendViaEmail(EmailTo, EmailCC, EmailBCC, Body, Subject, ListAtt, OrderNo, false);
                if (data.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + data.ResultDesc });
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }


        [HttpPost]
        public IHttpActionResult GetSendWATemplate(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetTemplate(OrderNo,"SendWA");
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetButtonWATemplate(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetTemplate(OrderNo, "ButtonWA");
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetSendSMSTemplate(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetTemplate(OrderNo, "SendSMS");
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }

        }
        [HttpPost]
        public IHttpActionResult GetButtonSMSTemplate(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetTemplate(OrderNo, "ButtonSMS");
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }

        }
        [HttpPost]
        public IHttpActionResult SendViaSMS(SendSMSModel param)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                if (string.IsNullOrEmpty(param.PhoneNo) && string.IsNullOrEmpty(param.SMSText))
                {
                    return BadRequest("Input Required");
                }

                A2isCommonResult data = MessageController.SendSMS(param.SMSText, param.PhoneNo);
                if (data.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send SMS - " + data.ResultDesc });

                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetPhoneList(FormDataCollection form)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                List<PhoneNoModel> res = AABRepository.GetPhoneList(OrderNo);
                return Json(new { status = true, message = "Success", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult UpdateEmail(FormDataCollection form)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                string OrderNo = form.Get("OrderNo");
                string Email = form.Get("Email");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                MobileRepository.UpdateEmail(OrderNo,Email);
                return Json(new { status = true, message = "Success"});
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult InsertUpdatePhoneNo(InsertUpdatePhoneNoParam form)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                List<PhoneNoModel> res = AABRepository.GetPhoneList(form.OrderNo);
                List<string> s = new List<string>();
                foreach (PhoneNoModel p in res)
                {
                    s.Add(p.NoHp);
                }
                foreach (PhoneNoParam n in form.PhoneNoParam)
                {
                    if (string.IsNullOrEmpty(n.ColumnName) && s.Contains(n.PhoneNo)) {
                        return Json(new { status = true, message = "Success", data = "Phone Number " + n.PhoneNo + " already exist" });
                    }
                }
                AABRepository.InsertUpdatePhoneNo(form);
                res = AABRepository.GetPhoneList(form.OrderNo);
                return Json(new { status = true, message = "Success", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetRelationshipWithInsured()
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                List<dynamic> res = AABRepository.GetRelationshipWithInsured();
                return Json(new { status = true, message = "Success", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetAVAYAUserTelerenewal(FormDataCollection form)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                string SalesOfficerID = form.Get("SalesOfficerID");
                List<dynamic> res = MobileRepository.GetAVAYAUserTelerenewal(SalesOfficerID);
                return Json(new { status = true, message = "Success", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetCallRejectReason()
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                List<dynamic> res = AABRepository.GetCallRejectReason();
                return Json(new { status = true, message = "Success", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        #endregion

        [HttpPost]
        public IHttpActionResult TestPushNotif(FormDataCollection form)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {

                string FollowUpNo = form.Get("FollowUpNo");

                string Editor = form.Get("Editor");
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                string OrderNo = db.ExecuteScalar<string>("SELECT OrderNo FROM Ordersimulation WHERE FollowUpNo=@0", FollowUpNo);
                Otosales.Logic.CommonLogic.CreatePushNotification(Otosales.Models.PushNotificationCategory.FOLLOWUPSTATUSCHANGEDBYOTHERS, OrderNo, Editor);
                   
                    return Json(new { status = true, message = "Success" });
             
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }

    }
}
