﻿using a2is.Framework.DataAccess;
using a2is.Framework.Security;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Configuration;
using System.Web.Http;

namespace Otosales.Controllers.vWeb2
{
    [AllowAnonymous]
    [AutoLoggingReference]
    public class AuthenticationController : OtosalesBaseController
    {
        #region Declare

        private static string GLOBAL_APPLICATION_CODE = WebConfigurationManager.AppSettings["GlobalApplicationCode"];
        private static string GLOBAL_SERVICE_ADDRESS = WebConfigurationManager.AppSettings["GlobalServiceAddress"];

        private static string JWT_SIGNING_KEY = WebConfigurationManager.AppSettings["JWTSigningKey"];
        private static string JWT_VALID_HOURS = WebConfigurationManager.AppSettings["JWTValidHours"];

        private static bool ALLOW_UNTRUSTED_SSL = Int32.Parse(WebConfigurationManager.AppSettings["allowUntrustedSSL"]) == 1 ? true : false;

        private static int CMS_APPLICATION_ID = Int32.Parse(WebConfigurationManager.AppSettings["GardaMobileCMSApplicationID"]);

        #endregion

        #region call globalAPI

        private GlobalAPIResult callGlobalAPI(string endpoint, KeyValuePair<string, string>[] param)
        {
            using (var handler = new WebRequestHandler())
            {
                if (ALLOW_UNTRUSTED_SSL) handler.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(GLOBAL_SERVICE_ADDRESS);
                    var response = client.PostAsync(endpoint, new FormUrlEncodedContent(param)).Result;

                    response.Headers.Remove("Access-Control-Allow-Origin");
                    response.Headers.Remove("Access-Control-Allow-Headers");
                    response.Headers.Remove("Access-Control-Allow-Methods");

                    return JsonConvert.DeserializeObject<GlobalAPIResult>(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        #endregion

        #region Test
        [HttpGet]
        public IHttpActionResult Test()
        {
            return Ok(new DateTime());
        }

        [HttpGet]
        public IHttpActionResult TestLogin(string user, string password)
        {
            return Login(new FormDataCollection("userID="+user+"&password="+password));
        }

        #endregion

        #region Login
        [HttpPost]
        public IHttpActionResult Login2(FormDataCollection form)
        {
            UserInfos us = new UserInfos();

            LoginResult<UserPrivilege> loginResult = new LoginResult<UserPrivilege>();
            

            loginResult.IsAuthenticated = true;
            loginResult.Status = "OK";

            UserPrivilege userPrivilege = new UserPrivilege();
            userPrivilege.Region = "DKI";
            userPrivilege.ReportTo = "FARDELIA@ASURANSIASTRA.COM";

                FormAccess formAccess = new FormAccess();
                formAccess.FormID = "APPROVALORDER";
                formAccess.CanRead = false;
                formAccess.CanWrite = false;
                formAccess.CanAdd = true;
                formAccess.CanDelete =true ;
                userPrivilege.FormAccessCollections.Add(formAccess);

                 formAccess = new FormAccess();
                 formAccess.FormID = "ORDERSIMULATION";
                formAccess.CanRead = false;
                formAccess.CanWrite = false;
                formAccess.CanAdd = true;
                formAccess.CanDelete = true;
                userPrivilege.FormAccessCollections.Add(formAccess);
                
               formAccess = new FormAccess();
               formAccess.FormID = "DASHBOARD";
                formAccess.CanWrite = false;
                formAccess.CanAdd = true;
                formAccess.CanDelete = true;
                userPrivilege.FormAccessCollections.Add(formAccess);
                
              formAccess = new FormAccess();
                formAccess.FormID = "INFO";
                formAccess.CanRead = false;
                formAccess.CanWrite = false;
                formAccess.CanAdd = true;
                formAccess.CanDelete = true;
                userPrivilege.FormAccessCollections.Add(formAccess);

                formAccess = new FormAccess();
                formAccess.FormID = "ORDERAPPROVAL";
                formAccess.CanRead = false;
                formAccess.CanWrite = false;
                formAccess.CanAdd = true;
                formAccess.CanDelete = true;
                userPrivilege.FormAccessCollections.Add(formAccess);
            
            SalesOfficer so = new SalesOfficer();
            so.BranchCode = "023";//"039";
            so.Channel = "INT";
            so.ChannelSource = "AAB";
            so.Class = "";
            so.Department = "";
            so.Email = "hyo@beyond.asuransi.astra.co.id";
            so.EntryDate = Convert.ToDateTime("2015-07-09 09:39:22.657");
            so.Expiry = Convert.ToDateTime("9999-07-09 09:39:22.657");
            so.Ext = "";
           // so.ExtUserID = "";
            so.Fax = "";
            //so.Key = "";
            so.LastUpdatedTime = Convert.ToDateTime("2015-07-09 09:39:22.657");
            so.Name = "HAYO";
            so.OfficePhone = "";
            so.Password = "";
            so.Password_Expiry = Convert.ToDateTime("9999-07-09 09:39:22.657");
            so.Phone1 = "";
            so.Phone2 = "";
            so.Phone3 = "";
            so.Role = "NATIONALMGR";
            so.RowStatus = true;
            so.SalesOfficerID = "hyo";
            so.Validation = "";
            userPrivilege.User = so; 
            loginResult.UserInfo = userPrivilege;
            loginResult.Token = "sFPkWRX9bU0Ijpm/iEaO08jBUW+LE2WRrZZpYQTX9L9iOH4X+frarFCC99/SCZGsbp6rt9ThJbypWFnpOUbkqSYy1ZF3V8WxpRUbTz+W6skzA+DLVNQ1t5MT8ZUAv+NdEnXXkHLdEVxjUDDmv9cLkszXqFBKML7Tn9PHorCX5W/+qK/asu2ZUe00ZH1eXsvGLMYSHR/QT6aHkoaY9o/O0dfL0Hp97k8WMxuOsBuwI3UaGb+nL6LPNhp09yoZCtoT";
            loginResult.ValidHour = 172;
 
            return Json(loginResult, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult Login(FormDataCollection form)
        {
            string userID = form.Get("userID");
            string password = form.Get("password");

            LoginResult<UserPrivilege> loginResult = new LoginResult<UserPrivilege>();

            GlobalAPIResult globalApiResult = callGlobalAPI("account/login", new[]{
                            new KeyValuePair<string,string> ("username", userID.Trim()),
                            new KeyValuePair<string,string> ("password", password),
                            new KeyValuePair<string,string> ("applicationCode", GLOBAL_APPLICATION_CODE)
                        });

            if (globalApiResult.status)
            {
                string Name = globalApiResult.userInfo.FullName;
                string SalesOfficerId = globalApiResult.userInfo.ID;
                string Department = globalApiResult.parameters.Department;
                string BranchCode = globalApiResult.parameters.BranchCode;
                string Phone1 = globalApiResult.parameters.Phone1;
                string Phone2 = globalApiResult.parameters.Phone2;
                string Phone3 = globalApiResult.parameters.Phone3;
                string OfficePhone = globalApiResult.parameters.OfficePhone;
                string Ext = globalApiResult.parameters.Ext;
                string Fax = globalApiResult.parameters.Fax;
                // 0261/URF/2015 - BSY
                string ExtUserID = globalApiResult.parameters.ExtUserID;
                string Channel = globalApiResult.parameters.Channel;
                string ChannelSource = globalApiResult.parameters.ChannelSource;
                var aabDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var aabmobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                //TODO : Role ID CSO ga boleh hardcode.!! pake table parameter.
                string roleidcso = aabmobileDB.ExecuteScalar<string>(@"select paramvalue from dbo.ApplicationParameters where paramname='LISTROLEIDCSO'");
                bool IsCSO = aabDB.ExecuteScalar<int>(@"SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[UserApplicationRoles] WHERE UserID=@0 AND RoleID IN ("+roleidcso+") AND ApplicationId=3", SalesOfficerId) > 0 ? true : false;
                dynamic so = aabmobileDB.Fetch<dynamic>("SELECT TOP 1 SalesOfficerID, Name, BranchCode, Phone1, Phone2, Phone3, OfficePhone, Ext, Fax FROM SalesOfficer WHERE 1=IIF(LEN(@0) = 3,IIF(SalesOfficerID = @0,1,0),IIF(Email=@0,1,0))", userID.Trim()).FirstOrDefault();
                if (so != null)
                {
                    BranchCode = so.BranchCode;
                    Phone1 = so.Phone1;
                    Phone2 = so.Phone2;
                    Phone3 = so.Phone3;
                    OfficePhone = so.OfficePhone;
                    Ext = so.Ext;
                    Fax = so.Fax;
                    //for agency
                    if (userID.Trim().Length > 3)
                    {
                        Name = so.Name;
                        ExtUserID = so.SalesOfficerID;
                        SalesOfficerId = so.SalesOfficerID;
                        Channel = "EXT";
                        ChannelSource = "AABAGT";
                    }
                }
                SalesOfficer user = new SalesOfficer()
                {
                    SalesOfficerID = SalesOfficerId,
                    Email = globalApiResult.userInfo.Email,
                    Name = Name,
                    Role = globalApiResult.menu[0].RoleCode,
                    Class = string.Empty,
                    Validation = string.Empty,
                    Expiry = globalApiResult.userInfo.PasswordExpiry,
                    Password_Expiry = globalApiResult.userInfo.PasswordExpiry,
                    Department = Department,
                    BranchCode = BranchCode,
                    Phone1 = Phone1,
                    Phone2 = Phone2,
                    Phone3 = Phone3,
                    OfficePhone = OfficePhone,
                    Ext = Ext,
                    Fax = Fax,
                    // 0261/URF/2015 - BSY
                    ExtUserID = ExtUserID,
                    Channel = Channel,
                    ChannelSource = ChannelSource,
                    // 0089/URF/2019 -FHK
                    IsCSO = IsCSO,
                    // end
                    RowStatus = true,
                    EntryDate = globalApiResult.EntryDate,
                    LastUpdatedTime = globalApiResult.LastUpdatedTime
                };

                loginResult.IsAuthenticated = true;
                loginResult.Status = "OK";

                UserPrivilege userPrivilege = new UserPrivilege();
                userPrivilege.Region = globalApiResult.parameters.Region;
                userPrivilege.ReportTo = globalApiResult.parameters.ReportTo;

                foreach (Menu m in globalApiResult.menu)
                {
                    FormAccess formAccess = new FormAccess();
                    formAccess.FormID = m.Name;
                    formAccess.CanRead = m.CanRead != null ? (bool)m.CanRead : false;
                    formAccess.CanWrite = m.CanWrite != null ? (bool)m.CanWrite : false;
                    formAccess.CanAdd = m.CanAdd != null ? (bool)m.CanAdd : false;
                    formAccess.CanDelete = m.CanDelete != null ? (bool)m.CanDelete : false;
                    userPrivilege.FormAccessCollections.Add(formAccess);
                }

                userPrivilege.User = user;
                loginResult.UserInfo = userPrivilege;
                loginResult.Token = globalApiResult.token;
                loginResult.ValidHour = globalApiResult.ValidHours;

                #region Save Log in User
                String query = "delete from [dbo].[LoggedInUser] where UserName = @0";
                aabmobileDB.Execute(query, userID);

                //sql = "insert into [LoggedInUser] values (NEWID(),'" + param.UserName + "','" + resultContent.token + "','" + resultContent.EntryDate.Value.ToString("yyyy/MM/dd HH:mm:ss") + "','" + resultContent.EntryDate.Value.AddHours(resultContent.ValidHours).ToString("yyyy/MM/dd HH:mm:ss") + "')";
                DateTime createdDate = DateTime.Now;

                query = "insert into [LoggedInUser](ID,UserName,Token,CreatedDate,ExpiredDate) values (NEWID(), @0, @1, GETDATE(),@2)";
                aabmobileDB.Execute(query, userID,
                                        globalApiResult.token,
                                        createdDate.AddHours(globalApiResult.ValidHours).ToString("yyyy/MM/dd HH:mm:ss"));
                #endregion

            }
            else
            {
                loginResult.IsAuthenticated = false;
                loginResult.Status = globalApiResult.ErrorCode == null ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode]; ;
                loginResult.UserInfo = null;
                loginResult.ValidHour = 0;
            }

            return Json(loginResult, Util.jsonSerializerSetting());
        }

        #endregion

        #region External User

        [HttpPost]
        public IHttpActionResult AccountActivation(FormDataCollection form)
        {
            string username = form.Get("username");
            string password = form.Get("password");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/activate", new[]{
                    new KeyValuePair<string,string> ("username", username),
                    new KeyValuePair<string,string> ("password", password),
                    new KeyValuePair<string,string> ("deliverychanneltype", "email"),
                    new KeyValuePair<string,string> ("applicationCode", GLOBAL_APPLICATION_CODE)
                });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ConfirmAccountActivation(FormDataCollection form)
        {
            string username = form.Get("username");
            string token = form.Get("token");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/confirmactivation", new[] {
                new KeyValuePair<string, string> ("username", username),
                new KeyValuePair<string, string> ("token", token)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ForgotPassword(FormDataCollection form)
        {
            string username = form.Get("username");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/forgotpassword", new[] {
                new KeyValuePair<string, string> ("username", username)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ConfirmForgotPassword(FormDataCollection form)
        {
            string username = form.Get("username");
            string token = form.Get("token");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/confirmtokenforgotpassword", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("token", token)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult UpdatePassword(FormDataCollection form)
        {
            string username = form.Get("username");
            string password = form.Get("password");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/updatepassword", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        public IHttpActionResult ResendTokenActivation(FormDataCollection form)
        {
            string username = form.Get("username");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/resendtoken", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("eventtype", "1")
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        public IHttpActionResult ResendTokenForgotPassword(FormDataCollection form)
        {
            string username = form.Get("username");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/resendtoken", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("eventtype", "2")
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ChangePassword(FormDataCollection form)
        {
            string username = form.Get("username");
            string password = form.Get("password");
            string oldpassword = form.Get("oldpassword");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/changepassword", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("oldpassword", oldpassword)
            });


            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        #endregion

        #region GCM

        [HttpPost]
        public IHttpActionResult RegisterDevice(FormDataCollection form)
        {
            string DeviceToken = form.Get("DeviceToken");
            int DeviceType = Int32.Parse(form.Get("DeviceType"));
            string UniqueID = form.Get("UniqueID");
            string AppVersion = form.Get("AppVersion");

            OtosalesAPIResult result = new OtosalesAPIResult();

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAMOBILECMS))
            {
                string sql = @";EXECUTE usp_AddDevice @0, @1, @2, @3, @4";
                //@DeviceToken AS VARCHAR(250), @DeviceType AS INT, @AppID AS INT, @ClientUniqueID AS VARCHAR(250), @AppVersion AS VARCHAR(10)
                List<Device> device = db.Fetch<Device>(sql, DeviceToken, DeviceType, CMS_APPLICATION_ID, UniqueID, AppVersion);

                if (device.Count > 0)
                {
                    result.Status = true;
                    result.Data = device[0].DeviceID;
                }
            }

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult RegisterUserDevice(FormDataCollection form)
        {
            Int64 DeviceID = Int64.Parse(form.Get("DeviceID"));
            string UserID = form.Get("UserID");

            OtosalesAPIResult result = new OtosalesAPIResult();
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAMOBILECMS))
            {
                string sql = @";EXECUTE usp_AddUserDevice @0, @1, @2";
                //@DeviceID AS BIGINT, @AppID AS INT, @UserID as varchar(100)
                List<dynamic> userDevice = db.Fetch<dynamic>(sql, DeviceID, CMS_APPLICATION_ID, UserID);

                result.ResponseCode = userDevice[0].Code;
                result.Message = userDevice[0].Message;

                if (userDevice[0].Code.Equals("200")) {
                    result.Status = true;
                }
                else
                {
                    result.Status = false;
                }
            }

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult LogoutTokenGCM(FormDataCollection form)
        {
            string DeviceID = form.Get("DeviceID");

            OtosalesAPIResult result = new OtosalesAPIResult();

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAMOBILECMS))
            {
                string query = "UPDATE UserDevice SET Status = 0 where DeviceID = @0";
                db.Execute(query, DeviceID);
            }

            result.Status = true;
            return Json(result, Util.jsonSerializerSetting());
        }

        #endregion


        #region Validate Token
        public static string ValidateToken(string token)
        {
            
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string qGetLoggedInUser = "select top 1 UserName from [dbo].[LoggedInUser] where Token = @0 and ExpiredDate > GETDATE()";
                    string username = db.ExecuteScalar<string>(qGetLoggedInUser, token);
                    return username;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
