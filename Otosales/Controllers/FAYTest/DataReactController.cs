﻿using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Repository.FAYTest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Otosales.Controllers.FAYTest
{
    [AutoLoggingReference]
    public class DataReactController : OtosalesBaseController
    {

        private static readonly a2isLogHelper _log = new a2isLogHelper();

        [HttpPost]
        public IHttpActionResult getTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                #endregion
                Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
                try
                {
                    result = MobileRepository.GetTaksListDetail(CustID, FollowUpNo);
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskDetailDocument(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                string typeDocument = form.Get("Type");
                #endregion
                dynamic result = null;

                if (string.IsNullOrEmpty(CustID) || string.IsNullOrEmpty(FollowUpNo) || string.IsNullOrEmpty(typeDocument))
                {
                    return BadRequest("Bad Request!");
                }
                try
                {
                    switch (typeDocument)
                    {
                        case "PERSONAL":
                            result = MobileRepository.GetTaksListDetailDocumentPersonal(CustID, FollowUpNo);
                            break;
                        case "COMPANY":
                            result = MobileRepository.GetTaksListDetailDocumentCompany(CustID, FollowUpNo);
                            break;
                        case "PROSPECT":
                            result = MobileRepository.GetTaksListDetailDocumentAll(CustID, FollowUpNo);
                            break;
                        default:
                            return BadRequest("Bad Type Cuy!");
                    }
                                        
                    return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateTaskDetail(Models.v0213URF2019.UpdateTaskDetailParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string CustID = param.CustID;
            string FollowUpNo = param.FollowUpNo;
            Models.vWeb2.PersonalData PersonalData = param.PersonalData;
            Models.vWeb2.CompanyData CompanyData = param.CompanyData;
            Models.vWeb2.VehicleData VehicleData = param.VehicleData;
            Models.vWeb2.PolicyAddress policyAddress = param.policyAddress;
            Models.vWeb2.SurveySchedule surveySchedule = param.surveySchedule;
            List<Models.vWeb2.ImageDataTaskDetail> imageData = param.imageData;
            Models.vWeb2.OrderSimulationModel OSData = param.OSData;
            Models.vWeb2.OrderSimulationMVModel OSMVData = param.OSMVData;
            Models.vWeb2.CalculatePremiModel calculatedPremiItems = param.calculatedPremiItems;
            string FollowUpStatus = param.FollowUpStatus;
            Models.vWeb2.Remarks Remarks = param.Remarks;
            #endregion

            if (string.IsNullOrEmpty(param.CustID) || string.IsNullOrEmpty(param.FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            string OrderNo = "";
            try
            {
                OrderNo = MobileRepository.UpdateTaskListDetail(CustID, FollowUpNo, PersonalData,
                    CompanyData, VehicleData, policyAddress, surveySchedule, imageData, OSData, OSMVData,
                    calculatedPremiItems, FollowUpStatus, Remarks);
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", data = new { OrderNo, PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo() } });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

    }
}
