﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Models;
using Otosales.Models.Xoom;
using Otosales.Models.Xoom.Contract;
using Otosales.Models.Xoom.Database;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Otosales.Controllers.XoomFileTransfer
{
    public class UploadController : ApiController
    {
        #region Declare

        private static readonly a2isLogHelper logger = new a2isLogHelper();

        #endregion

        [HttpPost]
        public IHttpActionResult Registration([FromBody] FileUploadRegistrationModel form)
        {
            try
            {
                GeneralResult GR = new GeneralResult();

                DateTime dtNow = DateTime.Now;
                string uid = Guid.NewGuid().ToString("N") + dtNow.ToString("-yyyyMMdd-HHmmss");

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_XOOM))
                {
                    db.Execute("INSERT INTO MultipartDataFileHeader VALUES (@0, @1, @2, @3, @4, @5, @6, @7)", uid, form.UserFileID, form.Name, form.Extension, form.PartCount, form.OSSender, dtNow, form.AdditionalInfo);

                    GR.isSuccess = true;
                    GR.result = uid;
                }

                return Ok(JsonConvert.SerializeObject(GR));
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "UploadController", "Registration", ex.Message.ToString());

                GeneralResult GR = new GeneralResult();
                GR.errorCode = 500;
                GR.errorMessage = "Server Error";
                return Ok(JsonConvert.SerializeObject(GR));
            }
        }

        [HttpPost]
        public IHttpActionResult UploadFile([FromBody] FileUploadModel form)
        {
            try
            {
                GeneralResult GR = new GeneralResult();

                using(var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_XOOM))
                {
                    if(db.ExecuteScalar<int>("SELECT COUNT(*) FROM MultipartDataFileHeader WHERE ID=@0", form.ID) != 1)
                    {
                        GR = ErrorBuilder.cannotFindID(GR);
                    }
                    else
                    {
                        if (db.ExecuteScalar<int>("SELECT COUNT(*) FROM MultipartDataFileDetail WHERE ID=@0 AND PartNumber=@1", form.ID, form.PartNumber) != 1)
                        {
                            db.Execute("INSERT INTO MultipartDataFileDetail VALUES (@0, @1, @2, @3)", form.ID, form.PartNumber, form.Data, DateTime.Now);
                        }
                        GR.isSuccess = true;

                        new Thread(() => JoinFilePart(form.ID)).Start();
                    }
                }

                return Ok(JsonConvert.SerializeObject(GR));
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "UploadController", "UploadFile", ex.Message.ToString());
                
                GeneralResult GR = new GeneralResult();
                GR.errorCode = 500;
                GR.errorMessage = "Server Error";
                return Ok(JsonConvert.SerializeObject(GR));
            }
        }

        private void JoinFilePart(string id)
        {
            try {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_XOOM))
                {
                    MultipartDataFileHeader header = db.First<MultipartDataFileHeader>("SELECT * FROM MultipartDataFileHeader WHERE ID=@0", id);
                    List<MultipartDataFileDetail> details = db.Fetch<MultipartDataFileDetail>("SELECT * FROM MultipartDataFileDetail WHERE ID=@0 ORDER BY PartNumber", id);

                    if(details.Count == header.PartCount)
                    {
                        int n = details.Count;
                        byte[][] datas = new byte[n][];
                        Parallel.For(0, n, index =>
                        {
                            string[] _arr = details[index].Data.Split(',');
                            sbyte[] _sbyte = Array.ConvertAll(_arr, sbyte.Parse);
                            int _n = _sbyte.Length;
                            byte[] _data = new byte[_n];
                            for (int i = 0; i < _n; i++)
                                _data[i] = (byte)_sbyte[i];
                            datas[index] = _data;
                        });

                        int x = 0;
                        for (int i = 0; i < n; i++)
                            x += datas[i].Length;
                        byte[] data = new byte[x];
                        int current = 0;
                        for (int i = 0; i < n; i++)
                        {
                            int z = datas[i].Length;
                            for (int j = 0; j < z; j++)
                            {
                                data[current] = datas[i][j];
                                current++;
                            }
                        }

                        // save to DataFileMaster XOOM
                        db.Execute("INSERT INTO DataFileMaster VALUES (@0, @1, @2, @3, @4, @5, @6)", header.ID, header.UserFileID, header.Name, header.Extension, header.CreateDate, data, header.AdditionalInfo);

                        // delete unecessary data
                        db.Execute("DELETE FROM MultipartDataFileDetail WHERE ID=@0", header.ID);
                        db.Execute("DELETE FROM MultipartDataFileHeader WHERE ID=@0", header.ID);

                        // Save to ImageData
                        using (var dbmbl = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                        {
                            // get detail info from data header
                            ImageData imageData = JsonConvert.DeserializeObject<ImageData>(header.AdditionalInfo);

                            // compress image for thumbnail
                            byte[] thumbnailData = ResizeImage2(data);

                            // save to ImageData in AABMobile
                            dbmbl.Execute("INSERT INTO ImageData (ImageID,DeviceID,SalesOfficerID,PathFile,EntryDate,LastUpdatedTime,RowStatus, Data, ThumbnailData) VALUES (@0, @1, @2, @3, @4, getdate(), 1, @5, @6)", imageData.ImageID, imageData.DeviceID, imageData.SalesOfficerID, imageData.PathFile, imageData.EntryDate, data, thumbnailData);
                        }
                    }
                }
            } catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "UploadController", "JoinFilePart", ex.Message.ToString());
            }
        }

        #region Resize Image

        private byte[] ResizeImage2(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                ms = new MemoryStream(data);
                System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
                ms = GetThumbnailImage(img);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "UploadController", "ResizeImage2", ex.Message.ToString());
            }
            return ms.ToArray();
        }

        private MemoryStream GetThumbnailImage(System.Drawing.Image img)
        {
            float iScale = img.Height > img.Width ? (float)img.Height / 100 : (float)img.Width / 100;
            img = img.GetThumbnailImage((int)(img.Width / iScale), (int)(img.Height / iScale), null, IntPtr.Zero);
            MemoryStream memStream = new MemoryStream();
            img.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            return memStream;
        }

        #endregion
    }
}
