﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Models;
using Otosales.Models.Xoom;
using Otosales.Models.Xoom.Contract;
using Otosales.Properties;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Web.Http;

namespace Otosales.Controllers.XoomFileTransfer
{
    public class DownloadController : ApiController
    {
        #region Declare

        private static string BASE_IMAGE_PATH = Settings.Default.BaseImagePath;
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        #endregion

        [HttpPost]
        public IHttpActionResult GetPart([FromBody] FileDownloadModel form) {
            try
            {
                GeneralResult GR = new GeneralResult();
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    int count = db.ExecuteScalar<int>("SELECT COUNT(*) FROM ImageData WHERE ImageID=@0 AND Data IS NOT NULL", form.UserFileID);

                    if (count == 0)
                    {
                        // if image not exist in imagedata, get data from folder
                        byte[] bytes = getBytesByFileName(form.UserFileID);

                        if(bytes == null)
                        {
                            // if file not found, soft delete image in ImageData Table
                            db.Execute("UPDATE ImageData set RowStatus = 0 WHERE ImageID=@0", @form.UserFileID);
                            GR = ErrorBuilder.cannotFindData(GR);
                        }
                        else
                        {
                            // divide data into some parts
                            int nPart = bytes.Length / form.PartLength;
                            int mod = bytes.Length % form.PartLength;
                            int nPartMod = mod == 0 ? 0 : 1;
                            if (form.PartRequest > (nPart + nPartMod))
                            {
                                GR = ErrorBuilder.partOutOfRange(GR);
                            }
                            else
                            {
                                int indexStart = 0;
                                int indexFinish = 0;

                                if (form.PartRequest > nPart)
                                {
                                    indexStart = form.PartLength * nPart;
                                    indexFinish = indexStart + mod;
                                }
                                else
                                {
                                    indexStart = form.PartLength * (form.PartRequest - 1);
                                    indexFinish = form.PartLength * form.PartRequest;
                                }

                                StringBuilder sb = new StringBuilder();
                                for (int i = indexStart; i < indexFinish; i++)
                                {
                                    sb.Append((sbyte)bytes[i]);
                                    if (i + 1 != indexFinish)
                                        sb.Append(",");
                                }
                                string data = sb.ToString();

                                GR.isSuccess = true;
                                GR.result = data;
                            }
                        }
                    }
                    else
                    {
                        // if data exist in imagedata
                        byte[] bytes = db.ExecuteScalar<byte[]>("SELECT TOP 1 Data FROM ImageData WHERE ImageID=@0", form.UserFileID);

                        int nPart = bytes.Length / form.PartLength;
                        int mod = bytes.Length % form.PartLength;
                        int nPartMod = mod == 0 ? 0 : 1;
                        if (form.PartRequest > (nPart + nPartMod))
                        {
                            GR = ErrorBuilder.partOutOfRange(GR);
                        }
                        else
                        {
                            int indexStart = 0;
                            int indexFinish = 0;

                            if (form.PartRequest > nPart)
                            {
                                indexStart = form.PartLength * nPart;
                                indexFinish = indexStart + mod;
                            }
                            else
                            {
                                indexStart = form.PartLength * (form.PartRequest - 1);
                                indexFinish = form.PartLength * form.PartRequest;
                            }

                            StringBuilder sb = new StringBuilder();
                            for (int i = indexStart; i < indexFinish; i++)
                            {
                                sb.Append((sbyte)bytes[i]);
                                if (i + 1 != indexFinish)
                                    sb.Append(",");
                            }
                            string data = sb.ToString();

                            GR.isSuccess = true;
                            GR.result = data;
                        }
                    }
                }
                return Ok(JsonConvert.SerializeObject(GR));
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "DownloadController", "GetPart", ex.Message.ToString());

                GeneralResult GR = new GeneralResult();
                GR = new GeneralResult();
                GR.errorMessage = ex.Message;
                GR.errorCode = 105;
                return Ok(JsonConvert.SerializeObject(GR));
            }
        }

        #region get Image Bytes

        private byte[] getBytesByFileName(String fileName)
        {
            try
            {
                using(var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    ImageData data = db.First<ImageData>("SELECT * FROM ImageData WHERE ImageId=@0", fileName);
                    Image image = Image.FromFile(BASE_IMAGE_PATH + data.SalesOfficerID + "\\" + data.PathFile);
                    MemoryStream ms = new MemoryStream();
                    image.Save(ms, ImageFormat.Jpeg);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "DownloadController", "getBytesByFileName", ex.Message.ToString());
            }
            return null;
        }

        #endregion
    }
}
