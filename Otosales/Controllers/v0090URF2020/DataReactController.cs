﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using Otosales.Repository.v0090URF2020;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace Otosales.Controllers.v0090URF2020
{
    public class DataReactController : OtosalesBaseController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();
        #region Properties
        IMobileRepository mobileRepository;
        IAABRepository aabRepository;

        public DataReactController()
        {
            mobileRepository = new MobileRepository();
            aabRepository = new AABRepository();
        }
        public DataReactController(IMobileRepository repository)
        {
            mobileRepository = repository;
        }
        public DataReactController(IAABRepository repository) {
            aabRepository = repository;
        }
        #endregion
        [HttpPost]
        public IHttpActionResult GenerateShortenLinkPaymentOtosales(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            string orderNo = form.Get("OrderNo").Trim();
            string IsInstallment = form.Get("IsInstallment");
            try
            {                
                if (string.IsNullOrEmpty(orderNo) || string.IsNullOrEmpty(IsInstallment))
                {
                    return BadRequest("Param Required");
                }
                bool InstallmentF = Convert.ToBoolean(IsInstallment);
                dynamic data = mobileRepository.GenerateLinkPayment(orderNo, InstallmentF);

                return Json(new { status = true, message = "OK", data }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                mobileRepository.RollBackGenerateLinkPayment(orderNo);
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetApprovalPaymentStatus(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            string orderNo = form.Get("OrderNo").Trim();
            try
            {
                if (string.IsNullOrEmpty(orderNo))
                {
                    return BadRequest("Param Required");
                }
                dynamic data = mobileRepository.GetApprovalPaymentStatus(orderNo);

                return Json(new { status = true, message = "OK", data }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult populateOrderApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            Models.OtosalesAPIResult response = new Models.OtosalesAPIResult();
            try
            {
                string userID = form.Get("UserID");
                string ReqType = form.Get("ReqType");
                if (userID.Equals(null) || userID.Equals(""))
                {
                    response.Status = false;
                    response.Message = "User cannot be Empty!";
                    response.ResponseCode = "6661";
                }
                else
                {
                    int reqType = 1;
                    if (!string.IsNullOrEmpty(ReqType)) {
                        reqType = Convert.ToInt32(ReqType);
                    }
                    response = aabRepository.PopulateOrderApproval(userID, reqType);
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = ex.Message.ToString();
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult populatePaymentApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            Models.OtosalesAPIResult response = new Models.OtosalesAPIResult();
            try
            {
                string userID = form.Get("UserID");
                string ReqType = form.Get("ReqType");
                if (userID.Equals(null) || userID.Equals(""))
                {
                    response.Status = false;
                    response.Message = "User cannot be Empty!";
                    response.ResponseCode = "6661";
                }
                else
                {
                    int reqType = 1;
                    if (!string.IsNullOrEmpty(ReqType))
                    {
                        reqType = Convert.ToInt32(ReqType);
                    }
                    response = mobileRepository.PopulateOrderMobileApproval(userID, reqType);
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = ex.Message.ToString();
            }
            return Json(response, Util.jsonSerializerSetting());
        }
        [HttpPost]
        public IHttpActionResult ProcessApprovalPayment(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            string OrderNo = form.Get("OrderNo");
            string UserId = form.Get("UserId");
            string ApprovalType = form.Get("ApprovalType");
            string ActionType = form.Get("ActionType");
            /* 1 Approve
               2 Reject/Revise */
            try
            {
                if (string.IsNullOrEmpty(OrderNo) || string.IsNullOrEmpty(UserId) 
                    || string.IsNullOrEmpty(ApprovalType) || string.IsNullOrEmpty(ActionType))
                {
                    return BadRequest("Param Required");
                }
                string data = mobileRepository.ProcessApprovalPayment(OrderNo, UserId, ApprovalType, ActionType);

                return Json(new { status = true, message = "Success", data }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult fetchDetailApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            Models.OtosalesAPIResult response = new Models.OtosalesAPIResult();
            try
            {
                string OrderID = form.Get("OrderID");
                string OrderNo = form.Get("OrderNo");
                string ApprovalType = form.Get("ApprovalType");
                string Role = form.Get("Role");
                response.ResponseCode = "1";
                response.Message = "Success!";
                switch (ApprovalType)
                {
                    case "COMSAMD":
                        {
                            response = aabRepository.detailApprovalComission(OrderID, Role, OrderNo);
                        }
                        break;
                    case "KOMISI":
                        {
                            response = aabRepository.detailApprovalComission(OrderID, Role, OrderNo);
                        }
                        break;
                    case "ADJUST":
                        {
                            response = aabRepository.detailApprovalAdjustment(OrderID, OrderNo);
                        }
                        break;
                    case "NEXTLMT":
                        {
                            response = aabRepository.detailApprovalTsiLimit(OrderID, OrderNo);
                        }
                        break;
                    case "PMT1":
                        {
                            response = aabRepository.detailApprovalPayment(OrderID, OrderNo);
                        }
                        break;
                    case "PMT2":
                        {
                            response = aabRepository.detailApprovalPayment(OrderID, OrderNo);
                        }
                        break;
                    default:
                        {
                            response.Status = false;
                            response.Message = "Unknown Order Type";
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = ex.Message;
                _log.Error("Function " + actionName + " Error : " + ex.ToString());
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult Penawaran(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OtosalesAPIResult response = new OtosalesAPIResult();
            string userID = form.Get("userID");
            if (string.IsNullOrEmpty(userID)) {
                response.Status = false;
                response.Message = "Param can't be null";
                return Json(response, Util.jsonSerializerSetting());
            }
            try
            {

                Util.CheckAllMGOBidExpiry();
                response = mobileRepository.GetPenawaranList(userID);
            }
            catch (Exception e)
            {
                response.Status = false;
                response.Message = "Unable to process your input please try again or contact administrator";
                response.Data = e.ToString();
                _log.Error("Function " + actionName + " Error : " + e.ToString());
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult InsertUpdatePhoneNo(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string query = @"";
                query = @";EXEC [dbo].[usp_GetPhoneList] @0";
                List<Otosales.Models.vWeb2.PhoneNoModel> ListPhoneNo = aabdb.Fetch<Otosales.Models.vWeb2.PhoneNoModel>(query, form.OrderNo);
                string oldPolicyNo = db.ExecuteScalar<string>(@";
DECLARE @@OldPolicyNo VARCHAR(20) = ''
SELECT @@OldPolicyNo=OldPolicyNo FROM dbo.OrderSimulation WHERE OrderNo = @0 AND ApplyF = 1 AND RowStatus = 1
SELECT @@OldPolicyNo", form.OrderNo);
                string segmentID = db.ExecuteScalar<string>(@";
DECLARE @@SegmentCode VARCHAR(20) = ''
SELECT @@SegmentCode=SegmentCode FROM dbo.OrderSimulation WHERE OrderNo = @0 AND ApplyF = 1 AND RowStatus = 1
SELECT @@SegmentCode", form.OrderNo);
                string qgetCustID = @";
DECLARE @@CustID CHAR(11) = ''
SELECT 
@@CustID = CASE WHEN @0 = 'P3A200' OR @0 = 'P3A500'
			THEN Cust_Id
			ELSE Policy_Holder_Code
		END
FROM dbo.Policy WHERE Policy_No = @1
SELECT @@CustID";
                string custid = string.IsNullOrEmpty(oldPolicyNo) || string.IsNullOrEmpty(segmentID) ? "" : aabdb.ExecuteScalar<string>(qgetCustID, segmentID, oldPolicyNo);
                List<Otosales.Models.vWeb2.PhoneNoModel> res = new List<Models.vWeb2.PhoneNoModel>();
                res = ListPhoneNo;
                
                if (!string.IsNullOrEmpty(oldPolicyNo)) {
                    Models.vWeb2.PhoneNoModel clmPhone = aabRepository.GetClaimeePhone(oldPolicyNo, custid);
                    if (!string.IsNullOrEmpty(clmPhone.NoHp))
                    {
                        res.Add(clmPhone);
                    }
                }

                List<string> s = new List<string>();
                foreach (Otosales.Models.vWeb2.PhoneNoModel p in res)
                {
                    s.Add(p.NoHp);
                }
                foreach (Otosales.Models.vWeb2.PhoneNoParam n in form.PhoneNoParam)
                {
                    if (string.IsNullOrEmpty(n.ColumnName) && s.Contains(n.PhoneNo))
                    {
                        return Json(new { status = true, message = "Already exist", data = "Phone Number " + n.PhoneNo + " already exist" });
                    }
                }
                bool isrenewal = db.ExecuteScalar<bool>(@"SELECT CAST(COALESCE(IsRenewal,0) AS BIT) FROM dbo.FollowUp f
                                        INNER JOIN dbo.OrderSimulation os 
                                        ON os.FollowUpNo = f.FollowUpNo
                                        WHERE os.OrderNo = @0 AND os.RowStatus = 1 AND os. ApplyF = 1", form.OrderNo);
                if (isrenewal)
                {
                    aabRepository.InsertUpdatePhoneNo(form ,custid);
                }
                else
                {
                    aabRepository.InsertUpdatePhoneNoOrderNew(form);
                }
                //res = Repository.v0219URF2019.AABRepository.GetPhoneList(form.OrderNo);
                return Json(new { status = true, message = "Success", data = new List<Otosales.Models.vWeb2.PhoneNoModel>() });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        #region Sprint 7 - FAY

        [HttpPost]
        public IHttpActionResult getBasicCover(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                List<Models.v0219URF2019.BasicCover> BasicCover = new List<Models.v0219URF2019.BasicCover>();

                //string Channel = form.Get("Channel").Trim();
                string vyear = form.Get("vyear").Trim();
                int year = string.IsNullOrEmpty(vyear) ? DateTime.Now.Year : Convert.ToInt32(vyear);
                bool isBasic = Convert.ToBoolean(form.Get("isBasic"));
                string isMVGodig = form.Get("isMvGodig");
                string productCode = form.Get("productCode");
                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                BasicCover = mobileRepository.GetBasicCover(year, isBasic, flagIsMvGodig, productCode);

                return Json(new { status = true, message = "OK", BasicCover }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getProductCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string insurancetype = form.Get("insurancetype").Trim();
                string salesofficerid = form.Get("salesofficerid").Trim();
                int isRenew = string.IsNullOrEmpty(form.Get("isRenewal")) ? 0 : Convert.ToInt32(form.Get("isRenewal"));
                string isMVGodig = form.Get("isMvGodig");
                string isNew = form.Get("isNew");

                List<Otosales.Models.Product> result = new List<Otosales.Models.Product>();
                if (insurancetype == null)
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }


                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                bool flagIsNew = false;
                if (!string.IsNullOrEmpty(isNew))
                {
                    flagIsNew = Boolean.Parse(isNew);
                }

                result = mobileRepository.GetProduct(insurancetype, salesofficerid, isRenew, flagIsNew, flagIsMvGodig);


                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        #endregion

    }
}