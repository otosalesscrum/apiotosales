﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Repository.v0001URF2020;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace Otosales.Controllers.v0001URF2020
{
    public class DataReactController : OtosalesBaseController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();
        #region Properties
        IMobileRepository mobileRepository;

        public DataReactController()
        {
            mobileRepository = new MobileRepository();
        }
        public DataReactController(IMobileRepository repository)
        {
            mobileRepository = repository;
        }
        #endregion

        [HttpPost]
        public IHttpActionResult getTaskList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                int currentPage = Convert.ToInt16(form.Get("currentPage"));
                int pageSize = Convert.ToInt16(form.Get("pageSize"));
                int currentPageLead = currentPage; // Convert.ToInt16(form.Get("currentPageLead"));
                int pageSizeLead = pageSize;// Convert.ToInt16(form.Get("pageSizeLead"));
                bool chIsSearchByChasNo = bool.Parse(form.Get("chIsSearchByChasNo").Trim());
                bool chIsSearchByEngNo = bool.Parse(form.Get("chIsSearchByEngNo").Trim());
                bool chIsSearchByPolicyNo = bool.Parse(form.Get("chIsSearchByPolicyNo").Trim());
                bool chIsSearchByProsName = bool.Parse(form.Get("chIsSearchByProsName").Trim());
                string salesofficerid = form.Get("salesofficerid").Trim();
                string search = form.Get("search").Trim();
                string indexOrderByItem = form.Get("indexOrderByItem").Trim();
                int indexOrderByTypeItem = Int32.Parse(form.Get("indexOrderByTypeItem").Trim());
                bool chFollowUpMonthlyChecked = bool.Parse(form.Get("chFollowUpMonthlyChecked").Trim());
                bool chFollowUpDateChecked = bool.Parse(form.Get("chFollowUpDateChecked").Trim());
                bool chNeedFUChecked = bool.Parse(form.Get("chNeedFUChecked").Trim());
                bool chPotentialChecked = bool.Parse(form.Get("chPotentialChecked").Trim());
                bool chCallLaterChecked = bool.Parse(form.Get("chCallLaterChecked").Trim());
                bool chCollectDocChecked = bool.Parse(form.Get("chCollectDocChecked").Trim());
                bool chNotDealChecked = bool.Parse(form.Get("chNotDealChecked").Trim());
                bool chSentToSAChecked = bool.Parse(form.Get("chSentToSAChecked").Trim());
                bool chBackToAOChecked = bool.Parse(form.Get("chBackToAOChecked").Trim());
                bool chPolicyCreatedChecked = bool.Parse(form.Get("chPolicyCreatedChecked").Trim());
                bool chOrderRejectedChecked = bool.Parse(form.Get("chOrderRejectedChecked").Trim());
                bool chNewChecked = bool.Parse(form.Get("chNewChecked").Trim());
                bool chRenewableChecked = bool.Parse(form.Get("chRenewableChecked").Trim());
                string dateFollowUp = form.Get("getFollowUpDate");
                int allowSearchOthers = Int32.Parse(form.Get("allowSearchOthers").Trim());

                DateTime getFollowUpDate = DateTime.Now;
                if (!string.IsNullOrEmpty(dateFollowUp))
                {
                    getFollowUpDate = Convert.ToDateTime(dateFollowUp);
                }

                if (salesofficerid == null || salesofficerid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @";
SELECT * 
INTO #tempGetTaskList
FROM dbo.FollowUp a WHERE #salesofficerid a.RowStatus = 1 AND #nextfollowupdate #followup


SELECT * FROM (
SELECT DISTINCT * 
        FROM  (SELECT 0 
                      AS 
                            IsRenewalNonRenNot, 
                      CASE 
                        WHEN IsRenewalDigital = 1 THEN 'Renewable Digital - ' + mft.FollowUpTypeDes 
                        ELSE 
						CASE WHEN IsRenewal=1 THEN 'Renewal - ' + mft.FollowUpTypeDes
						ELSE 'New - ' + mft.FollowUpTypeDes END 
                      END 
                      AS 
                            TaskStatus, 
                      b.description 
                      AS 
                            FollowUpDesc, 
                      c.description 
                      AS 
FollowUpReason, 
CASE 
    WHEN os.OldPolicyNo IS NOT NULL THEN (SELECT TOP 1 Period_To FROM BEYONDREPORT.AAB.dbo.Policy Where Policy_No = os.OldPolicyNo)
    ELSE ''
END ExpiredDate, 
a.CustID, 
a.FollowUpNo, 
a.LastFollowUpDate, 
a.NextFollowUpDate, 
a.LastSeqNo, 
CASE 
    WHEN a.followupstatus = 13 
        OR a.isrenewal = 1 THEN os.PolicyNo
    ELSE '' 
END PolicyNo, 
CASE pcm.isCompany
	WHEN 1 THEN pcs.CompanyName
	ELSE a.ProspectName 
END ProspectName, 
a.EntryDate,
a.IsRenewal,
p.isHotProspect,
a.followupstatus,
mws.WAStatusDes,
a.ReferenceNo,
wm.WAStatusID
FROM #tempGetTaskList a WITH (NOLOCK) 
INNER JOIN followupstatus b WITH (NOLOCK) 
  ON a.followupstatus = b.statuscode AND a.RowStatus = 1
LEFT JOIN followupstatusinfo c WITH (NOLOCK) 
 ON a.followupinfo = c.infocode 
LEFT JOIN dbo.OrderSimulation os WITH (NOLOCK)
 ON os.FollowUpNo = a.FollowUpNo AND os.ApplyF = 1 AND os.RowStatus = 1
 LEFT JOIN dbo.OrderSimulationMV omv WITH (NOLOCK)
 ON omv.OrderNo = os.OrderNo
 LEFT JOIN dbo.ProspectCustomer pcm 
 ON pcm.CustID = a.CustID
 LEFT JOIN dbo.ProspectCompany pcs
 ON pcs.CustID = a.CustID
 LEFT JOIN Quotation.dbo.HistoryPenawaran hp WITH (NOLOCK)
 ON hp.OrderNo = os.OrderNo 
 LEFT JOIN Quotation.dbo.Penawaran p WITH (NOLOCK)
 ON p.policyID = hp.PolicyID AND a.FollowUpStatus = 1 AND a.RowStatus = 1
 LEFT JOIN dbo.WAMessaging wm ON wm.OrderNo = os.OrderNo AND wm.ID = 
 (SELECT MAX(wm1.ID) FROM dbo.WAMessaging wm1 WHERE wm1.OrderNo = os.OrderNo AND wm1.WAStatusID IS NOT NULL) 
 LEFT JOIN dbo.MstWAStatus mws ON mws.WAStatusID = wm.WAStatusID
 LEFT JOIN dbo.MstFollowUpType mft ON mft.FollowUpTypeID = a.FollowUpTypeID
 #qwhere
 #chasisno #engineno
 #name
 #policyno) ass
#datakotor
#quotationlead
) res
#isrenewal
ORDER  BY #orderby
OFFSET @5 ROWS
FETCH NEXT @6 ROWS ONLY

DROP TABLE #tempGetTaskList";
                string qsalesofficerid = "";
                string qdatakotor = "";
                string qWhere = "";
                qsalesofficerid = string.IsNullOrEmpty(search) ? " a.salesofficerid = @0 AND " : "";
                qWhere = string.IsNullOrEmpty(search) ? "" : "WHERE";
                qdatakotor = !string.IsNullOrEmpty(search) ? @"UNION ALL 
SELECT *
FROM  (SELECT 1                   AS IsRenewalNonRenNot, 
'Renewable - MV'         AS TaskStatus, 
'Need FU'           AS FollowUpDesc, 
'Data baru diinput' AS FollowUpReason, 
p.Period_To   AS ExpiredDate, 
''                  CustID, 
''                  FollowUpNo, 
NULL                AS LastFollowUpDate, 
NULL                AS NextFollowUpDate, 
0                   AS LastSeqNo, 
ERP.policy_no       AS PolicyNo, 
p.Name_On_Policy    AS ProspectName, 
p.entrydt           AS EntryDate,
1                   AS IsRenewal,
NULL                isHotProspect,
1                   AS followupstatus,
NULL                AS WAStatusDes,
NULL                ReferenceNo,
NULL                WAStatusID
FROM [BeyondReport].[AAB].[dbo].[excluded_renewal_policy] ERP 
INNER JOIN [BeyondReport].[AAB].[dbo].policy p 
  ON p.policy_no = ERP.policy_no AND IsSendToSA=0
 AND p.Status = 'A' AND p.RENEWAL_STATUS = 0
 AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) #2name
     #2chasisno #2engineno #2policyno) b" : "";
                string qSalesOfficerIDQL = "";
                qSalesOfficerIDQL = string.IsNullOrEmpty(search) ? " ql.SalesOfficerID = @0 AND " : "";
                string qGetQuotationLead = @"UNION ALL 
SELECT * FROM  (
SELECT 0              AS IsRenewalNonRenNot, 
'New - MV GODIGITAL'  AS TaskStatus, 
'Need FU'             AS FollowUpDesc, 
'Data baru diinput'   AS FollowUpReason, 
''					  AS ExpiredDate, 
''                    CustID, 
''                    FollowUpNo, 
NULL                  AS LastFollowUpDate, 
NULL                  AS NextFollowUpDate, 
0                     AS LastSeqNo, 
ql.PolicyNo	          AS PolicyNo, 
ql.CustName           AS ProspectName, 
ql.CreatedDate        AS EntryDate,
0                     AS IsRenewal,
NULL                  AS isHotProspect,
1                     AS followupstatus,
NULL                  AS WAStatusDes,
ql.ReferenceNo        ReferenceNo,
NULL                  WAStatusID 
FROM dbo.QuotationLead ql 
LEFT JOIN dbo.FollowUp f ON f.ReferenceNo = ql.ReferenceNo
WHERE #qlsalesofficerid f.ReferenceNo IS NULL AND ql.RowStatus = 1 
AND ql.SalesOfficerID IS NOT NULL
#3name #3chasisno #3engineno #3policyno
) c";
                string qnextfollowupdate = "";
                if (chFollowUpDateChecked)
                {
                    qnextfollowupdate = @" ( nextfollowupdate <= @2 OR nextfollowupdate IS NULL ) ";
                }
                else if (chFollowUpMonthlyChecked)
                {
                    qnextfollowupdate = @" ((MONTH(a.EntryDate) = MONTH(GETDATE()) 
                                            AND YEAR(a.EntryDate) = YEAR(GETDATE()))) ";
                }

                List<int> listStatus = new List<int>();
                List<int> listInfo = new List<int>();

                if (chNeedFUChecked)
                {
                    listStatus.Add(1);
                }
                if (chPotentialChecked)
                {
                    listStatus.Add(2);
                }
                if (chCallLaterChecked)
                {
                    listStatus.Add(3);
                }
                if (chCollectDocChecked)
                {
                    listInfo.Add(56);
                }
                if (chNotDealChecked)
                {
                    listStatus.Add(5);
                }
                if (chSentToSAChecked)
                {
                    listInfo.Add(61);
                }
                if (chPolicyCreatedChecked)
                {
                    //listInfo.Add(50);
                    //listInfo.Add(51);
                    //listInfo.Add(52);
                    //listInfo.Add(53);
                    //listInfo.Add(54);
                    //listInfo.Add(55);
                    listStatus.Add(13);
                }
                if (chBackToAOChecked)
                {
                    listInfo.Add(60);
                }
                if (chOrderRejectedChecked)
                {
                    listStatus.Add(14);
                }

                string qfollowup = "";
                qfollowup = @" AND ( followupstatus IN(@3) OR (followupinfo IN(@4) AND followupinfo IS NOT NULL AND followupinfo <> 0)) ";
                string qisrenewal = "";
                qisrenewal = @"WHERE {0} {1} ";
                string snew = "";
                string srenew = "";

                if (chNewChecked)
                {
                    snew = "isrenewal = 0 OR isrenewal IS NULL";
                }
                if (chRenewableChecked)
                {
                    if (chNewChecked)
                    {
                        srenew = "OR isrenewal = 1";
                    }
                    else
                    {
                        srenew = "isrenewal = 1";
                    }
                }
                if (!chNewChecked && !chRenewableChecked)
                {
                    snew = "isrenewal = 2";
                    srenew = "";
                }
                qisrenewal = string.Format(qisrenewal, snew, srenew);
                string qchasisno = "";
                string qengineno = "";
                string qchasisno2 = "";
                string qengineno2 = "";
                string qchasisno3 = "";
                string qengineno3 = "";
                string qpolicyno = "";
                string qpolicyno2 = "";
                string qpolicyno3 = "";
                string qname = "";
                string qname2 = "";
                string qname3 = "";
                if (!string.IsNullOrEmpty(search))
                {
                    if (chIsSearchByChasNo)
                    {
                        qchasisno = " omv.ChasisNumber = @1 ";
                        qchasisno2 = " AND ERP.Chasis_Number = @1 ";
                        qchasisno3 = " AND ql.ChasisNumber = @1 ";
                    }
                    else if (chIsSearchByEngNo)
                    {
                        qengineno = " omv.EngineNumber = @1 ";
                        qengineno2 = " AND ERP.Engine_Number = @1 ";
                        qengineno3 = " AND ql.EngineNumber = @1 ";
                    }
                    else if (chIsSearchByPolicyNo)
                    {
                        qpolicyno = "os.PolicyNo = @1 ";
                        qpolicyno2 = " AND ERP.Policy_No = @1 ";
                        qpolicyno3 = " AND ql.PolicyNo = @1 ";
                    }
                    else if (chIsSearchByProsName)
                    {
                        //Flag form UI to Search Others ProsName - 0213/URF/2019
                        if (allowSearchOthers == 0) //
                        {
                            qsalesofficerid = " a.salesofficerid = @0 AND ";
                        }

                        search = '%' + search + '%';
                        qname = " (pcm.Name LIKE @1 OR pcs.CompanyName LIKE @1)";
                        qname2 = " WHERE Name_On_Policy LIKE @1 ";
                        qname3 = " AND ql.CustName LIKE @1 ";
                    }
                }

                string qorderby = "";
                if (indexOrderByItem.Equals("Status"))
                {
                    qorderby = @" isHotProspect DESC, IsRenewalNonRenNot ASC , COALESCE(isrenewal,0) ASC, COALESCE(NextFollowUpDate,DATEADD(YEAR,5,GETDATE())) ASC   , (CASE WHEN isrenewal = 1 THEN followupstatus END) ASC, (CASE WHEN isrenewal = 1 AND followupstatus=1 THEN ExpiredDate END) ASC, (CASE WHEN (isrenewal = 0 OR isrenewal IS NULL AND nextfollowupdate is null) OR (isrenewal = 1 AND followupstatus!=1)  THEN CAST(EntryDate AS DATE) END) DESC, WAStatusID DESC";
                }
                if (indexOrderByItem.Equals("Name"))
                {
                    qorderby = "ProspectName";
                }
                if (indexOrderByItem.Equals("EntryDate"))
                {
                    qorderby = "EntryDate";
                }
                if (indexOrderByItem.Equals("LastFollowUpdate"))
                {
                    qorderby = "LastFollowUpdate";
                }
                if (indexOrderByItem.Equals("NextFollowUpdate"))
                {
                    qorderby = "NextFollowUpdate";
                }


                if (!indexOrderByItem.Equals("Status"))
                {
                    if (indexOrderByTypeItem == 0)
                    {
                        qorderby = string.Concat(qorderby, " ASC");
                    }
                    else
                    {
                        qorderby = string.Concat(qorderby, " DESC");
                    }
                }
                query = query.Replace("#qwhere", qWhere);
                query = query.Replace("#quotationlead", qGetQuotationLead);
                query = query.Replace("#datakotor", qdatakotor);
                query = query.Replace("#salesofficerid", qsalesofficerid);
                query = query.Replace("#qlsalesofficerid", qSalesOfficerIDQL);
                query = query.Replace("#nextfollowupdate", qnextfollowupdate);
                query = query.Replace("#followup", qfollowup);
                query = query.Replace("#isrenewal", qisrenewal);
                query = query.Replace("#chasisno", qchasisno);
                query = query.Replace("#engineno", qengineno);
                query = query.Replace("#2chasisno", qchasisno2);
                query = query.Replace("#3chasisno", qchasisno3);
                query = query.Replace("#2engineno", qengineno2);
                query = query.Replace("#3engineno", qengineno3);
                query = query.Replace("#policyno", qpolicyno);
                query = query.Replace("#2policyno", qpolicyno2);
                query = query.Replace("#3policyno", qpolicyno3);
                query = query.Replace("#name", qname);
                query = query.Replace("#2name", qname2);
                query = query.Replace("#3name", qname3);
                query = query.Replace("#orderby", qorderby);

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    dynamic result = new ExpandoObject();
                    //result = db.Page<dynamic>(currentPage, pageSize, query, salesofficerid, search, getFollowUpDate
                    //    , listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 });
                    int start = (currentPage - 1) * pageSize;
                    if (start != 0)
                    {
                        start++;
                    }
                    result.Items = db.Fetch<dynamic>(query, salesofficerid, search, getFollowUpDate
                        , listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 }, start, pageSize);
                    result.CurrentPage = currentPage;

                    return Json(new { status = true, message = "OK", data = result, /*prospectLeadList = prospectLeadListPage, historyPenawaranList = historyPenawaranList, penawaranList = penawaranList*/ }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                #endregion
                Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
                try
                {
                    result = MobileRepository.GetTaksListDetail(CustID, FollowUpNo);
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskDetailDocument(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                string typeDocument = form.Get("Type");
                #endregion
                dynamic result = null;

                if (string.IsNullOrEmpty(CustID) || string.IsNullOrEmpty(FollowUpNo) || string.IsNullOrEmpty(typeDocument))
                {
                    return BadRequest("Bad Request!");
                }
                try
                {
                    switch (typeDocument)
                    {
                        case "PERSONAL":
                            result = MobileRepository.GetTaksListDetailDocumentPersonal(CustID, FollowUpNo);
                            break;
                        case "COMPANY":
                            result = MobileRepository.GetTaksListDetailDocumentCompany(CustID, FollowUpNo);
                            break;
                        case "PROSPECT":
                            result = MobileRepository.GetTaksListDetailDocumentAll(CustID, FollowUpNo);
                            break;
                        default:
                            return BadRequest("Bad Type Cuy!");
                    }

                    return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult SendQuotationEmail(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            string Email = form.Get("Email");
            string OrderNo = form.Get("OrderNo");
            Email = Regex.Replace(Email, @"\t|\n|\r", "");
            try
            {
                A2isCommonResult res = MobileRepository.SendQuotationEmail(OrderNo, Email);
                if (res.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + res.ResultDesc });
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult SendViaEmail(Models.vWeb2.SendViaEmailParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (string.IsNullOrEmpty(param.EmailTo) && string.IsNullOrEmpty(param.Body) && string.IsNullOrEmpty(param.OrderNo))
                {
                    return BadRequest("Input Required");
                }
                A2isCommonResult data = MobileRepository.SendViaEmail(param.EmailTo, param.EmailCC, param.EmailBCC, param.Body, param.Subject, param.ListAtt, param.OrderNo, param.IsAddAtt);
                if (data.ResultCode)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send Email - " + data.ResultDesc });

                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveFollowUpStatusDetails(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string FUData = form.Get("FollowUp");
                string Editor = form.Get("editor");
                Otosales.Models.FollowUp FU = JsonConvert.DeserializeObject<Otosales.Models.FollowUp>(FUData);

                string FULasData = form.Get("FollowUpLast");
                Otosales.Models.FollowUp FULast = JsonConvert.DeserializeObject<Otosales.Models.FollowUp>(FULasData);

                bool isInfoChanged = false;

                bool isSentToSA = bool.Parse(form.Get("isSentToSA").Trim());
                string STATE = form.Get("STATE");
                string TRName = form.Get("TRName");

                #region MGO BID
                if (STATE.Equals("MGOBID"))
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        string query = @";SELECT TOP 1 * FROM Quotation.dbo.HistoryPenawaran 
WHERE OrderNo = (SELECT TOP 1 OrderNo FROM OrderSimulation 
WHERE FollowUpNo =@0 ORDER BY EntryDate DESC)";

                        Otosales.Models.HistoryPenawaran hp = db.FirstOrDefault<Otosales.Models.HistoryPenawaran>(query, FU.FollowUpNo);
                        hp.IsAbleExpired = 0;

                        bool responsestatus = false;

                        // check all mgo bid if expired bid still exist
                        Util.CheckAllMGOBidExpiry();

                        using (var dbagent = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                        {
                            // check data not expired
                            int bidExist = dbagent.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", hp.HistoryPenawaranID);

                            if (bidExist == 1)
                            {
                                // permanently set data to agent
                                dbagent.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);
                                responsestatus = true;
                            }

                            if (responsestatus)
                            {

                                #region execUpdateFollowUp
                                #region UPDATE FOLLOW UP
                                if (FU.FollowUpInfo != FULast.FollowUpInfo)
                                {
                                    isInfoChanged = true;


                                }
                                query = @";UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6,[Receiver]=@7 WHERE [FollowUpNo]=@0";

                                db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1, FU.Receiver);

                                #endregion

                                if (isInfoChanged)
                                {
                                    #region INSERT FOLLOW UP HISTORY
                                    query = @";
    INSERT  INTO FollowUpHistory
        ( [FollowUpNo] ,
          [SeqNo] ,
          [CustID] ,
          [ProspectName] ,
          [Phone1] ,
          [Phone2] ,
          [SalesOfficerID] ,
          [EntryDate] ,
          [FollowUpName] ,
          [NextFollowUpDate] ,
          [LastFollowUpDate] ,
          [FollowUpStatus] ,
          [FollowUpInfo] ,
          [Remark] ,
          [BranchCode] ,
          [LastUpdatedTime] ,
          [RowStatus] ,
		  [FollowUpNotes] ,
          [Receiver]
        )
VALUES  ( @0 ,
          @1 ,
          @2 ,
          @3 ,
          @4 ,
          @5 ,
          @6 ,
          GETDATE() ,
          @7 ,
          @8 ,
          GETDATE() ,
          @10 ,
          @11 ,
          @12 ,
          @13 ,
          GETDATE() ,
          0 ,
		  @14 ,
          @15
        )
";
                                    int lastseqnofuh = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                    db.Execute(query, FU.FollowUpNo, lastseqnofuh, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode, FU.FollowUpNotes, FU.Receiver);
                                    #endregion
                                    Otosales.Repository.v0213URF2019.MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);

                                }
                                string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                                #endregion

                                int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                db.Execute("UPDATE Quotation.dbo.HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);

                                query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                                Otosales.Repository.v0213URF2019.MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = message, data = db.FirstOrDefault<Otosales.Models.FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());

                            }
                            else
                            {
                                query = @";SELECT * FROM OrderSimulation  WHERE FollowUpNo = @0";
                                List<Otosales.Models.OrderSimulation> orderSimulation = db.Fetch<Otosales.Models.OrderSimulation>(query, FU.FollowUpNo);

                                query = @";SELECT * FROM Quotation.dbo.HistoryPenawaran hp 
JOIN OrderSimulation os ON os.OrderNo = hp.OrderNo 
JOIN FollowUp fu On fu.FollowUpNo = os.FollowUpNo  WHERE fu.FollowUpNo = @0";

                                Otosales.Models.HistoryPenawaran hptemp = db.FirstOrDefault<Otosales.Models.HistoryPenawaran>(query, FU.FollowUpNo);

                                foreach (Otosales.Models.OrderSimulation os in orderSimulation)
                                {
                                    string queryDeleted = @";UPDATE OrderSimulationCoverage SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationInterest SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationMV SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulation SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);

                                }

                                string queryDelete = @";UPDATE FollowUpHistory SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE FollowUp SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE ProspectCustomer SET RowStatus=0 WHERE CustID=@0";
                                db.Execute(queryDelete, FU.CustID);
                                queryDelete = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE HistoryPenawaranID=@0";
                                db.Execute(queryDelete, hptemp.HistoryPenawaranID);

                                int hpCount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM HistoryPenawaran WHERE PolicyID = @0", hp.PolicyID);
                                if (hpCount == 0)
                                {
                                    queryDelete = @";UPDATE Quotation.dbo.Penawaran SET RowStatus=0 WHERE PolicyID=@0";
                                    db.Execute(queryDelete, hptemp.HistoryPenawaranID);
                                }

                                Otosales.Repository.v0213URF2019.MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = "Unable to process your request. This prospect has been expired" }, Util.jsonSerializerSetting());
                            }

                        }
                    }

                }
                #endregion

                else
                {
                    # region execUpdateFollowUp
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        #region UPDATE FOLLOW UP
                        if (FU.FollowUpInfo != FULast.FollowUpInfo)
                        {
                            isInfoChanged = true;
                        }
                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
BEGIN
SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
END
ELSE
BEGIN
SELECT 0 AS SeqNo
END", FU.FollowUpNo) + 1;

                        string query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6, [FollowUpNotes]=@7,[PICWA]=@8,[Receiver]=@9 WHERE [FollowUpNo]=@0";

                        db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, lastseqno, FU.FollowUpNotes, FU.PICWA, FU.Receiver);

                        if (FU.FollowUpStatus == 5)
                        {
                            string PolicyOrderNo = "";
                            query = @"SELECT COALESCE(PolicyOrderNo,'') FROM dbo.OrderSimulation WHERE FollowUpNo = @0 AND CustID = @1 AND ApplyF = 1 AND RowStatus = 1";
                            PolicyOrderNo = db.ExecuteScalar<string>(query, FU.FollowUpNo, FU.CustID);
                            if (!string.IsNullOrEmpty(PolicyOrderNo))
                            {
                                query = "UPDATE dbo.Mst_Order set Order_Status = '6' WHERE Order_No = @0";
                                var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                                aabdb.Execute(query, PolicyOrderNo);
                                db.Execute(@";IF EXISTS(SELECT * FROM Asuransiastra.GODigital.OrderSimulation WHERE PolicyOrderNo = @0)
                                            BEGIN
	                                            UPDATE Asuransiastra.GODigital.OrderSimulation 
	                                            SET OrderStatus = '14'
	                                            WHERE PolicyOrderNo = @0
                                            END", PolicyOrderNo);
                            }
                        }
                        #endregion

                        if (isInfoChanged)
                        {
                            #region INSERT FOLLOW UP HISTORY
                            query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[FollowUpNotes]
      ,[Receiver]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,GETDATE(),@9,@10,@11,@12,GETDATE(),0,@13,@14)
";

                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FULast.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode, FU.FollowUpNotes, FU.Receiver);
                            #endregion
                            Otosales.Repository.v0213URF2019.MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);

                        }
                        string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                        query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                        Otosales.Repository.v0213URF2019.MobileRepository.InsertMstOrderMobile(FU);
                        return Json(new { status = true, message = message, data = db.FirstOrDefault<Otosales.Models.FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                    }
                    #endregion
                }



            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetSlotTimeBooking(FormDataCollection form)
        {
            _log.Debug("Begin");
            if (form == null) return BadRequest("Bad Request");
            string zipcode = form.Get("ZipCode");
            string cityid = form.Get("CityId");
            string branchID = form.Get("BranchID");
            string date = form.Get("AvailableDate");

            DateTime d;
            if (!DateTime.TryParse(date, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out d)) return BadRequest("Incorrect Date format");

            if (string.IsNullOrEmpty(cityid) || string.IsNullOrEmpty(date)) return BadRequest("Bad Request");
            try
            {
                dynamic res = null;
                if (!string.IsNullOrEmpty(zipcode))
                {
                    // 0181/URF/2018
                    IEnumerable<dynamic> data = null;
                    data = MobileRepository.GetSurveyScheduleSurveyManagement(cityid, zipcode, d);
                    // 0181/URF/2018
                    _log.Debug("OK");
                    res = data;
                }
                else if (!string.IsNullOrEmpty(branchID))
                {
                    string SurveyLocation = "";
                    a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB);
                    SurveyLocation = db.SingleOrDefault<string>(@"SELECT type.NAME TNAME from Branch branch inner join BranchType type on type.ID = branch.TYPE  WHERE branch.ID=@0 AND branch.isDeleted = 0 and branch.TYPE in(2,5,6) order by branch.NAME asc", branchID);

                    SurveyLocation = SurveyLocation.Replace(" ", "").ToUpper();
                    List<string> TimeCodeSurveyDalam = db.Query<string>(@"
                select part from GODigital.Fn_SplitString((select optionValue from Asuransiastra.dbo.AsuransiAstraOptions
                where optionName = 'GODIG-'+'" + SurveyLocation + "'+'-SURVEY-TIME'),',') ").ToList();

                    List<dynamic> times = MobileRepository.GetScheduleSurveyDalam(TimeCodeSurveyDalam);
                    res = times;
                }

                res = MobileRepository.GetSurveyTimeByZoneNow(cityid, res, d);

                return Json(new { status = true, message = "Fetch success", data = res });
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }

        [HttpPost]
        public IHttpActionResult ValidateTimeSurvey(FormDataCollection form)
        {
            _log.Debug("Begin");
            if (form == null) return BadRequest("Bad Request");
            string zipcode = form.Get("ZipCode");
            string cityid = form.Get("CityId");
            string branchID = form.Get("BranchID");
            string date = form.Get("AvailableDate");
            string custID = form.Get("CustID");
            string followUpNo = form.Get("FollowUpNo");
            string scheduleTimeDesc = form.Get("ScheduleTimeDesc");

            DateTime d;
            if (!DateTime.TryParse(date, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out d)) return BadRequest("Incorrect Date format");

            if (string.IsNullOrEmpty(cityid) || string.IsNullOrEmpty(date) || string.IsNullOrEmpty(custID) || string.IsNullOrEmpty(followUpNo)) return BadRequest("Bad Request");
            try
            {
                dynamic res = null;
                
                dynamic times = MobileRepository.GetValidateTimeSurvey(cityid, scheduleTimeDesc, d, custID, followUpNo);
                res = times;

                return Json(new { status = true, message = "Fetch success", data = res });
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetTemplateCustomerConfirmation(FormDataCollection form)
        {
            _log.Debug("Begin");
            if (form == null) return BadRequest("Bad Request");
            string OrderNo = form.Get("OrderNo");
            string Type = form.Get("Type");
            string Time = form.Get("Time");
            string Date = form.Get("Date");
            
            if (string.IsNullOrEmpty(OrderNo)) return BadRequest("Bad Request");
            try
            {
                //string res = MobileRepository.GetTemplateCustomnerConfirmation(OrderNo, Int32.Parse(Type));
                string res = MobileRepository.GetTemplateCustomnerConfirmationSP(OrderNo, Int32.Parse(Type), Time, Date);

                return Json(new { status = true, message = "Fetch success", data = res });
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetShowIntermediaryConfirmationCustomer(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string ProductCode = form.Get("ProductCode");
                    string SalesOfficerID = form.Get("SalesOfficerID");
                    if (string.IsNullOrEmpty(ProductCode) && string.IsNullOrEmpty(SalesOfficerID))
                    {
                        return BadRequest("Input Required");
                    }

                    int IsAgency = SalesOfficerID.Length > 3 ? 1 : 0;

                    bool res = MobileRepository.ShowIntermediaryConfirmationCustomer(ProductCode, IsAgency);
                    return Json(new { status = true, message = "success", result = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpStatus(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            List<Models.FollowUpStatus> result = new List<Models.FollowUpStatus>();
            try
            {
                string followupstatus = form.Get("followupstatus").Trim();
                string followupstatusinfo = form.Get("followupstatusinfo").Trim();
                string isRenewal = form.Get("isRenewal").Trim();
                string followupno = form.Get("followupno").Trim();

                if (string.IsNullOrEmpty(followupstatus))
                {
                    return Json(new { status = false, message = "Param is null", data = "No Data" }, Util.jsonSerializerSetting());
                }

                result = mobileRepository.getFollowUpStatus(followupstatus, followupstatusinfo, isRenewal, followupno);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult UpdateTaskDetail(Models.v0213URF2019.UpdateTaskDetailParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string CustID = param.CustID;
            string FollowUpNo = param.FollowUpNo;
            Models.vWeb2.PersonalData PersonalData = param.PersonalData;
            Models.vWeb2.CompanyData CompanyData = param.CompanyData;
            Models.vWeb2.VehicleData VehicleData = param.VehicleData;
            Models.vWeb2.PolicyAddress policyAddress = param.policyAddress;
            Models.vWeb2.SurveySchedule surveySchedule = param.surveySchedule;
            List<Models.vWeb2.ImageDataTaskDetail> imageData = param.imageData;
            Models.vWeb2.OrderSimulationModel OSData = param.OSData;
            Models.vWeb2.OrderSimulationMVModel OSMVData = param.OSMVData;
            Models.vWeb2.CalculatePremiModel calculatedPremiItems = param.calculatedPremiItems;
            string FollowUpStatus = param.FollowUpStatus;
            Models.vWeb2.Remarks Remarks = param.Remarks;
            #endregion

            if (string.IsNullOrEmpty(param.CustID) || string.IsNullOrEmpty(param.FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            string OrderNo = "";
            try
            {
                OrderNo = MobileRepository.UpdateTaskListDetail(CustID, FollowUpNo, PersonalData,
                    CompanyData, VehicleData, policyAddress, surveySchedule, imageData, OSData, OSMVData,
                    calculatedPremiItems, FollowUpStatus, Remarks);
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", data = new { OrderNo, PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo() } });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }


        [HttpPost]
        public IHttpActionResult CheckPremiItems(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string OrderNo = form.Get("OrderNo");
                    if (string.IsNullOrEmpty(OrderNo))
                    {
                        return BadRequest("Input Required");
                    }

                    bool res = mobileRepository.CheckPremiItems(OrderNo);
                    return Json(new { status = true, message = "success", IsPremiItemExist = res });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult DownloadReport(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string TypeReport = form.Get("TypeReport");
                string StartDate = form.Get("StartDate");
                string EndDate = form.Get("EndDate");
                string UserId = form.Get("UserId");

                if (string.IsNullOrEmpty(TypeReport) || string.IsNullOrEmpty(StartDate) || string.IsNullOrEmpty(EndDate) || string.IsNullOrEmpty(UserId))
                {
                    return BadRequest("Bad Request");
                }

                Models.vWeb2.DownloadQuotationResult result = MobileRepository.GetByteReportDashboard(Convert.ToDateTime(StartDate), Convert.ToDateTime(EndDate), UserId, TypeReport);
                return Json(new { status = true, message = "success", data = result });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Error", data = e.ToString() });
                throw e;
            }
        }
        [HttpPost]
        public IHttpActionResult GetPaymentInfo(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                int IsGenerateVA = !string.IsNullOrEmpty(form.Get("IsGenerateVA")) ? Convert.ToInt32(form.Get("IsGenerateVA")) : 0;
                if (string.IsNullOrEmpty(CustID) && string.IsNullOrEmpty(FollowUpNo))
                    return Json(new { status = false, message = "Param can't be null" });

                Otosales.Models.vWeb2.PaymentInfo res = MobileRepository.GetPaymentInfo(CustID, FollowUpNo, IsGenerateVA);
                return Json(new { status = true, message = "Update Success.", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

    }
}
