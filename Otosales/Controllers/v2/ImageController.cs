﻿using a2is.Framework.DataAccess;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Otosales.Controllers.v2
{
    public class ImageController : OtosalesBaseController
    {
        [HttpPost]
        public IHttpActionResult ImageList(FormDataCollection form)
        {
            string userid = form.Get("userID");
            string date = form.Get("date");

            using(var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                return Json(db.Fetch<dynamic>("SELECT ImageID,DeviceID,SalesOfficerID,PathFile,EntryDate,LastUpdatedTime,RowStatus FROM ImageData WHERE salesofficerid = @0 AND LastUpdatedTime > @1 AND RowStatus = 1", userid, date), Util.jsonSerializerSetting());
            }
        }
    }
}
