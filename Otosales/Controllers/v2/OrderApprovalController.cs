﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace Otosales.Controllers.v2
{
    public class OrderApprovalController : OtosalesBaseController
    {

        OtosalesAPIResult response;
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        [HttpPost]
        public IHttpActionResult populateOrderApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            response = new OtosalesAPIResult();
            try
            {
                string userID = form.Get("UserID");
                if (userID.Equals(null) || userID.Equals(""))
                {
                    response.Status = false;
                    response.Message = "User cannot be Empty!";
                    response.ResponseCode = "6661";
                } else
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        // dynamic res = db.Fetch<dynamic>(@";EXEC usp_OtosalesGetApprovalList @0", userID);
                        dynamic res = db.Fetch<dynamic>(@";EXEC usp_GetOtosalesApprovalList @0", userID);
                       
                        response.Status = true;
                        response.Data = res;
                        response.ResponseCode = "1";
                        response.Message = "Success!";
                    }
                }

            }
            catch (Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = ex.Message.ToString();
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult approveOrderApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            string OrderNo = form.Get("OrderNo");
            string UserId = form.Get("UserId");
            string ApprovalType = form.Get("ApprovalType");

            #region Query Insert Update
            string qUpdateOrdMblAppr = @"UPDATE Mst_Order_Mobile_Approval
SET	UpdateUsr = @2, ApprovalStatus = 1, UpdateDt = GETDATE(), Status = 1, ApprovalUsr = @2
WHERE Order_No = @0 and ApprovalType = @1 and ApprovalStatus NOT IN (1)";

            string qInsertOrdMblApprHistory = @"INSERT INTO Mst_Order_Mobile_ApprovalHistory(Order_No,ApprovalType,ApprovalStatus,Reason,Status,UserID,RoleCode,EntryDt,EntryUsr)
SELECT Order_No, ApprovalType, ApprovalStatus, Reason, 1, 'OTOSL', RoleCode, GETDATE(), 'OTOSL'
FROM Mst_Order_Mobile_Approval WHERE order_no = @0 and ApprovalType = @1";

            string qCheckApprovalType = @";SELECT ApprovalType FROM Mst_Order_Mobile_Approval WHERE Order_No = @0 AND ApprovalType = @1 ORDER BY EntryDt DESC";

            string qCheckApprovalProcess = @";SELECT Max(Order_ID) Order_ID, Order_No, Approval_Process FROM Mst_Order_Mobile 
	WHERE Order_No = @0 GROUP BY Order_No, Approval_Process";

            string qUpdateFollowUp = @";UPDATE fu SET FollowUpStatus = '13', FollowUpInfo = IIF((COALESCE(fu.DocRep,'') = '') AND (COALESCE(os.IsNeedDocRep,0) = 1), '53', '50'), LastUpdatedTime = GETDATE()
FROM FollowUp fu INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo and os.ApplyF = 1 
WHERE os.PolicyOrderNo = @0";

            string qUpdateApprovalStatus = @";INSERT INTO Mst_Order_Mobile(Order_No,Approval_Status,Approval_Process,isSO,Remarks,Email_SA,Approval_Type,Actual_Date,EntryDt,EntryUsr,SA_State)
	SELECT @0, 0, 0, 0, '', '',0,GETDATE(),GETDATE(), @1, 2 ";
            #endregion

            response = new OtosalesAPIResult();
            string lastQuery = "";
            bool isSuccess = false;
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                try
                {
                    string val = db.FirstOrDefault<String>(qCheckApprovalType, OrderNo, ApprovalType);
                    if (val != null )
                    {
                        if (val.Equals("NEXTLMT"))
                        {
                            lastQuery = "qCheckApprovalProcess";
                            dynamic checkApprovalProcess = db.FirstOrDefault<dynamic>(qCheckApprovalProcess, OrderNo);
                            if(checkApprovalProcess != null)
                            {
                                long resAppProc = Convert.ToInt64(checkApprovalProcess.Approval_Process);
                                if (resAppProc == 0)
                                {
                                    lastQuery = "qUpdateOrdMblAppr";
                                    db.Execute(qUpdateOrdMblAppr, OrderNo, ApprovalType, UserId);
                                    lastQuery = "qInsertOrdMblApprHistory";
                                    db.Execute(qInsertOrdMblApprHistory, OrderNo, ApprovalType);
                                    lastQuery = "qUpdateApprovalStatus";
                                    db.Execute(qUpdateApprovalStatus, OrderNo, UserId);
                                    //using (var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                                    //{
                                    //    // Update table FollowUp
                                    //    lastQuery = "qUpdateFollowUp";
                                    //    dbMobile.Execute(qUpdateFollowUp, OrderNo);
                                    //}
                                    isSuccess = true;
                                }
                                else
                                {
                                    response.Status = false;
                                    response.Message = "Order Process Not Valid!";
                                    response.ResponseCode = "6661";
                                }
                                
                            }
                            else
                            {
                                response.Status = false;
                                response.Message = "Order Process Not Found!";
                                response.ResponseCode = "6662";
                            }
                        }
                        else
                        {
                            lastQuery = "qUpdateOrdMblAppr";
                            db.Execute(qUpdateOrdMblAppr, OrderNo, ApprovalType, UserId);
                            lastQuery = "qInsertOrdMblApprHistory";
                            db.Execute(qInsertOrdMblApprHistory, OrderNo, ApprovalType);
                            isSuccess = true;
                        }
                    } else if (ApprovalType.Equals("NEXTLMT"))
                    {
                        lastQuery = "qUpdateApprovalStatus";
                        db.Execute(qUpdateApprovalStatus, OrderNo, UserId);
                        using (var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                        {
                            // Update table FollowUp
                            lastQuery = "qUpdateFollowUp";
                            dbMobile.Execute(qUpdateFollowUp, OrderNo);
                        }
                    }
                     else 
                    {
                        response.Status = false;
                        response.Message = "Order Not Found!";
                        response.ResponseCode = "6663";
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    response.Status = false;
                    response.Message = "Error on " + lastQuery + " : \n" + ex.Message.ToString();
                    response.ResponseCode = "666";
                }
            }
            
            if (isSuccess)
            {
                response.Status = true;
                response.Message = "Order Approved";
                response.ResponseCode = "1";
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult rejectOrderApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            string OrderNo = form.Get("OrderNo");
            string UserId = form.Get("UserId");
            string Remark = form.Get("Remark");
            string ApprovalType = form.Get("ApprovalType");

            response = new OtosalesAPIResult();
            bool isSuccess = false;

            #region Query
            string qCheckOrderValid = @";SELECT COUNT(*) as OrderCount FROM Mst_Order_Mobile WHERE Order_No = @0";
            
            string qRejectMstOrderMobile = @";INSERT INTO Mst_Order_Mobile(Order_No,Approval_Status,Approval_Process,isSO,Remarks,Email_SA,Approval_Type,Actual_Date,EntryDt,EntryUsr,SA_State)
SELECT @0, 2, 0, 0, @1, '',0,GETDATE(),GETDATE(), @2, 1";

            string qRejectMstOrderMobileApproval = @";UPDATE Mst_Order_Mobile_Approval SET ApprovalStatus = 2, UpdateUsr = @2, UpdateDt = GETDATE(), ApprovalUsr = @2 WHERE ApprovalType = @1 and Order_No = @0
	
	INSERT INTO Mst_Order_Mobile_ApprovalHistory(Order_No,ApprovalType,ApprovalStatus,Reason,Status,UserID,RoleCode,EntryDt,EntryUsr)
	SELECT Order_No, ApprovalType, ApprovalStatus, Reason, 1, 'OTOSL', RoleCode, GETDATE(), 'OTOSL'
	FROM Mst_Order_Mobile_Approval WHERE order_no = @0 and ApprovalType = @1";

            string qUpdateFollowUp = @";Update FollowUp Set FollowUpInfo = null, FollowUpStatus = @1 , lastupdatedtime = GETDATE() FROM FollowUp fu inner join ordersimulation os on fu.followupno = os.followupno and os.applyf = 1 WHERE os.PolicyOrderNo = @0";


            #endregion

            string lastQuery = "";
            try
            {
                int FollowUpStatusReject = 14;
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    lastQuery = "qCheckOrderValid";
                    dynamic checkOrder = db.FirstOrDefault<dynamic>(qCheckOrderValid, OrderNo);
                    if (checkOrder.OrderCount > 0)
                    {
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        lastQuery = "qRejectMstOrderMobile";
                        db.Execute(qRejectMstOrderMobile, OrderNo, Remark, UserId); // From JSW, harus insert ke MstOrderMobile
                        //lastQuery = "qRejectMstOrderMobileApproval";
                        //db.Execute(qRejectMstOrderMobileApproval, OrderNo, ApprovalType);
                        lastQuery = "qRejectMstOrderMobileApproval";
                        db.Execute(qRejectMstOrderMobileApproval, OrderNo, ApprovalType, UserId);
                        lastQuery = "qUpdateFollowUp";
                        dbMobile.Execute(qUpdateFollowUp, OrderNo, FollowUpStatusReject);
                        isSuccess = true;
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "Invalid Order!";
                        response.ResponseCode = "6661";
                    }
                }
            } catch(Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = "Error on " + lastQuery + " => " + ex.Message.ToString();
            }


            if (isSuccess)
            {
                response.Status = true;
                response.ResponseCode = "1";
                response.Message = "Order Rejected";
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult reviseOrderApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            string OrderNo = form.Get("OrderNo");
            string UserId = form.Get("UserId");
            string Remark = form.Get("Remark");
            string ApprovalType = form.Get("ApprovalType");

            bool isSuccess = false;
            response = new OtosalesAPIResult();

            #region Query Reject Revise
            string qRejectNew = @";INSERT INTO Mst_Order_Mobile(Order_No,Approval_Status,Approval_Process,isSO,Remarks,Email_SA,Approval_Type,Actual_Date,EntryDt,EntryUsr,SA_State) 
                                    SELECT @0, 0, 0, 0, @1, '',0,GETDATE(),GETDATE(), @2, 1";

            string qReviseBackToAO = @";INSERT INTO Mst_Order_Mobile(Order_No,Approval_Status,Approval_Process,isSO,Remarks,Email_SA,Approval_Type,Actual_Date,EntryDt,EntryUsr,SA_State)
                                        SELECT @0, 0, 0, 0, @1, '',0,GETDATE(),GETDATE(), @2, 0";

            string qReviseSendToSA = @";INSERT INTO Mst_Order_Mobile(Order_No,Approval_Status,Approval_Process,isSO,Remarks,Email_SA,Approval_Type,Actual_Date,EntryDt,EntryUsr,SA_State)
                                        SELECT @0, 0, 0, 0, @1, '',0,GETDATE(),GETDATE(), @2, 1";

            string qUpdateOrdMblAppr = @"UPDATE Mst_Order_Mobile_Approval
SET	UpdateUsr = @2, ApprovalStatus = 2, UpdateDt = GETDATE(), Status = 1, ApprovalUsr = @2
WHERE Order_No = @0 and ApprovalType = @1 and ApprovalStatus NOT IN (1)";

            string qInsertOrdMblApprHistory = @"INSERT INTO Mst_Order_Mobile_ApprovalHistory(Order_No,ApprovalType,ApprovalStatus,Reason,Status,UserID,RoleCode,EntryDt,EntryUsr)
SELECT Order_No, ApprovalType, ApprovalStatus, Reason, 1, 'OTOSL', RoleCode, GETDATE(), 'OTOSL'
FROM Mst_Order_Mobile_Approval WHERE order_no = @0 and ApprovalType = @1";

            string qCheckOrderType = @";SELECT Order_Type as OrderType FROM Mst_Order WHERE Order_No = @0";

            string qCheckReviseTo = @";SELECT dbom.Order_ID, SA_State FROM dbo.Mst_Order_Mobile dbom 
                                            INNER JOIN (SELECT MAX(Order_ID) as Order_ID FROM dbo.Mst_Order_Mobile WHERE Order_No = @0 GROUP BY Order_No) dbmax
                                            ON dbom.Order_ID = dbmax.Order_ID";

            string qUpdateFollowUp = @";Update FollowUp Set FollowUpInfo = @1 , lastupdatedtime = GETDATE() FROM FollowUp fu inner join ordersimulation os on fu.followupno = os.followupno and os.applyf = 1 WHERE os.PolicyOrderNo = @0";

            #endregion

            string lastQuery = "";
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    lastQuery = "qCheckOrderType";
                    dynamic checkOrderType = db.FirstOrDefault<dynamic>(qCheckOrderType, OrderNo);
                    if (checkOrderType != null)
                    {
                        string orderType = Convert.ToString(checkOrderType.OrderType);
                        int infoCodeSendToSA = 61;
                        int infoCodeNeedDataRev = 60;

                        // Update ApprovalStatus on Order Mobile Approval
                        lastQuery = "checkMobileApproval";
                        dynamic checkMobileApproval = db.FirstOrDefault<dynamic>(@";SELECT * FROM Mst_Order_Mobile_Approval WHERE Order_No = @0 AND ApprovalType = @1", OrderNo, ApprovalType);
                        if (checkMobileApproval != null)
                        {
                            lastQuery = "qUpdateOrdMblAppr";
                            db.Execute(qUpdateOrdMblAppr, OrderNo, ApprovalType, UserId);
                            lastQuery = "qInsertOrdMblApprHistory";
                            db.Execute(qInsertOrdMblApprHistory, OrderNo, ApprovalType);
                        }

                        if (orderType.Equals("1"))
                        {
                            // Order New
                            lastQuery = "qRejectNew";
                            db.Execute(qRejectNew, OrderNo, Remark, UserId);
                            lastQuery = "qUpdateFollowUp";
                            dbMobile.Execute(qUpdateFollowUp, OrderNo, infoCodeSendToSA);
                            isSuccess = true;
                        }
                        else
                        {
                            // Order Renewal
                            lastQuery = "qCheckReviseTo";
                            dynamic res = db.FirstOrDefault<dynamic>(qCheckReviseTo, OrderNo);
                            if (res != null)
                            {
                                string saState = Convert.ToString(res.SA_State);
                                if (saState.Equals("2"))
                                {
                                    lastQuery = "qReviseSendToSA";
                                    db.Execute(qReviseSendToSA, OrderNo, Remark, UserId);
                                    lastQuery = "qUpdateFollowUp";
                                    dbMobile.Execute(qUpdateFollowUp, OrderNo, infoCodeSendToSA);
                                    isSuccess = true;
                                }
                                else if (saState.Equals("3"))
                                {
                                    lastQuery = "qReviseBackToAO";
                                    db.Execute(qReviseBackToAO, OrderNo, Remark, UserId);
                                    lastQuery = "qUpdateFollowUp";
                                    dbMobile.Execute(qUpdateFollowUp, OrderNo, infoCodeNeedDataRev);
                                    isSuccess = true;
                                }
                                else
                                {
                                    isSuccess = false;
                                    response.Status = false;
                                    response.ResponseCode = "6661";
                                    response.Message = "Approval already Done!";
                                }
                            }
                            else
                            {
                                isSuccess = false;
                                response.Status = false;
                                response.ResponseCode = "6662";
                                response.Message = "Approval Renewal Not Found!";
                            }
                        }
                    } else
                    {
                        isSuccess = false;
                        response.Status = false;
                        response.ResponseCode = "6663";
                        response.Message = "Order Not Found!";
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = "Error on " + lastQuery + " => " + ex.Message.ToString();
            }
            if (isSuccess)
            {
                response.Status = true;
                response.ResponseCode = "1";
                response.Message = "Order Revised";
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult fetchPolicySendAddress(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            #region Query
            string qGetPolicySendToAddress = @";DECLARE @@CustId VARCHAR(100)
                                            SELECT @@CustId=COALESCE(Cust_Id,'') FROM dbo.Mst_Order WHERE Order_No = @0
                                            if(@@CustId='')
                                            BEGIN
	                                            SELECT PolicySentToDes,
	                                            oda.Address [Address],
	                                            CASE Delivery_Code
	                                            WHEN 'GC' THEN (SELECT Name FROM dbo.GardaCenter WHERE GCCode = Source_Type)
	                                            ELSE ContactPerson
	                                            END NAME,
	                                            RTRIM(m.PostCode) + ' - ' + m.Description + ', ' + mg1.description + ', ' + mg2.description + ', ' + mg3.description
	                                            AS Postal
	                                            FROM dbo.Ord_Dtl_Address oda
	                                            INNER JOIN dbo.Mst_Order mo
	                                            ON mo.Order_No = oda.Order_No
	                                            INNER JOIN dbo.Mst_Prospect mp
	                                            ON mp.Prospect_Id = mo.Prospect_Id
	                                            LEFT JOIN BEYONDMOSS.AABMobile.dbo.Mst_PolicySentTo mpst
	                                            ON mpst.PolicySentToId = oda.Delivery_Code
	                                            INNER JOIN dbo.Mst_Postal m 
	                                            ON m.PostCode = oda.Postal_Code
	                                            LEFT JOIN dbo.Mst_General mg1 
	                                            ON mg1.code = m.City AND mg1.type = 'CTY'
	                                            LEFT JOIN dbo.Mst_General mg2
	                                            ON mg2.code = m.Province AND mg2.type = 'PRV'
	                                            LEFT JOIN dbo.Mst_General mg3
	                                            ON mg3.code = m.Country AND mg3.type = 'CON'
	                                            WHERE mo.Order_No = @0
	                                            AND Address_Type = 'DELIVR'
                                            END
                                            ELSE
                                            BEGIN
	                                            SELECT PolicySentToDes,
                                                oda.Address [Address],
	                                            CASE Delivery_Code
	                                            WHEN 'GC' THEN (SELECT Name FROM dbo.GardaCenter WHERE GCCode = Source_Type)
	                                            ELSE ContactPerson
	                                            END NAME,
	                                            RTRIM(m.PostCode) + ' - ' + m.Description + ', ' + mg1.description + ', ' + mg2.description + ', ' + mg3.description
	                                            AS Postal
	                                            FROM dbo.Ord_Dtl_Address oda
	                                            INNER JOIN dbo.Mst_Order mo
	                                            ON mo.Order_No = oda.Order_No
	                                            INNER JOIN dbo.Mst_Customer mp
	                                            ON mp.Cust_Id = mo.Cust_Id
	                                            LEFT JOIN BEYONDMOSS.AABMobile.dbo.Mst_PolicySentTo mpst
	                                            ON mpst.PolicySentToId = oda.Delivery_Code
	                                            INNER JOIN dbo.Mst_Postal m 
	                                            ON m.PostCode = oda.Postal_Code
	                                            LEFT JOIN dbo.Mst_General mg1 
	                                            ON mg1.code = m.City AND mg1.type = 'CTY'
	                                            LEFT JOIN dbo.Mst_General mg2
	                                            ON mg2.code = m.Province AND mg2.type = 'PRV'
	                                            LEFT JOIN dbo.Mst_General mg3
	                                            ON mg3.code = m.Country AND mg3.type = 'CON'
	                                            WHERE mo.Order_No = @0
	                                            AND Address_Type = 'DELIVR'
                                            END";
            #endregion

            response = new OtosalesAPIResult();
            try
            {
                String OrderNo = Convert.ToString(form.Get("OrderNo"));
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    dynamic result = db.FirstOrDefault<dynamic>(qGetPolicySendToAddress, OrderNo);
                    response.Data = result;
                }
                response.Status = true;
                response.Message = "Success!";
                response.ResponseCode = "1";
             }
            catch (Exception ex)
            {
                response.Status = false;
                response.Message = ex.Message;
                response.ResponseCode = "666";
            }
            return Json(response, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult fetchDetailApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            response = new OtosalesAPIResult();
           try
            {
                string OrderID = form.Get("OrderID");
                string OrderNo = form.Get("OrderNo");
                string ApprovalType = form.Get("ApprovalType");
                string Role = form.Get("Role");
                response.ResponseCode = "1";
                response.Message = "Success!";
                switch (ApprovalType)
                {
                    case "COMSAMD":
                        {
                            response = detailApprovalComission(OrderID, Role, OrderNo);
                        }
                        break;
                    case "KOMISI":
                        {
                            response = detailApprovalComission(OrderID, Role, OrderNo);
                        }
                        break;
                    case "ADJUST":
                        {
                            response = detailApprovalAdjustment(OrderID, OrderNo);
                        }
                        break;
                    case "NEXTLMT":
                        {
                            response = detailApprovalTsiLimit(OrderID, OrderNo);
                        }
                        break;
                    default:
                        {
                            response.Status = false;
                            response.Message = "Unknown Order Type";
                            break;
                        }
                }
            } catch(Exception ex)
            {
                response.Status = false;
                response.ResponseCode = "666";
                response.Message = ex.Message;
            }
            return Json(response, Util.jsonSerializerSetting());
        }
        

        private OtosalesAPIResult detailApprovalTsiLimit(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();

            #region Query
            string qGetOrderNum = @";SELECT Order_No FROM Mst_Order_Mobile WHERE Order_ID = @0";
            string qGetFromGen5SP = @";EXEC sp_Execute_Order_Analysis_0069URF2018 @0";
            #endregion

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    string orderNumber = db.FirstOrDefault<string>(qGetOrderNum, OrderID);
                    if (orderNumber.Equals("") || orderNumber == null)
                    { // untuk order bukan dari otosales
                        orderNumber = OrderNumber;
                    }

                    dynamic data = db.FirstOrDefault<dynamic>(qGetFromGen5SP, OrderNumber);
                    if (data == null)
                    {
                        resp.Status = false;
                        resp.Message = "Order Not Exists!";
                        resp.ResponseCode = "6661";
                    } else
                    {
                        resp.Data = data.Info_Text;
                        resp.Status = true;
                        resp.Message = "Success";
                        resp.ResponseCode = "1";
                    }
                }
            } catch (Exception ex)
            {
                logger.Error(ex);
                resp.Status = false;
                resp.Message = ex.Message.ToString();
                resp.ResponseCode = "666";
            }

            return resp;
        }

        private OtosalesAPIResult detailApprovalAdjustment(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    // TODO LCY : this query still has dummy value for
                    // isPTSB, Remarks, SendToBranch, SendToName, SendToAddress, SendToPostal ==> on different API
                    string query = @";SELECT IIF(','+mom.Reason+',' like '%,PSTB,%',1, 0) as isPTSB, COALESCE(os.Remarks, '') as Remarks, '' as SendToBranch, '' as SendToName, 
                    '' as SendToAddress, '' as SendToPostal, msom.Order_ID [Order_ID],
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    FROM [dbo].[Mst_Order] mo
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN [dbo].[Mst_Order_Mobile_Approval] mom ON mom.Order_No = mo.Order_No AND mom.ApprovalType = 'ADJUST'
                    LEFT JOIN BeyondMoss.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    AND mo.Order_No = @0
					--AND msom.Order_ID = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY mo.Order_No, msom.Order_ID, mom.Reason, os.Remarks, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC , Order_ID DESC
                ";

                    resp.Status = true;
                    resp.Message = "Success from detailApprovalAdjustment";
                    //resp.Data = db.FirstOrDefault<dynamic>(query, OrderID);
                    resp.Data = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
                //return Json(new { status = false, message = e.Message, data = "null" });
            }

            return resp;
        }

        public OtosalesAPIResult detailApprovalComission(string OrderID, string Role, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            switch(Role)
            {
                case "AO":
                    {
                        return detailComissionAO(OrderID, OrderNumber);
                    }
                case "MGO":
                    {
                        return detailComissionMGO(OrderID, OrderNumber);
                    }
                default:
                    {
                        throw new Exception("Invalid User!!!");
                    }
            }
        }

        public OtosalesAPIResult detailComissionMGO(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    #region Query Select Data
                    string query = @";SELECT X.AgentCode as AgentCode, x.AgentName as AgentName, X.UplinerCode as UplinerCode, x.UplinerName as UplinerName,
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(msom.Order_ID)) as OrderID, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    FROM [dbo].[Mst_Order] mo 
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN  ( SELECT b.Cust_Id AS CustomerID,
								a.Client_Code AS ClientCode,
								b.Name AS AgentName,
								a.Client_Code AS AgentCode,
                               a.Upliner_Client_Code AS UplinerCode ,
                               RTRIM(d.Name) AS UplinerName,
                               e.Cust_Id AS LeaderCustomerID,
                               a.Leader_Client_Code AS LeaderCode ,                  
                               RTRIM(f.Name) AS LeaderName                    
                                     FROM      dtl_cust_type a WITH ( NOLOCK )
                                               INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                               LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                               LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                               LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                               LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                     WHERE     a.Client_Type = 'AGENT') X ON X.CustomerID=mo.Broker_Code OR X.ClientCode=mo.Broker_Code
                    WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    --AND msom.Order_ID = @0
                    AND mo.Order_No = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY AgentCode, AgentName, UplinerCode, UplinerName, mo.Order_No, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Order_ID, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC, msom.Order_ID DESC
                ";
                    #endregion
                    // 002 end
                    dynamic result = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    if (result == null)
                    {
                        resp.Status = false;
                        resp.Message = "Data Not Found!";
                        resp.ResponseCode = "6661";
                    }
                    else
                    {
                        resp.Status = true;
                        resp.Message = "Success from detailComissionMGO";
                        resp.ResponseCode = "1";
                    }
                    resp.Data = result;
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
            }
            return resp;
        }

        public OtosalesAPIResult detailComissionAO(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    #region Query Select Data
                    string query = @";SELECT LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(msom.Order_ID)) as OrderId, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    FROM [dbo].[Mst_Order] mo 
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
                    WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    --AND msom.Order_ID = @0
					AND mo.Order_No = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY mo.Order_No, msom.Order_ID, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC, msom.Order_ID DESC
                    
                    ";
                    #endregion

                    dynamic result = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    if (result == null)
                    {
                        resp.Status = false;
                        resp.Message = "Data Not Found!";
                        resp.ResponseCode = "6661";
                    } else
                    {
                        resp.Status = true;
                        resp.Message = "Success from detailComissionAO";
                        resp.ResponseCode = "1";
                    }
                    resp.Data = result;
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
                //return Json(new { status = false, message = e.Message, data = "null" });
            }
            return resp;
        }


        [HttpPost]
        public IHttpActionResult fetchDetailObjectApproval(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            try
            {
                string OrderNo = form.Get("OrderNo");

                if (string.IsNullOrEmpty(OrderNo))
                {
                    return Json(new { status = false, message = "Parameter is null or empty", data = "null" });
                }


                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    #region Query DB
                    string query = @"SELECT os.Order_No, osmv.Object_No, 
                    CONCAT(LTRIM(RTRIM(msvb.Description)), ' ', LTRIM(RTRIM(msvm.Description)), ' ', LTRIM(RTRIM(osmv.Series))) as VehicleName, 
                    ROUND(osmv.Mfg_Year,0) as 'Year', osmv.Registration_Number as PoliceNumber, mi.agreed_value, 
                    osi.Interest_Code, mi.Description as 'Interest_Description', osc.Coverage_Id, mco.Name as 'Coverage_Description', osc.Ndays, osc.Begin_Date, osc.End_Date, osc.Rate, osc.Sum_Insured, 
                    osc.Loading as LoadingRate, osc.Cover_Premium FROM Mst_Order os 
                    INNER JOIN Ord_Dtl_MV osmv ON os.Order_No = osmv.Order_No 
                    INNER JOIN Ord_Dtl_Interest osi ON osmv.Order_No = osi.Order_no AND 
                    osmv.Object_No = osi.Object_No 
                    INNER JOIN Ord_Dtl_Coverage osc ON osc.Order_No = osi.Order_no AND 
                    osc.Interest_No = osi.Interest_No AND osc.Object_No = osi.Object_No 
                    INNER JOIN Mst_Interest mi ON osi.Interest_Code = mi.interest_id 
                    INNER JOIN Mst_Coverage mco ON mco.Coverage_Id = osc.Coverage_Id 
                    INNER JOIN Mst_Vehicle_Brand msvb ON msvb.Brand_id = osmv.Brand_Code 
                    INNER JOIN Mst_Vehicle_Model msvm ON msvm.Model_id = osmv.Model_Code 
                    WHERE os.Order_No = @0 
                    ORDER BY osmv.Object_No, osc.End_Date, osc.Coverage_No, osi.Interest_No ASC
                ";
                    #endregion

                    decimal ndays;
                    List<OrderApprovalDetailObject> oaDetailObjects = db.Fetch<OrderApprovalDetailObject>(query, OrderNo);
                    List<OrderApprovalDetailObject> oaDetailObjectYearly = new List<OrderApprovalDetailObject>();

                    // split objects per year
                    for (int i = 0; i < oaDetailObjects.Count; i++)
                    {
                        decimal maxDays = maxDays = DateTime.IsLeapYear(oaDetailObjects[i].End_Date.Value.Year)?366:365;
                        decimal yearsCount = Math.Floor(oaDetailObjects[i].Ndays / 365);
                        if (oaDetailObjects[i].Ndays <= maxDays || oaDetailObjects[i].aggreed_value.Equals(true))
                        {
                            OrderApprovalDetailObject oaObject = new OrderApprovalDetailObject();
                            oaObject.Order_No = oaDetailObjects[i].Order_No;
                            oaObject.Object_No = oaDetailObjects[i].Object_No;
                            oaObject.VehicleName = oaDetailObjects[i].VehicleName;
                            oaObject.Interest_Code = oaDetailObjects[i].Interest_Code;
                            oaObject.Interest_Description = oaDetailObjects[i].Interest_Description;
                            oaObject.Coverage_Id = oaDetailObjects[i].Coverage_Id;
                            oaObject.Coverage_Description = oaDetailObjects[i].Coverage_Description;
                            oaObject.Begin_Date = oaDetailObjects[i].Begin_Date;
                            oaObject.End_Date = oaDetailObjects[i].End_Date;
                            oaObject.Year = oaDetailObjects[i].Year;
                            oaObject.PoliceNumber = oaDetailObjects[i].PoliceNumber;
                            oaObject.Rate = oaDetailObjects[i].Rate;
                            oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured;
                            oaObject.LoadingRate = oaDetailObjects[i].LoadingRate;
                            oaObject.Cover_Premium = oaDetailObjects[i].Cover_Premium;

                            oaDetailObjectYearly.Add(oaObject);
                        }
                        else
                        {
                            ndays = Convert.ToDecimal(oaDetailObjects[i].Ndays);
                            decimal yearDuration = ndays / maxDays;
                            int years = 0;
                            int tmpMaxDays = 365;
                            int.TryParse(""+maxDays, out tmpMaxDays);

                            int dayBegin = oaDetailObjects[i].Begin_Date.Value.Day;
                            int dayEnd = oaDetailObjects[i].End_Date.Value.Day;
                            int monthBegin = oaDetailObjects[i].Begin_Date.Value.Month;
                            int monthEnd = oaDetailObjects[i].End_Date.Value.Month;

                            for (int j = 0; j < ndays; j += tmpMaxDays)
                            {
                                OrderApprovalDetailObject oaObject = new OrderApprovalDetailObject();
                                oaObject.Order_No = oaDetailObjects[i].Order_No;
                                oaObject.Object_No = oaDetailObjects[i].Object_No;
                                oaObject.VehicleName = oaDetailObjects[i].VehicleName;
                                oaObject.Interest_Code = oaDetailObjects[i].Interest_Code;
                                oaObject.Interest_Description = oaDetailObjects[i].Interest_Description;
                                oaObject.Coverage_Id = oaDetailObjects[i].Coverage_Id;
                                oaObject.Coverage_Description = oaDetailObjects[i].Coverage_Description;
                                DateTime date = Convert.ToDateTime(oaDetailObjects[i].Begin_Date);
                                oaObject.Begin_Date = date.AddYears(years);
                                oaObject.End_Date = oaDetailObjects[i].End_Date;
                                oaObject.Year = oaDetailObjects[i].Year;
                                oaObject.PoliceNumber = oaDetailObjects[i].PoliceNumber;
                                oaObject.Rate = oaDetailObjects[i].Rate;
                                oaObject.LoadingRate = oaDetailObjects[i].LoadingRate;
                                oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured; // 003
                                // Check if the Premi is yearly package (not Compre others or TLO others)
                                if (dayEnd == dayBegin && monthBegin == monthEnd)
                                {
                                    oaObject.Cover_Premium = oaDetailObjects[i].Cover_Premium / yearsCount;
                                } else
                                {
                                    if ((ndays - j) < tmpMaxDays)
                                    {
                                        //oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured * (ndays - j) / ndays; // 003
                                        oaObject.Cover_Premium = oaDetailObjects[i].Cover_Premium * (ndays - j) / ndays;
                                    }
                                    else
                                    {
                                        //oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured * 365 / ndays; // 003
                                        oaObject.Cover_Premium = oaDetailObjects[i].Cover_Premium * tmpMaxDays / ndays;
                                    }
                                }
                                oaDetailObjectYearly.Add(oaObject);
                                years++;
                                tmpMaxDays = DateTime.IsLeapYear(date.AddYears(years).Year) ? 366 : 365; // Check Next Years

                            }
                        }
                    }

                    return Json(new { status = true, message = "Success", data = oaDetailObjectYearly }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

    }
}
