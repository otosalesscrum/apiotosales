﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using a2is.Framework.API;
using a2is.Framework.DataAccess;
using Newtonsoft.Json;
using Otosales.Infrastructure;
using Otosales.Models;
using System.Net.Http.Formatting;
using Otosales.Codes;
using Newtonsoft.Json.Converters;

/*
    Rev No      : 001
    Rev Date    : 2016-05-03
    Dev         : BSY
    Remark      : read RowStatus from penawaran table, dibuat karena ada penambahan field pada tabel penawaran.
                  * untuk sementara changeset ini di-halt terlebih dahulu.

    Rev No      : 002
    Rev Date    : 2016-06-09
    Dev         : BSY & REL
    Remark      : # change query for getOrderApproval to avoid null value on customer name
                  # change query for getOrderApproval detail to avoid wrong comission calculation

    Rev No      : 003
    Rev Date    : 2016-06-21
    Doc Ref     : 0147/URF/2016 - 4.2.2
    Dev         : BSY
    Remark      : unsplit tsi for multiyear.

*/
namespace Otosales.Controllers.v2
{
    public class ReplicationController : OtosalesBaseController
    {
        #region Declare

        private static int MAXFETCH = int.Parse(ConfigurationManager.AppSettings["MaxData"].ToString());

        private OtosalesAPIResult response;

        #endregion

        #region Additional Function

        [HttpPost]
        public IHttpActionResult ReplicationTable(FormDataCollection form)
        {
            List<string> result = new List<string>();

            string userID = form.Get("userID");
            string data = form.Get("data");
            string deviceID = form.Get("deviceID");

            List<string[]> items = JsonConvert.DeserializeObject<List<string[]>>(data, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

            for (int i = 0; i < items.Count; i++)
            {
                string tableName = items[i][0];
                string datetime = items[i][1];
                int count = Convert.ToInt32(items[i][2]);

                switch (tableName)
                {
                    //Table Master
                    case "AABHead":
                        if (getAABHead(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "AABRegion":
                        if (getAABRegion(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Dealer":
                        if (getDealer(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "ProductType":
                        if (getProductType(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "ProductAgent":
                        if (getProductAgent(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Product":
                        if (getProduct(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Region":
                        if (getRegion(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "VehicleBrand":
                        if (getVehicleBrand(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "VehicleModel":
                        if (getVehicleModel(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "VehicleSeries":
                        if (getVehicleSeries(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Vehicle":
                        if (getVehicle(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Dtl_Ins_Factor":
                        if (getUsage(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Dtl_Rate_Factor":
                        if (getDtlRateFactor(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Dtl_Rate_Scoring":
                        if (getDtlRateScoring(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Prd_Deductible":
                        if (getPrdDeductible(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "PRD_POLICY_FEE":
                        if (getPrdPolicyFee(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Prd_Load_Mv":
                        if (getPrdLoadMv(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Prd_Agreed_Value":
                        if (getPrdAgreedValue(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Prd_Interest_Coverage":
                        if (getPrdInterestCoverage(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "B2B_Interest_Coverage":
                        if (getB2BInterestCoverage(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Accessories":
                        if (getAccessories(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "VehicleType":
                        if (getVehicleType(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "FollowUpStatus":
                        if (getFollowUpStatus(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "FollowUpStatusInfo":
                        if (getFollowUpStatusInfo(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "GraphicColorSetting":
                        if (getGraphicColorSetting(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "SysParam":
                        if (getSysParam(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "AABBranch":
                        if (getAABBranch(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Workshop":
                        if (getWorkshop(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "GardaCenter":
                        if (getGardaCenter(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "AccountOfficer":
                        if (getAccountOfficer(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "VehiclePriceTolerance":
                        if (getVehiclePriceTolerance(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "MstInterest":
                        if (getMstInterest(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "MstCoverage":
                        if (getMstCoverage(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "Branch":
                        if (getBranch(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    case "SalesmanDealer":
                        if (getSalesmanDealer(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    //Rev 004  : by REL penambahan Get Mapping_Cover_Progressive
                    case "Mapping_Cover_Progressive":
                        if (getMappingCoverProgressive(datetime, count).Count > 0) result.Add(tableName);
                        break;
                    //Table Transaction
                    case "ProspectCustomer":
                        if (getProspectCustomer(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                    case "FollowUp":
                        if (getFollowUp(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                    case "FollowUpHistory":
                        if (getFollowUpHistory(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                    case "OrderSimulation":
                        if (getOrderSimulation(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                    case "OrderSimulationMV":
                        if (getOrderSimulationMV(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                    case "OrderSimulationInterest":
                        if (getOrderSimulationInterest(datetime, count, userID).Count > 0)
                            result.Add(tableName);
                        break;
                    case "OrderSimulationCoverage":
                        if (getOrderSimulationCoverage(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                    case "EmailQuotation":
                        if (getEmailQuotation(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                    //Table Custome
                    case "FollowUpSummary":
                        if (getFollowUpSummary(datetime, count, userID).Count > 0) result.Add(tableName);
                        break;
                }
            }

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult SyncCountMasterData(FormDataCollection form)
        {
            string deviceID = form.Get("deviceID");
            string data = form.Get("data");
            Dictionary<string, string[]> dic = JsonConvert.DeserializeObject<Dictionary<string, string[]>>(data, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                foreach (KeyValuePair<string, string[]> item in dic)
                {
                    string tablename = item.Key;
                    string datetime = item.Value[0];
                    int count = int.Parse(item.Value[1]);

                    // delete replication info
                    db.Execute("DELETE FROM ReplicationInfo WHERE UserID is null AND DeviceID=@0 AND TableName=@1", deviceID, tablename);

                    // Insert replication info
                    db.Execute("INSERT INTO ReplicationInfo VALUES (@0, null, @1, @2, @3)", deviceID, tablename, count, datetime);
                }
                response = new OtosalesAPIResult();
                response.Status = true;
                return Json(response, Util.jsonSerializerSetting());
            }
        }

        #endregion

        #region Master Data

        [HttpPost]
        public IHttpActionResult AABBranch(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getAABBranch(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getAABBranch(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM AABBranch WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT BranchCode,Description,Region,City,AccountNo,AccountName,Bank,EntryDate,LastUpdatedTime,RowStatus,BankAddress,BranchInitial FROM AABBranch WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, BranchCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult AABHead(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getAABHead(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getAABHead(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM AABHead WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT Type,Param,Value,LastUpdatedTime,RowStatus FROM AABHead WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, Type, Param, Value ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult AABRegion(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getAABRegion(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getAABRegion(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM AABRegion WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT RegionCode, RegionDescription, NationalCode, LastUpdatedTime, RowStatus FROM AABRegion WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, RegionCode, RegionDescription, NationalCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Accessories(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getAccessories(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getAccessories(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Accessories WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT AccessoriesCode,Description,MaxSI,LastUpdatedTime,RowStatus FROM Accessories WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, AccessoriesCode ASC OFFSET @1 ROWS FETCH NEXT 100 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult AccountOfficer(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getAccountOfficer(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getAccountOfficer(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM SalesOfficer WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT SalesOfficerID, Email, Name, Role, BranchCode, Phone1, Phone2, Phone3, OfficePhone, Ext, Fax, left(OfficePhone, (charindex(')', OfficePhone))) AreaCodePhone, LastUpdatedTime, RowStatus FROM SalesOfficer WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, SalesOfficerID ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult B2BInterestCoverage(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getB2BInterestCoverage(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getB2BInterestCoverage(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM B2B_Interest_Coverage a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code = b.ProductCode WHERE a.Vehicle_Type='ALL' and a.LastUpdatedTime = @0", datetime);

                const string query = "SELECT a.Product_Code,a.Vehicle_Type,a.Usage,a.Type,a.Interest_Id,a.Coverage_Id,a.Cover_Flag,a.Comprehensive_Flag,a.TLO_Flag,a.Default_Currency,a.Default_Sum_Insured,a.Basic_Cover_Deductible,a.Deductible_Score_Code,a.Deductible_Code,a.Deductible_Currency,a.Deductible_Amount,a.Status,a.EntryUsr,a.EntryDt,a.UpdateUsr,a.UpdateDt,a.Auto_Apply,a.LastUpdatedTime,a.RowStatus FROM B2B_Interest_Coverage a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.Vehicle_Type='ALL' and a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Product_Code, a.Vehicle_Type, a.Usage, a.Type, a.Interest_Id, a.Coverage_Id, a.Deductible_Currency ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Mapping_Cover_Progressive(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getMappingCoverProgressive(datetime, count), Util.jsonSerializerSetting());
        }
        private List<dynamic> getMappingCoverProgressive(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>(@"SELECT count(1) from
                            (select distinct product_code,coverage_id, bc_up_to_si,lastupdatedtime,rowstatus
                              FROM [AABMobile].[dbo].[Mapping_Cover_Progressive] 
                              ) data WHERE LastUpdatedTime = @0", datetime);

                const string query = @"SELECT distinct product_code as ProductCode,coverage_id as CoverageID, bc_up_to_si as TSI,LastUpdatedTime,RowStatus
                        FROM [AABMobile].[dbo].[Mapping_Cover_Progressive]  where LastUpdatedTime>=@0
                        order by product_code,BC_Up_To_SI  OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Branch(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getBranch(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getBranch(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Branch WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT BranchId,Type,Name,Address,Phone,Fax,Latitude,Longitude,CityId,City,LastUpdatedTime,RowStatus FROM BRANCH WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, BranchId ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Dealer(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getDealer(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getDealer(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Dealer WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT DealerCode,Description,LastUpdatedTime,RowStatus FROM Dealer WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, DealerCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult DtlRateFactor(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getDtlRateFactor(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getDtlRateFactor(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Dtl_Rate_Factor a WITH(NOLOCK) INNER JOIN (SELECT DISTINCT b.Scoring_Code FROM Prd_Interest_Coverage b WITH(NOLOCK) INNER JOIN Product c WITH(NOLOCK) ON b.Product_Code=c.ProductCode) d ON a.Score_Code=d.Scoring_Code WHERE a.LastUpdatedTime = @0", datetime);

                const string query = "SELECT a.Score_Code,a.Insurance_type,a.Coverage_id,a.factor_code,a.sequence_no,a.Status,a.entryusr,a.entrydt,a.updateusr,a.updatedt,a.LastUpdatedTime,a.RowStatus FROM Dtl_Rate_Factor a WITH(NOLOCK) INNER JOIN (SELECT DISTINCT b.Scoring_Code FROM Prd_Interest_Coverage b WITH(NOLOCK) INNER JOIN Product c WITH(NOLOCK) ON b.Product_Code=c.ProductCode) d ON a.Score_Code=d.Scoring_Code WHERE a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Score_Code, a.Insurance_type, a.Coverage_id, a.factor_code ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult DtlRateScoring(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getDtlRateScoring(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getDtlRateScoring(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Dtl_Rate_Scoring a WITH(NOLOCK) INNER JOIN (SELECT DISTINCT b.Scoring_Code FROM Prd_Interest_Coverage b WITH(NOLOCK) INNER JOIN Product c WITH(NOLOCK) ON b.Product_Code=c.ProductCode) d ON a.Score_Code=d.Scoring_Code WHERE a.LastUpdatedTime = @0", datetime);

                const string query = "SELECT a.Coverage_ID,a.Score_code,a.Rate_No,a.BC_Up_To_SI,a.Insurance_Type,a.Rating_value1,a.Rating_value2,a.Rating_value3,a.Rating_value4,a.Rating_value5,a.Rating_value6,a.Rating_value7,a.Rating_value8,a.Rating_value9,a.Rating_value10,a.Rate,a.BC_Premium,a.BC_Max_SI,a.Excess_Rate,a.Status,a.Entryusr,a.Entrydt,a.Updateusr,a.Updatedt,a.LastUpdatedTime,a.RowStatus FROM Dtl_Rate_Scoring a WITH(NOLOCK) INNER JOIN (SELECT DISTINCT b.Scoring_Code FROM Prd_Interest_Coverage b WITH(NOLOCK) INNER JOIN Product c WITH(NOLOCK) ON b.Product_Code=c.ProductCode) d ON a.Score_Code=d.Scoring_Code WHERE a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Coverage_ID, a.Score_code, a.Rate_No ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult FollowUpStatus(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getFollowUpStatus(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getFollowUpStatus(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM FollowUpStatus WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT StatusCode,Description,LastUpdatedTime,RowStatus,SeqNo FROM FollowUpStatus WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, StatusCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult FollowUpStatusInfo(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getFollowUpStatusInfo(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getFollowUpStatusInfo(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM FollowUpStatusInfo WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT InfoCode, StatusCode, Description, LastUpdatedTime, RowStatus, SeqNo FROM FollowUpStatusInfo WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, InfoCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult GardaCenter(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getGardaCenter(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getGardaCenter(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM GardaCenter WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT GardaCenterId,GardaCenterName,Address,Phone,Fax,Latitude,Longitude,CityId,City,IsDeleted,LastUpdatedTime,RowStatus FROM GardaCenter WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, GardaCenterId ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult GraphicColorSetting(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getGraphicColorSetting(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getGraphicColorSetting(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM GraphicColorSetting WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT ID,PctUpTo,ColorCode,EntryDate,LastUpdatedTime,RowStatus FROM GraphicColorSetting WITH(NOLOCK) WHERE LastUpdatedTime >= @0 ORDER BY LastUpdatedTime, ID ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult MstCoverage(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getMstCoverage(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getMstCoverage(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM MstCoverage WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT CoverageID, Description, Sequence, LastUpdatedTime, RowStatus FROM MstCoverage WITH(NOLOCK) WHERE LastUpdatedTime >= @0 ORDER BY LastUpdatedTime, CoverageID ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult MstInterest(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getMstInterest(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getMstInterest(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM MstInterest WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                const string query = "SELECT InterestID, Description, Sequence, LastUpdatedTime, RowStatus FROM MstInterest WITH(NOLOCK) WHERE LastUpdatedTime >= @0 ORDER BY LastUpdatedTime, InterestID ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult PrdAgreedValue(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getPrdAgreedValue(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getPrdAgreedValue(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Prd_Agreed_Value a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime=@0", datetime);

                string query = "SELECT a.Product_Code,a.UPTO,a.Rate,a.CancelRate,a.Status,a.entryusr,a.entrydt,a.updateusr,a.updatedt,a.LastUpdatedTime,a.RowStatus FROM Prd_Agreed_Value a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Product_Code, a.UPTO ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult PrdDeductible(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getPrdDeductible(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getPrdDeductible(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Prd_Deductible a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime= @0", datetime);

                string query = "SELECT a.Product_Code,a.Deductible_Code,a.Description,a.Deductible_Type,a.Bc_From_Si,a.Bc_To_Si,a.Cover_Disc_Pct,a.Flat_Currency,a.Flat_Amount,a.Percent_Si,a.Percent_Claim,a.Bc_Min_Amount,a.Bc_Max_Amount,a.Status,a.EntryUsr,a.EntryDt,a.UpdateUsr,a.UpdateDt,a.LastUpdatedTime,a.RowStatus FROM Prd_Deductible a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Product_Code, a.Deductible_Code ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";
                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult PrdInterestCoverage(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getPrdInterestCoverage(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getPrdInterestCoverage(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Prd_Interest_Coverage a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime= @0", datetime);

                string query = "SELECT a.Product_Code,a.Interest_ID,a.Coverage_Id,a.Given_SI,a.Given_Curr_ID,a.Rate,a.Bc_Min_Premium,a.Bc_Max_Si,a.Excess_Rate,a.Scoring_Code,a.Change_Rate,a.Special_Commission,a.Discounts,a.IC_Status,a.entryusr,a.entrydt,a.updateusr,a.updatedt,a.LastUpdatedTime,a.RowStatus FROM Prd_Interest_Coverage a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Product_Code, a.Interest_ID, a.Coverage_Id ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult PrdLoadMv(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getPrdLoadMv(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getPrdLoadMv(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Prd_Load_Mv a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime= @0", datetime);

                string query = "SELECT a.Product_Code,a.Insurance_Code,a.Coverage_Id,a.Age,a.Load_Pct,a.Status,a.EntryUsr,a.EntryDt,a.UpdateUsr,a.UpdateDt,a.LastUpdatedTime,a.RowStatus FROM Prd_Load_Mv a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Product_Code, a.Insurance_Code, a.Coverage_Id, a.Age ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult PrdPolicyFee(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getPrdPolicyFee(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getPrdPolicyFee(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM PRD_POLICY_FEE a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime=@0", datetime);

                string query = "SELECT a.Product_Code,a.Order_Type,a.Curr_Id,a.Lower_Limit,a.Policy_Fee1,a.Stamp_Duty1,a.Policy_Fee2,a.Stamp_Duty2,a.Policy_Fee3,a.Stamp_Duty3,a.Policy_Fee4,a.Stamp_Duty4,a.Fee_Flags,a.Status,a.entryusr,a.entrydt,a.updateusr,a.updatedt,a.Print_Amount1,a.Print_Amount2,a.Print_Amount3,a.Print_Amount4,a.LastUpdatedTime,a.RowStatus FROM PRD_POLICY_FEE a WITH(NOLOCK) INNER JOIN Product b WITH(NOLOCK) ON a.Product_Code=b.ProductCode WHERE a.LastUpdatedTime>=@0 ORDER BY a.LastUpdatedTime, a.Product_Code, a.Order_Type, a.Curr_Id, a.Lower_Limit ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Product(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getProduct(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getProduct(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Product WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT ProductCode,Description,ProductTypeCode,LastUpdatedTime,RowStatus,InsuranceType FROM Product WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, ProductCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        // TODO: start from here.
        [HttpPost]
        public IHttpActionResult ProductAgent(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getProductAgent(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getProductAgent(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM ProductAgent WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT ProductType, ProductCode, ChannelSource, LastUpdatedTime, RowStatus FROM ProductAgent WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, ChannelSource, ProductType ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult ProductType(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getProductType(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getProductType(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM ProductType WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT ProductTypeCode,Description,LastUpdatedTime,RowStatus,InsuranceType FROM ProductType WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, ProductTypeCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Region(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getRegion(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getRegion(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Region WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT RegionCode,Description,Sequence,LastUpdatedTime,RowStatus FROM Region WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, RegionCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult SalesmanDealer(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getSalesmanDealer(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getSalesmanDealer(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM SalesmanDealer WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT SalesmanCode,Description,DealerCode,LastUpdatedTime,RowStatus,BeyondCustomerID FROM SalesmanDealer WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, SalesmanCode, DealerCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult ServerDate()
        {
            response = new OtosalesAPIResult();
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                response.Status = true;
                response.Data = db.ExecuteScalar<string>("SELECT Date FROM vwGetServerDate");

                return Json(response, Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult SysParam(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getSysParam(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getSysParam(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM SysParam WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT Param,Type,Value,LastUpdatedTime,RowStatus FROM SysParam WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, Param ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Usage(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getUsage(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getUsage(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Dtl_Ins_Factor WITH(NOLOCK) WHERE (Factor_Code in ('GEOGRA') OR (Factor_Code = 'MVUSAG' and insurance_code like 'U%') OR (Factor_Code = 'VHCTYP' and insurance_code like 'T0%')) AND LastUpdatedTime = @0", datetime);

                string query = "SELECT Factor_Code,insurance_code,Description,status,Entryusr,Entrydt,UpdateUsr,UpdateDt,LastUpdatedTime,RowStatus FROM Dtl_Ins_Factor WITH(NOLOCK) WHERE (Factor_Code in ('GEOGRA') OR (Factor_Code = 'MVUSAG' and insurance_code like 'U%') OR (Factor_Code = 'VHCTYP' and insurance_code like 'T0%')) AND LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, Factor_Code, insurance_code ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Vehicle(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getVehicle(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getVehicle(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Vehicle WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT VehicleCode,BrandCode,ProductTypeCode,ModelCode,Series,Type,Sitting,Year,CityCode,Price,LastUpdatedTime,RowStatus FROM Vehicle WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, VehicleCode, BrandCode, ProductTypeCode, ModelCode, Series, Year, CityCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult VehicleBrand(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getVehicleBrand(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getVehicleBrand(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM VehicleBrand WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT BrandCode,ProductTypeCode,Description,LastUpdatedTime,RowStatus FROM VehicleBrand WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, BrandCode, ProductTypeCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult VehicleModel(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getVehicleModel(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getVehicleModel(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM VehicleModel WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT BrandCode,ProductTypeCode,ModelCode,Description,LastUpdatedTime,RowStatus FROM VehicleModel WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, BrandCode, ProductTypeCode, ModelCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult VehiclePriceTolerance(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getVehiclePriceTolerance(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getVehiclePriceTolerance(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM VehiclePriceTolerance WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT ID, IsNew, IsATPM, FormType, VehicleCategory, MaxSI, ToleranceType, TolerancePct, ToleranceAmt, LastUpdatedTime, RowStatus FROM VehiclePriceTolerance WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, ID ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult VehicleSeries(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getVehicleSeries(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getVehicleSeries(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM VehicleSeries WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT BrandCode,ProductTypeCode,ModelCode,Series,LastUpdatedTime,RowStatus FROM VehicleSeries WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, BrandCode, ProductTypeCode, ModelCode, Series ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult VehicleType(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getVehicleType(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getVehicleType(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM VehicleType WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT VehicleTypeCode,ProductTypeCode,CategoryCode,Description,LastUpdatedTime,RowStatus FROM VehicleType WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, VehicleTypeCode ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult Workshop(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = int.Parse(form.Get("count"));
            return Json(getWorkshop(datetime, count), Util.jsonSerializerSetting());
        }

        private List<dynamic> getWorkshop(string datetime, int count)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM Workshop WITH(NOLOCK) WHERE LastUpdatedTime = @0", datetime);

                string query = "SELECT WorkshopId,WorkshopName,Address,Phone,Fax,Latitude,Longitude,CityId,City,IsDeleted,LastUpdatedTime,RowStatus FROM Workshop WITH(NOLOCK) WHERE LastUpdatedTime>=@0 ORDER BY LastUpdatedTime, WorkshopId ASC OFFSET @1 ROWS FETCH NEXT @2 ROWS ONLY";

                return db.Fetch<dynamic>(query, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        #endregion

        #region Transactional Data

        [HttpPost]
        public IHttpActionResult EmailNotification(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getEmailNotification(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getEmailNotification(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM EmailNotification WITH(NOLOCK) WHERE UserID = @0 AND LastUpdatedTime = @1", userID, datetime);

                const string query = "SELECT EmailNotificationNo,NotificationType,ParameterName,ParameterValue,EmailSendStatus,EmailDeliveryDate,UserID,EntryDate,LastUpdatedTime,RowStatus FROM EmailNotification WITH(NOLOCK) WHERE UserID = @0 and LastUpdatedTime>=@1 ORDER BY LastUpdatedTime, EmailNotificationNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult EmailQuotation(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getEmailQuotation(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getEmailQuotation(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM EmailQuotation WITH(NOLOCK) WHERE SalesOfficerID = @0 AND LastUpdatedTime = @1", userID, datetime);

                const string query = "SELECT EmailNo,OrderNo,Email,EmailSendStatus,EmailDeliveryDate,SalesOfficerID,EntryDate,LastUpdatedTime,RowStatus FROM EmailQuotation WITH(NOLOCK) WHERE SalesOfficerID = @0 and LastUpdatedTime>=@1 ORDER BY LastUpdatedTime, EmailNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult EmailSummary(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getEmailSummary(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getEmailSummary(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM EmailSummary WITH(NOLOCK) WHERE SalesOfficerID = @0 AND LastUpdatedTime = @1", userID, datetime);

                const string query = "SELECT EmailSummaryNo,ReportType,Period,Email,EmailSendStatus,EmailDeliveryDate,UserID,EntryDate,LastUpdatedTime,RowStatus FROM EmailSummary WITH(NOLOCK) WHERE UserID = @0 and LastUpdatedTime>=@1 ORDER BY LastUpdatedTime, EmailSummaryNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult FollowUp(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getFollowUp(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getFollowUp(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM FollowUp WITH(NOLOCK) WHERE SalesOfficerID = @0 AND LastUpdatedTime = @1", userID, datetime);

                const string query = "SELECT FollowUpNo,LastSeqNo,CustID,ProspectName,Phone1,Phone2,SalesOfficerID,EntryDate,FollowUpName,NextFollowUpDate,LastFollowUpDate,FollowUpStatus,FollowUpInfo,Remark,BranchCode,LastUpdatedTime,RowStatus,PolicyId,PolicyNo,TransactionNo,SalesAdminID,SendDocDate,IdentityCard,STNK,SPPAKB,BSTB1,BSTB2,BSTB3,BSTB4,CheckListSurvey1,CheckListSurvey2,CheckListSurvey3,CheckListSurvey4,PaymentReceipt1,PaymentReceipt2,PaymentReceipt3,PaymentReceipt4,PremiumCal1,PremiumCal2,PremiumCal3,PremiumCal4,FormA1,FormA2,FormA3,FormA4,FormB1,FormB2,FormB3,FormB4,FormC1,FormC2,FormC3,FormC4 FROM FollowUp WITH(NOLOCK) WHERE SalesOfficerID = @0 and LastUpdatedTime>=@1 ORDER BY LastUpdatedTime, FollowUpNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult FollowUpHistory(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getFollowUpHistory(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getFollowUpHistory(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM FollowUpHistory WITH(NOLOCK) WHERE SalesOfficerID = @0 AND LastUpdatedTime = @1", userID, datetime);

                const string query = "SELECT FollowUpNo,SeqNo,CustID,ProspectName,Phone1,Phone2,SalesOfficerID,EntryDate,FollowUpName,NextFollowUpDate,LastFollowUpDate,FollowUpStatus,FollowUpInfo,Remark,BranchCode,LastUpdatedTime,RowStatus FROM FollowUpHistory WITH(NOLOCK) WHERE SalesOfficerID = @0 and LastUpdatedTime>=@1 ORDER BY LastUpdatedTime, FollowUpNo, SeqNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult FollowUpSummary(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getFollowUpSummary(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getFollowUpSummary(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                string sqlQuery = "SELECT SalesOfficerID,Email,Name,Role,Class,Password,Validation,Expiry,Password_Expiry,Department,BranchCode,Phone1,Phone2,Phone3,OfficePhone,Ext,Fax,EntryDate,LastUpdatedTime,RowStatus FROM SalesOfficer WITH(NOLOCK) WHERE SalesOfficerID=@0";

                SalesOfficer soItem = db.FirstOrDefault<SalesOfficer>(sqlQuery, userID);

                if (soItem.Role == "BRANCHSUPPORT" || soItem.Role == "NATIONALMGR")
                {
                    sqlQuery = "SELECT SalesOfficerID,SalesName,BranchCode,BranchDesc,Region,Period,LastUpdatedTime,RowStatus,TotalProspect,TotalNeedFu,TotalPotential,TotalCallLater,TotalCollectDoc,TotalNotDeal,TotalSendToSA,TotalPolicyCreated,TotalBackToAo,TotalDeal,Frequency,[National] FROM FollowUpSummary WITH(NOLOCK) WHERE LastUpdatedTime > @1 ORDER BY LastUpdatedTime, SalesOfficerID, BranchCode, Region, Period ASC";
                }
                else if (soItem.Role == "REGIONALMGR")
                {
                    sqlQuery = "SELECT fuh.SalesOfficerID,fuh.SalesName,fuh.BranchCode,fuh.BranchDesc,fuh.Region,fuh.Period,fuh.LastUpdatedTime,fuh.RowStatus,fuh.TotalProspect,fuh.TotalNeedFu,fuh.TotalPotential,fuh.TotalCallLater,fuh.TotalCollectDoc,fuh.TotalNotDeal,fuh.TotalSendToSA,fuh.TotalPolicyCreated,fuh.TotalBackToAo,fuh.TotalDeal,fuh.Frequency,fuh.[National] FROM FollowUpSummary fuh WITH(NOLOCK) INNER JOIN AABHead ah WITH(NOLOCK) ON fuh.Region=ah.Param ";
                    sqlQuery += "WHERE ah.Value = @0 AND ah.Type = 'Region' AND fuh.LastUpdatedTime > @1 ";
                    sqlQuery += "ORDER BY fuh.LastUpdatedTime, fuh.SalesOfficerID, fuh.BranchCode, fuh.Region, fuh.Period ASC";
                }
                //DTK : SALESSECHEAD KELUPAAN!!!
                //else if (soItem.Role == "BRANCHMGR")
                else if (soItem.Role == "BRANCHMGR" || soItem.Role == "SALESSECHEAD")
                {
                    sqlQuery = "SELECT fuh.SalesOfficerID,fuh.SalesName,fuh.BranchCode,fuh.BranchDesc,fuh.Region,fuh.Period,fuh.LastUpdatedTime,fuh.RowStatus,fuh.TotalProspect,fuh.TotalNeedFu,fuh.TotalPotential,fuh.TotalCallLater,fuh.TotalCollectDoc,fuh.TotalNotDeal,fuh.TotalSendToSA,fuh.TotalPolicyCreated,fuh.TotalBackToAo,fuh.TotalDeal,fuh.Frequency,fuh.[National] FROM FollowUpSummary fuh WITH(NOLOCK) INNER JOIN AABHead ah WITH(NOLOCK) ON fuh.BranchCode=ah.Param ";
                    sqlQuery += "WHERE ah.Value = @0 AND ah.Type = 'Branch' AND fuh.LastUpdatedTime > @1 ";
                    sqlQuery += "ORDER BY fuh.LastUpdatedTime, fuh.SalesOfficerID, fuh.BranchCode, fuh.Region, fuh.Period ASC";
                }
                else
                {
                    sqlQuery = "SELECT SalesOfficerID,SalesName,BranchCode,BranchDesc,Region,Period,LastUpdatedTime,RowStatus,TotalProspect,TotalNeedFu,TotalPotential,TotalCallLater,TotalCollectDoc,TotalNotDeal,TotalSendToSA,TotalPolicyCreated,TotalBackToAo,TotalDeal,Frequency,[National] FROM FollowUpSummary WITH(NOLOCK) WHERE SalesOfficerID = @0 AND LastUpdatedTime > @1 ORDER BY LastUpdatedTime, SalesOfficerID, BranchCode, Region, Period ASC";
                }

                return db.Fetch<dynamic>(sqlQuery, userID, datetime);
            }
        }

        [HttpPost]
        public IHttpActionResult OrderSimulation(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getOrderSimulation(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getOrderSimulation(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM OrderSimulation a WITH(NOLOCK) inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo WHERE a.SalesOfficerID = @0 and a.LastUpdatedTime= @1", userID, datetime);

                string query = "SELECT a.OrderNo,a.CustID,a.FollowUpNo,a.QuotationNo,a.MultiYearF,a.YearCoverage,a.TLOPeriod,a.ComprePeriod,a.BranchCode,a.SalesOfficerID,a.PhoneSales,a.DealerCode,a.SalesDealer,a.ProductTypeCode,a.ProductCode,a.AdminFee,a.TotalPremium,a.Phone1,a.Phone2,a.Email1,a.Email2,a.SendStatus,a.SendDate,a.EntryDate,a.LastUpdatedTime,a.RowStatus,a.InsuranceType,a.ApplyF,a.SendF,a.LastInterestNo,a.LastCoverageNo FROM OrderSimulation a WITH(NOLOCK) inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo WHERE a.SalesOfficerID = @0 and a.LastUpdatedTime>=@1 ORDER BY a.LastUpdatedTime, a.OrderNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }



        [HttpPost]
        public IHttpActionResult OrderSimulationCoverage(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getOrderSimulationCoverage(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getOrderSimulationCoverage(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM OrderSimulation a WITH(NOLOCK) " +
                        "inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo \n" +
                        "inner join OrderSimulationInterest c WITH(NOLOCK) on b.OrderNo=c.OrderNo and b.ObjectNo=c.ObjectNo \n" +
                        "inner join OrderSimulationCoverage d WITH(NOLOCK) on c.OrderNo=d.OrderNo and c.ObjectNo=d.ObjectNo and c.InterestNo=d.InterestNo \n" +
                        "WHERE a.SalesOfficerID = @0 and d.LastUpdatedTime=@1", userID, datetime);

                string query = "SELECT d.OrderNo,d.ObjectNo,d.InterestNo,d.CoverageNo,d.CoverageID,d.Rate,d.SumInsured,d.LoadingRate,d.Loading,d.Premium,d.BeginDate,d.EndDate,d.LastUpdatedTime,d.RowStatus, d.IsBundling FROM OrderSimulation a WITH(NOLOCK) inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo \n" +
                        "inner join OrderSimulationInterest c WITH(NOLOCK) on b.OrderNo=c.OrderNo and b.ObjectNo=c.ObjectNo \n" +
                        "inner join OrderSimulationCoverage d WITH(NOLOCK) on c.OrderNo=d.OrderNo and c.ObjectNo=d.ObjectNo and c.InterestNo=d.InterestNo \n" +
                        "WHERE a.SalesOfficerID = @0 and d.LastUpdatedTime>=@1 ORDER BY d.LastUpdatedTime, d.OrderNo, d.ObjectNo, d.InterestNo, d.CoverageNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult OrderSimulationInterest(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getOrderSimulationInterest(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getOrderSimulationInterest(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM OrderSimulation a WITH(NOLOCK) 
                    inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo 
                    inner join OrderSimulationInterest c WITH(NOLOCK) on b.OrderNo=c.OrderNo and b.ObjectNo=c.ObjectNo 
                    WHERE a.SalesOfficerID = @0 and c.LastUpdatedTime=@1", userID, datetime);

                string query = @"SELECT c.OrderNo, c.ObjectNo, c.InterestNo, c.InterestID, c.Year, c.Premium, c.PeriodFrom, c.PeriodTo, c.LastUpdatedTime, c.RowStatus 
                    FROM OrderSimulation a WITH(NOLOCK) 
                    inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo
                    inner join OrderSimulationInterest c WITH(NOLOCK) on b.OrderNo=c.OrderNo and b.ObjectNo=c.ObjectNo
                    WHERE a.SalesOfficerID = @0 and c.LastUpdatedTime>=@1 ORDER BY c.LastUpdatedTime, c.OrderNo, c.ObjectNo, c.InterestNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        [HttpPost]
        public IHttpActionResult OrderSimulationMV(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getOrderSimulationMV(datetime, count, userID), Util.jsonSerializerSetting());

        }

        private List<dynamic> getOrderSimulationMV(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM OrderSimulation a WITH(NOLOCK) 
                    inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo 
                    WHERE a.SalesOfficerID = @0 and b.LastUpdatedTime= @1", userID, datetime);

                string query = @"SELECT b.OrderNo, b.ObjectNo, b.ProductTypeCode, b.VehicleCode, b.BrandCode, b.ModelCode, b.Series, b.Type, b.Sitting, b.Year, b.CityCode, b.UsageCode, b.SumInsured, b.AccessSI, b.RegistrationNumber, b.EngineNumber, b.ChasisNumber, b.LastUpdatedTime, b.RowStatus, b.IsNew 
                    FROM OrderSimulation a WITH(NOLOCK) 
                    inner join OrderSimulationMV b WITH(NOLOCK) on a.OrderNo=b.OrderNo 
                    WHERE a.SalesOfficerID = @0 and b.LastUpdatedTime>=@1 ORDER BY b.LastUpdatedTime, b.OrderNo, b.ObjectNo ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";
                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        public IHttpActionResult ProspectCustomer(FormDataCollection form)
        {
            string datetime = form.Get("datetime");
            int count = Int32.Parse(form.Get("count"));
            string userID = form.Get("userID");

            return Json(getProspectCustomer(datetime, count, userID), Util.jsonSerializerSetting());
        }

        private List<dynamic> getProspectCustomer(string datetime, int count, string userID)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                int totalcount = db.ExecuteScalar<int>("SELECT COUNT(*) TotalCount FROM ProspectCustomer WITH(NOLOCK) WHERE SalesOfficerID = @0 AND LastUpdatedTime = @1", userID, datetime);

                string query = "SELECT CustID,Name,Phone1,Phone2,Email1,Email2,CustIDAAB,SalesOfficerID,DealerCode,SalesDealer,BranchCode,EntryDate,LastUpdatedTime,RowStatus FROM ProspectCustomer WITH(NOLOCK) WHERE SalesOfficerID = @0 and LastUpdatedTime>=@1 ORDER BY LastUpdatedTime, CustID ASC OFFSET @2 ROWS FETCH NEXT @3 ROWS ONLY";

                return db.Fetch<dynamic>(query, userID, datetime, count > totalcount ? totalcount : count, MAXFETCH);
            }
        }

        #endregion

        #region 0328/URF/2015 - EKI - GetPenawaran, GetHistoryPenawaran

        [HttpPost]
        public IHttpActionResult Penawaran(FormDataCollection form)
        {
            response = new OtosalesAPIResult();
            string userID = form.Get("userID");

            Util.CheckAllMGOBidExpiry();

            int isSendBid = 0;

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                isSendBid = db.ExecuteScalar<int>("SELECT IsSendBid FROM SalesOfficer WHERE SalesOfficerID = @0", userID);
            }

            if (isSendBid > 0)
            {
                DateTime offdate = Convert.ToDateTime(Otosales.Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALESANDRIOD-OFFDATE")[0]);
                if (DateTime.Now > offdate)
                {
                    response.Data = new List<dynamic>();
                    response.Status = true;
                }
                else
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                    {
                        // 001 begin
                        // int hotProspect = db.ExecuteScalar<int>("SELECT COUNT(*) AS CountHotProspect FROM Penawaran WHERE isHotProspect = 1 AND BidStatus = 0 AND fuByCSOBranch = 0");
                        int hotProspect = db.ExecuteScalar<int>("SELECT COUNT(*) AS CountHotProspect FROM Penawaran WHERE isHotProspect = 1 AND BidStatus = 0 AND fuByCSOBranch = 0 AND RowStatus = 1");
                        // 001 end

                        string query;
                        if (hotProspect > 0)
                        {
                            query = "SELECT policyID, policyCode, policyGeneratedDate, agentCode, agentEmail, agentFullName, customerName, customerEmail, customerPhone, ";
                            query += "policeNumber, productCode, protectionType, vehicleCode, vehicleType, vehicleDescription, vehicleYear, usageCode, usageText, ";
                            query += "areaCode, totalSumInsured, baseRate, isTJH, tjhRate, tjhValue, isPaDriver, paDriverRate, paDriverValue, isPaPass, ";
                            query += "paPassRate, paPassQuantity, paPassValue, isAutoApply, srccRate, etvRate, fldRate, isCheckSrcc, autoApplyRate, isTRS, ";
                            query += "trsRate, isAcc, accRate, accValue, loadingRate, administrationPrice, basicPremi, accPremi, totalPremiKendaraanPlusAksesoris, ";
                            query += "premiLoading, totalPremiDasarPlusLoading, premiAutoApply, premiTRS, premiTJH, premiPADriver, premiPAPass, totalPremiPerluasan, ";
                            query += "totalPremiDasarPlusPerluasan, totalAllPremi, infoModified, BidStatus, LastUpdatedTime, Domisili, followUpDateTime, isHotProspect, fuByCSOBranch ";
                            // 001 begin
                            // query += "FROM Penawaran WHERE BidStatus = 0 AND fuByCSOBranch = 0 AND isHotProspect = 1 ";
                            query += "FROM Penawaran WHERE BidStatus = 0 AND fuByCSOBranch = 0 AND isHotProspect = 1 AND RowStatus = 1 ";
                            // 001 end
                            query += "AND ((followUpDateTime is NULL AND getdate() > DATEADD(HOUR, " + ConfigurationManager.AppSettings["DelayWOPT"].ToString() + ", infoModified)) OR (followUpDateTime < DATEADD(HOUR, " + ConfigurationManager.AppSettings["DelayWPT"].ToString() + ", getdate()))) ";
                            query += "ORDER BY isHotProspect DESC, infoModified DESC, policyID ASC";
                        }
                        else
                        {
                            query = "SELECT policyID, policyCode, policyGeneratedDate, agentCode, agentEmail, agentFullName, customerName, customerEmail, customerPhone, ";
                            query += "policeNumber, productCode, protectionType, vehicleCode, vehicleType, vehicleDescription, vehicleYear, usageCode, usageText, ";
                            query += "areaCode, totalSumInsured, baseRate, isTJH, tjhRate, tjhValue, isPaDriver, paDriverRate, paDriverValue, isPaPass, ";
                            query += "paPassRate, paPassQuantity, paPassValue, isAutoApply, srccRate, etvRate, fldRate, isCheckSrcc, autoApplyRate, isTRS, ";
                            query += "trsRate, isAcc, accRate, accValue, loadingRate, administrationPrice, basicPremi, accPremi, totalPremiKendaraanPlusAksesoris, ";
                            query += "premiLoading, totalPremiDasarPlusLoading, premiAutoApply, premiTRS, premiTJH, premiPADriver, premiPAPass, totalPremiPerluasan, ";
                            query += "totalPremiDasarPlusPerluasan, totalAllPremi, infoModified, BidStatus, LastUpdatedTime, Domisili, followUpDateTime, isHotProspect, fuByCSOBranch ";
                            // 001 begin
                            // query += "FROM Penawaran WHERE BidStatus = 0 AND fuByCSOBranch = 0 ";
                            query += "FROM Penawaran WHERE BidStatus = 0 AND fuByCSOBranch = 0 AND RowStatus = 1 ";
                            // 001 end
                            query += "AND ((followUpDateTime is NULL AND getdate() > DATEADD(HOUR, " + ConfigurationManager.AppSettings["DelayWOPT"].ToString() + ", infoModified)) OR (followUpDateTime < DATEADD(HOUR, " + ConfigurationManager.AppSettings["DelayWPT"].ToString() + ", getdate()))) ";
                            query += "ORDER BY isHotProspect DESC, infoModified DESC, policyID ASC";
                        }
                        response.Data = db.Fetch<Penawaran>(query);
                        response.Status = true;
                    }
                }
            }
            else
            {
                response.Status = false;
                response.Message = "You are not authorized to perform this action.";
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        /// <summary>
        /// Conversion from GetReplicatePenawaran method
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult ReplicatePenawaran(FormDataCollection form)
        {
            response = new OtosalesAPIResult();
            List<HistoryPenawaran> historyPenawaranList = new List<HistoryPenawaran>();
            List<Penawaran> penawaranList = new List<Penawaran>();

            string userID = form.Get("userID");
            List<int> policyIds = JsonConvert.DeserializeObject<List<int>>(form.Get("policyIds"));


            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
            {
                string query = "SELECT HistoryPenawaranID, PolicyID, OrderNo, ExpiredDate, LastUpdatedTime, SalesOfficerID, IsAbleExpired FROM HistoryPenawaran WHERE isAbleExpired = 1 AND GETDATE() < ExpiredDate AND SalesOfficerID = @0 ";

                if (policyIds.Count > 0)
                {
                    query += "AND PolicyID NOT IN ( ";
                    for (int i = 0; i < policyIds.Count; i++)
                    {
                        query += policyIds[i].ToString();
                        if (i != policyIds.Count - 1)
                            query += ", ";
                        else
                            query += " )";
                    }
                }
                query += "ORDER BY PolicyID, LastUpdatedTime DESC";
                historyPenawaranList = db.Fetch<HistoryPenawaran>(query, userID);

                if (historyPenawaranList.Count > 0)
                {
                    query = "SELECT policyID, policyCode, policyGeneratedDate, agentCode, agentEmail, agentFullName, customerName, customerEmail, customerPhone, ";
                    query += "policeNumber, productCode, protectionType, vehicleCode, vehicleType, vehicleDescription, vehicleYear, usageCode, usageText, ";
                    query += "areaCode, totalSumInsured, baseRate, isTJH, tjhRate, tjhValue, isPaDriver, paDriverRate, paDriverValue, isPaPass, ";
                    query += "paPassRate, paPassQuantity, paPassValue, isAutoApply, srccRate, etvRate, fldRate, isCheckSrcc, autoApplyRate, isTRS, ";
                    query += "trsRate, isAcc, accRate, accValue, loadingRate, administrationPrice, basicPremi, accPremi, totalPremiKendaraanPlusAksesoris, ";
                    query += "premiLoading, totalPremiDasarPlusLoading, premiAutoApply, premiTRS, premiTJH, premiPADriver, premiPAPass, totalPremiPerluasan, ";
                    query += "totalPremiDasarPlusPerluasan, totalAllPremi, infoModified, BidStatus, LastUpdatedTime, Domisili, followUpDateTime, isHotProspect, fuByCSOBranch ";
                    // 001 begin
                    // query += "FROM Penawaran WHERE PolicyID IN ( ";
                    query += "FROM Penawaran WHERE RowStatus = 1 AND PolicyID IN ( ";
                    // 001 end

                    for (int i = 0; i < historyPenawaranList.Count; i++)
                    {
                        query += historyPenawaranList[i].PolicyID;

                        if (i != historyPenawaranList.Count - 1)
                        {
                            query += ", ";
                        }
                        else
                        {
                            query += " )";
                        }
                    }

                    penawaranList = db.Fetch<Penawaran>(query);
                    response.Status = true;
                    response.Data = new { Penawaran = penawaranList, HistoryPenawaran = historyPenawaranList };
                    response.Message = "Penawaran and HistoryPenawaran available";
                }
                else
                {
                    response.Status = false;
                    response.Message = "There is no Penawaran and HistoryPenawaran";
                }
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        #endregion

        #region 0125/URF/2015 OrderApproval

        [HttpPost]
        public IHttpActionResult OrderApproval(FormDataCollection form)
        {
            string userID = form.Get("userID");
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                // 002 begin
                string query = @"
                    SELECT 
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, msom.Order_ID, mo.Name_On_Policy as Name, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    msom.isSO, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder 
                    FROM Mst_Order mo 
                    LEFT JOIN Mst_Order_Mobile msom ON mo.Order_No = msom.Order_No 
                    LEFT JOIN Order_Mobile_Notification omn ON omn.Order_ID = msom.Order_ID 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status  
                    WHERE msom.Approval_Status = 0 AND omn.User_Id = @0
                    AND mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') 
                    AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    GROUP BY mo.Order_No, mo.Name_On_Policy, mo.Broker_Code, 
                    mcp.Name, msom.Order_ID, msom.Account_Info_BankName, 
                    msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, msom.isSO
                ";
                // 002 end

                return Json(db.Fetch<dynamic>(query, userID), Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult OrderApprovalDetail(FormDataCollection form)
        {
            response = new OtosalesAPIResult();
            int OrderID = Int32.Parse(form.Get("OrderID"));

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                // 002 begin
                string query = @"
                    SELECT LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    FROM [dbo].[Mst_Order] mo 
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
                    WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    AND msom.Order_ID = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY mo.Order_No, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC
                ";
                // 002 end

                return Json(db.FirstOrDefault<dynamic>(query, OrderID), Util.jsonSerializerSetting());
            }
        }

        [HttpPost]
        public IHttpActionResult OrderApprovalDetailObject(FormDataCollection form)
        {
            string OrderNo = form.Get("OrderNo");

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                string query = @"
                    SELECT os.Order_No, osmv.Object_No, 
                    CONCAT(LTRIM(RTRIM(msvb.Description)), ' ', LTRIM(RTRIM(msvm.Description)), ' ', LTRIM(RTRIM(osmv.Series))) as VehicleName, 
                    ROUND(osmv.Mfg_Year,0) as 'Year', osmv.Registration_Number as PoliceNumber, mi.agreed_value, 
                    osi.Interest_Code, mi.Description as 'Interest_Description', osc.Coverage_Id, mco.Name as 'Coverage_Description', osc.Ndays, osc.Begin_Date, osc.End_Date, osc.Rate, osc.Sum_Insured, 
                    osc.Loading as LoadingRate, osc.Cover_Premium FROM Mst_Order os 
                    LEFT JOIN Ord_Dtl_MV osmv ON os.Order_No = osmv.Order_No 
                    LEFT JOIN Ord_Dtl_Interest osi ON osmv.Order_No = osi.Order_no AND 
                    osmv.Object_No = osi.Object_No 
                    LEFT JOIN Ord_Dtl_Coverage osc ON osc.Order_No = osi.Order_no AND 
                    osc.Interest_No = osi.Interest_No AND osc.Object_No = osi.Object_No 
                    LEFT JOIN Mst_Interest mi ON osi.Interest_Code = mi.interest_id 
                    LEFT JOIN Mst_Coverage mco ON mco.Coverage_Id = osc.Coverage_Id 
                    LEFT JOIN Mst_Vehicle_Brand msvb ON msvb.Brand_id = osmv.Brand_Code 
                    LEFT JOIN Mst_Vehicle_Model msvm ON msvm.Model_id = osmv.Model_Code 
                    WHERE os.Order_No = @0 
                    ORDER BY osmv.Object_No, osc.End_Date, osc.Coverage_No, osi.Interest_No ASC
                ";

                decimal ndays;
                List<OrderApprovalDetailObject> oaDetailObjects = db.Fetch<OrderApprovalDetailObject>(query, OrderNo);
                List<OrderApprovalDetailObject> oaDetailObjectYearly = new List<OrderApprovalDetailObject>();

                // split objects per year
                for (int i = 0; i < oaDetailObjects.Count; i++)
                {
                    if (oaDetailObjects[i].Ndays <= 365 || oaDetailObjects[i].aggreed_value.Equals(true))
                    {
                        OrderApprovalDetailObject oaObject = new OrderApprovalDetailObject();
                        oaObject.Order_No = oaDetailObjects[i].Order_No;
                        oaObject.Object_No = oaDetailObjects[i].Object_No;
                        oaObject.VehicleName = oaDetailObjects[i].VehicleName;
                        oaObject.Interest_Code = oaDetailObjects[i].Interest_Code;
                        oaObject.Interest_Description = oaDetailObjects[i].Interest_Description;
                        oaObject.Coverage_Id = oaDetailObjects[i].Coverage_Id;
                        oaObject.Coverage_Description = oaDetailObjects[i].Coverage_Description;
                        oaObject.Begin_Date = oaDetailObjects[i].Begin_Date;
                        oaObject.End_Date = oaDetailObjects[i].End_Date;
                        oaObject.Year = oaDetailObjects[i].Year;
                        oaObject.PoliceNumber = oaDetailObjects[i].PoliceNumber;
                        oaObject.Rate = oaDetailObjects[i].Rate;
                        oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured;
                        oaObject.LoadingRate = oaDetailObjects[i].LoadingRate;
                        oaObject.Cover_Premium = oaDetailObjects[i].Cover_Premium;

                        oaDetailObjectYearly.Add(oaObject);
                    }
                    else
                    {
                        ndays = Convert.ToDecimal(oaDetailObjects[i].Ndays);
                        decimal yearDuration = ndays / 365;
                        int years = 0;

                        for (int j = 0; j < ndays; j += 365)
                        {
                            OrderApprovalDetailObject oaObject = new OrderApprovalDetailObject();
                            oaObject.Order_No = oaDetailObjects[i].Order_No;
                            oaObject.Object_No = oaDetailObjects[i].Object_No;
                            oaObject.VehicleName = oaDetailObjects[i].VehicleName;
                            oaObject.Interest_Code = oaDetailObjects[i].Interest_Code;
                            oaObject.Interest_Description = oaDetailObjects[i].Interest_Description;
                            oaObject.Coverage_Id = oaDetailObjects[i].Coverage_Id;
                            oaObject.Coverage_Description = oaDetailObjects[i].Coverage_Description;
                            DateTime date = Convert.ToDateTime(oaDetailObjects[i].Begin_Date);
                            oaObject.Begin_Date = date.AddYears(years);
                            oaObject.End_Date = oaDetailObjects[i].End_Date;
                            oaObject.Year = oaDetailObjects[i].Year;
                            oaObject.PoliceNumber = oaDetailObjects[i].PoliceNumber;
                            oaObject.Rate = oaDetailObjects[i].Rate;
                            oaObject.LoadingRate = oaDetailObjects[i].LoadingRate;
                            oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured; // 003
                            if ((ndays - j) < 365)
                            {
                                //oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured * (ndays - j) / ndays; // 003
                                oaObject.Cover_Premium = oaDetailObjects[i].Cover_Premium * (ndays - j) / ndays;
                            }
                            else
                            {
                                //oaObject.Sum_Insured = oaDetailObjects[i].Sum_Insured * 365 / ndays; // 003
                                oaObject.Cover_Premium = oaDetailObjects[i].Cover_Premium * 365 / ndays;
                            }
                            oaDetailObjectYearly.Add(oaObject);
                            years++;
                        }
                    }
                }

                return Json(oaDetailObjectYearly, Util.jsonSerializerSetting());
            }
        }

        #endregion

    }
}
