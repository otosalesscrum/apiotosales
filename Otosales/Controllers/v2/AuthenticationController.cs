﻿using a2is.Framework.DataAccess;
using a2is.Framework.Security;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Configuration;
using System.Web.Http;

namespace Otosales.Controllers.v2
{
    [AllowAnonymous]
    public class AuthenticationController : OtosalesBaseController
    {
        #region Declare

        private static string GLOBAL_APPLICATION_CODE = WebConfigurationManager.AppSettings["GlobalApplicationCode"];
        private static string GLOBAL_SERVICE_ADDRESS = WebConfigurationManager.AppSettings["GlobalServiceAddress"];

        private static string JWT_SIGNING_KEY = WebConfigurationManager.AppSettings["JWTSigningKey"];
        private static string JWT_VALID_HOURS = WebConfigurationManager.AppSettings["JWTValidHours"];

        private static bool ALLOW_UNTRUSTED_SSL = Int32.Parse(WebConfigurationManager.AppSettings["allowUntrustedSSL"]) == 1 ? true : false;

        private static int CMS_APPLICATION_ID = Int32.Parse(WebConfigurationManager.AppSettings["GardaMobileCMSApplicationID"]);

        #endregion

        #region call globalAPI

        private GlobalAPIResult callGlobalAPI(string endpoint, KeyValuePair<string, string>[] param)
        {
            using (var handler = new WebRequestHandler())
            {
                if (ALLOW_UNTRUSTED_SSL) handler.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient(handler))
                {
                    client.BaseAddress = new Uri(GLOBAL_SERVICE_ADDRESS);
                    var response = client.PostAsync(endpoint, new FormUrlEncodedContent(param)).Result;

                    response.Headers.Remove("Access-Control-Allow-Origin");
                    response.Headers.Remove("Access-Control-Allow-Headers");
                    response.Headers.Remove("Access-Control-Allow-Methods");

                    return JsonConvert.DeserializeObject<GlobalAPIResult>(response.Content.ReadAsStringAsync().Result);
                }
            }
        }

        #endregion

        #region Login

        [HttpPost]
        public IHttpActionResult Login(FormDataCollection form)
        {
            string userID = form.Get("userID");
            string password = form.Get("password");

            LoginResult<UserPrivilege> loginResult = new LoginResult<UserPrivilege>();
            GlobalAPIResult globalApiResult = new GlobalAPIResult();
            DateTime offdate = Convert.ToDateTime(Otosales.Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALESANDRIOD-OFFDATE")[0]);
            if (DateTime.Now > offdate)
            {
                loginResult.IsAuthenticated = false;
                loginResult.Status = globalApiResult.ErrorCode == null ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode]; ;
                loginResult.UserInfo = null;
                loginResult.ValidHour = 0;
            }
            else
            {
                globalApiResult = callGlobalAPI("account/login", new[]{
                            new KeyValuePair<string,string> ("username", userID),
                            new KeyValuePair<string,string> ("password", password),
                            new KeyValuePair<string,string> ("applicationCode", GLOBAL_APPLICATION_CODE)
                        });

                if (globalApiResult.status)
                {
                    SalesOfficer user = new SalesOfficer()
                    {
                        SalesOfficerID = globalApiResult.userInfo.ID,
                        Email = globalApiResult.userInfo.Email,
                        Name = globalApiResult.userInfo.FullName,
                        Role = globalApiResult.menu[0].RoleCode,
                        Class = globalApiResult.parameters.Class,
                        Validation = globalApiResult.parameters.Validation,
                        Expiry = globalApiResult.userInfo.PasswordExpiry,
                        Password_Expiry = globalApiResult.userInfo.PasswordExpiry,
                        Department = globalApiResult.parameters.Department,
                        BranchCode = globalApiResult.parameters.BranchCode,
                        Phone1 = globalApiResult.parameters.Phone1,
                        Phone2 = globalApiResult.parameters.Phone2,
                        Phone3 = globalApiResult.parameters.Phone3,
                        OfficePhone = globalApiResult.parameters.OfficePhone,
                        Ext = globalApiResult.parameters.Ext,
                        Fax = globalApiResult.parameters.Fax,
                        // 0261/URF/2015 - BSY
                        ExtUserID = globalApiResult.parameters.ExtUserID,
                        Channel = globalApiResult.parameters.Channel,
                        ChannelSource = globalApiResult.parameters.ChannelSource,
                        // end
                        RowStatus = true,
                        EntryDate = globalApiResult.EntryDate,
                        LastUpdatedTime = globalApiResult.LastUpdatedTime
                    };

                    loginResult.IsAuthenticated = true;
                    loginResult.Status = "OK";

                    UserPrivilege userPrivilege = new UserPrivilege();
                    userPrivilege.Region = globalApiResult.parameters.Region;
                    userPrivilege.ReportTo = globalApiResult.parameters.ReportTo;

                    foreach (Menu m in globalApiResult.menu)
                    {
                        FormAccess formAccess = new FormAccess();
                        formAccess.FormID = m.Name;
                        formAccess.CanRead = m.CanRead != null ? (bool)m.CanRead : false;
                        formAccess.CanWrite = m.CanWrite != null ? (bool)m.CanWrite : false;
                        formAccess.CanAdd = m.CanAdd != null ? (bool)m.CanAdd : false;
                        formAccess.CanDelete = m.CanDelete != null ? (bool)m.CanDelete : false;
                        userPrivilege.FormAccessCollections.Add(formAccess);
                    }

                    userPrivilege.User = user;
                    loginResult.UserInfo = userPrivilege;
                    loginResult.Token = globalApiResult.token;
                    loginResult.ValidHour = globalApiResult.ValidHours;
                }
                else
                {
                    loginResult.IsAuthenticated = false;
                    loginResult.Status = globalApiResult.ErrorCode == null ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode]; ;
                    loginResult.UserInfo = null;
                    loginResult.ValidHour = 0;
                }
            }

            return Json(loginResult, Util.jsonSerializerSetting());
        }

        #endregion

        #region External User

        [HttpPost]
        public IHttpActionResult AccountActivation(FormDataCollection form)
        {
            string username = form.Get("username");
            string password = form.Get("password");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/activate", new[]{
                    new KeyValuePair<string,string> ("username", username),
                    new KeyValuePair<string,string> ("password", password),
                    new KeyValuePair<string,string> ("deliverychanneltype", "email"),
                    new KeyValuePair<string,string> ("applicationCode", GLOBAL_APPLICATION_CODE)
                });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ConfirmAccountActivation(FormDataCollection form)
        {
            string username = form.Get("username");
            string token = form.Get("token");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/confirmactivation", new[] {
                new KeyValuePair<string, string> ("username", username),
                new KeyValuePair<string, string> ("token", token)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ForgotPassword(FormDataCollection form)
        {
            string username = form.Get("username");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/forgotpassword", new[] {
                new KeyValuePair<string, string> ("username", username)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ConfirmForgotPassword(FormDataCollection form)
        {
            string username = form.Get("username");
            string token = form.Get("token");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/confirmtokenforgotpassword", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("token", token)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult UpdatePassword(FormDataCollection form)
        {
            string username = form.Get("username");
            string password = form.Get("password");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/updatepassword", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password)
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        public IHttpActionResult ResendTokenActivation(FormDataCollection form)
        {
            string username = form.Get("username");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/resendtoken", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("eventtype", "1")
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        public IHttpActionResult ResendTokenForgotPassword(FormDataCollection form)
        {
            string username = form.Get("username");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/resendtoken", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("eventtype", "2")
            });

            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult ChangePassword(FormDataCollection form)
        {
            string username = form.Get("username");
            string password = form.Get("password");
            string oldpassword = form.Get("oldpassword");

            GlobalAPIResult globalApiResult = callGlobalAPI("account/changepassword", new[] {
                new KeyValuePair<string, string>("username", username),
                new KeyValuePair<string, string>("password", password),
                new KeyValuePair<string, string>("oldpassword", oldpassword)
            });


            AuthResult result = new AuthResult()
            {
                Message = (globalApiResult.ErrorCode == null) ? globalApiResult.message : WebConfigurationManager.AppSettings[globalApiResult.ErrorCode],
                ErrorCode = globalApiResult.ErrorCode,
                ValidHour = globalApiResult.ValidHours,
                Status = globalApiResult.status,
                WaitingDelay = globalApiResult.WaitingDelay
            };

            return Json(result, Util.jsonSerializerSetting());
        }

        #endregion

        #region GCM

        [HttpPost]
        public IHttpActionResult RegisterDevice(FormDataCollection form)
        {
            string DeviceToken = form.Get("DeviceToken");
            int DeviceType = Int32.Parse(form.Get("DeviceType"));
            string UniqueID = form.Get("UniqueID");
            string AppVersion = form.Get("AppVersion");

            OtosalesAPIResult result = new OtosalesAPIResult();

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAMOBILECMS))
            {
                string sql = @";EXECUTE usp_AddDevice @0, @1, @2, @3, @4";
                //@DeviceToken AS VARCHAR(250), @DeviceType AS INT, @AppID AS INT, @ClientUniqueID AS VARCHAR(250), @AppVersion AS VARCHAR(10)
                List<Device> device = db.Fetch<Device>(sql, DeviceToken, DeviceType, CMS_APPLICATION_ID, UniqueID, AppVersion);

                if (device.Count > 0)
                {
                    result.Status = true;
                    result.Data = device[0].DeviceID;
                }
            }

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult RegisterUserDevice(FormDataCollection form)
        {
            Int64 DeviceID = Int64.Parse(form.Get("DeviceID"));
            string UserID = form.Get("UserID");

            OtosalesAPIResult result = new OtosalesAPIResult();
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAMOBILECMS))
            {
                string sql = @";EXECUTE usp_AddUserDevice @0, @1, @2";
                //@DeviceID AS BIGINT, @AppID AS INT, @UserID as varchar(100)
                List<dynamic> userDevice = db.Fetch<dynamic>(sql, DeviceID, CMS_APPLICATION_ID, UserID);

                result.ResponseCode = userDevice[0].Code;
                result.Message = userDevice[0].Message;

                if (userDevice[0].Code.Equals("200")) {
                    result.Status = true;
                }
                else
                {
                    result.Status = false;
                }
            }

            return Json(result, Util.jsonSerializerSetting());
        }

        [HttpPost]
        public IHttpActionResult LogoutTokenGCM(FormDataCollection form)
        {
            string DeviceID = form.Get("DeviceID");

            OtosalesAPIResult result = new OtosalesAPIResult();

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAMOBILECMS))
            {
                string query = "UPDATE UserDevice SET Status = 0 where DeviceID = @0";
                db.Execute(query, DeviceID);
            }

            result.Status = true;
            return Json(result, Util.jsonSerializerSetting());
        }

        #endregion
    }
}
