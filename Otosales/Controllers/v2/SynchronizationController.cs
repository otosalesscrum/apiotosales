﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web.Http;

namespace Otosales.Controllers.v2
{
    /**
     * Rev No   : 001
     * Rev Date : 2016-09-28
     * Dev      : BSY
     * Remark   : Mengubah validasi dalam query penawaran reserved, 
     *            agar hanya agent yang pertama kali bid yang mendapatkan prospect
     */ 
    public class SynchronizationController : OtosalesBaseController
    {
        #region Declare

        OtosalesAPIResult response;
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        private List<string> ExcludedColumn
        {
            get
            {
                return new List<string>()
                {
                    "FieldChanged", "PrimaryChanged", "RowState"
                };
            }
        }

        #endregion

        #region Sync Methods

        [HttpPost]
        public IHttpActionResult Sync(FormDataCollection form)
        {
            string tableName = form.Get("objectType");
            string data = form.Get("data");

            return Json(SyncData(tableName, data, Util.CONNECTION_STRING_AABMOBILE), Util.jsonSerializerSetting());
        }

        //FollowUp
        [HttpPost]
        public IHttpActionResult DirectSync(FormDataCollection form)
        {
            string data = form.Get("data");
            data = data.Replace("//''", "\\\"");

            ProspectData prospectData = JsonConvert.DeserializeObject<ProspectData>(data);
            ProspectDataResult responseData = new ProspectDataResult();

            if (prospectData.ProspectCustomer != null)
            {
                responseData.ProspectCustomer = SyncData("ProspectCustomer", JsonConvert.SerializeObject(prospectData.ProspectCustomer, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.FollowUp != null)
            {
                responseData.FollowUp = SyncData("FollowUp", JsonConvert.SerializeObject(prospectData.FollowUp, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.FollowUpHistory != null)
            {
                responseData.FollowUpHistory = SyncData("FollowUpHistory", JsonConvert.SerializeObject(prospectData.FollowUpHistory, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulation != null)
            { 
                responseData.OrderSimulation = SyncData("OrderSimulation", JsonConvert.SerializeObject(prospectData.OrderSimulation, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulationMV != null)
            { 
                responseData.OrderSimulationMV = SyncData("OrderSimulationMV", JsonConvert.SerializeObject(prospectData.OrderSimulationMV, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulationInterest != null)
            {
                responseData.OrderSimulationInterest = SyncData("OrderSimulationInterest", JsonConvert.SerializeObject(prospectData.OrderSimulationInterest, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.OrderSimulationCoverage != null)
            { 
                responseData.OrderSimulationCoverage = SyncData("OrderSimulationCoverage", JsonConvert.SerializeObject(prospectData.OrderSimulationCoverage, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            if (prospectData.EmailQuotation != null)
            { 
                responseData.EmailQuotation = SyncData("EmailQuotation", JsonConvert.SerializeObject(prospectData.EmailQuotation, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;

            }

            if (prospectData.EmailNotification != null)
            { 
                responseData.EmailNotification = SyncData("EmailNotification", JsonConvert.SerializeObject(prospectData.EmailNotification, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" }), Util.CONNECTION_STRING_AABMOBILE).Status;
            }

            return Json(responseData, Util.jsonSerializerSetting());
        }

        private SyncDataResponse SyncData(string tableName, string data, String connection)
        {
            SyncDataResponse response = new SyncDataResponse();
            response.Status = new List<bool>();

            Util.SaveSyncDebug(tableName, data);

            if (connection.Equals(Util.CONNECTION_STRING_AABAGENT))
            {
                Util.CheckAllMGOBidExpiry();
            }

            data = data.Replace("//''", "\\\"");

            List<JObject> items = JsonConvert.DeserializeObject<List<JObject>>(data, BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

            Type type = tableName.GetCT();
            PropertyInfo[] propertyInfos = type.GetPI();
            string[] primaryKeys = type.GetPK();

            for (int i = 0; i < items.Count; i++)
            {
                string rowState = items[i].GetValue("RowState").ToString();
                switch (rowState)
                {
                    case "C": response.Status.Add(InsertData(tableName, items[i], propertyInfos, connection)); break;
                    case "U": response.Status.Add(UpdateData(tableName, items[i], propertyInfos, primaryKeys, connection)); break;
                    case "D": response.Status.Add(DeleteData(tableName, items[i], propertyInfos, primaryKeys, connection)); break;
                }
            }
            response.TimeStamp = DateTime.Now;

            return response;
        }   

        #endregion

        #region Create Update Delete

        private bool InsertData(string tableName, JObject jObject, PropertyInfo[] propertyInfos, string connection)
        {
            try {
                string sqlCommand = "INSERT INTO " + tableName + " ({1}) VALUES ({2})";
                string column = "";
                string value = "";
                JToken _value = null;

                foreach (PropertyInfo info in propertyInfos)
                {
                    if (!(ExcludedColumn.Where(o => o.Contains(info.Name)).Count() > 0))
                    {
                        _value = jObject.GetValue(info.Name);
                        column += "," + info.Name;

                        if (info.Name == "LastUpdatedTime")
                        {
                            value += ",getdate()";
                        }
                        else
                        {
                            if (_value == null)
                                value += ",null";
                            else
                            {
                                if (_value.Type.ToString() == "Date")
                                {
                                    value += ",'" + DateTime.Parse(_value.ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";
                                }
                                else if (_value.Type.ToString().ToLower() == "float" || _value.Type.ToString().ToLower() == "double")
                                {
                                    Double dv = Double.Parse(_value.ToString(), System.Globalization.NumberStyles.Float);
                                    value += ",'" + dv.ToString().Replace(",", ".") + "'";
                                }
                                else
                                {
                                    value += ",'" + _value.ToString().Replace("'", "''").Replace("@", "@@") + "'";
                                }
                            }
                        }
                    }
                }

                column = column.Substring(1);
                value = value.Substring(1);

                sqlCommand = sqlCommand.Replace("{1}", column).Replace("{2}", value);

                using (var db = new a2isDBHelper.Database(connection))
                {
                    db.Execute(sqlCommand);
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "SynchronizationController", "InsertData", ex.Message.ToString());
                return true;
            }

            return true;
        }

        private bool UpdateData(string tableName, JObject jObject, PropertyInfo[] propertyInfos, string[] primaryKeys, string connection)
        {
            try
            {
                List<string> fieldChangedList = new List<string>();
                List<string[]> primaryChangedList = new List<string[]>();
                string setValue = "";
                string setFilter = "";
                JToken _value = null;

                using (var db = new a2isDBHelper.Database(connection))
                {
                    foreach (string keyItem in primaryKeys)
                    {
                        //kalau PK berubah, ambil PK dari PrimaryChanged
                        if (primaryChangedList.Where(o => o[0].Contains(keyItem)).Count() > 0)
                            setFilter += " AND " + keyItem + "='" + primaryChangedList.Where(o => o[0] == keyItem).FirstOrDefault()[1].ToString() + "'";
                        else
                        {
                            _value = jObject.GetValue(keyItem);
                            setFilter += " AND " + keyItem + "=" + (_value == null ? "null" : "'" + _value.ToString() + "'");
                        }
                    }
                    setFilter = setFilter.Substring(5);

                    //validate if data not exist, then call InsertData
                    int serverCount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM " + tableName + " WHERE " + setFilter);
                    if (serverCount > 0)
                    {
                        string sqlCommand = "UPDATE " + tableName + " SET {1} WHERE {2}";

                        //  List<string[]> -> PK yang berubah, index[0] -> Nama field,   index[1] -> Value
                        if (jObject.GetValue("PrimaryChanged") != null && jObject.GetValue("PrimaryChanged").ToString() != "")
                            primaryChangedList = JsonConvert.DeserializeObject<List<string[]>>(jObject.GetValue("PrimaryChanged").ToString(), BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

                        //  List<string>  -> Nama Field yang berubah 
                        if (jObject.GetValue("FieldChanged") != null && jObject.GetValue("FieldChanged").ToString() != "" && jObject.GetValue("FieldChanged").ToString() != "[]")
                        {
                            fieldChangedList = JsonConvert.DeserializeObject<List<string>>(jObject.GetValue("FieldChanged").ToString(), BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

                            _value = null;
                            foreach (string fieldChangedItem in fieldChangedList)
                            {
                                if (!(ExcludedColumn.Where(o => o.Contains(fieldChangedItem)).Count() > 0))
                                {
                                    if (fieldChangedItem == "LastUpdatedTime")
                                    {
                                        setValue += "," + fieldChangedItem + "=getdate()";
                                    }
                                    else
                                    {
                                        _value = jObject.GetValue(fieldChangedItem);
                                        string value = null;

                                        if (_value == null)
                                        {
                                            value = "null";
                                        }
                                        else
                                        {
                                            if (_value.Type.ToString() == "Date")
                                            {
                                                value = "'" + DateTime.Parse(_value.ToString()).ToString("yyyy-MM-dd HH:mm:ss.fff") + "'";
                                            }
                                            else if (_value.Type.ToString().ToLower() == "float" || _value.Type.ToString().ToLower() == "double")
                                            {
                                                //value += ",'" + _value.ToString().Replace(",", ".") + "'";
                                                Double dv = Double.Parse(_value.ToString(), System.Globalization.NumberStyles.Float);
                                                value = "'" + dv.ToString().Replace(",", ".") + "'";
                                            }
                                            else
                                            {
                                                value = "'" + _value.ToString().Replace("'", "''").Replace("@", "@@") + "'";
                                            }
                                        }
                                        setValue += "," + fieldChangedItem + "= " + value;
                                    }
                                }
                            }

                            setValue = setValue.Substring(1);
                            sqlCommand = sqlCommand.Replace("{1}", setValue).Replace("{2}", setFilter);
                            db.Execute(sqlCommand);
                        }
                    }
                    else
                    {
                        return InsertData(tableName, jObject, propertyInfos, connection);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "SynchronizationController", "UpdateData", ex.Message.ToString());
                return false;
            }

            return true;
        }

        private bool DeleteData(string tableName, JObject jObject, PropertyInfo[] propertyInfos, string[] primaryKeys, string connection)
        {
            try
            {
                string sqlCommand = "UPDATE " + tableName + " SET RowStatus=0, LastUpdatedTime=GETDATE() WHERE {1}";
                string setFilter = "";

                List<string[]> primaryChangedList = new List<string[]>();

                //  List<string[]> -> PK yang berubah, index[0] -> Nama field,   index[1] -> Value
                if (jObject.GetValue("PrimaryChanged").ToString() != "")
                    primaryChangedList = JsonConvert.DeserializeObject<List<string[]>>(jObject.GetValue("PrimaryChanged").ToString(), BoolConverter.create(), new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff" });

                object _value = null;
                foreach (string keyItem in primaryKeys)
                {
                    //kalau PK berubah, ambil PK dari PrimaryChanged
                    if (primaryChangedList.Where(o => o[0].Contains(keyItem)).Count() > 0)
                        setFilter += " AND " + keyItem + "='" + primaryChangedList.Where(o => o[0] == keyItem).FirstOrDefault()[1].ToString() + "'";
                    else
                    {
                        _value = jObject.GetValue(keyItem);
                        setFilter += " AND " + keyItem + "=" + (_value == null ? "null" : "'" + _value + "'");
                    }
                }

                setFilter = setFilter.Substring(5);

                sqlCommand = sqlCommand.Replace("{1}", setFilter);

                using (var db = new a2isDBHelper.Database(connection))
                {
                    db.Execute(sqlCommand);
                }

            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "SynchronizationController", "DeleteData", ex.Message.ToString());
                return false;
            }

            return true;
        }

        #endregion

        #region MGO Bid

        //CheckExpiredForFollowUp
        [HttpPost]
        public IHttpActionResult CheckMGOBidExpiry(FormDataCollection form)
        {
            string ids = form.Get("listHistoryPenawaranID");

            List<int> data = new List<int>();

            // check all mgo bid data first
            Util.CheckAllMGOBidExpiry();

            List<int> idList = JsonConvert.DeserializeObject<List<int>>(ids);
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
            {
                for (int i = 0; i < idList.Count; i++)
                {
                    string query = "SELECT OrderNo, ExpiredDate FROM HistoryPenawaran WHERE HistoryPenawaranID=@0 AND IsAbleExpired = 1";
                    HistoryPenawaran historyPenawaran = db.FirstOrDefault<HistoryPenawaran>(query, idList[i]);

                    if (historyPenawaran != null)
                    {
                        if (DateTime.Compare(Convert.ToDateTime(historyPenawaran.ExpiredDate), DateTime.Now) < 0)
                        {
                            //return true if its expired - EKI
                            data.Add(1);
                            Util.SoftDeleteProspect(historyPenawaran.OrderNo);
                        }
                        else
                        {
                            data.Add(0);
                        }
                    }
                    else
                    {
                        data.Add(0);
                    }
                }
            }

            return Json(data, Util.jsonSerializerSetting());
        }

        //CheckUpdateBidStatus
        [HttpPost]
        public IHttpActionResult GetItMGOBid(FormDataCollection form)
        {
            string PolicyID = form.Get("PolicyID");
            string OrderNo = form.Get("OrderNo");
            string SalesOfficerID = form.Get("SalesOfficerID");

            response = new OtosalesAPIResult();

            int isSendBid;
            int limitPenawaranAgent;
            int totalBidAgent;
            DateTime reserveDate;

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
            {
                reserveDate = db.ExecuteScalar<DateTime>("INSERT INTO PenawaranReserved(policyID, SalesOfficerID, CreatedDate) OUTPUT INSERTED.CreatedDate VALUES(@0, @1, getdate())", PolicyID, SalesOfficerID);

                //using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                //{
                isSendBid = db.ExecuteScalar<int>("SELECT IsSendBid FROM AABMobile.dbo.SalesOfficer WHERE SalesOfficerID = @0", SalesOfficerID);
                limitPenawaranAgent = db.ExecuteScalar<int>("SELECT Value FROM AABMobile.dbo.SysParam WHERE Param = 'LimitPenawaranAgent' AND Type = 'MGOBid'");

                string query = "SELECT COUNT(*) as TotalBid";
                query += " FROM HistoryPenawaran hp";
                query += " LEFT JOIN AABMobile.dbo.OrderSimulation os ON os.OrderNo = hp.OrderNo ";
                query += " AND hp.SalesOfficerID = os.SalesOFficerID";
                query += " LEFT JOIN AABMobile.dbo.FollowUp fu ON fu.FollowUpNo = os.FollowUpNo";
                query += " WHERE hp.SalesOfficerID = @0 AND os.RowStatus=1 AND fu.RowStatus=1 ";
                query += " AND (fu.FollowUpStatus IN (1,2,3,4,9) OR (os.OrderNo is null and hp.IsAbleExpired = 1))";
                query += " AND (hp.IsAbleExpired = 0 OR (hp.IsAbleExpired = 1 AND getdate() < hp.ExpiredDate))";
                totalBidAgent = db.ExecuteScalar<int>(query, SalesOfficerID);
                //}

                if (isSendBid > 0)
                {
                    if (limitPenawaranAgent > totalBidAgent)
                    {
                        // 001 begin
                        int getReserveCount = db.ExecuteScalar<int>(@"
                                                select count(*) from PenawaranReserved
                                                where policyid = @0
                                                and PenawaranReservedID = (select min(PenawaranReservedID) from PenawaranReserved where policyid = @0)
                                                and SalesOfficerID = @1
                                            ", PolicyID, SalesOfficerID);
                        // 001 end
                        if (getReserveCount == 1)
                        {
                            int isAvailable = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Penawaran WHERE PolicyID = @0 AND BidStatus = 0", PolicyID);
                            if (isAvailable == 1)
                            {
                                // update bid status to 1
                                db.Execute("UPDATE Penawaran SET BidStatus = 1 WHERE policyID = @0", PolicyID);

                                // generate history penawaran
                                db.Execute(";EXECUTE sp_GenerateHistoryPenawaran @0, @1, @2", PolicyID, OrderNo, SalesOfficerID);

                                query = "SELECT policyID, policyCode, policyGeneratedDate, agentCode, agentEmail, agentFullName, customerName, customerEmail, customerPhone, ";
                                query += "policeNumber, productCode, protectionType, vehicleCode, vehicleType, vehicleDescription, vehicleYear, usageCode, usageText, ";
                                query += "areaCode, totalSumInsured, baseRate, isTJH, tjhRate, tjhValue, isPaDriver, paDriverRate, paDriverValue, isPaPass, ";
                                query += "paPassRate, paPassQuantity, paPassValue, isAutoApply, srccRate, etvRate, fldRate, isCheckSrcc, autoApplyRate, isTRS, ";
                                query += "trsRate, isAcc, accRate, accValue, loadingRate, administrationPrice, basicPremi, accPremi, totalPremiKendaraanPlusAksesoris, ";
                                query += "premiLoading, totalPremiDasarPlusLoading, premiAutoApply, premiTRS, premiTJH, premiPADriver, premiPAPass, totalPremiPerluasan, ";
                                query += "totalPremiDasarPlusPerluasan, totalAllPremi, PDFFile, infoModified, BidStatus, LastUpdatedTime, Domisili, followUpDateTime, isHotProspect, fuByCSOBranch ";
                                query += "FROM Penawaran WHERE policyID = @0 ORDER BY isHotProspect DESC, infoModified DESC, policyID ASC";
                                Penawaran penawaran = db.First<Penawaran>(query, PolicyID);

                                HistoryPenawaran historyPenawaran = db.First<HistoryPenawaran>("SELECT TOP 1 PolicyID, OrderNo, ExpiredDate, LastUpdatedTime, SalesOfficerID, IsAbleExpired, HistoryPenawaranID FROM HistoryPenawaran WHERE PolicyID = @0 ORDER BY LastUpdatedTime DESC", PolicyID);

                                response.Data = new { Penawaran = penawaran, HistoryPenawaran = historyPenawaran };
                                response.Message = "'Get It' success";
                                response.Status = true;
                            }
                            else
                            {
                                response.Status = false;
                                response.Message = "This prospect has been taken by another mitra.";
                            }
                        }
                        else
                        {
                            response.Status = false;
                            response.Message = "This prospect has been taken by another mitra.";
                        }
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "Request cannot be processed because you are already taking the maximum number of prospects.";
                    }
                }
                else
                {
                    response.Status = false;
                    response.Message = "You are not authorized to perform this action.";
                }

                db.Execute("DELETE PenawaranReserved WHERE PolicyID = @0", PolicyID); // 001
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        // UpdateIsAbleExpiredFollowUp
        [HttpPost]
        public IHttpActionResult UpdateIsAbleExpiredFollowUp(FormDataCollection form)
        {
            response = new OtosalesAPIResult();
            int historyPenawaranId = int.Parse(form.Get("HistoryPenawaranID"));

            // check all mgo bid if expired bid still exist
            Util.CheckAllMGOBidExpiry();

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
            {
                // check data not expired
                int bidExist = db.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", historyPenawaranId);

                if(bidExist == 1)
                {
                    // permanently set data to agent
                    db.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", historyPenawaranId);
                    response.Status = true;
                }
                else
                {
                    response.Status = false;
                }
            }

            return Json(response, Util.jsonSerializerSetting());
        }

        #endregion

        #region OrderApproval

        //ApprovalReviseRejectOrder
        [HttpPost]
        public IHttpActionResult ProcessOrderApproval(FormDataCollection form)
        {
            response = new OtosalesAPIResult();
            int OrderId = int.Parse(form.Get("OrderId"));
            string OrderNo = form.Get("OrderNo");
            string userID = form.Get("userID");
            string submitOrder = form.Get("submitOrder");
            string remarksOrder = form.Get("remarksOrder");


            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                // set userid that approve/revise/reject order in Order_Mobile_Notification's table
                int notificationId = db.ExecuteScalar<int>("SELECT TOP 1 Notification_ID FROM Order_Mobile_Notification WHERE Order_ID = @0 ORDER BY EntryDt DESC", OrderId);
                db.Execute("UPDATE Order_Mobile_Notification SET User_Id = @0, UpdateDt = GETDATE(), UpdateUsr = @1 WHERE Notification_ID = @2", userID, userID, notificationId);

                Mst_Order_Mobile order = db.First<Mst_Order_Mobile>("SELECT TOP 1 Approval_Status, isSO FROM Mst_Order_Mobile WHERE Order_No like '%" + OrderNo + "%' AND Order_ID = @0 ORDER BY EntryDt DESC", OrderId);

                // get order status if any
                List<string> orderStatus = db.Fetch<string>("SELECT Order_Status FROM Mst_Order WHERE Order_No like '%" + OrderNo + "%' AND Order_Status in ('6','9','10','11')");

                if (order.Approval_Status.Equals(0)) // if order not approved yet
                {
                    if (orderStatus.Count < 1) // if order status not in (6,9,10,11)
                    {
                        switch (submitOrder)
                        {
                            case "Approve":
                                // approve order
                                db.Execute("UPDATE Mst_Order_Mobile SET Approval_Status = 1, UpdateDt = GETDATE(), UpdateUsr = @0 WHERE Order_ID = @1", userID, OrderId);

                                response.Status = true;
                                response.Message = "Order approved";
                                break;

                            case "Revise":
                                // revise order
                                db.Execute("UPDATE Mst_Order_Mobile SET Approval_Status = 2, Remarks = @0, UpdateDt = GETDATE(), UpdateUsr = @1 WHERE Order_ID = @2", remarksOrder, userID, OrderId);

                                // activate email notification
                                int notificationID = db.ExecuteScalar<int>("SELECT Top 1 Notification_ID from Order_Mobile_Notification where Order_ID = @0 order by (CASE WHEN UpdateDt is null THEN EntryDt ELSE UpdateDt END)", OrderId);
                                db.Execute("UPDATE Order_Mobile_Notification SET Email_Status = 0 WHERE Notification_ID = @0", notificationId);

                                response.Status = true;
                                response.Message = "Order sent back to SA";
                                break;

                            case "Reject":
                                // reject order
                                db.Execute("UPDATE Mst_Order_Mobile SET Approval_Status = 2, UpdateDt = GETDATE(), UpdateUsr = @0 WHERE Order_ID = @1", userID, OrderId);

                                response.Status = true;
                                response.Message = "Order rejected";
                                break;

                            default:
                                response.Status = false;
                                response.Message = "Failed to approve/revise/reject";
                                break;
                        }
                    }
                    else
                    {
                        string username = db.ExecuteScalar<string>("SELECT TOP 1 us.UserName as Name FROM AAB.dbo.Approval ms INNER JOIN a2isAuthorizationDB.General.Users us ON us.UserID = ms.UpdateUsr WHERE ms.Reference_No like '%" + OrderNo + "%' ORDER BY (CASE WHEN UpdateDt is null THEN EntryDt ELSE UpdateDt END) DESC");

                        switch (int.Parse(orderStatus[0]))
                        {
                            case 9:
                                response.Status = false;
                                response.Message = "This policy has been approved by " + username;
                                break;

                            case 10:
                                response.Status = false;
                                response.Message = "This policy has been approved by " + username;
                                break;

                            case 11:
                                response.Status = false;
                                response.Message = "This policy has been approved by " + username;
                                break;

                            case 6:
                                response.Status = false;
                                response.Message = "This policy has been rejected by " + username;
                                break;
                        }
                    }
                }
                else
                {
                    string username = db.ExecuteScalar<string>("SELECT TOP 1 us.UserName as Name, us.UserID FROM AAB.dbo.Mst_Order_Mobile ms INNER JOIN a2isAuthorizationDB.General.Users us ON us.UserID = ms.UpdateUsr WHERE ms.Order_No like '%" + OrderNo + "%' ORDER BY (CASE WHEN UpdateDt is null THEN EntryDt ELSE UpdateDt END) DESC");

                    switch (order.Approval_Status)
                    {
                        case 1:
                            response.Status = false;
                            response.Message = "This policy has been approved by " + username;
                            break;
                        case 2:
                            if (order.isSO.Equals(1))
                            {
                                response.Status = false;
                                response.Message = "This policy has been revised by " + username;
                            }
                            else if (order.isSO.Equals(0))
                            {
                                response.Status = false;
                                response.Message = "This policy has been rejected by " + username;
                            }
                            else
                            {
                                response.Status = false;
                                response.Message = "Check database, wrong SO status for this order";
                            }
                            break;
                        default:
                            response.Status = false;
                            response.Message = "Check database, wrong approval status for this order";
                            break;
                    }
                }
            }
                
            return Json(response, Util.jsonSerializerSetting());
        }

        #endregion
    }
}
