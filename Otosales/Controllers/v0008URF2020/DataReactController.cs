﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Repository.v0008URF2020;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Otosales.Controllers.v0008URF2020
{
    public class DataReactController : OtosalesBaseController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();

        [HttpPost]
        public IHttpActionResult sendToSARenewalNonRenNot(Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string PolicyNo = param.PolicyNo;
            string FollowUpNo = param.FollowUpNo;
            string RemarkToSA = param.RemarksToSA;
            string SalesOfficerID = param.SalesOfficerID;
            int isPTSB = Convert.ToInt32(param.isPTSB);
            #endregion

            //Generate GUID 
            string custID = System.Guid.NewGuid().ToString();
            string followUpID = String.IsNullOrEmpty(FollowUpNo) ? System.Guid.NewGuid().ToString() : FollowUpNo;
            string orderID = System.Guid.NewGuid().ToString();

            if (string.IsNullOrEmpty(PolicyNo) || string.IsNullOrEmpty(FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            try
            {
                #region Check Double Insured
                Otosales.Models.vWeb2.CheckDoubleInsuredResult dblins = MobileRepository.GetCheckDoubleInsuredDataKotor(PolicyNo);
                if (dblins.IsDoubleInsured)
                {
                    return Json(new { status = false, message = "Double Insured.", data = dblins });
                }
                #endregion
                MobileRepository.GenerateOrderSimulation(SalesOfficerID, custID, followUpID, orderID, PolicyNo, RemarkToSA, isPTSB);

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", orderID });
            }
            catch (Exception e)
            {
                Otosales.Repository.v0213URF2019.MobileRepository.ClearFailedData(custID, followUpID, orderID);
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                int currentPage = Convert.ToInt16(form.Get("currentPage"));
                int pageSize = Convert.ToInt16(form.Get("pageSize"));
                int currentPageLead = currentPage; // Convert.ToInt16(form.Get("currentPageLead"));
                int pageSizeLead = pageSize;// Convert.ToInt16(form.Get("pageSizeLead"));
                bool chIsSearchByChasNo = bool.Parse(form.Get("chIsSearchByChasNo").Trim());
                bool chIsSearchByEngNo = bool.Parse(form.Get("chIsSearchByEngNo").Trim());
                bool chIsSearchByPolicyNo = bool.Parse(form.Get("chIsSearchByPolicyNo").Trim());
                bool chIsSearchByProsName = bool.Parse(form.Get("chIsSearchByProsName").Trim());
                string salesofficerid = form.Get("salesofficerid").Trim();
                string search = form.Get("search").Trim();
                string indexOrderByItem = form.Get("indexOrderByItem").Trim();
                int indexOrderByTypeItem = Int32.Parse(form.Get("indexOrderByTypeItem").Trim());
                bool chFollowUpMonthlyChecked = bool.Parse(form.Get("chFollowUpMonthlyChecked").Trim());
                bool chFollowUpDateChecked = bool.Parse(form.Get("chFollowUpDateChecked").Trim());
                bool chNeedFUChecked = bool.Parse(form.Get("chNeedFUChecked").Trim());
                bool chPotentialChecked = bool.Parse(form.Get("chPotentialChecked").Trim());
                bool chCallLaterChecked = bool.Parse(form.Get("chCallLaterChecked").Trim());
                bool chCollectDocChecked = bool.Parse(form.Get("chCollectDocChecked").Trim());
                bool chNotDealChecked = bool.Parse(form.Get("chNotDealChecked").Trim());
                bool chSentToSAChecked = bool.Parse(form.Get("chSentToSAChecked").Trim());
                bool chBackToAOChecked = bool.Parse(form.Get("chBackToAOChecked").Trim());
                bool chPolicyCreatedChecked = bool.Parse(form.Get("chPolicyCreatedChecked").Trim());
                bool chOrderRejectedChecked = bool.Parse(form.Get("chOrderRejectedChecked").Trim());
                bool chNewChecked = bool.Parse(form.Get("chNewChecked").Trim());
                bool chRenewableChecked = bool.Parse(form.Get("chRenewableChecked").Trim());
                string dateFollowUp = form.Get("getFollowUpDate");
                int allowSearchOthers = Int32.Parse(form.Get("allowSearchOthers").Trim());

                DateTime getFollowUpDate = DateTime.Now;
                if (!string.IsNullOrEmpty(dateFollowUp))
                {
                    getFollowUpDate = Convert.ToDateTime(dateFollowUp);
                }

                if (salesofficerid == null || salesofficerid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @";SELECT * FROM (
SELECT DISTINCT * 
        FROM  (SELECT 0 
                      AS 
                            IsRenewalNonRenNot, 
                      CASE 
                        WHEN IsRenewalDigital = 1 THEN 'Renewable Digital - ' + mft.FollowUpTypeDes 
                        ELSE 
						CASE WHEN IsRenewal=1 THEN 'Renewal - ' + mft.FollowUpTypeDes
						ELSE 'New - ' + mft.FollowUpTypeDes END 
                      END 
                      AS 
                            TaskStatus, 
                      b.description 
                      AS 
                            FollowUpDesc, 
                      c.description 
                      AS 
FollowUpReason, 
CASE 
    WHEN os.OldPolicyNo IS NOT NULL THEN (SELECT TOP 1 Period_To FROM BEYONDREPORT.AAB.dbo.Policy Where Policy_No = os.OldPolicyNo)
    ELSE ''
END ExpiredDate, 
a.CustID, 
a.FollowUpNo, 
a.LastFollowUpDate, 
a.NextFollowUpDate, 
a.LastSeqNo, 
CASE 
    WHEN a.followupstatus = 13 
        OR a.isrenewal = 1 THEN os.PolicyNo
    ELSE '' 
END PolicyNo, 
CASE pcm.isCompany
	WHEN 1 THEN pcs.CompanyName
	ELSE a.ProspectName 
END ProspectName, 
a.EntryDate,
a.IsRenewal,
p.isHotProspect,
a.followupstatus,
mws.WAStatusDes,
a.ReferenceNo,
wm.WAStatusID
FROM   followup a WITH (NOLOCK) 
INNER JOIN followupstatus b WITH (NOLOCK) 
  ON a.followupstatus = b.statuscode AND a.RowStatus = 1
LEFT JOIN followupstatusinfo c WITH (NOLOCK) 
 ON a.followupinfo = c.infocode 
LEFT JOIN dbo.OrderSimulation os WITH (NOLOCK)
 ON os.FollowUpNo = a.FollowUpNo AND os.ApplyF = 1 AND os.RowStatus = 1
 LEFT JOIN dbo.OrderSimulationMV omv WITH (NOLOCK)
 ON omv.OrderNo = os.OrderNo
 LEFT JOIN dbo.ProspectCustomer pcm 
 ON pcm.CustID = a.CustID
 LEFT JOIN dbo.ProspectCompany pcs
 ON pcs.CustID = a.CustID
 LEFT JOIN Quotation.dbo.HistoryPenawaran hp WITH (NOLOCK)
 ON hp.OrderNo = os.OrderNo 
 LEFT JOIN Quotation.dbo.Penawaran p WITH (NOLOCK)
 ON p.policyID = hp.PolicyID AND a.FollowUpStatus = 1 AND a.RowStatus = 1
 LEFT JOIN dbo.WAMessaging wm ON wm.OrderNo = os.OrderNo AND wm.ID = 
 (SELECT MAX(wm1.ID) FROM dbo.WAMessaging wm1 WHERE wm1.OrderNo = os.OrderNo AND wm1.WAStatusID IS NOT NULL) 
 LEFT JOIN dbo.MstWAStatus mws ON mws.WAStatusID = wm.WAStatusID
 LEFT JOIN dbo.MstFollowUpType mft ON mft.FollowUpTypeID = a.FollowUpTypeID
WHERE  #salesofficerid
#nextfollowupdate 
 #followup  
 #chasisno #engineno
 #name
 #policyno) a
#datakotor
#quotationlead
) res
#isrenewal
ORDER  BY #orderby";
                string qsalesofficerid = "";
                string qdatakotor = "";
                qsalesofficerid = string.IsNullOrEmpty(search) ? " a.salesofficerid = @0 AND " : "";
                qdatakotor = !string.IsNullOrEmpty(search) ? @"UNION ALL 
SELECT *
FROM  (SELECT 1                   AS IsRenewalNonRenNot, 
'Renewable - MV'         AS TaskStatus, 
'Need FU'           AS FollowUpDesc, 
'Data baru diinput' AS FollowUpReason, 
p.Period_To   AS ExpiredDate, 
''                  CustID, 
''                  FollowUpNo, 
NULL                AS LastFollowUpDate, 
NULL                AS NextFollowUpDate, 
0                   AS LastSeqNo, 
ERP.policy_no       AS PolicyNo, 
p.Name_On_Policy    AS ProspectName, 
p.entrydt           AS EntryDate,
1                   AS IsRenewal,
NULL                isHotProspect,
1                   AS followupstatus,
NULL                AS WAStatusDes,
NULL                ReferenceNo,
NULL                WAStatusID
FROM [BeyondReport].[AAB].[dbo].[excluded_renewal_policy] ERP 
INNER JOIN [BeyondReport].[AAB].[dbo].policy p 
  ON p.policy_no = ERP.policy_no AND IsSendToSA=0
 AND p.Status = 'A' AND p.RENEWAL_STATUS = 0
 AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) #2name
     #2chasisno #2engineno #2policyno) b" : "";
                string qSalesOfficerIDQL = "";
                qSalesOfficerIDQL = string.IsNullOrEmpty(search) ? " ql.SalesOfficerID = @0 AND " : "";
                string qGetQuotationLead = @"UNION ALL 
SELECT * FROM  (
SELECT 0              AS IsRenewalNonRenNot, 
'New - MV GODIGITAL'  AS TaskStatus, 
'Need FU'             AS FollowUpDesc, 
'Data baru diinput'   AS FollowUpReason, 
''					  AS ExpiredDate, 
''                    CustID, 
''                    FollowUpNo, 
NULL                  AS LastFollowUpDate, 
NULL                  AS NextFollowUpDate, 
0                     AS LastSeqNo, 
ql.PolicyNo	          AS PolicyNo, 
ql.CustName           AS ProspectName, 
ql.CreatedDate        AS EntryDate,
0                     AS IsRenewal,
NULL                  AS isHotProspect,
1                     AS followupstatus,
NULL                  AS WAStatusDes,
ql.ReferenceNo        ReferenceNo,
NULL                  WAStatusID 
FROM dbo.QuotationLead ql 
LEFT JOIN dbo.FollowUp f ON f.ReferenceNo = ql.ReferenceNo
WHERE #qlsalesofficerid f.ReferenceNo IS NULL AND ql.RowStatus = 1 
AND ql.SalesOfficerID IS NOT NULL
#3name #3chasisno #3engineno #3policyno
) c";
                string qnextfollowupdate = "";
                if (chFollowUpDateChecked)
                {
                    qnextfollowupdate = @" ( nextfollowupdate <= @2 OR nextfollowupdate IS NULL ) ";
                }
                else if (chFollowUpMonthlyChecked)
                {
                    qnextfollowupdate = @" ((MONTH(a.EntryDate) = MONTH(GETDATE()) 
                                            AND YEAR(a.EntryDate) = YEAR(GETDATE()))) ";
                }

                List<int> listStatus = new List<int>();
                List<int> listInfo = new List<int>();

                if (chNeedFUChecked)
                {
                    listStatus.Add(1);
                }
                if (chPotentialChecked)
                {
                    listStatus.Add(2);
                }
                if (chCallLaterChecked)
                {
                    listStatus.Add(3);
                }
                if (chCollectDocChecked)
                {
                    listInfo.Add(56);
                }
                if (chNotDealChecked)
                {
                    listStatus.Add(5);
                }
                if (chSentToSAChecked)
                {
                    listInfo.Add(61);
                }
                if (chPolicyCreatedChecked)
                {
                    listInfo.Add(50);
                    listInfo.Add(51);
                    listInfo.Add(52);
                    listInfo.Add(53);
                    listInfo.Add(54);
                    listInfo.Add(55);
                }
                if (chBackToAOChecked)
                {
                    listInfo.Add(60);
                }
                if (chOrderRejectedChecked)
                {
                    listStatus.Add(14);
                }

                string qfollowup = "";
                qfollowup = @" AND ( followupstatus IN(@3) OR (followupinfo IN(@4) AND followupinfo IS NOT NULL AND followupinfo <> 0)) ";
                string qisrenewal = "";
                qisrenewal = @"WHERE {0} {1} ";
                string snew = "";
                string srenew = "";

                if (chNewChecked)
                {
                    snew = "isrenewal = 0 OR isrenewal IS NULL";
                }
                if (chRenewableChecked)
                {
                    if (chNewChecked)
                    {
                        srenew = "OR isrenewal = 1";
                    }
                    else
                    {
                        srenew = "isrenewal = 1";
                    }
                }
                if (!chNewChecked && !chRenewableChecked)
                {
                    snew = "isrenewal = 2";
                    srenew = "";
                }
                qisrenewal = string.Format(qisrenewal, snew, srenew);
                string qchasisno = "";
                string qengineno = "";
                string qchasisno2 = "";
                string qengineno2 = "";
                string qchasisno3 = "";
                string qengineno3 = "";
                string qpolicyno = "";
                string qpolicyno2 = "";
                string qpolicyno3 = "";
                string qname = "";
                string qname2 = "";
                string qname3 = "";
                if (!string.IsNullOrEmpty(search))
                {
                    if (chIsSearchByChasNo)
                    {
                        qchasisno = " AND omv.ChasisNumber = @1 ";
                        qchasisno2 = " AND ERP.Chasis_Number = @1 ";
                        qchasisno3 = " AND ql.ChasisNumber = @1 ";
                    }
                    else if (chIsSearchByEngNo)
                    {
                        qengineno = " AND omv.EngineNumber = @1 ";
                        qengineno2 = " AND ERP.Engine_Number = @1 ";
                        qengineno3 = " AND ql.EngineNumber = @1 ";
                    }
                    else if (chIsSearchByPolicyNo)
                    {
                        qpolicyno = "AND os.PolicyNo = @1 ";
                        qpolicyno2 = " AND ERP.Policy_No = @1 ";
                        qpolicyno3 = " AND ql.PolicyNo = @1 ";
                    }
                    else if (chIsSearchByProsName)
                    {
                        //Flag form UI to Search Others ProsName - 0213/URF/2019
                        if (allowSearchOthers == 0) //
                        {
                            qsalesofficerid = " a.salesofficerid = @0 AND ";
                        }

                        search = '%' + search + '%';
                        qname = " AND (pcm.Name LIKE @1 OR pcs.CompanyName LIKE @1)";
                        qname2 = " WHERE Name_On_Policy LIKE @1 ";
                        qname3 = " AND ql.CustName LIKE @1 ";
                    }
                }

                string qorderby = "";
                if (indexOrderByItem.Equals("Status"))
                {
                    qorderby = @" isHotProspect DESC, IsRenewalNonRenNot ASC , COALESCE(isrenewal,0) ASC, COALESCE(NextFollowUpDate,DATEADD(YEAR,5,GETDATE())) ASC   , (CASE WHEN isrenewal = 1 THEN followupstatus END) ASC, (CASE WHEN isrenewal = 1 AND followupstatus=1 THEN ExpiredDate END) ASC, (CASE WHEN (isrenewal = 0 OR isrenewal IS NULL AND nextfollowupdate is null) OR (isrenewal = 1 AND followupstatus!=1)  THEN CAST(EntryDate AS DATE) END) DESC, WAStatusID DESC";
                }
                if (indexOrderByItem.Equals("Name"))
                {
                    qorderby = "ProspectName";
                }
                if (indexOrderByItem.Equals("EntryDate"))
                {
                    qorderby = "EntryDate";
                }
                if (indexOrderByItem.Equals("LastFollowUpdate"))
                {
                    qorderby = "LastFollowUpdate";
                }
                if (indexOrderByItem.Equals("NextFollowUpdate"))
                {
                    qorderby = "NextFollowUpdate";
                }


                if (!indexOrderByItem.Equals("Status"))
                {
                    if (indexOrderByTypeItem == 0)
                    {
                        qorderby = string.Concat(qorderby, " ASC");
                    }
                    else
                    {
                        qorderby = string.Concat(qorderby, " DESC");
                    }
                }
                query = query.Replace("#quotationlead", qGetQuotationLead);
                query = query.Replace("#datakotor", qdatakotor);
                query = query.Replace("#salesofficerid", qsalesofficerid);
                query = query.Replace("#qlsalesofficerid", qSalesOfficerIDQL);
                query = query.Replace("#nextfollowupdate", qnextfollowupdate);
                query = query.Replace("#followup", qfollowup);
                query = query.Replace("#isrenewal", qisrenewal);
                query = query.Replace("#chasisno", qchasisno);
                query = query.Replace("#engineno", qengineno);
                query = query.Replace("#2chasisno", qchasisno2);
                query = query.Replace("#3chasisno", qchasisno3);
                query = query.Replace("#2engineno", qengineno2);
                query = query.Replace("#3engineno", qengineno3);
                query = query.Replace("#policyno", qpolicyno);
                query = query.Replace("#2policyno", qpolicyno2);
                query = query.Replace("#3policyno", qpolicyno3);
                query = query.Replace("#name", qname);
                query = query.Replace("#2name", qname2);
                query = query.Replace("#3name", qname3);
                query = query.Replace("#orderby", qorderby);

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    dynamic result = null;
                    result = db.Page<dynamic>(currentPage, pageSize, query, salesofficerid, search, getFollowUpDate
                        , listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 });

                    return Json(new { status = true, message = "OK", data = result, /*prospectLeadList = prospectLeadListPage, historyPenawaranList = historyPenawaranList, penawaranList = penawaranList*/ }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                #endregion
                Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
                try
                {
                    result = MobileRepository.GetTaksListDetail(CustID, FollowUpNo);
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        #region 0001urf2019
        [HttpPost]
        public IHttpActionResult GetPaymentInfo(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                int IsGenerateVA = !string.IsNullOrEmpty(form.Get("IsGenerateVA")) ? Convert.ToInt32(form.Get("IsGenerateVA")) : 0;
                if (string.IsNullOrEmpty(CustID) && string.IsNullOrEmpty(FollowUpNo))
                    return Json(new { status = false, message = "Param can't be null" });

                Otosales.Models.vWeb2.PaymentInfo res = MobileRepository.GetPaymentInfo(CustID, FollowUpNo, IsGenerateVA);
                return Json(new { status = true, message = "Update Success.", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
        [HttpPost]
        public IHttpActionResult CheckVAExpiry(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                    return Json(new { status = false, message = "Param can't be null" });

                bool res = MobileRepository.CheckVAExpiry(OrderNo);
                return Json(new { status = true, isVAExpired = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

        #endregion
    }
}
