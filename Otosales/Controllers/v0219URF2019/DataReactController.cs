﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Models.v0219URF2019;
using Otosales.Repository.v0219URF2019;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Otosales.Controllers.v0219URF2019
{
    [AutoLoggingReference]
    public class DataReactController : OtosalesBaseController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();
        #region Properties
        IMobileRepository mobileRepository;
        IAABRepository aabRepository;

        public DataReactController()
        {
            mobileRepository = new MobileRepository();
            aabRepository = new AABRepository();
        }
        public DataReactController(IMobileRepository repository)
        {
            mobileRepository = repository;
        }
        public DataReactController(IAABRepository repository)
        {
            aabRepository = repository;
        }
        #endregion

        #region Rate Calculation Global Variable
        private string ProductCode = "";
        private string CityCode = "";
        private string type = "";
        private string UsageCode = "";
        private int Year = 0;

        private bool IsSRCCChecked = false;
        private bool IsFLDChecked = false;
        private bool IsETVChecked = false;
        private bool IsTSChecked = false;
        private bool IsTPLChecked = false;
        private bool IsPAPASSChecked = false;
        private bool IsPADRVRChecked = false;
        private bool IsTPLSIEnabled = false;
        private bool IsPASSEnabled = false;
        private bool IsPAPASSSIEnabled = false;
        private bool IsPADRVRSIEnabled = false;
        private bool IsACCESSChecked = false;
        private bool IsACCESSSIEnabled = false;


        private bool IsSRCCEnabled = false;
        private bool IsETVEnabled = false;
        private bool IsFLDEnabled = false;
        private bool IsTSEnabled = false;
        private bool IsPAPASSEnabled = false;
        private bool IsPADRVREnabled = false;
        private bool IsACCESSEnabled = false;
        private bool IsTPLEnabled = false;

        private string TPLCoverageID = "";
        private double TPLSI = 0;
        private double PADRVRSI = 0;
        private double PAPASSSI = 0;
        private int PASS = 0;
        private double AccessSI = 0;

        private double BasicPremi = 0;
        private double SumInsured = 0;
        private double SRCCPremi = 0;
        private double FLDPremi = 0;
        private double ETVPremi = 0;
        private double TSPremi = 0;
        private double ACCESSPremi = 0;
        private double TPLPremi = 0;
        private double PADRVRPremi = 0;
        private double PAPASSPremi = 0;
        private double LoadingPremi = 0;
        private double TotalPremi = 0;
        private double GrossPremi = 0;
        private double AdminFee = 0;
        private double VehiclePrice = 0;


        List<Otosales.Models.CalculatedPremi> CalculatedPremiItems = new List<Otosales.Models.CalculatedPremi>();
        List<Otosales.Models.CalculatedPremi> InterestCoverageItems = new List<Otosales.Models.CalculatedPremi>();
        List<Otosales.Models.Prd_Load_Mv> loadingMaster = new List<Otosales.Models.Prd_Load_Mv>();
        List<Otosales.Models.Prd_Agreed_Value> prdAgreedValueMaster = new List<Otosales.Models.Prd_Agreed_Value>();
        List<Otosales.Models.CoveragePeriod> coveragePeriodItems = new List<Otosales.Models.CoveragePeriod>();
        List<Otosales.Models.Prd_Interest_Coverage> prdInterestCoverage = new List<Otosales.Models.Prd_Interest_Coverage>();


        Otosales.Models.VehiclePriceTolerance vptModel;
        Otosales.Models.ProspectCustomer pcModel;
        List<Otosales.Models.OrderSimulationInterest> osiDeletedModels;
        List<Otosales.Models.OrderSimulationCoverage> oscDeletedModels;
        List<Otosales.Models.CoveragePeriodNonBasic> coveragePeriodNonBasicItems = new List<Otosales.Models.CoveragePeriodNonBasic>();


        #endregion

        [HttpPost]
        public IHttpActionResult GetSurveyZipCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug("Begin");
            string CityID = form.Get("CityID");

            if (String.IsNullOrEmpty(CityID))
            {
                return Json(new { status = false, message = "error", data = "CityID Parameter Required", SessionTimeOutF = false });
            }
            else
            {
                try
                {
                    dynamic res = MobileRepository.GetSurveyZipCode(CityID);
                    return Json(new { status = true, message = "success", data = res, SessionTimeOutF = false });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "error", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult GetSlotTimeBooking(FormDataCollection form)
        {
            _log.Debug("Begin");
            if (form == null) return BadRequest("Bad Request");
            string zipcode = form.Get("ZipCode");
            string cityid = form.Get("CityId");
            string branchID = form.Get("BranchID");
            string date = form.Get("AvailableDate");

            DateTime d;
            if (!DateTime.TryParse(date, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out d)) return BadRequest("Incorrect Date format");

            if (string.IsNullOrEmpty(cityid) || string.IsNullOrEmpty(date)) return BadRequest("Bad Request");
            try
            {
                dynamic res = null;
                if (!string.IsNullOrEmpty(zipcode))
                {
                    // 0181/URF/2018
                    IEnumerable<dynamic> data = null;
                    data = MobileRepository.GetSurveyScheduleSurveyManagement(cityid, zipcode, d);
                    // 0181/URF/2018
                    _log.Debug("OK");
                    res = data;
                }
                else if (!string.IsNullOrEmpty(branchID))
                {
                    string SurveyLocation = "";
                    a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB);
                    SurveyLocation = db.SingleOrDefault<string>(@"SELECT type.NAME TNAME from Branch branch inner join BranchType type on type.ID = branch.TYPE  WHERE branch.ID=@0 AND branch.isDeleted = 0 and branch.TYPE in(2,5,6) order by branch.NAME asc", branchID);

                    SurveyLocation = SurveyLocation.Replace(" ", "").ToUpper();
                    List<string> TimeCodeSurveyDalam = db.Query<string>(@"
                select part from GODigital.Fn_SplitString((select optionValue from Asuransiastra.dbo.AsuransiAstraOptions
                where optionName = 'GODIG-'+'" + SurveyLocation + "'+'-SURVEY-TIME'),',') ").ToList();

                    List<dynamic> times = MobileRepository.GetScheduleSurveyDalam(TimeCodeSurveyDalam);
                    res = times;
                }
                return Json(new { status = true, message = "Fetch success", data = res });
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveProspect(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string State = form.Get("State");
                string PCData = form.Get("ProspectCustomer");
                string FUData = form.Get("FollowUp");
                string FUHData = form.Get("FollowUpHistory");
                string ENData = form.Get("EmailNotification ");
                string OSData = form.Get("OrderSimulation");
                string OSMVData = form.Get("OrderSimulationMV");
                // string OSIData = form.Get("OrderSimulationInterest");
                string calculatedPremiItems = form.Get("calculatedPremiItems");
                bool IsMvGodig = bool.Parse(form.Get("isMvGodig"));
                string GuidTempPenawaran = form.Get("GuidTempPenawaran");
                string PremiumModelData = form.Get("PremiumModel");

                string query = "";
                string CustIdEnd = "";
                string FollowUpNoEnd = "";
                string OrderNoEnd = "";


                FollowUp FUEnd = null;
                ProspectCustomer PCEnd = null;

                #region Prospect Tab
                if (State.Equals("PROSPECT") || State.Equals("PREMIUMSIMULATION"))
                {
                    string CustId = "";
                    string FollowUpNumber = "";
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT UPDATE PROSPECT CUSTOMER

                        ProspectCustomer PC = JsonConvert.DeserializeObject<ProspectCustomer>(PCData);
                        string CustID = System.Guid.NewGuid().ToString();
                        string FollowUpNo = System.Guid.NewGuid().ToString();
                        PC.CustID = string.IsNullOrEmpty(PC.CustID) ? CustID : PC.CustID;
                        query = @";IF EXISTS(SELECT * FROM ProspectCustomer WHERE CustId=@0)
BEGIN
UPDATE ProspectCustomer SET
       [Name]=@1
      ,[Phone1]=@2
      ,[Phone2]=@3
      ,[Email1]=@4
      ,[Email2]=@5
      ,[CustIDAAB]=@6
      ,[SalesOfficerID]=@7
      ,[DealerCode]=@8
      ,[SalesDealer]=@9
      ,[BranchCode]=@10
      ,[isCompany]=@11
      ,[LastUpdatedTime]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCustomer ([CustID]
      ,[Name]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[CustIDAAB]
      ,[SalesOfficerID]
      ,[DealerCode]
      ,[SalesDealer]
      ,[BranchCode]
      ,[isCompany]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1)
END";
                        // PC.SalesOfficerID = PC.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", PC.SalesOfficerID) : PC.SalesOfficerID;

                        db.Execute(query, PC.CustID, PC.Name, PC.Phone1, PC.Phone2, PC.Email1, PC.Email2, PC.CustIDAAB, PC.SalesOfficerID, PC.DealerCode, PC.SalesDealer, PC.BranchCode, PC.isCompany);
                        #endregion

                        #region INSERT UPDATE FOLLOWUP
                        FollowUp FU = JsonConvert.DeserializeObject<FollowUp>(FUData);

                        FU.FollowUpNo = string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo;
                        FU.CustID = string.IsNullOrEmpty(FU.CustID) ? CustID : FU.CustID;
                        FU.LastSeqNo = db.ExecuteScalar<int>(";IF EXISTS(SELECT * FROM FollowUpHistory Where FollowUpNo=@0)BEGIN SELECT ISNULL(MAX(SeqNo),0) From FollowUpHistory WHERE FollowUpNo=@0 END ELSE BEGIN SELECT 0 END", string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo);
                        query = @";IF EXISTS(SELECT * FROM FollowUp WHERE FollowUpNo=@0)
BEGIN
    UPDATE FollowUp SET [LastSeqNo]=@1
      ,[CustID]=@2
      ,[ProspectName]=@3
      ,[Phone1]=@4
      ,[Phone2]=@5
      ,[SalesOfficerID]=@6
      ,[EntryDate]=GETDATE()
      ,[FollowUpName]=@7
      ,[FollowUpStatus]=@8
      ,[FollowUpInfo]=@9
      ,[Remark]=@10
      ,[BranchCode]=@11
      ,[LastUpdatedTime]=GETDATE()
      ,[PolicyId]=@12
      ,[PolicyNo]=@13
      ,[TransactionNo]=@14
      ,[SalesAdminID]=@15
      ,[SendDocDate]=@16
      ,[IdentityCard]=@17
      ,[STNK]=@18
      ,[SPPAKB]=@19
      ,[BSTB1]=@20
      ,[BSTB2]=@21
      ,[BSTB3]=@22
      ,[BSTB4]=@23
      ,[CheckListSurvey1]=@24
      ,[CheckListSurvey2]=@25
      ,[CheckListSurvey3]=@26
      ,[CheckListSurvey4]=@27
      ,[PaymentReceipt1]=@28
      ,[PaymentReceipt2]=@29
      ,[PaymentReceipt3]=@30
      ,[PaymentReceipt4]=@31
      ,[PremiumCal1]=@32
      ,[PremiumCal2]=@33
      ,[PremiumCal3]=@34
      ,[PremiumCal4]=@35
      ,[FormA1]=@36
      ,[FormA2]=@37
      ,[FormA3]=@38
      ,[FormA4]=@39
      ,[FormB1]=@40
      ,[FormB2]=@41
      ,[FormB3]=@42
      ,[FormB4]=@43
      ,[FormC1]=@44
      ,[FormC2]=@45
      ,[FormC3]=@46
      ,[FormC4]=@47
      ,[FUBidStatus]=@48
      ,[BuktiBayar]=@49 WHERE [FollowUpNo]=@0
END
    ELSE
BEGIN
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[PolicyId]
      ,[PolicyNo]
      ,[TransactionNo]
      ,[SalesAdminID]
      ,[SendDocDate]
      ,[IdentityCard]
      ,[STNK]
      ,[SPPAKB]
      ,[BSTB1]
      ,[BSTB2]
      ,[BSTB3]
      ,[BSTB4]
      ,[CheckListSurvey1]
      ,[CheckListSurvey2]
      ,[CheckListSurvey3]
      ,[CheckListSurvey4]
      ,[PaymentReceipt1]
      ,[PaymentReceipt2]
      ,[PaymentReceipt3]
      ,[PaymentReceipt4]
      ,[PremiumCal1]
      ,[PremiumCal2]
      ,[PremiumCal3]
      ,[PremiumCal4]
      ,[FormA1]
      ,[FormA2]
      ,[FormA3]
      ,[FormA4]
      ,[FormB1]
      ,[FormB2]
      ,[FormB3]
      ,[FormB4]
      ,[FormC1]
      ,[FormC2]
      ,[FormC3]
      ,[FormC4]
      ,[FUBidStatus]
      ,[BuktiBayar]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38,@39,@40,@41,@42,@43,@44,@45,@46,@47,@48,@49)
END";
                        // FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;

                        db.Execute(query, FU.FollowUpNo
     , FU.LastSeqNo
     , FU.CustID
     , FU.ProspectName
     , FU.Phone1
     , FU.Phone2
     , FU.SalesOfficerID
     , FU.FollowUpName
     , FU.FollowUpStatus
     , FU.FollowUpInfo
     , FU.Remark
     , FU.BranchCode
     , FU.PolicyId
     , FU.PolicyNo
     , FU.TransactionNo
     , FU.SalesAdminID
     , FU.SendDocDate
     , FU.IdentityCard
     , FU.STNK
     , FU.SPPAKB
     , FU.BSTB1
     , FU.BSTB2
     , FU.BSTB3
     , FU.BSTB4
     , FU.CheckListSurvey1
     , FU.CheckListSurvey2
     , FU.CheckListSurvey3
     , FU.CheckListSurvey4
     , FU.PaymentReceipt1
     , FU.PaymentReceipt2
     , FU.PaymentReceipt3
     , FU.PaymentReceipt4
     , FU.PremiumCal1
     , FU.PremiumCal2
     , FU.PremiumCal3
     , FU.PremiumCal4
     , FU.FormA1
     , FU.FormA2
     , FU.FormA3
     , FU.FormA4
     , FU.FormB1
     , FU.FormB2
     , FU.FormB3
     , FU.FormB4
     , FU.FormC1
     , FU.FormC2
     , FU.FormC3
     , FU.FormC4
     , FU.FUBidStatus
     , FU.BuktiBayar);
                        #endregion


                        if (PC.isCompany)
                        {
                            #region INSERT UPDATE COMPANY DATA
                            db.Execute(@";IF EXISTS(SELECT * FROM ProspectCompany WHERE CustId=@0)
BEGIN
UPDATE ProspectCompany SET
       [CompanyName]=@1
      ,[FollowUpNo]=@2
      ,[PICPhoneNo]=@3
      ,[Email]=@4
      ,[RowStatus]=1
      ,[ModifiedDate]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName]
      ,[FollowUpNo]
      ,[PICPhoneNo]
      ,[Email]
      ,[ModifiedDate]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,GETDATE(),1)
END", PC.CustID, PC.Name, FU.FollowUpNo, PC.Phone1, PC.Email1);
                            #endregion
                        }

                        PCEnd = PC;
                        FUEnd = FU;

                        #region SAVE IMAGE
                        query = @";
SELECT [ImageID]
      ,[FollowUpNo]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                        List<dynamic> ImageData = db.Fetch<dynamic>(query, FU.FollowUpNo);
                        foreach (dynamic id in ImageData)
                        {
                            query = @"IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,1,@6,@7)
END";
                            db.Execute(query, id.ImageID, id.DeviceID
      , id.SalesOfficerID
      , id.PathFile
      , id.EntryDate
      , id.LastUpdatedTime
      , id.Data
      , id.ThumbnailData);
                            db.Execute("UPDATE dbo.TempImageData SET RowStatus=0 WHERE FollowUpNo=@0 AND PathFile=@1", id.FollowUpNo, id.PathFile);
                        }
                        #endregion


                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "PROSPECT", FU.CustID, FU.FollowUpNo, "", GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, FU.FollowUpNo, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion


                    }
                    //return Json(new { status = true, message = "Success", CustId, FollowUpNumber });
                    CustIdEnd = FUEnd.CustID;
                    FollowUpNoEnd = FUEnd.FollowUpNo;

                }
                #endregion


                #region Vehicle Tab
                if (State.Equals("VEHICLE") || State.Equals("PREMIUMSIMULATION"))
                {
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    string OrderNoReturn = null;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT ORDERSIMULATION
                        OrderSimulation OS = JsonConvert.DeserializeObject<OrderSimulation>(OSData);
                        string OrderNo = System.Guid.NewGuid().ToString();
                        OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                        OrderNoReturn = OS.OrderNo;
                        FollowUpNoEnd = OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;

                        CustIdEnd = OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;
                        OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                        OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                        OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PeriodFrom]
      ,[PeriodTo]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27)
END";
                        // OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                        db.Execute(query, OS.OrderNo
      , OS.CustID
      , OS.FollowUpNo
      , OS.QuotationNo
      , OS.MultiYearF
      , OS.YearCoverage
      , OS.TLOPeriod
      , OS.ComprePeriod
      , OS.BranchCode
      , OS.SalesOfficerID
      , OS.PhoneSales
      , OS.DealerCode
      , OS.SalesDealer
      , OS.ProductTypeCode
      , OS.AdminFee
      , OS.TotalPremium
      , OS.Phone1
      , OS.Phone2
      , OS.Email1
      , OS.Email2
      , OS.SendStatus
      , OS.InsuranceType
      , OS.ApplyF
      , OS.SendF
      , OS.LastInterestNo
      , OS.LastCoverageNo
      , OS.PeriodFrom
      , OS.PeriodTo);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION MV
                        OrderSimulationMV OSMV = JsonConvert.DeserializeObject<OrderSimulationMV>(OSMVData);
                        OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                        query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                        db.Execute(query, OSMV.OrderNo
      , OSMV.ObjectNo
      , OSMV.ProductTypeCode
      , OSMV.VehicleCode
      , OSMV.BrandCode
      , OSMV.ModelCode
      , OSMV.Series
      , OSMV.Type
      , OSMV.Sitting
      , OSMV.Year
      , OSMV.CityCode
      , OSMV.UsageCode
      , OSMV.SumInsured
      , OSMV.AccessSI
      , OSMV.RegistrationNumber
      , OSMV.EngineNumber
      , OSMV.ChasisNumber
      //, OSMV.LastUpdatedTime
      //, OSMV.RowStatus
      , OSMV.IsNew);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION SURVEY
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulationSurvey WHERE OrderNo=@0)
BEGIN
INSERT INTO ordersimulationsurvey (OrderNo,CityCode,LocationCode,SurveyAddress,SurveyPostalCode,IsNeedSurvey,IsNSASkipSurvey,RowStatus,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
VALUES(@0,'','','','',@1,0,1,'OtosalesAPI',GETDATE(),NULL,NULL)
END
ELSE
BEGIN
UPDATE ordersimulationsurvey SET IsNeedSurvey=@1,ModifiedBy='OtosalesAPI',ModifiedDate=GETDATE() WHERE OrderNo=@0
END";
                        int isneedsurvey = OSMV.Year.Equals(DateTime.Now.Year.ToString()) ? 0 : 1;
                        db.Execute(query, OS.OrderNo, isneedsurvey);
                        #endregion

                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "VEHICLE", OS.CustID, OS.FollowUpNo, OS.OrderNo, GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, OS.FollowUpNo, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion
                    }

                    //return Json(new { status = true, message = "Success", OrderNo = OrderNoReturn });
                    OrderNoEnd = OrderNoReturn;
                }
                #endregion

                #region Cover Tab
                if (State.Equals("COVER") || State.Equals("PREMIUMSIMULATION"))
                {
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    OrderSimulation OS = JsonConvert.DeserializeObject<OrderSimulation>(OSData);
                    OrderSimulationMV OSMV = JsonConvert.DeserializeObject<OrderSimulationMV>(OSMVData);
                    List<OrderSimulationInterest> OSI = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> OSC = new List<OrderSimulationCoverage>();
                    OS.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OS.OrderNo;
                    OSMV.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OSMV.OrderNo;

                    bool isexistSFE = false;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        int a = 1;
                        int year = 1;
                        List<CalculatedPremi> calculatedPremiItem = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculatedPremiItems);

                        DateTime? pt = null;
                        foreach (CalculatedPremi cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (CalculatedPremi cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                        pt = cpitem.PeriodTo;
                                    }
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                        pf = cpitem.PeriodFrom;
                                    }
                                }
                                OrderSimulationInterest osi = new OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (CalculatedPremi cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;

                        foreach (OrderSimulationInterest item in OSI)
                        {
                            foreach (CalculatedPremi item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    OrderSimulationCoverage oscItem = new OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }

                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert

                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);


                        int i = 1;
                        foreach (OrderSimulationInterest data in OSI)
                        {
                            query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                            i++;
                        }

                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationInterest> TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationInterest data in TempAllOSI)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in OSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) && data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                        query = @"UPDATE OrderSimulationInterest SET 
                        //                                Premium=@5,
                        //                                LastUpdatedTime=GETDATE()
                        //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4 AND Deductible_Code=@6";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year, data2.Premium, data.DeductibleCode);
                        //                                    }
                        //                                }

                        //                                if (!isExist)
                        //                                {
                        //                                    //soft delete coverage

                        //                                    query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 ";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo);


                        //                                    //soft delete interest
                        //                                    query = @"DELETE OrderSimulationInterest " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion

                        #region DELETE UPDATE ORDERSIMULATION COVERAGE

                        #region Insert
                        //  OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        i = 1;
                        foreach (OrderSimulationCoverage data in OSC)
                        {
                            query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                            i++;

                        }
                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationCoverage> TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationCoverage data in TempAllOSC)
                        //                            {

                        //                                OrderSimulationInterest osiItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2", data.OrderNo, data.ObjectNo, data.InterestNo);
                        //                                if (osiItem != null)
                        //                                {
                        //                                    bool isExist = false;
                        //                                    foreach (OrderSimulationCoverage item in OSC)
                        //                                    {
                        //                                        if (item.CoverageID.TrimEnd().Equals(data.CoverageID.TrimEnd()) && item.InterestNo.Equals(data.InterestNo) && item.CoverageNo.Equals(data.CoverageNo))
                        //                                        {
                        //                                            isExist = true;
                        //                                            query = @"UPDATE OrderSimulationCoverage SET 
                        //      [Rate]=@5
                        //      ,[SumInsured]=@6
                        //      ,[LoadingRate]=@7
                        //      ,[Loading]=@8
                        //      ,[Premium]=@9
                        //      ,[BeginDate]=@10
                        //      ,[EndDate]=@11
                        //      ,[LastUpdatedTime]=GETDATE()
                        //      ,[RowStatus]=1
                        //      ,[IsBundling]=@12
                        //      ,[Entry_Pct]=@13
                        //      ,[Ndays]=@14
                        //      ,[Excess_Rate]=@15
                        //      ,[Calc_Method]=@16
                        //      ,[Cover_Premium]=@17
                        //      ,[Gross_Premium]=@18
                        //      ,[Max_si]=@19
                        //      ,[Net1]=@20
                        //      ,[Net2]=@21
                        //      ,[Net3]=@22
                        //      ,[Deductible_Code]=@23 WHERE [OrderNo]=@0 AND [ObjectNo]=@1 AND [InterestNo]=@2 AND [CoverageNo] =@3 AND [CoverageID]=@4";
                        //                                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                        //                                            data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);

                        //                                        }
                        //                                    }
                        //                                    //delete data
                        //                                    if (!isExist)
                        //                                    {
                        //                                        //soft delete coverage

                        //                                        query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageId=@4";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID);


                        //                                    }
                        //                                }

                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems
                        //                            TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationCoverage item in OSC)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationCoverage data in TempAllOSC)
                        //                                {
                        //                                    if (data.InterestNo.Equals(item.InterestNo) &&
                        //                           data.CoverageNo.Equals(item.CoverageNo) && data.CoverageID.TrimEnd().Equals(item.CoverageID.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (isExist)
                        //                                {
                        //                                    OrderSimulationCoverage oscitem = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageID=@4", item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.CoverageID);
                        //                                    if (oscitem != null)
                        //                                    {
                        //                                        query = @"UPDATE OrderSimulationCoverage SET 
                        //                                RowStatus=1
                        //                                ,Rate=@4
                        //                                ,SumInsured=@5
                        //                                ,LoadingRate=@6
                        //                                ,Loading=@7
                        //                                ,Premium=@8
                        //                                ,IsBundling=@9     
                        //                                ,LastUpdatedTime=GETDATE()  
                        //                                ,[Entry_Pct]=@10
                        //                                ,[Ndays]=@11
                        //                                ,[Excess_Rate]=@12
                        //                                ,[Calc_Method]=@13
                        //                                ,[Cover_Premium]=@14
                        //                                ,[Gross_Premium]=@15
                        //                                ,[Max_si]=@16
                        //                                ,[Net1]=@17
                        //                                ,[Net2]=@18
                        //                                ,[Net3]=@19
                        //                                ,[Deductible_Code]=@20                      
                        //WHERE OrderNo=@0 AND ObjectNo = @1 AND InterestNo=@2 AND CoverageNo=@3";
                        //                                        db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.Rate, item.SumInsured, item.LoadingRate, item.LoadingRate, item.Premium, item.IsBundling ? 1 : 0,
                        //                                             item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
                        //                                    }
                        //                                }
                        //                                else
                        //                                {

                        //                                    int i = db.ExecuteScalar<int>("SELECT max(CoverageNo) FROM OrderSimulationCoverage WHERE OrderNo=@0 ", item.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[CoverageNo]
                        //      ,[CoverageID]
                        //      ,[Rate]
                        //      ,[SumInsured]
                        //      ,[LoadingRate]
                        //      ,[Loading]
                        //      ,[Premium]
                        //      ,[BeginDate]
                        //      ,[EndDate]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]
                        //      ,[IsBundling]    
                        //      ,[Entry_Pct]
                        //      ,[Ndays]
                        //      ,[Excess_Rate]
                        //      ,[Calc_Method]
                        //      ,[Cover_Premium]
                        //      ,[Gross_Premium]
                        //      ,[Max_si]
                        //      ,[Net1]
                        //      ,[Net2]
                        //      ,[Net3]
                        //      ,[Deductible_Code])) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                        //                                    db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, i, item.CoverageID, item.Rate, item.SumInsured, item.LoadingRate, item.Loading, item.Premium, item.BeginDate, item.EndDate, item.IsBundling ? 1 : 0,
                        //                                                      item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
                        //                                    i++;
                        //                                }


                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = OS.PeriodFrom ?? DateTime.Now;
                            DateTime end = OS.PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,PeriodFrom=@12
                        ,PeriodTo=@13
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo, OS.PeriodFrom, OS.PeriodTo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion

                        OrderNoEnd = string.IsNullOrEmpty(OrderNoEnd) ? OS.OrderNo : OrderNoEnd;
                        FollowUpNoEnd = string.IsNullOrEmpty(FollowUpNoEnd) ? OS.FollowUpNo : FollowUpNoEnd;
                        CustIdEnd = string.IsNullOrEmpty(CustIdEnd) ? OS.CustID : CustIdEnd;
                        if (!MobileRepository.IsAllowedIEP(OS.ProductCode, isexistSFE))
                        {
                            return Json(new { status = false, message = "Basic Cover Not Applied", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd });
                        }

                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "COVER", CustIdEnd, FollowUpNoEnd, OS.OrderNo, GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, FollowUpNoEnd, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion

                    }

                    //    return Json(new { status = true, message = "Success", OrderSimulation = OS, FollowUpNo = FollowUpNoEnd });
                }
                return Json(new { status = true, message = "Success", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd, GuidTempPenawaran = GuidTempPenawaran });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetVehicle(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                var search = " ";
                var dataVehicle = new List<dynamic>();
                if (form == null) return BadRequest("Bad Request");
                else
                {
                    search = string.IsNullOrEmpty(form.Get("search")) ? "null" : form.Get("search");
                    string istlo = form.Get("istlo") == null ? "ALLRIK" : form.Get("istlo");

                    string isMVGodig = form.Get("isMvGodig");

                    bool flagIsMvGodig = false;
                    if (!string.IsNullOrEmpty(isMVGodig))
                    {
                        flagIsMvGodig = Boolean.Parse(isMVGodig);
                    }


                    dataVehicle = MobileRepository.GetVehicleFullTextTerm(search, istlo, flagIsMvGodig);
                    return Json(new { status = true, message = "Fetch success", data = dataVehicle });
                }

            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = e.ToString() });
            }
        }


        [HttpPost]
        public IHttpActionResult getVehicleUsage(FormDataCollection form)
        {

            string isMVGodig = form.Get("isMvGodig");

            _log.Debug("Begin - getVehicleUsage");
            try
            {
                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                var result = MobileRepository.GetVehicleUsage(flagIsMvGodig);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }

        [HttpPost]
        public IHttpActionResult getProductType(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string isMVGodig = form.Get("isMvGodig");
                string salesOfficerId = form.Get("salesOfficerId").Trim();

                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                var result = MobileRepository.GetProductType(salesOfficerId, flagIsMvGodig);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.ErrorFormat(e.ToString());
                return Json(new { status = false, message = "Fetch Error" });
            }
        }

        [HttpPost]
        public IHttpActionResult getProductCode(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string insurancetype = form.Get("insurancetype").Trim();
                string salesofficerid = form.Get("salesofficerid").Trim();
                int isRenew = string.IsNullOrEmpty(form.Get("isRenewal")) ? 0 : Convert.ToInt32(form.Get("isRenewal"));
                string isMVGodig = form.Get("isMvGodig");
                string isNew = form.Get("isNew");

                List<Otosales.Models.Product> result = new List<Otosales.Models.Product>();
                if (insurancetype == null)
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }
               

                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                bool flagIsNew = false;
                if (!string.IsNullOrEmpty(isNew))
                {
                    flagIsNew = Boolean.Parse(isNew);
                }

                result = MobileRepository.GetProduct(insurancetype, salesofficerid, isRenew, flagIsNew, flagIsMvGodig);


                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getBasicCover(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                List<BasicCover> BasicCover = new List<BasicCover>();

                //string Channel = form.Get("Channel").Trim();
                string vyear = form.Get("vyear").Trim();
                int year = string.IsNullOrEmpty(vyear) ? DateTime.Now.Year : Convert.ToInt32(vyear);
                bool isBasic = Convert.ToBoolean(form.Get("isBasic"));
                string isMVGodig = form.Get("isMvGodig");
                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                BasicCover = MobileRepository.GetBasicCover(year, isBasic, flagIsMvGodig);

                return Json(new { status = true, message = "OK", BasicCover }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult hitButtonCallMVGodig(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string GuidPenawaran = form.Get("GuidPenawaran");
            
                if (String.IsNullOrEmpty(GuidPenawaran))
                {
                    return Json(new { status = false, message = "error", data = "Param Required" });
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string CustID = System.Guid.NewGuid().ToString();
                    string FollowUpNo = System.Guid.NewGuid().ToString();
                    string OrderNo = System.Guid.NewGuid().ToString();

                    string query = @";EXEC usp_InsertIntoOtosalesTableMVGodig @0, @1, @2, @3";
                    db.Execute(query, GuidPenawaran, CustID, FollowUpNo, OrderNo );

                    query = ";SELECT FollowUpNo, CustID, ReferenceNo AS GuidPenawaran FROM FollowUp WHERE ReferenceNo = @0";
                    var data = db.FirstOrDefault<dynamic>(query, GuidPenawaran);
                    return Json(new { status = true, message = "OK", data = data }, Util.jsonSerializerSetting());
                }

            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult updateUserIsActive(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string SalesOfficerID = form.Get("SalesOfficerID");
                int IsLogin = int.Parse(form.Get("IsLogin"));

                mobileRepository.UpdateUserActive(SalesOfficerID, IsLogin);

                return Json(new { status = true, message = "OK" }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpStatus(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            List<FollowUpStatus> result = new List<FollowUpStatus>();
            try
            {
                string followupstatus = form.Get("followupstatus").Trim();
                string followupstatusinfo = form.Get("followupstatusinfo").Trim();
                string isRenewal = form.Get("isRenewal").Trim();
                string followupno = form.Get("followupno").Trim();
                string isMVGodigGet = form.Get("isMVGodig");


                if (string.IsNullOrEmpty(followupstatus))
                {
                    return Json(new { status = false, message = "Param is null", data = "No Data" }, Util.jsonSerializerSetting());
                }

                int isMVGodig = 0;
                if (!string.IsNullOrEmpty(isMVGodigGet))
                {
                    isMVGodig = int.Parse(isMVGodigGet);
                }
                string query = @";exec [usp_GetFollowUpStatusOtosales] @0,@1,@2,@3,@4";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    #region check empty next status option
                    string query2 = @";SELECT COUNT(part) from AsuransiAstra.GODigital.Fn_SplitString((SELECT " + (followupstatus.Equals("14") ? "ParamValue" : "ParamValue2") + " FROM ApplicationParameters WHERE ParamName='FUSTATUSEMPTYSTATUSOPTION' AND ParamType='CNF'),',') where part=@0";
                    int exist = db.ExecuteScalar<int>(query2, (followupstatus.Equals("14") ? followupstatus : followupstatusinfo));
                    query = exist == 0 ? query : "";
                    #endregion

                    if (!string.IsNullOrEmpty(query))
                    {
                        result = db.Fetch<FollowUpStatus>(query, followupstatus, followupstatusinfo, isRenewal, followupno, isMVGodig); //totalcount, 100);

                    }
                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult getFollowUpDetailsStatusInfo(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string statuscode = form.Get("statuscode").Trim();
                string statuscodebefore = form.Get("statuscodebefore").Trim();
                string statusinfocodebefore = form.Get("statusinfocodebefore").Trim();
                string IsRenewal = form.Get("IsRenewal").Trim();
                string NeedSurvey = form.Get("NeedSurvey").Trim();
                string NeedNSA = form.Get("NeedNSA").Trim();
                string followUpNo = form.Get("followUpNo").Trim();
                string isMVGodigGet = form.Get("isMVGodig");
                List<dynamic> result = new List<dynamic>();

                int isMVGodig = 0;
                if (!string.IsNullOrEmpty(isMVGodigGet))
                {
                    isMVGodig = int.Parse(isMVGodigGet);
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    result = db.Fetch<dynamic>(";exec [usp_GetFollowUpDetailsStatusInfoOtosales] @0,@1,@2,@3,@4,@5,@6,@7", statuscode, statuscodebefore, statusinfocodebefore, IsRenewal, NeedSurvey, NeedNSA, followUpNo, isMVGodig);

                    return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }



        [HttpPost]
        public IHttpActionResult SendViaWA(SendViaWAParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name; 
            try
            {
                if (string.IsNullOrEmpty(param.OrderNo) && string.IsNullOrEmpty(param.WAMessage) && string.IsNullOrEmpty(param.OrderNo))
                {
                    return BadRequest("Input Required");
                }
                bool status = MobileRepository.SendViaWA(param);
                if (status)
                {
                    return Json(new { status = true, message = "Success" });
                }
                else
                {
                    return Json(new { status = false, message = "Failed Send WA"});
                }

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                int currentPage = Convert.ToInt16(form.Get("currentPage"));
                int pageSize = Convert.ToInt16(form.Get("pageSize"));
                int currentPageLead = currentPage; // Convert.ToInt16(form.Get("currentPageLead"));
                int pageSizeLead = pageSize;// Convert.ToInt16(form.Get("pageSizeLead"));
                bool chIsSearchByChasNo = bool.Parse(form.Get("chIsSearchByChasNo").Trim());
                bool chIsSearchByEngNo = bool.Parse(form.Get("chIsSearchByEngNo").Trim());
                bool chIsSearchByPolicyNo = bool.Parse(form.Get("chIsSearchByPolicyNo").Trim());
                bool chIsSearchByProsName = bool.Parse(form.Get("chIsSearchByProsName").Trim());
                string salesofficerid = form.Get("salesofficerid").Trim();
                string search = form.Get("search").Trim();
                string indexOrderByItem = form.Get("indexOrderByItem").Trim();
                int indexOrderByTypeItem = Int32.Parse(form.Get("indexOrderByTypeItem").Trim());
                bool chFollowUpMonthlyChecked = bool.Parse(form.Get("chFollowUpMonthlyChecked").Trim());
                bool chFollowUpDateChecked = bool.Parse(form.Get("chFollowUpDateChecked").Trim());
                bool chNeedFUChecked = bool.Parse(form.Get("chNeedFUChecked").Trim());
                bool chPotentialChecked = bool.Parse(form.Get("chPotentialChecked").Trim());
                bool chCallLaterChecked = bool.Parse(form.Get("chCallLaterChecked").Trim());
                bool chCollectDocChecked = bool.Parse(form.Get("chCollectDocChecked").Trim());
                bool chNotDealChecked = bool.Parse(form.Get("chNotDealChecked").Trim());
                bool chSentToSAChecked = bool.Parse(form.Get("chSentToSAChecked").Trim());
                bool chBackToAOChecked = bool.Parse(form.Get("chBackToAOChecked").Trim());
                bool chPolicyCreatedChecked = bool.Parse(form.Get("chPolicyCreatedChecked").Trim());
                bool chOrderRejectedChecked = bool.Parse(form.Get("chOrderRejectedChecked").Trim());
                bool chNewChecked = bool.Parse(form.Get("chNewChecked").Trim());
                bool chRenewableChecked = bool.Parse(form.Get("chRenewableChecked").Trim());
                string dateFollowUp = form.Get("getFollowUpDate");
                int allowSearchOthers = Int32.Parse(form.Get("allowSearchOthers").Trim());

                DateTime getFollowUpDate = DateTime.Now;
                if (!string.IsNullOrEmpty(dateFollowUp))
                {
                    getFollowUpDate = Convert.ToDateTime(dateFollowUp);
                }

                if (salesofficerid == null || salesofficerid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @";SELECT * FROM (
SELECT DISTINCT * 
        FROM  (SELECT 0 
                      AS 
                            IsRenewalNonRenNot, 
                      CASE 
                        WHEN IsRenewalDigital = 1 THEN 'Renewable Digital - ' + mft.FollowUpTypeDes 
                        ELSE 
						CASE WHEN IsRenewal=1 THEN 'Renewal - ' + mft.FollowUpTypeDes
						ELSE 'New - ' + mft.FollowUpTypeDes END 
                      END 
                      AS 
                            TaskStatus, 
                      b.description 
                      AS 
                            FollowUpDesc, 
                      c.description 
                      AS 
FollowUpReason, 
CASE 
    WHEN os.OldPolicyNo IS NOT NULL THEN (SELECT TOP 1 Period_To FROM BEYONDREPORT.AAB.dbo.Policy Where Policy_No = os.OldPolicyNo)
    ELSE ''
END ExpiredDate, 
a.CustID, 
a.FollowUpNo, 
a.LastFollowUpDate, 
a.NextFollowUpDate, 
a.LastSeqNo, 
CASE 
    WHEN a.followupstatus = 13 
        OR a.isrenewal = 1 THEN os.PolicyNo
    ELSE '' 
END PolicyNo, 
CASE pcm.isCompany
	WHEN 1 THEN pcs.CompanyName
	ELSE a.ProspectName 
END ProspectName, 
a.EntryDate,
a.IsRenewal,
p.isHotProspect,
a.followupstatus,
mws.WAStatusDes,
a.ReferenceNo,
wm.WAStatusID
FROM   followup a WITH (NOLOCK) 
INNER JOIN followupstatus b WITH (NOLOCK) 
  ON a.followupstatus = b.statuscode AND a.RowStatus = 1
LEFT JOIN followupstatusinfo c WITH (NOLOCK) 
 ON a.followupinfo = c.infocode 
LEFT JOIN dbo.OrderSimulation os WITH (NOLOCK)
 ON os.FollowUpNo = a.FollowUpNo AND os.ApplyF = 1 AND os.RowStatus = 1
 LEFT JOIN dbo.OrderSimulationMV omv WITH (NOLOCK)
 ON omv.OrderNo = os.OrderNo
 LEFT JOIN dbo.ProspectCustomer pcm 
 ON pcm.CustID = a.CustID
 LEFT JOIN dbo.ProspectCompany pcs
 ON pcs.CustID = a.CustID
 LEFT JOIN Quotation.dbo.HistoryPenawaran hp WITH (NOLOCK)
 ON hp.OrderNo = os.OrderNo 
 LEFT JOIN Quotation.dbo.Penawaran p WITH (NOLOCK)
 ON p.policyID = hp.PolicyID AND a.FollowUpStatus = 1 AND a.RowStatus = 1
 LEFT JOIN dbo.WAMessaging wm ON wm.OrderNo = os.OrderNo AND wm.ID = 
 (SELECT MAX(wm1.ID) FROM dbo.WAMessaging wm1 WHERE wm1.OrderNo = os.OrderNo AND wm1.WAStatusID IS NOT NULL) 
 LEFT JOIN dbo.MstWAStatus mws ON mws.WAStatusID = wm.WAStatusID
 LEFT JOIN dbo.MstFollowUpType mft ON mft.FollowUpTypeID = a.FollowUpTypeID
WHERE  #salesofficerid
#nextfollowupdate 
 #followup  
 #chasisno #engineno
 #name
 #policyno) a
#datakotor
#quotationlead
) res
#isrenewal
ORDER  BY #orderby";
                string qsalesofficerid = "";
                string qdatakotor = "";
                qsalesofficerid = string.IsNullOrEmpty(search) ? " a.salesofficerid = @0 AND " : "";
                qdatakotor = !string.IsNullOrEmpty(search) ? @"UNION ALL 
SELECT *
FROM  (SELECT 1                   AS IsRenewalNonRenNot, 
'Renewable - MV'         AS TaskStatus, 
'Need FU'           AS FollowUpDesc, 
'Data baru diinput' AS FollowUpReason, 
p.Period_To   AS ExpiredDate, 
''                  CustID, 
''                  FollowUpNo, 
NULL                AS LastFollowUpDate, 
NULL                AS NextFollowUpDate, 
0                   AS LastSeqNo, 
ERP.policy_no       AS PolicyNo, 
p.Name_On_Policy    AS ProspectName, 
p.entrydt           AS EntryDate,
1                   AS IsRenewal,
NULL                isHotProspect,
1                   AS followupstatus,
NULL                AS WAStatusDes,
NULL                ReferenceNo,
NULL                WAStatusID
FROM [BeyondReport].[AAB].[dbo].[excluded_renewal_policy] ERP 
INNER JOIN [BeyondReport].[AAB].[dbo].policy p 
  ON p.policy_no = ERP.policy_no AND RowStatus=0
 AND p.Status = 'A' AND p.RENEWAL_STATUS = 0
 AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0 ) #2name
     #2chasisno #2engineno #2policyno) b" : "";
                string qSalesOfficerIDQL = "";
                qSalesOfficerIDQL = string.IsNullOrEmpty(search) ? " ql.SalesOfficerID = @0 AND " : "";
                string qGetQuotationLead = @"UNION ALL 
SELECT * FROM  (
SELECT 0              AS IsRenewalNonRenNot, 
'New - MV GODIGITAL'  AS TaskStatus, 
'Need FU'             AS FollowUpDesc, 
'Data baru diinput'   AS FollowUpReason, 
''					  AS ExpiredDate, 
''                    CustID, 
''                    FollowUpNo, 
NULL                  AS LastFollowUpDate, 
NULL                  AS NextFollowUpDate, 
0                     AS LastSeqNo, 
ql.PolicyNo	          AS PolicyNo, 
ql.CustName           AS ProspectName, 
ql.CreatedDate        AS EntryDate,
0                     AS IsRenewal,
NULL                  AS isHotProspect,
1                     AS followupstatus,
NULL                  AS WAStatusDes,
ql.ReferenceNo        ReferenceNo,
NULL                  WAStatusID 
FROM dbo.QuotationLead ql 
LEFT JOIN dbo.FollowUp f ON f.ReferenceNo = ql.ReferenceNo
WHERE #qlsalesofficerid f.ReferenceNo IS NULL AND ql.RowStatus = 1 
AND ql.SalesOfficerID IS NOT NULL
#3name #3chasisno #3engineno #3policyno
) c";
                string qnextfollowupdate = "";
                if (chFollowUpDateChecked)
                {
                    qnextfollowupdate = @" ( nextfollowupdate <= @2 OR nextfollowupdate IS NULL ) ";
                }
                else if (chFollowUpMonthlyChecked)
                {
                    qnextfollowupdate = @" ((MONTH(a.EntryDate) = MONTH(GETDATE()) 
                                            AND YEAR(a.EntryDate) = YEAR(GETDATE()))) ";
                }

                List<int> listStatus = new List<int>();
                List<int> listInfo = new List<int>();

                if (chNeedFUChecked)
                {
                    listStatus.Add(1);
                }
                if (chPotentialChecked)
                {
                    listStatus.Add(2);
                }
                if (chCallLaterChecked)
                {
                    listStatus.Add(3);
                }
                if (chCollectDocChecked)
                {
                    listInfo.Add(56);
                }
                if (chNotDealChecked)
                {
                    listStatus.Add(5);
                }
                if (chSentToSAChecked)
                {
                    listInfo.Add(61);
                }
                if (chPolicyCreatedChecked)
                {
                    listInfo.Add(50);
                    listInfo.Add(51);
                    listInfo.Add(52);
                    listInfo.Add(53);
                    listInfo.Add(54);
                    listInfo.Add(55);
                }
                if (chBackToAOChecked)
                {
                    listInfo.Add(60);
                }
                if (chOrderRejectedChecked)
                {
                    listStatus.Add(14);
                }

                string qfollowup = "";
                qfollowup = @" AND ( followupstatus IN(@3) OR (followupinfo IN(@4) AND followupinfo IS NOT NULL AND followupinfo <> 0)) ";
                string qisrenewal = "";
                qisrenewal = @"WHERE {0} {1} ";
                string snew = "";
                string srenew = "";

                if (chNewChecked)
                {
                    snew = "isrenewal = 0 OR isrenewal IS NULL";
                }
                if (chRenewableChecked)
                {
                    if (chNewChecked)
                    {
                        srenew = "OR isrenewal = 1";
                    }
                    else
                    {
                        srenew = "isrenewal = 1";
                    }
                }
                if (!chNewChecked && !chRenewableChecked)
                {
                    snew = "isrenewal = 2";
                    srenew = "";
                }
                qisrenewal = string.Format(qisrenewal, snew, srenew);
                string qchasisno = "";
                string qengineno = "";
                string qchasisno2 = "";
                string qengineno2 = "";
                string qchasisno3 = "";
                string qengineno3 = "";
                string qpolicyno = "";
                string qpolicyno2 = "";
                string qpolicyno3 = "";
                string qname = "";
                string qname2 = "";
                string qname3 = "";
                if (!string.IsNullOrEmpty(search))
                {
                    if (chIsSearchByChasNo)
                    {
                        qchasisno = " AND omv.ChasisNumber = @1 ";
                        qchasisno2 = " AND ERP.Chasis_Number = @1 ";
                        qchasisno3 = " AND ql.ChasisNumber = @1 ";
                    }
                    else if (chIsSearchByEngNo)
                    {
                        qengineno = " AND omv.EngineNumber = @1 ";
                        qengineno2 = " AND ERP.Engine_Number = @1 ";
                        qengineno3 = " AND ql.EngineNumber = @1 ";
                    }
                    else if (chIsSearchByPolicyNo)
                    {
                        qpolicyno = "AND os.PolicyNo = @1 ";
                        qpolicyno2 = " AND ERP.Policy_No = @1 ";
                        qpolicyno3 = " AND ql.PolicyNo = @1 ";
                    }
                    else if (chIsSearchByProsName)
                    {
                        //Flag form UI to Search Others ProsName - 0213/URF/2019
                        if (allowSearchOthers == 0) //
                        {
                            qsalesofficerid = " a.salesofficerid = @0 AND ";
                        }

                        search = '%' + search + '%';
                        qname = " AND (pcm.Name LIKE @1 OR pcs.CompanyName LIKE @1)";
                        qname2 = " WHERE Name_On_Policy LIKE @1 ";
                        qname3 = " AND ql.CustName LIKE @1 ";
                    }
                }

                string qorderby = "";
                if (indexOrderByItem.Equals("Status"))
                {
                    qorderby = @" isHotProspect DESC, IsRenewalNonRenNot ASC , COALESCE(isrenewal,0) ASC, COALESCE(NextFollowUpDate,DATEADD(YEAR,5,GETDATE())) ASC   , (CASE WHEN isrenewal = 1 THEN followupstatus END) ASC, (CASE WHEN isrenewal = 1 AND followupstatus=1 THEN ExpiredDate END) ASC, (CASE WHEN (isrenewal = 0 OR isrenewal IS NULL AND nextfollowupdate is null) OR (isrenewal = 1 AND followupstatus!=1)  THEN CAST(EntryDate AS DATE) END) DESC, WAStatusID DESC";
                }
                if (indexOrderByItem.Equals("Name"))
                {
                    qorderby = "ProspectName";
                }
                if (indexOrderByItem.Equals("EntryDate"))
                {
                    qorderby = "EntryDate";
                }
                if (indexOrderByItem.Equals("LastFollowUpdate"))
                {
                    qorderby = "LastFollowUpdate";
                }
                if (indexOrderByItem.Equals("NextFollowUpdate"))
                {
                    qorderby = "NextFollowUpdate";
                }


                if (!indexOrderByItem.Equals("Status"))
                {
                    if (indexOrderByTypeItem == 0)
                    {
                        qorderby = string.Concat(qorderby, " ASC");
                    }
                    else
                    {
                        qorderby = string.Concat(qorderby, " DESC");
                    }
                }
                query = query.Replace("#quotationlead", qGetQuotationLead);
                query = query.Replace("#datakotor", qdatakotor);
                query = query.Replace("#salesofficerid", qsalesofficerid);
                query = query.Replace("#qlsalesofficerid", qSalesOfficerIDQL);
                query = query.Replace("#nextfollowupdate", qnextfollowupdate);
                query = query.Replace("#followup", qfollowup);
                query = query.Replace("#isrenewal", qisrenewal);
                query = query.Replace("#chasisno", qchasisno);
                query = query.Replace("#engineno", qengineno);
                query = query.Replace("#2chasisno", qchasisno2);
                query = query.Replace("#3chasisno", qchasisno3);
                query = query.Replace("#2engineno", qengineno2);
                query = query.Replace("#3engineno", qengineno3);
                query = query.Replace("#policyno", qpolicyno);
                query = query.Replace("#2policyno", qpolicyno2);
                query = query.Replace("#3policyno", qpolicyno3);
                query = query.Replace("#name", qname);
                query = query.Replace("#2name", qname2);
                query = query.Replace("#3name", qname3);
                query = query.Replace("#orderby", qorderby);

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    dynamic result = null;
                    result = db.Page<dynamic>(currentPage, pageSize, query, salesofficerid, search, getFollowUpDate
                        , listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 });

                    return Json(new { status = true, message = "OK", data = result, /*prospectLeadList = prospectLeadListPage, historyPenawaranList = historyPenawaranList, penawaranList = penawaranList*/ }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetPhoneList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                List<Otosales.Models.vWeb2.PhoneNoModel> res = AABRepository.GetPhoneList(OrderNo);
                return Json(new { status = true, message = "Success", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult InsertUpdatePhoneNo(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                List<Otosales.Models.vWeb2.PhoneNoModel> res = AABRepository.GetPhoneList(form.OrderNo);
                List<string> s = new List<string>();
                foreach (Otosales.Models.vWeb2.PhoneNoModel p in res)
                {
                    s.Add(p.NoHp);
                }
                foreach (Otosales.Models.vWeb2.PhoneNoParam n in form.PhoneNoParam)
                {
                    if (string.IsNullOrEmpty(n.ColumnName) && s.Contains(n.PhoneNo))
                    {
                        return Json(new { status = true, message = "Success", data = "Phone Number " + n.PhoneNo + " already exist" });
                    }
                }
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                bool isrenewal = db.ExecuteScalar<bool>(@"SELECT CAST(COALESCE(IsRenewal,0) AS BIT) FROM dbo.FollowUp f
                                        INNER JOIN dbo.OrderSimulation os 
                                        ON os.FollowUpNo = f.FollowUpNo
                                        WHERE os.OrderNo = @0 AND os.RowStatus = 1 AND os. ApplyF = 1", form.OrderNo);
                if (isrenewal)
                {
                    AABRepository.InsertUpdatePhoneNo(form);
                }
                else {
                    AABRepository.InsertUpdatePhoneNoOrderNew(form);
                }
                res = AABRepository.GetPhoneList(form.OrderNo);
                return Json(new { status = true, message = "Success", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + "Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveFollowUpStatusDetails(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string FUData = form.Get("FollowUp");
                string Editor = form.Get("editor");
                Otosales.Models.FollowUp FU = JsonConvert.DeserializeObject<Otosales.Models.FollowUp>(FUData);

                string FULasData = form.Get("FollowUpLast");
                Otosales.Models.FollowUp FULast = JsonConvert.DeserializeObject<Otosales.Models.FollowUp>(FULasData);

                bool isInfoChanged = false;

                bool isSentToSA = bool.Parse(form.Get("isSentToSA").Trim());
                string STATE = form.Get("STATE");
                string TRName = form.Get("TRName");

                #region MGO BID
                if (STATE.Equals("MGOBID"))
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        string query = @";SELECT TOP 1 * FROM Quotation.dbo.HistoryPenawaran 
WHERE OrderNo = (SELECT TOP 1 OrderNo FROM OrderSimulation 
WHERE FollowUpNo =@0 ORDER BY EntryDate DESC)";

                        Otosales.Models.HistoryPenawaran hp = db.FirstOrDefault<Otosales.Models.HistoryPenawaran>(query, FU.FollowUpNo);
                        hp.IsAbleExpired = 0;

                        bool responsestatus = false;

                        // check all mgo bid if expired bid still exist
                        Util.CheckAllMGOBidExpiry();

                        using (var dbagent = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                        {
                            // check data not expired
                            int bidExist = dbagent.ExecuteScalar<int>("SELECT COUNT(*) FROM PENAWARAN p JOIN HistoryPenawaran hp ON hp.PolicyID = p.PolicyID AND hp.HistoryPenawaranID =  @0 AND BidStatus = 1", hp.HistoryPenawaranID);

                            if (bidExist == 1)
                            {
                                // permanently set data to agent
                                dbagent.Execute("UPDATE HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);
                                responsestatus = true;
                            }

                            if (responsestatus)
                            {

                                #region execUpdateFollowUp
                                #region UPDATE FOLLOW UP
                                if (FU.FollowUpInfo != FULast.FollowUpInfo)
                                {
                                    isInfoChanged = true;


                                }
                                query = @";UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6,[Receiver]=@7 WHERE [FollowUpNo]=@0";

                                db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1, FU.Receiver);

                                #endregion

                                if (isInfoChanged)
                                {
                                    #region INSERT FOLLOW UP HISTORY
                                    query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,GETDATE(),@10,@11,@12,@13,GETDATE(),0)
";
                                    int lastseqnofuh = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                    db.Execute(query, FU.FollowUpNo, lastseqnofuh, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                                    #endregion
                                    Otosales.Repository.v0213URF2019.MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);                                    

                                }
                                string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                                #endregion

                                int lastseqno = db.ExecuteScalar<int>("SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC", FU.FollowUpNo) + 1;

                                db.Execute("UPDATE Quotation.dbo.HistoryPenawaran SET IsAbleExpired = 0 WHERE HistoryPenawaranID = @0", hp.HistoryPenawaranID);

                                query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                                Otosales.Repository.v0213URF2019.MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = message, data = db.FirstOrDefault<Otosales.Models.FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());

                            }
                            else
                            {
                                query = @";SELECT * FROM OrderSimulation  WHERE FollowUpNo = @0";
                                List<Otosales.Models.OrderSimulation> orderSimulation = db.Fetch<Otosales.Models.OrderSimulation>(query, FU.FollowUpNo);

                                query = @";SELECT * FROM Quotation.dbo.HistoryPenawaran hp 
JOIN OrderSimulation os ON os.OrderNo = hp.OrderNo 
JOIN FollowUp fu On fu.FollowUpNo = os.FollowUpNo  WHERE fu.FollowUpNo = @0";

                                Otosales.Models.HistoryPenawaran hptemp = db.FirstOrDefault<Otosales.Models.HistoryPenawaran>(query, FU.FollowUpNo);

                                foreach (Otosales.Models.OrderSimulation os in orderSimulation)
                                {
                                    string queryDeleted = @";UPDATE OrderSimulationCoverage SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationInterest SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulationMV SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE OrderSimulation SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);
                                    queryDeleted = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE OrderNo=@0";
                                    db.Execute(queryDeleted, os.OrderNo);

                                }

                                string queryDelete = @";UPDATE FollowUpHistory SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE FollowUp SET RowStatus=0 WHERE FollowUpNo=@0";
                                db.Execute(queryDelete, FU.FollowUpNo);
                                queryDelete = @";UPDATE ProspectCustomer SET RowStatus=0 WHERE CustID=@0";
                                db.Execute(queryDelete, FU.CustID);
                                queryDelete = @";UPDATE Quotation.dbo.HistoryPenawaran SET RowStatus=0 WHERE HistoryPenawaranID=@0";
                                db.Execute(queryDelete, hptemp.HistoryPenawaranID);

                                int hpCount = db.ExecuteScalar<int>("SELECT COUNT(*) FROM HistoryPenawaran WHERE PolicyID = @0", hp.PolicyID);
                                if (hpCount == 0)
                                {
                                    queryDelete = @";UPDATE Quotation.dbo.Penawaran SET RowStatus=0 WHERE PolicyID=@0";
                                    db.Execute(queryDelete, hptemp.HistoryPenawaranID);
                                }

                                Otosales.Repository.v0213URF2019.MobileRepository.InsertMstOrderMobile(FU);
                                return Json(new { status = true, message = "Unable to process your request. This prospect has been expired" }, Util.jsonSerializerSetting());
                            }

                        }
                    }

                }
                #endregion

                else
                {
                    # region execUpdateFollowUp
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        #region UPDATE FOLLOW UP
                        if (FU.FollowUpInfo != FULast.FollowUpInfo)
                        {
                            isInfoChanged = true;
                        }
                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
BEGIN
SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
END
ELSE
BEGIN
SELECT 0 AS SeqNo
END", FU.FollowUpNo) + 1;

                        string query = @"UPDATE FollowUp SET [FollowUpName]=@1,[FollowUpStatus]=@2,[FollowUpInfo]=@3,[Remark]=@4,[LastFollowUpDate]= GETDATE(),[NextFollowUpDate]= @5,[LastUpdatedTime]=GETDATE(), [LastSeqNo] = @6, [FollowUpNotes]=@7,[PICWA]=@8,[Receiver]=@9 WHERE [FollowUpNo]=@0";

                        db.Execute(query, FU.FollowUpNo, FU.FollowUpName, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.NextFollowUpDate, lastseqno, FU.FollowUpNotes, FU.PICWA, FU.Receiver);

                        if (FU.FollowUpStatus == 5)
                        {
                            string PolicyOrderNo = "";
                            query = @"SELECT COALESCE(PolicyOrderNo,'') FROM dbo.OrderSimulation WHERE FollowUpNo = @0 AND CustID = @1 AND ApplyF = 1 AND RowStatus = 1";
                            PolicyOrderNo = db.ExecuteScalar<string>(query, FU.FollowUpNo, FU.CustID);
                            if (!string.IsNullOrEmpty(PolicyOrderNo))
                            {
                                query = "UPDATE dbo.Mst_Order set Order_Status = '6' WHERE Order_No = @0";
                                var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                                aabdb.Execute(query, PolicyOrderNo);
                                db.Execute(@";IF EXISTS(SELECT * FROM Asuransiastra.GODigital.OrderSimulation WHERE PolicyOrderNo = @0)
                                            BEGIN
	                                            UPDATE Asuransiastra.GODigital.OrderSimulation 
	                                            SET OrderStatus = '14'
	                                            WHERE PolicyOrderNo = @0
                                            END", PolicyOrderNo);
                            }
                        }
                        #endregion

                        if (isInfoChanged)
                        {
                            #region INSERT FOLLOW UP HISTORY
                            query = @";
    INSERT INTO FollowUpHistory (
       [FollowUpNo]
      ,[SeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[NextFollowUpDate]
      ,[LastFollowUpDate]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,GETDATE(),@9,@10,@11,@12,GETDATE(),0)
";

                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FULast.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                            #endregion
                            Otosales.Repository.v0213URF2019.MobileRepository.SendPushNotifEditedByOthers(Editor, FU.SalesOfficerID, FU.FollowUpNo);

                        }
                        string message = (isSentToSA ? "Follow up has been sent to SA" : "Follow up details has been saved");

                        query = @"SELECT * FROM FollowUp WHERE [FollowUpNo]=@0 Order by [LastSeqNo] desc";

                        Otosales.Repository.v0213URF2019.MobileRepository.InsertMstOrderMobile(FU);
                        return Json(new { status = true, message = message, data = db.FirstOrDefault<Otosales.Models.FollowUp>(query, FU.FollowUpNo) }, Util.jsonSerializerSetting());
                    }
                    #endregion
                }



            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [HttpPost]
        public IHttpActionResult GetSendEmailTemplate(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetEmailTemplate(OrderNo, 0);
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult GetButtonEmailTemplate(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                {
                    return BadRequest("Input Required");
                }
                dynamic data = MobileRepository.GetEmailTemplate(OrderNo, 1);
                return Json(new { status = true, message = "Success", data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult GetOrderNoQuotation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string ReferenceNo = form.Get("ReferenceNo");
                if (string.IsNullOrEmpty(ReferenceNo))
                {
                    return BadRequest("Input Required");
                }
                string data = MobileRepository.GetOrderNoQuotation(ReferenceNo);
                return Json(new { status = true, message = "Success", OrderNo = data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }


        [HttpPost]
        public IHttpActionResult GetSoIdQuotationLead(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string ReferenceNo = form.Get("ReferenceNo");
                if (string.IsNullOrEmpty(ReferenceNo))
                {
                    return BadRequest("Input Required");
                }
                string data = MobileRepository.GetSoIdQuotationLead(ReferenceNo);
                return Json(new { status = true, message = "Success", SalesOfficerID = data });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.StackTrace + "," + e.ToString() });
            }
        }
        [HttpPost]
        public IHttpActionResult BasicPremiCalculation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");

            #region declare param
            List<PremiumCalculation.Model.DetailScoring> dtlScoring = new List<PremiumCalculation.Model.DetailScoring>();
            List<decimal> RenDiscountPct = new List<decimal>();
            List<decimal> DiscountPct = new List<decimal>();
            int comprePeriod = Convert.ToInt32(form.Get("comprePeriod"));
            int TLOPeriod = Convert.ToInt32(form.Get("TLOPeriod"));
            string vPeriodFrom = form.Get("PeriodFrom");
            string vPeriodTo = form.Get("PeriodTo");
            // string vPeriodFromCover = form.Get("PeriodFromCover");
            // string vPeriodToCover = form.Get("PeriodToCover");
            string vtype = form.Get("vtype");
            string vcitycode = form.Get("vcitycode");
            string vusagecode = form.Get("vusagecode");
            string vyear = form.Get("vyear");
            string vsitting = form.Get("vsitting");
            ProductCode = form.Get("vProductCode");
            string vMouID = ProductCode;//form.Get("vMouID");
            decimal vTSInterest = Convert.ToDecimal(form.Get("vTSInterest"));
            decimal vPrimarySI = Convert.ToDecimal(form.Get("vPrimarySI"));
            string vCoverageId = form.Get("vCoverageId");
            string vInterestId = "CASCO";//form.Get("vInterestId");
            string vModel = form.Get("vModel");
            string vBrand = form.Get("vBrand");
            string vIsNew = form.Get("isNew");
            string pQuotationNo = form.Get("pQuotationNo");
            string Ndays = form.Get("Ndays");
            string OldPolicyNo = form.Get("OldPolicyNo");
            //int calcmethod = Convert.ToInt16(form.Get("calcMethod"));
            if (string.IsNullOrEmpty(Ndays) || string.IsNullOrEmpty(vCoverageId) || string.IsNullOrEmpty(ProductCode) || string.IsNullOrEmpty(vPeriodFrom) || string.IsNullOrEmpty(vPeriodTo))
            {
                return Json(new
                {
                    status = false,
                    message = "Param Not Completed"
                });
            }
            DateTime PeriodFrom = DateTime.ParseExact(vPeriodFrom, "yyyy-MM-dd", null);
            DateTime PeriodTo = DateTime.ParseExact(vPeriodTo, "yyyy-MM-dd", null);
            DateTime PeriodFromCover = PeriodFrom;
            DateTime PeriodToCover = PeriodTo;
            List<CalculatedPremi> result = new List<CalculatedPremi>();
            #region set vehicleyear to year now if not choose vehicle
            if (Convert.ToInt16(vyear) == 0)
            {
                vyear = Convert.ToString(DateTime.Now.Year);
            }

            #endregion
            #endregion

            try
            {
                #region check product code is valid in mapping segment
                if (Otosales.Repository.vWeb2.MobileRepository.isValidMappingSegment(ProductCode))
                {
                    var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    #region Initialize Enable Coverage
                    InitializeEnableCoverage();
                    #endregion
                    #region initialize Param

                    List<Otosales.Models.CoverageParam> CoverageList = new List<Otosales.Models.CoverageParam>();
                    if (comprePeriod == 0 && TLOPeriod == 0)
                    {
                        CoverageList.Add(new Otosales.Models.CoverageParam { CoverageId = vCoverageId, PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });
                    }
                    else
                    {
                        if (comprePeriod > 0 && TLOPeriod > 0)
                        {
                            CoverageList.Add(new Otosales.Models.CoverageParam { CoverageId = "ALLRIK", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodFromCover.AddYears(comprePeriod) });
                            CoverageList.Add(new Otosales.Models.CoverageParam { CoverageId = "TLO", PeriodFromCover = PeriodFromCover.AddYears(comprePeriod), PeriodToCover = PeriodFromCover.AddYears(comprePeriod + TLOPeriod) });

                        }
                        else if (comprePeriod > 0)
                        {
                            CoverageList.Add(new Otosales.Models.CoverageParam { CoverageId = "ALLRIK", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });

                        }
                        else if (TLOPeriod > 0)
                        {
                            CoverageList.Add(new Otosales.Models.CoverageParam { CoverageId = "TLO", PeriodFromCover = PeriodFromCover, PeriodToCover = PeriodToCover });

                        }
                    }
                    #endregion
                    bool isProductSupported = true;
                    CalculatedPremiItems = Otosales.Repository.vWeb2.MobileRepository.CalculateBasicPremi(CoverageList, ProductCode, vcitycode, vusagecode, vBrand, vtype, vModel, vIsNew, vsitting, vyear, PeriodFrom, PeriodTo, Ndays, vTSInterest, vPrimarySI, vInterestId, OldPolicyNo, out RenDiscountPct, out isProductSupported);
                    if (!isProductSupported)
                    {
                        return Json(new
                        {
                            status = true,
                            message = "Product Is Not Supported!"
                        });

                    }
                #endregion

                    #region recalculate extended
                    PremiumCalculation.Model.DetailScoring ds = new PremiumCalculation.Model.DetailScoring();
                    if (!string.IsNullOrEmpty(vcitycode))
                    {
                        ds.FactorCode = "GEOGRA";
                        ds.InsuranceCode = vcitycode;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vusagecode))
                    {
                        ds = new PremiumCalculation.Model.DetailScoring();
                        ds.FactorCode = "MVUSAG";
                        ds.InsuranceCode = vusagecode;
                        dtlScoring.Add(ds);
                    }
                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    ds.InsuranceCode = string.IsNullOrEmpty(vtype) ? "ALL   " : vtype;
                    dtlScoring.Add(ds);
                    if (!string.IsNullOrEmpty(vBrand))
                    {

                        ds = new PremiumCalculation.Model.DetailScoring();
                        ds.FactorCode = "VBRAND";
                        ds.InsuranceCode = vBrand;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vIsNew))
                    {

                        ds = new PremiumCalculation.Model.DetailScoring();
                        ds.FactorCode = "NEWVHC";
                        ds.InsuranceCode = vIsNew;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vModel))
                    {
                        ds = new PremiumCalculation.Model.DetailScoring();
                        ds.FactorCode = "VMODEL";
                        ds.InsuranceCode = vModel;
                        dtlScoring.Add(ds);
                    }

                    if (!string.IsNullOrEmpty(OldPolicyNo))
                    {
                        List<CalculatedPremi> recalcpremi = db.Fetch<CalculatedPremi>(@";SELECT a.InterestID,b.coverageid,case when coverageid in('TRS','TRRTLO') then osmv.SumInsured else b.SumInsured end SumInsured,b.begindate AS PeriodFrom,b.enddate AS PeriodTo FROM OrderSimulation os inner join orderSimulationinterest a on a.orderno=os.orderno 
									 inner join orderSimulationcoverage b on a.orderno=b.orderno and a.interestno=b.interestno and b.RowStatus=1  and b.orderno=os.orderno
									 inner join OrderSimulationMV osmv on osmv.orderno=os.orderno 
                                     where a.RowStatus=1 AND os.oldpolicyno=@0 and (a.interestid='CASCO' AND b.coverageid in('TRS','TRRTLO') OR a.InterestID in('TPLPER','PADRVR','PAPASS')OR (a.InterestID='ACCESS' AND b.coverageid in('ALLRIK','TLO') AND b.BeginDate=CONVERT(char,YEAR(os.PeriodFrom))+'-'+CONVERT(char,MONTH(os.PeriodFrom))+'-'+CONVERT(char,DAY(os.PeriodFrom))+' 00:00:00.000' ))order by a.interestid desc", OldPolicyNo);


                        foreach (CalculatedPremi item in recalcpremi)
                        {
                            if (item.InterestID.Equals("ACCESS"))
                            {
                                List<Otosales.Models.CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<Otosales.Models.CalculatedPremi>>(JsonConvert.SerializeObject(CalculatedPremiItems));
                                List<Otosales.Models.CalculatedPremi> cItems = Otosales.Repository.vWeb2.MobileRepository.CalculateACCESS(vTSInterest, Convert.ToDecimal(item.SumInsured), ProductCode, PeriodFrom, PeriodTo, "ACCESS", vtype, vyear, vsitting, pQuotationNo, calculateaccess, dtlScoring, OldPolicyNo, out RenDiscountPct, out DiscountPct);
                                CalculatedPremiItems.AddRange(cItems);

                            }
                        }
                    }
                    #endregion
                    #region calculate final item

                    RefreshPremi();
                    SumInsured = Convert.ToDouble(vTSInterest);
                    CalculateLoading();

                    decimal NoClaimBonus = Otosales.Repository.vWeb2.MobileRepository.CalculateNCB(GrossPremi, RenDiscountPct);
                    decimal DiscountPremi = Otosales.Repository.vWeb2.MobileRepository.CalculateDiscountPremi(GrossPremi, DiscountPct);
                    CalculateTotalPremi();

                    #endregion

                    return Json(new
                    {
                        status = true,
                        message = "OK",
                        ProductCode,
                        CalculatedPremiItems,
                        //coveragePeriodItems,
                        //coveragePeriodNonBasicItems,
                        IsTPLEnabled,
                        IsTPLChecked,
                        IsTPLSIEnabled,
                        IsSRCCChecked,
                        IsSRCCEnabled,
                        IsFLDChecked,
                        IsFLDEnabled,
                        IsETVChecked,
                        IsETVEnabled,
                        IsTSChecked,
                        IsTSEnabled,
                        IsPADRVRChecked,
                        IsPADRVREnabled,
                        IsPADRVRSIEnabled,
                        IsPASSEnabled,
                        IsPAPASSSIEnabled,
                        IsPAPASSEnabled,
                        IsPAPASSChecked,
                        IsACCESSChecked,
                        IsACCESSEnabled,
                        IsACCESSSIEnabled,
                        SRCCPremi,
                        FLDPremi,
                        ETVPremi,
                        TSPremi,
                        PADRVRPremi,
                        PAPASSPremi,
                        TPLPremi,
                        ACCESSPremi,
                        AdminFee,
                        TotalPremi,
                        GrossPremi,
                        NoClaimBonus,
                        DiscountPremi
                        //Alert
                    }, Util.jsonSerializerSetting());
                }
                else
                {
                    return Json(new { status = false, message = "Product Has Not Mapping Segment", data = "null" });

                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public IHttpActionResult DownloadQuotation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string OrderNo = form.Get("OrderNo");

                DownloadQuotationResult result = MobileRepository.GetByteReport(OrderNo);
                return Json(new { status = true, message = "success", data = result });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Error", data = e.ToString() });
                throw e;
            }
        }

        #region private
        private void InitializeEnableCoverage()
        {
            try
            {
                #region initialize enable cover

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string query = @"SELECT * FROM Prd_Interest_Coverage WHERE Product_Code=@0";
                prdInterestCoverage = db.Fetch<Otosales.Models.Prd_Interest_Coverage>(query, ProductCode);
                foreach (Otosales.Models.Prd_Interest_Coverage prd in prdInterestCoverage)
                {
                    if (prd.Coverage_Id.Trim().Equals("SRCC") ||
                            prd.Coverage_Id.Trim().Equals("SRCTLO") ||
                            prd.Coverage_Id.Trim().Equals("SRCCTS"))
                    {
                        IsSRCCEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Equals("FLD") ||
                          prd.Coverage_Id.Trim().Equals("FLDTLO"))
                    {
                        IsFLDEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Equals("ETV") ||
                          prd.Coverage_Id.Trim().Equals("ETVTLO") ||
                            prd.Coverage_Id.Trim().Equals("EQK"))
                    {
                        IsETVEnabled = true;
                    }
                    if (prd.Coverage_Id.Trim().Equals("TRS") ||
                         prd.Coverage_Id.Trim().Equals("TRRTLO"))
                    {
                        IsTSEnabled = true;
                    }
                    else if (prd.Coverage_Id.Trim().Contains("MVTP"))
                    {
                        IsTPLEnabled = true;
                    }
                    else if (prd.Interest_ID.Trim().Equals("PADRVR") || prd.Interest_ID.Trim().Equals("PADDR1"))
                    {
                        IsPADRVREnabled = true;
                    }
                    else if (prd.Interest_ID.Equals("PAPASS") || prd.Interest_ID.Equals("PA24AV"))
                    {
                        IsPAPASSEnabled = true;
                    }
                }
                IsACCESSEnabled = true;

                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        private void RefreshPremi()
        {
            BasicPremi = 0;
            SRCCPremi = 0;
            FLDPremi = 0;
            ETVPremi = 0;
            TSPremi = 0;
            TPLPremi = 0;
            PADRVRPremi = 0;
            PAPASSPremi = 0;
            ACCESSPremi = 0;
            LoadingPremi = 0;

            foreach (Otosales.Models.CalculatedPremi x in CalculatedPremiItems)
            {
                GrossPremi += Convert.ToDouble(x.GrossPremium);
                if (x.InterestID.TrimEnd().Equals("CASCO") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    BasicPremi += Convert.ToDouble(x.Premium);
                }
                else if (x.InterestID.TrimEnd().Equals("ACCESS") &&
                        (x.CoverageID.TrimEnd().Equals("ALLRIK") || x.CoverageID.TrimEnd().Equals("TLO")))
                {
                    ACCESSPremi += Convert.ToDouble(x.Net3);
                    if (ACCESSPremi > 0)
                    {
                        IsACCESSChecked = true;
                    }
                    IsACCESSSIEnabled = true;
                }
                else if (x.CoverageID.TrimEnd().Equals("SRCC") || x.CoverageID.TrimEnd().Equals("SRCTLO") || x.CoverageID.TrimEnd().Equals("SRCCTS"))
                {
                    SRCCPremi += Convert.ToDouble(x.Net3);
                    if (SRCCPremi > 0)
                    {
                        IsSRCCChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("FLD") || x.CoverageID.TrimEnd().Equals("FLDTLO"))
                {
                    FLDPremi += Convert.ToDouble(x.Net3);
                    if (FLDPremi > 0)
                    {
                        IsFLDChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("ETV") || x.CoverageID.TrimEnd().Equals("ETVTLO") || x.CoverageID.TrimEnd().Equals("EQK"))
                {
                    ETVPremi += Convert.ToDouble(x.Net3);
                    if (ETVPremi > 0)
                    {
                        IsETVChecked = true;
                    }
                }
                else if (x.CoverageID.TrimEnd().Equals("TRS") || x.CoverageID.TrimEnd().Equals("TRRTLO"))
                {
                    TSPremi += Convert.ToDouble(x.Net3);

                    if (TSPremi > 0)
                    {
                        IsTSChecked = true;
                    }
                    IsTSEnabled = true;
                    /** start 0040/URF/2017 */
                    //} else if (x.InterestID.equals("TPLPER") && (x.CoverageID.equals("MVTPL1"))) {
                }
                else if (x.InterestID.Equals("TPLPER") && (x.CoverageID.Contains("MVTP")))
                {
                    TPLPremi += Convert.ToDouble(x.Net3);

                    if (TPLPremi > 0 || x.Rate == -1)
                    {
                        IsTPLChecked = true;
                    }
                    IsTPLEnabled = true;
                    IsTPLSIEnabled = true;
                }
                else if (x.InterestID.Equals("PADRVR") || x.InterestID.Equals("PADDR1"))
                {
                    PADRVRPremi += Convert.ToDouble(x.Net3);
                    if (PADRVRPremi > 0)
                    {
                        IsPADRVRChecked = true;
                    }
                    IsPADRVRSIEnabled = true;
                }
                else if (x.InterestID.Equals("PAPASS") || x.InterestID.Equals("PA24AV"))
                {
                    PAPASSPremi += Convert.ToDouble(x.Net3);

                    if (PAPASSPremi > 0)
                    {
                        IsPADRVRChecked = true;
                        IsPAPASSChecked = true;
                    }
                    IsPAPASSEnabled = true;
                    IsPAPASSSIEnabled = true;
                    IsPASSEnabled = true;

                }

            }
        }
        private void CalculateLoading()
        {
            LoadingPremi = 0;
            foreach (Otosales.Models.CalculatedPremi item in CalculatedPremiItems)
                LoadingPremi += item.Loading;
        }
        private void CalculateTotalPremi()
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    TotalPremi = 0;
                    GrossPremi = 0;

                    IsTPLEnabled = false;
                    foreach (Otosales.Models.CalculatedPremi item in CalculatedPremiItems)
                    {
                        TotalPremi += Convert.ToDouble(item.Net3);
                        GrossPremi += Convert.ToDouble(item.GrossPremium);
                        //CalculatePremiPerCoverage(item.InterestID, item.CoverageID, Convert.ToDouble(item.Premium));

                        if (item.CoverageID.TrimEnd().Equals("ALLRIK"))
                        {
                            IsTPLEnabled = true;
                        }
                    }


                    string query = @"select top 1 Policy_Fee1 from prd_policy_fee where product_code=@0 AND Order_Type=1 AND Lower_limit<=@1  AND Status=1 order by Lower_Limit desc";
                    AdminFee = db.ExecuteScalar<double>(query, ProductCode, TotalPremi);

                    // 003 begin
                    if (TotalPremi != 0)
                    {
                        TotalPremi += AdminFee; //+ LoadingPremi;
                    }
                    // 003 end
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        #endregion
    }
}