﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Infrastructure;
using Otosales.Repository.v0063URF2020;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Otosales.Controllers.v0063URF2020
{
    public class DataReactController : OtosalesBaseController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();

        #region Properties
        IMobileRepository mobileRepository;
        IAABRepository aabRepository;

        public DataReactController() {
            mobileRepository = new MobileRepository();
            aabRepository = new AABRepository();
        }
        public DataReactController(IMobileRepository repository) {
            mobileRepository = repository;
        }
        public DataReactController(IAABRepository repository)
        {
            aabRepository = repository;
        }
        #endregion

        [HttpPost]
        public IHttpActionResult UpdateTaskDetail(Models.v0213URF2019.UpdateTaskDetailParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string CustID = param.CustID;
            string FollowUpNo = param.FollowUpNo;
            Models.vWeb2.PersonalData PersonalData = param.PersonalData;
            Models.vWeb2.CompanyData CompanyData = param.CompanyData;
            Models.vWeb2.VehicleData VehicleData = param.VehicleData;
            Models.vWeb2.PolicyAddress policyAddress = param.policyAddress;
            Models.vWeb2.SurveySchedule surveySchedule = param.surveySchedule;
            List<Models.vWeb2.ImageDataTaskDetail> imageData = param.imageData;
            Models.vWeb2.OrderSimulationModel OSData = param.OSData;
            Models.vWeb2.OrderSimulationMVModel OSMVData = param.OSMVData;
            Models.vWeb2.CalculatePremiModel calculatedPremiItems = param.calculatedPremiItems;
            string FollowUpStatus = param.FollowUpStatus;
            Models.vWeb2.Remarks Remarks = param.Remarks;
            #endregion

            if (string.IsNullOrEmpty(param.CustID) || string.IsNullOrEmpty(param.FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            string OrderNo = "";
            try
            {
                OrderNo = mobileRepository.UpdateTaskListDetail(CustID, FollowUpNo, PersonalData,
                    CompanyData, VehicleData, policyAddress, surveySchedule, imageData, OSData, OSMVData,
                    calculatedPremiItems, FollowUpStatus, Remarks);
                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", data = new { OrderNo, PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo() } });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }

        [HttpPost]
        public IHttpActionResult SaveProspect(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string State = form.Get("State");
                string PCData = form.Get("ProspectCustomer");
                string FUData = form.Get("FollowUp");
                string FUHData = form.Get("FollowUpHistory");
                string ENData = form.Get("EmailNotification ");
                string OSData = form.Get("OrderSimulation");
                string OSMVData = form.Get("OrderSimulationMV");
                // string OSIData = form.Get("OrderSimulationInterest");
                string calculatedPremiItems = form.Get("calculatedPremiItems");
                bool IsMvGodig = bool.Parse(form.Get("isMvGodig"));
                string GuidTempPenawaran = form.Get("GuidTempPenawaran");
                string PremiumModelData = form.Get("PremiumModel");

                string query = "";
                string CustIdEnd = "";
                string FollowUpNoEnd = "";
                string OrderNoEnd = "";


                Models.v0219URF2019.FollowUp FUEnd = null;
                Models.v0219URF2019.ProspectCustomer PCEnd = null;

                #region Prospect Tab
                if (State.Equals("PROSPECT") || State.Equals("PREMIUMSIMULATION"))
                {
                    string CustId = "";
                    string FollowUpNumber = "";
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? Repository.v0219URF2019.MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT UPDATE PROSPECT CUSTOMER

                        Models.v0219URF2019.ProspectCustomer PC = JsonConvert.DeserializeObject<Models.v0219URF2019.ProspectCustomer>(PCData);
                        string CustID = System.Guid.NewGuid().ToString();
                        string FollowUpNo = System.Guid.NewGuid().ToString();
                        PC.CustID = string.IsNullOrEmpty(PC.CustID) ? CustID : PC.CustID;
                        query = @";IF EXISTS(SELECT * FROM ProspectCustomer WHERE CustId=@0)
BEGIN
UPDATE ProspectCustomer SET
       [Name]=@1
      ,[Phone1]=@2
      ,[Phone2]=@3
      ,[Email1]=@4
      ,[Email2]=@5
      ,[CustIDAAB]=@6
      ,[SalesOfficerID]=@7
      ,[DealerCode]=@8
      ,[SalesDealer]=@9
      ,[BranchCode]=@10
      ,[isCompany]=@11
      ,[LastUpdatedTime]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCustomer ([CustID]
      ,[Name]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[CustIDAAB]
      ,[SalesOfficerID]
      ,[DealerCode]
      ,[SalesDealer]
      ,[BranchCode]
      ,[isCompany]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1)
END";
                        // PC.SalesOfficerID = PC.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", PC.SalesOfficerID) : PC.SalesOfficerID;

                        db.Execute(query, PC.CustID, PC.Name, PC.Phone1, PC.Phone2, PC.Email1, PC.Email2, PC.CustIDAAB, PC.SalesOfficerID, PC.DealerCode, PC.SalesDealer, PC.BranchCode, PC.isCompany);
                        #endregion

                        #region INSERT UPDATE FOLLOWUP
                        Models.v0219URF2019.FollowUp FU = JsonConvert.DeserializeObject<Models.v0219URF2019.FollowUp>(FUData);

                        FU.FollowUpNo = string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo;
                        FU.CustID = string.IsNullOrEmpty(FU.CustID) ? CustID : FU.CustID;
                        FU.LastSeqNo = db.ExecuteScalar<int>(";IF EXISTS(SELECT * FROM FollowUpHistory Where FollowUpNo=@0)BEGIN SELECT ISNULL(MAX(SeqNo),0) From FollowUpHistory WHERE FollowUpNo=@0 END ELSE BEGIN SELECT 0 END", string.IsNullOrEmpty(FU.FollowUpNo) ? FollowUpNo : FU.FollowUpNo);
                        query = @";IF EXISTS(SELECT * FROM FollowUp WHERE FollowUpNo=@0)
BEGIN
    UPDATE FollowUp SET [LastSeqNo]=@1
      ,[CustID]=@2
      ,[ProspectName]=@3
      ,[Phone1]=@4
      ,[Phone2]=@5
      ,[SalesOfficerID]=@6
      ,[EntryDate]=GETDATE()
      ,[FollowUpName]=@7
      ,[FollowUpStatus]=@8
      ,[FollowUpInfo]=@9
      ,[Remark]=@10
      ,[BranchCode]=@11
      ,[LastUpdatedTime]=GETDATE()
      ,[PolicyId]=@12
      ,[PolicyNo]=@13
      ,[TransactionNo]=@14
      ,[SalesAdminID]=@15
      ,[SendDocDate]=@16
      ,[IdentityCard]=@17
      ,[STNK]=@18
      ,[SPPAKB]=@19
      ,[BSTB1]=@20
      ,[BSTB2]=@21
      ,[BSTB3]=@22
      ,[BSTB4]=@23
      ,[CheckListSurvey1]=@24
      ,[CheckListSurvey2]=@25
      ,[CheckListSurvey3]=@26
      ,[CheckListSurvey4]=@27
      ,[PaymentReceipt1]=@28
      ,[PaymentReceipt2]=@29
      ,[PaymentReceipt3]=@30
      ,[PaymentReceipt4]=@31
      ,[PremiumCal1]=@32
      ,[PremiumCal2]=@33
      ,[PremiumCal3]=@34
      ,[PremiumCal4]=@35
      ,[FormA1]=@36
      ,[FormA2]=@37
      ,[FormA3]=@38
      ,[FormA4]=@39
      ,[FormB1]=@40
      ,[FormB2]=@41
      ,[FormB3]=@42
      ,[FormB4]=@43
      ,[FormC1]=@44
      ,[FormC2]=@45
      ,[FormC3]=@46
      ,[FormC4]=@47
      ,[FUBidStatus]=@48
      ,[BuktiBayar]=@49 WHERE [FollowUpNo]=@0
END
    ELSE
BEGIN
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[Remark]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[PolicyId]
      ,[PolicyNo]
      ,[TransactionNo]
      ,[SalesAdminID]
      ,[SendDocDate]
      ,[IdentityCard]
      ,[STNK]
      ,[SPPAKB]
      ,[BSTB1]
      ,[BSTB2]
      ,[BSTB3]
      ,[BSTB4]
      ,[CheckListSurvey1]
      ,[CheckListSurvey2]
      ,[CheckListSurvey3]
      ,[CheckListSurvey4]
      ,[PaymentReceipt1]
      ,[PaymentReceipt2]
      ,[PaymentReceipt3]
      ,[PaymentReceipt4]
      ,[PremiumCal1]
      ,[PremiumCal2]
      ,[PremiumCal3]
      ,[PremiumCal4]
      ,[FormA1]
      ,[FormA2]
      ,[FormA3]
      ,[FormA4]
      ,[FormB1]
      ,[FormB2]
      ,[FormB3]
      ,[FormB4]
      ,[FormC1]
      ,[FormC2]
      ,[FormC3]
      ,[FormC4]
      ,[FUBidStatus]
      ,[BuktiBayar]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38,@39,@40,@41,@42,@43,@44,@45,@46,@47,@48,@49)
END";
                        // FU.SalesOfficerID = FU.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", FU.SalesOfficerID) : FU.SalesOfficerID;

                        db.Execute(query, FU.FollowUpNo
     , FU.LastSeqNo
     , FU.CustID
     , FU.ProspectName
     , FU.Phone1
     , FU.Phone2
     , FU.SalesOfficerID
     , FU.FollowUpName
     , FU.FollowUpStatus
     , FU.FollowUpInfo
     , FU.Remark
     , FU.BranchCode
     , FU.PolicyId
     , FU.PolicyNo
     , FU.TransactionNo
     , FU.SalesAdminID
     , FU.SendDocDate
     , FU.IdentityCard
     , FU.STNK
     , FU.SPPAKB
     , FU.BSTB1
     , FU.BSTB2
     , FU.BSTB3
     , FU.BSTB4
     , FU.CheckListSurvey1
     , FU.CheckListSurvey2
     , FU.CheckListSurvey3
     , FU.CheckListSurvey4
     , FU.PaymentReceipt1
     , FU.PaymentReceipt2
     , FU.PaymentReceipt3
     , FU.PaymentReceipt4
     , FU.PremiumCal1
     , FU.PremiumCal2
     , FU.PremiumCal3
     , FU.PremiumCal4
     , FU.FormA1
     , FU.FormA2
     , FU.FormA3
     , FU.FormA4
     , FU.FormB1
     , FU.FormB2
     , FU.FormB3
     , FU.FormB4
     , FU.FormC1
     , FU.FormC2
     , FU.FormC3
     , FU.FormC4
     , FU.FUBidStatus
     , FU.BuktiBayar);
                        #endregion


                        if (PC.isCompany)
                        {
                            #region INSERT UPDATE COMPANY DATA
                            db.Execute(@";IF EXISTS(SELECT * FROM ProspectCompany WHERE CustId=@0)
BEGIN
UPDATE ProspectCompany SET
       [CompanyName]=@1
      ,[FollowUpNo]=@2
      ,[PICPhoneNo]=@3
      ,[Email]=@4
      ,[RowStatus]=1
      ,[ModifiedDate]=GETDATE() WHERE CustID=@0
END
    ELSE
BEGIN
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName]
      ,[FollowUpNo]
      ,[PICPhoneNo]
      ,[Email]
      ,[ModifiedDate]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,GETDATE(),1)
END", PC.CustID, PC.Name, FU.FollowUpNo, PC.Phone1, PC.Email1);
                            #endregion
                        }

                        PCEnd = PC;
                        FUEnd = FU;

                        #region SAVE IMAGE
                        query = @";
SELECT [ImageID]
      ,[FollowUpNo]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                        List<dynamic> ImageData = db.Fetch<dynamic>(query, FU.FollowUpNo);
                        foreach (dynamic id in ImageData)
                        {
                            query = @"IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,1,@6,@7)
END";
                            db.Execute(query, id.ImageID, id.DeviceID
      , id.SalesOfficerID
      , id.PathFile
      , id.EntryDate
      , id.LastUpdatedTime
      , id.Data
      , id.ThumbnailData);
                            db.Execute("UPDATE dbo.TempImageData SET RowStatus=0 WHERE FollowUpNo=@0 AND PathFile=@1", id.FollowUpNo, id.PathFile);
                        }
                        #endregion


                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "PROSPECT", FU.CustID, FU.FollowUpNo, "", GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, FU.FollowUpNo, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion


                    }
                    //return Json(new { status = true, message = "Success", CustId, FollowUpNumber });
                    CustIdEnd = FUEnd.CustID;
                    FollowUpNoEnd = FUEnd.FollowUpNo;

                }
                #endregion


                #region Vehicle Tab
                if (State.Equals("VEHICLE") || State.Equals("PREMIUMSIMULATION"))
                {
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? Repository.v0219URF2019.MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    string OrderNoReturn = null;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        #region INSERT ORDERSIMULATION
                        Models.v0219URF2019.OrderSimulation OS = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulation>(OSData);

                        FollowUpNoEnd = OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;

                        CustIdEnd = OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;

                        string OrderNo = System.Guid.NewGuid().ToString();
                        OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                        // 
                        query = @";Select OrderNo from OrderSimulation where FollowUpNo=@0 and CustId = @1 and ApplyF = 1";
                        string OrderNoCheck = db.FirstOrDefault<string>(query, FollowUpNoEnd, CustIdEnd);
                        OS.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OS.OrderNo : OrderNoCheck;
                        //
                        OrderNoReturn = OS.OrderNo;
                        OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                        OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                        OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PeriodFrom]
      ,[PeriodTo]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27)
END";
                        // OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                        db.Execute(query, OS.OrderNo
      , OS.CustID
      , OS.FollowUpNo
      , OS.QuotationNo
      , OS.MultiYearF
      , OS.YearCoverage
      , OS.TLOPeriod
      , OS.ComprePeriod
      , OS.BranchCode
      , OS.SalesOfficerID
      , OS.PhoneSales
      , OS.DealerCode
      , OS.SalesDealer
      , OS.ProductTypeCode
      , OS.AdminFee
      , OS.TotalPremium
      , OS.Phone1
      , OS.Phone2
      , OS.Email1
      , OS.Email2
      , OS.SendStatus
      , OS.InsuranceType
      , OS.ApplyF
      , OS.SendF
      , OS.LastInterestNo
      , OS.LastCoverageNo
      , OS.PeriodFrom
      , OS.PeriodTo);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION MV
                        Models.v0219URF2019.OrderSimulationMV OSMV = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulationMV>(OSMVData);
                        OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                        OSMV.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OSMV.OrderNo : OrderNoCheck;

                        query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                        db.Execute(query, OSMV.OrderNo
      , OSMV.ObjectNo
      , OSMV.ProductTypeCode
      , OSMV.VehicleCode
      , OSMV.BrandCode
      , OSMV.ModelCode
      , OSMV.Series
      , OSMV.Type
      , OSMV.Sitting
      , OSMV.Year
      , OSMV.CityCode
      , OSMV.UsageCode
      , OSMV.SumInsured
      , OSMV.AccessSI
      , OSMV.RegistrationNumber
      , OSMV.EngineNumber
      , OSMV.ChasisNumber
      //, OSMV.LastUpdatedTime
      //, OSMV.RowStatus
      , OSMV.IsNew);
                        #endregion

                        #region INSERT UPDATE ORDERSIMULATION SURVEY
                        query = @"IF NOT EXISTS(SELECT * FROM OrderSimulationSurvey WHERE OrderNo=@0)
BEGIN
INSERT INTO ordersimulationsurvey (OrderNo,CityCode,LocationCode,SurveyAddress,SurveyPostalCode,IsNeedSurvey,IsNSASkipSurvey,RowStatus,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate)
VALUES(@0,'','','','',@1,0,1,'OtosalesAPI',GETDATE(),NULL,NULL)
END
ELSE
BEGIN
UPDATE ordersimulationsurvey SET IsNeedSurvey=@1,ModifiedBy='OtosalesAPI',ModifiedDate=GETDATE() WHERE OrderNo=@0
END";
                        int isneedsurvey = OSMV.Year.Equals(DateTime.Now.Year.ToString()) ? 0 : 1;
                        db.Execute(query, OS.OrderNo, isneedsurvey);
                        #endregion

                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "VEHICLE", OS.CustID, OS.FollowUpNo, OS.OrderNo, GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, OS.FollowUpNo, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion
                    }

                    //return Json(new { status = true, message = "Success", OrderNo = OrderNoReturn });
                    OrderNoEnd = OrderNoReturn;
                }
                #endregion

                #region Cover Tab
                if (State.Equals("COVER") || State.Equals("PREMIUMSIMULATION"))
                {
                    GuidTempPenawaran = string.IsNullOrEmpty(GuidTempPenawaran) ? Repository.v0219URF2019.MobileRepository.GenerateUUIDTempPenawaran() : GuidTempPenawaran;

                    Models.v0219URF2019.OrderSimulation OS = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulation>(OSData);
                    Models.v0219URF2019.OrderSimulationMV OSMV = JsonConvert.DeserializeObject<Models.v0219URF2019.OrderSimulationMV>(OSMVData);
                    List<Models.v0219URF2019.OrderSimulationInterest> OSI = new List<Models.v0219URF2019.OrderSimulationInterest>();
                    List<Models.v0219URF2019.OrderSimulationCoverage> OSC = new List<Models.v0219URF2019.OrderSimulationCoverage>();
                    OS.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OS.OrderNo;
                    OSMV.OrderNo = State.Equals("PREMIUMSIMULATION") ? OrderNoEnd : OSMV.OrderNo;

                    bool isexistSFE = false;
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {

                        // 
                        query = @";Select OrderNo from OrderSimulation where FollowUpNo=@0 and CustId = @1 and ApplyF = 1";
                        string OrderNoCheck = db.FirstOrDefault<string>(query, FollowUpNoEnd, CustIdEnd);
                        OS.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OS.OrderNo : OrderNoCheck;
                        OSMV.OrderNo = string.IsNullOrEmpty(OrderNoCheck) ? OSMV.OrderNo : OrderNoCheck;
                        //

                        int a = 1;
                        int year = 1;
                        List<Models.v0219URF2019.CalculatedPremi> calculatedPremiItem = JsonConvert.DeserializeObject<List<Models.v0219URF2019.CalculatedPremi>>(calculatedPremiItems);

                        DateTime? pt = null;
                        foreach (Models.v0219URF2019.CalculatedPremi cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (Models.v0219URF2019.OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (Models.v0219URF2019.CalculatedPremi cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                        pt = cpitem.PeriodTo;
                                    }
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                        pf = cpitem.PeriodFrom;
                                    }
                                }
                                Models.v0219URF2019.OrderSimulationInterest osi = new Models.v0219URF2019.OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (Models.v0219URF2019.CalculatedPremi cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;

                        foreach (Models.v0219URF2019.OrderSimulationInterest item in OSI)
                        {
                            foreach (Models.v0219URF2019.CalculatedPremi item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    Models.v0219URF2019.OrderSimulationCoverage oscItem = new Models.v0219URF2019.OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }

                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert

                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);


                        int i = 1;
                        foreach (Models.v0219URF2019.OrderSimulationInterest data in OSI)
                        {
                            query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                            i++;
                        }

                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationInterest> TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationInterest data in TempAllOSI)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in OSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) && data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                        query = @"UPDATE OrderSimulationInterest SET 
                        //                                Premium=@5,
                        //                                LastUpdatedTime=GETDATE()
                        //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4 AND Deductible_Code=@6";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year, data2.Premium, data.DeductibleCode);
                        //                                    }
                        //                                }

                        //                                if (!isExist)
                        //                                {
                        //                                    //soft delete coverage

                        //                                    query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 ";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo);


                        //                                    //soft delete interest
                        //                                    query = @"DELETE OrderSimulationInterest " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion

                        #region DELETE UPDATE ORDERSIMULATION COVERAGE

                        #region Insert
                        //  OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        i = 1;
                        foreach (Models.v0219URF2019.OrderSimulationCoverage data in OSC)
                        {
                            query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                            i++;

                        }
                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationCoverage> TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationCoverage data in TempAllOSC)
                        //                            {

                        //                                OrderSimulationInterest osiItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2", data.OrderNo, data.ObjectNo, data.InterestNo);
                        //                                if (osiItem != null)
                        //                                {
                        //                                    bool isExist = false;
                        //                                    foreach (OrderSimulationCoverage item in OSC)
                        //                                    {
                        //                                        if (item.CoverageID.TrimEnd().Equals(data.CoverageID.TrimEnd()) && item.InterestNo.Equals(data.InterestNo) && item.CoverageNo.Equals(data.CoverageNo))
                        //                                        {
                        //                                            isExist = true;
                        //                                            query = @"UPDATE OrderSimulationCoverage SET 
                        //      [Rate]=@5
                        //      ,[SumInsured]=@6
                        //      ,[LoadingRate]=@7
                        //      ,[Loading]=@8
                        //      ,[Premium]=@9
                        //      ,[BeginDate]=@10
                        //      ,[EndDate]=@11
                        //      ,[LastUpdatedTime]=GETDATE()
                        //      ,[RowStatus]=1
                        //      ,[IsBundling]=@12
                        //      ,[Entry_Pct]=@13
                        //      ,[Ndays]=@14
                        //      ,[Excess_Rate]=@15
                        //      ,[Calc_Method]=@16
                        //      ,[Cover_Premium]=@17
                        //      ,[Gross_Premium]=@18
                        //      ,[Max_si]=@19
                        //      ,[Net1]=@20
                        //      ,[Net2]=@21
                        //      ,[Net3]=@22
                        //      ,[Deductible_Code]=@23 WHERE [OrderNo]=@0 AND [ObjectNo]=@1 AND [InterestNo]=@2 AND [CoverageNo] =@3 AND [CoverageID]=@4";
                        //                                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                        //                                            data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);

                        //                                        }
                        //                                    }
                        //                                    //delete data
                        //                                    if (!isExist)
                        //                                    {
                        //                                        //soft delete coverage

                        //                                        query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageId=@4";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID);


                        //                                    }
                        //                                }

                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems
                        //                            TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationCoverage item in OSC)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationCoverage data in TempAllOSC)
                        //                                {
                        //                                    if (data.InterestNo.Equals(item.InterestNo) &&
                        //                           data.CoverageNo.Equals(item.CoverageNo) && data.CoverageID.TrimEnd().Equals(item.CoverageID.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (isExist)
                        //                                {
                        //                                    OrderSimulationCoverage oscitem = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageID=@4", item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.CoverageID);
                        //                                    if (oscitem != null)
                        //                                    {
                        //                                        query = @"UPDATE OrderSimulationCoverage SET 
                        //                                RowStatus=1
                        //                                ,Rate=@4
                        //                                ,SumInsured=@5
                        //                                ,LoadingRate=@6
                        //                                ,Loading=@7
                        //                                ,Premium=@8
                        //                                ,IsBundling=@9     
                        //                                ,LastUpdatedTime=GETDATE()  
                        //                                ,[Entry_Pct]=@10
                        //                                ,[Ndays]=@11
                        //                                ,[Excess_Rate]=@12
                        //                                ,[Calc_Method]=@13
                        //                                ,[Cover_Premium]=@14
                        //                                ,[Gross_Premium]=@15
                        //                                ,[Max_si]=@16
                        //                                ,[Net1]=@17
                        //                                ,[Net2]=@18
                        //                                ,[Net3]=@19
                        //                                ,[Deductible_Code]=@20                      
                        //WHERE OrderNo=@0 AND ObjectNo = @1 AND InterestNo=@2 AND CoverageNo=@3";
                        //                                        db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.Rate, item.SumInsured, item.LoadingRate, item.LoadingRate, item.Premium, item.IsBundling ? 1 : 0,
                        //                                             item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
                        //                                    }
                        //                                }
                        //                                else
                        //                                {

                        //                                    int i = db.ExecuteScalar<int>("SELECT max(CoverageNo) FROM OrderSimulationCoverage WHERE OrderNo=@0 ", item.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[CoverageNo]
                        //      ,[CoverageID]
                        //      ,[Rate]
                        //      ,[SumInsured]
                        //      ,[LoadingRate]
                        //      ,[Loading]
                        //      ,[Premium]
                        //      ,[BeginDate]
                        //      ,[EndDate]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]
                        //      ,[IsBundling]    
                        //      ,[Entry_Pct]
                        //      ,[Ndays]
                        //      ,[Excess_Rate]
                        //      ,[Calc_Method]
                        //      ,[Cover_Premium]
                        //      ,[Gross_Premium]
                        //      ,[Max_si]
                        //      ,[Net1]
                        //      ,[Net2]
                        //      ,[Net3]
                        //      ,[Deductible_Code])) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                        //                                    db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, i, item.CoverageID, item.Rate, item.SumInsured, item.LoadingRate, item.Loading, item.Premium, item.BeginDate, item.EndDate, item.IsBundling ? 1 : 0,
                        //                                                      item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
                        //                                    i++;
                        //                                }


                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = OS.PeriodFrom ?? DateTime.Now;
                            DateTime end = OS.PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,PeriodFrom=@12
                        ,PeriodTo=@13
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo, OS.PeriodFrom, OS.PeriodTo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion

                        OrderNoEnd = string.IsNullOrEmpty(OrderNoEnd) ? OS.OrderNo : OrderNoEnd;
                        FollowUpNoEnd = string.IsNullOrEmpty(FollowUpNoEnd) ? OS.FollowUpNo : FollowUpNoEnd;
                        CustIdEnd = string.IsNullOrEmpty(CustIdEnd) ? OS.CustID : CustIdEnd;
                        if (!Repository.v0219URF2019.MobileRepository.IsAllowedIEP(OS.ProductCode, isexistSFE))
                        {
                            return Json(new { status = false, message = "Basic Cover Not Applied", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd });
                        }

                        #region ISMVGODIG
                        // FAY 0219-URF-2019
                        if (IsMvGodig)
                        {
                            query = @";EXEC usp_AddTempPenawaranMVGodigTabVehicle @0, @1, @2, @3, @4";
                            db.Execute(query, "COVER", CustIdEnd, FollowUpNoEnd, OS.OrderNo, GuidTempPenawaran);

                        }
                        else
                        {
                            query = @";EXEC [usp_RemoveMVGodigFromOtosales] @0, @1";
                            db.Execute(query, FollowUpNoEnd, GuidTempPenawaran);
                        }
                        // Ended FAY 0219-URF-2019
                        #endregion

                    }

                    //    return Json(new { status = true, message = "Success", OrderSimulation = OS, FollowUpNo = FollowUpNoEnd });
                }
                return Json(new { status = true, message = "Success", OrderNo = OrderNoEnd, CustId = CustIdEnd, FollowUpNo = FollowUpNoEnd, FU = FUEnd, PC = PCEnd, GuidTempPenawaran = GuidTempPenawaran });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }


        [HttpPost]
        public IHttpActionResult getBasicCover(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                List<Models.v0219URF2019.BasicCover> BasicCover = new List<Models.v0219URF2019.BasicCover>();

                //string Channel = form.Get("Channel").Trim();
                string vyear = form.Get("vyear").Trim();
                int year = string.IsNullOrEmpty(vyear) ? DateTime.Now.Year : Convert.ToInt32(vyear);
                bool isBasic = Convert.ToBoolean(form.Get("isBasic"));
                string isMVGodig = form.Get("isMvGodig");
                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                BasicCover = mobileRepository.GetBasicCover(year, isBasic, flagIsMvGodig);

                return Json(new { status = true, message = "OK", BasicCover }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        #region Sprint 6 Otosales - FAY
        public IHttpActionResult GenerateShortenLinkPaymentOtosales(FormDataCollection form)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                //string Channel = form.Get("Channel").Trim();
                string orderNo = form.Get("OrderNo").Trim();
                string typeCC = form.Get("TypeCC");

                if (string.IsNullOrEmpty(orderNo) || string.IsNullOrEmpty(typeCC))
                {
                    return BadRequest("Param Required");
                }

                dynamic data = mobileRepository.GenerateLinkPayment(orderNo, typeCC);

                return Json(new { status = true, message = "OK", data }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {

                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message, data = "null" });
            }
        }

        #endregion

        #region 0072URF2020
        [HttpPost]
        public IHttpActionResult GetEnableDisableSalesmanDealer(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            if (form == null) return BadRequest("Input Required");
            else
            {
                try
                {
                    string ProductCode = form.Get("ProductCode");
                    string BranchCode = form.Get("BranchCode");
                    if (string.IsNullOrEmpty(ProductCode) && string.IsNullOrEmpty(BranchCode))
                    {
                        return BadRequest("Input Required");
                    }

                    dynamic res = aabRepository.GetEnableDisableSalesmanDealer(ProductCode, BranchCode);
                    return Json(new { status = true, message = "success", IsDealerEnable = res.IsDealerEnable, IsSalesmanInfoEnable = res.IsSalesmanInfoEnable });
                }
                catch (Exception e)
                {
                    _log.Error("Function " + actionName + " Error : " + e.ToString());
                    return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString(), spesific = false });
                }
            }
        }

        [HttpPost]
        public IHttpActionResult getProductType(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string isMVGodig = form.Get("isMvGodig");
                string salesOfficerId = form.Get("salesOfficerId").Trim();

                bool flagIsMvGodig = false;
                if (!string.IsNullOrEmpty(isMVGodig))
                {
                    flagIsMvGodig = Boolean.Parse(isMVGodig);
                }

                var result = mobileRepository.GetProductType(salesOfficerId, flagIsMvGodig);

                return Json(new { status = true, message = "OK", data = result }, Util.jsonSerializerSetting());

            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskDetailDocument(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string CustID = form.Get("CustID");
            string FollowUpNo = form.Get("FollowUpNo");
            string typeDocument = form.Get("Type");
            #endregion
            dynamic result = null;

            if (string.IsNullOrEmpty(CustID) || string.IsNullOrEmpty(FollowUpNo) || string.IsNullOrEmpty(typeDocument))
            {
                return BadRequest("Bad Request!");
            }
            try
            {
                switch (typeDocument)
                {
                    case "PERSONAL":
                        result = mobileRepository.GetTaksListDetailDocumentPersonal(CustID, FollowUpNo);
                        break;
                    case "COMPANY":
                        result = mobileRepository.GetTaksListDetailDocumentCompany(CustID, FollowUpNo);
                        break;
                    case "PROSPECT":
                        result = mobileRepository.GetTaksListDetailDocumentAll(CustID, FollowUpNo);
                        break;
                    default:
                        return BadRequest("Bad Type!");
                }

                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }


        [HttpPost]
        public IHttpActionResult getTaskList(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                int currentPage = Convert.ToInt16(form.Get("currentPage"));
                int pageSize = Convert.ToInt16(form.Get("pageSize"));
                int currentPageLead = currentPage; // Convert.ToInt16(form.Get("currentPageLead"));
                int pageSizeLead = pageSize;// Convert.ToInt16(form.Get("pageSizeLead"));
                bool chIsSearchByChasNo = bool.Parse(form.Get("chIsSearchByChasNo").Trim());
                bool chIsSearchByEngNo = bool.Parse(form.Get("chIsSearchByEngNo").Trim());
                bool chIsSearchByPolicyNo = bool.Parse(form.Get("chIsSearchByPolicyNo").Trim());
                bool chIsSearchByProsName = bool.Parse(form.Get("chIsSearchByProsName").Trim());
                string salesofficerid = form.Get("salesofficerid").Trim();
                string search = form.Get("search").Trim();
                string indexOrderByItem = form.Get("indexOrderByItem").Trim();
                int indexOrderByTypeItem = Int32.Parse(form.Get("indexOrderByTypeItem").Trim());
                bool chFollowUpMonthlyChecked = bool.Parse(form.Get("chFollowUpMonthlyChecked").Trim());
                bool chFollowUpDateChecked = bool.Parse(form.Get("chFollowUpDateChecked").Trim());
                bool chNeedFUChecked = bool.Parse(form.Get("chNeedFUChecked").Trim());
                bool chPotentialChecked = bool.Parse(form.Get("chPotentialChecked").Trim());
                bool chCallLaterChecked = bool.Parse(form.Get("chCallLaterChecked").Trim());
                bool chCollectDocChecked = bool.Parse(form.Get("chCollectDocChecked").Trim());
                bool chNotDealChecked = bool.Parse(form.Get("chNotDealChecked").Trim());
                bool chSentToSAChecked = bool.Parse(form.Get("chSentToSAChecked").Trim());
                bool chBackToAOChecked = bool.Parse(form.Get("chBackToAOChecked").Trim());
                bool chPolicyCreatedChecked = bool.Parse(form.Get("chPolicyCreatedChecked").Trim());
                bool chOrderRejectedChecked = bool.Parse(form.Get("chOrderRejectedChecked").Trim());
                bool chNewChecked = bool.Parse(form.Get("chNewChecked").Trim());
                bool chRenewableChecked = bool.Parse(form.Get("chRenewableChecked").Trim());
                string dateFollowUp = form.Get("getFollowUpDate");
                int allowSearchOthers = Int32.Parse(form.Get("allowSearchOthers").Trim());

                DateTime getFollowUpDate = DateTime.Now;
                if (!string.IsNullOrEmpty(dateFollowUp))
                {
                    getFollowUpDate = Convert.ToDateTime(dateFollowUp);
                }

                if (salesofficerid == null || salesofficerid == "")
                {
                    return Json(new { status = false, message = "Parameter is null", data = "null" });
                }

                string query = @";
#dataTelesales

SELECT a.FollowUpNo, a.SalesOfficerID, os.CustID, os.OrderNo, os.PolicyNo
,CASE 
    WHEN os.OldPolicyNo IS NOT NULL THEN os.PeriodFrom
    ELSE NULL
END ExpiredDate
INTO #tempOSFU
FROM dbo.FollowUp a 
INNER JOIN dbo.OrderSimulation os WITH (NOLOCK)
ON os.FollowUpNo = a.FollowUpNo 
AND os.ApplyF = 1 AND os.RowStatus = 1 
WHERE #salesofficerid a.RowStatus = 1 AND #nextfollowupdate #followup

SELECT a.CustID, a.FollowUpNo, a.LastFollowUpDate, a.NextFollowUpDate, a.LastSeqNo, 
a.followupstatus, a.isrenewal, a.EntryDate, a.ReferenceNo, a.followupinfo,
a.RowStatus, a.salesofficerid, a.IsRenewalDigital, a.FollowUpTypeID, os.OrderNo,
os.PolicyNo, a.ProspectName, os.ExpiredDate
INTO #tempFU
FROM dbo.FollowUp a 
LEFT JOIN #tempOSFU os ON os.FollowUpNo = a.FollowUpNo AND os.CustID = a.CustID
WHERE #salesofficerid a.RowStatus = 1 AND #nextfollowupdate #followup

SELECT a.CustID, a.FollowUpNo, a.LastFollowUpDate, a.NextFollowUpDate, a.LastSeqNo, 
a.followupstatus, a.isrenewal, a.EntryDate, a.ReferenceNo, a.followupinfo, a.RowStatus, 
a.salesofficerid, a.IsRenewalDigital, a.FollowUpTypeID, a.ProspectName
, a.OrderNo, a.PolicyNo, a.ExpiredDate
, pcm.isCompany,pcm.Name, p.isHotProspect
#searchMVField
INTO #tempGetTaskList
FROM #tempFU a
#searchMVTable
INNER JOIN dbo.ProspectCustomer pcm WITH (NOLOCK) 
ON pcm.CustID = a.CustID  
#searchCust
LEFT JOIN Quotation.dbo.HistoryPenawaran hp WITH (NOLOCK)
ON hp.OrderNo = a.OrderNo 
LEFT JOIN Quotation.dbo.Penawaran p WITH (NOLOCK)
ON p.policyID = hp.PolicyID AND a.FollowUpStatus = 1 AND a.RowStatus = 1
#qwhere
#chasisno #engineno
#name
#policyno

SELECT IsRenewalNonRenNot, IsRenewalDigital, FollowUpTypeDes, FollowUpDesc, FollowUpReason, CustID, FollowUpNo, LastFollowUpDate, NextFollowUpDate, LastSeqNo, 
EntryDate, IsRenewal, isHotProspect, followupstatus, WAStatusDes, ReferenceNo, WAStatusID, PolicyNo, ExpiredDate, ProspectName, isCompany, Name
INTO #tempFUList
FROM (
SELECT 0 IsRenewalNonRenNot, 
IsRenewalDigital, 
mft.FollowUpTypeDes, 
b.description FollowUpDesc, 
c.description FollowUpReason,
a.CustID, 
a.FollowUpNo, 
a.LastFollowUpDate, 
a.NextFollowUpDate, 
a.LastSeqNo, 
a.EntryDate,
a.IsRenewal,
a.isHotProspect,
a.followupstatus,
mws.WAStatusDes,
a.ReferenceNo,
wm.WAStatusID,
a.PolicyNo, a.ExpiredDate, a.ProspectName, a.isCompany,a.Name
FROM #tempGetTaskList a
INNER JOIN followupstatus b WITH (NOLOCK) 
  ON a.followupstatus = b.statuscode AND b.RowStatus = 1 AND a.RowStatus = 1
LEFT JOIN followupstatusinfo c WITH (NOLOCK) 
 ON a.followupstatus = c.statuscode AND a.followupinfo = c.infocode AND c.RowStatus = 1 
 LEFT JOIN dbo.WAMessaging wm ON wm.OrderNo = a.OrderNo AND wm.ID = 
 (SELECT MAX(wm1.ID) FROM dbo.WAMessaging wm1 WHERE wm1.OrderNo = a.OrderNo AND wm1.WAStatusID IS NOT NULL) 
 LEFT JOIN dbo.MstWAStatus mws ON mws.WAStatusID = wm.WAStatusID
 LEFT JOIN dbo.MstFollowUpType mft ON mft.FollowUpTypeID = a.FollowUpTypeID
#datakotor
#quotationlead
) res
#isrenewal
ORDER  BY #orderby
OFFSET @5 ROWS
FETCH NEXT @6 ROWS ONLY

SELECT IsRenewalNonRenNot, 
CASE 
	WHEN IsRenewalDigital = 1 THEN 'Renewable Digital - ' + FollowUpTypeDes 
ELSE 
	CASE WHEN IsRenewal=1 THEN 'Renewal - ' + FollowUpTypeDes
ELSE 'New - ' + FollowUpTypeDes END 
END AS TaskStatus
, FollowUpDesc, FollowUpReason, ExpiredDate, CustID, FollowUpNo, LastFollowUpDate, NextFollowUpDate, LastSeqNo
,CASE 
    WHEN followupstatus = 13 
        OR isrenewal = 1 THEN PolicyNo
    ELSE '' 
END PolicyNo
,
CASE isCompany
	WHEN 1 THEN (SELECT TOP 1 CompanyName FROM dbo.ProspectCompany pc WHERE pc.FollowUpNo = ful.FollowUpNo AND pc.CustID = ful.CustID AND pc.RowStatus = 1)
	ELSE ProspectName 
END ProspectName
, EntryDate, IsRenewal, isHotProspect, followupstatus, WAStatusDes, ReferenceNo, WAStatusID
FROM #tempFUList ful

#qdropqlead
DROP TABLE #tempOSFU
DROP TABLE #tempFU
DROP TABLE #tempGetTaskList
DROP TABLE #tempFUList";
                string qsalesofficerid = "";
                string qdatakotor = "";
                string qWhere = "";
                qsalesofficerid = string.IsNullOrEmpty(search) ? " a.salesofficerid = @0 AND " : "";
                qWhere = string.IsNullOrEmpty(search) ? "" : "WHERE";
                qdatakotor = !string.IsNullOrEmpty(search) ? @"UNION ALL
SELECT 1                AS IsRenewalNonRenNot, 
0						AS IsRenewalDigital,
'MV'					FollowUpTypeDes, 
'Need FU'           AS FollowUpDesc, 
'Data baru diinput' AS FollowUpReason, 
''                  CustID, 
''                  FollowUpNo, 
NULL                AS LastFollowUpDate, 
NULL                AS NextFollowUpDate, 
0                   AS LastSeqNo, 
p.entrydt           AS EntryDate,
1                   AS IsRenewal,
NULL                isHotProspect,
1                   AS followupstatus,
NULL                AS WAStatusDes,
NULL                ReferenceNo,
NULL                WAStatusID, 
ERP.policy_no       AS PolicyNo, 
NULL					ExpiredDate, 
p.Name_On_Policy    AS ProspectName, 
0						isCompany,
''						[Name]
FROM [BeyondReport].[AAB].[dbo].[excluded_renewal_policy] ERP 
INNER JOIN [BeyondReport].[AAB].[dbo].policy p 
  ON p.policy_no = ERP.policy_no AND IsSendToSA=0
 AND p.Status = 'A' AND p.RENEWAL_STATUS = 0
 AND ((p.endorsement_no = 0 and p.ORDER_STATUS IN ('11','9')) or p.endorsement_no <> 0) #2name
     #2chasisno #2engineno #2policyno" : "";
                string qSalesOfficerIDQL = "";
                qSalesOfficerIDQL = string.IsNullOrEmpty(search) ? " ql.SalesOfficerID = @0 AND " : "";
                bool isTelesales = false;
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    isTelesales = db.ExecuteScalar<bool>(@";
DECLARE @@Count INT = 0
SELECT @@Count=COUNT(1) FROM dbo.SalesOfficer WHERE SalesOfficerID = @0 AND Role = 'TELESALES' AND RowStatus = 1
IF(@@Count>0)
BEGIN SELECT CAST(1 AS BIT) END
ELSE
BEGIN SELECT CAST(0 AS BIT) END", salesofficerid);
                }
                string qGetQuotationLead = "";
                string qGetDataLead = "";
                string qDropQLead = "";
                if (isTelesales && chNeedFUChecked) {
                    qGetDataLead = @"SELECT ReferenceNo 
INTO #tempFULead
FROM dbo.FollowUp WHERE SalesOfficerID = @0 AND ReferenceNo IS NOT NULL
SELECT * INTO #tempQLead
FROM dbo.QuotationLead WHERE SalesOfficerID = @0 AND RowStatus = 1";
                    qGetQuotationLead = @"UNION ALL 
SELECT 0              AS IsRenewalNonRenNot, 
0					  AS IsRenewalDigital, 
'MV GODIGITAL'			FollowUpTypeDes,
'Need FU'             AS FollowUpDesc, 
'Data baru diinput'   AS FollowUpReason,
''                    CustID, 
''                    FollowUpNo, 
NULL                  AS LastFollowUpDate, 
NULL                  AS NextFollowUpDate, 
0                     AS LastSeqNo, 
ql.CreatedDate        AS EntryDate,
0                     AS IsRenewal,
NULL                  AS isHotProspect,
1                     AS followupstatus,
NULL                  AS WAStatusDes,
ql.ReferenceNo        ReferenceNo,
NULL                  WAStatusID,
ql.PolicyNo	          AS PolicyNo, 
NULL					ExpiredDate,
ql.CustName           AS ProspectName, 
0						isCompany,
''						[Name]
FROM #tempQLead ql 
LEFT JOIN #tempFULead f ON f.ReferenceNo = ql.ReferenceNo
WHERE ql.RowStatus = 1 AND ql.SalesOfficerID = @0 AND f.ReferenceNo IS NULL
#3name #3chasisno #3engineno #3policyno";
                    qDropQLead = @"DROP TABLE #tempQLead
DROP TABLE #tempFULead";
                }
                string qnextfollowupdate = "";
                if (chFollowUpDateChecked)
                {
                    qnextfollowupdate = @" ( nextfollowupdate <= @2 OR nextfollowupdate IS NULL ) ";
                }
                else if (chFollowUpMonthlyChecked)
                {
                    qnextfollowupdate = @" ((MONTH(a.EntryDate) = MONTH(GETDATE()) 
                                            AND YEAR(a.EntryDate) = YEAR(GETDATE()))) ";
                }

                List<int> listStatus = new List<int>();
                List<int> listInfo = new List<int>();

                if (chNeedFUChecked)
                {
                    listStatus.Add(1);
                }
                if (chPotentialChecked)
                {
                    listStatus.Add(2);
                }
                if (chCallLaterChecked)
                {
                    listStatus.Add(3);
                }
                if (chCollectDocChecked)
                {
                    listInfo.Add(56);
                }
                if (chNotDealChecked)
                {
                    listStatus.Add(5);
                }
                if (chSentToSAChecked)
                {
                    listInfo.Add(61);
                }
                if (chPolicyCreatedChecked)
                {
                    //listInfo.Add(50);
                    //listInfo.Add(51);
                    //listInfo.Add(52);
                    //listInfo.Add(53);
                    //listInfo.Add(54);
                    //listInfo.Add(55);
                    listStatus.Add(13);
                }
                if (chBackToAOChecked)
                {
                    listInfo.Add(60);
                }
                if (chOrderRejectedChecked)
                {
                    listStatus.Add(14);
                }

                string qfollowup = "";
                qfollowup = @" AND ( followupstatus IN(@3) OR (followupinfo IN(@4) AND followupinfo IS NOT NULL AND followupinfo <> 0)) ";
                string qisrenewal = "";
                qisrenewal = @"WHERE {0} {1} ";
                string snew = "";
                string srenew = "";

                if (chNewChecked)
                {
                    snew = "isrenewal = 0 OR isrenewal IS NULL";
                }
                if (chRenewableChecked)
                {
                    if (chNewChecked)
                    {
                        srenew = "OR isrenewal = 1";
                    }
                    else
                    {
                        srenew = "isrenewal = 1";
                    }
                }
                if (!chNewChecked && !chRenewableChecked)
                {
                    snew = "isrenewal = 2";
                    srenew = "";
                }
                qisrenewal = string.Format(qisrenewal, snew, srenew);
                string qchasisno = "";
                string qengineno = "";
                string qchasisno2 = "";
                string qengineno2 = "";
                string qchasisno3 = "";
                string qengineno3 = "";
                string qpolicyno = "";
                string qpolicyno2 = "";
                string qpolicyno3 = "";
                string qname = "";
                string qname2 = "";
                string qname3 = "";
                string qsearchMVField = "";
                string qsearchMVTable = "";
                string qsearchCust = "";
                if (!string.IsNullOrEmpty(search))
                {
                    if (chIsSearchByChasNo)
                    {
                        qsearchMVField = ",omv.ChasisNumber, omv.EngineNumber";
                        qsearchMVTable = @"LEFT JOIN dbo.OrderSimulationMV omv WITH (NOLOCK)
 ON omv.OrderNo = a.OrderNo";
                        qchasisno = " omv.ChasisNumber = @1 ";
                        qchasisno2 = " AND ERP.Chasis_Number = @1 ";
                        qchasisno3 = " AND ql.ChasisNumber = @1 ";
                    }
                    else if (chIsSearchByEngNo)
                    {
                        qsearchMVField = ",omv.ChasisNumber, omv.EngineNumber";
                        qsearchMVTable = @"LEFT JOIN dbo.OrderSimulationMV omv WITH (NOLOCK)
 ON omv.OrderNo = a.OrderNo";
                        qengineno = " omv.EngineNumber = @1 ";
                        qengineno2 = " AND ERP.Engine_Number = @1 ";
                        qengineno3 = " AND ql.EngineNumber = @1 ";
                    }
                    else if (chIsSearchByPolicyNo)
                    {
                        qpolicyno = "a.PolicyNo = @1 ";
                        qpolicyno2 = " AND ERP.Policy_No = @1 ";
                        qpolicyno3 = " AND ql.PolicyNo = @1 ";
                    }
                    else if (chIsSearchByProsName)
                    {
                        //Flag form UI to Search Others ProsName - 0213/URF/2019
                        if (allowSearchOthers == 0) //
                        {
                            qsalesofficerid = " a.salesofficerid = @0 AND ";
                        }
                        qsearchCust = @"
LEFT JOIN dbo.ProspectCompany pcs WITH (NOLOCK)
ON pcs.CustID = a.CustID AND pcs.FollowUpNo = a.FollowUpNo";
                        search = '%' + search + '%';
                        qname = " (pcm.Name LIKE @1 OR pcs.CompanyName LIKE @1)";
                        qname2 = " WHERE Name_On_Policy LIKE @1 ";
                        qname3 = " AND ql.CustName LIKE @1 ";
                    }
                }

                string qorderby = "";
                if (indexOrderByItem.Equals("Status"))
                {
                    qorderby = @" isHotProspect DESC, IsRenewalNonRenNot ASC , ISNULL(isrenewal,0) ASC, ISNULL(NextFollowUpDate, '3000-12-12T23:59:00') ASC , (CASE WHEN isrenewal = 1 THEN followupstatus END) ASC, (CASE WHEN isrenewal = 1 AND followupstatus=1 THEN ExpiredDate END) ASC, (CASE WHEN (isrenewal = 0 OR isrenewal IS NULL AND nextfollowupdate is null) OR (isrenewal = 1 AND followupstatus!=1)  THEN CAST(EntryDate AS DATE) END) DESC, WAStatusID DESC";
                }
                if (indexOrderByItem.Equals("Name"))
                {
                    qorderby = "ProspectName";
                }
                if (indexOrderByItem.Equals("EntryDate"))
                {
                    qorderby = "EntryDate";
                }
                if (indexOrderByItem.Equals("LastFollowUpdate"))
                {
                    qorderby = "LastFollowUpdate";
                }
                if (indexOrderByItem.Equals("NextFollowUpdate"))
                {
                    qorderby = "NextFollowUpdate";
                }


                if (!indexOrderByItem.Equals("Status"))
                {
                    if (indexOrderByTypeItem == 0)
                    {
                        qorderby = string.Concat(qorderby, " ASC");
                    }
                    else
                    {
                        qorderby = string.Concat(qorderby, " DESC");
                    }
                }
                query = query.Replace("#qwhere", qWhere);
                query = query.Replace("#dataTelesales", qGetDataLead);
                query = query.Replace("#quotationlead", qGetQuotationLead);
                query = query.Replace("#qdropqlead", qDropQLead);
                query = query.Replace("#datakotor", qdatakotor);
                query = query.Replace("#salesofficerid", qsalesofficerid);
                query = query.Replace("#qlsalesofficerid", qSalesOfficerIDQL);
                query = query.Replace("#nextfollowupdate", qnextfollowupdate);
                query = query.Replace("#followup", qfollowup);
                query = query.Replace("#isrenewal", qisrenewal);
                query = query.Replace("#searchMVField", qsearchMVField);
                query = query.Replace("#searchMVTable", qsearchMVTable);
                query = query.Replace("#chasisno", qchasisno);
                query = query.Replace("#engineno", qengineno);
                query = query.Replace("#2chasisno", qchasisno2);
                query = query.Replace("#3chasisno", qchasisno3);
                query = query.Replace("#2engineno", qengineno2);
                query = query.Replace("#3engineno", qengineno3);
                query = query.Replace("#policyno", qpolicyno);
                query = query.Replace("#2policyno", qpolicyno2);
                query = query.Replace("#3policyno", qpolicyno3);
                query = query.Replace("#searchCust", qsearchCust);
                query = query.Replace("#name", qname);
                query = query.Replace("#2name", qname2);
                query = query.Replace("#3name", qname3);
                query = query.Replace("#orderby", qorderby);

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    dynamic result = new ExpandoObject();
                    //result = db.Page<dynamic>(currentPage, pageSize, query, salesofficerid, search, getFollowUpDate
                    //    , listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 });
                    int start = (currentPage - 1) * pageSize;
                    if (start != 0)
                    {
                        start++;
                    }
                    result.Items = db.Fetch<dynamic>(query, salesofficerid, search, getFollowUpDate
                        , listStatus.Count > 0 ? listStatus : new List<int>() { 0 }, listInfo.Count > 0 ? listInfo : new List<int>() { 0 }, start, pageSize);
                    result.CurrentPage = currentPage;

                    return Json(new { status = true, message = "OK", data = result, /*prospectLeadList = prospectLeadListPage, historyPenawaranList = historyPenawaranList, penawaranList = penawaranList*/ }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }

        [HttpPost]
        public IHttpActionResult getTaskDetail(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                #region Parameter
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                #endregion
                Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
                try
                {
                    result = mobileRepository.GetTaksListDetail(CustID, FollowUpNo);
                }
                catch (Exception e)
                {
                    _log.Error("Error at Function " + actionName + " :" + e.StackTrace);
                    throw e;
                }

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Success", data = result }, Util.jsonSerializerSetting());
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = "Unable to process your input please try again or contact administrator", data = e.ToString() });
            }
        }
        #endregion

    }
}
