﻿using a2is.Framework.Monitoring;
using Otosales.Infrastructure;
using Otosales.Models.v0232URF2019;
using Otosales.Repository.v0232URF2019;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Otosales.Controllers.v0232URF2019
{
    public class DataReactController : OtosalesBaseController
    {
        private static readonly a2isLogHelper _log = new a2isLogHelper();

        [HttpPost]
        public IHttpActionResult sendToSARenewalNonRenNot(SendToSARenewalNonRenNotParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            #region Parameter
            string PolicyNo = param.PolicyNo;
            string FollowUpNo = param.FollowUpNo;
            string RemarkToSA = param.RemarksToSA;
            string SalesOfficerID = param.SalesOfficerID;
            int isPTSB = Convert.ToInt32(param.isPTSB);
            #endregion

            //Generate GUID 
            string custID = System.Guid.NewGuid().ToString();
            string followUpID = String.IsNullOrEmpty(FollowUpNo) ? System.Guid.NewGuid().ToString() : FollowUpNo;
            string orderID = System.Guid.NewGuid().ToString();

            if (string.IsNullOrEmpty(PolicyNo) || string.IsNullOrEmpty(FollowUpNo))
                return Json(new { status = false, message = "Param can't be null" });
            try
            {
                #region Check Double Insured
                Otosales.Models.vWeb2.CheckDoubleInsuredResult dblins = Otosales.Repository.v0213URF2019.MobileRepository.GetCheckDoubleInsuredDataKotor(PolicyNo);
                if (dblins.IsDoubleInsured)
                {
                    return Json(new { status = false, message = "Double Insured.", data = dblins });
                }
                #endregion
                Otosales.Repository.v0213URF2019.MobileRepository.GenerateOrderSimulation(SalesOfficerID, custID, followUpID, orderID, PolicyNo, RemarkToSA, isPTSB);

                _log.Debug(actionName + " Function end.");
                return Json(new { status = true, message = "Update Success.", orderID });
            }
            catch (Exception e)
            {
                Otosales.Repository.v0213URF2019.MobileRepository.ClearFailedData(custID, followUpID, orderID);
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
        [HttpPost]
        public IHttpActionResult GetPaymentInfo(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string CustID = form.Get("CustID");
                string FollowUpNo = form.Get("FollowUpNo");
                if (string.IsNullOrEmpty(CustID) && string.IsNullOrEmpty(FollowUpNo))
                    return Json(new { status = false, message = "Param can't be null" });

                Otosales.Models.vWeb2.PaymentInfo res = MobileRepository.GetPaymentInfo(CustID, FollowUpNo);
                return Json(new { status = true, message = "Update Success.", data = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
        [HttpPost]
        public IHttpActionResult ReactiveVA(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string VANumber = form.Get("VANumber");
                string PolicyOrderNo = form.Get("PolicyOrderNo");
                if (string.IsNullOrEmpty(VANumber))
                    return Json(new { status = false, message = "Param can't be null" });

                bool res = MobileRepository.ReactiveVA(VANumber, PolicyOrderNo);
                return Json(new { status = res, message = "Reactive Success." });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
        [HttpPost]
        public IHttpActionResult CheckVAPayment(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            _log.Debug(actionName + " Function start : ");
            try
            {
                string OrderNo = form.Get("OrderNo");
                if (string.IsNullOrEmpty(OrderNo))
                    return Json(new { status = false, message = "Param can't be null" });

                bool res = MobileRepository.CheckVAPayment(OrderNo);
                return Json(new { status = true, message = "Success.", IsPaid = res });
            }
            catch (Exception e)
            {
                _log.Error("Function " + actionName + " Error : " + e.ToString());
                return Json(new { status = false, message = e.Message });
            }
        }
    }
}
