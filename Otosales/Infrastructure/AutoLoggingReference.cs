﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;
using log4net;
using Otosales.Utils;
using Otosales.Controllers.vWeb2;

namespace Otosales.Infrastructure
{
    public class AutoLoggingReference : ActionFilterAttribute
    {

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            string username = "";
            try
            {
                string token = actionContext.Request.Headers.Authorization.Parameter;
                var temp = MemoryCacheUtil.GetValue(token);
                if (temp == null)
                {
                    username = AuthenticationController.ValidateToken(token);
                    MemoryCacheUtil.Add(token, username);
                }
                else
                    username = temp.ToString();

                a2isUtil.SetLoggingReference(username);
            }
            catch (Exception)
            {

            }
            //a2isUtil.SetLoggingReference(Thread.CurrentPrincipal.Identity.Name);
            base.OnActionExecuting(actionContext);
        }
    }
}