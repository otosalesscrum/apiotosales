﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using a2is.Framework.Filters;
using Otosales.Filters;

namespace Otosales.Infrastructure
{
    [OtosalesExceptionFilter]
    [a2isLoggerFilter]
    [Authorize]
    public class OtosalesBaseController : ApiController
    {
    }
}
