﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;
using a2is.Framework.Monitoring;

namespace Otosales.Filters
{
    public class OtosalesExceptionFilter : ExceptionFilterAttribute
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnException(actionExecutedContext);
            logger.ErrorFormat("Error occured in {0} - {1}: {2}",
                actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName,
                actionExecutedContext.ActionContext.ActionDescriptor.ActionName,
                actionExecutedContext.Exception.StackTrace.ToString());
            
            actionExecutedContext.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent("error service | " + actionExecutedContext.Exception.GetBaseException().Message) };
        }
    }
}