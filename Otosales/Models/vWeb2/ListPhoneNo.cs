﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class PhoneNoModel
    {
        public string NoHp { get; set; }
        public string HPStatus { get; set; }
        public string Column_Name { get; set; }
        public int Priority { get; set; }
        public int RelationshipId { get; set; }
        public string PICName { get; set; }
    }
}