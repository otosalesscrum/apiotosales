﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class DashboardTotalValue
    {

        public string StatusCode { get; set; }
        public string Description { get; set; }
        public string TotalOrder { get; set; }
        public string TotalPremi { get; set; }
    }
}