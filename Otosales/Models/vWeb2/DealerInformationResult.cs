﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class BankInformationResult
    {
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string ErrorMessage { get; set; }
    }
}