﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class SendSMSModel
    {
        public string PhoneNo { get; set; }
        public string SMSText { get; set; }
    }
}