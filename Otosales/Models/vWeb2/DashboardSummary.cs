﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class DashboardSummary
    {
        public double TotalPremi { get; set; }
        public double NewTotalPremi { get; set; }
        public int NewTotalPolicy { get; set; }
        public double RenewTotalPremi { get; set; }
        public int RenewTotalPolicy { get; set; }
        public double EndorseTotalPremi { get; set; }
        public int EndorseTotalPolicy { get; set; }
        public double CancelTotalPremi { get; set; }
        public int CancelTotalPolicy { get; set; }
        public double RenewalRatio { get; set; } 
        public double DealTotalPremi { get; set; }
        public int DealTotalOrder { get; set; }
        public double PotentialTotalPremi { get; set; }
        public int PotentialTotalOrder { get; set; }
        public double CallLaterTotalPremi { get; set; }
        public int CallLaterTotalOrder { get; set; }
        public double NeedFUTotalPremi { get; set; }
        public int NeedFUTotalOrder { get; set; }
        public double NotDealTotalPremi { get; set; }
        public int NotDealTotalOrder { get; set; }
        public double RejectedTotalPremi { get; set; }
        public int RejectedTotalOrder { get; set; }
        public string DetailDashboardState { get; set; }
    }
}