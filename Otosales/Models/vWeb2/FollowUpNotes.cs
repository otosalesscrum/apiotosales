﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class FollowUpNotes
    {
        public int NotesCode { get; set; }
        public string Description { get; set; }
    }
}