﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class SurveyDocument
    {
        public string Survey_No { get; set; }
        public string Image_ID { get; set; }
        public string File_Name { get; set; }
        public string Doc_DESCRIPTION { get; set; }
        public string Document_Type { get; set; }
        public byte[] Image { get; set; }
    }
}