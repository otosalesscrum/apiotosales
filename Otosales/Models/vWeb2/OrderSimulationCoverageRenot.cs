﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class OrderSimulationCoverageRenot
    {
        public int ObjectNo { get; set; }
        public int InterestNo { get; set; }
        public int CoverageNo { get; set; }
        public string CoverageID { get; set; }
        public double Rate { get; set; }
        public double SumInsured { get; set; }
        public double LoadingRate { get; set; }
        public double Loading { get; set; }
        public double Premium { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public int IsBundling { get; set; }
        // bdrd 0089/urf/2019
        public decimal EntryPct { get; set; }
        public decimal NDays { get; set; }
        public decimal ExcessRate { get; set; }
        public decimal CalcMethod { get; set; }
        public decimal CoverPremium { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal MaxSi { get; set; }
        public decimal Net1 { get; set; }
        public decimal Net2 { get; set; }
        public decimal Net3 { get; set; }
        public string DeductibleCode { get; set; }
    }
}