﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class PremiumScheme
    {
        public decimal CoverPremium { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal NetPremium { get; set; }
        public decimal Net1 { get; set; }
        public decimal Net2 { get; set; }
        public decimal Net3 { get; set; }
    }
}