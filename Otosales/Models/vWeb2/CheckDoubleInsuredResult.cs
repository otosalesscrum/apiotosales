﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class CheckDoubleInsuredResult
    {
        public bool IsDoubleInsured { get; set; }
        public string OrderNo { get; set; }
        public string PolicyNo { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
    }
}