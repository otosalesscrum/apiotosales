﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class InsertUpdatePhoneNoParam
    {
        public List<PhoneNoParam> PhoneNoParam { get; set; }
        public string OrderNo { get; set; }
        public string SalesOfficerID { get; set; }
    }
    public class PhoneNoParam {
        public string PhoneNo { get; set; }
        public string ColumnName { get; set; }
        public string Relation { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsWrongNumber { get; set; }
        public bool IsLandline { get; set; }
        public bool IsConnected { get; set; }
        public string PICName { get; set; }
        public string CallRejectReasonId { get; set; }
    }
}