﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using A2isMessaging;

namespace Otosales.Models.vWeb2
{
    public class SendViaEmailParam
    {
        public string EmailTo { get; set; }
        public string EmailCC { get; set; }
        public string EmailBCC { get; set; }
        public string Body { get; set; }
        public string Subject { get; set; }
        public string OrderNo { get; set; }
        public List<A2isMessagingAttachment> ListAtt { get; set; }
        public bool IsAddAtt { get; set; }
    }
}