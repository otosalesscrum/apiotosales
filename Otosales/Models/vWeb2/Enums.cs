﻿
using System;

namespace Otosales.Models.vWeb2
{
        public static class ReferenceTypeImage
        {
            public const string KTPSURVEY = "KTP/KITAS";
            public const string KTP = "KTP";
            public const string SIM = "SIM";
            public const string KITAS = "KITAS";
            public const string NPWP = "NPWP";
            public const string SIUP = "IDC";
            public const string SPPAKB = "SPPAKB";
            public const string FAKTUR = "FAKTUR";
            public const string STNK = "STNK";
            public const string BSTB = "BSTB";
            public const string CHECKLISTSURVEY = "CLSV";
            public const string ADJUSTMENT = "ADJS";
            public const string NSA = "NSA";
            public const string DOCREP = "DOCREP";
            public const string CONFIRMATIONCUSTOMER = "CSCF";
            public const string DOCPENDUKUNG = "DOCPKG";
            public const string BUKTIBAYAR = "BKTBYR";
        }
    
    
}