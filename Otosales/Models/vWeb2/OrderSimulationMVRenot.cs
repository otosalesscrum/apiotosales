﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class OrderSimulationMVRenot
    {
        public int ObjectNo { get; set; }
        public string ProductTypeCode { get; set; }
        public string VehicleCode { get; set; }
        public string BrandCode { get; set; }
        public string ModelCode { get; set; }
        public string Series { get; set; }
        public string Type { get; set; }
        public string Sitting { get; set; }
        public string Year { get; set; }
        public string CityCode { get; set; }
        public string UsageCode { get; set; }
        public double SumInsured { get; set; }
        public double AccessSI { get; set; }
        public string RegistrationNumber { get; set; }
        public string EngineNumber { get; set; }
        public string ChasisNumber { get; set; }
        public int IsNew { get; set; }
        public string Color { get; set; }
    }
}