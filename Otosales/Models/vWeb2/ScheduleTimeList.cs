﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class ScheduleTimeList
    {
        public string SurveyTime { get; set; }
        public int IsBooked { get; set; }
        public int ScheduleTimeID { get; set; }
    }
}