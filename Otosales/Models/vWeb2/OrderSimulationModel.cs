﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class OrderSimulationModel
    {
        public string OrderNo { get; set; }
        public string CustID { get; set; }
        public string FollowUpNo { get; set; }
        public string QuotationNo { get; set; }
        public bool MultiYearF { get; set; }
        public int YearCoverage { get; set; }
        public int TLOPeriod { get; set; }
        public int ComprePeriod { get; set; }
        public string BranchCode { get; set; }
        public string SalesOfficerID { get; set; }
        public string PhoneSales { get; set; }
        public Int64 DealerCode { get; set; }
        public Int64 SalesDealer { get; set; }
        public string ProductTypeCode { get; set; }
        public string ProductCode { get; set; }
        public decimal AdminFee { get; set; }
        public decimal TotalPremium { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public int SendStatus { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int InsuranceType { get; set; }
        public bool ApplyF { get; set; }
        public bool SendF { get; set; }
        public decimal LastInterestNo { get; set; }
        public decimal LastCoverageNo { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }
        public string PolicySentTo { get; set; }
        public string PolicyDeliveryName { get; set; }
        public string PolicyOrderNo { get; set; }
        public string PolicyDeliveryAddress { get; set; }
        public string PolicyDeliveryPostalCode { get; set; }
        public string VANumber { get; set; }
        public string SurveyNo { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public string PolicyNo { get; set; }
        public decimal AmountRep { get; set; }
        public string PolicyID { get; set; }
        public string SegmentCode { get; set; }
    }
}