﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class OrderSimulationMVModel
    {
        public string OrderNo { get; set; }
        public decimal ObjectNo { get; set; }
        public string ProductTypeCode { get; set; }
        public string VehicleCode { get; set; }
        public string BrandCode { get; set; }
        public string ModelCode { get; set; }
        public string Series { get; set; }
        public string Type { get; set; }
        public decimal Sitting { get; set; }
        public string Year { get; set; }
        public string CityCode { get; set; }
        public string UsageCode { get; set; }
        public decimal SumInsured { get; set; }
        public decimal AccessSI { get; set; }
        public string RegistrationNumber { get; set; }
        public string EngineNumber { get; set; }
        public string ChasisNumber { get; set; }
        public string searchVehicle { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public bool IsNew { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }
    }
}