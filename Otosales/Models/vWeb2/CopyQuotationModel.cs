﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class CopyQuotationModel
    {
        public OrderSimulationModel orderSimulation { get; set; }
        public OrderSimulationMVModel orderSimulationMV { get; set; }
        public List<OrderSimulationInterest> orderSimulationInterest { get; set; }
        public List<OrderSimulationCoverage> orderSimulationCoverage { get; set; }
    }

}