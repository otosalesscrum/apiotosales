﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class DashboardDetail
    {
        public string ID { get; set; }
        public string Label { get; set; }
        public double Value { get; set; }
        public string StateDashboard { get; set; }
    }
}