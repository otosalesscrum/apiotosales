﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class CalculatePremiModel {
        public List<CalculatedPremiItems> ListCalculatePremi { get; set; }
        public OSData OSData { get; set; }
        public OSMVData OSMVData { get; set; }
    }
    public class CalculatedPremiItems
    {
        public string InterestID { get; set; }
        public string CoverageID { get; set; }
        public int Year { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public double Rate { get; set; }
        public decimal ExcessRate { get; set; }
        public double Premium { get; set; }
        public decimal CoverPremium { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal MaxSI { get; set; }
        public string DeductibleCode { get; set; }
        public decimal EntryPrecentage { get; set; }
        public decimal Ndays { get; set; }
        public decimal Net1 { get; set; }
        public decimal Net2 { get; set; }
        public decimal Net3 { get; set; }
        public double SumInsured { get; set; }
        public double Loading { get; set; }
        public double LoadingRate { get; set; }
        public bool IsBundling { get; set; }
        public string AutoApply { get; set; }
        public int CalcMethod { get; set; }
    }
    public class OSData {
        public string OrderNo { get; set; }
        public string ProductTypeCode { get; set; }
        public int InsuranceType { get; set; }
        public string ProductCode { get; set; }
        public decimal AdminFee { get; set; }
        public bool MultiYearF { get; set; }
        public int ComprePeriod { get; set; }
        public int TLOPeriod { get; set; }
        public decimal TotalPremium { get; set; }
        public decimal LastInterestNo { get; set; }
        public decimal LastCoverageNo { get; set; }
        public int YearCoverage { get; set; }
    }
    public class OSMVData {
        public string OrderNo { get; set; }
        public string ProductTypeCode { get; set; }
        public decimal SumInsured { get; set; }
        public decimal AccessSI { get; set; }
    }
}