﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.vWeb2
{
    public class TaskListDetail
    {
        public FieldPremi FieldPremi { get; set; }
        public PersonalData PersonalData { get; set; }
        public List<dynamic> PersonalDocument { get; set; }
        public CompanyData CompanyData { get; set; }
        public List<dynamic> CompanyDocument { get; set; }
        public VehicleData VehicleData { get; set; }
        public PaymentInfo PaymentInfo { get; set; }
        public PolicyAddress PolicyAddres { get; set; }
        public SurveySchedule SurveySchedule { get; set; }
        public SurveyData SurveyData { get; set; }
        public List<dynamic> Document { get; set; }
        public List<ImageDataTaskDetail> ImageDate { get; set; }
        public List<dynamic> FollowUpInfo { get; set; }
        public Remarks Remaks { get; set; } 
    }

    public class FieldPremi
    {
        public decimal GrossPremium { get; set; }
        public decimal Admin { get; set; }
        public decimal NetPremi { get; set; }
        public string PolicyNo { get; set; }
        public string OrderNo { get; set; }
        public string PolicyOrderNo { get; set; }
        public string SurveyNo { get; set; }
        public decimal NoClaimBonus { get; set; }
        public string OldPolicyNo { get; set; }
        public DateTime? OldPolicyPeriodTo { get; set; }
    }

    public class PersonalData
    {
        public string Name { get; set; }
        public DateTime? CustBirthDay { get; set; }
        public string CustGender { get; set; }
        public string CustAddress { get; set; }
        public string PostalCode { get; set; }
        public string Email1 { get; set; }
        public string Phone1 { get; set; }
        public string IdentityNo { get; set; }
        public bool isCompany { get; set; }
        public string SalesOfficerID { get; set; }
        public bool isNeedDocRep { get; set; }
        public decimal AmountRep { get; set; }
        //0223URF2019
        public bool? IsPayerCompany { get; set; }
    }

    //public class PersonalDocument
    //{
    //    public byte[] IdentityCard { get; set; }
    //    public byte[] STNK { get; set; }
    //    public byte[] BSTB { get; set; }
    //    public byte[] DOCREP { get; set; }
    //    public byte[] KonfirmasiCust { get; set; }
    //}
    public class CompanyData 
    {
        public string CompanyName { get; set; }
        public string NPWPno { get; set; }
        public DateTime? NPWPdate { get; set; }
        public string NPWPaddres { get; set; }
        public string OfficeAddress { get; set; }
        public string PostalCode { get; set; }
        public string PICPhoneNo { get; set; }
        public string PICname { get; set; }
        public string PICEmail { get; set; }
    }

    //public class CompanyDocument {
    //    public byte[] NPWP { get; set; }
    //    public byte[] SIUP { get; set; }
    //}

    public class VehicleData
    {
        public string Vehicle { get; set; }
        public string ProductTypeCode { get; set; }
        public string VehicleCode { get; set; }
        public string BrandCode { get; set; }
        public string ModelCode { get; set; }
        public string Series { get; set; }
        public string Type { get; set; }
        public decimal Sitting { get; set; }
        public string Year { get; set; }
        public string CityCode { get; set; }
        public int? InsuranceType { get; set; }
        public decimal? SumInsured { get; set; }
        public string UsageCode { get; set; }
        public decimal? AccessSI { get; set; }
        public string RegistrationNumber { get; set; }
        public string EngineNumber { get; set; }
        public string ChasisNumber { get; set; }
        public bool? IsNew { get; set; }
        public string VANumber { get; set; }
        public long? DealerCode { get; set; }
        public long? SalesDealer { get; set; }
        public string OrderNo { get; set; }
        public int BasicCoverID { get; set; }
        public string ProductCode { get; set; }
        public string PolicyNo { get; set; }
        public string SegmentCode { get; set; }
        public string ColorOnBPKB { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public bool? IsORDefectsRepair { get; set; }
        public string ORDefectsDesc { get; set; }
        public bool? IsAccessoriesChange { get; set; }
        public bool? IsPolicyIssuedBeforePaying { get; set; }
        public string Remarks { get; set; }
        public string DocPendukung { get; set; }
        public byte[] DocPendukungData { get; set; }
        public string NoCover { get; set; }
        public string Accessories { get; set; }
        public string MouID { get; set; }
    }

    public class PaymentInfo {
        public DateTime? DueDate { get; set; }
        public string VAPermata { get; set; }
        public string VAMandiri { get; set; }
        public string VABCA { get; set; }
        public string BuktiBayar { get; set; }
        public byte[] BuktiBayarData { get; set; }
        public string BuktiBayar2 { get; set; }
        public byte[] BuktiBayar2Data { get; set; }
        public string BuktiBayar3 { get; set; }
        public byte[] BuktiBayar3Data { get; set; }
        public string BuktiBayar4 { get; set; }
        public byte[] BuktiBayar4Data { get; set; }
        public string BuktiBayar5 { get; set; }
        public byte[] BuktiBayar5Data { get; set; }
        public string RekeningPenampung { get; set; }
        //0223URF2019
        public dynamic IsVAActive { get; set; }
        public string VANo { get; set; }
        //0001URF2020
        public bool IsGenerateVA { get; set; }
        public bool IsReGenerateVA { get; set; }
    }

    public class PolicyAddress{
        public string SentTo { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
    }

    public class SurveySchedule{
        public string CityCode { get; set; }
        public string LocationCode { get; set; }
        public string SurveyAddress { get; set; }
        public string SurveyPostalCode { get; set; }
        public DateTime? SurveyDate { get; set; }
        public int? ScheduleTimeID { get; set; }
        public bool? IsNeedSurvey { get; set; }
        public bool? IsNSASkipSurvey { get; set; }
        public string ScheduleTimeDesc { get; set; }
        //0223URF2019
        public bool? IsManualSurvey { get; set; }
    }

    public class SurveyData
    {
        public string NoCover { get; set; }
        public string OriginalDefect { get; set; }
        public string SurveyStatus { get; set; }
        public string SurveyorRecomendation { get; set; }
        public string Remaks { get; set; }
        public string Surveyor { get; set; }
        public string Accessories { get; set; }
    }

    public class ImageDataTaskDetail {
        public string ImageType { get; set; }
        public string PathFile { get; set; }
    }

    public class Remarks {
        public string RemarkToSA { get; set; }
        public string RemarkFromSA { get; set; }
    }
}