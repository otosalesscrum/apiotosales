﻿namespace Otosales.Models.Xoom
{
    public class GeneralResult
    {
        public bool isSuccess = false;
        public int errorCode = 0;
        public string errorMessage = "unknown";
        public object result;
    }
}