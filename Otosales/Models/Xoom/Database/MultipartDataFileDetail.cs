﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.Xoom.Database
{
    public class MultipartDataFileDetail
    {
        public string ID { get; set; }
        public int PartNumber { get; set; }
        public string Data { get; set; }
        public DateTime CreateDate { get; set; }
    }
}