﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.Xoom.Database
{
    public class MultipartDataFileHeader
    {
        public string ID { get; set; }
        public string UserFileID { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public int PartCount { get; set; }
        public string OSSender { get; set; }
        public DateTime CreateDate { get; set; }
        public string AdditionalInfo { get; set; }
    }
}