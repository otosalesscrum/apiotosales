﻿using System.Runtime.Serialization;

namespace Otosales.Models.Xoom.Contract
{
    [DataContract]
    public class CheckInternet
    {
        [DataMember]
        public string XOOM { get; set; }
    }
}