﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Otosales.Models.Xoom.Contract
{
    [DataContract]
    public class FileUploadRegistrationModel
    {
        [DataMember]
        public string UserFileID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Extension { get; set; }

        [DataMember]
        public int PartCount { get; set; }

        [DataMember]
        public string OSSender { get; set; }

        [DataMember]
        public string AdditionalInfo { get; set; }
    }
}