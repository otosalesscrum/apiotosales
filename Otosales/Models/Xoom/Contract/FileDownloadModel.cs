﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Otosales.Models.Xoom.Contract
{
    [DataContract]
    public class FileDownloadModel
    {
        [DataMember]
        public string UserFileID { get; set; }

        [DataMember]
        public int PartLength { get; set; }

        [DataMember]
        public int PartRequest { get; set; }
    }
}