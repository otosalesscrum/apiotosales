﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Otosales.Models.Xoom.Contract
{
    [DataContract]
    public class FileUploadModel
    {
        [DataMember]
        public string ID { get; set; }

        [DataMember]
        public int PartNumber { get; set; }

        [DataMember]
        public string Data { get; set; }
    }
}