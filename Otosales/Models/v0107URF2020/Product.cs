﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0107URF2020
{
    public class Product
    {
        public string MouID { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ProductTypeCode { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int InsuranceType { get; set; }
        public int Ndays { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

    }
}