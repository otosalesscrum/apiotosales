﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0107URF2020
{
    public class RptPrintQuotationModel
    {
        public string OrderNo { get; set; }
        public double AdminFee { get; set; }
        public double PADRVRTSI { get; set; }
        public double PAPASSTSI { get; set; }
        public double TPLTSI { get; set; }
        public int InsuranceType { get; set; }
        public string QuotationNo { get; set; }
        public int YearCoverage { get; set; }
        public string InsuredAddress  { get; set; }
        public string CompanyAddress  { get; set; }
        public string PolicyNo { get; set; }
        public bool IsCompany { get; set; }
        public string CompanyName { get; set; }
        public string InsuredName { get; set; }
        public string PICName { get; set; }
        public string InsuredPhone { get; set; }
        public string PICPhoneNo { get; set; }
        public string InsuredEmail { get; set; }
        public string PICEmail { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public string SalesOfficerID { get; set; }
        public string BranchCode { get; set; }
        public string SalesmanName  { get; set; }
        public string SalesmanPhone { get; set; }
        public string SalesmanEmail { get; set; }
        public string SalesmanRole { get; set; }
        public string InsuredObject { get; set; }
        public string VehicleYear { get; set; }
        public int PassSeat { get; set; }
        public string ChasisNumber { get; set; }
        public string EngineNumber { get; set; }
        public string Usage { get; set; }
        public string RegistrationNumber { get; set; }
        public string GeoArea { get; set; }
        public string VAPermata { get; set; }
        public string VAMandiri { get; set; }
        public string VABCA { get; set; }
        public string ProductType { get; set; }
        public string City { get; set; }
        public string AccountNo { get; set; }
        public string BankDescription { get; set; }
        public string BankName { get; set; }
        public string NoCover { get; set; }
        public string OriginalDefect { get; set; }
        public string NonStandardAcc { get; set; }
        public double DiscountPct { get; set; }
    }
}