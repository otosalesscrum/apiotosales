﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0232URF2019
{
    public class SendToSARenewalNonRenNotParam
    {
        public string PolicyNo { get; set; }
        public string FollowUpNo { get; set; }
        public string RemarksToSA { get; set; }
        public string SalesOfficerID { get; set; }
        public int isPTSB { get; set; }

    }
}