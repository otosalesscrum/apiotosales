﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0232URF2019
{
    public class PreBookVAModel
    {
        public string VirtualAccountNumber { get; set; }
        public string PolicyType { get; set; }
        public string PolicyNumber { get; set; }
        public DateTime? PolicyPeriodFrom { get; set; }
        public DateTime? PolicyPeriodTo { get; set; }
        public string AccountManager { get; set; }
        public string AccountManagerName { get; set; }
        public string SAAdmin { get; set; }
        public string SAAdminName { get; set; }
        public string PICMobileNumber { get; set; }
        public string CustomerName { get; set; }
        public string IDType { get; set; }
        public string IDNumber { get; set; }
        public string Remarks { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public bool IsNew { get; set; }
        public string CreatedBy { get; set; }
    }
}