﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0219URF2019
{
    public class DownloadQuotationResult
    {
        public string FileName { get; set; }
        public byte[] Bytes { get; set; }
    }
}