﻿using A2isMessaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0219URF2019
{
    public class SendViaWAParam
    {
        public string OrderNo { get; set; }
        public string PhoneNo { get; set; }
        public string WAMessage { get; set; }
        public List<A2isMessagingAttachment> ListAtt { get; set; }
        public bool IsAddAtt { get; set; }
    }
}