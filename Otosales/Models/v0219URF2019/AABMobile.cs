﻿using System;
using System.Collections.Generic;

namespace Otosales.Models.v0219URF2019
{
    public class SalesOfficer
    {
        public SalesOfficer()
        {
            Key = new Dictionary<string, string>()
            {

            };
        }

        public string SalesOfficerID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string Class { get; set; }
        public string Password { get; set; }
        public string Validation { get; set; }
        public DateTime? Expiry { get; set; }
        public DateTime? Password_Expiry { get; set; }
        public string Department { get; set; }
        public string BranchCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string OfficePhone { get; set; }
        public string Ext { get; set; }
        public string Fax { get; set; }
        public string ExtUserID { get; set; }
        // 0261/URF/2015
        public string Channel { get; set; }
        public string ChannelSource { get; set; }
        // 0089/URF/2019
        public bool IsCSO { get; set; }
        // end
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool? RowStatus { get; set; }

        public Dictionary<string, string> Key { get; set; }
    }

    public class BodySync
    {
        public string Data { get; set; }
        public string Fiter { get; set; }
    }

    #region AABMobile

    public class Accessories
    {
        public string AccessoriesCode { get; set; }
        public string Description { get; set; }
        public decimal MaxSI { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "AccessoriesCode" };
        }
    }

    public class Dealer
    {
        public Int64 DealerCode { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "DealerCode" };
        }
    }

    public class SalesmanDealer
    {
        public Int64 SalesmanCode { get; set; }
        public string Description { get; set; }
        public Int64 DealerCode { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public Int64 BeyondCustomerID { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "SalesmanCode" };
        }
    }

    public class Region
    {
        public string RegionCode { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "RegionCode" };
        }
    }

    public class ProspectCustomer
    {
        public string CustID { get; set; }
        public string Name { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string CustIDAAB { get; set; }
        public string SalesOfficerID { get; set; }
        public Int64? DealerCode { get; set; }
        public Int64? SalesDealer { get; set; }
        public string BranchCode { get; set; }
        public bool isCompany { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "CustID" };
        }
    }

    public class ProspectCompany
    {
        public string CustID { get; set; }
        public string FollowUpNo { get; set; }
        public string CompanyName { get; set; }
        public string NpwpNo { get; set; }
        public DateTime? NpwpDate { get; set; }
        public string NpwpAddress { get; set; }
        public string OfficeAddress { get; set; }
        public string PostalCode { get; set; }
        public string PICPhoneNo { get; set; }
        public string PICName { get; set; }
        public string Email { get; set; }
        public string NPWP { get; set; }
        public string SIUP { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "CustID" };
        }
    }

    public class ProductType
    {
        public string ProductTypeCode { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int? InsuranceType { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "ProductTypeCode", "InsuranceType" };
        }
    }

    public class Product
    {
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string ProductTypeCode { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int InsuranceType { get; set; }
        //ADD Ndays for BRD 0089/URF/2019
        public int Ndays { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "ProductCode" };
        }
    }

    public class VehicleType
    {
        public string VehicleTypeCode { get; set; }
        public string ProductTypeCode { get; set; }
        public string CategoryCode { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "VehicleTypeCode" };
        }
    }

    public class VehicleBrand
    {
        public string BrandCode { get; set; }
        public string ProductTypeCode { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "BrandCode", "ProductTypeCode" };
        }
    }

    public class VehicleModel
    {
        public string BrandCode { get; set; }
        public string ProductTypeCode { get; set; }
        public string ModelCode { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "BrandCode", "ProductTypeCode", "ModelCode" };
        }
    }

    public class VehicleSeries
    {
        public string BrandCode { get; set; }
        public string ProductTypeCode { get; set; }
        public string ModelCode { get; set; }
        public string Series { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "BrandCode", "ProductTypeCode", "ModelCode", "Series" };
        }
    }

    public class Vehicle
    {
        public string VehicleDescription { get; set; }
        public string VehicleCode { get; set; }
        public string BrandCode { get; set; }
        public string ProductTypeCode { get; set; }
        public string ModelCode { get; set; }
        public string Series { get; set; }
        public string Type { get; set; }
        public decimal Sitting { get; set; }
        public string Year { get; set; }
        public string CityCode { get; set; }
        public decimal Price { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "VehicleCode", "BrandCode", "ProductTypeCode", "ModelCode", "Series", "Year", "CityCode" };
        }
    }

    public class FollowUpStatus
    {
        public int? StatusCode { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int? SeqNo { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "StatusCode" };
        }
    }

    public class FollowUpStatusInfo
    {
        public int InfoCode { get; set; }
        public int StatusCode { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int SeqNo { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "InfoCode" };
        }
    }

    public class FollowUp
    {
        public string FollowUpNo { get; set; }
        public int LastSeqNo { get; set; }
        public string CustID { get; set; }
        public string ProspectName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string SalesOfficerID { get; set; }
        public DateTime? EntryDate { get; set; }
        public string FollowUpName { get; set; }
        public DateTime? NextFollowUpDate { get; set; }
        public DateTime? LastFollowUpDate { get; set; }
        public int FollowUpStatus { get; set; }
        public int FollowUpInfo { get; set; }
        public string Remark { get; set; }
        public string BranchCode { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string PolicyId { get; set; }
        public string PolicyNo { get; set; }
        public string TransactionNo { get; set; }
        public string SalesAdminID { get; set; }
        public DateTime? SendDocDate { get; set; }
        public string IdentityCard { get; set; }
        public string STNK { get; set; }
        public string SPPAKB { get; set; }
        public string BSTB1 { get; set; }
        public string BSTB2 { get; set; }
        public string BSTB3 { get; set; }
        public string BSTB4 { get; set; }
        public string CheckListSurvey1 { get; set; }
        public string CheckListSurvey2 { get; set; }
        public string CheckListSurvey3 { get; set; }
        public string CheckListSurvey4 { get; set; }
        public string PaymentReceipt1 { get; set; }
        public string PaymentReceipt2 { get; set; }
        public string PaymentReceipt3 { get; set; }
        public string PaymentReceipt4 { get; set; }
        public string PremiumCal1 { get; set; }
        public string PremiumCal2 { get; set; }
        public string PremiumCal3 { get; set; }
        public string PremiumCal4 { get; set; }
        public string FormA1 { get; set; }
        public string FormA2 { get; set; }
        public string FormA3 { get; set; }
        public string FormA4 { get; set; }
        public string FormB1 { get; set; }
        public string FormB2 { get; set; }
        public string FormB3 { get; set; }
        public string FormB4 { get; set; }
        public string FormC1 { get; set; }
        public string FormC2 { get; set; }
        public string FormC3 { get; set; }
        public string FormC4 { get; set; }
        // add FUBidStatus for BRD 328 handle 5 or more bidding
        public string FUBidStatus { get; set; }
        public int IsRenewal { get; set; }
        // add FollowUpNotes,PICWA,BuktiBayar for BRD 0089
        public string BuktiBayar { get; set; }
        public string FollowUpNotes { get; set; }
        public string PICWA { get; set; }
        public string Receiver { get; set; }
        public string RemarkToSA { get; set; }

        public string DocNSA1 { get; set; }
        public string DocNSA2 { get; set; }
        public string DocNSA3 { get; set; }
        public string DocNSA4 { get; set; }
        public string DocNSA5 { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "FollowUpNo" };
        }
    }

    public class FollowUpHistory
    {
        public string FollowUpNo { get; set; }
        public int SeqNo { get; set; }
        public string CustID { get; set; }
        public string ProspectName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string SalesOfficerID { get; set; }
        public DateTime? EntryDate { get; set; }
        public string FollowUpName { get; set; }
        public DateTime? NextFollowUpDate { get; set; }
        public DateTime? LastFollowUpDate { get; set; }
        public int FollowUpStatus { get; set; }
        public int FollowUpInfo { get; set; }
        public string Remark { get; set; }
        public string BranchCode { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "FollowUpNo", "SeqNo" };
        }
    }

    public class FollowUpSummary
    {
        public string SalesOfficerID { get; set; }
        public string SalesName { get; set; }
        public string BranchCode { get; set; }
        public string BranchDesc { get; set; }
        public string Region { get; set; }

        // 0261/URF/2015 EKI
        // add `National` field for other Sales (ex:GoMitra)
        public string National { get; set; }

        public DateTime? Period { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int TotalProspect { get; set; }
        public int TotalNeedFu { get; set; }
        public int TotalPotential { get; set; }
        public int TotalCallLater { get; set; }
        public int TotalCollectDoc { get; set; }
        public int TotalNotDeal { get; set; }
        public int TotalSendToSA { get; set; }
        public int TotalPolicyCreated { get; set; }
        public int TotalBackToAo { get; set; }
        public int TotalDeal { get; set; }
        public string Frequency { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "SalesOfficerID", "BranchCode", "Region", "Period" };
        }
    }

    public class OrderSimulation
    {
        public string OrderNo { get; set; }
        public string CustID { get; set; }
        public string FollowUpNo { get; set; }
        public string QuotationNo { get; set; }
        public bool MultiYearF { get; set; }
        public int YearCoverage { get; set; }
        public int TLOPeriod { get; set; }
        public int ComprePeriod { get; set; }
        public string BranchCode { get; set; }
        public string SalesOfficerID { get; set; }
        public string PhoneSales { get; set; }
        public Int64? DealerCode { get; set; }
        public Int64? SalesDealer { get; set; }
        public string ProductTypeCode { get; set; }
        public string ProductCode { get; set; }
        public string MouID { get; set; }
        public decimal AdminFee { get; set; }
        public decimal TotalPremium { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public int SendStatus { get; set; }
        public DateTime? SendDate { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public int InsuranceType { get; set; }
        public bool ApplyF { get; set; }
        public bool SendF { get; set; }
        public decimal LastInterestNo { get; set; }
        public decimal LastCoverageNo { get; set; }
        //add item for 0089/URF/2019
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public string VANumber { get; set; }
        public string PolicyNo { get; set; }
        public string OldPolicyNo { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "OrderNo" };
        }
    }

    public class OrderSimulationMV
    {
        public string OrderNo { get; set; }
        public decimal ObjectNo { get; set; }
        public string ProductTypeCode { get; set; }
        public string VehicleCode { get; set; }
        public string BrandCode { get; set; }
        public string ModelCode { get; set; }
        public string Series { get; set; }
        public string Type { get; set; }
        public decimal Sitting { get; set; }
        public string Year { get; set; }
        public string CityCode { get; set; }
        public string UsageCode { get; set; }
        public decimal SumInsured { get; set; }
        public decimal AccessSI { get; set; }
        public string RegistrationNumber { get; set; }
        public string EngineNumber { get; set; }
        public string ChasisNumber { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public bool IsNew { get; set; }
        //add ColorOnBPKB for 0089/URD/2019
        public string ColorOnBPKB { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "OrderNo", "ObjectNo" };
        }
    }
   

    public class OrderSimulationInterest
    {
        public string OrderNo { get; set; }
        public decimal ObjectNo { get; set; }
        public decimal InterestNo { get; set; }
        public string InterestID { get; set; }
        public string Year { get; set; }
        public decimal Premium { get; set; }
        public DateTime? PeriodFrom { get; set; }
        public DateTime? PeriodTo { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        //add Deductiblecode for BRD 0089/urf/2019
        public string DeductibleCode { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "OrderNo", "ObjectNo", "InterestNo" };
        }
    }

    public class OrderSimulationCoverage
    {
        public string OrderNo { get; set; }
        public decimal ObjectNo { get; set; }
        public decimal InterestNo { get; set; }
        public decimal CoverageNo { get; set; }
        public string CoverageID { get; set; }
        public decimal Rate { get; set; }
        public decimal SumInsured { get; set; }
        public decimal LoadingRate { get; set; }
        public decimal Loading { get; set; }
        public decimal Premium { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        // bdrd 0089/urf/2019
        public decimal EntryPct { get; set; }
        public decimal Ndays { get; set; }
        public decimal ExcessRate { get; set; }
        public decimal CalcMethod { get; set; }
        public decimal CoverPremium { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal Maxsi { get; set; }
        public decimal Net1 { get; set; }
        public decimal Net2 { get; set; }
        public decimal Net3 { get; set; }
        public string DeductibleCode { get; set; } 

        public bool RowStatus { get; set; }
        public bool IsBundling { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "OrderNo", "ObjectNo", "InterestNo", "CoverageNo" };
        }
    }

    public class EmailQuotation
    {
        public string EmailNo { get; set; }
        public string OrderNo { get; set; }
        public string Email { get; set; }
        public int EmailSendStatus { get; set; }
        public DateTime? EmailDeliveryDate { get; set; }
        public string SalesOfficerID { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "EmailNo" };
        }
    }

    public class EmailSummary
    {
        public string EmailSummaryNo { get; set; }
        public string ReportType { get; set; }
        public DateTime? Period { get; set; }
        public string Email { get; set; }
        public int EmailSendStatus { get; set; }
        public DateTime? EmailDeliveryDate { get; set; }
        public string UserID { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "EmailSummaryNo" };
        }
    }

    /*
     * 0125/URF/2015
     * Dev = DTK
     * */

    public class OrderApproval
    {
        public string OrderNo { get; set; }
        public int Order_ID { get; set; }
        public string Name { get; set; }
        public string PayToCode { get; set; }
        public string PayToName { get; set; }
        public string isSO { get; set; }
        public string Account_Info_BankName { get; set; }
        public string Account_Info_AccountNo { get; set; }
        public string Account_Info_AccountHolder { get; set; }
    }

    public class OrderApprovalDetail
    {
        public string OrderNo { get; set; }
        public string SalesmanId { get; set; }
        public string SalesmanName { get; set; }
        public string CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string PayToCode { get; set; }
        public string PayToName { get; set; }
        public string PolicyNo { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Account_Info_BankName { get; set; }
        public string Account_Info_AccountNo { get; set; }
        public string Account_Info_AccountHolder { get; set; }
        public decimal Discount { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal Premium { get; set; }
        public decimal AdminFee { get; set; }
        public decimal TotalSI { get; set; }
        public decimal TotalPremium { get; set; }
        public decimal TotalCommission { get; set; }
        public decimal CommissionPrecentage { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
    }

    public class OrderApprovalDetailObject
    {
        public string Order_No { get; set; }
        public decimal Object_No { get; set; }
        public string VehicleName { get; set; }
        public string Interest_Code { get; set; }
        public string Interest_Description { get; set; }
        public string Coverage_Id { get; set; }
        public string Coverage_Description { get; set; }
        public DateTime? Begin_Date { get; set; }
        public DateTime? End_Date { get; set; }
        public decimal Year { get; set; }
        public string PoliceNumber { get; set; }
        public decimal Rate { get; set; }
        public decimal Sum_Insured { get; set; }
        public decimal LoadingRate { get; set; }
        public decimal Cover_Premium { get; set; }
        public decimal Ndays { get; set; }
        public bool aggreed_value { get; set; }
    }

    public class TempOrderApprovalDetailObject
    {
        public string Order_No { get; set; }
        public decimal Object_No { get; set; }
        public string VehicleName { get; set; }
        public string Interest_Code { get; set; }
        public string Interest_Description { get; set; }
        public string Coverage_Id { get; set; }
        public string Coverage_Description { get; set; }
        public decimal Ndays { get; set; }
        public DateTime? Begin_Date { get; set; }
        public DateTime? End_Date { get; set; }
        public decimal Year { get; set; }
        public string PoliceNumber { get; set; }
        public bool aggreed_value { get; set; }
        public decimal Rate { get; set; }
        public decimal Sum_Insured { get; set; }
        public decimal LoadingRate { get; set; }
        public decimal Cover_Premium { get; set; }
    }
    /*  END */

    public class B2B_Interest_Coverage
    {
        public string Product_Code { get; set; }
        public string Vehicle_Type { get; set; }
        public string Usage { get; set; }
        public string Type { get; set; }
        public string Interest_Id { get; set; }
        public string Coverage_Id { get; set; }
        public int Cover_Flag { get; set; }
        public bool Comprehensive_Flag { get; set; }
        public bool TLO_Flag { get; set; }
        public string Default_Currency { get; set; }
        public decimal Default_Sum_Insured { get; set; }
        public byte Basic_Cover_Deductible { get; set; }
        public string Deductible_Score_Code { get; set; }
        public string Deductible_Code { get; set; }
        public string Deductible_Currency { get; set; }
        public decimal Deductible_Amount { get; set; }
        public int Auto_Apply { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Product_Code", "Vehicle_Type", "Usage", "Type", "Interest_Id", "Coverage_Id", "Deductible_Currency" };
        }
    }

    public class Dtl_Ins_Factor
    {
        public string Factor_Code { get; set; }
        public string insurance_code { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Factor_Code", "insurance_code" };
        }
    }

    public class Dtl_Rate_Factor
    {
        public string Score_Code { get; set; }
        public string Insurance_type { get; set; }
        public string Coverage_id { get; set; }
        public string factor_code { get; set; }
        public int sequence_no { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Score_Code", "Insurance_type", "Coverage_id", "factor_code" };
        }
    }

    public class Dtl_Rate_Scoring
    {
        public string Coverage_ID { get; set; }
        public string Score_code { get; set; }
        public decimal Rate_No { get; set; }
        public decimal BC_Up_To_SI { get; set; }
        public string Insurance_Type { get; set; }
        public string Rating_value1 { get; set; }
        public string Rating_value2 { get; set; }
        public string Rating_value3 { get; set; }
        public string Rating_value4 { get; set; }
        public string Rating_value5 { get; set; }
        public string Rating_value6 { get; set; }
        public string Rating_value7 { get; set; }
        public string Rating_value8 { get; set; }
        public string Rating_value9 { get; set; }
        public string Rating_value10 { get; set; }
        public decimal Rate { get; set; }
        public decimal BC_Premium { get; set; }
        public decimal BC_Max_SI { get; set; }
        public decimal Excess_Rate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Coverage_ID", "Score_code", "Rate_No" };
        }
    }

    public class Prd_Agreed_Value
    {
        public string Product_Code { get; set; }
        public decimal UPTO { get; set; }
        public decimal Rate { get; set; }
        public decimal CancelRate { get; set; } // bsy
        public Boolean Status { get; set; } // bsy
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Product_Code", "UPTO" };
        }
    }

    public class Prd_Deductible
    {
        public string Product_Code { get; set; }
        public string Deductible_Code { get; set; }
        public string Description { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Product_Code", "Deductible_Code" };
        }
    }

    public class Prd_Interest_Coverage
    {
        public string Product_Code { get; set; }
        public string Interest_ID { get; set; }
        public string Coverage_Id { get; set; }
        public decimal Given_SI { get; set; }
        public string Given_Curr_ID { get; set; }
        public decimal Rate { get; set; }
        public decimal Bc_Min_Premium { get; set; }
        public decimal Bc_Max_Si { get; set; }
        public decimal Excess_Rate { get; set; }
        public string Scoring_Code { get; set; }
        public bool Change_Rate { get; set; }
        public string Special_Commission { get; set; }
        public string Discounts { get; set; }
        public bool IC_Status { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Product_Code", "Interest_ID", "Coverage_Id" };
        }
    }

    public class Prd_Load_Mv
    {
        public string Product_Code { get; set; }
        public string Insurance_Code { get; set; }
        public string Coverage_Id { get; set; }
        public decimal Age { get; set; }
        public decimal Load_Pct { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Product_Code", "Insurance_Code", "Coverage_Id", "Age" };
        }
    }

    public class PRD_POLICY_FEE
    {
        public string Product_Code { get; set; }
        public string Order_Type { get; set; }
        public string Curr_Id { get; set; }
        public decimal Lower_Limit { get; set; }
        public decimal Policy_Fee1 { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Product_Code", "Order_Type", "Curr_Id", "Lower_Limit" };
        }
    }

    public class GraphicColorSetting
    {
        public int ID { get; set; }
        public decimal PctUpTo { get; set; }
        public string ColorCode { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }


        public static string[] getPrimaryKey()
        {
            return new string[] { "ID" };
        }
    }

    public class AABHead
    {
        public string Type { get; set; }
        public string Param { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Type", "Param", "Value" };
        }
    }

    public class SysParam
    {
        public string Param { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "Param" };
        }
    }

    public class ImageData
    {
        public string ImageID { get; set; }
        public string DeviceID { get; set; }
        public string SalesOfficerID { get; set; }
        public string PathFile { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "ImageID" };
        }
    }

    public class EmailNotification
    {
        public string EmailNotificationNo { get; set; }
        public int NotificationType { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
        public int EmailSendStatus { get; set; }
        public DateTime? EmailDeliveryDate { get; set; }
        public string UserID { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "EmailNotificationNo" };
        }
    }

    public class SalesAdmin
    {
        public string SalesAdminID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Validation { get; set; }
        public DateTime? Expiry { get; set; }
        public DateTime? Password_Expiry { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string OfficePhone { get; set; }
        public string Ext { get; set; }
        public string Fax { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
    }

    public class SalesAdminBranch
    {
        public string SalesAdminID { get; set; }
        public string BranchCode { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
    }

    public class vwListFollowUp
    {
        public string FollowUpNo { get; set; }
        public DateTime? SendDocDate { get; set; }
        public string OrderNo { get; set; }
        public string QuotationNo { get; set; }
        public string ProspectName { get; set; }
        public string AOName { get; set; }
        public string VehicleDesc { get; set; }
        public int FollowUpStatus { get; set; }
        public string StatusDescription { get; set; }
        public int FollowUpInfo { get; set; }
        public string InfoDescription { get; set; }
        public string TransactionNo { get; set; }
        public string PolicyNo { get; set; }
        public string SalesAdminID { get; set; }
        public string SalesAdminName { get; set; }
        public string BranchCode { get; set; }
    }

    public class vwDetailFollowUp
    {
        public string FollowUpNo { get; set; }
        public string OrderNo { get; set; }
        public DateTime? EntryDate { get; set; }
        public string ProspectName { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string CustEmail { get; set; }
        public string ProductCode { get; set; }
        public int YearCoverage { get; set; }
        public string SalesName { get; set; }
        public string SalesOfficerID { get; set; }
        public Int64 DealerCode { get; set; }
        public string DealerName { get; set; }
        public Int64 SalesDealer { get; set; }
        public string SalesDealerName { get; set; }
        public string VehicleCode { get; set; }
        public string VehicleType { get; set; }
        public string VehicleBrand { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleSeries { get; set; }
        public string VehicleYear { get; set; }
        public bool IsNew { get; set; }
        public decimal Sitting { get; set; }
        public string Transmission { get; set; }
        public string RegistrationNumber { get; set; }
        public string EngineNumber { get; set; }
        public string ChasisNumber { get; set; }
        public string Usage { get; set; }
        public string Geography { get; set; }
        public decimal Premium { get; set; }
        public decimal AdminFee { get; set; }
        public decimal TotalPremium { get; set; }
        public string TransactionNo { get; set; }
        public string PolicyNo { get; set; }
        public int FollowUpStatus { get; set; }
        public int FollowUpInfo { get; set; }
        public string IdentityCard { get; set; }
        public string STNK { get; set; }
        public string SPPAKB { get; set; }
        public string BSTB1 { get; set; }
        public string BSTB2 { get; set; }
        public string BSTB3 { get; set; }
        public string BSTB4 { get; set; }
        public string CheckListSurvey1 { get; set; }
        public string CheckListSurvey2 { get; set; }
        public string CheckListSurvey3 { get; set; }
        public string CheckListSurvey4 { get; set; }
        public string PaymentReceipt1 { get; set; }
        public string PaymentReceipt2 { get; set; }
        public string PaymentReceipt3 { get; set; }
        public string PaymentReceipt4 { get; set; }
        public string PremiumCal1 { get; set; }
        public string PremiumCal2 { get; set; }
        public string PremiumCal3 { get; set; }
        public string PremiumCal4 { get; set; }
        public string FormA1 { get; set; }
        public string FormA2 { get; set; }
        public string FormA3 { get; set; }
        public string FormA4 { get; set; }
        public string FormB1 { get; set; }
        public string FormB2 { get; set; }
        public string FormB3 { get; set; }
        public string FormB4 { get; set; }
        public string FormC1 { get; set; }
        public string FormC2 { get; set; }
        public string FormC3 { get; set; }
        public string FormC4 { get; set; }
        public string Remark { get; set; }
    }

    public class vwOrderCoverage
    {
        public string OrderNo { get; set; }
        public string InterestID { get; set; }
        public string InterestDesc { get; set; }
        public string CoverageID { get; set; }
        public string CoverageDesc { get; set; }
        public string Year { get; set; }
        public DateTime? BeginDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal SumInsured { get; set; }
        public decimal Rate { get; set; }
        public decimal LoadingRate { get; set; }
        public decimal Premium { get; set; }
        public decimal Loading { get; set; }
        public decimal NetPremium { get; set; }
        public int InterestSequence { get; set; }
        public int CoverageSequence { get; set; }
    }

    public class AABBranch
    {
        public string BranchCode { get; set; }
        public string Description { get; set; }
        public string Region { get; set; }

        public string City { get; set; }
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string Bank { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string BankAddress { get; set; }
        public string BranchInitial { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "BranchCode" };
        }
    }

    public class Branch
    {
        public string BranchId { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CityId { get; set; }
        public string City { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "BranchId" };
        }
    }

    public class Workshop
    {
        public Int64 WorkshopId { get; set; }
        public string WorkshopName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CityId { get; set; }
        public string City { get; set; }
        public Int64 isDeleted { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "WorkshopId" };
        }
    }

    public class GardaCenter
    {
        public Int64 GardaCenterId { get; set; }
        public string GardaCenterName { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string CityId { get; set; }
        public string City { get; set; }
        public Int64 isDeleted { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "GardaCenterId" };
        }
    }

    public class AccountOfficer
    {
        public string SalesOfficerID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Role { get; set; }
        public string BranchCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string OfficePhone { get; set; }
        public string Ext { get; set; }
        public string Fax { get; set; }
        public string AreaCodePhone { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "SalesOfficerID" };
        }
    }

    public class VehiclePriceTolerance
    {
        public int ID { get; set; }
        public bool IsNew { get; set; }
        public bool IsATPM { get; set; }
        public int FormType { get; set; }
        public int VehicleCategory { get; set; }
        public decimal MaxSI { get; set; }
        public int ToleranceType { get; set; }
        public decimal TolerancePct { get; set; }
        public decimal ToleranceAmt { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "ID" };
        }
    }

    public class MstInterest
    {
        public string InterestID { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "InterestID" };
        }
    }

    public class MstCoverage
    {
        public string CoverageID { get; set; }
        public string Description { get; set; }
        public int Sequence { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string FieldChanged { get; set; }
        public string PrimaryChanged { get; set; }
        public string RowState { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "CoverageID" };
        }
    }

    #endregion

    #region SyncData

    public class SyncDataResponse
    {
        public List<Boolean> Status { get; set; }
        public DateTime TimeStamp { get; set; }
    }

    #endregion

    #region DirectSync

    public class ProspectData
    {
        public List<FollowUp> FollowUp { get; set; }
        public List<ProspectCustomer> ProspectCustomer { get; set; }
        public List<FollowUpHistory> FollowUpHistory { get; set; }
        public List<OrderSimulation> OrderSimulation { get; set; }
        public List<OrderSimulationMV> OrderSimulationMV { get; set; }
        public List<OrderSimulationInterest> OrderSimulationInterest { get; set; }
        public List<OrderSimulationCoverage> OrderSimulationCoverage { get; set; }
        public List<EmailNotification> EmailNotification { get; set; }
        public List<EmailQuotation> EmailQuotation { get; set; }
    }

    public class ProspectDataResult
    {
        public List<Boolean> FollowUp { get; set; }
        public List<Boolean> ProspectCustomer { get; set; }
        public List<Boolean> FollowUpHistory { get; set; }
        public List<Boolean> OrderSimulation { get; set; }
        public List<Boolean> OrderSimulationMV { get; set; }
        public List<Boolean> OrderSimulationInterest { get; set; }
        public List<Boolean> OrderSimulationCoverage { get; set; }
        public List<Boolean> EmailNotification { get; set; }
        public List<Boolean> EmailQuotation { get; set; }
    }

    public class ProspectDataStringResult
    {
        public List<string> FollowUp { get; set; }
        public List<string> ProspectCustomer { get; set; }
        public List<string> FollowUpHistory { get; set; }
        public List<string> OrderSimulation { get; set; }
        public List<string> OrdersSimulationMV { get; set; }
        public List<string> OrderSimulationInterest { get; set; }
        public List<string> OrderSimulationCoverage { get; set; }
        public List<string> EmailNotification { get; set; }
        public List<string> EmailQuotation { get; set; }
    }

    #endregion

    #region 0261/URF/2015 EKI
    /**
    *   add AABRegion and ProductAgent status
    **/
    public class AABRegion
    {
        public string RegionCode { get; set; }
        public string RegionDescription { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string NationalCode { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "RegionCode" };
        }
    }

    public class ProductAgent
    {
        public string ProductType { get; set; }
        public string ProductCode { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public bool RowStatus { get; set; }
        public string ChannelSource { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "ProductType", "ProductCode" };
        }
    }
    #endregion

    #region 0328/URF/2015 EKI
    public class Penawaran
    {
        public int policyID { get; set; }
        public string policyCode { get; set; }
        public DateTime? policyGeneratedDate { get; set; }
        public string agentCode { get; set; }
        public string agentEmail { get; set; }
        public string agentFullName { get; set; }
        public string customerName { get; set; }
        public string customerEmail { get; set; }
        public string customerPhone { get; set; }
        public string policeNumber { get; set; }
        public string productCode { get; set; }
        public string protectionType { get; set; }
        public string vehicleCode { get; set; }
        public string vehicleType { get; set; }
        public string vehicleDescription { get; set; }
        public string vehicleYear { get; set; }
        public string usageCode { get; set; }
        public string usageText { get; set; }
        public string areaCode { get; set; }
        public decimal totalSumInsured { get; set; }
        public decimal baseRate { get; set; }
        public Int16 isTJH { get; set; }
        public decimal tjhRate { get; set; }
        public decimal tjhValue { get; set; }
        public Int16 isPaDriver { get; set; }
        public decimal paDriverRate { get; set; }
        public decimal paDriverValue { get; set; }
        public Int16 isPaPass { get; set; }
        public decimal paPassRate { get; set; }
        public int paPassQuantity { get; set; }
        public decimal paPassValue { get; set; }
        public Int16 isAutoApply { get; set; }
        public decimal srccRate { get; set; }
        public decimal etvRate { get; set; }
        public decimal fldRate { get; set; }
        public Int16 isCheckSrcc { get; set; }
        public decimal autoApplyRate { get; set; }
        public Int16 isTRS { get; set; }
        public decimal trsRate { get; set; }
        public Int16 isAcc { get; set; }
        public decimal accRate { get; set; }
        public decimal accValue { get; set; }
        public decimal loadingRate { get; set; }
        public decimal administrationPrice { get; set; }
        public decimal basicPremi { get; set; }
        public decimal accPremi { get; set; }
        public decimal totalPremiKendaraanPlusAksesoris { get; set; }
        public decimal premiLoading { get; set; }
        public decimal totalPremiDasarPlusLoading { get; set; }
        public decimal premiAutoApply { get; set; }
        public decimal premiTRS { get; set; }
        public decimal premiTJH { get; set; }
        public decimal premiPADriver { get; set; }
        public decimal premiPAPass { get; set; }
        public decimal totalPremiPerluasan { get; set; }
        public decimal totalPremiDasarPlusPerluasan { get; set; }
        public decimal totalAllPremi { get; set; }
        public DateTime? infoModified { get; set; }
        public int BidStatus { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public int Domisili { get; set; }
        public DateTime? followUpDateTime { get; set; }
        public Byte isHotProspect { get; set; }
        public Byte fuByCSOBranch { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "PolicyID" };
        }
    }

    public class HistoryPenawaran
    {
        public int PolicyID { get; set; }
        public string OrderNo { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public string SalesOfficerID { get; set; }
        public int IsAbleExpired { get; set; }
        public int HistoryPenawaranID { get; set; }

        public static string[] getPrimaryKey()
        {
            return new string[] { "HistoryPenawaranID" };
        }
    }
    #endregion

    #region Order Approval

    public class Mst_Order_Mobile
    {
        public int Order_ID { get; set; }
        public string Order_No { get; set; }
        public string Account_Info_BankName { get; set; }
        public string Account_Info_AccountNo { get; set; }
        public string Account_Info_AccountHolder { get; set; }
        public int Approval_Status { get; set; }
        public int Approval_Process { get; set; }
        public int isSO { get; set; }
        public string Remarks { get; set; }
        public string Email_SA { get; set; }
        public DateTime? EntryDt { get; set; }
        public string EntryUsr { get; set; }
        public DateTime? UpdateDt { get; set; }
        public string UpdateUsr { get; set; }
        public int Approval_Type { get; set; }
        public DateTime? Actual_Date { get; set; }
    }

    #endregion

    #region Rewrite Otosales

    public class OrderSimulationSurvey
    {
        public string OrderNo { get; set; }
        public string CityCode { get; set; }
        public string LocationCode { get; set; }
        public string SurveyAddress { get; set; }
        public string SurveyPostalCode { get; set; }
        public DateTime? SurveyDate { get; set; }
        public string ScheduleTimeID { get; set; }
        public int IsNeedSurvey { get; set; }
        public int IsNSASkipSurvey { get; set; }
    }

    public class BasicCover
    {
        public int Id { get; set; }
        public string CoverageId { get; set; }
        public int ComprePeriod { get; set; }
        public int TLOPeriod { get; set; }
        public string  Description { get; set; }
    }
    public class CoveragePeriod
    {
        public int Year { get; set; }
        public CoverageType CoverageType { get; set; }
    }
    public enum CoverageType
    {
        Comprehensive,
        TLO
    }
    public enum CoverageTypeNonBasic
    {
        SRCC,
        FLD,
        ETV,
        TS,
        TPLPER,
        PADRVR,
        PAPASS,
        ACCESS
    }
    public class CoveragePeriodNonBasic
    {
        public CoverageTypeNonBasic CoverageTypeNonBasic { get; set; }
        public CoverageType CoverageType { get; set; }
        public int Year { get; set; }
        public bool IsBundling { get; set; }

    public CoveragePeriodNonBasic(CoverageTypeNonBasic coverageTypeNonBasic, int year, bool isBundling){
        CoverageTypeNonBasic = coverageTypeNonBasic;
        Year = year;
        IsBundling = isBundling;
    }
    }
    public class CoverageBundling
    {
        public String Coverage_Id { get; set; }
        public String Scoring_Code { get; set; }
        public int Auto_Apply { get; set; }
        public int Year { get; set; }


    }
    public class CommissionScheme
    {
        public decimal TotalFlatAmount { get; set; }
        public decimal TotalPercentage { get; set; }
        public int Level { get; set; }
        public List<CScheme> Schemes { get; set; }
    }
    public class CScheme
    {
        public bool Active { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
        public decimal Percentage { get; set; }
        public decimal FlatAmount { get; set; }
        public decimal Amount { get; set; }
        public string PayToCode { get; set; }
        public int ApplyFlags { get; set; }
        public decimal SI = -1;
        public decimal OriginalFlatAmount = -1;
    }
    public class AutoApplyFlag
    {
        public const string NonAutoApply = "0";
        public const string TLOBundling = "2";
        public const string CompreBundling = "1";
        public const string TLOExtendedGroup = "4";
        public const string CompreExtendedGroup = "3";
    }
    public class DetailCoverageSummary
    {
        public List<DetailCoverage> dtlCoverage { get; set; }
        public double TPLSI { get; set; }
        public double PADRVRSI { get; set; }
        public double PAPASSSI { get; set; }
        public double AdminFee { get; set; }
    }
    public class DetailCoverage
    {
        public int Year { get; set; }
        public double VehiclePremi { get; set; }
        public double AccessPremi { get; set; }
        public double VehicleAccessPremi { get; set; }
        public double Rate { get; set; }
        public double LoadingPremi { get; set; }
        public double BasicPremi { get; set; }
        public string BasiCoverName { get; set; }
        public string BasiCoverage { get; set; }
        public string TSCoverage { get; set; }
        public double TSPremi { get; set; }
        public double SFEPremi { get; set; }
        public double ExtendedSFEPremi { get; set; }
        public double PADRVRCoverage { get; set; }
        public double PAPASSCoverage { get; set; }
        public double TPLCoverage { get; set; }
        public double PADRVRRate { get; set; }
        public double PAPASSRate { get; set; }
        public double TPLRate { get; set; }
        public double TSRate { get; set; }
        public double SFERate { get; set; }
        public double PremiPerluasan { get; set; }
        public double PremiDasarPerluasan { get; set; }
        public double TotalPremi { get; set; }
        public int IsBundling { get; set; }
        public int IsBundlingTRS { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
    }
    public class CalculatedPremi
    {
        public string InterestID { get; set; }
        public string CoverageID { get; set; }
        public int Year { get; set; }
        public DateTime PeriodFrom { get; set; }
        public DateTime PeriodTo { get; set; }
        public double Rate { get; set; }
        public decimal ExcessRate { get; set; }
        public double Premium { get; set; }
        public decimal CoverPremium { get; set; }
        public decimal GrossPremium { get; set; }
        public decimal MaxSI { get; set; }
        public string DeductibleCode { get; set; }
        public decimal EntryPrecentage { get; set; }
        public decimal Ndays { get; set; }
        public decimal Net1 { get; set; }
        public decimal Net2 { get; set; }
        public decimal Net3 { get; set; }
        public double SumInsured { get; set; }
        public double Loading { get; set; }
        public double LoadingRate { get; set; }
        public bool IsBundling { get; set; }
        public string AutoApply { get; set; }
        public string CoverType { get; set; }
        public int CalcMethod { get; set; }
    }
    public class WTCommission
    {
        public string QuotationNo { get; set; }
        public string CommissionID { get; set; }
        public string CommissionName { get; set; }
        public string CoverageID { get; set; }
        public decimal Percentage { get; set; }
        public string PayToParty { get; set; }
        public int Level { get; set; }
        public decimal IndividualTax { get; set; }
        public decimal CorporateTax { get; set; }
        public int CommissionType { get; set; }
        public int ApplyFlags { get; set; }
    }
    public class DashboardProspect
    {
       public string TotalProspect { get; set; }
       public string TotalNeedFu { get; set; }
       public string TotalPotential { get; set; }
       public string TotalCallLater { get; set; }
       public string TotalNotDeal { get; set; }
       public string TotalDeal { get; set; }
       public string TotalRejected { get; set; }
	   public string TotalPremium { get; set; }
       public string TotalPremiNeedFu { get; set; }
       public string TotalPremiPotential { get; set; }
       public string TotalPremiCallLater { get; set; }
       public string TotalPremiNotDeal { get; set; }
       public string TotalPremiDeal { get; set; }
       public string TotalPremiRejected { get; set; }
       public string RenewalRatio { get; set; }
    }
    public class DashboardSubordinate
    {
        public string Description { get; set; }
        public string TotalPremium { get; set; }
    }
    public class DashboardStatus
    {
        public string Description { get; set; }
        public string TotalPremium { get; set; }
        public string TotalPolicy { get; set; }
        public string TotalPremi { get; set; }
    }
    public class DataUpload
    {
        public string Data { get; set; }
        public string ThumbnailData { get; set; }
    }
    public class StatusCoverage
    {
        public const string Include = "Include";
        public const string NoCover = "No Cover";
    }
    public class CoverageParam
    {
        public string CoverageId { get; set; }
        public DateTime PeriodFromCover { get; set; }
        public DateTime PeriodToCover { get; set; }
    }
    public class GenGuid
    {
        public const int FollowUpNo = 1;
        public const int OrderNo = 2;
        public const int CustId = 3;
        public const int ImageID = 4;
    }
    #endregion
    public class Device
    {
        public Int64 DeviceID { get; set; }
    }
    public class Upliner
    {
        public string UplinerCustomerID { get; set; }
        public string UplinerCode { get; set; }
        public string UplinerName { get; set; }
        public string LeaderCustomerID { get; set; }
        public string LeaderCode { get; set; }
        public string LeaderName { get; set; }
    }

    public class UserDevice
    {
        public string Code;
        public string Message;
    }
}