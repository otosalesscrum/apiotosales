﻿using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0213URF2019
{
    public class UpdateTaskDetailParam
    {
        public string CustID { get; set; }
        public string FollowUpNo { get; set; }
        public PersonalData PersonalData { get; set; }
        public CompanyData CompanyData { get; set; }
        public VehicleData VehicleData { get; set; }
        public PolicyAddress policyAddress { get; set; }
        public SurveySchedule surveySchedule { get; set; }
        public List<ImageDataTaskDetail> imageData { get; set; }
        public OrderSimulationModel OSData { get; set; }
        public OrderSimulationMVModel OSMVData { get; set; }
        public CalculatePremiModel calculatedPremiItems { get; set; }
        public string FollowUpStatus { get; set; }
        public Remarks Remarks { get; set; }
    }
}