﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models.v0213URF2019
{
    public class VATRansaction
    {
        public string PolicyNo { get; set; }   // varchar(16),
        public string VirtualAccountNo { get; set; } // varchar(30),
        public string CurrencyCode { get; set; } // varchar(3),
        public decimal PremiumAmount { get; set; }// NUMERIC(20,4),
        public string PolicyStatusCode { get; set; } // varchar(3),
        public string InsuredCode { get; set; } // varchar(11),
        public string InsuredName { get; set; } // varchar(200),
        public string MarketingCode { get; set; } // varchar(5),
        public string SalesmanAdminLogID { get; set; } // varchar(5),
        public string InternalDistributionCode { get; set; } // varchar(6),
        public DateTime PolicyPeriodFrom { get; set; }//DATETIME,
        public DateTime PolicyPeriodTo { get; set; }//DATETIME,
        public int GracePeriod { get; set; }//int,
        public DateTime PaymentDueDate { get; set; }//datetime,
        public string PolicyNoRenewalBeyond { get; set; } // varchar(20),	
        public string OrderNo { get; set; } // varchar(50),
        public string ChasisNo { get; set; } // varchar(50),
        public string EngineNo { get; set; } // varchar(50),
        public string PolicyStatus { get; set; } // varchar(50),
        public string PoliceNo { get; set; } // varchar(50),
        public string CustomerName { get; set; } // varchar(200),	    
        public string ConnectionString { get; set; }//	VARCHAR(200),
    }
}