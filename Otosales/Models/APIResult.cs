﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Models
{
    public class OtosalesAPIResult
    {
        public bool Status = false;
        public string Message;
        public string ResponseCode;
        public object Data;
    }

    #region Login Result

    public class LoginStatus
    {
        public bool IsAuthenticated { get; set; }
        public String Status { get; set; }
        public String Token { get; set; }
        //0129/URF/2015
        public int ValidHour { get; set; }
    }

    public class LoginResult<T> : LoginStatus
    {
        public T UserInfo { get; set; }
    }

    public class FormAccess
    {
        public string FormID { get; set; }
        public bool? CanRead { get; set; }
        public bool? CanWrite { get; set; }
        public bool? CanAdd { get; set; }
        public bool? CanDelete { get; set; }
    }

    public class UserPrivilege
    {
        public UserPrivilege()
        {
            this.FormAccessCollections = new List<FormAccess>();
        }

        public SalesOfficer User { get; set; }
        public string Region { get; set; }
        public string ReportTo { get; set; }
        public List<FormAccess> FormAccessCollections { get; set; }
    }

    public class UserPrivilegeWebSA
    {
        public UserPrivilegeWebSA()
        {
            this.BranchCollections = new List<SalesAdminBranch>();
        }

        public SalesAdmin User { get; set; }
        public List<SalesAdminBranch> BranchCollections { get; set; }
    }

    #endregion

    #region 0193/URF/2015 Authentication

    public class GlobalAPIResult
    {
        public Boolean status { get; set; }
        public String token { get; set; }
        public int ValidHours { get; set; }
        public DateTime? EntryDate { get; set; }
        public DateTime? LastUpdatedTime { get; set; }
        public UserInfos userInfo { get; set; }
        public List<Menu> menu { get; set; }
        public Parameters parameters { get; set; }
        public String message { get; set; }
        // 0261/URF/2015 BSY
        public String ErrorCode { get; set; }
        public String WaitingDelay { get; set; }
    }

    public class UserInfos
    {
        public string ID { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public DateTime? LastChangedPassword { get; set; }
        public DateTime? PasswordExpiry { get; set; }

    }

    public class Menu
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Caption { get; set; }
        public int ParentID { get; set; }
        public int SeqNo { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public string Info { get; set; }
        public bool? CanRead { get; set; }
        public bool? CanWrite { get; set; }
        public bool? CanAdd { get; set; }
        public bool? CanDelete { get; set; }
    }

    public class Parameters
    {
        public string BranchCode { get; set; }
        public string Class { get; set; }
        public string Department { get; set; }
        public string Ext { get; set; }
        public string Fax { get; set; }
        public string OfficePhone { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Phone3 { get; set; }
        public string Region { get; set; }
        public string ReportTo { get; set; }
        public string Validation { get; set; }
        // 0261/URF/2015 - BSY
        public string Channel { get; set; }
        public string ChannelSource { get; set; }
        public string ExtUserID { get; set; }
        // end
    }


    /// <summary>
    /// Dev: BSY
    /// api result for external user while perform authentication process
    /// </summary>
    public class AuthResult
    {
        public bool Status { get; set; }
        public int ValidHour { get; set; }
        public string Message { get; set; }
        public string ErrorCode { get; set; }
        public string WaitingDelay { get; set; }
    }

    #endregion
}