﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Otosales.Models.vWeb2;

namespace Otosales.Models
{
    public class DashboardResult
    {
        public DashboardSummary summary { get; set; }
        public List<DashboardDetail> detail { get; set; }
    }
}