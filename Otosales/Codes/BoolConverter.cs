﻿using Newtonsoft.Json;
using System;

namespace Otosales.Codes
{
    public class BoolConverter : JsonConverter
    {
        public static BoolConverter create()
        {
            return new BoolConverter();
        }

        private BoolConverter()
        {
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((bool)value) ? 1 : 0);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return reader.Value.ToString() == "1";
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(bool) || objectType == typeof(bool?);
        }
    }
}