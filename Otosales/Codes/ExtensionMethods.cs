﻿using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Otosales.Codes
{
    public static class ExtensionMethods
    {
        private static string assemblyQualifiedName = null;
        public static Type GetCT(this string className)
        {
            if (assemblyQualifiedName == null)
            {
                Type type = typeof(B2B_Interest_Coverage);
                string name = type.AssemblyQualifiedName.ToString();
                assemblyQualifiedName = name.Replace("B2B_Interest_Coverage", "{0}");
            }
            return Type.GetType(String.Format(assemblyQualifiedName, className));
        }

        public static PropertyInfo[] GetPI(this Type type)
        {
            return type.GetProperties();
        }

        public static string[] GetPK(this Type type)
        {
            return (string[])type.GetMethod("getPrimaryKey").Invoke(null, null);
        }
    }
}