﻿using Otosales.Models.Xoom;

namespace Otosales.Codes
{
    public class ErrorBuilder
    {
        public static GeneralResult cannotConnectToDB(GeneralResult GR)
        {
            GR.isSuccess = false;
            GR.errorCode = 101;
            GR.errorMessage = "Cannot connect to XOOM database.";
            GR.result = "";

            return GR;
        }

        public static GeneralResult cannotInsertData(GeneralResult GR)
        {
            GR.isSuccess = false;
            GR.errorCode = 102;
            GR.errorMessage = "Cannot insert data to XOOM database.";
            GR.result = "";

            return GR;
        }

        public static GeneralResult cannotFindID(GeneralResult GR)
        {
            GR.isSuccess = false;
            GR.errorCode = 103;
            GR.errorMessage = "Cannot find your ID.";
            GR.result = "";

            return GR;
        }

        public static GeneralResult partOutOfRange(GeneralResult GR)
        {
            GR.isSuccess = false;
            GR.errorCode = 104;
            GR.errorMessage = "Part requested out of range.";
            GR.result = "";

            return GR;
        }

        public static GeneralResult cannotFindData(GeneralResult GR)
        {
            GR.isSuccess = false;
            GR.errorCode = 105;
            GR.errorMessage = "Cannot find your data.";
            GR.result = "";

            return GR;
        }
    }
}