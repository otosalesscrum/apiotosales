﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;

/**
 *  Rev No      : 001
 *  Rev Date    : 2016-09-21
 *  Dev         : BSY
 *  Remark      : add condition hp.IsAbleExpired > 0 when obtain policies in CheckAllMGObidExpiry
 */
namespace Otosales.Codes
{
    public class Util
    {

        #region Declare

        // connection string
        public const string CONNECTION_STRING_AABMOBILE = "AABMobile";
        public const string CONNECTION_STRING_AABAGENT = "AABAgent";
        public const string CONNECTION_STRING_GARDAMOBILECMS = "GardaMobileCMS";
        public const string CONNECTION_STRING_AAB = "AAB";
        public const string CONNECTION_STRING_XOOM = "XOOM";
        public const string CONNECTION_STRING_BEYONDDB = "BeyondDB";
        public const string CONNECTION_STRING_ASURANSIASTRADB = "AsuransiAstraDB";
        public const string CONNECTION_STRING_OTOCAREDB = "OtocareDB";
        public const string CONNECTION_STRING_GARDAAKSES = "GardaAksesDB";
        
        // utility var
        public static int IS_DEBUG = int.Parse(ConfigurationManager.AppSettings["IsDebug"].ToString());

        private static readonly a2isLogHelper logger = new a2isLogHelper();

        #endregion

        #region Debug Utility Methods

        public static void SaveSyncDebug(string tableName, string data)
        {
            if (IS_DEBUG == 1)
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    try
                    {
                        db.Execute("INSERT INTO SyncDebug (TableName, Data, EntryDate) VALUES (@0,@1,GETDATE())", tableName, data);
                    }
                    catch (Exception ex)
                    { 
                        logger.ErrorFormat("Error occured in {0} - {1}: {2}", "Util", "SaveSyncDebug", ex.Message.ToString());
                    }
                }
            }
        }

        #endregion

        #region MGOBid Utility Methods

        /// <summary>
        ///  conversion from checkExpired method
        /// </summary>
        #region old
        //public static void CheckAllMGOBidExpiry()
        //{
        //    using (var db = new a2isDBHelper.Database(CONNECTION_STRING_AABAGENT))
        //    {
        //        List<int> policiesObtainedByMGO = db.Fetch<int>("SELECT DISTINCT hp.PolicyID as PolicyID FROM HistoryPenawaran hp JOIN Penawaran p ON hp.PolicyID = p.policyID AND p.BidStatus = 1 AND hp.IsAbleExpired > 0 ORDER BY hp.PolicyID DESC"); // 001

        //        for (int i = 0; i < policiesObtainedByMGO.Count; i++)
        //        {
        //            HistoryPenawaran historyPenawaran = db.First<HistoryPenawaran>("SELECT TOP 1 hp.PolicyID AS PolicyID, hp.ExpiredDate as ExpiredDate, hp.IsAbleExpired as IsAbleExpired, hp.OrderNo FROM HistoryPenawaran hp JOIN Penawaran p ON hp.PolicyID = p.policyID AND p.BidStatus = 1 AND hp.PolicyID = @0 ORDER BY hp.LastUpdatedTime DESC", policiesObtainedByMGO[i]);

        //            if ((DateTime.Compare(Convert.ToDateTime(historyPenawaran.ExpiredDate), DateTime.Now) < 0) && (historyPenawaran.IsAbleExpired > 0))
        //            {
        //                db.Execute("UPDATE Penawaran SET BidStatus = 0 WHERE policyID = @0", historyPenawaran.PolicyID);
        //                SoftDeleteProspect(historyPenawaran.OrderNo);
        //            }
        //        }

        //    }
        //}
        #endregion
        public static void CheckAllMGOBidExpiry()
        {
            try
            {
                using (var db = new a2isDBHelper.Database(CONNECTION_STRING_AABAGENT))
                {
                    List<HistoryPenawaran> listHP = db.Fetch<HistoryPenawaran>(@"
        SELECT hp.PolicyID AS PolicyID, hp.ExpiredDate as ExpiredDate, hp.IsAbleExpired as IsAbleExpired, hp.OrderNo 
        FROM dbo.HistoryPenawaran hp 
        INNER JOIN (SELECT MAX(hp1.HistoryPenawaranID) HistoryPenawaranID, PolicyID 
        			FROM dbo.HistoryPenawaran hp1 GROUP BY PolicyID ) a
        ON a.HistoryPenawaranID = hp.HistoryPenawaranID
        INNER JOIN dbo.Penawaran p ON hp.PolicyID = p.policyID AND p.BidStatus = 1 
        WHERE hp.IsAbleExpired > 0 AND hp.ExpiredDate < GETDATE()");
                    List<int> lsPolicyID = listHP.Select(x => x.PolicyID).ToList();
                    if (lsPolicyID.Count > 0)
                    {
                        db.Execute("UPDATE Penawaran SET BidStatus = 0 WHERE policyID IN (@0)", lsPolicyID);
                        List<string> lsOrdNo = listHP.Where(y => !string.IsNullOrEmpty(y.OrderNo)).Select(x => x.OrderNo?.Trim()).ToList();
                        if (lsOrdNo.Count > 0)
                        {
                            using (var db2 = new a2isDBHelper.Database(CONNECTION_STRING_AABMOBILE))
                            {
                                db2.Execute(@"
        UPDATE f 
        SET f.RowStatus = 0, f.LastUpdatedTime = GETDATE()
        FROM dbo.FollowUp f
        INNER JOIN dbo.OrderSimulation os 
        ON os.FollowUpNo = f.FollowUpNo
        WHERE os.OrderNo IN (@0)

        UPDATE dbo.OrderSimulation
        SET RowStatus = 0, ApplyF = 0, LastUpdatedTime = GETDATE()
        WHERE OrderNo IN (@0)

        UPDATE pc
        SET pc.RowStatus = 0, pc.LastUpdatedTime = GETDATE()
        FROM dbo.ProspectCustomer pc
        INNER JOIN dbo.OrderSimulation os
        ON os.CustID = pc.CustID
        WHERE os.OrderNo IN (@0)", lsOrdNo);
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
                logger.ErrorFormat("Error occured in {0} - {1}: {2}", "Util", "CheckAllMGOBidExpiry", e.Message.ToString());
            }
        }
        /// <summary>
        /// Conversion from UpdateRowStatusFromOrderNo method
        /// </summary>
        /// <param name="OrderNo"></param>
        public static void SoftDeleteProspect(string OrderNo)
        {
            using( var db = new a2isDBHelper.Database(CONNECTION_STRING_AABMOBILE))
            {
                FollowUp followUp = db.First<FollowUp>("SELECT FollowUpNo, CustID FROM OrderSimulation WHERE OrderNo = @0", OrderNo);

                if (!followUp.Equals(null))
                {
                    db.Execute("UPDATE ProspectCustomer SET RowStatus = 0, LastUpdatedTime = getdate() WHERE CustID = @0", followUp.CustID);
                    db.Execute("UPDATE FollowUp SET RowStatus = 0, LastUpdatedTime = getdate() WHERE FollowUpNo = @0", followUp.FollowUpNo);
                    db.Execute("UPDATE FollowUpHistory SET RowStatus = 0, LastUpdatedTime = getdate() WHERE FollowUpNo = @0", followUp.FollowUpNo);

                    List<string> orderNo = db.Fetch<string>("SELECT OrderNo FROM OrderSimulation WHERE FollowUpNo = @0", followUp.FollowUpNo);
                    for(int i = 0; i < orderNo.Count; i++)
                    {
                        db.Execute("UPDATE OrderSimulation SET RowStatus = 0, LastUpdatedTime = getdate() WHERE OrderNo = @0", orderNo[i]);
                        db.Execute("UPDATE OrderSimulationMV SET RowStatus = 0, LastUpdatedTime = getdate() WHERE OrderNo = @0", orderNo[i]);
                        db.Execute("UPDATE OrderSimulationInterest SET RowStatus = 0, LastUpdatedTime = getdate() WHERE OrderNo = @0", orderNo[i]);
                        db.Execute("UPDATE OrderSimulationCoverage SET RowStatus = 0, LastUpdatedTime = getdate() WHERE OrderNo = @0", orderNo[i]);
                    }
                }
            }
        }

        #endregion

        #region Converter Method

        public static JsonSerializerSettings jsonSerializerSetting()
        {
            JsonSerializerSettings setting = new JsonSerializerSettings();
            setting.DateFormatString = "yyyy-MM-dd HH:mm:ss.fff";
            setting.Converters.Add(BoolConverter.create());
            return setting;
        }

        #endregion

        #region PadRight
       public static String padRight(String value, int n, String changer)
        {
            String result = String.Format("%-" + (n).ToString() + "s", value);
            if (!changer.Equals(" "))
            {
                result = result.Replace(value, "").Replace(" ", changer);
                result = value + result;
            }

            return result;
        }
        #endregion

        #region Convert To Money
       public static String ToThousand(decimal value)
       {
          return value.ToString("N1", CultureInfo.CurrentCulture).Replace(".0","").Replace(",",".");

       }
#endregion
    }
}