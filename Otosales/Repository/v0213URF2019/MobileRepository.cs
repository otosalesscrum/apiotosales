﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.dta;
using Otosales.Models;
using Otosales.Models.v0213URF2019;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Repository.v0213URF2019
{
    public interface IMobileRepository {
        List<string> GetApplicationParametersValue(string ParamName);
    }
    public class MobileRepository : IMobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();
        public static void GenerateOrderSimulation(string salesOfficerID, string custID, string followUpID, string orderID, string PolicyNo, string remarkToSA, int isPSTB)
        {
            try
            {
                //Param
                string query = "";
                string SOBranchID = "";

                //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                query = @"SELECT TOP 1 BranchCode FROM SalesOfficer WHERE SalesOfficerID = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    SOBranchID = db.FirstOrDefault<String>(query, salesOfficerID);
                }

                //Data
                Otosales.Models.vWeb2.CustomerInfo custInfo = new Otosales.Models.vWeb2.CustomerInfo();
                Models.vWeb2.OrderSimulationRenot orderSimulation = new Models.vWeb2.OrderSimulationRenot();
                Models.vWeb2.OrderSimulationMVRenot orderSimulationMV = new Models.vWeb2.OrderSimulationMVRenot();
                List<Models.vWeb2.OrderSimulationInterestRenot> listOSInterest = new List<Models.vWeb2.OrderSimulationInterestRenot>();
                List<Models.vWeb2.OrderSimulationCoverageRenot> listOSCoverage = new List<Models.vWeb2.OrderSimulationCoverageRenot>();

                #region Query Prospect Customer
                query = @"SELECT mc.Name AS Name,
		mc.Email AS Email1,
		mcp.HP AS Phone1,
		mcp.HP_2 AS Phone2,
		mc.Cust_Id AS CustIDAAB,
		CASE WHEN mpr.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		END AS SalesOfficerID,
		rn.Salesman_Dealer_Code AS SalesDealer,
		rn.Dealer_Code AS DealerCode,
		rn.Branch_Id AS BranchCode,
		CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany, 
		isnull(mcp.id_card,'') AS IdentityNo,
		CASE WHEN isnull(mcp.sex , '') = '' THEN '' 
		WHEN isnull(mcp.sex , '') = '1' THEN 'M'
		ELSE 'F' END AS Gender,
		mp.Prospect_Id AS ProspectID,
		mc.Birth_Date AS BirthDate,
		CASE WHEN mc.Cust_Type = 2 THEN mc.Office_Address ELSE mc.Home_Address END AS [Address],
		CASE WHEN mc.Cust_Type = 2 THEN mc.Office_PostCode ELSE mc.Home_PostCode END AS PostCode,
		isnull(mca.AdditionalInfoValue,'') AS CompanyNPWPAddress,
		isnull(mca2.AdditionalInfoValue,'') AS CompanyNPWPNo,
		CASE WHEN coalesce(mca3.AdditionalInfoValue,'') <> ''
        THEN SUBSTRING(mca3.AdditionalInfoValue, 7, 4)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 4, 2)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 1, 2) 
        ELSE '' END  AS CompanyNPWPDate,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mca4.AdditionalInfoValue,'') 
		ELSE '' END AS PICPhone,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mcpi.Name,'') 
		ELSE '' END AS PIC
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_prospect mp WITH ( NOLOCK ) ON mp.cust_id = a.policy_holder_code
LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 	
LEFT JOIN mst_cust_personal mcp WITH ( NOLOCK ) ON mcp.cust_id = a.policy_holder_code
inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.product_code = rn.new_product_code
    left join Mst_Customer_AdditionalInfo mca WITH ( NOLOCK ) ON mca.custid = a.policy_holder_code 
    AND mca.AdditionalCode IN ('NPAD')
    left join Mst_Customer_AdditionalInfo mca2 WITH ( NOLOCK ) ON mca2.custid = a.policy_holder_code 
    AND mca2.AdditionalCode IN ('NPWN')
    left join Mst_Customer_AdditionalInfo mca3 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca3.AdditionalCode IN ('NPDT')
    left join Mst_Customer_AdditionalInfo mca4 WITH ( NOLOCK ) ON mca4.custid = a.policy_holder_code 
    AND mca4.AdditionalCode IN ('MPC1')
	left join Mst_cust_pic mcpi WITH ( NOLOCK ) ON mcpi.cust_id = a.policy_holder_code    
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    custInfo = db.FirstOrDefault<Otosales.Models.vWeb2.CustomerInfo>(query, PolicyNo);
                }

                query = @"SELECT mc.Name AS Name,
        mc.Email AS Email1,
        mcp.HP AS Phone1,
		mcp.HP_2 AS Phone2,
        mc.Cust_Id AS CustIDAAB,
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END  AS SalesOfficerID,
        a.Broker_Code AS DealerCode,
        mc2.Cust_Id AS SalesDealer,
        COALESCE(rm.new_branch_id, a.branch_id) AS BranchCode,
        CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany,
        isnull(mcp.id_card, '') AS IdentityNo,
        CASE WHEN isnull(mcp.sex, '') = '' THEN ''
        WHEN isnull(mcp.sex, '') = '1' THEN 'M'
        ELSE 'F' END AS Gender,
		mc.Prospect_Id AS ProspectID,
		mc.Birth_Date AS BirthDate,
		mc.Home_Address AS [Address],
		mc.Home_PostCode AS PostCode,
		isnull(mca.AdditionalInfoValue,'') AS CompanyNPWPAddress,
		isnull(mca2.AdditionalInfoValue,'') AS CompanyNPWPNo,
		CASE WHEN coalesce(mca3.AdditionalInfoValue,'') <> ''
        THEN SUBSTRING(mca3.AdditionalInfoValue, 7, 4)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 4, 2)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 1, 2) 
        ELSE '' END  AS CompanyNPWPDate,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mca4.AdditionalInfoValue,'') 
		ELSE '' END AS PICPhone,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mcpi.Name,'') 
		ELSE '' END AS PIC
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc WITH(NOLOCK) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_cust_personal mcp WITH ( NOLOCK ) ON mcp.cust_id = a.policy_holder_code
INNER JOIN mst_product mp ON mp.product_code = a.product_code
LEFT JOIN mst_salesman ms WITH(NOLOCK) ON ms.salesman_id = a.Salesman_Id
LEFT JOIN dbo.ComSales_Salesman AS css ON a.Broker_Code = css.Salesman_Code
LEFT JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
LEFT OUTER JOIN renewal_map rm ON mp.product_code = rm.product_code
                                                          AND rm.status = 1
                                                          AND ( rm.old_branch_id = ''
                                                              OR rm.old_branch_id = a.branch_id
                                                              )
                                                          AND ( rm.old_segment_code = ''
                                                              OR rm.old_segment_code = a.segment_code
                                                              )

    left join Mst_Customer_AdditionalInfo mca WITH ( NOLOCK ) ON mca.custid = a.policy_holder_code 
    AND mca.AdditionalCode IN ('NPAD')
    left join Mst_Customer_AdditionalInfo mca2 WITH ( NOLOCK ) ON mca2.custid = a.policy_holder_code 
    AND mca2.AdditionalCode IN ('NPWN')
    left join Mst_Customer_AdditionalInfo mca3 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca3.AdditionalCode IN ('NPDT')
    left join Mst_Customer_AdditionalInfo mca4 WITH ( NOLOCK ) ON mca4.custid = a.policy_holder_code 
    AND mca4.AdditionalCode IN ('MPC1')
	left join Mst_cust_pic mcpi WITH ( NOLOCK ) ON mcpi.cust_id = a.policy_holder_code
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if (custInfo == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        custInfo = db.FirstOrDefault<Otosales.Models.vWeb2.CustomerInfo>(query, PolicyNo);
                    }
                }


                #region Sales Dealer dan Dealer Code
                int SalesmanCode = 0;
                int DealerCode = 0;
                int salesmancode = 0;
                int dealercode = 0;
                string querySalesman = "SELECT SalesmanCode FROM dbo.SalesmanDealer WHERE CustID = @0";
                string queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE CustID = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    if (custInfo.SalesDealer != null)
                        salesmancode = db.First<int>(querySalesman, custInfo.SalesDealer);
                    else
                        salesmancode = 0;
                    int value = 0;
                    if (custInfo.DealerCode != null && int.TryParse(custInfo.DealerCode, out value))
                        dealercode = db.First<int>(queryDealer, custInfo.DealerCode);
                    else
                        dealercode = 0;
                }

                SalesmanCode = Convert.ToInt32(salesmancode);
                DealerCode = Convert.ToInt32(dealercode);

                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {

                    #region Query Insert Prospect Customer
                    query = @"
INSERT INTO ProspectCustomer([CustID]
      , [Name] --*
      , [Phone1] --*
      , [Phone2]
      , [Email1] --*
      , [Email2]
      , [CustIDAAB] --*
      , [SalesOfficerID] --*
      , [DealerCode] --*
      , [SalesDealer] --*
      , [BranchCode] --*
      , [isCompany] --*
      , [EntryDate]
      , [LastUpdatedTime]
      , [RowStatus]
      , [IdentityNo]
      , [CustGender]      
      , [ProspectID]
      , [CustBirthDay]
      , [CustAddress]
      , [PostalCode])
      SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1,@12,@13,@14,@15,@16,@17";
                    #endregion

                    //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                    db.Execute(query, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2, custInfo.Email1
                        , "", custInfo.CustIDAAB, salesOfficerID, DealerCode, SalesmanCode, SOBranchID
                        , custInfo.isCompany, custInfo.IdentityNo, custInfo.Gender, custInfo.ProspectID, custInfo.BirthDate
                        , custInfo.Address, custInfo.PostCode);


                    #region Query Insert Prospect Company
                    query = @"
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName] --*
      ,[FollowUpNo] --*
      ,[Email] --*
      ,[ModifiedDate]
      ,[RowStatus]
      ,[NPWPno]
      ,[NPWPdate]
      ,[NPWPaddress]
      ,[OfficeAddress]
      ,[PostalCode]
      ,[PICPhoneNo]
      ,[PICname]) VALUES(@0,@1,@2,@3,GETDATE(),1,@4,@5,@6,@7,@8,@9,@10)";
                    #endregion

                    if (custInfo.isCompany == 1)
                        db.Execute(query, custID, custInfo.Name, followUpID, custInfo.Email1
                            , custInfo.CompanyNPWPNo, custInfo.CompanyNPWPDate, custInfo.CompanyNPWPAddress, custInfo.Address, custInfo.PostCode
                            , custInfo.PICPhone, custInfo.PIC);


                    #region Query Insert Follow Up
                    query = @"
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[RemarkToSA]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsRenewal]) 
SELECT @0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,1";
                    #endregion

                    //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                    db.Execute(query, followUpID, 0, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2
                        , salesOfficerID, custInfo.Name, 1, 1, remarkToSA, SOBranchID);

                }

                #region Query Order Simulation
                //Update 105 Hotfix - ganti period policy ke range 1 tahun
                query = @"SELECT 
		NULL AS NewPolicyId,
		rn.New_Policy_No AS NewPolicyNo,
		rn.VANumber AS VANumber,
		'' AS QuotationNo,
		0 AS MultiYearF,
		1 AS YearCoverage,
		CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
		CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
		rn.Branch_Id AS BranchCode,
		CASE WHEN mpr.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		END AS SalesOfficerID,
		'' AS PhoneSales,
		rn.Dealer_Code AS DealerCode,
		rn.Salesman_Dealer_Code AS SalesDealer,
		'GARDAOTO' AS ProductTypeCode,
		rp.Policy_Fee AS AdminFee,
		rp.net_premium AS TotalPremium,
		mc.Home_Phone1 AS Phone1,
		mc.Home_Phone2 AS Phone2,
		mc.Email AS Email1,
		'' AS Email2,
		0 AS SendStatus,
		CASE WHEN mp.cob_id = '404' THEN 2 
		WHEN PDT.product_code is not null THEN 3 
        WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
        ELSE 1 END AS InsuranceType,
		1 AS ApplyF,
		0 AS SendF,
		(SELECT top 1 Interest_no from Ren_Dtl_Interest where policy_no = @0) AS LastInterestNo,
		(SELECT top 1 coverage_no from Ren_Dtl_Coverage where policy_no = @0) AS LastCoverageNo,
		rn.New_Product_code AS ProductCode,
        rn.Due_Date AS PeriodFrom,
        DATEADD(YEAR,1,rn.Due_Date) AS PeriodTo,
        rn.segment_code AS SegmentCode,
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 
LEFT JOIN Ren_Dtl_Coverage rdc1 WITH ( NOLOCK ) ON  rdc1.policy_no = rn.policy_no
AND rdc1.interest_id = 'CASCO' AND rdc1.coverage_id = 'TLO'
LEFT JOIN Ren_Dtl_Coverage rdc2 WITH ( NOLOCK ) ON  rdc2.policy_no = rn.policy_no
AND rdc2.interest_id = 'CASCO' AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN premium rp ON  rp.policy_id = a.policy_id
INNER JOIN mst_product mp on mp.product_code = rn.old_product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.new_product_code = rn.new_product_code
left join dtl_address da on da.policy_id = a.policy_id AND da. address_type ='DELIVR'
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo);
                }

                query = @"SELECT TOP 1
NULL AS NewPolicyId,
NULL AS NewPolicyNo,
NULL AS VANumber,
'' AS QuotationNo,
0 AS MultiYearF,
1 AS YearCoverage,
CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
a.Branch_Id AS BranchCode,
CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END  AS SalesOfficerID,
'' AS PhoneSales,
a.Broker_Code AS DealerCode,
        mc.Cust_Id AS SalesDealer,
'GARDAOTO' AS ProductTypeCode,
rp.Policy_Fee AS AdminFee,
rp.net_premium AS TotalPremium,
mc.Home_Phone1 AS Phone1,
mc.Home_Phone2 AS Phone2,
mc.Email AS Email1,
'' AS Email2,
0 AS SendStatus,
CASE WHEN mp.cob_id = '404' THEN 2 
WHEN PDT.product_code is not null THEN 3 
WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
ELSE 1 END AS InsuranceType,
1 AS ApplyF,
0 AS SendF,
(SELECT max(Interest_no) from Dtl_Interest where policy_id = a.policy_id) AS LastInterestNo,
(SELECT max(coverage_no) from Dtl_Coverage where policy_id = a.policy_id) AS LastCoverageNo,
a.Product_Code [ProductCode],
        a.Period_To AS PeriodFrom,
        DATEADD(YEAR,1,a.Period_To) AS PeriodTo,
a.Segment_Code [SegmentCode],
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc  ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms ON ms.salesman_id = a.Salesman_Id 
LEFT JOIN dbo.ComSales_Salesman AS css ON a.Broker_Code = css.Salesman_Code
LEFT JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
LEFT JOIN dtl_interest di ON di.policy_id = a.policy_id AND di.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc1 ON  rdc1.policy_id = a.policy_id 
AND rdc1.interest_no = di.interest_no AND rdc1.coverage_id = 'TLO'
LEFT JOIN dtl_interest di2 ON di2.policy_id = a.policy_id AND di2.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc2 ON  rdc2.policy_id = a.policy_id
AND rdc2.interest_no = di2.interest_no AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN premium rp ON  rp.policy_id = a.policy_id
INNER JOIN mst_product mp on mp.product_code = a.product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
left join dtl_address da on da.policy_id = a.policy_id AND da. address_type ='DELIVR'
WHERE a.policy_no = @0
AND a.Status = 'A'";

                #endregion

                if (orderSimulation == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo);
                    }
                }


                #region Query Insert Order Simulation
                query = @"
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[OldPolicyNo]
      ,[PolicyNo]
      ,[PolicyID]
      ,[VANumber]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[ProductCode]
      ,[SegmentCode]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
	  ,IsPolicyIssuedBeforePaying
        ) 
  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10, @11,@12,
        @13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    orderSimulation.SalesOfficerID = salesOfficerID;
                    //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                    db.Execute(query, orderID, custID, followUpID, orderSimulation.QuotationNo, orderSimulation.MultiYearF
                        , orderSimulation.YearCoverage, orderSimulation.TLOPeriod, orderSimulation.ComprePeriod, SOBranchID
                        , salesOfficerID, orderSimulation.PhoneSales, DealerCode, SalesmanCode
                        , orderSimulation.ProductTypeCode, orderSimulation.AdminFee, orderSimulation.TotalPremium, orderSimulation.Phone1
                        , orderSimulation.Phone2, orderSimulation.Email1, orderSimulation.Email2, orderSimulation.SendStatus
                        , orderSimulation.InsuranceType, orderSimulation.ApplyF, orderSimulation.SendF, orderSimulation.LastInterestNo
                        , orderSimulation.LastCoverageNo, PolicyNo, orderSimulation.NewPolicyNo, orderSimulation.NewPolicyId
                        , orderSimulation.VANumber, orderSimulation.PeriodFrom, orderSimulation.PeriodTo, orderSimulation.ProductCode
                        , orderSimulation.SegmentCode, orderSimulation.PolicyDeliverAddress, orderSimulation.PolicyDeliverPostalCode
                        , orderSimulation.PolicyDeliveryType, orderSimulation.PolicyDeliveryName
                        , isPSTB == 1 ? true : false);
                }

                #region Query Order Simulation MV
                query = @"SELECT rdm.Object_No AS ObjectNo,
		'GARDAOTO' AS ProductTypeCode,
		rdm.Vehicle_Code AS VehicleCode,
		rdm.Brand_Code AS BrandCode,
		rdm.Model_Code AS ModelCode,
		rdm.Series  AS Series,
		rdm.Vehicle_Type AS [Type],
		rdm.Sitting_Capacity AS Sitting,
		rdm.Mfg_Year AS [Year],
		rdm.Geo_Area_Code AS CityCode,
		rdm.Usage_Code AS UsageCode,
		rdi.Sum_Insured AS SumInsured,
		ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Ren_Dtl_Non_Standard_Accessories] where policy_no = @0),0) AS AccessSI,
		rdm.Registration_Number AS RegistrationNumber,
		rdm.Engine_Number AS EngineNumber,
		rdm.Chasis_Number AS ChasisNumber,
		rdm.New_Car AS IsNew,
        rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Interest rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.policy_no 
AND rdi.Object_no = rdm.object_no
AND rdi.interest_id = 'CASCO'
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);
                }

                query = @"SELECT rdm.Object_No AS ObjectNo,
'GARDAOTO' AS ProductTypeCode,
rdm.Vehicle_Code AS VehicleCode,
rdm.Brand_Code AS BrandCode,
rdm.Model_Code AS ModelCode,
rdm.Series  AS Series,
rdm.Vehicle_Type AS [Type],
rdm.Sitting_Capacity AS Sitting,
rdm.Mfg_Year AS [Year],
rdm.Geo_Area_Code AS CityCode,
rdm.Usage_Code AS UsageCode,
rdm.Market_Price AS SumInsured,
ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Dtl_Non_Standard_Accessories] where policy_id = a.policy_id),0) AS AccessSI,
rdm.Registration_Number AS RegistrationNumber,
rdm.Engine_Number AS EngineNumber,
rdm.Chasis_Number AS ChasisNumber,
rdm.New_Car AS IsNew,
rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if (orderSimulationMV == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);
                    }
                }


                #region Query Insert Order Simulation MV
                query = @"DELETE FROM OrdersimulationMV WHERE OrderNo=@0
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]
      ,[ColorOnBPKB]) 
  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17,@18";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, orderID, orderSimulationMV.ObjectNo, orderSimulationMV.ProductTypeCode
                        , orderSimulationMV.VehicleCode, orderSimulationMV.BrandCode, orderSimulationMV.ModelCode
                        , orderSimulationMV.Series, orderSimulationMV.Type, orderSimulationMV.Sitting
                        , orderSimulationMV.Year, orderSimulationMV.CityCode, orderSimulationMV.UsageCode
                        , orderSimulationMV.SumInsured, orderSimulationMV.AccessSI, orderSimulationMV.RegistrationNumber
                        , orderSimulationMV.EngineNumber, orderSimulationMV.ChasisNumber, orderSimulationMV.IsNew, orderSimulationMV.Color);
                }

                #region Query Order Simulation Interest
                //Update 105 Hotfix - ganti period policy ke range 1 tahun
                query = @"SELECT rdi.Object_No AS ObjectNo,
		rdi.Interest_No AS InterestNo,
		rdi.Interest_Id AS InterestID,
		1 AS [Year],
		(SELECT SUM(rdc.net_premium) FROM [dbo].[Ren_Dtl_Coverage] rdc WHERE rdc.policy_no = @0 
		and rdc.interest_no = rdi.Interest_No) AS Premium,
        rn.Due_Date AS PeriodFrom,
        DATEADD(YEAR,1,rn.Due_Date) AS PeriodTo,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    listOSInterest = db.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(query, PolicyNo);
                }

                query = @"SELECT rdi.Object_No AS ObjectNo,
rdi.Interest_No AS InterestNo,
rdi.Interest_Id AS InterestID,
1 AS [Year],
(SELECT SUM(rdc.net_premium) FROM [dbo].[Dtl_Coverage] rdc WHERE rdc.policy_id = a.policy_id 
and rdc.interest_no = rdi.Interest_No) AS Premium,
        a.Period_To AS PeriodFrom,
        DATEADD(YEAR,1,a.Period_To) AS PeriodTo,
rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a 
INNER JOIN [Dtl_Interest] rdi ON rdi.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if (listOSInterest == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        listOSInterest = db.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(query, PolicyNo);
                    }
                }

                #region Insert Order Simulation Interest
                query = @"
DELETE FROM OrderSimulationInterest WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2
INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1,@8";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationInterestRenot osI in listOSInterest)
                    {
                        db.Execute(query, orderID, osI.ObjectNo, osI.InterestNo, osI.InterestID, osI.Year
                            , osI.Premium, osI.PeriodFrom, osI.PeriodTo, osI.DeductibleCode);
                    }
                }

                #region Query Order Simulation Coverage
                //Update 105 Hotfix - ganti period policy ke range 1 tahun
                query = @"SELECT rdc.Object_No AS ObjectNo,
		rdc.Interest_No AS InterestNo,
		rdc.Coverage_no AS CoverageNo,
		rdc.Coverage_ID AS CoverageID,
		rdc.Rate AS Rate,
		rdi.sum_insured AS SumInsured,
		rdc.Loading AS LoadingRate,
		(rdc.Loading*rdc.net_premium)/100 AS Loading,
		rdc.net_premium AS Premium,
        rn.Due_Date AS BeginDate,
        DATEADD(YEAR,1,rn.Due_Date) AS EndDate,
		CASE WHEN rdc.net_premium = 0 OR rdc.net_premium is null THEN 1 ELSE 0 END AS IsBundling,
		100 AS EntryPct,
		DATEDIFF(day, a.Period_To, DATEADD(YEAR, 1,a.Period_To)) AS Ndays,
		rdc.Excess_Rate AS ExcessRate,
		1 AS CalcMethod,
		rdc.Cover_Premium AS CoverPremium,
		rdc.Gross_Premium AS GrossPremium,
		rdc.Max_SI AS MaxSI,
		rdc.Net1 AS Net1,
		rdc.Net2 AS Net2,
		rdc.Net3 AS Net3,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Coverage] rdc WITH ( NOLOCK ) ON rdc.policy_no = rn.Policy_No AND rdi.interest_no = rdc.interest_no
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    listOSCoverage = db.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(query, PolicyNo);
                }

                query = @"SELECT rdc.Object_No AS ObjectNo,
rdc.Interest_No AS InterestNo,
rdc.Coverage_no AS CoverageNo,
rdc.Coverage_Id AS CoverageID,
rdc.Rate AS Rate,
rdc.Sum_Insured  AS SumInsured,
rdc.Loading AS LoadingRate,
(rdc.Loading*rdc.net_premium)/100 AS Loading,
rdc.net_premium AS Premium,
        a.Period_To AS BeginDate,
        DATEADD(YEAR,1,a.Period_To) AS EndDate,
CASE WHEN rdc.net_premium = 0 OR rdc.net_premium is null THEN 1 ELSE 0 END AS IsBundling,
rdc.Entry_Pct [EntryPct],
rdc.NDays [NDays],
Net1,
Net2,
Net3,
Gross_Premium [GrossPremium],
Cover_Premium [CoverPremium],
rdc.Calc_Method [CalcMethod],
Excess_Rate [ExcessRate],
Max_si [MaxSi],
Deductible_Code [DeductibleCode]
FROM Policy AS a 
INNER JOIN [Dtl_Coverage] rdc ON rdc.Policy_id = a.Policy_id
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.Policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if (listOSCoverage == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        listOSCoverage = db.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(query, PolicyNo);
                    }
                }


                #region Insert Order Simulation Coverage
                query = @"DELETE FROM OrderSimulationCoverage WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2 AND CoverageNo = @3
    INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@22,@23,GETDATE(),1,@10,
            @11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationCoverageRenot osC in listOSCoverage)
                    {
                        db.Execute(query, orderID, osC.ObjectNo, osC.InterestNo, osC.CoverageNo
                            , osC.CoverageID, osC.Rate, osC.SumInsured, osC.LoadingRate, osC.Loading
                            , osC.Premium, osC.IsBundling, osC.EntryPct, osC.NDays, osC.ExcessRate
                            , osC.CalcMethod, osC.CoverPremium, osC.GrossPremium, osC.MaxSi
                            , osC.Net1, osC.Net2, osC.Net3, osC.DeductibleCode, osC.BeginDate, osC.EndDate);
                    }
                }

                query = @"DELETE FROM [dbo].[ImageData] WHERE DeviceID = @0
                INSERT INTO [dbo].[ImageData]
                ([ImageID],[DeviceID],[SalesOfficerID],[PathFile]
                ,[EntryDate],[LastUpdatedTime],[RowStatus],[Data]
                ,[ThumbnailData],[CoreImage_ID])
                SELECT tid.ImageID,tid.FollowUpNo,tid.SalesOfficerID,tid.PathFile,
                GETDATE(),GETDATE(),1,tid.Data,tid.ThumbnailData,NULL
                FROM dbo.TempImageData tid
                WHERE tid.FollowUpNo = @0";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, followUpID);
                }

                /*FORMRENEWAL
                RENEWALNOTICE
                BUKTIBAYAR
                KTP
                STNK
                SURVEY
                DOCNSA1
                DOCNSA2*/

                UpdateImageFollowUp(followUpID);

                query = @"UPDATE FollowUp
                          SET [FollowUpStatus] = 2
                          ,[FollowUpInfo] = 61
                           WHERE FollowUpNo = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, followUpID);
                }


                #region Update Status Exclude Renewal Policy
                query = @"UPDATE [Excluded_Renewal_Policy] SET RowStatus=1 WHERE Policy_No=@0";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    db.Execute(query, PolicyNo);

                }

            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static OtosalesAPIResult detailApprovalTsiLimit(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();

            #region Query
            string qGetOrderNum = @";SELECT Order_No FROM Mst_Order_Mobile WHERE Order_ID = @0";
            string qGetFromGen5SP = @";EXEC sp_Execute_Order_Analysis_0069URF2018 @0";
            #endregion

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    string orderNumber = db.FirstOrDefault<string>(qGetOrderNum, OrderID);
                    if (orderNumber.Equals("") || orderNumber == null)
                    { // untuk order bukan dari otosales
                        orderNumber = OrderNumber;
                    }

                    dynamic data = db.FirstOrDefault<dynamic>(qGetFromGen5SP, OrderNumber);
                    if (data == null)
                    {
                        resp.Status = false;
                        resp.Message = "Order Not Exists!";
                        resp.ResponseCode = "6661";
                    }
                    else
                    {
                        resp.Data = data.Info_Text;
                        resp.Status = true;
                        resp.Message = "Success";
                        resp.ResponseCode = "1";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                resp.Status = false;
                resp.Message = ex.Message.ToString();
                resp.ResponseCode = "666";
            }

            return resp;
        }

        public static OtosalesAPIResult detailApprovalAdjustment(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    // TODO LCY : this query still has dummy value for
                    // isPTSB, Remarks, SendToBranch, SendToName, SendToAddress, SendToPostal ==> on different API
                    string query = @";SELECT IIF(','+mom.Reason+',' like '%,PSTB,%',1, 0) as isPTSB, COALESCE(os.Remarks, '') as Remarks, '' as SendToBranch, '' as SendToName, 
                    '' as SendToAddress, '' as SendToPostal, msom.Order_ID [Order_ID],
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    , so.Name MarketingName, mb.Nama BranchName
					FROM [dbo].[Mst_Order] mo
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN [dbo].[Mst_Order_Mobile_Approval] mom ON mom.Order_No = mo.Order_No AND mom.ApprovalType = 'ADJUST'
                    LEFT JOIN BeyondMoss.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
                    LEFT JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = mo.Salesman_Id
					LEFT JOIN dbo.Mst_Branch mb ON mb.Branch_id = mo.Branch_Id
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    AND mo.Order_No = @0
					--AND msom.Order_ID = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY mo.Order_No, msom.Order_ID, mom.Reason, os.Remarks, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    ,so.Name, mb.Nama
					ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC , Order_ID DESC";

                    resp.Status = true;
                    resp.Message = "Success from detailApprovalAdjustment";
                    //resp.Data = db.FirstOrDefault<dynamic>(query, OrderID);
                    resp.Data = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
                //return Json(new { status = false, message = e.Message, data = "null" });
            }

            return resp;
        }

        public static OtosalesAPIResult detailApprovalComission(string OrderID, string Role, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            switch (Role)
            {
                case "AO":
                    {
                        return detailComissionAO(OrderID, OrderNumber);
                    }
                case "MGO":
                    {
                        return detailComissionMGO(OrderID, OrderNumber);
                    }
                default:
                    {
                        throw new Exception("Invalid User!!!");
                    }
            }
        }

        public static OtosalesAPIResult detailComissionMGO(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    #region Query Select Data
                    string query = @";SELECT X.AgentCode as AgentCode, x.AgentName as AgentName, X.UplinerCode as UplinerCode, x.UplinerName as UplinerName,
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(msom.Order_ID)) as OrderID, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    , so.Name MarketingName, mb.Nama BranchName
					FROM [dbo].[Mst_Order] mo 
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN  ( SELECT b.Cust_Id AS CustomerID,
								a.Client_Code AS ClientCode,
								b.Name AS AgentName,
								a.Client_Code AS AgentCode,
                               a.Upliner_Client_Code AS UplinerCode ,
                               RTRIM(d.Name) AS UplinerName,
                               e.Cust_Id AS LeaderCustomerID,
                               a.Leader_Client_Code AS LeaderCode ,                  
                               RTRIM(f.Name) AS LeaderName                    
                                     FROM      dtl_cust_type a WITH ( NOLOCK )
                                               INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                               LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                               LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                               LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                               LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                     WHERE     a.Client_Type = 'AGENT') X ON X.CustomerID=mo.Broker_Code OR X.ClientCode=mo.Broker_Code
                    LEFT JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
					LEFT JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = mo.Salesman_Id
					LEFT JOIN dbo.Mst_Branch mb ON mb.Branch_id = mo.Branch_Id
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    --AND msom.Order_ID = @0
                    AND mo.Order_No = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY AgentCode, AgentName, UplinerCode, UplinerName, mo.Order_No, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Order_ID, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    , so.Name, mb.Nama
					ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC, msom.Order_ID DESC";
                    #endregion
                    // 002 end
                    dynamic result = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    if (result == null)
                    {
                        resp.Status = false;
                        resp.Message = "Data Not Found!";
                        resp.ResponseCode = "6661";
                    }
                    else
                    {
                        resp.Status = true;
                        resp.Message = "Success from detailComissionMGO";
                        resp.ResponseCode = "1";
                    }
                    resp.Data = result;
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
            }
            return resp;
        }

        public static OtosalesAPIResult detailComissionAO(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            OtosalesAPIResult resp = new OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    #region Query Select Data
                    string query = @";SELECT X.AgentCode as AgentCode, x.AgentName as AgentName, X.UplinerCode as UplinerCode, x.UplinerName as UplinerName,
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(msom.Order_ID)) as OrderID, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus, 
                    so.Name MarketingName, mb.Nama BranchName
					FROM [dbo].[Mst_Order] mo 
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN  ( SELECT b.Cust_Id AS CustomerID,
								a.Client_Code AS ClientCode,
								b.Name AS AgentName,
								a.Client_Code AS AgentCode,
                               a.Upliner_Client_Code AS UplinerCode ,
                               RTRIM(d.Name) AS UplinerName,
                               e.Cust_Id AS LeaderCustomerID,
                               a.Leader_Client_Code AS LeaderCode ,                  
                               RTRIM(f.Name) AS LeaderName                    
                                     FROM      dtl_cust_type a WITH ( NOLOCK )
                                               INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                               LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                               LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                               LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                               LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                     WHERE     a.Client_Type = 'AGENT') X ON X.CustomerID=mo.Broker_Code OR X.ClientCode=mo.Broker_Code
                    LEFT JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
					LEFT JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = mo.Salesman_Id
					LEFT JOIN dbo.Mst_Branch mb ON mb.Branch_id = mo.Branch_Id
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    --AND msom.Order_ID = @0
                    AND mo.Order_No = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY AgentCode, AgentName, UplinerCode, UplinerName, mo.Order_No, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Order_ID, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.NAME
					, so.Name, mb.Nama 
                    ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC, msom.Order_ID DESC";
                    #endregion

                    dynamic result = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    if (result == null)
                    {
                        resp.Status = false;
                        resp.Message = "Data Not Found!";
                        resp.ResponseCode = "6661";
                    }
                    else
                    {
                        resp.Status = true;
                        resp.Message = "Success from detailComissionAO";
                        resp.ResponseCode = "1";
                    }
                    resp.Data = result;
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
                //return Json(new { status = false, message = e.Message, data = "null" });
            }
            return resp;
        }

        public static bool loggingOtosalesUI(string ErrorInfo, string UserLogin)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string qLogging = @";INSERT INTO GardaMobile.Otosales.ErrorLogUI (TimeTransition, Information, userLogin)
                                        VALUES (GETDATE(), @0, @1)";

                    var temp = db.Execute(qLogging, ErrorInfo, UserLogin);

                    return true;
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                return false;
            }
        }

        public static Otosales.Models.vWeb2.PaymentInfo GetPaymentInfo(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var BYNDdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
            List<dynamic> listDyn = new List<dynamic>();
            Otosales.Models.vWeb2.PaymentInfo pf = new Otosales.Models.vWeb2.PaymentInfo();
            string VaNumber = "";

            try
            {
                query = @";IF EXISTS(SELECT * FROM dbo.OrderSimulation os
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						WHERE os.ApplyF = 1 AND f.CustID = @0 AND f.FollowUpNo = @1)
						BEGIN
							SELECT os.VANumber, f.BuktiBayar AS BuktiBayar, 
							id.Data AS BuktiBayarData, os.PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data
							FROM dbo.OrderSimulation os 
							INNER JOIN dbo.FollowUp f 
							ON f.FollowUpNo = os.FollowUpNo
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE f.CustID = @0 AND f.FollowUpNo = @1 
							AND f.RowStatus = 1
						END
						ELSE
						BEGIN
							SELECT '' AS VANumber, BuktiBayar, 
                            id.Data AS BuktiBayarData, '' AS PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data							 
							FROM dbo.FollowUp f 
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE CustID = @0 AND FollowUpNo = @1
							AND f.RowStatus = 1
						END";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    query = @"SELECT OrderNo,COALESCE(ProductCode,'') AS ProductCode,isCompany,os.SalesOfficerID,os.PolicyOrderNo, VANumber, os.PolicyNo
                        ,CAST(COALESCE(f.IsRenewal,0) AS BIT) IsRenewal
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        INNER JOIN dbo.ProspectCustomer ps
                        ON ps.CustID = f.CustID
                        WHERE os.CustID = @0 
                        AND os.FollowUpNo = @1
                        AND os.ApplyF = 1";
                    List<dynamic> ordData = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                    string qUpdateAAB2000VA = @";DECLARE @@ID BIGINT
DECLARE @@Date DATETIME
SELECT @@ID=AAB2000VirtualAccountID, @@Date=os.EntryDate 
FROM Finance.AAB2000VirtualAccount a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNo
WHERE VirtualAccountNo = @0 AND a.{0} = @1 AND a.RowStatus = 0

UPDATE Finance.AAB2000VirtualAccount 
SET RowStatus = 1
WHERE AAB2000VirtualAccountID = @@ID";
                    if (ordData.Count > 0)
                    {
                        if (Convert.ToBoolean(ordData.First().isCompany))
                        {
                            query = @"SELECT a.branch_id ,
                                            a.accounting_code_sun ,
                                            b.accountno ,
                                            b.bankdescription ,
                                            b.name ,
                                            b.branchdescription
                                    FROM   mst_branch AS a WITH ( NOLOCK )
                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                            List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                            if (lists.Count > 0)
                            {
                                pf.RekeningPenampung = lists.First().accountno;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ordData.First().ProductCode))
                            {
                                query = @"select CAST(COALESCE(IsAllowVA,0) AS BIT) IsAllowVA, Biz_type from mst_product where Product_Code = @0";
                                List<dynamic> listBizType = AABdb.Fetch<dynamic>(query, ordData.First().ProductCode);
                                if (listBizType.Count > 0)
                                {
                                    int b = 0;
                                    b = listBizType.First().Biz_type;
                                    query = @"Select Salesman_Id,mb.Branch_id From Mst_Salesman ms
                                        INNER JOIN dbo.Mst_Branch mb
                                        ON mb.Branch_id = ms.Branch_Id
                                        Where User_id_otosales= @0 
                                        and ms.status=1 AND mb.Biz_type = @1";
                                    string qGetIsPayerCompany = @"SELECT CAST(COALESCE(IsPayerCompany,0) AS BIT) FROM dbo.ProspectCustomer WHERE CustID = @0";
                                    bool IsPayerCompany = db.ExecuteScalar<bool>(qGetIsPayerCompany, CustID);
                                    if (!listBizType.First().IsAllowVA || IsPayerCompany)
                                    {
                                        if (b == 2)
                                        {
                                            pf.RekeningPenampung = AppParameterAccess.GetApplicationParametersValue("REKPENAMPUNGSYARIAH").First();
                                        }
                                        else
                                        {
                                            query = @"SELECT a.branch_id ,
                                                            a.accounting_code_sun ,
                                                            b.accountno ,
                                                            b.bankdescription ,
                                                            b.name ,
                                                            b.branchdescription
                                                        FROM mst_branch AS a WITH ( NOLOCK )
                                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                                            List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                                            if (lists.Count > 0)
                                            {
                                                pf.RekeningPenampung = lists.First().accountno;
                                            }
                                        }
                                        query = @"SELECT OrderNo,VANumber,OldPolicyNo, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal
                                                FROM dbo.OrderSimulation os
                                                INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                WHERE os.CustID = @0 AND os.FollowUpNo = @1";
                                        List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                        if (ordData.Count > 0)
                                        {
                                            string VaNo = Convert.ToString(ordDt.First().VANumber);
                                            if (!string.IsNullOrEmpty(VaNo))
                                            {
                                                string trnsDate = "";
                                                string exprDate = "";

                                                string qGetTransDate = @"SELECT EntryDate StartDate, DATEADD(DAY,CAST(ParamValue AS INT),EntryDate) EndDate FROM dbo.OrderSimulation os
	                                                                    INNER JOIN dbo.ApplicationParameters ap ON 1=1 AND ParamName = 'END-CHECKVAPAYMENT'
	                                                                    WHERE OrderNo = @0";
                                                List<dynamic> lsDate = db.Fetch<dynamic>(qGetTransDate, ordData.First().OrderNo);
                                                if (lsDate.Count > 0)
                                                {
                                                    trnsDate = lsDate.First().StartDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                    exprDate = lsDate.First().EndDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                    string cSQLGetTotalBayarVA = @";WITH cte
                                                                                        AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                                                    a.VI_TraceNo
                                                                                            FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                                                            WHERE    b.VI_VANumber IN (
                                                                                                        SELECT  value
                                                                                                        FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                                                        )
                                                                                        --AND a.PermataVirtualAccountPaymentSourceCode = '02'
		                                                                                AND a.VI_TransactionDate BETWEEN @1 AND @2
                                                                                            )
                                                                                SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                                                        SUM(pvas.VI_Amount) AS TotalBayar ,
                                                                                        MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                                                        MAX(pva.BillAmount) AS TotalBill
                                                                                FROM    cte
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                                                            AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                                                                WHERE   IsReversal = 0";
                                                    List<dynamic> ls = BYNDdb.Fetch<dynamic>(cSQLGetTotalBayarVA, VaNo, trnsDate, exprDate);
                                                    if (ls.Count > 0)
                                                    {
                                                        decimal TotalBayar = ls.First().TotalBayar == null ? 0 : Convert.ToDecimal(ls.First().TotalBayar);
                                                        if (TotalBayar == 0)
                                                        {
                                                            if (!ordDt.First().IsRenewal)
                                                            {
                                                                BYNDdb.Execute(string.Format(qUpdateAAB2000VA, "OrderNo")
                                                                    , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                                //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                            }
                                                            else
                                                            {
                                                                BYNDdb.Execute(string.Format(qUpdateAAB2000VA, "PolicyNo")
                                                                , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                            }
                                                            listDyn.First().VANumber = "";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!ordDt.First().IsRenewal)
                                                        {
                                                            BYNDdb.Execute(string.Format(qUpdateAAB2000VA, "OrderNo")
                                                                , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                            //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                        }
                                                        else
                                                        {
                                                            BYNDdb.Execute(string.Format(qUpdateAAB2000VA, "PolicyNo")
                                                                , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                        }
                                                        listDyn.First().VANumber = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!ordData.First().IsRenewal)
                                        {
                                            List<dynamic> lists = BYNDdb.Fetch<dynamic>("SELECT RowStatus FROM Finance.AAB2000VirtualAccount WHERE VirtualAccountNo = @0 AND OrderNo = @1"
                                                , ordData.First().VANumber, ordData.First().PolicyOrderNo == null ? "" : ordData.First().PolicyOrderNo);
                                            if (lists.Count > 0)
                                            {
                                                DateTime dt = BYNDdb.ExecuteScalar<DateTime>("SELECT CreatedDate FROM Finance.AAB2000VirtualAccount WHERE VirtualAccountNo = @0 AND OrderNo = @1"
                                                , ordData.First().VANumber, ordData.First().PolicyOrderNo == null ? "" : ordData.First().PolicyOrderNo);
                                                if (dt.Date > DateTime.Now.Date.AddDays(-7))
                                                {
                                                    BYNDdb.Execute(string.Format(@";DECLARE @@ID BIGINT
DECLARE @@Date DATETIME
SELECT @@ID=AAB2000VirtualAccountID, @@Date=os.EntryDate 
FROM Finance.AAB2000VirtualAccount a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNo
WHERE VirtualAccountNo = @0 AND a.{0} = @1 AND a.RowStatus = 1

UPDATE Finance.AAB2000VirtualAccount 
SET RowStatus = 0
WHERE AAB2000VirtualAccountID = @@ID", "OrderNo")
                                                        , ordData.First().VANumber, ordData.First().PolicyOrderNo == null ? "" : ordData.First().PolicyOrderNo);
                                                }
                                            }
                                            //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                        }
                                        else
                                        {
                                            BYNDdb.Execute(string.Format(@";DECLARE @@ID BIGINT
DECLARE @@Date DATETIME
SELECT @@ID=AAB2000VirtualAccountID, @@Date=os.EntryDate 
FROM Finance.AAB2000VirtualAccount a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNo
WHERE VirtualAccountNo = @0 AND a.{0} = @1 AND a.RowStatus = 1

UPDATE Finance.AAB2000VirtualAccount 
SET RowStatus = 0
WHERE AAB2000VirtualAccountID = @@ID", "PolicyNo")
    , ordData.First().VANumber, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                        }
                                        query = @"SELECT OrderNo,VANumber,OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1";
                                        List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                        if (ordData.Count > 0)
                                        {
                                            bool isGenerate = true;
                                            VaNumber = Convert.ToString(ordDt.First().VANumber);
                                            if (string.IsNullOrEmpty(VaNumber))
                                            {
                                                if (!string.IsNullOrEmpty(ordDt.First().OldPolicyNo))
                                                {
                                                    query = @";SELECT CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal FROM dbo.OrderSimulation os 
                                                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                            WHERE os.OrderNo = @0";
                                                    List<dynamic> lsRen = db.Fetch<dynamic>(query, ordDt.First().OrderNo);
                                                    if (lsRen.Count > 0)
                                                    {
                                                        isGenerate = !lsRen.First().IsRenewal;
                                                    }
                                                    query = @";SELECT Cust_Type FROM dbo.Mst_Order mo
                                                            INNER JOIN dbo.Mst_Customer mc 
                                                            ON mo.Payer_Code = mc.Cust_Id
                                                            WHERE Policy_No = @0";
                                                    lsRen = AABdb.Fetch<dynamic>(query, ordDt.First().OldPolicyNo);
                                                    if (lsRen.Count > 0)
                                                    {
                                                        if (Convert.ToInt32(lsRen.First().Cust_Type) == 2)
                                                        {
                                                            db.Execute(@"UPDATE dbo.ProspectCustomer SET IsPayerCompany = 1 WHERE CustID = @0", CustID);
                                                        }
                                                    }
                                                }
                                                if (isGenerate)
                                                {
                                                    VaNumber = GetBookingVANo();
                                                    query = @"UPDATE dbo.OrderSimulation 
                                                                SET VANumber=@2
                                                                WHERE CustID = @0 AND FollowUpNo = @1";
                                                    db.Execute(query, CustID, FollowUpNo, VaNumber);
                                                    AddVA(ordData.First().OrderNo, ordData.First().SalesOfficerID);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            string pPermata = AppParameterAccess.GetApplicationParametersValue("PREFIXVAPERMATA").First();
                            string pMandiri = AppParameterAccess.GetApplicationParametersValue("PREFIXVAMANDIRI").First();
                            string pBCA = AppParameterAccess.GetApplicationParametersValue("PREFIXVABCA").First();
                            VaNumber = string.IsNullOrEmpty(Convert.ToString(listDyn.First().VANumber)) ? VaNumber : Convert.ToString(listDyn.First().VANumber);
                            if (!string.IsNullOrEmpty(VaNumber))
                            {
                                pf.VAPermata = pPermata + VaNumber;
                                pf.VAMandiri = pMandiri + VaNumber;
                                pf.VABCA = pBCA + VaNumber;
                            }
                        }
                        pf.BuktiBayar = listDyn.First().BuktiBayar;
                        pf.BuktiBayarData = listDyn.First().BuktiBayarData;
                        pf.BuktiBayar2 = listDyn.First().BuktiBayar2;
                        pf.BuktiBayar2Data = listDyn.First().BuktiBayar2Data;
                        pf.BuktiBayar3 = listDyn.First().BuktiBayar3;
                        pf.BuktiBayar3Data = listDyn.First().BuktiBayar3Data;
                        pf.BuktiBayar4 = listDyn.First().BuktiBayar4;
                        pf.BuktiBayar4Data = listDyn.First().BuktiBayar4Data;
                        pf.BuktiBayar5 = listDyn.First().BuktiBayar5;
                        pf.BuktiBayar5Data = listDyn.First().BuktiBayar5Data;
                        string PolicyOrderNo = Convert.ToString(listDyn.First().PolicyOrderNo);
                        if (!string.IsNullOrEmpty(PolicyOrderNo))
                        {
                            query = @"SELECT DATEADD(DAY,13,Order_Date) AS DueDate FROM dbo.Mst_Order where Order_No = @0";
                            List<dynamic> listDate = AABdb.Fetch<dynamic>(query, PolicyOrderNo);
                            if (listDate.Count > 0)
                            {
                                pf.DueDate = listDate.First().DueDate;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ordData.First().ProductCode) && !string.IsNullOrEmpty(VaNumber))
                            {
                                decimal gp = AABdb.ExecuteScalar<decimal>("SELECT COALESCE(grace_period,0) FROM dbo.Mst_Product WHERE Product_Code = @0", ordData.First().ProductCode);
                                if (gp > 0)
                                {
                                    query = @"SELECT DATEADD(DAY,grace_period,os.EntryDate) DueDate
FROM dbo.OrderSimulation os
INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Product mp
ON os.ProductCode = mp.Product_Code
WHERE os.OrderNo = @0";
                                    List<dynamic> listDate = db.Fetch<dynamic>(query, ordData.First().OrderNo);
                                    if (listDate.Count > 0)
                                    {
                                        pf.DueDate = listDate.First().DueDate;
                                    }
                                }
                            }
                        }
                        pf.IsVAActive = null;
                        if (!string.IsNullOrEmpty(VaNumber) && string.IsNullOrEmpty(pf.RekeningPenampung))
                        {
                            listDyn = BYNDdb.Fetch<dynamic>("SELECT RowStatus FROM Finance.AAB2000VirtualAccount WHERE VirtualAccountNo = @0 AND OrderNo = @1"
                                , VaNumber, ordData.First().PolicyOrderNo == null ? "" : ordData.First().PolicyOrderNo);
                            if (listDyn.Count > 0)
                            {
                                int isReactive = db.ExecuteScalar<int>("SELECT COALESCE(isVAReactive,0) isVAreactive FROM dbo.OrderSimulation WHERE OrderNo = @0"
                                    , ordData.First().OrderNo);
                                if (isReactive == 0)
                                {
                                    if (listDyn.First().RowStatus == 1)
                                    {
                                        pf.IsVAActive = false;
                                    }
                                    else
                                    {
                                        pf.IsVAActive = true;
                                    }
                                }
                            }
                        }
                        pf.VANo = VaNumber;
                    }
                }
                return pf;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                throw e;
            }
        }

        public static bool ReactiveVA(string VANo, string PolicyOrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(actionName + " Function start : ");
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
                var mbldb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                db.Execute(@";DECLARE @@ID BIGINT
DECLARE @@Date DATETIME
SELECT @@ID=AAB2000VirtualAccountID, @@Date=os.EntryDate 
FROM Finance.AAB2000VirtualAccount a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNo
WHERE VirtualAccountNo = @0 AND a.OrderNo = @1 AND a.RowStatus = 1

UPDATE Finance.AAB2000VirtualAccount 
SET RowStatus = 0, CreatedDate = GETDATE()
WHERE AAB2000VirtualAccountID = @@ID"
, VANo, PolicyOrderNo == null ? "" : PolicyOrderNo);
                mbldb.Execute(@";UPDATE dbo.OrderSimulation SET isVAReactive = 1 WHERE VANumber = @0", VANo);
                return true;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                return false;
            }
        }

        public static Otosales.Models.vWeb2.CheckDoubleInsuredResult GetCheckDoubleInsuredDataKotor(string OldPolicyNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            Otosales.Models.vWeb2.CheckDoubleInsuredResult result = new Otosales.Models.vWeb2.CheckDoubleInsuredResult();
            try
            {
                string ChasisNo = "";
                string EngineNo = "";
                string PolicyNo = "";
                DateTime PeriodFrom = DateTime.MinValue;
                DateTime PeriodTo = DateTime.MinValue;
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = @";SELECT Chasis_Number,Engine_Number, rn.New_Policy_No, P.Period_To PeriodFrom, DATEADD(YEAR,1,Period_To) PeriodTo 
                        FROM dbo.Excluded_Renewal_Policy erp
                        INNER JOIN dbo.Renewal_Notice rn
                        ON erp.Policy_No = rn.Policy_No
                        inner JOIN dbo.Policy p
                        ON P.Policy_No = rn.Policy_No AND P.Order_Status IN ('11','9')
                        WHERE erp.Policy_No = @0";
                List<dynamic> listoldData = db.Fetch<dynamic>(query, OldPolicyNo);
                if (listoldData.Count > 0)
                {
                    ChasisNo = AppParameterAccess.SafeTrim(listoldData.First().Chasis_Number);
                    EngineNo = AppParameterAccess.SafeTrim(listoldData.First().Engine_Number);
                    PolicyNo = AppParameterAccess.SafeTrim(listoldData.First().New_Policy_No);
                    PeriodFrom = listoldData.First().PeriodFrom;
                    PeriodTo = listoldData.First().PeriodTo;
                }
                query = @";EXEC [dbo].[usp_CheckDoubleInsuredOtosales] @0, @1, @2, @3, @4, @5, @6";
                List<dynamic> res = db.Fetch<dynamic>(query, 2, ChasisNo, EngineNo, PolicyNo, "", PeriodFrom, PeriodTo);
                result.IsDoubleInsured = false;
                if (res.Count > 0)
                {
                    string DoubleInsuredType = Convert.ToString(res.First().DoubleInsuredType);
                    if (DoubleInsuredType.ToUpper().Equals("POLICY"))
                    {
                        int ThdDay = Convert.ToInt32(AppParameterAccess.GetApplicationParametersValue("DOUBLEINSUREDDAY-RENEW")[1]);
                        foreach (dynamic p in res)
                        {
                            result.IsDoubleInsured = true;
                            result.OrderNo = p.Order_No;
                            result.PolicyNo = p.Policy_No;
                            result.PeriodFrom = p.Period_From;
                            result.PeriodTo = p.Period_To;
                            //if (Convert.ToInt32(p.Days) > ThdDay)
                            //{
                            //    result.IsDoubleInsured = true;
                            //    result.OrderNo = p.Order_No;
                            //    result.PolicyNo = p.Policy_No;
                            //    result.PeriodFrom = p.Period_From;
                            //    result.PeriodTo = p.Period_To;
                            //}
                        }
                    }
                    else if (DoubleInsuredType.ToUpper().Equals("ORDER"))
                    {
                        result.IsDoubleInsured = true;
                        result.OrderNo = res.First().Order_No;
                        result.PolicyNo = res.First().Policy_No;
                        result.PeriodFrom = res.First().Period_From;
                        result.PeriodTo = res.First().Period_To;
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static CheckDoubleInsuredResult GetCheckDoubleInsured(string ChasisNo, string EngineNo, string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            CheckDoubleInsuredResult result = new CheckDoubleInsuredResult();
            try
            {
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                query = @"SELECT CAST(COALESCE(f.IsRenewal,0) AS BIT) AS IsRenewal, 
						PeriodFrom, PeriodTo, PolicyOrderNo, COALESCE(os.PolicyNo,'') PolicyNo
						FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        WHERE os.OrderNo = @0";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(query, OrderNo);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = @";EXEC [dbo].[usp_CheckDoubleInsuredOtosales] @0, @1, @2, @3, @4, @5, @6";
                List<dynamic> res = db.Fetch<dynamic>(query, 1, ChasisNo, EngineNo
                    , ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo
                    , ordData.First().PolicyOrderNo == null ? "" : ordData.First().PolicyOrderNo
                    , ordData.First().PeriodFrom, ordData.First().PeriodTo);
                result.IsDoubleInsured = false;
                if (res.Count > 0)
                {
                    string DoubleInsuredType = Convert.ToString(res.First().DoubleInsuredType);
                    if (DoubleInsuredType.ToUpper().Equals("POLICY"))
                    {
                        int ThdDay = Convert.ToInt32(AppParameterAccess.GetApplicationParametersValue("DOUBLEINSUREDDAY-RENEW")[0]);
                        if (!ordData.First().IsRenewal)
                        {
                            result.IsDoubleInsured = true;
                            result.OrderNo = res.First().Order_No;
                            result.PolicyNo = res.First().Policy_No;
                            result.PeriodFrom = res.First().Period_From;
                            result.PeriodTo = res.First().Period_To;
                        }
                        else
                        {
                            foreach (dynamic p in res)
                            {
                                if (Convert.ToInt32(p.Days) > ThdDay)
                                {
                                    result.IsDoubleInsured = true;
                                    result.OrderNo = p.Order_No;
                                    result.PolicyNo = p.Policy_No;
                                    result.PeriodFrom = p.Period_From;
                                    result.PeriodTo = p.Period_To;
                                }
                            }
                        }
                    }
                    else if (DoubleInsuredType.ToUpper().Equals("ORDER"))
                    {
                        result.IsDoubleInsured = true;
                        result.OrderNo = res.First().Order_No;
                        result.PolicyNo = res.First().Policy_No;
                        result.PeriodFrom = res.First().Period_From;
                        result.PeriodTo = res.First().Period_To;
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        #region old
        //        public static Otosales.Models.vWeb2.CheckDoubleInsuredResult GetCheckDoubleInsuredDataKotor(string OldPolicyNo)
        //        {
        //            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
        //            string query = "";
        //            Otosales.Models.vWeb2.CheckDoubleInsuredResult result = new Otosales.Models.vWeb2.CheckDoubleInsuredResult();
        //            try
        //            {
        //                string ChasisNo = "";
        //                string EngineNo = "";
        //                string PolicyNo = "";
        //                DateTime PeriodFrom = DateTime.MinValue;
        //                DateTime PeriodTo = DateTime.MinValue;
        //                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
        //                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
        //                query = @";SELECT Chasis_Number,Engine_Number, rn.New_Policy_No, P.Period_To PeriodFrom, DATEADD(DAY,DATEDIFF(DAY,Period_From,Period_To),Period_To) PeriodTo 
        //                        FROM dbo.Excluded_Renewal_Policy erp
        //                        INNER JOIN dbo.Renewal_Notice rn
        //                        ON erp.Policy_No = rn.Policy_No
        //                        inner JOIN dbo.Policy p
        //                        ON P.Policy_No = rn.Policy_No AND P.Order_Status IN ('11','9')
        //                        WHERE erp.Policy_No = @0";
        //                List<dynamic> listoldData = db.Fetch<dynamic>(query, OldPolicyNo);
        //                if (listoldData.Count > 0)
        //                {
        //                    ChasisNo = AppParameterAccess.SafeTrim(listoldData.First().Chasis_Number);
        //                    EngineNo = AppParameterAccess.SafeTrim(listoldData.First().Engine_Number);
        //                    PolicyNo = AppParameterAccess.SafeTrim(listoldData.First().New_Policy_No);
        //                    PeriodFrom = listoldData.First().PeriodFrom;
        //                    PeriodTo = listoldData.First().PeriodTo;
        //                }
        //                query = @";SELECT mo.Order_No, mo.Policy_No, 
        //                        p.Period_From, p.Period_To, 
        //						DATEDIFF(DAY,GETDATE(),p.Period_To) AS Days,
        //                        mo.Order_Type
        //                        FROM dbo.Dtl_MV dm
        //						INNER JOIN dbo.Policy p
        //						ON p.Policy_Id = dm.Policy_Id
        //						INNER JOIN dbo.Mst_Order mo
        //						ON mo.Order_No = p.Order_No
        //                        WHERE (dm.Chasis_Number = @0
        //                        OR dm.Engine_Number = @1)
        //						AND p.Status = 'A'
        //						AND p.Order_Status NOT IN('6','7','8')
        //						AND DATEDIFF(DAY,GETDATE(),p.Period_To) > 0
        //						AND dm.Endorsement_No = (SELECT MAX(Endorsement_No) FROM 
        //						dbo.Mst_Order WHERE Policy_No = mo.Policy_No AND Order_Status <> '6')";
        //                List<dynamic> res = db.Fetch<dynamic>(query, ChasisNo, EngineNo);
        //                result.IsDoubleInsured = false;
        //                if (res.Count > 0)
        //                {
        //                    foreach (dynamic p in res)
        //                    {
        //                        if (Convert.ToInt32(p.Days) > 30)
        //                        {
        //                            result.IsDoubleInsured = true;
        //                            result.OrderNo = p.Order_No;
        //                            result.PolicyNo = p.Policy_No;
        //                            result.PeriodFrom = p.Period_From;
        //                            result.PeriodTo = p.Period_To;
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    query = @"SELECT mo.Order_No, mo.Policy_No, 
        //                        mo.Period_From, mo.Period_To,
        //                        mo.Order_Type
        //                        FROM dbo.Mst_Order mo
        //                        INNER JOIN dbo.Ord_Dtl_MV odm
        //                        ON odm.Order_No = mo.Order_No						
        //                        WHERE (odm.Chasis_Number = @0
        //                        OR odm.Engine_Number = @1)
        //                        AND mo.Order_Status NOT IN (9,6)
        //						AND (mo.Period_To BETWEEN @2 AND @3
        //                        OR (mo.Period_From+1) BETWEEN @2 AND @3)
        //						AND Order_Status NOT IN('6','7','8')
        //						AND Order_Type NOT IN('4','6')
        //						AND Endorsement_No = (SELECT MAX(Endorsement_No) FROM 
        //						dbo.Mst_Order WHERE Order_No = mo.Order_No)";
        //                    res = db.Fetch<dynamic>(query, ChasisNo, EngineNo, PeriodFrom
        //                        , PeriodTo);
        //                    if (res.Count > 0)
        //                    {
        //                        result.IsDoubleInsured = true;
        //                        result.OrderNo = res.First().Order_No;
        //                        result.PolicyNo = res.First().Policy_No;
        //                        result.PeriodFrom = res.First().Period_From;
        //                        result.PeriodTo = res.First().Period_To;
        //                    }
        //                }
        //                return result;
        //            }
        //            catch (Exception e)
        //            {
        //                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
        //                throw e;
        //            }
        //        }
        #endregion
        #region private method
        public static void InsertOrderSimulation(OrderSimulationModel OS, OrderSimulationMVModel OSMV)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            // OrderSimulationModel OS = new OrderSimulationModel();
            // OrderSimulationMVModel OSMV = new OrderSimulationMVModel();
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                #region INSERT ORDERSIMULATION
                string OrderNoReturn = "";
                string FollowUpNoEnd = "";
                string CustIdEnd = "";
                // OS = JsonConvert.DeserializeObject<OrderSimulationModel>(OSData);
                query = "SELECT OrderNo FROM OrderSimulation WHERE CustID=@0 AND FollowUpNo=@1 AND ApplyF = 1";
                List<dynamic> ordno = db.Fetch<dynamic>(query, OS.CustID, OS.FollowUpNo);
                string OrderNo = "";
                if (ordno.Count > 0)
                {
                    OrderNo = ordno.First().OrderNo;
                }
                else
                {
                    OrderNo = System.Guid.NewGuid().ToString();
                }
                OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                OrderNoReturn = OS.OrderNo;
                OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;
                OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;
                OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[VANumber]
      ,[SurveyNo]
      ,[PeriodFrom]
      ,[PolicyNo]
      ,[AmountRep]
      ,[PolicyID]
      ,[SegmentCode]
) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25
,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36)
END";
                OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                db.Execute(query, OS.OrderNo
, OS.CustID
, OS.FollowUpNo
, OS.QuotationNo
, OS.MultiYearF
, OS.YearCoverage
, OS.TLOPeriod
, OS.ComprePeriod
, OS.BranchCode
, OS.SalesOfficerID
, OS.PhoneSales
, OS.DealerCode
, OS.SalesDealer
, OS.ProductTypeCode
, OS.AdminFee
, OS.TotalPremium
, OS.Phone1
, OS.Phone2
, OS.Email1
, OS.Email2
, OS.SendStatus
, OS.InsuranceType
, OS.ApplyF
, OS.SendF
, OS.LastInterestNo
, OS.LastCoverageNo
, OS.PolicySentTo
, OS.PolicyDeliveryName
, OS.PolicyDeliveryAddress
, OS.PolicyDeliveryPostalCode
, OS.VANumber
, OS.SurveyNo
, OS.PeriodFrom
, OS.PeriodTo
, OS.PolicyNo
, OS.AmountRep
, OS.PolicyID
, OS.SegmentCode);
                #endregion

                #region INSERT UPDATE ORDERSIMULATION MV
                // OSMV = JsonConvert.DeserializeObject<OrderSimulationMVModel>(OSMVData);
                OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                db.Execute(query, OSMV.OrderNo
, OSMV.ObjectNo
, OSMV.ProductTypeCode
, OSMV.VehicleCode
, OSMV.BrandCode
, OSMV.ModelCode
, OSMV.Series
, OSMV.Type
, OSMV.Sitting
, OSMV.Year
, OSMV.CityCode
, OSMV.UsageCode
, OSMV.SumInsured
, OSMV.AccessSI
, OSMV.RegistrationNumber
, OSMV.EngineNumber
, OSMV.ChasisNumber
//, OSMV.LastUpdatedTime
//, OSMV.RowStatus
, OSMV.IsNew);
                #endregion

            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        //TODO Test calculatedpremi baru
        public static void InsertExtendedCover(string OrderNo, CalculatePremiModel CalculatePremiModel, DateTime? PeriodFrom, DateTime? PeriodTo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            bool isexistSFE = false;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    List<OrderSimulationInterest> OSI = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> OSC = new List<OrderSimulationCoverage>();
                    int a = 1;
                    int year = 1;
                    //CalculatePremiModel CalculatePremiModel = JsonConvert.DeserializeObject<CalculatePremiModel>(calculatedPremiItems);
                    List<CalculatedPremiItems> calculatedPremiItem = CalculatePremiModel.ListCalculatePremi;
                    OSData OS = CalculatePremiModel.OSData;
                    OSMVData OSMV = CalculatePremiModel.OSMVData;
                    OS.OrderNo = OS.OrderNo;
                    OSMV.OrderNo = OSMV.OrderNo;
                    if (calculatedPremiItem.Count > 0)
                    {
                        DateTime? pt = null;
                        foreach (CalculatedPremiItems cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (CalculatedPremiItems cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                        pt = cpitem.PeriodTo;
                                    }
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                        pf = cpitem.PeriodFrom;
                                    }
                                }
                                OrderSimulationInterest osi = new OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (CalculatedPremiItems cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;
                        foreach (OrderSimulationInterest item in OSI)
                        {
                            foreach (CalculatedPremiItems item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    OrderSimulationCoverage oscItem = new OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }

                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert

                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OrderNo);

                        int i = 1;
                        foreach (OrderSimulationInterest data in OSI)
                        {
                            query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                            i++;
                        }

                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationInterest> TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationInterest data in TempAllOSI)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in OSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) && data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                        query = @"UPDATE OrderSimulationInterest SET 
                        //                                Premium=@5,
                        //                                LastUpdatedTime=GETDATE()
                        //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4 AND Deductible_Code=@6";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year, data2.Premium, data.DeductibleCode);
                        //                                    }
                        //                                }

                        //                                if (!isExist)
                        //                                {
                        //                                    //soft delete coverage

                        //                                    query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 ";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo);


                        //                                    //soft delete interest
                        //                                    query = @"DELETE OrderSimulationInterest " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }



                        #region Insert
                        //  OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        i = 1;
                        foreach (OrderSimulationCoverage data in OSC)
                        {
                            query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                            i++;

                        }
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        OS.TotalPremium = db.ExecuteScalar<decimal>("SELECT COALESCE((SELECT SUM(Premium) FROM OrderSimulationCoverage where OrderNo=@0)+(SELECT AdminFee FROM OrderSimulation where OrderNo=@0),0)", OS.OrderNo);
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = PeriodFrom ?? DateTime.Now;
                            DateTime end = PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion
                    }else
                    {
                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OrderNo);
                    }
                }
            }
            catch (Exception e)
            {
                InsertExtendedCover(OrderNo, CalculatePremiModel, PeriodFrom, PeriodTo);
            }
        }
        #endregion
        public static string UpdateTaskListDetailOld(string CustID, string FollowUpNo,
    string personalData, string companyData, string vehicleData, string policyAddress,
    string surveySchedule, string imageData, string OSData, string OSMVData,
    string calculatedPremiItems, string FollowUpStatus, string Remarks)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string result = "";
            Otosales.Models.vWeb2.PersonalData pd = new Otosales.Models.vWeb2.PersonalData();
            Otosales.Models.vWeb2.CompanyData cd = new Otosales.Models.vWeb2.CompanyData();
            Otosales.Models.vWeb2.VehicleData vd = new Otosales.Models.vWeb2.VehicleData();
            Otosales.Models.vWeb2.PolicyAddress pa = new Otosales.Models.vWeb2.PolicyAddress();
            Otosales.Models.vWeb2.SurveySchedule ss = new Otosales.Models.vWeb2.SurveySchedule();
            Otosales.Models.vWeb2.Remarks rm = new Otosales.Models.vWeb2.Remarks();
            List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData = new List<Otosales.Models.vWeb2.ImageDataTaskDetail>();

            string query = @"";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            try
            {
                Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
                if (!string.IsNullOrEmpty(personalData))
                {
                    pd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PersonalData>(personalData);
                    query = @";IF EXISTS(SELECT * FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCustomer SET 
	                        Name = @1, CustBirthDay = @2, CustGender = @3, CustAddress = @4, 
                            PostalCode = @5, Email1 = @6, Phone1 = @7, IdentityNo = @8, 
                            isCompany = @9
                            WHERE CustID = @0 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @12
                        AND ApplyF = 1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.OrderSimulation 
	                        SET IsNeedDocRep = @10, AmountRep = @11
	                        WHERE CustID = @0 AND FollowUpNo = @12
	                        AND ApplyF = 1 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @12)
                        BEGIN
	                        UPDATE dbo.FollowUp 
	                        SET FollowUpName = @1, ProspectName = @1
	                        WHERE CustID = @0 AND FollowUpNo = @12
                        END ";
                    db.Execute(query, CustID
                        , pd.Name, pd.CustBirthDay, pd.CustGender, pd.CustAddress
                        , pd.PostalCode, pd.Email1, pd.Phone1, pd.IdentityNo
                        , pd.isCompany, pd.isNeedDocRep, pd.AmountRep, FollowUpNo);
                    if (pd.IsPayerCompany != null)
                    {
                        query = @";UPDATE dbo.ProspectCustomer SET IsPayerCompany = @1 WHERE CustID = @0";
                        db.Execute(query, CustID, pd.IsPayerCompany);
                    }
                }
                if (!string.IsNullOrEmpty(companyData))
                {
                    cd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.CompanyData>(companyData);
                    query = @";IF EXISTS (SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCompany SET CompanyName = @2, NPWPno = @3, NPWPdate = @4, 
	                        NPWPaddress = @5, OfficeAddress = @6, PostalCode = @7, PICPhoneNo = @8, PICname = @9, Email = @10 
	                        WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
							UPDATE dbo.ProspectCustomer SET Name = @9, Phone1 = @8, Email1 = @10
							WHERE CustID = @0
                        END";
                    db.Execute(query, CustID, FollowUpNo
                        , cd.CompanyName, cd.NPWPno, cd.NPWPdate
                        , cd.NPWPaddres, cd.OfficeAddress, cd.PostalCode, cd.PICPhoneNo, cd.PICname, cd.PICEmail);
                }
                if (!string.IsNullOrEmpty(OSData) && !string.IsNullOrEmpty(OSMVData))
                {
                    Otosales.Repository.vWeb2.MobileRepository.InsertOrderSimulation(OSData, OSMVData);
                }
                if (!string.IsNullOrEmpty(vehicleData))
                {
                    vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);

                    if (!string.IsNullOrEmpty(vd.ProductCode))
                    {
                        query = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET ProductTypeCode = @2,
	                            VehicleCode = @3, BrandCode = @4, ModelCode = @5,
	                            Series = @6, Type = @7, Sitting = @8, Year = @9,
	                            CityCode = @10, UsageCode = @11, AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET InsuranceType = @19, 
                                ProductCode = @20, 
								SegmentCode = @21, PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                    }
                    else
                    {
                        query = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                    }
                    db.Execute(query, CustID, FollowUpNo
                        , vd.ProductTypeCode
                        , vd.VehicleCode, vd.BrandCode, vd.ModelCode
                        , vd.Series, vd.Type, vd.Sitting, vd.Year
                        , vd.CityCode, vd.UsageCode, vd.AccessSI
                        , vd.RegistrationNumber, vd.EngineNumber
                        , vd.ChasisNumber, vd.IsNew, vd.SumInsured
                        , vd.ColorOnBPKB

                        , vd.InsuranceType
                        , vd.ProductCode
                        , vd.SegmentCode, vd.PeriodFrom, vd.PeriodTo
                        , vd.IsORDefectsRepair, vd.IsAccessoriesChange
                        , vd.IsPolicyIssuedBeforePaying
                        , vd.Remarks

                        , vd.DealerCode
                        , vd.SalesDealer);
                }
                if (!string.IsNullOrEmpty(calculatedPremiItems))
                {
                    string OrderNo = db.ExecuteScalar<string>(
                        @";DECLARE @@Orderno VARCHAR(100) 
                        SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                    INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                    WHERE f.CustID = @0 AND f.FollowUpNo = @1 
                        AND o.RowStatus = 1 AND ApplyF = 1
                        SELECT @@Orderno"
                        , CustID, FollowUpNo);

                    vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                    Otosales.Repository.vWeb2.MobileRepository.InsertExtendedCover(OrderNo, calculatedPremiItems, vd.PeriodFrom, vd.PeriodTo);
                }
                if (!string.IsNullOrEmpty(policyAddress))
                {
                    pa = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PolicyAddress>(policyAddress);
                    query = @"IF EXISTS (SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulation SET PolicySentTo = @2, PolicyDeliveryName = @3, 
	                            PolicyDeliveryAddress = @4, PolicyDeliveryPostalCode = @5 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                    db.Execute(query, CustID, FollowUpNo
                        , pa.SentTo, pa.Name
                        , pa.Address, pa.PostalCode);
                }
                if (!string.IsNullOrEmpty(surveySchedule))
                {
                    ss = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.SurveySchedule>(surveySchedule);
                    query = @";DECLARE @@OrderNo VARCHAR(100);
                            SELECT @@OrderNo=OrderNo FROM dbo.OrderSimulation
                            WHERE CustID = @0 AND FollowUpNo = @1 AND 
                            RowStatus = 1 AND ApplyF = 1
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationSurvey oss
                            INNER JOIN dbo.OrderSimulation os ON os.OrderNo = oss.OrderNo
                            WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND 
                            os.RowStatus = 1 AND os.ApplyF = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulationSurvey SET CityCode = @2, LocationCode = @3, SurveyAddress = @4,
	                            SurveyPostalCode = @5, SurveyDate = @6, ScheduleTimeID = @7, ModifiedBy = 'OtosalesAPI',
								IsNeedSurvey = @8, IsNSASkipSurvey = @9, IsManualSurvey = @10,
								ModifiedDate = GETDATE() WHERE OrderNo = @@OrderNo
                            END
							ELSE
							BEGIN
								INSERT INTO dbo.OrderSimulationSurvey
								        ( OrderNo ,
								          CityCode ,
								          LocationCode ,
								          SurveyAddress ,
								          SurveyPostalCode ,
								          SurveyDate ,
								          ScheduleTimeID ,
								          RowStatus ,
								          CreatedBy ,
								          CreatedDate,
										  IsNeedSurvey,
										  IsNSASkipSurvey,
										  IsManualSurvey
								        )
								VALUES  ( @@OrderNo , -- OrderNo - varchar(100)
								          @2 , -- CityCode - int
								          @3 , -- LocationCode - int
								          @4 , -- SurveyAddress - varchar(500)
								          @5 , -- SurveyPostalCode - varchar(5)
								          @6 , -- SurveyDate - datetime
								          @7 , -- ScheduleTimeID - datetime
								          1 , -- RowStatus - bit
								          'OtosalesAPI' , -- CreatedBy - varchar(50)
								          GETDATE(), -- CreatedDate - datetime
										  @8,
										  @9,
										  @10
								        )
							END";
                    db.Execute(query, CustID, FollowUpNo
                        , ss.CityCode, ss.LocationCode, ss.SurveyAddress
                        , ss.SurveyPostalCode, ss.SurveyDate, ss.ScheduleTimeID
                        , ss.IsNeedSurvey, ss.IsNSASkipSurvey, ss.IsManualSurvey);
                }
                if (!string.IsNullOrEmpty(imageData))
                {
                    ListImgData = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.ImageDataTaskDetail>>(imageData);
                    Otosales.Repository.vWeb2.MobileRepository.UpdateImageData(ListImgData, CustID, FollowUpNo);

                }
                if (!string.IsNullOrEmpty(FollowUpStatus))
                {
                    query = @"UPDATE dbo.FollowUp SET FollowUpStatus = @2 WHERE CustID = @0 AND FollowUpNo = @1";
                    db.Execute(query, CustID, FollowUpNo, FollowUpStatus);
                }
                query = @"SELECT OrderNo FROM dbo.OrderSimulation
                        WHERE CustID = @0 AND FollowUpNo = @1 AND 
                        RowStatus = 1 AND ApplyF = 1";
                List<dynamic> res = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (res.Count > 0)
                {
                    result = res.First().OrderNo;
                }
                if (!string.IsNullOrEmpty(Remarks))
                {
                    query = @"UPDATE dbo.FollowUp
                            SET RemarkToSA = @0 WHERE FollowUpNo = @1";
                    rm = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.Remarks>(Remarks);
                    db.Execute(query, rm.RemarkToSA, FollowUpNo);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static string UpdateTaskListDetail(string CustID, string FollowUpNo,
Otosales.Models.vWeb2.PersonalData pd, Otosales.Models.vWeb2.CompanyData cd, Otosales.Models.vWeb2.VehicleData vd, Otosales.Models.vWeb2.PolicyAddress pa,
Otosales.Models.vWeb2.SurveySchedule ss, List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, OrderSimulationModel OS, OrderSimulationMVModel OSMV,
CalculatePremiModel CalculatePremiModel, string FollowUpStatus, Otosales.Models.vWeb2.Remarks rm)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string result = "";
            //Otosales.Models.vWeb2.PersonalData pd = new Otosales.Models.vWeb2.PersonalData();
            //Otosales.Models.vWeb2.CompanyData cd = new Otosales.Models.vWeb2.CompanyData();
            //Otosales.Models.vWeb2.VehicleData vd = new Otosales.Models.vWeb2.VehicleData();
            //Otosales.Models.vWeb2.PolicyAddress pa = new Otosales.Models.vWeb2.PolicyAddress();
            //Otosales.Models.vWeb2.SurveySchedule ss = new Otosales.Models.vWeb2.SurveySchedule();
            //Otosales.Models.vWeb2.Remarks rm = new Otosales.Models.vWeb2.Remarks();
            //List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData = new List<Otosales.Models.vWeb2.ImageDataTaskDetail>();

            string query = @"";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            try
            {
                Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
                if (pd != null)
                {
                    // pd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PersonalData>(personalData);
                    query = @";IF EXISTS(SELECT * FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCustomer SET 
	                        Name = @1, CustBirthDay = @2, CustGender = @3, CustAddress = @4, 
                            PostalCode = @5, Email1 = @6, Phone1 = @7, IdentityNo = @8, 
                            isCompany = @9
                            WHERE CustID = @0 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @12
                        AND ApplyF = 1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.OrderSimulation 
	                        SET IsNeedDocRep = @10, AmountRep = @11
	                        WHERE CustID = @0 AND FollowUpNo = @12
	                        AND ApplyF = 1 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @12)
                        BEGIN
	                        UPDATE dbo.FollowUp 
	                        SET FollowUpName = @1, ProspectName = @1
	                        WHERE CustID = @0 AND FollowUpNo = @12
                        END ";
                    db.Execute(query, CustID
                        , pd.Name, pd.CustBirthDay, pd.CustGender, pd.CustAddress
                        , pd.PostalCode, pd.Email1, pd.Phone1, pd.IdentityNo
                        , pd.isCompany, pd.isNeedDocRep, pd.AmountRep, FollowUpNo);
                    if (pd.IsPayerCompany != null)
                    {
                        query = @";UPDATE dbo.ProspectCustomer SET IsPayerCompany = @1 WHERE CustID = @0";
                        db.Execute(query, CustID, pd.IsPayerCompany);
                    }
                }
                if (cd != null)
                {
                    //cd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.CompanyData>(companyData);
                    query = @";DECLARE @@IsCompany BIT
                            SELECT @@IsCompany=IsCompany FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1
                            IF @@IsCompany=1
                            BEGIN
	                            IF EXISTS (SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                                BEGIN
	                                UPDATE dbo.ProspectCompany SET CompanyName = @2, NPWPno = @3, NPWPdate = @4, 
	                                NPWPaddress = @5, OfficeAddress = @6, PostalCode = @7, PICPhoneNo = @8, PICname = @9, Email = @10 
	                                WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
		                            UPDATE dbo.ProspectCustomer SET Name = @9, Phone1 = @8, Email1 = @10
		                            WHERE CustID = @0
                                END
                            END";
                    db.Execute(query, CustID, FollowUpNo
                        , cd.CompanyName, cd.NPWPno, cd.NPWPdate
                        , cd.NPWPaddres, cd.OfficeAddress, cd.PostalCode, cd.PICPhoneNo, cd.PICname, cd.PICEmail);
                }
                if (OS != null && OSMV != null)
                {
                    InsertOrderSimulation(OS, OSMV);
                }
                if (vd != null)
                {
                    // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);

                    if (!string.IsNullOrEmpty(vd.ProductCode))
                    {
                        query = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET ProductTypeCode = @2,
	                            VehicleCode = @3, BrandCode = @4, ModelCode = @5,
	                            Series = @6, Type = @7, Sitting = @8, Year = @9,
	                            CityCode = @10, UsageCode = @11, AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET InsuranceType = @19, 
                                ProductCode = @20, 
								SegmentCode = @21, PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                    }
                    else
                    {
                        query = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                    }
                    db.Execute(query, CustID, FollowUpNo
                        , vd.ProductTypeCode
                        , vd.VehicleCode, vd.BrandCode, vd.ModelCode
                        , vd.Series, vd.Type, vd.Sitting, vd.Year
                        , vd.CityCode, vd.UsageCode, vd.AccessSI
                        , vd.RegistrationNumber, vd.EngineNumber
                        , vd.ChasisNumber, vd.IsNew, vd.SumInsured
                        , vd.ColorOnBPKB

                        , vd.InsuranceType
                        , vd.ProductCode
                        , vd.SegmentCode, vd.PeriodFrom, vd.PeriodTo
                        , vd.IsORDefectsRepair, vd.IsAccessoriesChange
                        , vd.IsPolicyIssuedBeforePaying
                        , vd.Remarks

                        , vd.DealerCode
                        , vd.SalesDealer);
                }
                if (CalculatePremiModel != null)
                {
                    string OrderNo = db.ExecuteScalar<string>(
                        @";DECLARE @@Orderno VARCHAR(100) 
                        SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                    INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                    WHERE f.CustID = @0 AND f.FollowUpNo = @1 
                        AND o.RowStatus = 1 AND ApplyF = 1
                        SELECT @@Orderno"
                        , CustID, FollowUpNo);

                    // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                    InsertExtendedCover(OrderNo, CalculatePremiModel, vd.PeriodFrom, vd.PeriodTo);
                }
                if (pa != null)
                {
                    //pa = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PolicyAddress>(policyAddress);
                    query = @"IF EXISTS (SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulation SET PolicySentTo = @2, PolicyDeliveryName = @3, 
	                            PolicyDeliveryAddress = @4, PolicyDeliveryPostalCode = @5 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                    db.Execute(query, CustID, FollowUpNo
                        , pa.SentTo, pa.Name
                        , pa.Address, pa.PostalCode);
                }
                if (ss != null)
                {
                    //ss = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.SurveySchedule>(surveySchedule);
                    query = @";DECLARE @@OrderNo VARCHAR(100);
                            SELECT @@OrderNo=OrderNo FROM dbo.OrderSimulation
                            WHERE CustID = @0 AND FollowUpNo = @1 AND 
                            RowStatus = 1 AND ApplyF = 1
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationSurvey oss
                            INNER JOIN dbo.OrderSimulation os ON os.OrderNo = oss.OrderNo
                            WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND 
                            os.RowStatus = 1 AND os.ApplyF = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulationSurvey SET CityCode = @2, LocationCode = @3, SurveyAddress = @4,
	                            SurveyPostalCode = @5, SurveyDate = @6, ScheduleTimeID = @7, ModifiedBy = 'OtosalesAPI',
								IsNeedSurvey = @8, IsNSASkipSurvey = @9, IsManualSurvey = @10,
								ModifiedDate = GETDATE() WHERE OrderNo = @@OrderNo
                            END
							ELSE
							BEGIN
								INSERT INTO dbo.OrderSimulationSurvey
								        ( OrderNo ,
								          CityCode ,
								          LocationCode ,
								          SurveyAddress ,
								          SurveyPostalCode ,
								          SurveyDate ,
								          ScheduleTimeID ,
								          RowStatus ,
								          CreatedBy ,
								          CreatedDate,
										  IsNeedSurvey,
										  IsNSASkipSurvey,
										  IsManualSurvey
								        )
								VALUES  ( @@OrderNo , -- OrderNo - varchar(100)
								          @2 , -- CityCode - int
								          @3 , -- LocationCode - int
								          @4 , -- SurveyAddress - varchar(500)
								          @5 , -- SurveyPostalCode - varchar(5)
								          @6 , -- SurveyDate - datetime
								          @7 , -- ScheduleTimeID - datetime
								          1 , -- RowStatus - bit
								          'OtosalesAPI' , -- CreatedBy - varchar(50)
								          GETDATE(), -- CreatedDate - datetime
										  @8,
										  @9,
										  @10
								        )
							END";
                    int maxLength = 150;
                    string SurveyAddress = "";
                    if (!string.IsNullOrEmpty(ss.SurveyAddress) && !string.IsNullOrWhiteSpace(ss.SurveyAddress)) {
                        SurveyAddress = ss.SurveyAddress.Length <= maxLength ? ss.SurveyAddress : ss.SurveyAddress.Substring(0, maxLength);
                    }
                    db.Execute(query, CustID, FollowUpNo
                        , ss.CityCode, ss.LocationCode, ss.SurveyAddress
                        , ss.SurveyPostalCode, ss.SurveyDate, ss.ScheduleTimeID
                        , ss.IsNeedSurvey, ss.IsNSASkipSurvey, ss.IsManualSurvey);
                }
                if (ListImgData.Count > 0)
                {
                    //ListImgData = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.ImageDataTaskDetail>>(imageData);
                    Otosales.Repository.vWeb2.MobileRepository.UpdateImageData(ListImgData, CustID, FollowUpNo);

                }
                if (!string.IsNullOrEmpty(FollowUpStatus))
                {
                    query = @"UPDATE dbo.FollowUp SET FollowUpStatus = @2 WHERE CustID = @0 AND FollowUpNo = @1";
                    db.Execute(query, CustID, FollowUpNo, FollowUpStatus);
                }
                query = @"SELECT OrderNo FROM dbo.OrderSimulation
                        WHERE CustID = @0 AND FollowUpNo = @1 AND 
                        RowStatus = 1 AND ApplyF = 1";
                List<dynamic> res = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (res.Count > 0)
                {
                    result = res.First().OrderNo;
                }
                if (rm != null)
                {
                    query = @"UPDATE dbo.FollowUp
                            SET RemarkToSA = @0 WHERE FollowUpNo = @1";
                    // rm = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.Remarks>(Remarks);
                    db.Execute(query, rm.RemarkToSA, FollowUpNo);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static Otosales.Models.vWeb2.TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<dynamic> listDyn = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
            string query = "";

            try
            {
                Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
                query = @";DECLARE @@Admin DECIMAL
                        --DECLARE @@GrossPremium DECIMAL
                        --DECLARE @@NetPremi DECIMAL
                        DECLARE @@OrderNo VARCHAR(100)
                        DECLARE @@PolicyNo VARCHAR(100)
                        DECLARE @@PolicyOrderNo VARCHAR(100)
                        DECLARE @@SurveyNo VARCHAR(100)
                        DECLARE @@OldPolicyNo VARCHAR(100)

                        SELECT @@Admin=AdminFee,@@OrderNo=OrderNo, @@PolicyNo=PolicyNo, @@PolicyOrderNo=PolicyOrderNo, 
                        @@SurveyNo=SurveyNo, @@OldPolicyNo=OldPolicyNo
                        FROM dbo.OrderSimulation 
                        WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1
                        --SELECT @@GrossPremium=SUM(Gross_Premium) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @@OrderNo AND RowStatus = 1 
                        --SELECT @@NetPremi=@@GrossPremium+@@Admin

                        SELECT 0 GrossPremium, CAST(@@Admin AS DECIMAL) Admin, 0 NetPremi,
						@@PolicyNo AS PolicyNo, @@OrderNo AS OrderNo, @@PolicyOrderNo AS PolicyOrderNo, 
                        @@SurveyNo AS SurveyNo, COALESCE(@@OldPolicyNo,'') AS OldPolicyNo, 0 NoClaimBonus, NULL OldPolicyPeriodTo";
                Otosales.Models.vWeb2.FieldPremi premi = db.Query<Otosales.Models.vWeb2.FieldPremi>(query, CustID, FollowUpNo).FirstOrDefault();
                if (premi != null)
                {
                    if (!string.IsNullOrEmpty(premi.OldPolicyNo))
                    {
                        List<dynamic> ls = new List<dynamic>();
                        //query = @"SELECT COALESCE(SUM(Amount), 0) AS NCB FROM dbo.Ren_Cover_Disc WHERE Policy_No = @0";
                        //ls = AABdb.Fetch<dynamic>(query, listDyn.First().OldPolicyNo);
                        //if (ls.Count > 0)
                        //{
                        //    premi.NoClaimBonus = ls.First().NCB;
                        //}
                        query = @"SELECT Period_To FROM dbo.Policy WHERE Policy_No = @0";
                        ls = AABdb.Fetch<dynamic>(query, premi.OldPolicyNo);
                        if (ls.Count > 0)
                        {
                            premi.OldPolicyPeriodTo = ls.First().Period_To;
                        }
                    }
                    result.FieldPremi = premi;
                }
                query = @"SELECT p.Name,p.CustBirthDay,p.CustGender,p.CustAddress,p.PostalCode,
                        p.Email1,p.Phone1,p.IdentityNo, CAST(COALESCE(p.isCompany,0) AS BIT) isCompany, 
                        CAST(0 AS BIT) isNeedDocRep, 0 AmountRep, IsPayerCompany
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
                        WHERE p.CustID = @0 AND p.RowStatus = 1";
                Otosales.Models.vWeb2.PersonalData pd = db.Query<Otosales.Models.vWeb2.PersonalData>(query, CustID).FirstOrDefault();
                if (pd != null)
                {
                    query = @"SELECT CAST(COALESCE(os.IsNeedDocRep,0) AS BIT)IsNeedDocRep, 
                            COALESCE(os.AmountRep, 0) AmountRep
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
						INNER JOIN dbo.OrderSimulation os
						ON os.FollowUpNo = f.FollowUpNo
                        WHERE p.CustID = @0 AND p.RowStatus = 1
						AND os.RowStatus = 1 AND os.ApplyF = 1";
                    listDyn = db.Fetch<dynamic>(query, CustID);
                    if (listDyn.Count > 0)
                    {
                        pd.isNeedDocRep = listDyn.First().IsNeedDocRep;
                        pd.AmountRep = listDyn.First().AmountRep;
                    }
                    result.PersonalData = pd;
                }
                query = @";SELECT IdentityCard, id1.Data IdentityCardData,
                        f.STNK, id2.Data STNKData,
                        f.BSTB1 BSTB, id3.Data BSTBData,
                        f.KonfirmasiCust, id4.Data KonfirmasiCustData,
                        f.DocRep, id5.Data DocRepData
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON f.IdentityCard = id1.PathFile
                        LEFT JOIN dbo.ImageData id2
                        ON f.STNK = id2.PathFile
                        LEFT JOIN dbo.ImageData id3
                        ON f.BSTB1 = id3.PathFile
                        LEFT JOIN dbo.ImageData id4
                        ON f.KonfirmasiCust = id4.PathFile
                        LEFT JOIN dbo.ImageData id5
                        ON f.DocRep = id5.PathFile
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result.PersonalDocument = listDyn;
                }

                query = @"SELECT CompanyName,NPWPno,NPWPdate,NPWPaddress NPWPaddres,OfficeAddress
                            ,PostalCode,PICPhoneNo,PICname,Email PICEmail FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";
                Otosales.Models.vWeb2.CompanyData cd = db.Query<Otosales.Models.vWeb2.CompanyData>(query, CustID, FollowUpNo).FirstOrDefault();
                if (cd != null)
                {
                    result.CompanyData = cd;
                }
                query = @";IF EXISTS(SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1)
                        BEGIN
	                        SELECT pc.NPWP, id1.Data NPWPData,
	                        pc.SIUP, id2.Data SIUPData
	                        FROM dbo.ProspectCompany pc
	                        LEFT JOIN dbo.ImageData id1
	                        ON pc.NPWP = id1.PathFile
	                        LEFT JOIN dbo.ImageData id2
	                        ON pc.SIUP = id2.PathFile
	                        WHERE pc.CustID = @0 AND pc.FollowUpNo = @1 AND pc.RowStatus = 1
                        END
                        ELSE
                        BEGIN
	                        SELECT NULL NPWP, NULL NPWPData,
	                        NULL SIUP, NULL SIUPData
                        END";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result.CompanyDocument = listDyn;
                }
                query = @";SELECT c.Description + ' ' + d.Description + ' ' + omv.Year + ' ' + a.Series  AS Vehicle,
                        omv.VehicleCode, omv.BrandCode, os.ProductTypeCode, omv.ModelCode,  omv.Series, 
                        omv.Type, CAST(omv.Sitting AS DECIMAL) Sitting, omv.Year, omv.CityCode, CAST(omv.SumInsured AS DECIMAL) SumInsured, os.InsuranceType,
                        os.VANumber, p.DealerCode, p.SalesDealer, omv.UsageCode, omv.ColorOnBPKB, 
                        omv.RegistrationNumber, omv.EngineNumber, omv.ChasisNumber, omv.IsNew, os.OrderNo,
						os.PolicyNo, os.ProductCode, os.SegmentCode, os.PeriodFrom, os.PeriodTo,
						os.IsORDefectsRepair, IsAccessoriesChange, IsPolicyIssuedBeforePaying, 
                        os.Remarks, f.DocPendukung, id.Data AS DocPendukungData,
                        0 BasicCoverID, '' ORDefectsDesc, '' NoCover
                        FROM Vehicle a 
                        LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
                        AND c.ProductTypeCode = a.ProductTypeCode 
                        LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
                        AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
                        LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                        a.BrandCode=b.BrandCode
                        AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                        AND a.Series=b.Series AND a.Type=b.Type 
                        AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                        AND a.Year > b.Year 
                        INNER JOIN dbo.OrderSimulationMV omv ON omv.VehicleCode = a.VehicleCode
                        INNER JOIN dbo.OrderSimulation os ON os.OrderNo = omv.OrderNo
                        INNER JOIN dbo.ProspectCustomer p ON p.CustID = os.CustID
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						LEFT JOIN dbo.ImageData id ON id.PathFile = f.DocPendukung
                        WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND omv.RowStatus = 1
						AND f.RowStatus = 1 AND os.CustID = @0 AND os.FollowUpNo = @1 AND b.Year IS NULL 
                        ORDER BY a.VehicleCode";
                Otosales.Models.vWeb2.VehicleData vData = db.Query<Otosales.Models.vWeb2.VehicleData>(query, CustID, FollowUpNo).FirstOrDefault();
                if (vData != null)
                {
                    query = @";DECLARE @@TLOPer INT
                            DECLARE @@ComprePer INT
                            SELECT @@TLOPer=TLOPeriod,@@ComprePer=ComprePeriod FROM dbo.OrderSimulation WHERE OrderNo = @0
                            SELECT ID AS BasicCoverID FROM dbo.Mst_Basic_Cover 
                            WHERE ComprePeriod = @@ComprePer
                            AND TLOPeriod = @@TLOPer";
                    List<dynamic> bscCvr = db.Fetch<dynamic>(query, vData.OrderNo);
                    if (bscCvr.Count > 1)
                    {
                        query = @"SELECT RTRIM(LTRIM(CoverageID)) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                        List<string> ListCvrID = db.Fetch<string>(query, vData.OrderNo);
                        if (ListCvrID.Contains("TLO"))
                        {
                            query = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%TLO Others%'";
                            int id = db.ExecuteScalar<int>(query);
                            vData.BasicCoverID = id;
                        }
                        else if (ListCvrID.Contains("ALLRIK"))
                        {
                            query = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%Comprehensive Others%'";
                            int id = db.ExecuteScalar<int>(query);
                            vData.BasicCoverID = id;
                        }
                    }
                    else
                    {
                        vData.BasicCoverID = bscCvr.First().BasicCoverID;
                    }
                    if (!string.IsNullOrEmpty(result.FieldPremi.OldPolicyNo))
                    {
                        query = @";DECLARE @@PolicyID VARCHAR(100)
                                SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
                                SELECT STUFF(( SELECT ', ' + Name
                                FROM dbo.Dtl_Original_Defect 
                                where Policy_Id = @@PolicyID
                                FOR XML PATH('')
                                ), 1, 1, '')  AS OriginalDefect,
								STUFF(( SELECT ', ' + Name
                                FROM dbo.Ren_Dtl_NoCover 
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS NoCover,
								STUFF(( SELECT ', ' + ao.AccsDescription + (CASE WHEN rdna.Accs_Cat_Code != '' AND rdna.Accs_Cat_Code is not null
                                THEN ' '+ac.accscatdescription ELSE '' END) + (CASE WHEN rdna.Accs_part_Code != '' AND rdna.Accs_part_Code is not null
                                THEN ' '+ap.accspartdescription ELSE '' END)
                                FROM dbo.Ren_Dtl_Non_Standard_Accessories  rdna
                                INNER JOIN dbo.AccessoriesOnline ao
                                ON ao.AccsCode = rdna.Accs_Code
                                left join dbo.AccessoriesCategory ac
                                ON ac.AccsCatCode = rdna.Accs_Cat_Code
                                left join dbo.AccessoriesPart ap
                                on ap.accspartcode = rdna.accs_part_code
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS Accessories";
                        List<dynamic> list = AABdb.Fetch<dynamic>(query, result.FieldPremi.OldPolicyNo);
                        if (list.Count > 0)
                        {
                            vData.ORDefectsDesc = list.First().OriginalDefect;
                            vData.NoCover = list.First().NoCover;
                            vData.Accessories = list.First().Accessories;
                        }
                    }
                    result.VehicleData = vData;
                }
                //result.PaymentInfo = GetPaymentInfo(CustID, FollowUpNo);
                result.PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo();
                query = @"SELECT PolicySentTo SentTo, PolicyDeliveryName Name, PolicyDeliveryAddress Address, PolicyDeliveryPostalCode PostalCode FROM dbo.OrderSimulation WHERE CustID = @0 
                            AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1";
                Otosales.Models.vWeb2.PolicyAddress pa = db.Query<Otosales.Models.vWeb2.PolicyAddress>(query, CustID, FollowUpNo).FirstOrDefault();
                if (pa != null)
                {
                    result.PolicyAddres = pa;
                }
                if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                {
                    query = @";SELECT odi.[Description] AS NoCover,
                        STUFF(( SELECT ', ' + [Description]
                                    FROM dbo.Ord_Object_Clause 
			                        WHERE Order_No = @0
								    AND Clause_Id = 'OMV99'
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS OriginalDefect,
                        STUFF(( SELECT ', ' + Item_Name
                                    FROM dbo.Ord_Dtl_Interest_Item 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS Accessories,
                        ms.[Description] AS SurveyStatus,
                        s.ResultRecommendation AS SurveyorRecomendation,
                        s.remarks AS Remaks,
                        mu.[User_Name] AS Surveyor
                        FROM dbo.Survey s 
						INNER JOIN dbo.Lnk_Survey_Order lso
						ON s.Survey_No = lso.Survey_No
                        INNER JOIN dbo.Mst_Order mo 
                        ON mo.Order_No = s.Reference_No
                        INNER JOIN dbo.Ord_Dtl_Interest odi
                        ON odi.Order_No = mo.Order_No
                        INNER JOIN dbo.Mst_State ms 
                        ON ms.[Type] = 'survey' 
                        AND ms.[State] = s.[Status]
                        INNER JOIN dbo.Mst_User mu
                        ON mu.[User_Id] = s.Surveyor
                        WHERE mo.Order_No = @0
						AND odi.Interest_Code = 'CASCO'";
                    Otosales.Models.vWeb2.SurveyData sd = AABdb.Query<Otosales.Models.vWeb2.SurveyData>(query, result.FieldPremi.PolicyOrderNo).FirstOrDefault();
                    if (listDyn.Count > 0)
                    {
                        result.SurveyData = sd;
                    }
                }
                query = @"SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc, CAST(COALESCE(IsManualSurvey,0) AS BIT) IsManualSurvey
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                Otosales.Models.vWeb2.SurveySchedule ss = db.Fetch<Otosales.Models.vWeb2.SurveySchedule>(query, CustID, FollowUpNo).FirstOrDefault();
                if (listDyn.Count > 0)
                {
                    result.SurveySchedule = ss;
                }
                query = @";SELECT f.SPPAKB, id1.Data SPPAKBData,
                        f.FAKTUR, id2.Data FAKTURData,
                        f.DocNSA1, id3.Data DocNSA1Data,
                        f.DocNSA2, id4.Data DocNSA2Data,
                        f.DocNSA3, id5.Data DocNSA3Data,
                        f.DocNSA4, id6.Data DocNSA4Data,
                        f.DocNSA5, id7.Data DocNSA5Data,
                        RemarkToSA, RemarkFromSA
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON id1.PathFile = f.SPPAKB
                        LEFT JOIN dbo.ImageData id2
                        ON id2.PathFile = f.FAKTUR
                        LEFT JOIN dbo.ImageData id3
                        ON id3.PathFile = f.DocNSA1
                        LEFT JOIN dbo.ImageData id4
                        ON id4.PathFile = f.DocNSA2
                        LEFT JOIN dbo.ImageData id5
                        ON id5.PathFile = f.DocNSA3
                        LEFT JOIN dbo.ImageData id6
                        ON id6.PathFile = f.DocNSA4
                        LEFT JOIN dbo.ImageData id7
                        ON id7.PathFile = f.DocNSA5
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                List<dynamic> listDoc = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDoc.Count > 0)
                {
                    query = @"SELECT TOP 1 mu.User_Name Name 
                                FROM dbo.Mst_Order_Mobile mom
                                INNER JOIN dbo.Mst_User mu
                                ON mu.User_Id = mom.EntryUsr
                                WHERE Order_No = @0 
                                AND (SA_State = 0 OR SA_State = 2) 
                                AND mom.EntryUsr <> 'OTOSL' ORDER BY mom.EntryDt DESC";
                    List<dynamic> ls = AABdb.Fetch<dynamic>(query, result.FieldPremi.PolicyOrderNo);
                    if (ls.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(listDoc.First().RemarkFromSA) && !string.IsNullOrWhiteSpace(listDoc.First().RemarkFromSA)
                            && !string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name + " - " + listDoc.First().RemarkFromSA;
                        }
                        else if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name;
                        }
                    }
                    result.Document = listDoc;
                }

                query = @"SELECT fu.FollowUpStatus,fu.FollowUpInfo,fu.IsRenewal,so.Name as Agency,'' as Upliner, fu.SalesOfficerID, so.Name
                        FROM dbo.FollowUp fu  
                        INNER JOIN dbo.SalesOfficer so 
                        ON so.SalesOfficerId=fu.SalesOfficerId
                        WHERE fu.FollowUpNo = @0";
                listDyn = db.Fetch<dynamic>(query, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    foreach (dynamic item in listDyn)
                    {
                        bool isAgency = AABdb.ExecuteScalar<int>(@";SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] eu  WITH ( NOLOCK ) INNER JOIN beyondmoss.AABMobile.dbo.SalesOfficer so ON eu.UserID=so.Email WHERE so.SalesOfficerId=@0", item.SalesOfficerID) > 0 ? true : false;
                        item.Agency = isAgency ? item.Agency : "";
                        item.Upliner = isAgency ? AABdb.ExecuteScalar<string>(@"SELECT TOP 1 RTRIM(d.Name) AS Upliner         
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT' AND b.Cust_Id=@0 OR a.Client_Code=@0", item.SalesOfficerID) : "";
                    }
                    result.FollowUpInfo = listDyn;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public //static 
            List<string> GetApplicationParametersValue(string ParamName)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            List<string> result = new List<string>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                query = @"SELECT ParamValue, ParamValue2 FROM dbo.ApplicationParameters
                        WHERE RowStatus = 1 AND ParamName = @0";
                List<dynamic> listVal = db.Fetch<dynamic>(query, ParamName);
                if (listVal.Count > 0)
                {
                    result.Add(listVal.First().ParamValue);
                    result.Add(listVal.First().ParamValue2);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        #region private
        public static void UpdateImageFollowUp(string followUpID)
        {
            try
            {
                string query = @"SELECT ImageType,PathFile FROM tempimagedata WHERE FollowUpNo = @0";

                List<dynamic> listImageData = new List<dynamic>();
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    listImageData = db.Fetch<dynamic>(query, followUpID);
                }

                string qUpdateFUImg = @"UPDATE FollowUp ";
                #region
                foreach (dynamic a in listImageData)
                {
                    qUpdateFUImg = @"UPDATE FollowUp ";
                    string imageType = Convert.ToString(a.ImageType);
                    switch (imageType)
                    {
                        case "FORMRENEWAL":
                            qUpdateFUImg += @"SET KonfirmasiCust= @0 WHERE FollowUpNo =@1";
                            break;
                        case "RENEWALNOTICE":
                            qUpdateFUImg += @"SET RenewalNotice= @0 WHERE FollowUpNo =@1";
                            break;
                        case "BUKTIBAYAR":
                            qUpdateFUImg += @"SET BuktiBayar= @0 WHERE FollowUpNo =@1";
                            break;
                        case "KTP":
                            qUpdateFUImg += @"SET IdentityCard= @0 WHERE FollowUpNo =@1";
                            break;
                        case "STNK":
                            qUpdateFUImg += @"SET STNK= @0 WHERE FollowUpNo =@1";
                            break;
                        case "SURVEY":
                            qUpdateFUImg += @"SET Survey= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA1":
                            qUpdateFUImg += @"SET DocNSA1= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA2":
                            qUpdateFUImg += @"SET DocNSA2= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA3":
                            qUpdateFUImg += @"SET DocNSA3= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA4":
                            qUpdateFUImg += @"SET DocNSA4= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA5":
                            qUpdateFUImg += @"SET DocNSA5= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA6":
                            qUpdateFUImg += @"SET DocNSA6= @0 WHERE FollowUpNo =@1";
                            break;
                        default:
                            qUpdateFUImg = "";
                            break;
                    }

                    if (!String.IsNullOrEmpty(qUpdateFUImg))
                    {
                        using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                        {
                            db.Execute(qUpdateFUImg, a.PathFile, followUpID);
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static void ClearFailedData(string custID, string followUpID, string orderID)
        {
            #region Query Delete Fail GT
            string qDeleteFailGT = @"
                                               DELETE FROM OrderSimulationCoverage WHERE OrderNo=@2
                                               DELETE FROM OrderSimulationInterest WHERE OrderNo=@2
                                               DELETE FROM OrdersimulationMV WHERE OrderNo=@2
                                               DELETE FROM OrderSimulation WHERE CustId=@0 AND FollowUpNo=@1 AND OrderNo=@2
                                               DELETE FROM FollowUp WHERE CustId=@0 AND FollowUpNo=@1
                                               DELETE FROM ProspectCompany WHERE CustId=@0
                                               DELETE FROM ProspectCustomer WHERE CustId=@0";
            #endregion
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
            {
                db.Execute(qDeleteFailGT, custID, followUpID, orderID);
            }
        }

        public static void InsertMstOrderMobile(FollowUp FU)
        {
            try
            {
                #region insert to MST_ORDER_MOBILE

                if (FU.FollowUpStatus == 2 && (FU.FollowUpInfo == 61 || FU.FollowUpInfo == 57)) //Status Send To SA dan FollowUpPembayaran
                {

                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        using (var db2 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                        {
                            string OrderNo = db2.ExecuteScalar<string>("SELECT PolicyOrderNo FROM OrderSimulation WHERE FollowUpNo=@0", FU.FollowUpNo);
                            if (!string.IsNullOrEmpty(OrderNo))
                            {
                                string prospectId = db2.ExecuteScalar<string>("SELECT prospectId FROM ProspectCustomer WHERE CustId=@0", FU.CustID);
                                string PolicyOrderNo = db2.ExecuteScalar<string>("SELECT PolicyOrderNo FROM OrderSimulation WHERE CustId=@0 AND FollowUpNo=@1", FU.CustID, FU.FollowUpNo);
                                int SAState = 1;
                                if (FU.FollowUpInfo == 57 && Convert.ToBoolean(FU.IsRenewal) && (string.IsNullOrEmpty(FU.DocNSA1) && string.IsNullOrEmpty(FU.DocNSA2) && string.IsNullOrEmpty(FU.DocNSA3)
                                    && string.IsNullOrEmpty(FU.DocNSA4) && string.IsNullOrEmpty(FU.DocNSA5)))
                                {
                                    SAState = 3;
                                }
                                string entryusr = FU.SalesOfficerID.Length > 5 ? "OTOSL" : FU.SalesOfficerID;
                                db.Execute(@"INSERT INTO [AAB].[dbo].[Mst_Order_Mobile] (Order_No,SA_State,Approval_Status,Approval_Process,Approval_Type,isSO,Remarks,Actual_Date,EntryDt,EntryUsr) VALUES(@0,@1,0,0,0,0,@2,GETDATE(),getdate(),@3)
                                    ", OrderNo, SAState, FU.RemarkToSA, entryusr);
                                //RetailRepository.CopyImageDataToMstImage(FU.FollowUpNo, prospectId, "");
                                //RetailRepository.CopyImageDataToMstImage(FU.FollowUpNo, "", PolicyOrderNo);
                                string transOrderNo = db2.ExecuteScalar<string>("SELECT OrderNo FROM OrderSimulation WHERE FollowUpNo=@0", FU.FollowUpNo);
                                Otosales.Logic.OrdLogic.UpdateAABOrderData(transOrderNo);
                            }

                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static void SendPushNotifEditedByOthers(string Editor, string SalesOfficerID, string FollowUpNo)
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    if (!Editor.Trim().Equals(SalesOfficerID.Trim()))
                    {
                        string OrderNo = db.ExecuteScalar<string>("SELECT OrderNo FROM Ordersimulation WHERE FollowUpNo=@0", FollowUpNo);
                        Otosales.Logic.CommonLogic.CreatePushNotification(Otosales.Models.PushNotificationCategory.FOLLOWUPSTATUSCHANGEDBYOTHERS, OrderNo, Editor);
                    }
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static string GetBookingVANo()
        {
            string qGetVANo = @";DECLARE @@VANumber VARCHAR(MAX)
                                EXEC usp_GetVANumber @@VANumber OUTPUT
                                SELECT @@VANumber VirtualAccountNo";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                var result = db.Query<dynamic>(qGetVANo).FirstOrDefault();
                return result.VirtualAccountNo;
            }
        }

        public static void AddVA(string OrderNo, string EntryUser)
        {
            string query = @";exec dbo.usp_GetVATransactionOtosales @0";

            string qInsertAABVA = @";DECLARE @@SuccessStatus AS BIT
                                    EXEC [Finance].[usp_UpsertAAB2000VirtualAccount] 
                                    @@PolicyNo = @0, 
                                    @@VirtualAccountNo = @1, 
                                    @@CurrencyCode = @2,
                                    @@PremiumAmount = @3,
                                    @@PolicyStatusCode = @4,
                                    @@InsuredCode = @5,
                                    @@InsuredName = @6,
                                    @@MarketingCode = @7,
                                    @@SalesmanAdminLogID = @8,
                                    @@InternalDistributionCode = @9,
                                    @@PolicyPeriodFrom = @10,
                                    @@PolicyPeriodTo = @11,
                                    @@GracePeriod = @12, 
                                    @@PaymentDueDate = @13,
                                    @@UserName = @14,
                                    @@PolicyNoRenewalBeyond =@15,
                                    @@OrderNo = @16,
                                    @@ChasisNo = @17,
                                    @@EngineNo = @18,
                                    @@PolicyStatus = @19,
                                    @@CustomerName = @20,
                                    @@PoliceNo = @21,
                                    @@SuccessStatus = @@SuccessStatus OUTPUT
                                    SELECT @@SuccessStatus as UploadStatus ";

            string qUpsertVALog = @";EXEC [usp_InsertAAB2000VirtualAccount_Log] @@PolicyNo = @0,
                          @@VirtualAccountNo = @1,  
                          @@CurrencyCode = @2, 
                          @@PremiumAmount =  @3, 
                          @@PolicyStatusCode = @4, 
                          @@InsuredCode = @5, 
                          @@InsuredName = @6, 
                          @@MarketingCode = @7, 
                          @@SalesmanAdminLogID = @8, 
                          @@InternalDistributionCode = @9, 
                          @@PolicyPeriodFrom = @10, 
                          @@PolicyPeriodTo = @11, 
                          @@GracePeriod =  @12,  
                          @@PaymentDueDate = @13,                           
                          @@PolicyNoRenewalBeyond =@14, 
                          @@OrderNo = @15, 
                          @@ChasisNo = @16, 
                          @@EngineNo = @17, 
                          @@PolicyStatus = @18, 
                          @@CustomerName = @19, 
                          @@PoliceNo = @20";

            int result = 0;
            VATRansaction VAInfo = null;


            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                VAInfo = db.Query<VATRansaction>(query, OrderNo).FirstOrDefault();
            }
            if (VAInfo != null)
            {
                VAInfo.SalesmanAdminLogID = EntryUser;

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB))
                {
                    //result = db.Execute(qInsertAABVA, IsNA(VAInfo.PolicyNo), IsNA(VAInfo.VirtualAccountNo), IsNA(VAInfo.CurrencyCode), IsNA(VAInfo.PremiumAmount), IsNA(VAInfo.PolicyStatusCode), IsNA(VAInfo.InsuredCode), IsNA(VAInfo.InsuredName), IsNA(VAInfo.MarketingCode), IsNA(VAInfo.SalesmanAdminLogID), IsNA(VAInfo.InternalDistributionCode), IsNA(VAInfo.PolicyPeriodFrom), IsNA(VAInfo.PolicyPeriodTo), IsNA(VAInfo.GracePeriod), IsNA(VAInfo.PaymentDueDate), IsNA(VAInfo.PolicyNoRenewalBeyond), IsNA(VAInfo.OrderNo), IsNA(VAInfo.ChasisNo), IsNA(VAInfo.EngineNo), IsNA(VAInfo.PolicyStatus), IsNA(VAInfo.PoliceNo), IsNA(VAInfo.CustomerName), EntryUser);
                    result = db.Query<int>(qInsertAABVA,
                        VAInfo.PolicyNo == null ? "" : VAInfo.PolicyNo,
                        VAInfo.VirtualAccountNo == null ? "" : VAInfo.VirtualAccountNo,
                        VAInfo.CurrencyCode == null ? "" : VAInfo.CurrencyCode,
                        VAInfo.PremiumAmount == null ? 0 : VAInfo.PremiumAmount,
                        VAInfo.PolicyStatusCode == null ? "" : VAInfo.PolicyStatusCode,
                        VAInfo.InsuredCode == null ? "" : VAInfo.InsuredCode,
                        VAInfo.InsuredName == null ? "" : VAInfo.InsuredName,
                        VAInfo.MarketingCode == null ? "" : VAInfo.MarketingCode,
                        VAInfo.SalesmanAdminLogID == null ? "" : VAInfo.SalesmanAdminLogID,
                        VAInfo.InternalDistributionCode == null ? "" : VAInfo.InternalDistributionCode,
                        VAInfo.PolicyPeriodFrom,
                        VAInfo.PolicyPeriodTo,
                        VAInfo.GracePeriod == null ? 0 : VAInfo.GracePeriod,
                        VAInfo.PaymentDueDate,
                        EntryUser == null ? "" : EntryUser,
                        VAInfo.PolicyNoRenewalBeyond == null ? "" : VAInfo.PolicyNoRenewalBeyond,
                        VAInfo.OrderNo == null ? "" : VAInfo.OrderNo,
                        VAInfo.ChasisNo == null ? "" : VAInfo.ChasisNo,
                        VAInfo.EngineNo == null ? "" : VAInfo.EngineNo,
                        VAInfo.PolicyStatus == null ? "" : VAInfo.PolicyStatus,
                        VAInfo.CustomerName == null ? "" : VAInfo.CustomerName,
                        VAInfo.PoliceNo == null ? "" : VAInfo.PoliceNo).SingleOrDefault();
                }
                if (result == 0)
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        db.Query<dynamic>(qUpsertVALog, VAInfo.PolicyNo, VAInfo.VirtualAccountNo, VAInfo.CurrencyCode, VAInfo.PremiumAmount, VAInfo.PolicyStatusCode, VAInfo.InsuredCode, VAInfo.InsuredName, VAInfo.MarketingCode, VAInfo.SalesmanAdminLogID, VAInfo.InternalDistributionCode, VAInfo.PolicyPeriodFrom, VAInfo.PolicyPeriodTo, VAInfo.GracePeriod, VAInfo.PaymentDueDate, VAInfo.PolicyNoRenewalBeyond, VAInfo.OrderNo, VAInfo.ChasisNo, VAInfo.EngineNo, VAInfo.PolicyStatus, VAInfo.CustomerName, VAInfo.PoliceNo).FirstOrDefault();
                    }
                }
            }
        }
        #endregion
    }
}