﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Microsoft.Reporting.WinForms;
using Otosales.Codes;
using Otosales.Controllers.vWeb2;
using Otosales.dta;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace Otosales.Repository.v0001URF2020
{
    public interface IMobileRepository
    {
        dynamic getFollowUpStatus(string followupstatus, string followupstatusinfo, string isRenewal, string followupno);
        bool CheckPremiItems(string OrderNo);
    }
    public class MobileRepository : IMobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        public static A2isCommonResult SendQuotationEmail(string OrderNo, string Email)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            A2isCommonResult rslt = new A2isCommonResult();
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();
                string DSOrder = "select a.OrderNo, a.EntryDate,b.Name 'CustName',b.Phone1 + case when isnull(b.Phone2,'')='' then'' else '/' + b.Phone2 end 'CustPhone', ";
                DSOrder += " b.Email1 'CustEmail', so.Name 'SalesName', sr.Description 'SalesRole',sr.Role, so.Phone1 + case when isnull(so.Phone2,'')='' then'' else '/' + so.Phone2 end 'SalesPhone',so.Email 'SalesEmail', so.Fax 'SalesFax',";
                DSOrder += " so.OfficePhone 'SalesOfficePhone', so.Ext 'SalesOfficePhoneExt', a.QuotationNo, ab.AccountName, ab.AccountNo, ab.Bank, ab.Description 'Branch', ab.City 'BrachCity',";
                DSOrder += " d.Sequence 'Region', cast(isnull(a.TLOPeriod,0)+isnull(a.ComprePeriod,0) as varchar(10)) + ' Tahun' 'Period',";
                DSOrder += " a.MultiYearF, case when isnull(a.TLOPeriod,0)>0 and isnull(a.ComprePeriod,0)=0 then 'TLO'";
                DSOrder += " when  isnull(a.TLOPeriod,0)=0 and isnull(a.ComprePeriod,0)>0 then 'Comprehensive'";
                DSOrder += " else 'Kombinasi' end 'Pertanggungan', CAST(DAY(GETDATE()) AS VARCHAR(2)) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS [ExecTime], a.InsuranceType, pt.Description AS Product";
                DSOrder += " from OrderSimulation a";
                DSOrder += " inner join ProspectCustomer b on a.CustID=b.CustID";
                DSOrder += " inner join SalesOfficer so on a.SalesOfficerID=so.SalesOfficerID";
                DSOrder += " inner join SysUserRole sr on so.Role=sr.Role";
                DSOrder += " inner join AABBranch ab on so.BranchCode=ab.BranchCode";
                DSOrder += " inner join OrderSimulationMV c on a.OrderNo=c.OrderNo";
                DSOrder += " inner join Region d on c.CityCode=d.RegionCode";
                DSOrder += " inner join ProductType pt on pt.InsuranceType=a.InsuranceType";
                DSOrder += " where a.OrderNo=@0";

                dynamic dtOrder = db.FirstOrDefault<dynamic>(DSOrder, OrderNo);

                string DSVehicle = "select  c.Description + ' ' + d.Description + ' ' + e.Series + ' ' + b.Year 'VehicleDescription',";
                DSVehicle += " a.RegistrationNumber, a.Year, f.Description 'Usage', a.sitting, a.SumInsured, o.YearCoverage,c.Description AS Brand, d.Description AS Model,b.Type,vt.Description AS TypeDesc";
                DSVehicle += " from OrderSimulation o ";
                DSVehicle += " inner join OrderSimulationMV a on a.OrderNo=o.OrderNo";
                DSVehicle += " inner join Vehicle b on a.VehicleCode=b.VehicleCode and a.BrandCode=b.BrandCode and a.ProductTypeCode=b.ProductTypeCode";
                DSVehicle += " and a.ModelCode=b.ModelCode and a.Series=b.Series and a.Type=b.Type and a.Sitting=b.Sitting and a.Year=b.Year";
                DSVehicle += " inner join VehicleBrand c on a.BrandCode=c.BrandCode";
                DSVehicle += " inner join VehicleModel d on a.BrandCode=d.BrandCode and a.ModelCode=d.ModelCode";
                DSVehicle += " inner join VehicleSeries e on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series";
                DSVehicle += " inner join VehicleType vt on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series AND vt.VehicleTypeCode=a.Type";
                DSVehicle += " inner join Dtl_Ins_Factor f on a.UsageCode=f.insurance_code and f.Factor_Code='MVUSAG'";
                DSVehicle += " where a.OrderNo=@0";

                dynamic dtVehicle = db.FirstOrDefault<dynamic>(DSVehicle, OrderNo);

                string NameCust = dtOrder.CustName;
                string NameProduct = dtOrder.Product;
                string PeriodePertanggungan = dtOrder.Period;
                string NamaSales = dtOrder.SalesName;
                string NomorSales = dtOrder.SalesPhone;
                string EmailSales = dtOrder.SalesEmail;
                string Brand = dtVehicle.Brand;
                string Model = dtVehicle.Model;
                string Type = dtVehicle.TypeDesc;
                string Kontak1 = ConfigurationManager.AppSettings["ContactSales" + dtOrder.Role].ToString();
                Kontak1 = Kontak1.Replace("@Nama", NamaSales).Replace("@Email", EmailSales).Replace("@Phone", NomorSales);

                string Kontak2 = ConfigurationManager.AppSettings["ContactInfo" + dtOrder.InsuranceType].ToString();
                //int yearCoverage = Convert.ToInt32(dtVehicle.YearCoverage);
                int insuranceType = Convert.ToInt32(dtOrder.InsuranceType);
                List<A2isMessagingAttachment> attFile = new List<A2isMessagingAttachment>();
                // int yearCoverage = 1;
                attFile.Add(Otosales.Repository.v0219URF2019.MobileRepository.GenerateReportFile(OrderNo, insuranceType));

                #region Ikhtisar Product
                //A2isMessagingAttachment attFileProduct = new A2isMessagingAttachment();

                //string endPath = "";

                //if (insuranceType == 1) // GardaOto
                //{
                //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductGardaotoKonve"].ToString()).First();
                //}
                //else if (insuranceType == 2) // Toyota Insurance
                //{
                //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductToyotaIns"].ToString()).First();
                //}
                //else if (insuranceType == 3) // Lexus Insurance
                //{
                //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductLexusIns"].ToString()).First();
                //}
                //else if (insuranceType == 4) // Garda Oto Syariah
                //{
                //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductGardaotoSyariah"].ToString()).First();
                //}

                //attFileProduct.FileByte = GetByte(AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesPathFile"].ToString()).First(), endPath);
                //attFileProduct.AttachmentDescription = "Ikhtisar Product";
                //attFileProduct.FileContentType = "image/png";
                //attFileProduct.FileExtension = ".png";

                //attFile.Add(attFileProduct);
                #endregion


                EmailSubject = EmailSubject.Replace("@Product", dtOrder.Product).Replace("@Customer", NameCust);
                if (!string.IsNullOrEmpty(EmailSubject))
                {
                    string EmailBody = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='CONTENT-QUOTATIONEMAIL' AND ParamType='TMP'");
                    string emailText = EmailBody.Replace("@Product", NameProduct).Replace("@Contact1", Kontak1).Replace("@Contact2", Kontak2).Replace("@Brand", Brand).Replace("@Type", Type).Replace("@Model", Model);
                    rslt = Controllers.vWeb2.MessageController.SendEmail(Email, "", "", emailText, EmailSubject, attFile);

                }
                if (rslt.ResultCode)
                {
                    string query = @"UPDATE ORDERSIMULATION SET SendStatus=1, SendDate=GETDATE() WHERE OrderNo=@0";
                    db.Execute(query, OrderNo);
                    //                    bool isInfoChanged = false;
                    //                    FollowUp FU = db.FirstOrDefault<FollowUp>("SELECT * FROM FollowUp WHERE FollowUpNo=(SELECT TOP 1 FollowUpNo From OrderSimulation WHERE OrderNo=@0)", OrderNo);
                    //                    if (FU.FollowUpStatus < 4 || FU.FollowUpStatus == 9)
                    //                    {

                    ////                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
                    ////BEGIN
                    ////SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
                    ////END
                    ////ELSE
                    ////BEGIN
                    ////SELECT 1 AS SeqNo
                    ////END", FU.FollowUpNo);
                    ////                        if (FU.FollowUpInfo != 3)
                    ////                        {
                    ////                            isInfoChanged = true;
                    ////                            lastseqno++;
                    ////                        }
                    ////                        FU.LastSeqNo = lastseqno;
                    ////                        FU.FollowUpStatus = 2;
                    ////                        FU.FollowUpInfo = 3;
                    ////                        query = @"UPDATE FollowUp SET 
                    ////                           LastSeqNo=@1,
                    ////                           FollowUpStatus=@2,
                    ////                           FollowUpInfo=@3,
                    ////                           NextFollowUpDate=GETDATE(),
                    ////                           LastFollowUpDate=GETDATE()
                    ////                           WHERE FollowUpNo=@0
                    ////                           ";
                    ////                        db.Execute(query, FU.FollowUpNo, FU.LastSeqNo, FU.FollowUpStatus, FU.FollowUpInfo);
                    ////                        if (isInfoChanged)
                    ////                        {
                    ////                            #region INSERT FOLLOW UP HISTORY
                    ////                            query = @";
                    ////    INSERT INTO FollowUpHistory (
                    ////       [FollowUpNo]
                    ////      ,[SeqNo]
                    ////      ,[CustID]
                    ////      ,[ProspectName]
                    ////      ,[Phone1]
                    ////      ,[Phone2]
                    ////      ,[SalesOfficerID]
                    ////      ,[EntryDate]
                    ////      ,[FollowUpName]
                    ////      ,[NextFollowUpDate]
                    ////      ,[LastFollowUpDate]
                    ////      ,[FollowUpStatus]
                    ////      ,[FollowUpInfo]
                    ////      ,[Remark]
                    ////      ,[BranchCode]
                    ////      ,[LastUpdatedTime]
                    ////      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),0)
                    ////";

                    ////                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
                    ////                            #endregion
                    ////                        }
                    //                    }
                }
                return rslt;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static A2isCommonResult SendViaEmail(string EmailTo, string EmailCC, string EmailBCC
            , string body, string subject, List<A2isMessagingAttachment> att, string OrderNo, bool IsAddAtt)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            A2isCommonResult res = new A2isCommonResult();
            try
            {
                string qGetInsType = @"SELECT COALESCE(InsuranceType,0) InsuranceType FROM dbo.OrderSimulation WHERE OrderNo = @0";
                int insuranceType = db.ExecuteScalar<int>(qGetInsType, OrderNo);
                if (att == null)
                {
                    att = new List<A2isMessagingAttachment>();
                }
                if (IsAddAtt)
                {
                    att.Add(Otosales.Repository.v0219URF2019.MobileRepository.GenerateReportFile(OrderNo, insuranceType));

                    #region Ikhtisar Product
                    //A2isMessagingAttachment attFileProduct = new A2isMessagingAttachment();

                    //string endPath = "";

                    //if (insuranceType == 1) // GardaOto
                    //{
                    //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductGardaotoKonve"].ToString()).First();
                    //}
                    //else if (insuranceType == 2) // Toyota Insurance
                    //{
                    //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductToyotaIns"].ToString()).First();
                    //}
                    //else if (insuranceType == 3) // Lexus Insurance
                    //{
                    //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductLexusIns"].ToString()).First();
                    //}
                    //else if (insuranceType == 4) // Garda Oto Syariah
                    //{
                    //    endPath = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesIkhtisarProductGardaotoSyariah"].ToString()).First();
                    //}

                    //attFileProduct.FileByte = GetByte(AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesPathFile"].ToString()).First(), endPath);
                    //attFileProduct.AttachmentDescription = "Ikhtisar Product";
                    //attFileProduct.FileContentType = "image/png";
                    //attFileProduct.FileExtension = ".png";

                    //att.Add(attFileProduct);
                    #endregion

                }
                res = MessageController.SendEmail(EmailTo, EmailCC, EmailBCC, body, subject, att);
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static byte[] GetByte(string basePath, string endPath)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            byte[] result = null;
            string fullPath = string.Empty;
            try
            {
                string path = basePath;
                string end = endPath;
                fullPath = new Uri(path + end).LocalPath;

                result = File.ReadAllBytes(fullPath);
            }
            catch (Exception Ex)
            {
                //result = File.ReadAllBytes(fullPath);
                try
                {
                    using (FileStream fileStream = File.OpenRead(fullPath))
                    {
                        //create new MemoryStream object
                        MemoryStream memStream = new MemoryStream();
                        memStream.SetLength(fileStream.Length);
                        //read file to MemoryStream
                        fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                        result = memStream.ToArray();
                    }
                }
                catch (Exception E)
                {

                    logger.Error(string.Format("Function {0} URL :{1} Error : {2} ", CurrentMethod, fullPath, E));
                    //throw;
                }
                logger.Error(string.Format("Function {0} URL :{1} Error : {2} ", CurrentMethod, fullPath, Ex));
            }
            return result;
        }

        internal static dynamic GetSurveyScheduleSurveyManagement(string cityid, string zipcode, DateTime d)
        {
            dynamic result = null;
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            string qGetSurveyAddress = @"SELECT AreaCode GeoAreaCode FROM SurveyManagement.dbo.MappingSurveyAreaZipCode where ZipCode = @0 AND RowStatus = 0";
            List<dynamic> addresses = AABDB.Fetch<dynamic>(qGetSurveyAddress, zipcode);
            if (addresses.Count > 0)
            {
                string GeoAreaCode = addresses.First().GeoAreaCode;
                if (!string.IsNullOrEmpty(GeoAreaCode))
                {
                    string QueryGetGAScheduleWithDate = @";SELECT  @0 AS SurveyDate
        INTO    #SurveyDate

            SELECT  CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
						 WHEN tc.TimeCategoryCode = 'TC2' THEN 40
						 WHEN tc.TimeCategoryCode = 'TC3' THEN 41
					END ScheduleTime ,
					ZipCode ,
					msast.AreaCode SurveyareaCode ,
					CityId ,
					TC.TimeCategoryDescription ScheduleTimeTextID ,
					CASE WHEN tc.TimeCategoryCode = 'TC1'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
						 WHEN tc.TimeCategoryCode = 'TC2'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
						 WHEN tc.TimeCategoryCode = 'TC3'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
					END ScheduleTimeTextEN ,
                    msast.Quota ,
                    ( SELECT    COUNT(SurveyOrderID)
                      FROM      SurveyManagement.dbo.SurveyOrder AS so
                      WHERE     so.RowStatus = 0
                                AND so.IsRescheduleF = 0
                                AND so.SurveyDate = sd.SurveyDate
                                AND so.AreaCode = msast.AreaCode
                                AND so.SurveyTime >= msast.Start
                                AND so.SurveyTime < msast.[End]
                    ) AS slotbooked
            INTO    #DateSlot
            FROM    #SurveyDate AS sd
                    FULL JOIN SurveyManagement.dbo.MappingSurveyAreaSlotTime AS msast ON 1 = 1
					INNER JOIN ( SELECT DISTINCT
                            PostalCode ZipCode,
                            CityID,
							AreaCode
                     FROM   BEYONDMOSS.Asuransiastra.dbo.Postal p
					 INNER JOIN SurveyManagement.dbo.MappingSurveyAreaZipCode msazc
					 ON p.PostalCode = msazc.ZipCode AND p.RowStatus = 0
                   ) c ON c.AreaCode = msast.AreaCode
				   LEFT JOIN BEYONDMOSS.GardaAkses.GA.TimeCategory tc ON TC.ScheduleTime =  SUBSTRING(CONVERT(VARCHAR, msast.Start), 1, 5)
            WHERE   msast.AreaCode = @1
                    AND msast.RowStatus = 0
					AND c.ZipCode = @2

			IF (CAST(@0 AS DATE) = CAST(GETDATE() AS DATE))
			BEGIN
				SELECT * FROM #DateSlot
			END
			ELSE
			BEGIN
				SELECT * FROM #DateSlot
				WHERE Quota - slotbooked >0
			END
    DROP TABLE #SurveyDate
    DROP TABLE #DateSlot";
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        string AvailableDate = d.ToString("yyyy-MM-dd");
                        result = db.Fetch<dynamic>(QueryGetGAScheduleWithDate, AvailableDate, GeoAreaCode, zipcode);
                    }
                }
            }

            return result;

        }
        internal static dynamic GetScheduleSurveyDalam(List<string> TimeCodeSurveyDalam)
        {
            dynamic result = null;

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAAKSES))
            {
                string queryGetScheduleSurveyDalam = @"
SELECT 
        CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
             WHEN tc.TimeCategoryCode = 'TC2' THEN 40
             WHEN tc.TimeCategoryCode = 'TC3' THEN 41
             WHEN tc.TimeCategoryCode = 'TC4' THEN 42
        END ScheduleTime ,
        TC.TimeCategoryDescription ScheduleTimeTextID ,
        CASE WHEN tc.TimeCategoryCode = 'TC1'
             THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
             WHEN tc.TimeCategoryCode = 'TC2'
             THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
             WHEN tc.TimeCategoryCode = 'TC3'
             THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
             WHEN tc.TimeCategoryCode = 'TC4'
             THEN REPLACE(TC.TimeCategoryDescription, 'Malam', 'Night')
        END ScheduleTimeTextEN
FROM    GA.TimeCategory TC WHERE  timecategorycode in (@0)";
                result = db.Fetch<dynamic>(queryGetScheduleSurveyDalam, TimeCodeSurveyDalam.Count > 0 ? TimeCodeSurveyDalam : new List<string>(new string[] { "0" }));
                return result;
            }
        }

        internal static dynamic GetSurveyTimeByZoneNow(string cityId, dynamic timeObjects, DateTime timeSurvey)
        {
            List<dynamic> result = new List<dynamic>();


            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            #region
            string query = @";DECLARE 
	@@CityName VARCHAR(MAX),
	@@IdCity VARCHAR(MAX),
	@@OptionName VARCHAR(MAX),
	@@GMT VARCHAR(MAX)

SET @@IdCity = @1
SET @@OptionName = @0

SELECT  @0 = COALESCE(OptionValue, '')
FROM    [AsuransiAstra].[dbo].[AsuransiAstraOptions]
WHERE   OptionName = @@OptionName;

SELECT  @@CityName = result.NAME
FROM    ( SELECT    c.ID ,
                    c.NAME
          FROM      Asuransiastra.dbo.Branch b
                    INNER JOIN Asuransiastra.dbo.City c ON b.CITYID = c.ID
          WHERE     b.type IN ( 2, 5, 6 )
                    AND b.ISDELETED = 0
          UNION
          SELECT    a.ID ,
                    a.NAME
          FROM      Asuransiastra.dbo.City a
                    INNER JOIN Asuransiastra.GODigital.Fn_SplitString(@0, ',') b ON b.part = a.ID
        ) result
WHERE ID = @@IdCity

SELECT @@GMT = GMT FROM MstZonaTime WHERE District LIKE '%' + @@CityName + '%'

SELECT 
	CASE 
	WHEN @@GMT IS NULL THEN '7' 
	ELSE @@GMT
	END AS GMT";
            #endregion

            int getGMT = AABMobileDB.FirstOrDefault<int>(query, AppParameterAccess.GetAppParamValue("SURVEY-CITY-DEFAULT-LIST"), cityId);
            DateTime datetimeNow = DateTime.Now.AddHours(getGMT - 7);
            timeSurvey = DateTime.ParseExact(timeSurvey.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

            foreach (dynamic timeObject in timeObjects)
            {
                if (timeSurvey <= datetimeNow)
                {
                    #region logic gendeng
                    var Hour = datetimeNow.Hour;
                    var Minutes = datetimeNow.Minute;
                    var Minutestr = (Minutes < 10) ? "0" + Minutes : Minutes.ToString();
                    int datahour = Int32.Parse(Hour + "" + Minutestr);
                    string[] datadesc = timeObject.ScheduleTimeTextID.ToString().Split('–');
                    #endregion

                    int data1 = Int32.Parse(Regex.Replace(datadesc[0], @"[^\d]", ""));
                    int data2 = Int32.Parse(Regex.Replace(datadesc[1], @"[^\d]", ""));

                    if ((data1 <= datahour && data2 >= datahour) || data1 >= datahour)
                    {
                        result.Add(timeObject);
                    }
                }
                else
                {
                    result.Add(timeObject);
                }
            }

            return result;
        }

        internal static dynamic GetValidateTimeSurvey(string cityId, string ScheduleTimeDesc, DateTime timeSurvey, string custid, string FollowUpNo)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();

            result.Add("isValidTimeSurvey", true);
            result.Add("message", null);

            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            timeSurvey = DateTime.ParseExact(timeSurvey.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

            try
            {
                #region
                string query = @";DECLARE 
	@@CityName VARCHAR(MAX),
	@@IdCity VARCHAR(MAX),
	@@OptionName VARCHAR(MAX),
	@@GMT VARCHAR(MAX)

SET @@IdCity = @1
SET @@OptionName = @0

SELECT  @0 = COALESCE(OptionValue, '')
FROM    [AsuransiAstra].[dbo].[AsuransiAstraOptions]
WHERE   OptionName = @@OptionName;

SELECT  @@CityName = result.NAME
FROM    ( SELECT    c.ID ,
                    c.NAME
          FROM      Asuransiastra.dbo.Branch b
                    INNER JOIN Asuransiastra.dbo.City c ON b.CITYID = c.ID
          WHERE     b.type IN ( 2, 5, 6 )
                    AND b.ISDELETED = 0
          UNION
          SELECT    a.ID ,
                    a.NAME
          FROM      Asuransiastra.dbo.City a
                    INNER JOIN Asuransiastra.GODigital.Fn_SplitString(@0, ',') b ON b.part = a.ID
        ) result
WHERE ID = @@IdCity

SELECT @@GMT = GMT FROM MstZonaTime WHERE District LIKE '%' + @@CityName + '%'

SELECT 
	CASE 
	WHEN @@GMT IS NULL THEN '7' 
	ELSE @@GMT
	END AS GMT";
                #endregion

                int getGMT = AABMobileDB.FirstOrDefault<int>(query, AppParameterAccess.GetAppParamValue("SURVEY-CITY-DEFAULT-LIST"), cityId);
                DateTime datetimeNow = DateTime.Now.AddHours(getGMT - 7);


                if (timeSurvey <= datetimeNow && !(string.IsNullOrEmpty(ScheduleTimeDesc) || ScheduleTimeDesc.Equals("null") || ScheduleTimeDesc.Equals("undefined")))
                {

                    string querySurvey = @"SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc, CAST(COALESCE(IsManualSurvey,0) AS BIT) IsManualSurvey
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                    Otosales.Models.vWeb2.SurveySchedule SurveySchedule = AABMobileDB.Fetch<Otosales.Models.vWeb2.SurveySchedule>(querySurvey, custid, FollowUpNo).FirstOrDefault();

                    #region logic gendeng
                    var Hour = datetimeNow.Hour;
                    var Minutes = datetimeNow.Minute;
                    var Minutestr = (Minutes < 10) ? "0" + Minutes : Minutes.ToString();
                    int datahour = Int32.Parse(Hour + "" + Minutestr);
                    string[] datadesc = ScheduleTimeDesc.ToString().Split('-');
                    #endregion

                    int data1 = Int32.Parse(Regex.Replace(datadesc[0], @"[^\d]", ""));
                    int data2 = Int32.Parse(Regex.Replace(datadesc[1], @"[^\d]", ""));

                    string LabelMaxTimeSurvey = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["MaxSurveyLuarOtosales"].ToString()).First();
                    int MaxSurveyOthersTime = Int32.Parse(Regex.Replace(LabelMaxTimeSurvey, @"[^\d]", ""));

                    if (!((data1 <= datahour && data2 >= datahour) || data1 >= datahour))
                    {
                        result["isValidTimeSurvey"] = false;
                        result["message"] = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesWordingSurveyTimeWrong"].ToString()).First();
                    }

                    if (SurveySchedule.LocationCode.Equals("Others") && datahour >= MaxSurveyOthersTime) // Survey luar lebih jam 14.00
                    {
                        result["isValidTimeSurvey"] = false;
                        result["message"] = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesWordingSurveyTimeExpired"].ToString()).First().ToString().Replace("#SURVEYOTHER", LabelMaxTimeSurvey);
                    }
                }

                if ((string.IsNullOrEmpty(ScheduleTimeDesc) || ScheduleTimeDesc.ToLower().Equals("null") || ScheduleTimeDesc.Equals("undefined")))
                {
                    result["isValidTimeSurvey"] = false;
                    result["message"] = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesWordingSurveyTimeWrong"].ToString()).First();
                }
            }
            catch (Exception e)
            {
                result["isValidTimeSurvey"] = false;
                result["message"] = e.ToString();
            }

            return result;
        }

        internal static string GetTemplateCustomnerConfirmation(string OrderNo, int typeTemplate)
        {
            string result = "";

            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            AABMobileDB.Execute(@";
UPDATE dbo.OrderSimulation
SET IsGenerateCustomerConfirmation = 1
WHERE OrderNo = @0
", OrderNo);

            #region query
            var query = @";
SELECT CASE WHEN ( fu.IsRenewal IS NULL ) THEN 0
             ELSE fu.IsRenewal
        END IsRenewal ,
        os.SalesDealer ,
        os.DealerCode ,
        fu.FollowUpName ,
        d.Description DealerName ,
        sd.Description SalesmanDealerName ,
        CASE WHEN ( pc.isCompany = 1 ) THEN pcc.PICname
             ELSE fu.ProspectName
        END NameOnTemplate ,
		os.QuotationNo
FROM    dbo.FollowUp fu
        INNER JOIN dbo.ProspectCustomer pc ON pc.CustID = fu.CustID
        INNER JOIN OrderSimulation os ON fu.FollowUpNo = os.FollowUpNo
                                         AND os.ApplyF = 1 AND os.RowStatus = 1
        LEFT JOIN dbo.SalesmanDealer sd ON sd.SalesmanCode = os.SalesDealer
                                            AND sd.RowStatus = 1
        LEFT JOIN Dealer d ON d.DealerCode = os.DealerCode
                               AND d.RowStatus = 1
        LEFT JOIN dbo.ProspectCompany pcc ON pcc.CustID = pc.CustID
WHERE   os.OrderNo = @0
";
            #endregion

            dynamic resQuery = AABMobileDB.FirstOrDefault<dynamic>(query, OrderNo);

            // typeTemplate == 1 => Customer
            // typeTemplate == 0 => Intermediary
            if (resQuery.IsRenewal == 0 && typeTemplate == 1)
            {
                result = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesCustConfNewCustomer"].ToString()).First();
            }
            else if (resQuery.IsRenewal == 1 && typeTemplate == 0)
            {
                result = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesCustConfRenewIntermediary"].ToString()).First();
            }
            else if (resQuery.IsRenewal == 0 && typeTemplate == 0)
            {
                result = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesCustConfNewIntermediary"].ToString()).First();
            }
            else if (resQuery.IsRenewal == 1 && typeTemplate == 1)
            {
                result = AppParameterAccess.GetApplicationParametersValue(ConfigurationManager.AppSettings["OtosalesCustConfRenewCustomer"].ToString()).First();
            }
            string salesdealername = string.IsNullOrEmpty(resQuery.SalesmanDealerName) ? resQuery.SalesmanDealerName : Convert.ToString(resQuery.SalesmanDealerName).Split('-')[1].Trim();
            result = result.Replace("#CUSTOMER", resQuery.NameOnTemplate).Replace("#QUOTATIONNO", resQuery.QuotationNo).Replace("#SALESMANDEALERNAME", salesdealername);


            return result;
        }

        internal static string GetTemplateCustomnerConfirmationSP(string OrderNo, int typeTemplate, string time, string date)
        {
            dynamic result = "";

            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            result = AABMobileDB.FirstOrDefault<dynamic>(";EXEC dbo.usp_OrderSimulationGenerateTemplateConfirmation @0, @1, @2, @3", OrderNo, typeTemplate, time, date);

            result = result.Result;

            return result;
        }

        public static bool ShowIntermediaryConfirmationCustomer(string ProductCode, int IsAgency)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                query = ";EXEC usp_IsShowIntermediaryConfirmationCustomer @0, @1";
                dynamic Res = db.FirstOrDefault<dynamic>(query, IsAgency, ProductCode);
                if (Res.Result == 1)
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static Otosales.Models.vWeb2.TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            //List<dynamic> listDyn = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
            string query = "";

            try
            {
                Thread threadPremi = new Thread(() =>
                {
                    Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
                    string queryFieldPremi = @";DECLARE @@Admin DECIMAL
                        --DECLARE @@GrossPremium DECIMAL
                        --DECLARE @@NetPremi DECIMAL
                        DECLARE @@OrderNo VARCHAR(100)
                        DECLARE @@PolicyNo VARCHAR(100)
                        DECLARE @@PolicyOrderNo VARCHAR(100)
                        DECLARE @@SurveyNo VARCHAR(100)
                        DECLARE @@OldPolicyNo VARCHAR(100)

                        SELECT @@Admin=AdminFee,@@OrderNo=OrderNo, @@PolicyNo=PolicyNo, @@PolicyOrderNo=PolicyOrderNo, 
                        @@SurveyNo=SurveyNo, @@OldPolicyNo=OldPolicyNo
                        FROM dbo.OrderSimulation 
                        WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1
                        --SELECT @@GrossPremium=SUM(Gross_Premium) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @@OrderNo AND RowStatus = 1 
                        --SELECT @@NetPremi=@@GrossPremium+@@Admin

                        SELECT 0 GrossPremium, CAST(@@Admin AS DECIMAL) Admin, 0 NetPremi,
						@@PolicyNo AS PolicyNo, @@OrderNo AS OrderNo, @@PolicyOrderNo AS PolicyOrderNo, 
                        @@SurveyNo AS SurveyNo, COALESCE(@@OldPolicyNo,'') AS OldPolicyNo, 0 NoClaimBonus, NULL OldPolicyPeriodTo";

                    var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                    var dbFieldPremiAABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                    Otosales.Models.vWeb2.FieldPremi premi = dbFieldPremiMobile.Query<Otosales.Models.vWeb2.FieldPremi>(queryFieldPremi, CustID, FollowUpNo).FirstOrDefault();
                    if (premi != null)
                    {
                        if (!string.IsNullOrEmpty(premi.OldPolicyNo))
                        {
                            List<dynamic> ls = new List<dynamic>();
                            //query = @"SELECT COALESCE(SUM(Amount), 0) AS NCB FROM dbo.Ren_Cover_Disc WHERE Policy_No = @0";
                            //ls = AABdb.Fetch<dynamic>(query, listDyn.First().OldPolicyNo);
                            //if (ls.Count > 0)
                            //{
                            //    premi.NoClaimBonus = ls.First().NCB;
                            //}
                            queryFieldPremi = @";SELECT Period_To FROM dbo.Policy WHERE Policy_No = @0";

                            ls = dbFieldPremiAABdb.Fetch<dynamic>(queryFieldPremi, premi.OldPolicyNo);
                            if (ls.Count > 0)
                            {
                                premi.OldPolicyPeriodTo = ls.First().Period_To;
                            }
                        }
                        result.FieldPremi = premi;
                    }

                    if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                    {
                        queryFieldPremi = @";SELECT STUFF(( SELECT ', ' + CONCAT(Name,' ',Description,' ',Quantity,' No Cover')
                                    FROM dbo.Ord_Dtl_NoCover 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '') AS NoCover,
                        STUFF(( SELECT ', ' + [Description]
                                    FROM dbo.Ord_Object_Clause 
			                        WHERE Order_No = @0
								    AND Clause_Id = 'OMV99'
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS OriginalDefect,
                        STUFF(( SELECT ', ' + Item_Name
                                    FROM dbo.Ord_Dtl_Interest_Item 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS Accessories,
                        ms.[Description] AS SurveyStatus,
                        s.ResultRecommendation AS SurveyorRecomendation,
                        s.remarks AS Remaks,
                        mu.[User_Name] AS Surveyor
                        FROM dbo.Survey s 
						INNER JOIN dbo.Lnk_Survey_Order lso
						ON s.Survey_No = lso.Survey_No
                        INNER JOIN dbo.Mst_Order mo 
                        ON mo.Order_No = s.Reference_No
                        INNER JOIN dbo.Mst_State ms 
                        ON ms.[Type] = 'survey' 
                        AND ms.[State] = s.[Status]
                        INNER JOIN dbo.Mst_User mu
                        ON mu.[User_Id] = s.Surveyor
                        WHERE mo.Order_No = @0";
                        Otosales.Models.vWeb2.SurveyData sd = dbFieldPremiAABdb.Query<Otosales.Models.vWeb2.SurveyData>(queryFieldPremi, result.FieldPremi.PolicyOrderNo).FirstOrDefault();
                        result.SurveyData = sd;
                    }

                    string queryFollowUpInfo = @";SELECT fu.FollowUpStatus,fu.FollowUpInfo,fu.IsRenewal,so.Name as Agency,'' as Upliner, fu.SalesOfficerID, so.Name
                        FROM dbo.FollowUp fu  
                        INNER JOIN dbo.SalesOfficer so 
                        ON so.SalesOfficerId=fu.SalesOfficerId
                        WHERE fu.FollowUpNo = @0";
                    List<dynamic> listDynFollowUpInfo = dbFieldPremiMobile.Fetch<dynamic>(queryFollowUpInfo, FollowUpNo);
                    if (listDynFollowUpInfo.Count > 0)
                    {
                        foreach (dynamic item in listDynFollowUpInfo)
                        {
                            bool isAgency = dbFieldPremiAABdb.ExecuteScalar<int>(@";SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] eu  WITH ( NOLOCK ) INNER JOIN beyondmoss.AABMobile.dbo.SalesOfficer so ON eu.UserID=so.Email WHERE so.SalesOfficerId=@0", item.SalesOfficerID) > 0 ? true : false;
                            item.Agency = isAgency ? item.Agency : "";
                            item.Upliner = isAgency ? dbFieldPremiAABdb.ExecuteScalar<string>(@";SELECT TOP 1 RTRIM(d.Name) AS Upliner         
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT' AND b.Cust_Id=@0 OR a.Client_Code=@0", item.SalesOfficerID) : "";
                        }
                        result.FollowUpInfo = listDynFollowUpInfo;
                    }
                });

                Thread threadPersonalData = new Thread(() =>
                {
                    string queryPersonalData = @";SELECT p.Name,p.CustBirthDay,p.CustGender,p.CustAddress,p.PostalCode,
                        p.Email1,p.Phone1,p.IdentityNo, CAST(COALESCE(p.isCompany,0) AS BIT) isCompany, 
                        CAST(0 AS BIT) isNeedDocRep, 0 AmountRep, IsPayerCompany
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
                        WHERE p.CustID = @0 AND p.RowStatus = 1";

                    var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.PersonalData pd = dbFieldPremiMobile.Query<Otosales.Models.vWeb2.PersonalData>(queryPersonalData, CustID).FirstOrDefault();
                    if (pd != null)
                    {
                        queryPersonalData = @";SELECT CAST(COALESCE(os.IsNeedDocRep,0) AS BIT)IsNeedDocRep, 
                            COALESCE(os.AmountRep, 0) AmountRep
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
						INNER JOIN dbo.OrderSimulation os
						ON os.FollowUpNo = f.FollowUpNo
                        WHERE p.CustID = @0 AND p.RowStatus = 1
						AND os.RowStatus = 1 AND os.ApplyF = 1";
                        List<dynamic> listDyn = dbFieldPremiMobile.Fetch<dynamic>(queryPersonalData, CustID);
                        if (listDyn.Count > 0)
                        {
                            pd.isNeedDocRep = listDyn.First().IsNeedDocRep;
                            pd.AmountRep = listDyn.First().AmountRep;
                        }
                        result.PersonalData = pd;
                    }
                });

                #region threadPersonalDocument
                //Thread threadPersonalDocument = new Thread(() =>
                //{
                //    string queryPersonalDocument = @";SELECT IdentityCard, id1.Data IdentityCardData,
                //        f.STNK, id2.Data STNKData,
                //        f.BSTB1 BSTB, id3.Data BSTBData,
                //        f.KonfirmasiCust, id4.Data KonfirmasiCustData,
                //        f.DocRep, id5.Data DocRepData
                //        FROM dbo.FollowUp f
                //        LEFT JOIN dbo.ImageData id1
                //        ON f.IdentityCard = id1.PathFile
                //        LEFT JOIN dbo.ImageData id2
                //        ON f.STNK = id2.PathFile
                //        LEFT JOIN dbo.ImageData id3
                //        ON f.BSTB1 = id3.PathFile
                //        LEFT JOIN dbo.ImageData id4
                //        ON f.KonfirmasiCust = id4.PathFile
                //        LEFT JOIN dbo.ImageData id5
                //        ON f.DocRep = id5.PathFile
                //        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";

                //    var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                //    List<dynamic> listDyn = dbFieldPremiMobile.Fetch<dynamic>(queryPersonalDocument, CustID, FollowUpNo);
                //    if (listDyn.Count > 0)
                //    {
                //        result.PersonalDocument = listDyn;
                //    }
                //});
                #endregion

                Thread threadCompanyData = new Thread(() =>
                {
                    string queryCompanyData = @";SELECT CompanyName,NPWPno,NPWPdate,NPWPaddress NPWPaddres,OfficeAddress
                            ,PostalCode,PICPhoneNo,PICname,Email PICEmail FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";

                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.CompanyData cd = dbMobile.Query<Otosales.Models.vWeb2.CompanyData>(queryCompanyData, CustID, FollowUpNo).FirstOrDefault();
                    if (cd != null)
                    {
                        result.CompanyData = cd;
                    }
                    //queryCompanyData = @";IF EXISTS(SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1)
                    //    BEGIN
                    //     SELECT pc.NPWP, id1.Data NPWPData,
                    //     pc.SIUP, id2.Data SIUPData
                    //     FROM dbo.ProspectCompany pc
                    //     LEFT JOIN dbo.ImageData id1
                    //     ON pc.NPWP = id1.PathFile
                    //     LEFT JOIN dbo.ImageData id2
                    //     ON pc.SIUP = id2.PathFile
                    //     WHERE pc.CustID = @0 AND pc.FollowUpNo = @1 AND pc.RowStatus = 1
                    //    END
                    //    ELSE
                    //    BEGIN
                    //     SELECT NULL NPWP, NULL NPWPData,
                    //     NULL SIUP, NULL SIUPData
                    //    END";
                    //listDyn = dbMobile.Fetch<dynamic>(queryCompanyData, CustID, FollowUpNo);
                    //if (listDyn.Count > 0)
                    //{
                    //    result.CompanyDocument = listDyn;
                    //}
                });

                Thread threadVehicleData = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                    string queryVehicleData = @";SELECT c.Description + ' ' + d.Description + ' ' + omv.Year + ' ' + a.Series  AS Vehicle,
                        omv.VehicleCode, omv.BrandCode, os.ProductTypeCode, omv.ModelCode,  omv.Series, 
                        omv.Type, CAST(omv.Sitting AS DECIMAL) Sitting, omv.Year, omv.CityCode, CAST(omv.SumInsured AS DECIMAL) SumInsured, os.InsuranceType,
                        os.VANumber, p.DealerCode, p.SalesDealer, omv.UsageCode, omv.ColorOnBPKB, 
                        omv.RegistrationNumber, omv.EngineNumber, omv.ChasisNumber, omv.IsNew, os.OrderNo,
						os.PolicyNo, os.ProductCode, os.SegmentCode, os.PeriodFrom, os.PeriodTo,
						os.IsORDefectsRepair, IsAccessoriesChange, IsPolicyIssuedBeforePaying, 
                        os.Remarks, f.DocPendukung, id.Data AS DocPendukungData,
                        0 BasicCoverID, '' ORDefectsDesc, '' NoCover
                        FROM Vehicle a 
                        LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
                        AND c.ProductTypeCode = a.ProductTypeCode 
                        LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
                        AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
                        LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                        a.BrandCode=b.BrandCode
                        AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                        AND a.Series=b.Series AND a.Type=b.Type 
                        AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                        AND a.Year > b.Year 
                        INNER JOIN dbo.OrderSimulationMV omv ON omv.VehicleCode = a.VehicleCode
                        INNER JOIN dbo.OrderSimulation os ON os.OrderNo = omv.OrderNo
                        INNER JOIN dbo.ProspectCustomer p ON p.CustID = os.CustID
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						LEFT JOIN dbo.ImageData id ON id.PathFile = f.DocPendukung
                        WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND omv.RowStatus = 1
						AND f.RowStatus = 1 AND os.CustID = @0 AND os.FollowUpNo = @1 AND b.Year IS NULL 
                        ORDER BY a.VehicleCode";
                    Otosales.Models.vWeb2.VehicleData vData = dbMobile.Query<Otosales.Models.vWeb2.VehicleData>(queryVehicleData, CustID, FollowUpNo).FirstOrDefault();
                    if (vData != null)
                    {
                        queryVehicleData = @";DECLARE @@TLOPer INT
                            DECLARE @@ComprePer INT
                            SELECT @@TLOPer=TLOPeriod,@@ComprePer=ComprePeriod FROM dbo.OrderSimulation WHERE OrderNo = @0
                            SELECT ID AS BasicCoverID FROM dbo.Mst_Basic_Cover 
                            WHERE ComprePeriod = @@ComprePer
                            AND TLOPeriod = @@TLOPer";
                        List<dynamic> bscCvr = dbMobile.Fetch<dynamic>(queryVehicleData, vData.OrderNo);
                        if (bscCvr.Count > 1)
                        {
                            queryVehicleData = @"SELECT RTRIM(LTRIM(CoverageID)) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                            List<string> ListCvrID = dbMobile.Fetch<string>(queryVehicleData, vData.OrderNo);
                            if (ListCvrID.Contains("TLO"))
                            {
                                queryVehicleData = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%TLO Others%'";
                                int id = dbMobile.ExecuteScalar<int>(queryVehicleData);
                                vData.BasicCoverID = id;
                            }
                            else if (ListCvrID.Contains("ALLRIK"))
                            {
                                queryVehicleData = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%Comprehensive Others%'";
                                int id = dbMobile.ExecuteScalar<int>(queryVehicleData);
                                vData.BasicCoverID = id;
                            }
                        }
                        else
                        {
                            if (bscCvr.Count > 0)
                            {
                                vData.BasicCoverID = bscCvr.First().BasicCoverID;
                            }
                            else
                            {
                                vData.BasicCoverID = 0;
                            }
                        }

                        queryVehicleData = @";SELECT OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1 ";
                        string OldPolicyNo = dbMobile.FirstOrDefault<string>(queryVehicleData, CustID, FollowUpNo);

                        if (!string.IsNullOrEmpty(OldPolicyNo))
                        {
                            queryVehicleData = @";DECLARE @@PolicyID VARCHAR(100)
                                SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
                                SELECT STUFF(( SELECT ', ' + Name
                                FROM dbo.Dtl_Original_Defect 
                                where Policy_Id = @@PolicyID
                                FOR XML PATH('')
                                ), 1, 1, '')  AS OriginalDefect,
								STUFF(( SELECT ', ' + Name
                                FROM dbo.Ren_Dtl_NoCover 
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS NoCover,
								STUFF(( SELECT ', ' + RTRIM(CONCAT(AccsDescription, ' ', AccsCatDescription, ' ',
                                AccsPartDescription))
                                FROM    ( SELECT DISTINCT
                                                    RTRIM(ISNULL(wnsa.Policy_No, '')) AS PolicyNo ,
                                                    wnsa.Object_No AS ObjectNo ,
                                                    RTRIM(ISNULL(wnsa.Accs_Code, '')) AS AccsCode ,
                                                    RTRIM(ISNULL(AO.AccsDescription, '')) AS AccsDescription ,
                                                    RTRIM(ISNULL(wnsa.Accs_Part_Code, '')) AS AccsPartCode ,
                                                    CASE ao.AccsType
                                                      WHEN 4 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      WHEN 5 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      ELSE ''
                                                    END AS AccsCatDescription ,
                                                    RTRIM(ISNULL(ap.AccsPartDescription, '')) AS AccsPartDescription ,
                                                    Include_Tsi AS IncludeTsi ,
                                                    RTRIM(ISNULL(Brand, '')) AS Brand ,
                                                    Sum_Insured AS SumInsured ,
                                                    Quantity ,
                                                    Premi ,
                                                    RTRIM(ISNULL(Category, '')) AS Category ,
                                                    RTRIM(ISNULL(wnsa.Accs_Cat_Code, '')) AS AccsCatCode
                                          FROM      dbo.Ren_Dtl_Non_Standard_Accessories AS wnsa
                                                    LEFT JOIN dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.Accs_Code
                                                                                          AND ao.RowStatus = 1
                                                    LEFT JOIN AccessoriesPart AP ON Ap.AccsCode = wnsa.Accs_Code
                                                                                    AND AP.AccsPartCode = wnsa.Accs_Part_Code
                                                                                    AND ap.RowStatus = 1
                                                    LEFT JOIN ( SELECT  RTRIM(ISNULL(ac.AccsCode, '')) AS AccsCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatCode, '')) AS AccsCatCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatDescription, '')) AS AccsCatDescription ,
                                                                        RTRIM(ISNULL(acp.AccsPartCode, '')) AS AccsPartCode ,
                                                                        ac.MaxSICategory ,
                                                                        ac.IsEditable ,
                                                                        acp.QtyDefault ,
                                                                        ac.RowStatus
                                                                FROM    dbo.AccessoriesCategory AS ac
                                                                        LEFT JOIN dbo.AccessoriesCategoryPart
                                                                        AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                                                  AND acp.RowStatus = 1
                                                              ) x ON x.AccsCode = ao.AccsCode
                                                                     AND x.RowStatus = 1
                                                                     AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                                                              '')
                                                                     AND x.AccsCode = wnsa.Accs_Code
                                                                     AND x.AccsCatCode = wnsa.Accs_Cat_Code
                                          WHERE     Policy_No = @0
                                        ) AS X
                                FOR XML PATH('')
                                ), 1, 1, '')  AS Accessories";

                            var AABdbThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                            List<dynamic> list = AABdbThread.Fetch<dynamic>(queryVehicleData, OldPolicyNo);
                            if (list.Count > 0)
                            {
                                vData.ORDefectsDesc = list.First().OriginalDefect;
                                vData.NoCover = list.First().NoCover;
                                vData.Accessories = list.First().Accessories;
                            }
                        }
                        result.VehicleData = vData;
                    }
                });

                Thread threadPolicyAddress = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    string queryPolicyAddress = @";SELECT PolicySentTo SentTo, PolicyDeliveryName Name, PolicyDeliveryAddress Address, PolicyDeliveryPostalCode PostalCode FROM dbo.OrderSimulation WHERE CustID = @0 
                            AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1";
                    Otosales.Models.vWeb2.PolicyAddress pa = dbMobile.Query<Otosales.Models.vWeb2.PolicyAddress>(queryPolicyAddress, CustID, FollowUpNo).FirstOrDefault();
                    if (pa != null)
                    {
                        result.PolicyAddres = pa;
                    }
                });

                Thread threadSurveySchedule = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    string querySurveySchedule = @";SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc, CAST(COALESCE(IsManualSurvey,0) AS BIT) IsManualSurvey
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                    Otosales.Models.vWeb2.SurveySchedule ss = dbMobile.Fetch<Otosales.Models.vWeb2.SurveySchedule>(querySurveySchedule, CustID, FollowUpNo).FirstOrDefault();
                    result.SurveySchedule = ss;
                });

                #region threadDocument
                //Thread threadDocument = new Thread(() =>
                //{
                //    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                //    var AABdbThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                //    string queryDocument = @";SELECT f.SPPAKB, id1.Data SPPAKBData,
                //        f.FAKTUR, id2.Data FAKTURData,
                //        f.DocNSA1, id3.Data DocNSA1Data,
                //        f.DocNSA2, id4.Data DocNSA2Data,
                //        f.DocNSA3, id5.Data DocNSA3Data,
                //        f.DocNSA4, id6.Data DocNSA4Data,
                //        f.DocNSA5, id7.Data DocNSA5Data,
                //        RemarkToSA, RemarkFromSA
                //        FROM dbo.FollowUp f
                //        LEFT JOIN dbo.ImageData id1
                //        ON id1.PathFile = f.SPPAKB
                //        LEFT JOIN dbo.ImageData id2
                //        ON id2.PathFile = f.FAKTUR
                //        LEFT JOIN dbo.ImageData id3
                //        ON id3.PathFile = f.DocNSA1
                //        LEFT JOIN dbo.ImageData id4
                //        ON id4.PathFile = f.DocNSA2
                //        LEFT JOIN dbo.ImageData id5
                //        ON id5.PathFile = f.DocNSA3
                //        LEFT JOIN dbo.ImageData id6
                //        ON id6.PathFile = f.DocNSA4
                //        LEFT JOIN dbo.ImageData id7
                //        ON id7.PathFile = f.DocNSA5
                //        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                //    List<dynamic> listDoc = dbMobile.Fetch<dynamic>(queryDocument, CustID, FollowUpNo);
                //    if (listDoc.Count > 0)
                //    {
                //        queryDocument = @";SELECT PolicyOrderNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1 ";

                //        string policyOrderNo = dbMobile.FirstOrDefault<string>(queryDocument, CustID, FollowUpNo);

                //        queryDocument = @";SELECT TOP 1 mu.User_Name Name 
                //                FROM dbo.Mst_Order_Mobile mom
                //                INNER JOIN dbo.Mst_User mu
                //                ON mu.User_Id = mom.EntryUsr
                //                WHERE Order_No = @0 
                //                AND (SA_State = 0 OR SA_State = 2) 
                //                AND mom.EntryUsr <> 'OTOSL' ORDER BY mom.EntryDt DESC";
                //        List<dynamic> ls = AABdbThread.Fetch<dynamic>(queryDocument, policyOrderNo);
                //        if (ls.Count > 0)
                //        {
                //            if (!string.IsNullOrEmpty(listDoc.First().RemarkFromSA) && !string.IsNullOrWhiteSpace(listDoc.First().RemarkFromSA)
                //                && !string.IsNullOrEmpty(policyOrderNo))
                //            {
                //                listDoc.First().RemarkFromSA = ls.First().Name + " - " + listDoc.First().RemarkFromSA;
                //            }
                //            else if (!string.IsNullOrEmpty(policyOrderNo))
                //            {
                //                listDoc.First().RemarkFromSA = ls.First().Name;
                //            }
                //        }
                //        result.Document = listDoc;
                //    }
                //});
                #endregion

                threadPremi.Start();
                threadPersonalData.Start();
                //threadPersonalDocument.Start();
                threadCompanyData.Start();
                threadVehicleData.Start();
                threadPolicyAddress.Start();
                threadSurveySchedule.Start();
                //threadDocument.Start();

                threadPremi.Join();
                threadPersonalData.Join();
                //threadPersonalDocument.Join();
                threadCompanyData.Join();
                threadVehicleData.Join();
                threadPolicyAddress.Join();
                threadSurveySchedule.Join();
                //threadDocument.Join();


                //result.PaymentInfo = GetPaymentInfo(CustID, FollowUpNo);
                result.PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo();


                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static dynamic GetTaksListDetailDocumentPersonal(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                dynamic result = null;

                string queryPersonalDocument = @";SELECT IdentityCard, id1.Data IdentityCardData,
                        f.STNK, id2.Data STNKData,
                        f.BSTB1 BSTB, id3.Data BSTBData,
                        f.KonfirmasiCust, id4.Data KonfirmasiCustData,
                        f.DocRep, id5.Data DocRepData
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON f.IdentityCard = id1.PathFile
                        LEFT JOIN dbo.ImageData id2
                        ON f.STNK = id2.PathFile
                        LEFT JOIN dbo.ImageData id3
                        ON f.BSTB1 = id3.PathFile
                        LEFT JOIN dbo.ImageData id4
                        ON f.KonfirmasiCust = id4.PathFile
                        LEFT JOIN dbo.ImageData id5
                        ON f.DocRep = id5.PathFile
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";

                var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<dynamic> listDyn = dbFieldPremiMobile.Fetch<dynamic>(queryPersonalDocument, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result = listDyn;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static dynamic GetTaksListDetailDocumentCompany(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                dynamic result = null;

                string queryDocument = @";IF EXISTS(SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1)
                        BEGIN
	                        SELECT pc.NPWP, id1.Data NPWPData,
	                        pc.SIUP, id2.Data SIUPData
	                        FROM dbo.ProspectCompany pc
	                        LEFT JOIN dbo.ImageData id1
	                        ON pc.NPWP = id1.PathFile
	                        LEFT JOIN dbo.ImageData id2
	                        ON pc.SIUP = id2.PathFile
	                        WHERE pc.CustID = @0 AND pc.FollowUpNo = @1 AND pc.RowStatus = 1
                        END
                        ELSE
                        BEGIN
	                        SELECT NULL NPWP, NULL NPWPData,
	                        NULL SIUP, NULL SIUPData
                        END";

                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<dynamic> listDyn = dbMobile.Fetch<dynamic>(queryDocument, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result = listDyn;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static dynamic GetTaksListDetailDocumentAll(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                dynamic result = null;

                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var AABdbThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string queryDocument = @";SELECT f.SPPAKB, id1.Data SPPAKBData,
                        f.FAKTUR, id2.Data FAKTURData,
                        f.DocNSA1, id3.Data DocNSA1Data,
                        f.DocNSA2, id4.Data DocNSA2Data,
                        f.DocNSA3, id5.Data DocNSA3Data,
                        f.DocNSA4, id6.Data DocNSA4Data,
                        f.DocNSA5, id7.Data DocNSA5Data,
                        RemarkToSA, RemarkFromSA
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON id1.PathFile = f.SPPAKB
                        LEFT JOIN dbo.ImageData id2
                        ON id2.PathFile = f.FAKTUR
                        LEFT JOIN dbo.ImageData id3
                        ON id3.PathFile = f.DocNSA1
                        LEFT JOIN dbo.ImageData id4
                        ON id4.PathFile = f.DocNSA2
                        LEFT JOIN dbo.ImageData id5
                        ON id5.PathFile = f.DocNSA3
                        LEFT JOIN dbo.ImageData id6
                        ON id6.PathFile = f.DocNSA4
                        LEFT JOIN dbo.ImageData id7
                        ON id7.PathFile = f.DocNSA5
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                List<dynamic> listDoc = dbMobile.Fetch<dynamic>(queryDocument, CustID, FollowUpNo);
                if (listDoc.Count > 0)
                {
                    queryDocument = @";SELECT PolicyOrderNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1 ";

                    string policyOrderNo = dbMobile.FirstOrDefault<string>(queryDocument, CustID, FollowUpNo);

                    queryDocument = @"SELECT TOP 1 mu.User_Name Name 
                                FROM dbo.Mst_Order_Mobile mom
                                INNER JOIN dbo.Mst_User mu
                                ON mu.User_Id = mom.EntryUsr
                                WHERE Order_No = @0 
                                AND (SA_State = 0 OR SA_State = 2) 
                                AND mom.EntryUsr <> 'OTOSL' ORDER BY mom.EntryDt DESC";
                    List<dynamic> ls = AABdbThread.Fetch<dynamic>(queryDocument, policyOrderNo);
                    if (ls.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(listDoc.First().RemarkFromSA) && !string.IsNullOrWhiteSpace(listDoc.First().RemarkFromSA)
                            && !string.IsNullOrEmpty(policyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name + " - " + listDoc.First().RemarkFromSA;
                        }
                        else if (!string.IsNullOrEmpty(policyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name;
                        }
                    }
                    result = listDoc;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static void InsertQuotationPrintTable(string orderNo)
        {
            try
            {
                #region QueryData
                string query = @"";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                double AdminFee = db.ExecuteScalar<double>("SELECT AdminFee FROM OrderSimulation WHERE OrderNo=@0 ", orderNo);
                List<Otosales.Models.CalculatedPremi> cp = Otosales.Repository.vWeb2.MobileRepository.LoadCalculatedpremibyOrderNo(orderNo, "");
                query = @"select * from ordersimulation WHERE orderno=@0";
                Otosales.Models.OrderSimulation os = db.FirstOrDefault<Otosales.Models.OrderSimulation>(query, orderNo);
                query = @"select * from ordersimulationMV WHERE orderno=@0";
                Otosales.Models.OrderSimulationMV osmv = db.FirstOrDefault<Otosales.Models.OrderSimulationMV>(query, orderNo);
                #region detail scoring
                List<PremiumCalculation.Model.DetailScoring> dtlScoring = new List<PremiumCalculation.Model.DetailScoring>();
                PremiumCalculation.Model.DetailScoring ds = new PremiumCalculation.Model.DetailScoring();
                if (!string.IsNullOrEmpty(osmv.CityCode))
                {
                    ds.FactorCode = "GEOGRA";
                    ds.InsuranceCode = osmv.CityCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.UsageCode))
                {
                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "MVUSAG";
                    ds.InsuranceCode = osmv.UsageCode;
                    dtlScoring.Add(ds);
                }
                ds = new PremiumCalculation.Model.DetailScoring();
                ds.FactorCode = "VHCTYP";
                ds.InsuranceCode = string.IsNullOrEmpty(osmv.Type) ? "ALL   " : osmv.Type;
                dtlScoring.Add(ds);
                if (!string.IsNullOrEmpty(osmv.BrandCode))
                {

                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "VBRAND";
                    ds.InsuranceCode = osmv.BrandCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.IsNew.ToString()))
                {

                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "NEWVHC";
                    ds.InsuranceCode = osmv.IsNew.ToString();
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.ModelCode))
                {
                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "VMODEL";
                    ds.InsuranceCode = osmv.ModelCode;
                    dtlScoring.Add(ds);
                }
                #endregion
                Otosales.Models.DetailCoverageSummary rs = Otosales.Repository.vWeb2.MobileRepository.GetDetailCoverageSummary(os.ProductCode, dtlScoring, cp, AdminFee, osmv.Year, osmv.UsageCode, Convert.ToInt16(cp[0].Ndays), os.PeriodFrom, os.PeriodTo
                                                                                      , osmv.Type, osmv.Sitting.ToString(), os.OldPolicyNo);
                List<string> IEP = Otosales.Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALES-SEGMENT-IEP");
                var DiscountList = Otosales.Repository.vWeb2.RetailRepository.GetWTDiscount(os.ProductCode, os.ProductCode);
                decimal Discount = DiscountList.Count() > 0 ? DiscountList[0].Percentage : 0;
                query = @" DECLARE @@id AS bigint
set @@id= (SELECT RptPrintQuotationID FROM [RptPrintQuotation] WHERE OrderNo=@0)
IF ( @@id is not null)
BEGIN
IF EXISTS(SELECT * FROM [RptPrintQuotationDetail] WHERE RptPrintQuotationID=@@id)
BEGIN
DELETE FROM RptPrintQuotationDetail WHERE RptPrintQuotationID=@@id
END
DELETE FROM [RptPrintQuotation] WHERE OrderNo=@0
END
";
                db.Execute(query, orderNo);

                query = @";Declare @@TEMPID TABLE (RptPrintQuotationID bigint)
INSERT INTO [dbo].[RptPrintQuotation]
           ([OrderNo]
           ,[AdminFee]
           ,[PADRVRTSI]
           ,[PAPASSTSI]
           ,[TPLTSI]
           ,[InsuranceType]
           ,[QuotationNo]
           ,[YearCoverage]
           ,[InsuredAddress]
           ,[CompanyAddress]
           ,[PolicyNo]
           ,[IsCompany]
           ,[CompanyName]
           ,[InsuredName]
           ,[PICName]
           ,[InsuredPhone]
           ,[PICPhoneNo]
           ,[InsuredEmail]
           ,[PICEmail]
           ,[PeriodFrom]
           ,[PeriodTo]
           ,[SalesOfficerID]
           ,[BranchCode]
           ,[SalesmanName]
           ,[SalesmanPhone]
           ,[SalesmanEmail]
           ,[SalesmanRole]
           ,[InsuredObject]
           ,[VehicleYear]
           ,[PassSeat]
           ,[ChasisNumber]
           ,[EngineNumber]
           ,[Usage]
           ,[RegistrationNumber]
           ,[GeoArea]
           ,[VAPermata]
           ,[VAMandiri]
           ,[VABCA]
           ,[ProductType]
           ,[City]
           ,[AccountNo]
           ,[BankDescription]
           ,[BankName]
           ,[NoCover]
           ,[OriginalDefect]
           ,[NonStandardAcc]
           ,[DiscountPct])
       output Inserted.RptPrintQuotationID INTO @@TEMPID(RptPrintQuotationID)
    select 
			@0
           ,@1
           ,@2
           ,@3
           ,@4
		   ,CASE WHEN mp.Biz_type = 2 THEN 4 
				ELSE os.InsuranceType END AS InsuranceType
			,os.QuotationNo
			,CEILING(CAST(DATEDIFF(MONTH,PeriodFrom,PeriodTo)/12.0 AS DECIMAL(10,2))) YearCoverage
			,os.PolicyDeliveryAddress AS InsuredAddress
			,COALESCE(pcy.OfficeAddress,pcy.NPWPAddress) AS CompanyAddress
			,COALESCE(os.PolicyNo,'-') AS PolicyNo
			,pc.IsCompany
			,COALESCE(pcy.CompanyName,'') as CompanyName
			,pc.Name as InsuredName
			,COALESCE(pcy.PICname ,'') as PICname
			,pc.Phone1 AS InsuredPhone
			,COALESCE(pcy.PICPhoneNo,'') as PICPhoneNo
			,pc.Email1 as InsuredEmail
			,COALESCE(pcy.Email,'') AS PICEmail
			,os.PeriodFrom
			,os.PeriodTo
			,os.SalesOfficerID
			,os.BranchCode
			,so.Name AS SalesmanName
			,COALESCE(geu.MobileNumber,COALESCE(so.Phone1,'')) AS SalesmanPhone
			,COALESCE(geu.UserId,COALESCE(so.email,'')) AS SalesmanEmail
			,so.Role AS SalesmanRole
			,CONCAT(vb.Description,' ',vm.Description,' ',osm.Series) AS InsuredObject
			,osm.Year AS VehicleYear
			,(osm.Sitting-1) AS PassSeat
			,osm.ChasisNumber
			,osm.EngineNumber
			,u.Description AS Usage
			,osm.RegistrationNumber
			,r.Description AS GeoArea
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVAPERMATA'),COALESCE(os.VANumber,'')) as VAPermata
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVAMANDIRI'),COALESCE(os.VANumber,'')) as VAMandiri
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVABCA'),COALESCE(os.VANumber,'')) as VABCA
			,CASE WHEN os.SegmentCode IN (SELECT ms.SegmentCode FROM [BeyondReport].[AAB].dbo.Mapping_Product_SOB mpsob
                                    INNER JOIN [BeyondReport].[AAB].dbo.Mapping_Segment ms
                                    ON mpsob.SOB_ID = ms.SegmentCode
                                    WHERE 
                                    (SegmentCode2 = 'SG2016' OR SegmentCode2 = 'SG2017')) THEN 'PROKHUS'
					WHEN os.ProductCode IN (" + IEP[0] + @") THEN 'IEP'
					ELSE ' ' END AS ProductType
			,ab.City
			, b.accountno 
			, b.bankdescription  
			, b.name as BankName
           ,STUFF((	SELECT ', ' +  name
				FROM [BeyondReport].[AAB].[dbo].ord_dtl_nocover where order_no=os.PolicyOrderNo
				FOR XML PATH('')
			), 1, 1, '') AS NoCover
           ,STUFF((	SELECT ', ' +  name
				FROM [BeyondReport].[AAB].[dbo].Ord_Dtl_Original_Defect where order_no=os.PolicyOrderNo
				FOR XML PATH('')
			), 1, 1, '') AS OriginalDefect
           ,STUFF((SELECT ', '+
case when Category = 1
then
CONCAT(AccsDescription,' ',Brand)
when Category = 2
then
CONCAT(AccsDescription,' ',AccsPartDescription,' ',Brand)
when Category = 3
then
CONCAT(AccsDescription,' ',AccsPartDescription,' ',Brand)
when Category = 4
then
CONCAT(AccsCatDescription,' ',AccsPartDescription,' ',Brand)
when Category = 5
then
CONCAT(AccsCatDescription,' ',AccsPartDescription,' ',Brand)
end 
FROM    ( SELECT DISTINCT
                    wnsa.Category Category,
					AO.AccsDescription AccsDescription,
					wnsa.Brand Brand,
					AP.AccsPartDescription,
					x.AccsCatDescription
          FROM      [BeyondReport].[AAB].dbo.Ord_Non_Standard_Accessories AS wnsa
                    LEFT JOIN [BeyondReport].[AAB].dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.Accs_Code
                                                          AND ao.RowStatus = 1
                    LEFT JOIN [BeyondReport].[AAB].dbo.AccessoriesPart AP ON Ap.AccsCode = wnsa.Accs_Code
                                                    AND AP.AccsPartCode = wnsa.Accs_Part_Code
                                                    AND ap.RowStatus = 1
                    LEFT JOIN ( SELECT  RTRIM(ISNULL(ac.AccsCode, '')) AS AccsCode ,
                                        RTRIM(ISNULL(ac.AccsCatCode, '')) AS AccsCatCode ,
                                        RTRIM(ISNULL(ac.AccsCatDescription, '')) AS AccsCatDescription ,
                                        RTRIM(ISNULL(acp.AccsPartCode, '')) AS AccsPartCode ,
                                        ac.MaxSICategory ,
                                        ac.IsEditable ,
                                        acp.QtyDefault ,
                                        ac.RowStatus
                                FROM    [BeyondReport].[AAB].dbo.AccessoriesCategory AS ac
                                        LEFT JOIN [BeyondReport].[AAB].dbo.AccessoriesCategoryPart
                                        AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                  AND acp.RowStatus = 1
                              ) x ON x.AccsCode = ao.AccsCode
                                     AND x.RowStatus = 1
                                     AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                              '')
                                     AND x.AccsCode = wnsa.Accs_Code
                                     AND x.AccsCatCode = wnsa.Accs_Cat_Code
          WHERE     wnsa.Order_No = os.PolicyOrderNo
        ) AS X
				FOR XML PATH('')
			), 1, 1, '')  AS NonStandardAcc
    ,@5
	From ordersimulation os 
	INNER JOIN OrderSimulationMV osm ON osm.orderno = os.orderno
	INNER JOIN [AABMobile].[dbo].[VehicleBrand] vb ON vb.BrandCode = osm.BrandCode
	INNER JOIN [AABMobile].[dbo].[VehicleModel] vm ON vm.ModelCode = osm.ModelCode
	INNER JOIN [AABMobile].[dbo].[Dtl_Ins_Factor] u ON u.insurance_code = osm.UsageCode 
	INNER JOIN [AABMobile].[dbo].[Region] r ON r.RegionCode = osm.CityCode
	INNER JOIN [AABMobile].[dbo].[SalesOfficer] so ON so.SalesOfficerID =os.SalesOfficerID
	INNER JOIN ProspectCustomer pc ON pc.CustID = os.CustID
	LEFT JOIN dbo.Mst_Product mp ON mp.Product_Code = os.ProductCode
	LEFT JOIN dbo.ProspectCompany pcy ON pcy.CustId=pc.CustID
	LEFT JOIN [AABMobile].[dbo].[OrderSimulationSurvey] oss ON oss.OrderNo = os.OrderNo
	LEFT JOIN [AABMobile].[dbo].[AABBranch] ab ON ab.BranchCode = os.BranchCode
	LEFT JOIN [BeyondReport].[AAB].[dbo].mst_branch AS a WITH ( NOLOCK ) ON a.Branch_Id = os.BranchCode
	LEFT JOIN [BeyondReport].[AAB].[dbo].branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun 
	LEFT JOIN [BeyondReport].[a2isAuthorizationDB].[General].[ExternalUsers] geu WITH (NOLOCK) ON geu.UserId=so.Email
    where os.orderno = @0

     SELECT TOP 1 RptPrintQuotationID FROM @@TEMPID;


";
                string RptPrintQuotationID = db.ExecuteScalar<string>(query, orderNo, AdminFee, rs.PADRVRSI, rs.PAPASSSI, rs.TPLSI, Discount);
                query = @"INSERT INTO RptPrintQuotationDetail ([RptPrintQuotationID]
      ,[Year]
      ,[BasicCoverName]
      ,[TSICasco]
      ,[TSIAccess]
      ,[TSITotal]
      ,[Rate]
      ,[Loading]
      ,[BasicPremi]
      ,[SRCCPremi]
      ,[TRSPremi]
      ,[TPLPremi]
      ,[PADRVRPremi]
      ,[PAPASSPremi]
      ,[ExtendedPremi]
      ,[TotalPremi]
      ,[NettPremiAmount]
      ,[IsBundling]
      ,[IsBundlingTRS]
      ,[DiscountAmount])
VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,COALESCE(@10,0),@11,@12,@13,@14,@15,@16,@17,@18,@19)";
                foreach (Otosales.Models.DetailCoverage item in rs.dtlCoverage)
                {
                    db.Execute(query, RptPrintQuotationID, item.Year, item.BasiCoverName, item.VehiclePremi, item.AccessPremi, item.VehicleAccessPremi, item.Rate / 100, item.LoadingPremi / 100, item.BasicPremi, item.SFEPremi, item.TSPremi, item.TPLCoverage, item.PADRVRCoverage, item.PAPASSCoverage, item.PremiPerluasan, item.PremiDasarPerluasan, item.TotalPremi, item.IsBundling, item.IsBundlingTRS, item.PremiDasarPerluasan * Convert.ToDouble(Discount) / 100);
                }

                #endregion
            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }
        }

        public bool CheckPremiItems(string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    int Interest = db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM dbo.OrderSimulationInterest WHERE OrderNo = @0", OrderNo);
                    if (Interest > 0)
                    {
                        int Coverage = db.ExecuteScalar<int>(@"SELECT COUNT(*) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0", OrderNo);
                        if (Coverage > 0)
                        {
                            return true;
                        }
                    }
                    db.Execute(@";UPDATE dbo.OrderSimulation 
                                        SET TLOPeriod = 0, ComprePeriod = 0, YearCoverage = 0
                                        WHERE OrderNo = @0", OrderNo);
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static string UpdateTaskListDetail(string CustID, string FollowUpNo,
Otosales.Models.vWeb2.PersonalData pd, Otosales.Models.vWeb2.CompanyData cd, Otosales.Models.vWeb2.VehicleData vd, Otosales.Models.vWeb2.PolicyAddress pa,
Otosales.Models.vWeb2.SurveySchedule ss, List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, Models.vWeb2.OrderSimulationModel OS, Models.vWeb2.OrderSimulationMVModel OSMV,
Models.vWeb2.CalculatePremiModel CalculatePremiModel, string FollowUpStatus, Otosales.Models.vWeb2.Remarks rm)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string result = "";
            //Otosales.Models.vWeb2.PersonalData pd = new Otosales.Models.vWeb2.PersonalData();
            //Otosales.Models.vWeb2.CompanyData cd = new Otosales.Models.vWeb2.CompanyData();
            //Otosales.Models.vWeb2.VehicleData vd = new Otosales.Models.vWeb2.VehicleData();
            //Otosales.Models.vWeb2.PolicyAddress pa = new Otosales.Models.vWeb2.PolicyAddress();
            //Otosales.Models.vWeb2.SurveySchedule ss = new Otosales.Models.vWeb2.SurveySchedule();
            //Otosales.Models.vWeb2.Remarks rm = new Otosales.Models.vWeb2.Remarks();
            //List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData = new List<Otosales.Models.vWeb2.ImageDataTaskDetail>();

            string query = @"";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            try
            {
                Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);

                Thread threadPd = new Thread(() =>
                {
                    if (pd != null)
                    {
                        // pd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PersonalData>(personalData);
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryThread = @";IF EXISTS(SELECT * FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCustomer SET 
	                        Name = @1, CustBirthDay = @2, CustGender = @3, CustAddress = @4, 
                            PostalCode = @5, Email1 = @6, Phone1 = @7, IdentityNo = @8, 
                            isCompany = @9
                            WHERE CustID = @0 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @12
                        AND ApplyF = 1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.OrderSimulation 
	                        SET IsNeedDocRep = @10, AmountRep = @11
	                        WHERE CustID = @0 AND FollowUpNo = @12
	                        AND ApplyF = 1 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @12)
                        BEGIN
	                        UPDATE dbo.FollowUp 
	                        SET FollowUpName = @1, ProspectName = @1
	                        WHERE CustID = @0 AND FollowUpNo = @12
                        END ";
                        dbMobile.Execute(queryThread, CustID
                            , pd.Name, pd.CustBirthDay, pd.CustGender, pd.CustAddress
                            , pd.PostalCode, pd.Email1, pd.Phone1, pd.IdentityNo
                            , pd.isCompany, pd.isNeedDocRep, pd.AmountRep, FollowUpNo);
                        if (pd.IsPayerCompany != null)
                        {
                            queryThread = @";UPDATE dbo.ProspectCustomer SET IsPayerCompany = @1 WHERE CustID = @0";
                            dbMobile.Execute(queryThread, CustID, pd.IsPayerCompany);
                        }
                    }
                });

                Thread threadCd = new Thread(() =>
                {
                    if (cd != null)
                    {
                        //cd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.CompanyData>(companyData);
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryThread = @";DECLARE @@IsCompany BIT
                            SELECT @@IsCompany=IsCompany FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1
                            IF @@IsCompany=1
                            BEGIN
	                            IF EXISTS (SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                                BEGIN
	                                UPDATE dbo.ProspectCompany SET CompanyName = @2, NPWPno = @3, NPWPdate = @4, 
	                                NPWPaddress = @5, OfficeAddress = @6, PostalCode = @7, PICPhoneNo = @8, PICname = @9, Email = @10 
	                                WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
		                            UPDATE dbo.ProspectCustomer SET Name = @9, Phone1 = @8, Email1 = @10
		                            WHERE CustID = @0
                                END
                            END";
                        dbMobile.Execute(queryThread, CustID, FollowUpNo
                            , cd.CompanyName, cd.NPWPno, cd.NPWPdate
                            , cd.NPWPaddres, cd.OfficeAddress, cd.PostalCode, cd.PICPhoneNo, cd.PICname, cd.PICEmail);
                    }
                });

                Thread threadOS = new Thread(() =>
                {
                    if (OS != null && OSMV != null)
                    {
                        InsertOrderSimulation(OS, OSMV);
                    }
                });

                Thread threadVd = new Thread(() =>
                {
                    if (vd != null)
                    {
                        // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryThread = "";

                        if (!string.IsNullOrEmpty(vd.ProductCode))
                        {
                            queryThread = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET ProductTypeCode = @2,
	                            VehicleCode = @3, BrandCode = @4, ModelCode = @5,
	                            Series = @6, Type = @7, Sitting = @8, Year = @9,
	                            CityCode = @10, UsageCode = @11, AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET InsuranceType = @19, 
                                ProductCode = @20, 
								SegmentCode = @21, PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                        }
                        else
                        {
                            queryThread = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                        }
                        dbMobile.Execute(queryThread, CustID, FollowUpNo
                            , vd.ProductTypeCode
                            , vd.VehicleCode, vd.BrandCode, vd.ModelCode
                            , vd.Series, vd.Type, vd.Sitting, vd.Year
                            , vd.CityCode, vd.UsageCode, vd.AccessSI
                            , vd.RegistrationNumber, vd.EngineNumber
                            , vd.ChasisNumber, vd.IsNew, vd.SumInsured
                            , vd.ColorOnBPKB

                            , vd.InsuranceType
                            , vd.ProductCode
                            , vd.SegmentCode, vd.PeriodFrom, vd.PeriodTo
                            , vd.IsORDefectsRepair, vd.IsAccessoriesChange
                            , vd.IsPolicyIssuedBeforePaying
                            , vd.Remarks

                            , vd.DealerCode
                            , vd.SalesDealer);
                    }
                });

                Thread threadCalculatePremi = new Thread(() =>
                {
                    if (CalculatePremiModel != null)
                    {
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string OrderNo = dbMobile.ExecuteScalar<string>(
                            @";DECLARE @@Orderno VARCHAR(100) 
                        SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                    INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                    WHERE f.CustID = @0 AND f.FollowUpNo = @1 
                        AND o.RowStatus = 1 AND ApplyF = 1
                        SELECT @@Orderno"
                            , CustID, FollowUpNo);

                        // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                        InsertExtendedCover(OrderNo, CalculatePremiModel, vd.PeriodFrom, vd.PeriodTo);
                    }
                });

                Thread threadPa = new Thread(() =>
                {
                    if (pa != null)
                    {

                        //pa = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PolicyAddress>(policyAddress);
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryThread = @"IF EXISTS (SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulation SET PolicySentTo = @2, PolicyDeliveryName = @3, 
	                            PolicyDeliveryAddress = @4, PolicyDeliveryPostalCode = @5 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                        dbMobile.Execute(queryThread, CustID, FollowUpNo
                            , pa.SentTo, pa.Name
                            , pa.Address, pa.PostalCode);
                    }
                });

                Thread threadSs = new Thread(() =>
                {
                    if (ss != null)
                    {
                        //ss = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.SurveySchedule>(surveySchedule);
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryThread = @";DECLARE @@OrderNo VARCHAR(100);
                            SELECT @@OrderNo=OrderNo FROM dbo.OrderSimulation
                            WHERE CustID = @0 AND FollowUpNo = @1 AND 
                            RowStatus = 1 AND ApplyF = 1
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationSurvey oss
                            INNER JOIN dbo.OrderSimulation os ON os.OrderNo = oss.OrderNo
                            WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND 
                            os.RowStatus = 1 AND os.ApplyF = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulationSurvey SET CityCode = @2, LocationCode = @3, SurveyAddress = @4,
	                            SurveyPostalCode = @5, SurveyDate = @6, ScheduleTimeID = @7, ModifiedBy = 'OtosalesAPI',
								IsNeedSurvey = @8, IsNSASkipSurvey = @9, IsManualSurvey = @10,
								ModifiedDate = GETDATE() WHERE OrderNo = @@OrderNo
                            END
							ELSE
							BEGIN
								INSERT INTO dbo.OrderSimulationSurvey
								        ( OrderNo ,
								          CityCode ,
								          LocationCode ,
								          SurveyAddress ,
								          SurveyPostalCode ,
								          SurveyDate ,
								          ScheduleTimeID ,
								          RowStatus ,
								          CreatedBy ,
								          CreatedDate,
										  IsNeedSurvey,
										  IsNSASkipSurvey,
										  IsManualSurvey
								        )
								VALUES  ( @@OrderNo , -- OrderNo - varchar(100)
								          @2 , -- CityCode - int
								          @3 , -- LocationCode - int
								          @4 , -- SurveyAddress - varchar(500)
								          @5 , -- SurveyPostalCode - varchar(5)
								          @6 , -- SurveyDate - datetime
								          @7 , -- ScheduleTimeID - datetime
								          1 , -- RowStatus - bit
								          'OtosalesAPI' , -- CreatedBy - varchar(50)
								          GETDATE(), -- CreatedDate - datetime
										  @8,
										  @9,
										  @10
								        )
							END";
                        int maxLength = 150;
                        string SurveyAddress = "";
                        if (!string.IsNullOrEmpty(ss.SurveyAddress) && !string.IsNullOrWhiteSpace(ss.SurveyAddress))
                        {
                            SurveyAddress = ss.SurveyAddress.Length <= maxLength ? ss.SurveyAddress : ss.SurveyAddress.Substring(0, maxLength);
                        }
                        dbMobile.Execute(queryThread, CustID, FollowUpNo
                            , ss.CityCode, ss.LocationCode, ss.SurveyAddress
                            , ss.SurveyPostalCode, ss.SurveyDate, ss.ScheduleTimeID
                            , ss.IsNeedSurvey, ss.IsNSASkipSurvey, ss.IsManualSurvey);
                    }
                });

                threadPd.Start();
                threadCd.Start();
                threadOS.Start();
                threadVd.Start();
                threadCalculatePremi.Start();
                threadPa.Start();
                threadSs.Start();


                threadPd.Join();
                threadCd.Join();
                threadOS.Join();
                threadVd.Join();
                threadCalculatePremi.Join();
                threadPa.Join();
                threadSs.Join();


                if (ListImgData.Count > 0)
                {
                    //ListImgData = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.ImageDataTaskDetail>>(imageData);
                    UpdateImageData(ListImgData, CustID, FollowUpNo);

                }
                if (!string.IsNullOrEmpty(FollowUpStatus))
                {
                    query = @"UPDATE dbo.FollowUp SET FollowUpStatus = @2 WHERE CustID = @0 AND FollowUpNo = @1";
                    db.Execute(query, CustID, FollowUpNo, FollowUpStatus);
                }
                query = @"SELECT OrderNo FROM dbo.OrderSimulation
                        WHERE CustID = @0 AND FollowUpNo = @1 AND 
                        RowStatus = 1 AND ApplyF = 1";
                List<dynamic> res = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (res.Count > 0)
                {
                    result = res.First().OrderNo;
                }
                if (rm != null)
                {
                    query = @"UPDATE dbo.FollowUp
                            SET RemarkToSA = @0 WHERE FollowUpNo = @1";
                    // rm = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.Remarks>(Remarks);
                    db.Execute(query, rm.RemarkToSA, FollowUpNo);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static void UpdateImageData(List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, string CustID, string FollowUpNo)
        {
            string query = "";
            try
            {
                string SPPAKB = "";
                string FAKTUR = "";
                string KonfirmasiCust = "";
                string DocNSA1 = "";
                string DocNSA2 = "";
                string DocNSA3 = "";
                string DocNSA4 = "";
                string DocNSA5 = "";
                string IdentityCard = "";
                string STNK = "";
                string BSTB = "";
                string DocRep = "";
                string BuktiBayar = "";
                string BuktiBayar2 = "";
                string BuktiBayar3 = "";
                string BuktiBayar4 = "";
                string BuktiBayar5 = "";
                string DocPendukung = "";
                string NPWP = "";
                string SIUP = "";
                int lengthahdj = Convert.ToInt32(Math.Truncate(Convert.ToDouble(6 / 5)));

                Thread threadSetImage = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    var img = dbMobile.Fetch<dynamic>(@";SELECT SPPAKB,FAKTUR,KonfirmasiCust,DocNSA1,DocNSA2,DocNSA3,DocNSA4,DocNSA5,IdentityCard,STNK,
                                                        BSTB1, DocRep, BuktiBayar, DocPendukung, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
                                                        FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1", CustID, FollowUpNo);
                    if (img.Count > 0)
                    {
                        SPPAKB = Convert.ToString(img.First().SPPAKB);
                        FAKTUR = Convert.ToString(img.First().FAKTUR);
                        KonfirmasiCust = Convert.ToString(img.First().KonfirmasiCust);
                        DocNSA1 = Convert.ToString(img.First().DocNSA1);
                        DocNSA2 = Convert.ToString(img.First().DocNSA2);
                        DocNSA3 = Convert.ToString(img.First().DocNSA3);
                        DocNSA4 = Convert.ToString(img.First().DocNSA4);
                        DocNSA5 = Convert.ToString(img.First().DocNSA5);
                        IdentityCard = Convert.ToString(img.First().IdentityCard);
                        STNK = Convert.ToString(img.First().STNK);
                        BSTB = Convert.ToString(img.First().BSTB1);
                        DocRep = Convert.ToString(img.First().DocRep);
                        BuktiBayar = Convert.ToString(img.First().BuktiBayar);
                        BuktiBayar2 = Convert.ToString(img.First().BuktiBayar2);
                        BuktiBayar3 = Convert.ToString(img.First().BuktiBayar3);
                        BuktiBayar4 = Convert.ToString(img.First().BuktiBayar4);
                        BuktiBayar5 = Convert.ToString(img.First().BuktiBayar5);
                        DocPendukung = Convert.ToString(img.First().DocPendukung);
                    }
                    img = dbMobile.Fetch<dynamic>(@";SELECT NPWP, SIUP
                                                    FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1",
                                                    CustID, FollowUpNo);
                    if (img.Count > 0)
                    {
                        NPWP = Convert.ToString(img.First().NPWP);
                        SIUP = Convert.ToString(img.First().SIUP);
                    }
                });
                threadSetImage.Start();
                threadSetImage.Join();

                string qUpdateImgData = @";IF EXISTS (SELECT * FROM dbo.ImageData WHERE PathFile = @0)
                                            BEGIN 
	                                            UPDATE dbo.ImageData SET RowStatus = 0
	                                            WHERE PathFile = @0
                                            END";
                Thread ThreadSPPAKB = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail SPPAKABimg = ListImgData.Where(x => x.ImageType == "SPPAKB").FirstOrDefault();
                    if (!string.IsNullOrEmpty(SPPAKB))
                    {
                        if (!SPPAKB.Trim().Equals(SPPAKABimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, SPPAKB);
                        }
                    }
                    SPPAKB = SPPAKABimg.PathFile;
                });
                Thread ThreadFAKTUR = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail FAKTURimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("FAKTUR")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(FAKTUR))
                    {
                        if (!FAKTUR.Trim().Equals(FAKTURimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, FAKTUR);
                        }
                    }
                    FAKTUR = FAKTURimg.PathFile;
                });
                Thread ThreadKonfirmasiCust = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail KonfirmasiCustimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("KONFIRMASICUST")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(KonfirmasiCust))
                    {
                        if (!KonfirmasiCust.Trim().Equals(KonfirmasiCustimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, KonfirmasiCust);
                        }
                    }
                    KonfirmasiCust = KonfirmasiCustimg.PathFile;
                });
                Thread ThreadDocNSA1 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA1img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA1")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA1))
                    {
                        if (!DocNSA1.Trim().Equals(DocNSA1img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, DocNSA1);
                        }
                    }
                    DocNSA1 = DocNSA1img.PathFile;
                });
                Thread ThreadDocNSA2 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA2img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA2")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA2))
                    {
                        if (!DocNSA2.Trim().Equals(DocNSA2img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, DocNSA2);
                        }
                    }
                    DocNSA2 = DocNSA2img.PathFile;
                });
                Thread ThreadDocNSA3 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA3img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA3")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA3))
                    {
                        if (!DocNSA3.Trim().Equals(DocNSA3img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, DocNSA3);
                        }
                    }
                    DocNSA3 = DocNSA3img.PathFile;
                });
                Thread ThreadDocNSA4 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA4img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA4")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA4))
                    {
                        if (!DocNSA4.Trim().Equals(DocNSA4img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, DocNSA4);
                        }
                    }
                    DocNSA4 = DocNSA4img.PathFile;
                });
                Thread ThreadDocNSA5 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA5img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA5")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA5))
                    {
                        if (!DocNSA5.Trim().Equals(DocNSA5img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, DocNSA5);
                        }
                    }
                    DocNSA5 = DocNSA5img.PathFile;
                });
                Thread ThreadIdentityCard = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail IdentityCardimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("IDENTITYCARD")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(IdentityCard))
                    {
                        if (!IdentityCard.Trim().Equals(IdentityCardimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, IdentityCard);
                        }
                    }
                    IdentityCard = IdentityCardimg.PathFile;
                });
                Thread ThreadSTNK = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail STNKimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("STNK")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(STNK))
                    {
                        if (!STNK.Trim().Equals(STNKimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, STNK);
                        }
                    }
                    STNK = STNKimg.PathFile;
                });
                Thread ThreadBSTB = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BSTBimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BSTB")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BSTB))
                    {
                        if (!BSTB.Trim().Equals(BSTBimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, BSTB);
                        }
                    }
                    BSTB = BSTBimg.PathFile;
                });
                Thread ThreadDocRep = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocRepimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCREP")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocRep))
                    {
                        if (!DocRep.Trim().Equals(DocRepimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, DocRep);
                        }
                    }
                    DocRep = DocRepimg.PathFile;
                });
                Thread ThreadNPWP = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail NPWPimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("NPWP")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(NPWP))
                    {
                        if (!NPWP.Trim().Equals(NPWPimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, NPWP);
                        }
                    }
                    NPWP = NPWPimg.PathFile;
                });
                Thread ThreadSIUP = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail SIUPimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("SIUP")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(SIUP))
                    {
                        if (!SIUP.Trim().Equals(SIUPimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, SIUP);
                        }
                    }
                    SIUP = SIUPimg.PathFile;
                });
                //Thread ThreadDocPendukung = new Thread(() => {
                //    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                //    Otosales.Models.vWeb2.ImageDataTaskDetail DocPendukungimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCPENDUKUNG")).FirstOrDefault();
                //    if (!string.IsNullOrEmpty(DocPendukung))
                //    {
                //        if (!DocPendukung.Trim().Equals(DocPendukungimg.PathFile.Trim()))
                //        {
                //            dbMobile.Execute(qUpdateImgData, DocPendukung);
                //        }
                //    }
                //    DocPendukung = DocPendukungimg.PathFile;
                //});
                Thread ThreadBuktiBayar = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayarimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar))
                    {
                        if (!BuktiBayar.Trim().Equals(BuktiBayarimg.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, BuktiBayar);
                        }
                    }
                    BuktiBayar = BuktiBayarimg.PathFile;
                });
                Thread ThreadBuktiBayar2 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar2img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR2")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar2))
                    {
                        if (!BuktiBayar2.Trim().Equals(BuktiBayar2img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, BuktiBayar2);
                        }
                    }
                    BuktiBayar2 = BuktiBayar2img.PathFile;
                });
                Thread ThreadBuktiBayar3 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar3img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR3")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar3))
                    {
                        if (!BuktiBayar3.Trim().Equals(BuktiBayar3img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, BuktiBayar3);
                        }
                    }
                    BuktiBayar3 = BuktiBayar3img.PathFile;
                });
                Thread ThreadBuktiBayar4 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar4img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR4")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar4))
                    {
                        if (!BuktiBayar4.Trim().Equals(BuktiBayar4img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, BuktiBayar4);
                        }
                    }
                    BuktiBayar4 = BuktiBayar4img.PathFile;
                });
                Thread ThreadBuktiBayar5 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar5img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR5")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar5))
                    {
                        if (!BuktiBayar5.Trim().Equals(BuktiBayar5img.PathFile.Trim()))
                        {
                            dbMobile.Execute(qUpdateImgData, BuktiBayar5);
                        }
                    }
                    BuktiBayar5 = BuktiBayar5img.PathFile;
                });
                #region old
                //foreach (Otosales.Models.vWeb2.ImageDataTaskDetail id in ListImgData)
                //{
                //    if (id.ImageType.ToUpper().Equals("SPPAKB"))
                //    {
                //        if (!string.IsNullOrEmpty(SPPAKB))
                //        {
                //            if (!SPPAKB.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, SPPAKB);
                //            }
                //        }
                //        SPPAKB = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("FAKTUR"))
                //    {
                //        if (!string.IsNullOrEmpty(FAKTUR))
                //        {
                //            if (!FAKTUR.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, FAKTUR);
                //            }
                //        }
                //        FAKTUR = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("KONFIRMASICUST"))
                //    {
                //        if (!string.IsNullOrEmpty(KonfirmasiCust))
                //        {
                //            if (!KonfirmasiCust.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, KonfirmasiCust);
                //            }
                //        }
                //        KonfirmasiCust = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("DOCNSA1"))
                //    {
                //        if (!string.IsNullOrEmpty(DocNSA1))
                //        {
                //            if (!DocNSA1.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, DocNSA1);
                //            }
                //        }
                //        DocNSA1 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("DOCNSA2"))
                //    {
                //        if (!string.IsNullOrEmpty(DocNSA2))
                //        {
                //            if (!DocNSA2.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, DocNSA2);
                //            }
                //        }
                //        DocNSA2 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("DOCNSA3"))
                //    {
                //        if (!string.IsNullOrEmpty(DocNSA3))
                //        {
                //            if (!DocNSA3.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, DocNSA3);
                //            }
                //        }
                //        DocNSA3 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("DOCNSA4"))
                //    {
                //        if (!string.IsNullOrEmpty(DocNSA4))
                //        {
                //            if (!DocNSA4.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, DocNSA4);
                //            }
                //        }
                //        DocNSA4 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("DOCNSA5"))
                //    {
                //        if (!string.IsNullOrEmpty(DocNSA5))
                //        {
                //            if (!DocNSA5.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, DocNSA5);
                //            }
                //        }
                //        DocNSA5 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("IDENTITYCARD"))
                //    {
                //        if (!string.IsNullOrEmpty(IdentityCard))
                //        {
                //            if (!IdentityCard.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, IdentityCard);
                //            }
                //        }
                //        IdentityCard = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("STNK"))
                //    {
                //        if (!string.IsNullOrEmpty(STNK))
                //        {
                //            if (!STNK.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, STNK);
                //            }
                //        }
                //        STNK = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("BSTB"))
                //    {
                //        if (!string.IsNullOrEmpty(BSTB))
                //        {
                //            if (!BSTB.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, BSTB);
                //            }
                //        }
                //        BSTB = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("DOCREP"))
                //    {
                //        if (!string.IsNullOrEmpty(DocRep))
                //        {
                //            if (!DocRep.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, DocRep);
                //            }
                //        }
                //        DocRep = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("NPWP"))
                //    {
                //        if (!string.IsNullOrEmpty(NPWP))
                //        {
                //            if (!NPWP.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, NPWP);
                //            }
                //        }
                //        NPWP = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("SIUP"))
                //    {
                //        if (!string.IsNullOrEmpty(SIUP))
                //        {
                //            if (!SIUP.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, SIUP);
                //            }
                //        }
                //        SIUP = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR"))
                //    {
                //        if (!string.IsNullOrEmpty(BuktiBayar))
                //        {
                //            if (!BuktiBayar.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, BuktiBayar);
                //            }
                //        }
                //        BuktiBayar = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("DOCPENDUKUNG"))
                //    {
                //        if (!string.IsNullOrEmpty(DocPendukung))
                //        {
                //            if (!DocPendukung.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, DocPendukung);
                //            }
                //        }
                //        DocPendukung = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR2"))
                //    {
                //        if (!string.IsNullOrEmpty(BuktiBayar2))
                //        {
                //            if (!BuktiBayar2.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, BuktiBayar2);
                //            }
                //        }
                //        BuktiBayar2 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR3"))
                //    {
                //        if (!string.IsNullOrEmpty(BuktiBayar3))
                //        {
                //            if (!BuktiBayar3.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, BuktiBayar3);
                //            }
                //        }
                //        BuktiBayar3 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR4"))
                //    {
                //        if (!string.IsNullOrEmpty(BuktiBayar4))
                //        {
                //            if (!BuktiBayar4.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, BuktiBayar4);
                //            }
                //        }
                //        BuktiBayar4 = id.PathFile;
                //    }
                //    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR5"))
                //    {
                //        if (!string.IsNullOrEmpty(BuktiBayar5))
                //        {
                //            if (!BuktiBayar5.Trim().Equals(id.PathFile.Trim()))
                //            {
                //                dbMobile.Execute(qUpdateImgData, BuktiBayar5);
                //            }
                //        }
                //        BuktiBayar5 = id.PathFile;
                //    }
                //}
                #endregion

                Thread threadMoveImage = new Thread(() =>
                {
                    #region SAVE IMAGE
                    var mblDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    string queryThread = @";
SELECT [ImageID]
      ,[FollowUpNo]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                    List<dynamic> ImageData = mblDB.Fetch<dynamic>(queryThread, FollowUpNo);
                    foreach (dynamic imd in ImageData)
                    {
                        queryThread = @";IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,1,@6,@7)
END";
                        mblDB.Execute(queryThread, imd.ImageID, imd.DeviceID
    , imd.SalesOfficerID
    , imd.PathFile
    , imd.EntryDate
    , imd.LastUpdatedTime
    , imd.Data
    , imd.ThumbnailData);
                        mblDB.Execute(";UPDATE dbo.TempImageData SET RowStatus=0 WHERE FollowUpNo=@0 AND PathFile=@1", FollowUpNo, imd.PathFile);
                    }
                    #endregion
                });

                ThreadSPPAKB.Start();
                ThreadFAKTUR.Start();
                ThreadKonfirmasiCust.Start();
                ThreadDocNSA1.Start();
                ThreadDocNSA2.Start();
                ThreadDocNSA3.Start();
                ThreadDocNSA4.Start();
                ThreadDocNSA5.Start();
                ThreadIdentityCard.Start();
                ThreadSTNK.Start();
                ThreadBSTB.Start();
                ThreadDocRep.Start();
                ThreadNPWP.Start();
                ThreadSIUP.Start();
                //ThreadDocPendukung.Start();
                ThreadBuktiBayar.Start();
                ThreadBuktiBayar2.Start();
                ThreadBuktiBayar3.Start();
                ThreadBuktiBayar4.Start();
                ThreadBuktiBayar5.Start();
                threadMoveImage.Start();

                ThreadSPPAKB.Join();
                ThreadFAKTUR.Join();
                ThreadKonfirmasiCust.Join();
                ThreadDocNSA1.Join();
                ThreadDocNSA2.Join();
                ThreadDocNSA3.Join();
                ThreadDocNSA4.Join();
                ThreadDocNSA5.Join();
                ThreadIdentityCard.Join();
                ThreadSTNK.Join();
                ThreadBSTB.Join();
                ThreadDocRep.Join();
                ThreadNPWP.Join();
                ThreadSIUP.Join();
                //ThreadDocPendukung.Join();
                ThreadBuktiBayar.Join();
                ThreadBuktiBayar2.Join();
                ThreadBuktiBayar3.Join();
                ThreadBuktiBayar4.Join();
                ThreadBuktiBayar5.Join();
                threadMoveImage.Join();


                Thread threadUpdateImage = new Thread(() =>
                {
                    var dbMobileThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    query = @";IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.FollowUp SET SPPAKB = @2, FAKTUR = @3, KonfirmasiCust = @4,
	                            DocNSA1 = @5, DocNSA2 = @6, DocNSA3 = @7, IdentityCard = @8, STNK = @9,
                                BSTB1 = @10, DocRep = @11, BuktiBayar = @12, DocPendukung = @13
                                , BuktiBayar2 = @14, BuktiBayar3 = @15, BuktiBayar4 = @16, BuktiBayar5 = @17
                                , DocNSA4 = @18, DocNSA5 = @19
	                            WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                    //                query = @"IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                    //                            BEGIN
                    //	                            UPDATE dbo.FollowUp SET SPPAKB = @2, FAKTUR = @3, KonfirmasiCust = @4,
                    //	                            DocNSA1 = @5, DocNSA2 = @6, DocNSA3 = @7, IdentityCard = @8, STNK = @9,
                    //                                BSTB1 = @10, DocRep = @11, BuktiBayar = @12, DocPendukung = @13
                    //	                            WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                    //                            END";

                    dbMobileThread.Execute(query, CustID, FollowUpNo
                        , SPPAKB, FAKTUR, KonfirmasiCust
                        , DocNSA1, DocNSA2, DocNSA3, IdentityCard, STNK
                        , BSTB, DocRep, BuktiBayar, DocPendukung
                        , BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5);

                    if (!string.IsNullOrEmpty(NPWP) || !string.IsNullOrEmpty(SIUP))
                    {
                        query = @";UPDATE dbo.ProspectCompany 
                                SET NPWP = @0, SIUP = @1
                                WHERE CustID = @2 AND FollowUpNo = @3";
                        dbMobileThread.Execute(query, NPWP, SIUP, CustID, FollowUpNo);
                    }

                    string qGetBktByr = @";SELECT a.ImageName BuktiBayar, id.Data, PolicyOrderNo, id.CoreImage_ID, id.ImageID FROM
(
SELECT
    FollowUpNo,ImageName, ImageType, PolicyOrderNo
FROM
(
select fu.FollowUpNo, PolicyOrderNo, BuktiBayar, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
from dbo.FollowUp fu WITH (NOLOCK)
INNER JOIN dbo.OrderSimulation os WITH (NOLOCK) ON os.FollowUpNo = fu.FollowUpNo
where fu.followupno = @0 AND fu.RowStatus = 1 AND ApplyF = 1 AND os.RowStatus = 1
)a
UNPIVOT (
    ImageName FOR ImageType IN (
        BuktiBayar, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
    )
) unpvt
where ImageName <> ''
) a
INNER JOIN dbo.ImageData id WITH (NOLOCK) on ImageName = id.PathFile
AND (CoreImage_ID IS NULL OR CoreImage_ID = '')";
                    List<dynamic> bktbyr = dbMobileThread.Fetch<dynamic>(qGetBktByr, FollowUpNo);
                    if (bktbyr.Count > 0)
                    {
                        foreach (dynamic item in bktbyr)
                        {
                            string outImageID = null;
                            string referenceImageType = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage("BuktiBayar", false);
                            if (!string.IsNullOrEmpty(item.PolicyOrderNo))
                            {
                                if (Otosales.Logic.ImageLogic.UploadImage(item.Data, item.BuktiBayar, referenceImageType, 1, "OTOSL", item.PolicyOrderNo, out outImageID) && outImageID != null)
                                {
                                    Otosales.Logic.ImageLogic.UpdateImageData(item.ImageID, outImageID);
                                }
                            }
                        }
                        Otosales.Logic.ImageLogic.DeleteUnusedImages(FollowUpNo);
                    }
                    else
                    {
                        Otosales.Logic.ImageLogic.DeleteUnusedImages(FollowUpNo);
                    }
                });

                threadUpdateImage.Start();
                threadUpdateImage.Join();
                Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void InsertOrderSimulation(Models.vWeb2.OrderSimulationModel OS, Models.vWeb2.OrderSimulationMVModel OSMV)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            // OrderSimulationModel OS = new OrderSimulationModel();
            // OrderSimulationMVModel OSMV = new OrderSimulationMVModel();
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                #region INSERT ORDERSIMULATION
                string OrderNoReturn = "";
                string FollowUpNoEnd = "";
                string CustIdEnd = "";
                // OS = JsonConvert.DeserializeObject<OrderSimulationModel>(OSData);
                query = "SELECT OrderNo FROM OrderSimulation WHERE CustID=@0 AND FollowUpNo=@1 AND ApplyF = 1";
                List<dynamic> ordno = db.Fetch<dynamic>(query, OS.CustID, OS.FollowUpNo);
                string OrderNo = "";
                if (ordno.Count > 0)
                {
                    OrderNo = ordno.First().OrderNo;
                }
                else
                {
                    OrderNo = System.Guid.NewGuid().ToString();
                }
                OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                OrderNoReturn = OS.OrderNo;
                OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;
                OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;
                OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[VANumber]
      ,[SurveyNo]
      ,[PeriodFrom]
      ,[PolicyNo]
      ,[AmountRep]
      ,[PolicyID]
      ,[SegmentCode]
) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25
,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36)
END";
                OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                db.Execute(query, OS.OrderNo
, OS.CustID
, OS.FollowUpNo
, OS.QuotationNo
, OS.MultiYearF
, OS.YearCoverage
, OS.TLOPeriod
, OS.ComprePeriod
, OS.BranchCode
, OS.SalesOfficerID
, OS.PhoneSales
, OS.DealerCode
, OS.SalesDealer
, OS.ProductTypeCode
, OS.AdminFee
, OS.TotalPremium
, OS.Phone1
, OS.Phone2
, OS.Email1
, OS.Email2
, OS.SendStatus
, OS.InsuranceType
, OS.ApplyF
, OS.SendF
, OS.LastInterestNo
, OS.LastCoverageNo
, OS.PolicySentTo
, OS.PolicyDeliveryName
, OS.PolicyDeliveryAddress
, OS.PolicyDeliveryPostalCode
, OS.VANumber
, OS.SurveyNo
, OS.PeriodFrom
, OS.PeriodTo
, OS.PolicyNo
, OS.AmountRep
, OS.PolicyID
, OS.SegmentCode);
                #endregion

                #region INSERT UPDATE ORDERSIMULATION MV
                // OSMV = JsonConvert.DeserializeObject<OrderSimulationMVModel>(OSMVData);
                OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                db.Execute(query, OSMV.OrderNo
, OSMV.ObjectNo
, OSMV.ProductTypeCode
, OSMV.VehicleCode
, OSMV.BrandCode
, OSMV.ModelCode
, OSMV.Series
, OSMV.Type
, OSMV.Sitting
, OSMV.Year
, OSMV.CityCode
, OSMV.UsageCode
, OSMV.SumInsured
, OSMV.AccessSI
, OSMV.RegistrationNumber
, OSMV.EngineNumber
, OSMV.ChasisNumber
//, OSMV.LastUpdatedTime
//, OSMV.RowStatus
, OSMV.IsNew);
                #endregion

            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static void InsertExtendedCover(string OrderNo, Models.vWeb2.CalculatePremiModel CalculatePremiModel, DateTime? PeriodFrom, DateTime? PeriodTo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            bool isexistSFE = false;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    List<Models.OrderSimulationInterest> OSI = new List<Models.OrderSimulationInterest>();
                    List<Models.OrderSimulationCoverage> OSC = new List<Models.OrderSimulationCoverage>();
                    int a = 1;
                    int year = 1;
                    //CalculatePremiModel CalculatePremiModel = JsonConvert.DeserializeObject<CalculatePremiModel>(calculatedPremiItems);
                    List<CalculatedPremiItems> calculatedPremiItem = CalculatePremiModel.ListCalculatePremi;
                    OSData OS = CalculatePremiModel.OSData;
                    OSMVData OSMV = CalculatePremiModel.OSMVData;
                    OS.OrderNo = OS.OrderNo;
                    OSMV.OrderNo = OSMV.OrderNo;
                    if (calculatedPremiItem.Count > 0)
                    {
                        DateTime? pt = null;
                        foreach (CalculatedPremiItems cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (Models.OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (CalculatedPremiItems cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                        pt = cpitem.PeriodTo;
                                    }
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                        pf = cpitem.PeriodFrom;
                                    }
                                }
                                Models.OrderSimulationInterest osi = new Models.OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (CalculatedPremiItems cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;
                        foreach (Models.OrderSimulationInterest item in OSI)
                        {
                            foreach (CalculatedPremiItems item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    Models.OrderSimulationCoverage oscItem = new Models.OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }

                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert

                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OrderNo);

                        int i = 1;
                        foreach (Models.OrderSimulationInterest data in OSI)
                        {
                            query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                            i++;
                        }

                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationInterest> TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationInterest data in TempAllOSI)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in OSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) && data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                        query = @"UPDATE OrderSimulationInterest SET 
                        //                                Premium=@5,
                        //                                LastUpdatedTime=GETDATE()
                        //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4 AND Deductible_Code=@6";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year, data2.Premium, data.DeductibleCode);
                        //                                    }
                        //                                }

                        //                                if (!isExist)
                        //                                {
                        //                                    //soft delete coverage

                        //                                    query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 ";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo);


                        //                                    //soft delete interest
                        //                                    query = @"DELETE OrderSimulationInterest " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }



                        #region Insert
                        //  OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        i = 1;
                        foreach (Models.OrderSimulationCoverage data in OSC)
                        {
                            query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                            i++;

                        }
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        OS.TotalPremium = db.ExecuteScalar<decimal>("SELECT COALESCE((SELECT SUM(Premium) FROM OrderSimulationCoverage where OrderNo=@0)+(SELECT AdminFee FROM OrderSimulation where OrderNo=@0),0)", OS.OrderNo);
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = PeriodFrom ?? DateTime.Now;
                            DateTime end = PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                InsertExtendedCover(OrderNo, CalculatePremiModel, PeriodFrom, PeriodTo);
            }
        }

        public static DownloadQuotationResult GetByteReportDashboard(DateTime startDate, DateTime EndDate, string UserId, string TypeReport)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();

            string ReportFollowUpNewPath = ConfigurationManager.AppSettings["ReportFollowUpNewPath"].ToString();
            string ReportFollowUpRenewPath = ConfigurationManager.AppSettings["ReportFollowUpRenewPath"].ToString();
            string ReportDailyRawDataPath = ConfigurationManager.AppSettings["ReportDailyRawDataPath"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;

            DownloadQuotationResult result = new DownloadQuotationResult();
            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                string fileName = "";

                using (ReportViewer rView = new ReportViewer())
                {
                    rView.ProcessingMode = ProcessingMode.Remote;

                    ServerReport serverReport = rView.ServerReport;

                    if (TypeReport.Equals("FollowUpNew")) {
                        serverReport.ReportPath = ReportFollowUpNewPath;
                        fileName = "Report Follow Up New " + startDate.ToString("MMMM dd yyyy") + " - " + EndDate.ToString("MMMM dd yyyy") + " " +UserId.ToUpper();
                    }
                    else if (TypeReport.Equals("FollowUpRenew"))
                    {
                        serverReport.ReportPath = ReportFollowUpRenewPath;
                        fileName = "Report Follow Up Renew " + startDate.ToString("MMMM dd yyyy") + " - " + EndDate.ToString("MMMM dd yyyy") + " " + UserId.ToUpper();
                    }
                    else if (TypeReport.Equals("DailyRawData"))
                    {
                        serverReport.ReportPath = ReportDailyRawDataPath;
                        fileName = "Report Daily Raw Data " + startDate.ToString("MMMM dd yyyy") + " - " + EndDate.ToString("MMMM dd yyyy") + " " + UserId.ToUpper();
                    }

                    serverReport.ReportServerUrl = new Uri(ReportServerURL);
                    serverReport.ReportServerCredentials.NetworkCredentials = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);

                    ReportParameter[] parameters = new ReportParameter[3];
                    ReportParameter reportParamStartDate = new ReportParameter();
                    reportParamStartDate.Name = "StartDate";
                    reportParamStartDate.Values.Add(startDate.ToString("MM/dd/yyyy"));

                    ReportParameter reportParamEndDate = new ReportParameter();
                    reportParamEndDate.Name = "EndDate";
                    reportParamEndDate.Values.Add(EndDate.ToString("MM/dd/yyyy"));

                    ReportParameter reportParamUserId = new ReportParameter();
                    reportParamUserId.Name = "UserId";
                    reportParamUserId.Values.Add(UserId);

                    parameters[0] = reportParamStartDate;
                    parameters[1] = reportParamEndDate;
                    parameters[2] = reportParamUserId;
                    rView.ServerReport.SetParameters(parameters);
                    bytes = rView.ServerReport.Render("EXCEL", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                    result.Bytes = bytes;

                    result.FileName = fileName;
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.Error("Error at Function " + actionName + " :" + ex.StackTrace);
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();
                throw ex;
            }
        }


        public dynamic getFollowUpStatus(string followupstatus, string followupstatusinfo, string isRenewal, string followupno)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                dynamic result = null;

                string query = @";exec [usp_GetFollowUpStatusOtosales] @0,@1,@2,@3";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    if (!string.IsNullOrEmpty(query))
                    {
                        result = db.Fetch<Models.FollowUpStatus>(query, followupstatus, followupstatusinfo, isRenewal, followupno); //totalcount, 100);
                    }
                }

                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static Otosales.Models.vWeb2.PaymentInfo GetPaymentInfo(string CustID, string FollowUpNo, int IsGenerateVA)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var BYNDdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
            List<dynamic> listDyn = new List<dynamic>();
            List<dynamic> listVANo = new List<dynamic>();
            Otosales.Models.vWeb2.PaymentInfo pf = new Otosales.Models.vWeb2.PaymentInfo();
            string VaNumber = "";
            int isReactive = 0;
            try
            {
                query = @";IF EXISTS(SELECT * FROM dbo.OrderSimulation os
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						WHERE os.ApplyF = 1 AND f.CustID = @0 AND f.FollowUpNo = @1)
						BEGIN
							SELECT os.VANumber, f.BuktiBayar AS BuktiBayar, 
							id.Data AS BuktiBayarData, os.PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data
							FROM dbo.OrderSimulation os 
							INNER JOIN dbo.FollowUp f 
							ON f.FollowUpNo = os.FollowUpNo
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE f.CustID = @0 AND f.FollowUpNo = @1 
							AND f.RowStatus = 1
						END
						ELSE
						BEGIN
							SELECT '' AS VANumber, BuktiBayar, 
                            id.Data AS BuktiBayarData, '' AS PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data							 
							FROM dbo.FollowUp f 
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE CustID = @0 AND FollowUpNo = @1
							AND f.RowStatus = 1
						END";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    query = @"SELECT OrderNo,COALESCE(ProductCode,'') AS ProductCode,isCompany,os.SalesOfficerID,os.PolicyOrderNo, VANumber, os.PolicyNo
                        ,CAST(COALESCE(f.IsRenewal,0) AS BIT) IsRenewal, PeriodFrom, PeriodTo
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        INNER JOIN dbo.ProspectCustomer ps
                        ON ps.CustID = f.CustID
                        WHERE os.CustID = @0 
                        AND os.FollowUpNo = @1
                        AND os.ApplyF = 1
                        AND os.RowStatus = 1";
                    List<dynamic> ordData = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                    string qUpdateVirtualAccountNo = @";DECLARE @@ID BIGINT
DECLARE @@SoID VARCHAR(16)
SELECT @@ID=VirtualAccountNumberID, @@SoId = SalesOfficerID
FROM Finance.VirtualAccountNumber a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNumber
WHERE a.VirtualAccountNumber = @0

UPDATE Finance.VirtualAccountNumber 
SET ValidTo = ValidFrom, ModifiedBy = @@SoId, ModifiedDate = GETDATE()
WHERE VirtualAccountNumberID = @@ID";
                    string qUpdateAAB2000VA = @";DECLARE @@ID BIGINT 
DECLARE @@SoID VARCHAR(16)
SELECT @@ID=AAB2000VirtualAccountID
FROM Finance.AAB2000VirtualAccount a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNo
WHERE VirtualAccountNo = @0 AND a.PolicyNo = @1

UPDATE Finance.AAB2000VirtualAccount 
SET PolicyPeriodTo = GETDATE(), ModifiedBy = @@SoID, ModifiedDate = GETDATE()
WHERE AAB2000VirtualAccountID = @@ID";
                    if (ordData.Count > 0)
                    {
                        pf.IsGenerateVA = false;
                        pf.IsReGenerateVA = false;
                        //if (Convert.ToBoolean(ordData.First().isCompany))
                        //{
                        //    query = @"SELECT a.branch_id ,
                        //                    a.accounting_code_sun ,
                        //                    b.accountno ,
                        //                    b.bankdescription ,
                        //                    b.name ,
                        //                    b.branchdescription
                        //            FROM   mst_branch AS a WITH ( NOLOCK )
                        //                    INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
                        //              WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                        //    List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                        //    if (lists.Count > 0)
                        //    {
                        //        pf.RekeningPenampung = lists.First().accountno;
                        //    }
                        //}
                        //else
                        //{
                        if (!string.IsNullOrEmpty(ordData.First().ProductCode))
                        {
                            query = @"select CAST(COALESCE(IsAllowVA,0) AS BIT) IsAllowVA, Biz_type from mst_product where Product_Code = @0";
                            List<dynamic> listBizType = AABdb.Fetch<dynamic>(query, ordData.First().ProductCode);
                            if (listBizType.Count > 0)
                            {
                                int b = 0;
                                b = listBizType.First().Biz_type;
                                query = @"Select Salesman_Id,mb.Branch_id From Mst_Salesman ms
                                        INNER JOIN dbo.Mst_Branch mb
                                        ON mb.Branch_id = ms.Branch_Id
                                        Where User_id_otosales= @0 
                                        and ms.status=1 AND mb.Biz_type = @1";
                                string qGetIsPayerCompany = @"SELECT CAST(COALESCE(IsPayerCompany,0) AS BIT) FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1";
                                bool IsPayerCompany = db.ExecuteScalar<bool>(qGetIsPayerCompany, CustID);
                                if (!listBizType.First().IsAllowVA || IsPayerCompany || Convert.ToBoolean(ordData.First().isCompany))
                                {
                                    if (b == 2)
                                    {
                                        pf.RekeningPenampung = AppParameterAccess.GetApplicationParametersValue("REKPENAMPUNGSYARIAH").First();
                                    }
                                    else
                                    {
                                        query = @"SELECT a.branch_id ,
                                                            a.accounting_code_sun ,
                                                            b.accountno ,
                                                            b.bankdescription ,
                                                            b.name ,
                                                            b.branchdescription
                                                        FROM mst_branch AS a WITH ( NOLOCK )
                                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                                        List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                                        if (lists.Count > 0)
                                        {
                                            pf.RekeningPenampung = lists.First().accountno;
                                        }
                                    }
                                    query = @"SELECT OrderNo,VANumber,OldPolicyNo, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal
                                                FROM dbo.OrderSimulation os
                                                INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND os.RowStatus = 1";
                                    List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                    if (ordData.Count > 0)
                                    {
                                        string VaNo = Convert.ToString(ordDt.First().VANumber);
                                        if (!string.IsNullOrEmpty(VaNo))
                                        {
                                            string trnsDate = "";
                                            string exprDate = "";

                                            string qGetTransDate = @"SELECT EntryDate StartDate, DATEADD(DAY,CAST(ParamValue AS INT),EntryDate) EndDate FROM dbo.OrderSimulation os
	                                                                    INNER JOIN dbo.ApplicationParameters ap ON 1=1 AND ParamName = 'END-CHECKVAPAYMENT'
	                                                                    WHERE OrderNo = @0";
                                            List<dynamic> lsDate = db.Fetch<dynamic>(qGetTransDate, ordData.First().OrderNo);
                                            if (lsDate.Count > 0)
                                            {
                                                trnsDate = lsDate.First().StartDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                exprDate = lsDate.First().EndDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                string cSQLGetTotalBayarVA = @";WITH cte
                                                                                        AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                                                    a.VI_TraceNo
                                                                                            FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                                                            WHERE    b.VI_VANumber IN (
                                                                                                        SELECT  value
                                                                                                        FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                                                        )
                                                                                        --AND a.PermataVirtualAccountPaymentSourceCode = '02'
		                                                                                AND a.VI_TransactionDate BETWEEN @1 AND @2
                                                                                            )
                                                                                SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                                                        SUM(pvas.VI_Amount) AS TotalBayar ,
                                                                                        MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                                                        MAX(pva.BillAmount) AS TotalBill
                                                                                FROM    cte
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                                                            AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                                                                WHERE   IsReversal = 0";
                                                List<dynamic> ls = BYNDdb.Fetch<dynamic>(cSQLGetTotalBayarVA, VaNo, trnsDate, exprDate);
                                                if (ls.Count > 0)
                                                {
                                                    decimal TotalBayar = ls.First().TotalBayar == null ? 0 : Convert.ToDecimal(ls.First().TotalBayar);
                                                    if (TotalBayar == 0)
                                                    {
                                                        if (!ordDt.First().IsRenewal)
                                                        {
                                                            BYNDdb.Execute(qUpdateVirtualAccountNo, VaNo);
                                                            //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                        }
                                                        else
                                                        {
                                                            BYNDdb.Execute(qUpdateAAB2000VA
                                                                , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                        }
                                                        listDyn.First().VANumber = "";
                                                    }
                                                }
                                                else
                                                {
                                                    if (!ordDt.First().IsRenewal)
                                                    {
                                                        BYNDdb.Execute(qUpdateVirtualAccountNo, VaNo);
                                                        //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                    }
                                                    else
                                                    {
                                                        BYNDdb.Execute(qUpdateAAB2000VA
                                                            , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                    }
                                                    listDyn.First().VANumber = "";
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().VANumber))
                                    {
                                        if (!ordData.First().IsRenewal)
                                        {
                                            List<dynamic> lists = BYNDdb.Fetch<dynamic>("SELECT RowStatus FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                , ordData.First().VANumber);
                                            if (lists.Count > 0)
                                            {
                                                DateTime dtF = BYNDdb.ExecuteScalar<DateTime>("SELECT ValidFrom FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                , ordData.First().VANumber);
                                                DateTime dtT = BYNDdb.ExecuteScalar<DateTime>("SELECT ValidTo FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                , ordData.First().VANumber);
                                                if (dtF.Date.AddDays(7) > DateTime.Now.Date && dtT.Date == dtF.Date)
                                                {
                                                    BYNDdb.Execute(@";DECLARE @@ID BIGINT
                                                    DECLARE @@SoID VARCHAR(16)
													SELECT @@ID=VirtualAccountNumberID, @@SoID = SalesOfficerID
                                                    FROM Finance.VirtualAccountNumber a
                                                    INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
                                                    ON os.VANumber = a.VirtualAccountNumber
                                                    WHERE a.VirtualAccountNumber = @0

                                                    UPDATE Finance.VirtualAccountNumber 
                                                    SET ValidTo = ValidFrom+7, ModifiedBy = @@SoID, ModifiedDate = GETDATE()
                                                    WHERE VirtualAccountNumberID = @@ID"
                                                    , ordData.First().VANumber);
                                                    Otosales.Repository.v0232URF2019.MobileRepository.AddVA(ordData.First().OrderNo);
                                                }
                                            }
                                            //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                        }
                                        else
                                        {
                                            BYNDdb.Execute(@";DECLARE @@ID BIGINT
                                                DECLARE @@PeriodTo DATETIME
												DECLARE @@SoId VARCHAR(16)
                                                SELECT @@ID=AAB2000VirtualAccountID, @@PeriodTo=os.PeriodTo, @@SoId = SalesOfficerID
                                                FROM Finance.AAB2000VirtualAccount a
                                                INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
                                                ON os.VANumber = a.VirtualAccountNo
                                                WHERE VirtualAccountNo = @0 AND a.PolicyNo = @1

                                                UPDATE Finance.AAB2000VirtualAccount 
                                                SET PolicyPeriodTo = @@PeriodTo, ModifiedBy = @@SoId, ModifiedDate = GETDATE()
                                                WHERE AAB2000VirtualAccountID = @@ID"
                                            , ordData.First().VANumber, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                        }
                                    }
                                    query = @"SELECT OrderNo,VANumber,OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";
                                    List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                    if (ordData.Count > 0)
                                    {
                                        bool isGenerate = true;
                                        VaNumber = Convert.ToString(ordDt.First().VANumber);
                                        int VAstatus = 0;
                                        DateTime validto = DateTime.Now;
                                        if (!string.IsNullOrEmpty(VaNumber))
                                        {
                                            listVANo = BYNDdb.Fetch<dynamic>("SELECT ValidTo, RowStatus FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0", VaNumber);
                                            isReactive = db.ExecuteScalar<int>("SELECT COALESCE(isVAReactive,0) isVAreactive FROM dbo.OrderSimulation WHERE OrderNo = @0", ordData.First().OrderNo);
                                            if (listVANo.Count > 0)
                                            {
                                                validto = Convert.ToDateTime(listVANo.First().ValidTo);
                                                VAstatus = listVANo.First().RowStatus;
                                            }
                                        }
                                        if (string.IsNullOrEmpty(VaNumber) || (validto.Date < DateTime.Now.Date && isReactive == 0))
                                        {
                                            if (!string.IsNullOrEmpty(ordDt.First().OldPolicyNo))
                                            {
                                                query = @";SELECT CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal FROM dbo.OrderSimulation os 
                                                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                            WHERE os.OrderNo = @0";
                                                List<dynamic> lsRen = db.Fetch<dynamic>(query, ordDt.First().OrderNo);
                                                if (lsRen.Count > 0)
                                                {
                                                    isGenerate = !lsRen.First().IsRenewal;
                                                }
                                                query = @";SELECT Cust_Type FROM dbo.Mst_Order mo
                                                            INNER JOIN dbo.Mst_Customer mc 
                                                            ON mo.Payer_Code = mc.Cust_Id
                                                            WHERE Policy_No = @0";
                                                lsRen = AABdb.Fetch<dynamic>(query, ordDt.First().OldPolicyNo);
                                                if (lsRen.Count > 0)
                                                {
                                                    if (Convert.ToInt32(lsRen.First().Cust_Type) == 2)
                                                    {
                                                        db.Execute(@"UPDATE dbo.ProspectCustomer SET IsPayerCompany = 1 WHERE CustID = @0 AND RowStatus = 1", CustID);
                                                    }
                                                }
                                            }
                                            if (isGenerate)
                                            {
                                                if (ordData.First().PeriodFrom != null && ordData.First().PeriodTo != null)
                                                {
                                                    if (string.IsNullOrEmpty(VaNumber))
                                                    {
                                                        pf.IsGenerateVA = true;
                                                    }
                                                    else if (isReactive == 0)
                                                    {
                                                        pf.IsReGenerateVA = true;
                                                    }

                                                    if (IsGenerateVA == 1)
                                                    {
                                                        VaNumber = Otosales.Repository.v0232URF2019.MobileRepository.GetBookingVANo();
                                                        query = @"UPDATE dbo.OrderSimulation 
                                                                SET VANumber=@2
                                                                WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";
                                                        db.Execute(query, CustID, FollowUpNo, VaNumber);
                                                        Otosales.Repository.v0232URF2019.MobileRepository.AddVA(ordData.First().OrderNo);
                                                        pf.IsGenerateVA = false;
                                                        if (pf.IsReGenerateVA)
                                                        {
                                                            db.Execute(@";UPDATE dbo.OrderSimulation SET isVAReactive = 1 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1"
                                                                , CustID, FollowUpNo);
                                                            pf.IsReGenerateVA = false;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        string pPermata = AppParameterAccess.GetApplicationParametersValue("PREFIXVAPERMATA").First();
                        string pMandiri = AppParameterAccess.GetApplicationParametersValue("PREFIXVAMANDIRI").First();
                        string pBCA = AppParameterAccess.GetApplicationParametersValue("PREFIXVABCA").First();
                        string vaold = db.ExecuteScalar<string>("SELECT COALESCE(VANumber,'') FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1", CustID, FollowUpNo);
                        VaNumber = string.IsNullOrEmpty(Convert.ToString(vaold)) ? VaNumber : Convert.ToString(vaold);
                        if (!string.IsNullOrEmpty(VaNumber))
                        {
                            pf.VAPermata = pPermata + VaNumber;
                            pf.VAMandiri = pMandiri + VaNumber;
                            pf.VABCA = pBCA + VaNumber;
                        }
                        //}
                        pf.BuktiBayar = listDyn.First().BuktiBayar;
                        pf.BuktiBayarData = listDyn.First().BuktiBayarData;
                        pf.BuktiBayar2 = listDyn.First().BuktiBayar2;
                        pf.BuktiBayar2Data = listDyn.First().BuktiBayar2Data;
                        pf.BuktiBayar3 = listDyn.First().BuktiBayar3;
                        pf.BuktiBayar3Data = listDyn.First().BuktiBayar3Data;
                        pf.BuktiBayar4 = listDyn.First().BuktiBayar4;
                        pf.BuktiBayar4Data = listDyn.First().BuktiBayar4Data;
                        pf.BuktiBayar5 = listDyn.First().BuktiBayar5;
                        pf.BuktiBayar5Data = listDyn.First().BuktiBayar5Data;
                        string PolicyOrderNo = Convert.ToString(listDyn.First().PolicyOrderNo);
                        if (!string.IsNullOrEmpty(PolicyOrderNo))
                        {
                            query = @"SELECT DATEADD(DAY,13,Order_Date) AS DueDate FROM dbo.Mst_Order where Order_No = @0";
                            List<dynamic> listDate = AABdb.Fetch<dynamic>(query, PolicyOrderNo);
                            if (listDate.Count > 0)
                            {
                                pf.DueDate = listDate.First().DueDate;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ordData.First().ProductCode) && !string.IsNullOrEmpty(VaNumber))
                            {
                                decimal gp = AABdb.ExecuteScalar<decimal>("SELECT COALESCE(grace_period,0) FROM dbo.Mst_Product WHERE Product_Code = @0", ordData.First().ProductCode);
                                if (gp > 0)
                                {
                                    query = @"SELECT DATEADD(DAY,grace_period,os.EntryDate) DueDate
                                            FROM dbo.OrderSimulation os
                                            INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Product mp
                                            ON os.ProductCode = mp.Product_Code
                                            WHERE os.OrderNo = @0";
                                    List<dynamic> listDate = db.Fetch<dynamic>(query, ordData.First().OrderNo);
                                    if (listDate.Count > 0)
                                    {
                                        pf.DueDate = listDate.First().DueDate;
                                    }
                                }
                            }
                        }
                        pf.IsVAActive = null;
                        if (!string.IsNullOrEmpty(VaNumber) && string.IsNullOrEmpty(pf.RekeningPenampung) && string.IsNullOrEmpty(ordData.First().PolicyOrderNo))
                        {
                            if (listVANo.Count > 0)
                            {
                                if (isReactive == 0)
                                {
                                    DateTime validto = Convert.ToDateTime(listVANo.First().ValidTo);
                                    if (listVANo.First().RowStatus == 0 && (validto.Date >= DateTime.Now.Date))
                                    {
                                        pf.IsVAActive = true;
                                    }
                                    else
                                    {
                                        pf.IsVAActive = false;
                                    }
                                }
                            }
                        }
                        pf.VANo = VaNumber;
                    }
                }
                return pf;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                throw e;
            }
        }

    }
}