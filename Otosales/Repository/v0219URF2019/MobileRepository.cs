﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Microsoft.Reporting.WinForms;
using Otosales.Codes;
using Otosales.dta;
using Otosales.Models.v0219URF2019;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;
using System.Web.Http;

namespace Otosales.Repository.v0219URF2019
{
    public interface IMobileRepository
    {
        void UpdateUserActive(string SalesOfficerID, int IsLogin = 1);
    }
    public class MobileRepository : IMobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();
        public static dynamic GetSurveyZipCode(string CityID)
        {
            dynamic result;
            try
            {
                #region QueryData
                string queryData = @";
SELECT PostalCode ZipCode,PostalDescription ZipCodeDescription
FROM dbo.Postal p
INNER JOIN dbo.City c 
ON p.CityID = c.ID
INNER JOIN BEYONDREPORT.SurveyManagement.dbo.MappingSurveyAreaZipCode msaz
ON msaz.ZipCode = p.PostalCode
WHERE c.ID = @0 AND p.RowStatus = 0 AND msaz.RowStatus = 0
ORDER BY PostalDescription ASC";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB))
                {
                    result = db.Fetch<dynamic>(queryData, CityID);
                    logger.Debug("OK");
                    return result;
                }
            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }
        }

        public static bool IsAllowedIEP(string productcode, bool isexistSFE)
        {
            bool isallowed = false;
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                bool isProductIEP = db.ExecuteScalar<int>(@";select COUNT(*) from applicationparameters where paramvalue like '%" + productcode + "%' AND paramname='Otosales-segment-IEP'") > 0 ? true : false;
                if (isProductIEP)
                {

                    if (isexistSFE)
                    {
                        return true;
                    }

                }
                else
                {
                    isallowed = true;
                }
                return isallowed;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static dynamic GetVehicleUsage(bool isMVGodig = false)
        {
            dynamic res;
            try {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    string query = "";

                    if (isMVGodig)
                    {

                        #region QueryData Godig
                        query = @";
     SELECT 
    Factor_Code,insurance_code,
    Description,status,Entryusr,Entrydt,UpdateUsr,
    UpdateDt,LastUpdatedTime,RowStatus 
    into #TempDtlInsFact  
    FROM Dtl_Ins_Factor WITH(NOLOCK) 
    WHERE 
    (
	    Factor_Code in ('GEOGRA') OR 
	    (Factor_Code = 'MVUSAG' and insurance_code like 'U%') OR 
	    (Factor_Code = 'VHCTYP' and insurance_code like 'T0%')
    )  ORDER BY LastUpdatedTime, Factor_Code, insurance_code ASC

    SELECT * FROM #TempDtlInsFact 
    WHERE Factor_Code='MVUSAG' AND RowStatus = 1 
    AND insurance_code = (SELECT TOP 1 ParamValue FROM AABMobile.dbo.ApplicationParameters 
    WHERE ParamName = 'GODIGITAL-DEFAULT-USAGECODE')
    ORDER BY DESCRIPTION
                    ";
                        #endregion

                    }
                    else
                    {
                        #region QueryData non godig
                        query = @"
     SELECT Factor_Code,insurance_code,Description,status,Entryusr,Entrydt,UpdateUsr,UpdateDt,LastUpdatedTime,RowStatus into #TempDtlInsFact  FROM Dtl_Ins_Factor WITH(NOLOCK) WHERE (Factor_Code in ('GEOGRA') OR (Factor_Code = 'MVUSAG' and insurance_code like 'U%') OR (Factor_Code = 'VHCTYP' and insurance_code like 'T0%'))  ORDER BY LastUpdatedTime, Factor_Code, insurance_code ASC
      SELECT * FROM #TempDtlInsFact WHERE Factor_Code='MVUSAG' AND RowStatus = 1 ORDER BY Description
                    ";
                        #endregion

                    }

                    res = db.Fetch<dynamic>(query);
                }

                return res;
            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }
        }
       
        public static dynamic GetProductType(string salesOfficerId, bool isMVGodig = false)
        {
            dynamic res = null;

            try {

                string query = @"
                  SELECT ProductTypeCode, Description, InsuranceType, RowStatus FROM ProductType WHERE RowStatus = 1
                ";


                if (isMVGodig)
                {
                    query += " AND InsuranceType=1";
                }


                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    using (var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        bool isAllowedSharia = Convert.ToBoolean(aabdb.ExecuteScalar<int>(@"select Count(*) from mst_salesman s WITH (NOLOCK)
inner join Mst_Branch b on s.Branch_id = b.Branch_id
where s.status = 1 and user_id_otosales = @0 and b.biz_type=2", salesOfficerId));
                        query = string.Concat(query, (isAllowedSharia ? " Order By SeqNo ASC" : " AND SeqNo NOT IN(2) Order By SeqNo ASC"));
                        res = db.Fetch<dynamic>(query);
                    }
                }


                return res;
            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }
        }

        public static List<Models.Product> GetProduct(string insurancetype, string salesofficerid, int isRenewal, bool isNew = false, bool isMVGodig = false)
        {
            List<Otosales.Models.Product> result = new List<Models.Product>();
            try
            {
                string query = @"
select DISTINCT  mp.Product_Code [ProductCode], mp.Description, 'GARDAOTO' [ProductTypeCode], @0 [InsuranceType], 1 [RowStatus], CASE WHEN mp.day_calculation_method =360 THEN 360 ELSE 366 END [NDays], Biz_type,mp.cob_id, IIF(PDT.Partner_ID is null, 0, 1) [IsLexus]

from mst_product mp

LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code AND PDT.Partner_ID = 'LEX'

where GETDATE() BETWEEN Valid_From and Valid_To AND mp.Product_Code !='' AND mp.isb2b=0

AND 1 = CASE WHEN @0 = 1 THEN IIF(mp.cob_id = '403' and Biz_type=1 and PDT.Partner_ID is null,1,0)

                      WHEN @0 = 2 THEN IIF(mp.cob_id= '404' and PDT.Partner_ID is null,1,0)

                     WHEN @0 = 3 THEN IIF(PDT.Partner_ID is not null,1,0) 

                     WHEN @0 = 4 THEN IIF(mp.cob_id = '403'and Biz_type=2 and PDT.Partner_ID is null,1,0 ) END

  ";                // Handle ProductAgency
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    bool isAgency = db.ExecuteScalar<int>(@" SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] eu INNER JOIN [BeyondMoss].[AABMobile].[dbo].[SalesOfficer] so ON so.Email = eu.UserID WHERE so.SalesOfficerId=@0", salesofficerid) > 0 ? true : false;
                    query = string.Concat(query, isAgency ? " AND mp.product_type=6" : " AND mp.product_type!=6");

                    if (isMVGodig)
                    {
                        if (isNew && !isAgency)
                        {
                            query = string.Concat(query, @"AND  mp.Product_Code = (SELECT TOP 1 optionValue 
FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions WHERE OptionName = 'GODIGITAL-NEWVEHICLE-PRODUCTCODE')");
                        }
                        else if (isNew && isAgency)
                        {
                            query = string.Concat(query, @"AND  mp.Product_Code = (SELECT TOP 1 optionValue 
FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions WHERE OptionName = 'GODIGITAL-NEWVEHICLE-PRODUCTCODE-AGENCY')");
                        }
                        else if (!isNew && !isAgency)
                        {
                            query = string.Concat(query, @"AND  mp.Product_Code = (SELECT TOP 1 optionValue 
FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions WHERE OptionName = 'GODIGITAL-USEDVEHICLE-PRODUCTCODE')");
                        }
                        else if (!isNew && isAgency)
                        {
                            query = string.Concat(query, @"AND  mp.Product_Code = (SELECT TOP 1 optionValue 
FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions WHERE OptionName = 'GODIGITAL-USEDVEHICLE-PRODUCTCODE-AGENCY')");
                        }

                    }

                    result = db.Fetch<Otosales.Models.Product>(query, insurancetype, isRenewal);

                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static List<BasicCover> GetBasicCover(int year, bool isBasic, bool isMVGodig = false)
        {
            List<BasicCover> bcList = new List<BasicCover>();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    if (DateTime.Now.Year - year < 5)
                    {
                        bcList = db.Fetch<BasicCover>(@"
SELECT Id,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE isBasic in (" + (isBasic ? "1,0" : "1") + @") AND RowStatus=1");
                    }
                    else
                    {
                        bcList = db.Fetch<BasicCover>(@"SELECT ID,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE (ComprePeriod=1 AND TLOPeriod=0) or(ComprePeriod=0 AND TLOPeriod=1)");
                    }

                    if (isMVGodig)
                    {
                        bcList = db.Fetch<BasicCover>(@"SELECT ID,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE (ComprePeriod=1 AND TLOPeriod=0) or(ComprePeriod=0 AND TLOPeriod=1)");
                    }
                }

                return bcList;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static List<dynamic> GetVehicleFullTextTerm(string search, string istlo, bool isMVGodig = false)
        {
            List<dynamic> dataVehicle = new List<dynamic>();
            try
            {

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                   
                   
                    if (isMVGodig)
                    {
                        #region isMVGodig
                        string query = @";DECLARE @@MinVal INT = (SELECT MaxAge FROM [AABMobile].[dbo].[VehicleLoadingAgeMatrix] where CoverageID=@3 AND RenewalF='NEW' AND BrandCode is null)
DECLARE @@TempResult TABLE
            (
              VehicleFullTextObjectID INT ,
              VehicleCode VARCHAR(MAX) ,
              BrandCode VARCHAR(MAX) ,
              VehicleTypeCode VARCHAR(MAX) ,
              ModelCode VARCHAR(MAX) ,
              VehicleYear VARCHAR(MAX) ,
              GeoAreaCode VARCHAR(MAX) ,
              BrandDescription VARCHAR(MAX) ,
              VehicleTypeDescription VARCHAR(MAX) ,
              ModelDescription VARCHAR(MAX) ,
              Series VARCHAR(MAX) ,
              GeoAreaDescription VARCHAR(MAX) ,
              PlatCode VARCHAR(MAX) ,
              Terms VARCHAR(MAX) ,
              VehicleDescription VARCHAR(MAX) ,
              CreatedBy VARCHAR(MAX) ,
              CreatedDate DATETIME ,
              ModifiedBy VARCHAR(MAX) ,
              ModifiedDate DATETIME
            )
INSERT @@TempResult exec usp_VehicleFulltextSearchNonGeoArea @@Keywords = @2 , @@Condition = 'AND' , @@PrecisionF = 0
SELECT DISTINCT CASE WHEN VehicleYear < YEAR(GETDATE())  THEN 0 ELSE 1 END AS IsNew, VehicleFullTextObjectID AS VehicleID, VehicleDescription as VehicleDescription, tr.VehicleCode, tr.BrandCode ,tr.ModelCode ,VehicleYear AS Year, VehicleTypeCode as Type,tr.Series, Sitting,v.ProductTypeCode
FROM @@TempResult tr
INNER JOIN Vehicle v ON V.VehicleCode = tr.VehicleCode
    AND V.BrandCode = tr.BrandCode
    AND V.ModelCode = tr.ModelCode
    AND v.year = tr.VehicleYear
    AND v.Series = tr.Series
WHERE VehicleYear >= YEAR(DATEADD(year, @@MinVal,GETDATE()))
AND NOT EXISTS (SELECT vehiclecode FROM ExcludedVehicle e WHERE e.VehicleCode = tr.vehicleCode AND e.ApplicationName = @0 AND e.ModuleName = @1 AND e.rowStatus = 1)
                              AND NOT EXISTS (SELECT vehicleTypeCode FROM ExcludedVehicleType e WHERE e.VehicleTypeCode =  tr.VehicleTypeCode and e.ApplicationName = @0 AND e.ModuleName = @1 AND e.rowStatus = 1)
 AND v.price>10000000";

                        dataVehicle = db.Fetch<dynamic>(query, "ASURANSIASTRAWEB", "QUOTATION", search, istlo);


                        #endregion
                    }
                    else
                    {

                        #region Non Godig 

                        string query = @";DECLARE @@TempResult TABLE
                                                        (
                                                          VehicleFullTextObjectID INT ,
                                                          VehicleCode VARCHAR(MAX) ,
                                                          BrandCode VARCHAR(MAX) ,
                                                          VehicleTypeCode VARCHAR(MAX) ,
                                                          ModelCode VARCHAR(MAX) ,
                                                          VehicleYear VARCHAR(MAX) ,
                                                          BrandDescription VARCHAR(MAX) ,
                                                          VehicleTypeDescription VARCHAR(MAX) ,
                                                          ModelDescription VARCHAR(MAX) ,
                                                          Series VARCHAR(MAX) ,
                                                          Terms VARCHAR(MAX) ,
                                                          VehicleDescription VARCHAR(MAX) ,
                                                          CreatedBy VARCHAR(MAX) ,
                                                          CreatedDate DATETIME ,
                                                          ModifiedBy VARCHAR(MAX) ,
                                                          ModifiedDate DATETIME
                                                        )
                                            INSERT @@TempResult exec usp_VehicleFulltextSearchOtosales  @0 ,  'AND' ,  0
                                                    SELECT DISTINCT TOP 1500 CASE WHEN VehicleYear < YEAR(GETDATE())  THEN 0 ELSE 1 END AS IsNew, VehicleFullTextObjectID AS VehicleID,VehicleDescription as VehicleDescription, tr.VehicleCode, tr.BrandCode ,tr.ModelCode ,VehicleYear AS Year, VehicleTypeCode as Type,tr.Series , Sitting,v.ProductTypeCode
                                    FROM @@TempResult tr
                                            INNER JOIN Vehicle v ON V.VehicleCode = tr.VehicleCode 
                                                AND V.BrandCode = tr.BrandCode 
                                                AND V.ModelCode = tr.ModelCode 
                                                AND v.year = tr.VehicleYear 
                                                AND v.Series = tr.Series
                                                AND v.RowStatus=1
                                            INNER JOIN VehicleModel vm ON vm.ModelCode=tr.ModelCode AND vm.Rowstatus=1
                                            INNER JOIN VehicleBrand vb ON vb.BrandCode=tr.BrandCode AND vb.Rowstatus=1
                                            INNER JOIN VehicleSeries vs ON vs.Series=tr.Series AND vs.Rowstatus=1";

                        dataVehicle = db.Fetch<dynamic>(query, search);
                        #endregion

                    }


                    logger.Debug("OK");

                }
                return dataVehicle;
            }
            catch (Exception e)
            {
                throw (e);

            }
        }

        public //static 
            void UpdateUserActive(string SalesOfficerID, int IsLogin = 1)
        {
            try
            {
                #region query
                string query = @";
UPDATE [dbo].[MstTelesalesUser]
   SET [IsOnline] = @1
      ,[LastActiveDate] = GETDATE()
      ,[ModifiedBy] = 'OTOSALES'
      ,[ModifiedDate] = GETDATE()
 WHERE SalesOfficerID = @0";

                #endregion
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    db.Execute(query, SalesOfficerID, IsLogin);
                }

            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }

        }

        #region Generate GUID Temp Penawaran
        public static string GenerateUUIDTempPenawaran()
        {
            return generateID() + md5Hash(new Random().Next(10000, 99999)+"");
        }

        private static string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }

        private static string md5Hash(string hash)
        {
            byte[] asciiBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(hash);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
        }
        #endregion

        public static bool SendViaWA(SendViaWAParam param) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var mblDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                if (param.ListAtt == null)
                {
                    param.ListAtt = new List<A2isMessagingAttachment>();
                }
                if (param.IsAddAtt)
                {
                    string qGetInsType = @"SELECT COALESCE(InsuranceType,0) InsuranceType FROM dbo.OrderSimulation WHERE OrderNo = @0";
                    int insuranceType = mblDB.ExecuteScalar<int>(qGetInsType, param.OrderNo);
                    param.ListAtt.Add(GenerateReportFile(param.OrderNo, insuranceType));
                }
                string qGetData = @";SELECT os.PolicyOrderNo, CASE WHEN COALESCE(isCompany,0) = 0 THEN pc.Name ELSE pcy.CompanyName END Name, 
mf.FollowUpTypeDes OrderType, 
os.SalesOfficerID, pc.Phone1 FROM dbo.OrderSimulation os
INNER JOIN dbo.ProspectCustomer pc ON pc.CustID = os.CustID
LEFT JOIN dbo.ProspectCompany pcy ON pcy.CustID = os.CustID
INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
INNER JOIN dbo.MstFollowUpType mf ON mf.FollowUpTypeID = f.FollowUpTypeID
WHERE os.OrderNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1";
                List<dynamic> ordData = mblDB.Fetch<dynamic>(qGetData, param.OrderNo);
                if (ordData.Count > 0) {
                    string AttachmentID = "";
                    if (param.ListAtt.Count > 0) {
                        int ac = 0;
                        foreach (A2isMessagingAttachment a in param.ListAtt)
                        {
                            if (a.FileByte != null) {
                                ac++;
                            }
                        }
                        if (ac > 0) {
                            AttachmentID = System.Guid.NewGuid().ToString();
                        }
                    }
                    string qInsertWAmsg = @"
INSERT INTO dbo.WAMessaging
        ( OrderNo ,
          PolicyOrderNo ,
          CustomerName ,
          PhoneNo ,
          OrderType ,
          SalesOfficerID ,
          TextMessage ,
          RowStatus ,
		  AttachmentID,
          CreatedBy ,
          CreatedDate
        )
VALUES  ( @0 , -- OrderNo - varchar(100)
          @1 , -- PolicyOrderNo - varchar(16)
          @2 , -- CustomerName - varchar(100)
          @3 , -- PhoneNo - varchar(30)
          @4 , -- OrderType - varchar(10)
          @5 , -- SalesOfficerID - varchar(16)
          @6 , -- TextMessage - varchar(max)
          1 , -- RowStatus - bit
		  @7, -- AttachmentID - varchar(100)
          @5 , -- CreatedBy - varchar(50)
          GETDATE()  -- CreatedDate - datetime
        )";
                    string qInsertWAAttMsg = @";INSERT INTO dbo.WAMessagingAttachment
        ( AttachmentID ,
          AttachmentDescription ,
          FileByte ,
          FileExtension ,
          FileContentType ,
          CreatedBy ,
          CreatedDate ,
          RowStatus
        )
VALUES  ( @0 , -- AttachmentID - varchar(100)
          @1 , -- AttachmentDescription - varchar(255)
          @2 , -- FileByte - varbinary(max)
          @3 , -- FileExtension - varchar(10)
          @4 , -- FileContentType - varchar(max)
          @5 , -- CreatedBy - varchar(50)
          GETDATE(),  -- CreatedDate - datetime
          1  -- RowStatus - smallint
        )";
                    mblDB.Execute(qInsertWAmsg, param.OrderNo, ordData.First().PolicyOrderNo, ordData.First().Name
                        , param.PhoneNo, ordData.First().OrderType, ordData.First().SalesOfficerID, param.WAMessage, AttachmentID);
                    foreach (A2isMessagingAttachment att in param.ListAtt)
                    {
                        if (att.FileByte != null) {
                            mblDB.Execute(qInsertWAAttMsg
                                , AttachmentID
                                , att.AttachmentDescription
                                , att.FileByte
                                , att.FileExtension
                                , att.FileContentType
                                , ordData.First().SalesOfficerID);                        
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static dynamic GetEmailTemplate(string OrderNo, int flag)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            dynamic res = new ExpandoObject();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qSelect = @";SELECT TOP 1 COALESCE(IsRenewal,0) IsRenewal, pt.Description ProductName,
                                f.ProspectName CustName, vb.Description Brand, vm.Description Model,
                                vt.Description [Type], so.Name, so.Email, so.Phone1, os.CustID
                                FROM dbo.OrderSimulation os 
                                INNER JOIN dbo.FollowUp f
                                ON f.FollowUpNo = os.FollowUpNo
                                INNER JOIN dbo.ProductType pt 
                                ON os.InsuranceType = pt.InsuranceType
                                INNER JOIN dbo.OrderSimulationMV omv
                                ON omv.OrderNo = os.OrderNo
                                LEFT JOIN dbo.VehicleBrand vb 
                                ON omv.BrandCode = vb.BrandCode
                                LEFT JOIN dbo.VehicleModel vm
                                ON omv.BrandCode = vm.BrandCode AND omv.ModelCode = vm.ModelCode
                                LEFT JOIN dbo.VehicleType vt 
                                ON vt.VehicleTypeCode = omv.Type
                                LEFT JOIN dbo.SalesOfficer so
                                ON so.SalesOfficerID = os.SalesOfficerID
                                WHERE os.OrderNo = @0";
                List<dynamic> ordData = db.Fetch<dynamic>(qSelect, OrderNo);
                if (ordData.Count > 0)
                {
                    int isNew = Convert.ToInt32(ordData.First().IsRenewal);
                    List<string> Subjects = AppParameterAccess.GetApplicationParametersValue("SUBJECT-SEND-EMAIL");
                    string subject = Subjects[isNew];
                    subject = subject.Replace("#0", ordData.First().ProductName);
                    subject = subject.Replace("#1", ordData.First().CustName);
                    string body = "";
                    string emailto = "";
                    List<dynamic> lsCust = db.Fetch<dynamic>(@"SELECT COALESCE(Email1, '') EmailTo FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1"
                        , ordData.First().CustID);
                    if (lsCust.Count > 0) {
                        emailto = lsCust.First().EmailTo;
                    }
                    if (flag == 0)
                    {
                        List<string> Bodys = AppParameterAccess.GetApplicationParametersValue("BODY-SEND-EMAIL");
                        body = Bodys[isNew];
                        body = body.Replace("#ProductName", ordData.First().ProductName);
                        body = body.Replace("#BrandName", ordData.First().Brand);
                        body = body.Replace("#Model", ordData.First().Model);
                        body = body.Replace("#Type", ordData.First().Type);
                        body = body.Replace("#SalesOfficerName", ordData.First().Name);
                        body = body.Replace("#SalesOfficerEmail", ordData.First().Email);
                        body = body.Replace("#SalesOfficerPhoneno", ordData.First().Phone1);
                    }
                    res.Subject = subject;
                    res.Body = body;
                    res.EmailTo = emailto;

                    return res;
                }
                return null;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static string GetOrderNoQuotation(string ReferenceNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string res = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qSelect = @";
SELECT  COALESCE(OrderNo, '') AS OrderNo
FROM    dbo.FollowUp fu
        INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = fu.FollowUpNo
WHERE   fu.ReferenceNo = @0";
                List<dynamic> ordData = db.Fetch<dynamic>(qSelect, ReferenceNo);
                if (ordData.Count > 0)
                {
                    res = ordData.First().OrderNo;
                }
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        
        public static string GetSoIdQuotationLead(string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string res = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qSelect = @";SELECT COALESCE(SalesOfficerID,'') SalesOfficerID
                        FROM dbo.QuotationLead WHERE ReferenceNo = @0 AND RowStatus = 1";
                List<dynamic> ordData = db.Fetch<dynamic>(qSelect, OrderNo);
                if (ordData.Count > 0)
                {
                    res = ordData.First().SalesOfficerID;
                }
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static DownloadQuotationResult GetByteReport(string orderNo)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportServerURLBeyond = ConfigurationManager.AppSettings["ReportServerURLBeyond"].ToString();
            string ReportPathOne = ConfigurationManager.AppSettings["ReportPathOne"].ToString();
            string ReportPathTinsOne = ConfigurationManager.AppSettings["ReportPathTinsOne"].ToString();
            string ReportPathLexusOne = ConfigurationManager.AppSettings["ReportPathLexusOne"].ToString();
            string ReportPathTwo = ConfigurationManager.AppSettings["ReportPathTwo"].ToString();
            string ReportPathTinsTwo = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusTwo = ConfigurationManager.AppSettings["ReportPathLexusTwo"].ToString();
            string ReportPathThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathTinsThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusThree = ConfigurationManager.AppSettings["ReportPathLexusThree"].ToString();
            string CertificateQuotation = ConfigurationManager.AppSettings["CertificateQuotation"].ToString();
            string CertificateRenewalNotice = ConfigurationManager.AppSettings["CertificateRenewalNotice"].ToString();
            string CertificateQuotationGODIG = ConfigurationManager.AppSettings["CertificateQuotationGODIG"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;
            string CredentialsRenewalUsername = ConfigurationManager.AppSettings["CredentialsRenewalUsername"].ToString(); ;
            string CredentialsRenewalPassword = ConfigurationManager.AppSettings["CredentialsRenewalPassword"].ToString(); ;
            string CredentialsRenewalDomain = ConfigurationManager.AppSettings["CredentialsRenewalDomain"].ToString(); ;

            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";
            int idReportRennot = 0;

            #region Insert Data to RPT_Renewal_Notice Dan RPT_Renewal_Notice_Detail
            string qReport = @";SELECT os.OldPolicyNo, os.SalesOfficerID, CAST(IIF(LEN(f.ReferenceNo)>10,1,0) AS BIT) IsMVGodig 
FROM OrderSimulation os INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo 
WHERE OrderNo = @0 ANd os.RowStatus = 1 AND f.RowStatus = 1";
            dynamic policyNo = "";
            using (a2isDBHelper.Database dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                policyNo = dbMobile.FirstOrDefault<dynamic>(qReport, orderNo);
            }
            if (!policyNo.IsMVGodig)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(policyNo.OldPolicyNo)))
                {
                    qReport = @";EXECUTE [dbo].[usp_InsertPrintRenewalNoticeOtosales] @0, @1";
                    using (a2isDBHelper.Database dbAAB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        idReportRennot = dbAAB.FirstOrDefault<int>(qReport, Convert.ToString(policyNo.OldPolicyNo), Convert.ToString(policyNo.SalesOfficerID));
                        Otosales.Repository.vWeb2.MobileRepository.InsertPrintRennotExtended(idReportRennot, orderNo);
                    }
                }
                else
                {
                    Otosales.Repository.v0001URF2020.MobileRepository.InsertQuotationPrintTable(orderNo);
                }
            }
            else {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE)) {
                    qReport = @";EXEC dbo.usp_InsertDataOtosalesForQuotationTelesales @0";
                    db.Execute(qReport, orderNo);
                }
            }
            #endregion
            DownloadQuotationResult result = new DownloadQuotationResult();
            NetworkCredential NetCredential = new NetworkCredential();
            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                int yearCoverage;
                int insuranceType;

                string query = @"";

                query = @"SELECT o.PolicyNo, p.Name, o.InsuranceType, o.YearCoverage, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal,
                        CASE 
	                        WHEN f.referenceno IS NOT NULL AND LEN(f.referenceno) > 10
		                        THEN 1
	                        ELSE 0
                        END AS
	                        IsMVGodig
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f 
                        ON f.CustID = p.CustID
                        INNER JOIN dbo.OrderSimulation o
                        ON o.FollowUpNo = f.FollowUpNo
                        INNER JOIN dbo.OrderSimulationMV omv
                        ON omv.OrderNo = o.OrderNo
                        WHERE o.OrderNo = @0";
                List<dynamic> dt = db.Fetch<dynamic>(query, orderNo);
                if (dt.Count > 0)
                {
                    yearCoverage = Convert.ToInt32(dt.First().YearCoverage);
                    insuranceType = Convert.ToInt32(dt.First().InsuranceType);
                    using (ReportViewer rView = new ReportViewer())
                    {
                        rView.ProcessingMode = ProcessingMode.Remote;

                        ServerReport serverReport = rView.ServerReport;
                        if (!dt.First().IsRenewal)
                        {
                            if (dt.First().IsMVGodig == 1) //Generate Quotation With RDL 0219URF2019
                            {
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotationGODIG;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }
                            else
                            {                      //Generate Quotation With Old RDL
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotation;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }

                        }
                        else
                        {
                            serverReport.ReportServerUrl = new Uri(ReportServerURLBeyond);
                            serverReport.ReportPath = CertificateRenewalNotice;
                            NetCredential = new NetworkCredential(CredentialsRenewalUsername, CredentialsRenewalPassword, CredentialsRenewalDomain);


                        }

                        serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;
                        #region Catat Print Quotation
                        query = @"INSERT INTO dbo.PrintQuotationHistoryLog
                                        ( OrderNo, RowStatus, EntryDate )
                                VALUES  ( @0, -- OrderNo - varchar(100)
                                          1, -- RowStatus - smallint
                                          GETDATE()  -- EntryDate - datetime
                                          )";
                        db.Execute(query, orderNo);
                        #endregion

                        ReportParameter[] parameters = new ReportParameter[1];
                        ReportParameter reportParam = new ReportParameter();
                        if (!dt.First().IsRenewal)
                        {
                            reportParam.Name = "OrderNo";
                            reportParam.Values.Add(orderNo);
                        }
                        else
                        {

                            reportParam.Name = "PolicyNo";
                            reportParam.Values.Add(Convert.ToString(policyNo.OldPolicyNo));
                        }
                        parameters[0] = reportParam;
                        rView.ServerReport.SetParameters(parameters);
                        bytes = rView.ServerReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                        result.Bytes = bytes;

                        #region filename
                        int cetakanke = db.ExecuteScalar<int>("SELECT COUNT(*) FROM dbo.PrintQuotationHistoryLog WHERE OrderNo=@0", orderNo) + 1;

                        string fileName = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='ATTFILENAME' AND ParamType='TMP' AND ParamValue2=@0", dt.First().IsRenewal);
                        fileName = fileName.Replace("@CetakanKe", cetakanke.ToString()).Replace("@TanggalCetak", DateTime.Now.ToString("dd-MM-yyyy"));
                        if (dt.First().IsRenewal)
                        {
                            fileName = fileName.Replace("@NoPolis", dt.First().PolicyNo.Trim());

                        }
                        #endregion

                        result.FileName = fileName;
                        //                        query = @"INSERT INTO dbo.PrintQuotationHistoryLog
                        //                                                ( OrderNo, RowStatus, EntryDate )
                        //                                        VALUES  ( @0, -- OrderNo - varchar(100)
                        //                                                  1, -- RowStatus - smallint
                        //                                                  GETDATE()  -- EntryDate - datetime
                        //                                                  )";
                        //                        db.Execute(query, orderNo);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();
                throw ex;
            }
        }

        public static A2isMessagingAttachment GenerateReportFile(string orderNo, int insuranceType)
        {

            A2isMessagingAttachment att = new A2isMessagingAttachment();
            string filePath = "";
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportServerURLBeyond = ConfigurationManager.AppSettings["ReportServerURLBeyond"].ToString();
            string ReportPathOne = ConfigurationManager.AppSettings["ReportPathOne"].ToString();
            string ReportPathTinsOne = ConfigurationManager.AppSettings["ReportPathTinsOne"].ToString();
            string ReportPathLexusOne = ConfigurationManager.AppSettings["ReportPathLexusOne"].ToString();
            string ReportPathTwo = ConfigurationManager.AppSettings["ReportPathTwo"].ToString();
            string ReportPathTinsTwo = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusTwo = ConfigurationManager.AppSettings["ReportPathLexusTwo"].ToString();
            string ReportPathThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathTinsThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusThree = ConfigurationManager.AppSettings["ReportPathLexusThree"].ToString();
            string CertificateQuotation = ConfigurationManager.AppSettings["CertificateQuotation"].ToString();
            string CertificateRenewalNotice = ConfigurationManager.AppSettings["CertificateRenewalNotice"].ToString();
            string CertificateQuotationGODIG = ConfigurationManager.AppSettings["CertificateQuotationGODIG"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;
            string CredentialsRenewalUsername = ConfigurationManager.AppSettings["CredentialsRenewalUsername"].ToString(); ;
            string CredentialsRenewalPassword = ConfigurationManager.AppSettings["CredentialsRenewalPassword"].ToString(); ;
            string CredentialsRenewalDomain = ConfigurationManager.AppSettings["CredentialsRenewalDomain"].ToString(); ;

            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";
            int idReportRennot = 0;

            #region Insert Data to RPT_Renewal_Notice Dan RPT_Renewal_Notice_Detail
            string qReport = @"SELECT os.OldPolicyNo, os.SalesOfficerID, CAST(IIF(LEN(f.ReferenceNo)>10,1,0) AS BIT) IsMVGodig 
FROM OrderSimulation os INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo 
WHERE OrderNo = @0 ANd os.RowStatus = 1 AND f.RowStatus = 1";
            dynamic policyNo = "";
            using (a2isDBHelper.Database dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                policyNo = dbMobile.FirstOrDefault<dynamic>(qReport, orderNo);
            }
            if (!policyNo.IsMVGodig)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(policyNo.OldPolicyNo)))
                {
                    qReport = @";EXECUTE [dbo].[usp_InsertPrintRenewalNoticeOtosales] @0, @1";
                    using (a2isDBHelper.Database dbAAB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        idReportRennot = dbAAB.FirstOrDefault<int>(qReport, Convert.ToString(policyNo.OldPolicyNo), Convert.ToString(policyNo.SalesOfficerID));
                        Otosales.Repository.vWeb2.MobileRepository.InsertPrintRennotExtended(idReportRennot, orderNo);
                    }
                }
                else
                {

                    Otosales.Repository.v0001URF2020.MobileRepository.InsertQuotationPrintTable(orderNo);
                }
            }
            else
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    qReport = @";EXEC dbo.usp_InsertDataOtosalesForQuotationTelesales @0";
                    db.Execute(qReport, orderNo);
                }
            }

            #endregion
            DownloadQuotationResult result = new DownloadQuotationResult();
            NetworkCredential NetCredential = new NetworkCredential();
            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                int yearCoverage;

                string query = @"";

                query = @"SELECT o.PolicyNo, p.Name, o.InsuranceType, o.YearCoverage, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal,
                        CASE 
	                        WHEN f.referenceno IS NOT NULL AND LEN(f.referenceno) > 10
		                        THEN 1
	                        ELSE 0
                        END AS
	                        IsMVGodig
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f 
                        ON f.CustID = p.CustID
                        INNER JOIN dbo.OrderSimulation o
                        ON o.FollowUpNo = f.FollowUpNo
                        INNER JOIN dbo.OrderSimulationMV omv
                        ON omv.OrderNo = o.OrderNo
                        WHERE o.OrderNo = @0";
                List<dynamic> dt = db.Fetch<dynamic>(query, orderNo);
                if (dt.Count > 0)
                {
                    yearCoverage = Convert.ToInt32(dt.First().YearCoverage);
                    insuranceType = Convert.ToInt32(dt.First().InsuranceType);
                    using (ReportViewer rView = new ReportViewer())
                    {
                        rView.ProcessingMode = ProcessingMode.Remote;

                        ServerReport serverReport = rView.ServerReport;

                        //switch (yearCoverage)
                        //{
                        //    case 1:
                        //        switch (insuranceType)
                        //        {
                        //            case 1: serverReport.ReportPath = ReportPathOne; break;
                        //            case 2: serverReport.ReportPath = ReportPathTinsOne; break;
                        //            case 3: serverReport.ReportPath = ReportPathLexusOne; break;
                        //        }; break;
                        //    case 2:
                        //        switch (insuranceType)
                        //        {
                        //            case 1: serverReport.ReportPath = ReportPathTwo; break;
                        //            case 2: serverReport.ReportPath = ReportPathTinsTwo; break;
                        //            case 3: serverReport.ReportPath = ReportPathLexusTwo; break;
                        //        }; break;
                        //    case 3:
                        //        switch (insuranceType)
                        //        {
                        //            case 1: serverReport.ReportPath = ReportPathThree; break;
                        //            case 2: serverReport.ReportPath = ReportPathTinsThree; break;
                        //            case 3: serverReport.ReportPath = ReportPathLexusThree; break;
                        //        }; break;
                        //}
                        if (!dt.First().IsRenewal)
                        {
                            if (dt.First().IsMVGodig == 1) //Generate Quotation With RDL 0219URF2019
                            {
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotationGODIG;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }
                            else
                            {                      //Generate Quotation With Old RDL
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotation;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }


                        }
                        else
                        {
                            serverReport.ReportServerUrl = new Uri(ReportServerURLBeyond);
                            serverReport.ReportPath = CertificateRenewalNotice;
                            NetCredential = new NetworkCredential(CredentialsRenewalUsername, CredentialsRenewalPassword, CredentialsRenewalDomain);
                        }

                        serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;

                        #region Catat Print Quotation
                        query = @"INSERT INTO dbo.PrintQuotationHistoryLog
                                        ( OrderNo, RowStatus, EntryDate )
                                VALUES  ( @0, -- OrderNo - varchar(100)
                                          1, -- RowStatus - smallint
                                          GETDATE()  -- EntryDate - datetime
                                          )";
                        db.Execute(query, orderNo);
                        #endregion
                        ReportParameter[] parameters = new ReportParameter[1];
                        ReportParameter reportParam = new ReportParameter();
                        if (!dt.First().IsRenewal)
                        {
                            reportParam.Name = "OrderNo";
                            reportParam.Values.Add(orderNo);
                        }
                        else
                        {

                            reportParam.Name = "PolicyNo";
                            reportParam.Values.Add(Convert.ToString(policyNo.OldPolicyNo));
                        }
                        parameters[0] = reportParam;
                        rView.ServerReport.SetParameters(parameters);
                        bytes = rView.ServerReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

                        #region filename
                        int cetakanke = db.ExecuteScalar<int>("SELECT COUNT(*) FROM dbo.PrintQuotationHistoryLog WHERE OrderNo=@0", orderNo) + 1;

                        string fileName = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='ATTFILENAME' AND ParamType='TMP' AND ParamValue2=@0", Convert.ToInt16(dt.First().IsRenewal));
                        fileName = fileName.Replace("@CetakanKe", cetakanke.ToString()).Replace("@TanggalCetak", DateTime.Now.ToString("dd-MM-yyyy"));
                        if (dt.First().IsRenewal)
                        {
                            fileName = fileName.Replace("@NoPolis", dt.First().PolicyNo.Trim());

                        }
                        #endregion
                        filePath = ReportFiles + fileName + ".pdf"; //"QUOTATION-" + OrderNo + ".pdf";

                        if (File.Exists(filePath))
                            File.Delete(filePath);
                        // 1. Read lock information from file   2. If locked by us, delete the file
                        //using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Delete))
                        //{
                        //    File.Delete(filePath);
                        //}

                        //using (FileStream fs = File.Create(filePath)) { }

                        att.FileByte = bytes;
                        att.FileContentType = "application/pdf";
                        att.FileExtension = ".pdf";
                        att.AttachmentDescription = fileName;
                    }
                }
            }
            catch (Exception ex)
            {
                filePath = "";
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();

            }

            return att;
        }
        #region private
        internal static dynamic GetSurveyScheduleSurveyManagement(string cityid, string zipcode, DateTime d)
        {
            dynamic result = null;
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            string qGetSurveyAddress = @"SELECT AreaCode GeoAreaCode FROM SurveyManagement.dbo.MappingSurveyAreaZipCode where ZipCode = @0 AND RowStatus = 0";
            List<dynamic> addresses = AABDB.Fetch<dynamic>(qGetSurveyAddress, zipcode);
            if (addresses.Count > 0)
            {
                string GeoAreaCode = addresses.First().GeoAreaCode;
                if (!string.IsNullOrEmpty(GeoAreaCode))
                {
                    string QueryGetGAScheduleWithDate = @";SELECT  @0 AS SurveyDate
        INTO    #SurveyDate

            SELECT  CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
						 WHEN tc.TimeCategoryCode = 'TC2' THEN 40
						 WHEN tc.TimeCategoryCode = 'TC3' THEN 41
					END ScheduleTime ,
					ZipCode ,
					msast.AreaCode SurveyareaCode ,
					CityId ,
					TC.TimeCategoryDescription ScheduleTimeTextID ,
					CASE WHEN tc.TimeCategoryCode = 'TC1'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
						 WHEN tc.TimeCategoryCode = 'TC2'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
						 WHEN tc.TimeCategoryCode = 'TC3'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
					END ScheduleTimeTextEN ,
                    msast.Quota ,
                    ( SELECT    COUNT(SurveyOrderID)
                      FROM      SurveyManagement.dbo.SurveyOrder AS so
                      WHERE     so.RowStatus = 0
                                AND so.IsRescheduleF = 0
                                AND so.SurveyDate = sd.SurveyDate
                                AND so.AreaCode = msast.AreaCode
                                AND so.SurveyTime >= msast.Start
                                AND so.SurveyTime < msast.[End]
                    ) AS slotbooked
            INTO    #DateSlot
            FROM    #SurveyDate AS sd
                    FULL JOIN SurveyManagement.dbo.MappingSurveyAreaSlotTime AS msast ON 1 = 1
					INNER JOIN ( SELECT DISTINCT
                            PostalCode ZipCode,
                            CityID,
							AreaCode
                     FROM   BEYONDMOSS.Asuransiastra.dbo.Postal p
					 INNER JOIN SurveyManagement.dbo.MappingSurveyAreaZipCode msazc
					 ON p.PostalCode = msazc.ZipCode AND p.RowStatus = 0
                   ) c ON c.AreaCode = msast.AreaCode
				   LEFT JOIN BEYONDMOSS.GardaAkses.GA.TimeCategory tc ON TC.ScheduleTime =  SUBSTRING(CONVERT(VARCHAR, msast.Start), 1, 5)
            WHERE   msast.AreaCode = @1
                    AND msast.RowStatus = 0
					AND c.ZipCode = @2

			IF (CAST(@0 AS DATE) = CAST(GETDATE() AS DATE))
			BEGIN
				SELECT * FROM #DateSlot
			END
			ELSE
			BEGIN
				SELECT * FROM #DateSlot
				WHERE Quota - slotbooked >0
			END
    DROP TABLE #SurveyDate
    DROP TABLE #DateSlot";
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        string AvailableDate = d.ToString("yyyy-MM-dd");
                        result = db.Fetch<dynamic>(QueryGetGAScheduleWithDate, AvailableDate, GeoAreaCode, zipcode);
                    }
                }
            }

            return result;

        }
        internal static dynamic GetScheduleSurveyDalam(List<string> TimeCodeSurveyDalam)
        {
            dynamic result = null;

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAAKSES))
            {
                string queryGetScheduleSurveyDalam = @"
SELECT 
        CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
             WHEN tc.TimeCategoryCode = 'TC2' THEN 40
             WHEN tc.TimeCategoryCode = 'TC3' THEN 41
             WHEN tc.TimeCategoryCode = 'TC4' THEN 42
        END ScheduleTime ,
        TC.TimeCategoryDescription ScheduleTimeTextID ,
        CASE WHEN tc.TimeCategoryCode = 'TC1'
             THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
             WHEN tc.TimeCategoryCode = 'TC2'
             THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
             WHEN tc.TimeCategoryCode = 'TC3'
             THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
             WHEN tc.TimeCategoryCode = 'TC4'
             THEN REPLACE(TC.TimeCategoryDescription, 'Malam', 'Night')
        END ScheduleTimeTextEN
FROM    GA.TimeCategory TC WHERE  timecategorycode in (@0)";
                result = db.Fetch<dynamic>(queryGetScheduleSurveyDalam, TimeCodeSurveyDalam.Count > 0 ? TimeCodeSurveyDalam : new List<string>(new string[] { "0" }));
                return result;
            }
        }

        #endregion
    }
}