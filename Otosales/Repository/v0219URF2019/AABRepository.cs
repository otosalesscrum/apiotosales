﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.dta;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Otosales.Repository.v0219URF2019
{
    public interface IAABRepository
    {

    }
    public class AABRepository : IAABRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();
        #region private query
        private static string qUpdatePhoneCompanyABB = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @2 AND CustID = @0)
                                                    BEGIN
	                                                    UPDATE dbo.Mst_Customer_AdditionalInfo SET AdditionalInfoValue = @1 WHERE AdditionalCode = @2 AND CustID = @0
                                                    END
                                                    ELSE
                                                    BEGIN
	                                                    INSERT INTO dbo.Mst_Customer_AdditionalInfo
	                                                            ( CustID ,
	                                                              AdditionalCode ,
	                                                              AdditionalInfoValue ,
	                                                              CreatedBy ,
	                                                              CreatedDate ,
	                                                              RowStatus
	                                                            )
	                                                    VALUES  ( @0 , -- CustID - char(11)
	                                                              @2 , -- AdditionalCode - varchar(5)
	                                                              @1 , -- AdditionalInfoValue - varchar(255)
	                                                              'OTOSL' , -- CreatedBy - varchar(50)
	                                                              GETDATE() , -- CreatedDate - datetime
	                                                              0  -- RowStatus - smallint
	                                                            )
                                                    END";
        private static string qUpdatePhoneCompanyProspectABB = @";IF EXISTS(SELECT * FROM dbo.Mst_Prospect_AdditionalInfo WHERE AdditionalCode = @2 AND ProspectID = @0)
                                                    BEGIN
	                                                    UPDATE dbo.Mst_Prospect_AdditionalInfo SET AdditionalInfoValue = @1 WHERE AdditionalCode = @2 AND ProspectID = @0
                                                    END
                                                    ELSE
                                                    BEGIN
	                                                    INSERT INTO dbo.Mst_Prospect_AdditionalInfo
	                                                            ( ProspectID ,
	                                                              AdditionalCode ,
	                                                              AdditionalInfoValue ,
	                                                              CreatedBy ,
	                                                              CreatedDate ,
	                                                              RowStatus
	                                                            )
	                                                    VALUES  ( @0 , -- CustID - char(11)
	                                                              @2 , -- AdditionalCode - varchar(5)
	                                                              @1 , -- AdditionalInfoValue - varchar(255)
	                                                              'OTOSL' , -- CreatedBy - varchar(50)
	                                                              GETDATE() , -- CreatedDate - datetime
	                                                              0  -- RowStatus - smallint
	                                                            )
                                                    END";
        private static string qInsertUpdateValidation = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND App_Source = 'otosales')
                                    BEGIN
	                                    UPDATE dbo.Mst_Customer_Data_Validation SET Status = @2, RelationshipId = @4, PICName = @5,
										CallRejectReasonId = @8, Verified_Date = @7, Verified_By = @3, Created_Date = GETDATE()
	                                    WHERE Cust_Id = @0 AND Column_Name = @1 AND App_Source = 'otosales'
                                    END
                                    ELSE
                                    BEGIN
	                                    INSERT INTO dbo.Mst_Customer_Data_Validation
	                                            ( Cust_Id ,
	                                                Column_Name ,
	                                                App_Source ,
	                                                Status ,
	                                                Verified_By ,
	                                                Verified_Date,
													RelationshipId,
													PICName,
													Created_By,
													Created_Date,
													CallRejectReasonId 
	                                            )
	                                    VALUES  ( @0 , -- Cust_Id - char(11)
	                                                @1 , -- Column_Name - varchar(5)
	                                                'otosales' , -- App_Source - char(10)
	                                                @2 , -- Status - nchar(1)
	                                                @3 , -- Verified_By - char(5)
	                                                @7,  -- Verified_Date - datetime
													@4,
													@5,
													@6,
													GETDATE(),
													@8
	                                            )
                                    END";
        #endregion

        public static List<Otosales.Models.vWeb2.PhoneNoModel> GetPhoneList(string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                Otosales.Models.vWeb2.PhoneNoModel clmPhone = new Otosales.Models.vWeb2.PhoneNoModel();
                string query = @"";
                query = @";EXEC [dbo].[usp_GetPhoneList] @0";
                List<Otosales.Models.vWeb2.PhoneNoModel> ListPhoneNo = db.Fetch<Otosales.Models.vWeb2.PhoneNoModel>(query, OrderNo);
                query = @"SELECT CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal,OldPolicyNo, CustIDAAB, 
                        CAST(COALESCE(isCompany,0) AS BIT) isCompany
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        INNER JOIN dbo.ProspectCustomer pc
                        ON pc.CustID = os.CustID
                        WHERE OrderNo = @0 AND ApplyF  = 1";
                string custid = "";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(query, OrderNo);
                if (ordData.Count > 0)
                {
                    if (ordData.First().IsRenewal)
                    {
                        if (!string.IsNullOrEmpty(ordData.First().OldPolicyNo))
                        {
                            string qgetCustID = @";DECLARE @@CustID VARCHAR(100)
                            DECLARE @@PolicyNo VARCHAR(100)
                            DECLARE @@SegmenID VARCHAR(100)

                            SELECT @@PolicyNo=OldPolicyNo, @@SegmenID = SegmentCode FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0

                            IF(@@SegmenID = 'P3A200' OR @@SegmenID = 'P3A500')
                            BEGIN
	                            SELECT @@CustID=Cust_Id FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            ELSE
                            BEGIN
	                            SELECT @@CustID=Policy_Holder_Code FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            SELECT COALESCE(@@CustID,'')";
                            custid = db.ExecuteScalar<string>(qgetCustID, OrderNo);
                            query = @"SELECT Claimee_Phone, Notes, lp.EntryDt, mcdv.status FROM dbo.Policy p 
                                    INNER JOIN dbo.Loss_Report lp
                                    ON lp.Policy_No = p.Policy_No
						            LEFT JOIN dbo.Mst_Customer_Data_Validation mcdv
						            ON mcdv.Cust_Id = @1 AND Column_Name = 'Claimee_Phone' 
						            WHERE lp.Policy_No = @0 AND @1 NOT IN(
						            SELECT Cust_Id FROM dbo.Mst_Customer_Data_Validation 
						            WHERE Cust_Id = @1 AND Status = 2 AND Column_Name = 'Claimee_Phone')";
                            List<dynamic> listClaimee = db.Fetch<dynamic>(query, ordData.First().OldPolicyNo, custid);
                            List<dynamic> ClaimeePhones = new List<dynamic>();
                            foreach (dynamic c in listClaimee)
                            {
                                string notes = Convert.ToString(c.Notes);
                                dynamic mdl = new ExpandoObject();
                                if (notes.Contains("RelationshipInsured"))
                                {
                                    List<string> noteList = notes.Split(';').ToList();
                                    string relationship;
                                    foreach (string s in noteList)
                                    {
                                        if (SafeTrim(s).StartsWith("RelationshipInsured"))
                                        {
                                            relationship = SafeTrim(s.Split('|').ToList()[1]).ToUpper();
                                            if (relationship.Equals("KELUARGA", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 2;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("PEGAWAI", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 1;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("SUAMI", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 5;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("ISTRI", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 4;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("ANAK", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 3;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("SATU KK", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 6;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("TERTANGGUNG", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 7;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                        }
                                    }
                                }
                            }
                            ClaimeePhones = ClaimeePhones.OrderByDescending(x => x.Priority).ThenByDescending(x => x.EntryDt).ToList();
                            if (ClaimeePhones.Count > 0)
                            {
                                clmPhone.NoHp = ClaimeePhones[0].NoHp;
                                clmPhone.Priority = 0;
                                clmPhone.HPStatus = string.IsNullOrEmpty(listClaimee.First().status) ? "0" : Convert.ToString(listClaimee.First().status);
                                clmPhone.Column_Name = "Claimee_Phone";
                                ListPhoneNo.Add(clmPhone);
                            }
                        }

                    }
                    else
                    {
                        if (ListPhoneNo.Count == 0)
                        {
                            string qGetPhoneOrderNew = @"SELECT COALESCE(pc.Phone1,'') Phone1, Name FROM dbo.ProspectCustomer pc 
                                                    INNER JOIN dbo.OrderSimulation os ON os.CustID = pc.CustID
                                                    WHERE os.OrderNo = @0 AND os.RowStatus = 1 AND ApplyF = 1";
                            List<dynamic> lsPhone = mobiledb.Fetch<dynamic>(qGetPhoneOrderNew, OrderNo);
                            if (lsPhone.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(lsPhone.First().Phone1))
                                {
                                    Otosales.Models.vWeb2.PhoneNoModel mdl = new Models.vWeb2.PhoneNoModel();
                                    if (ordData.First().isCompany)
                                    {
                                        mdl.Column_Name = "MPC1";
                                        mdl.HPStatus = "0";
                                        mdl.NoHp = lsPhone.First().Phone1;
                                        mdl.PICName = lsPhone.First().Name;
                                        mdl.Priority = 0;
                                        mdl.RelationshipId = 0;
                                        ListPhoneNo.Add(mdl);
                                    }
                                    else
                                    {
                                        mdl.Column_Name = "HP";
                                        mdl.HPStatus = "0";
                                        mdl.NoHp = lsPhone.First().Phone1;
                                        mdl.PICName = "";
                                        mdl.Priority = 0;
                                        mdl.RelationshipId = 0;
                                        ListPhoneNo.Add(mdl);
                                    }
                                }
                            }
                        }
                    }
                    string PrefixNo = AppParameterAccess.GetApplicationParametersValue("PREFIX-PHONENO")[0];
                    List<string> LisPrefixNo = PrefixNo.Split(',').ToList();

                    if (ordData.First().isCompany)
                    {
                        #region company
                        foreach (Otosales.Models.vWeb2.PhoneNoModel phoneNo in ListPhoneNo)
                        {
                            phoneNo.Priority = 0;
                            if (SafeTrim(phoneNo.Column_Name).Equals("MPC1"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 7;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("MPC2"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 6;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone1"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 5;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone2"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 4;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Claimee_Phone"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 3;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("AWO HP"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 2;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("AWO Off"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 1;
                            }
                            foreach (string s in LisPrefixNo)
                            {
                                if (SafeTrim(phoneNo.NoHp).StartsWith(s))
                                {
                                    phoneNo.Priority = phoneNo.Priority + 8;
                                }
                            }
                            if (Convert.ToInt32(phoneNo.HPStatus) == 1)
                            {
                                phoneNo.Priority = phoneNo.Priority + 16;
                            }
                            string qGetRelation = @"SELECT COALESCE(RelationshipId,0) RelationshipId, COALESCE(PICName,'') PICName FROM dbo.Mst_Customer_Data_Validation WHERE Column_Name = @1 AND Cust_Id = @0 ORDER BY Created_Date DESC";
                            if (!string.IsNullOrEmpty(custid))
                            {
                                List<dynamic> relations = db.Fetch<dynamic>(qGetRelation, custid, phoneNo.Column_Name);
                                if (relations.Count > 0)
                                {
                                    phoneNo.RelationshipId = relations.First().RelationshipId;
                                    phoneNo.PICName = relations.First().PICName;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region personal
                        foreach (Otosales.Models.vWeb2.PhoneNoModel phoneNo in ListPhoneNo)
                        {
                            phoneNo.Priority = 0;
                            if (SafeTrim(phoneNo.Column_Name).Equals("HP"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 13;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("HP_2"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 12;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("HP_4"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 11;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("HP_3"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 10;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("InsuredPhoneNo"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 9;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("AWO HP"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 8;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Claimee_Phone"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 7;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Home_Phone1"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 6;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone1"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 5;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Home_Phone2"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 4;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone2"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 3;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("AWO Home"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 2;
                            }
                            else if (SafeTrim(phoneNo.Column_Name).Equals("AWO Off"))
                            {
                                phoneNo.Priority = phoneNo.Priority + 1;
                            }
                            foreach (string s in LisPrefixNo)
                            {
                                if (SafeTrim(phoneNo.NoHp).StartsWith(s))
                                {
                                    phoneNo.Priority = phoneNo.Priority + 14;
                                }
                            }
                            if (Convert.ToInt32(phoneNo.HPStatus) == 1)
                            {
                                phoneNo.Priority = phoneNo.Priority + 28;
                            }
                            string qGetRelation = @"SELECT COALESCE(RelationshipId,0) RelationshipId, COALESCE(PICName,'') PICName FROM dbo.Mst_Customer_Data_Validation WHERE Column_Name = @1 AND Cust_Id = @0 ORDER BY Created_Date DESC";
                            if (!string.IsNullOrEmpty(custid))
                            {
                                List<dynamic> relations = db.Fetch<dynamic>(qGetRelation, custid, phoneNo.Column_Name);
                                if (relations.Count > 0)
                                {
                                    phoneNo.RelationshipId = relations.First().RelationshipId;
                                    phoneNo.PICName = relations.First().PICName;
                                }
                            }
                        }
                        #endregion
                    }
                }

                ListPhoneNo = ListPhoneNo.OrderByDescending(x => x.Priority).ToList();
                return ListPhoneNo;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace + "," + e.ToString());
                throw e;
            }
        }

        public static void InsertUpdatePhoneNo(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelectOrd = @"SELECT CAST(COALESCE(isCompany,0) AS BIT) isCompany, os.CustID, COALESCE(pc.CustIDAAB,'') CustIDAAB 
								    FROM dbo.OrderSimulation os
                                    INNER JOIN dbo.ProspectCustomer pc
                                    ON pc.CustID = os.CustID
                                    WHERE OrderNo = @0";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(qSelectOrd, param.OrderNo);
                if (ordData.Count > 0)
                {
                    string qgetCustID = @";DECLARE @@CustID VARCHAR(100)
                            DECLARE @@PolicyNo VARCHAR(100)
                            DECLARE @@SegmenID VARCHAR(100)

                            SELECT @@PolicyNo=OldPolicyNo, @@SegmenID = SegmentCode FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0

                            IF(@@SegmenID = 'P3A200' OR @@SegmenID = 'P3A500')
                            BEGIN
	                            SELECT @@CustID=Cust_Id FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            ELSE
                            BEGIN
	                            SELECT @@CustID=Policy_Holder_Code FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            SELECT COALESCE(@@CustID,'')";
                    string custid = db.ExecuteScalar<string>(qgetCustID, param.OrderNo);
                    if (!string.IsNullOrEmpty(custid))
                    {
                        ordData.First().CustIDAAB = custid;
                    }
                    string qUpdatePhoneABB = @"";

                    if (ordData.First().isCompany)
                    {
                        //param.PhoneNoParam = RemoveDuplicatePhone(param, param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID, ordData.First().isCompany);

                        string qSelectCompanyHP = @"SELECT AdditionalInfoValue NoHP, 'MPC1' ColumnName 
                                                        FROM dbo.Mst_Customer mc
														LEFT JOIN dbo.Mst_Customer_AdditionalInfo mca
														ON mc.Cust_Id = mca.CustID
                                                        WHERE AdditionalCode = 'MPC1' AND mc.Cust_Id = @0
                                                        UNION
                                                        SELECT AdditionalInfoValue NoHP, 'MPC2' ColumnName 
                                                        FROM dbo.Mst_Customer mc
														LEFT JOIN dbo.Mst_Customer_AdditionalInfo mca
														ON mc.Cust_Id = mca.CustID
                                                        WHERE AdditionalCode = 'MPC2' AND mc.Cust_Id = @0";
                        List<dynamic> lsPhoneNo = db.Fetch<dynamic>(qSelectCompanyHP, ordData.First().CustIDAAB);
                        List<string> listAvailColName = new List<string>();
                        foreach (dynamic no in lsPhoneNo)
                        {
                            if (string.IsNullOrEmpty(no.NoHP))
                            {
                                listAvailColName.Add(no.ColumnName);
                            }
                        }
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].IsWrongNumber)
                            {
                                SetWrongNumberCompany(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                            }
                            else if (param.PhoneNoParam[i].IsLandline && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                dynamic dateVerified = null;
                                if (param.PhoneNoParam[i].IsConnected)
                                {
                                    dateVerified = DateTime.Now;
                                }
                                db.Execute(qInsertUpdateValidation
                                    , ordData.First().CustIDAAB
                                    , "Office_Phone1"
                                    , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                    , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    , null
                                    , ""
                                    , param.SalesOfficerID
                                    , dateVerified
                                    , param.PhoneNoParam[i].CallRejectReasonId);
                            }
                            else
                            {
                                if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    UpdatePrimaryCompany(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                                }
                                else
                                {
                                    if (!param.PhoneNoParam[i].IsWrongNumber && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                                    {
                                        if (listAvailColName.Count > 0)
                                        {
                                            db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, listAvailColName[0]);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                                                , ordData.First().CustIDAAB
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , listAvailColName[0]
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , null
                                                , ""
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            if (listAvailColName[0].Contains("MPC1"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedBy = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            }
                                            listAvailColName.RemoveAt(0);
                                        }
                                        else
                                        {
                                            InsertCustHistory(ordData.First().CustIDAAB, "MPC2", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "", "");
                                            db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC2");
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                                                , ordData.First().CustIDAAB
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , "MPC2"
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , null
                                                , ""
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                        }
                                    }
                                    else
                                    {
                                        List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
                                        if (isverified.Count > 0)
                                        {
                                            param.PhoneNoParam[i].IsConnected = true;
                                        }
                                        dynamic dateVerified = null;
                                        if (param.PhoneNoParam[i].IsConnected)
                                        {
                                            dateVerified = DateTime.Now;
                                        }
                                        db.Execute(qInsertUpdateValidation
                                            , ordData.First().CustIDAAB
                                            , param.PhoneNoParam[i].ColumnName
                                            , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                            , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                            , param.PhoneNoParam[i].Relation
                                            , param.PhoneNoParam[i].PICName
                                            , param.SalesOfficerID
                                            , dateVerified
                                            , param.PhoneNoParam[i].CallRejectReasonId);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //param.PhoneNoParam = RemoveDuplicatePhone(param, param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID, ordData.First().isCompany);
                        string qSelectPersonalHP = @"SELECT HP NoHP, 'HP' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0
                                            UNION
                                            SELECT HP_2 NoHP, 'HP_2' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0
                                            UNION
                                            SELECT HP_3 NoHP, 'HP_3' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0
                                            UNION
                                            SELECT HP_4 NoHP, 'HP_4' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0";
                        List<dynamic> lsPhoneNo = db.Fetch<dynamic>(qSelectPersonalHP, ordData.First().CustIDAAB);
                        List<string> listAvailColName = new List<string>();
                        foreach (dynamic no in lsPhoneNo)
                        {
                            if (string.IsNullOrEmpty(no.NoHP))
                            {
                                listAvailColName.Add(no.ColumnName);
                            }
                        }
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].IsWrongNumber)
                            {
                                SetWrongNumberPersonal(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                            }
                            else if (param.PhoneNoParam[i].IsLandline && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                dynamic dateVerified = null;
                                if (param.PhoneNoParam[i].IsConnected)
                                {
                                    dateVerified = DateTime.Now;
                                }
                                db.Execute(qInsertUpdateValidation
                                    , ordData.First().CustIDAAB
                                    , "Home_Phone1"
                                    , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                    , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    , null
                                    , ""
                                    , param.SalesOfficerID
                                    , dateVerified
                                    , param.PhoneNoParam[i].CallRejectReasonId);
                            }
                            else
                            {
                                if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    UpdatePrimaryPersonal(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                                }
                                else
                                {
                                    if (!param.PhoneNoParam[i].IsWrongNumber && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                                    {
                                        if (listAvailColName.Count > 0)
                                        {
                                            qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                            db.Execute(string.Format(qUpdatePhoneABB, listAvailColName[0])
                                                , ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , listAvailColName[0]
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , param.PhoneNoParam[i].Relation
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            if (listAvailColName[0].Equals("HP"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                            }
                                            else if (listAvailColName[0].Equals("HP_2"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone2 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                            }
                                            listAvailColName.RemoveAt(0);
                                        }
                                        else
                                        {
                                            InsertCustHistory(ordData.First().CustIDAAB, "HP_2", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "", "");
                                            qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET HP_2 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                            db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , "HP_2"
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , param.PhoneNoParam[i].Relation
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone2 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                            mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                        }
                                    }
                                    else
                                    {
                                        List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
                                        if (isverified.Count > 0)
                                        {
                                            param.PhoneNoParam[i].IsConnected = true;
                                        }
                                        dynamic dateVerified = null;
                                        if (param.PhoneNoParam[i].IsConnected)
                                        {
                                            dateVerified = DateTime.Now;
                                        }
                                        db.Execute(qInsertUpdateValidation
                                            , ordData.First().CustIDAAB
                                            , param.PhoneNoParam[i].ColumnName
                                            , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                            , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                            , param.PhoneNoParam[i].Relation
                                            , param.PhoneNoParam[i].PICName
                                            , param.SalesOfficerID
                                            , dateVerified
                                            , param.PhoneNoParam[i].CallRejectReasonId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace + "," + e.ToString());
                throw e;
            }
        }

        public static void InsertUpdatePhoneNoOrderNew(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelectOrd = @"SELECT CAST(COALESCE(isCompany,0) AS BIT) isCompany, os.CustID, COALESCE(pc.CustIDAAB,'') CustIDAAB, 
									COALESCE(ProspectID,'') ProspectID, os.SalesOfficerID, LOWER(COALESCE(pc.Name,'')) Name
								    FROM dbo.OrderSimulation os
                                    INNER JOIN dbo.ProspectCustomer pc
                                    ON pc.CustID = os.CustID
                                    WHERE OrderNo = @0";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(qSelectOrd, param.OrderNo);
                if (ordData.Count > 0)
                {
                    string qUpdatePhoneABB = @"";
                    string qUpdatePhoneMobile = @"";

                    if (ordData.First().isCompany)
                    {
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].ColumnName.ToUpper().Equals("MPC1") || string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                if (param.PhoneNoParam[i].IsWrongNumber)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "MPC1", "", ordData.First().SalesOfficerID, "", "");
                                        db.Execute(@"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0", ordData.First().CustIDAAB);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "MPC1", "", ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        db.Execute(@"DELETE FROM dbo.Mst_Prospect_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0", ordData.First().CustIDAAB);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0
                                                        UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, "", ordData.First().SalesOfficerID);
                                }
                                else if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "MPC1", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END", ordData.First().CustIDAAB, param.PhoneNoParam[i].PICName
                                                                                         , ordData.First().SalesOfficerID);
                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "MPC1", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        db.Execute(qUpdatePhoneCompanyProspectABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@"UPDATE dbo.Mst_Prospect_Pic SET Name = @1, updatedt = GETDATE(), updateusr = @2
                                                    WHERE Prospect_Id = @0 AND Name = @3", ordData.First().ProspectID, param.PhoneNoParam[i].PICName
                                                                                             , ordData.First().SalesOfficerID, ordData.First().Name);
                                        }
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0
                                                        UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    mobiledb.Execute(@";UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0"
                                        , ordData.First().CustID, param.PhoneNoParam[i].PICName, ordData.First().SalesOfficerID);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END", ordData.First().CustIDAAB, param.PhoneNoParam[i].PICName
                                                                                         , ordData.First().SalesOfficerID);
                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        db.Execute(qUpdatePhoneCompanyProspectABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@"UPDATE dbo.Mst_Prospect_Pic SET Name = @1, updatedt = GETDATE(), updateusr = @2
                                                    WHERE Prospect_Id = @0 AND Name = @3", ordData.First().ProspectID, param.PhoneNoParam[i].PICName
                                                                                             , ordData.First().SalesOfficerID, ordData.First().Name);
                                        }
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0
                                                        UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    mobiledb.Execute(@";UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0"
                                        , ordData.First().CustID, param.PhoneNoParam[i].PICName, ordData.First().SalesOfficerID);
                                }
                            }

                        }
                    }
                    else
                    {
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].ColumnName.ToUpper().Equals("HP") || string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                if (param.PhoneNoParam[i].IsWrongNumber)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", "", ordData.First().SalesOfficerID, "", "");
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, "", ordData.First().SalesOfficerID);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", "", ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Prospect_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Prospect_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().ProspectID, "", ordData.First().SalesOfficerID);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, "");
                                }
                                else if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Prospect_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Prospect_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Prospect_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Prospect_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace + "," + e.ToString());
                throw e;
            }
        }

        #region private
        public static string SafeTrim(string str)
        {
            return str != null ? str.Trim() : "";
        }
        public static void InsertCustHistory(string CustId, string ColumnName, string PhoneNo, string SalesOfficerID, string Email, string ProspectID)
        {
            try
            {
                string qInsertHistory = @"INSERT INTO dbo.CustomerHistory
                                                    ( CustId ,
                                                      PhoneNumberNew ,
                                                      PhoneNumberOld ,
                                                      CreatedBy ,
                                                      CreatedDate,
													  EmailNew,
													  EmailOld,
                                                      Column_Name
                                                    )
                                            VALUES  ( @0 , -- CustId - char(11)
                                                      @1 , -- PhoneNumberNew - varchar(16)
                                                      @2 , -- PhoneNumberOld - varchar(16)
                                                      @3 , -- CreatedBy - varchar(50)
                                                      GETDATE(),  -- CreatedDate - datetime
													  @5,
													  @6,
                                                      @4
                                                    )";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string oldNumber = "";
                if (ColumnName.Contains("MPC"))
                {
                    if (!string.IsNullOrEmpty(CustId))
                    {
                        oldNumber = db.ExecuteScalar<string>(
            @"SELECT COALESCE(AdditionalInfoValue,'') AdditionalInfoValue 
            FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0"
            , CustId, ColumnName);
                    }
                    else
                    {
                        oldNumber = db.ExecuteScalar<string>(
            @"SELECT COALESCE(AdditionalInfoValue,'') AdditionalInfoValue 
            FROM dbo.Mst_Prospect_AdditionalInfo WHERE AdditionalCode = @1 AND ProspectID = @0"
            , ProspectID, ColumnName);

                    }
                }
                else if (ColumnName.Contains("HP"))
                {
                    if (!string.IsNullOrEmpty(CustId))
                    {
                        oldNumber = db.ExecuteScalar<string>(
                        string.Format(@"SELECT {0} FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0", ColumnName)
                        , CustId);
                    }
                    else
                    {
                        oldNumber = db.ExecuteScalar<string>(
                        string.Format(@"SELECT {0} FROM dbo.Mst_Prospect_Personal WHERE Prospect_Id = @0", ColumnName)
                        , ProspectID);
                    }
                }
                else if (ColumnName.Contains("Office_Phone") || ColumnName.Contains("Home_Phone"))
                {
                    oldNumber = db.ExecuteScalar<string>(
        string.Format(@"SELECT {0} FROM dbo.Mst_Customer WHERE Cust_Id = @0", ColumnName)
        , CustId);
                }
                string oldEmail = "";
                if (string.IsNullOrEmpty(ColumnName))
                {
                    oldEmail = db.ExecuteScalar<string>(@"SELECT COALESCE(Email,'') Email FROM mst_customer where Cust_Id  = @0", CustId);
                }
                else
                {
                    oldEmail = "";
                }
                if (!string.IsNullOrEmpty(oldNumber) || !string.IsNullOrEmpty(oldEmail))
                {
                    db.Execute(qInsertHistory
                       , CustId
                       , PhoneNo
                       , oldNumber
                       , SalesOfficerID
                       , ColumnName
                       , Email
                       , oldEmail);
                }

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void UpdatePrimaryCompany(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                InsertCustHistory(CustIDAAB, "MPC1", param.PhoneNo, SalesOfficerID, "", "");
                db.Execute(qUpdatePhoneCompanyABB, CustIDAAB, param.PhoneNo, "MPC1");
                List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", CustIDAAB, param.ColumnName);
                if (isverified.Count > 0)
                {
                    param.IsConnected = true;
                }
                dynamic dateVerified = null;
                if (param.IsConnected)
                {
                    dateVerified = DateTime.Now;
                }
                db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                    , CustIDAAB
                    , param.PICName
                    , SalesOfficerID);
                mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                    , param.PICName
                    , SalesOfficerID);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , param.ColumnName
                    , 2
                    , param.IsConnected ? SalesOfficerID : ""
                    , null
                    , ""
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , "MPC1"
                    , param.IsConnected ? 1 : 0
                    , param.IsConnected ? SalesOfficerID : ""
                    , null
                    , ""
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);
                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                mobiledb.Execute(qUpdateHpAABMobile, CustID, param.PhoneNo, SalesOfficerID);
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, "MPC2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void SetWrongNumberCompany(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC1") || param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                if (!string.IsNullOrEmpty(param.ColumnName))
                {
                    if (!string.IsNullOrEmpty(param.PICName))
                    {
                        db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                            , CustIDAAB
                            , param.PICName
                            , SalesOfficerID);
                        mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                            , param.PICName
                            , SalesOfficerID);
                    }
                    dynamic dateVerified = null;
                    if (param.IsConnected)
                    {
                        dateVerified = DateTime.Now;
                    }
                    db.Execute(qInsertUpdateValidation
                        , CustIDAAB
                        , param.ColumnName
                        , 2
                        , param.IsConnected ? SalesOfficerID : ""
                        , null
                        , ""
                        , SalesOfficerID
                        , dateVerified
                        , param.CallRejectReasonId);
                    if (param.ColumnName.Contains("MPC1"))
                    {
                        string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                        mobiledb.Execute(qUpdateHpAABMobile, CustID, SalesOfficerID);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void UpdatePrimaryPersonal(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string qUpdatePhoneABB = "";
                InsertCustHistory(CustIDAAB, "HP", param.PhoneNo, SalesOfficerID, "", "");
                qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                db.Execute(qUpdatePhoneABB, CustIDAAB, param.PhoneNo, SalesOfficerID);
                List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", CustIDAAB, param.ColumnName);
                if (isverified.Count > 0)
                {
                    param.IsConnected = true;
                }
                dynamic dateVerified = null;
                if (param.IsConnected)
                {
                    dateVerified = DateTime.Now;
                }

                db.Execute(qInsertUpdateValidation
                , CustIDAAB
                , param.ColumnName
                , 2
                , param.IsConnected ? SalesOfficerID : ""
                , null
                , ""
                , SalesOfficerID
                , dateVerified
                , param.CallRejectReasonId);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , "HP"
                    , param.IsConnected ? 1 : 0
                    , param.IsConnected ? SalesOfficerID : ""
                    , param.Relation
                    , param.PICName
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);
                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                mobiledb.Execute(qUpdateHpAABMobile, CustID, param.PhoneNo);
                if (param.ColumnName.Contains("HP_"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(string.Format(qUpdatePhoneABB, param.ColumnName)
                        , CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void SetWrongNumberPersonal(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC1") || param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                if (!string.IsNullOrEmpty(param.ColumnName))
                {
                    if (!string.IsNullOrEmpty(param.PICName))
                    {
                        db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                            , CustIDAAB
                            , param.PICName
                            , SalesOfficerID);
                        mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                            , param.PICName
                            , SalesOfficerID);
                    }
                    dynamic dateVerified = null;
                    if (param.IsConnected)
                    {
                        dateVerified = DateTime.Now;
                    }
                    db.Execute(qInsertUpdateValidation
                        , CustIDAAB
                        , param.ColumnName
                        , 2
                        , param.IsConnected ? SalesOfficerID : ""
                        , null
                        , ""
                        , SalesOfficerID
                        , dateVerified
                        , param.CallRejectReasonId);
                    if (param.ColumnName.Contains("MPC1"))
                    {
                        string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                        mobiledb.Execute(qUpdateHpAABMobile, CustID, SalesOfficerID);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        #endregion
    }
}