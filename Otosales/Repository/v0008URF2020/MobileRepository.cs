﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.dta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Repository.v0008URF2020
{
    public class MobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();
        public static void GenerateOrderSimulation(string salesOfficerID, string custID, string followUpID, string orderID, string PolicyNo, string remarkToSA, int isPSTB)
        {
            try
            {
                //Param
                string query = "";
                string SOBranchID = "";

                //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                query = @"SELECT TOP 1 BranchCode FROM SalesOfficer WHERE SalesOfficerID = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    SOBranchID = db.FirstOrDefault<String>(query, salesOfficerID);
                }

                //Data
                Otosales.Models.vWeb2.CustomerInfo custInfo = new Otosales.Models.vWeb2.CustomerInfo();
                Models.vWeb2.OrderSimulationRenot orderSimulation = new Models.vWeb2.OrderSimulationRenot();
                Models.vWeb2.OrderSimulationMVRenot orderSimulationMV = new Models.vWeb2.OrderSimulationMVRenot();
                List<Models.vWeb2.OrderSimulationInterestRenot> listOSInterest = new List<Models.vWeb2.OrderSimulationInterestRenot>();
                List<Models.vWeb2.OrderSimulationCoverageRenot> listOSCoverage = new List<Models.vWeb2.OrderSimulationCoverageRenot>();

                #region Query Prospect Customer
                query = @"SELECT mc.Name AS Name,
		    mc.Email AS Email1,
		    mcp.HP AS Phone1,
		    mcp.HP_2 AS Phone2,
		    mc.Cust_Id AS CustIDAAB,
		    CASE WHEN mpr.product_type = 6 
		    THEN 
		    CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		    ELSE rn.broker_code
		    END
		    ELSE
            CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		    END AS SalesOfficerID,
		    rn.Salesman_Dealer_Code AS SalesDealer,
		    rn.Dealer_Code AS DealerCode,
		    rn.Branch_distribution AS BranchCode,
		    CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany, 
		    isnull(mcp.id_card,'') AS IdentityNo,
		    CASE WHEN isnull(mcp.sex , '') = '' THEN '' 
		    WHEN isnull(mcp.sex , '') = '1' THEN 'M'
		    ELSE 'F' END AS Gender,
		    mp.Prospect_Id AS ProspectID,
		    mc.Birth_Date AS BirthDate,
		    CASE WHEN mc.Cust_Type = 2 THEN mc.Office_Address ELSE mc.Home_Address END AS [Address],
		    CASE WHEN mc.Cust_Type = 2 THEN mc.Office_PostCode ELSE mc.Home_PostCode END AS PostCode,
		    isnull(mca.AdditionalInfoValue,'') AS CompanyNPWPAddress,
		    isnull(mca2.AdditionalInfoValue,'') AS CompanyNPWPNo,
		    CASE WHEN mca3.AdditionalInfoValue is not null AND mca3.AdditionalInfoValue <> '' 
            THEN SUBSTRING(mca3.AdditionalInfoValue, 7, 4)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 4, 2)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 1, 2) 
            ELSE NULL END  AS CompanyNPWPDate,
			CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mca4.AdditionalInfoValue,'') 
			ELSE '' END AS PICPhone,
			CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mcpi.Name,'') 
			ELSE '' END AS PIC,
            a.order_no AS OrderNo
    FROM Policy AS a WITH(NOLOCK)
    INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
    INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
    LEFT JOIN mst_prospect mp WITH ( NOLOCK ) ON mp.cust_id = a.policy_holder_code
    LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 	
    LEFT JOIN mst_cust_personal mcp WITH ( NOLOCK ) ON mcp.cust_id = a.policy_holder_code
    inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.product_code = rn.new_product_code
    left join Mst_Customer_AdditionalInfo mca WITH ( NOLOCK ) ON mca.custid = a.policy_holder_code 
    AND mca.AdditionalCode IN ('NPAD')
    left join Mst_Customer_AdditionalInfo mca2 WITH ( NOLOCK ) ON mca2.custid = a.policy_holder_code 
    AND mca2.AdditionalCode IN ('NPWN')
    left join Mst_Customer_AdditionalInfo mca3 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca3.AdditionalCode IN ('NPDT')
    left join Mst_Customer_AdditionalInfo mca4 WITH ( NOLOCK ) ON mca4.custid = a.policy_holder_code 
    AND mca4.AdditionalCode IN ('MPC1')
	left join Mst_cust_pic mcpi WITH ( NOLOCK ) ON mcpi.cust_id = a.policy_holder_code
    WHERE a.policy_no = @0
		    AND a.Status = 'A'
            AND a.ORDER_STATUS IN ( '11','9' )
            AND a.RENEWAL_STATUS = 0
            AND rn.Status IN ('0', '1' )
            AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    custInfo = db.FirstOrDefault<Otosales.Models.vWeb2.CustomerInfo>(query, PolicyNo);
                }

                query = @"SELECT mc.Name AS Name,
        mc.Email AS Email1,
        mcp.HP AS Phone1,
		mcp.HP_2 AS Phone2,
        mc.Cust_Id AS CustIDAAB,
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END  AS SalesOfficerID,
        a.Broker_Code AS DealerCode,
        mc2.Cust_Id AS SalesDealer,
        COALESCE(rm.new_branch_id, a.branch_id) AS BranchCode,
        CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany,
        isnull(mcp.id_card, '') AS IdentityNo,
        CASE WHEN isnull(mcp.sex, '') = '' THEN ''
        WHEN isnull(mcp.sex, '') = '1' THEN 'M'
        ELSE 'F' END AS Gender,
		mc.Prospect_Id AS ProspectID,
		mc.Birth_Date AS BirthDate,
		mc.Home_Address AS [Address],
		mc.Home_PostCode AS PostCode,
		isnull(mca.AdditionalInfoValue,'') AS CompanyNPWPAddress,
		isnull(mca2.AdditionalInfoValue,'') AS CompanyNPWPNo,
		CASE WHEN mca3.AdditionalInfoValue is not null AND mca3.AdditionalInfoValue <> ''
        THEN SUBSTRING(mca3.AdditionalInfoValue, 7, 4)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 4, 2)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 1, 2) 
        ELSE NULL END  AS CompanyNPWPDate,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mca4.AdditionalInfoValue,'') 
		ELSE '' END AS PICPhone,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mcpi.Name,'') 
		ELSE '' END AS PIC
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc WITH(NOLOCK) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_cust_personal mcp WITH ( NOLOCK ) ON mcp.cust_id = a.policy_holder_code
INNER JOIN mst_product mp ON mp.product_code = a.product_code
LEFT JOIN mst_salesman ms WITH(NOLOCK) ON ms.salesman_id = a.Salesman_Id
LEFT JOIN dbo.ComSales_Salesman AS css ON a.Broker_Code = css.Salesman_Code
LEFT JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
LEFT OUTER JOIN renewal_map rm ON mp.product_code = rm.product_code
                                                          AND rm.status = 1
                                                          AND ( rm.old_branch_id = ''
                                                              OR rm.old_branch_id = a.branch_id
                                                              )
                                                          AND ( rm.old_segment_code = ''
                                                              OR rm.old_segment_code = a.segment_code
                                                              )

    left join Mst_Customer_AdditionalInfo mca WITH ( NOLOCK ) ON mca.custid = a.policy_holder_code 
    AND mca.AdditionalCode IN ('NPAD')
    left join Mst_Customer_AdditionalInfo mca2 WITH ( NOLOCK ) ON mca2.custid = a.policy_holder_code 
    AND mca2.AdditionalCode IN ('NPWN')
    left join Mst_Customer_AdditionalInfo mca3 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca3.AdditionalCode IN ('NPDT')
    left join Mst_Customer_AdditionalInfo mca4 WITH ( NOLOCK ) ON mca4.custid = a.policy_holder_code 
    AND mca4.AdditionalCode IN ('MPC1')
	left join Mst_cust_pic mcpi WITH ( NOLOCK ) ON mcpi.cust_id = a.policy_holder_code
WHERE a.policy_no = @0
AND a.Status = 'A'
AND a.ORDER_STATUS IN ( '11','9' )
AND a.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                #endregion

                if (custInfo == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        custInfo = db.FirstOrDefault<Otosales.Models.vWeb2.CustomerInfo>(query, PolicyNo);
                    }
                }


                #region Sales Dealer dan Dealer Code
                int SalesmanCode = 0;
                int DealerCode = 0;
                int salesmancode = 0;
                int dealercode = 0;
                string querySalesman = "SELECT SalesmanCode FROM dbo.SalesmanDealer WHERE CustID = @0";
                string queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE CustID = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    if (custInfo.SalesDealer != null)
                        salesmancode = db.First<int>(querySalesman, custInfo.SalesDealer);
                    else
                        salesmancode = 0;
                    int value = 0;
                    if (custInfo.DealerCode != null && int.TryParse(custInfo.DealerCode, out value))
                        dealercode = db.First<int>(queryDealer, custInfo.DealerCode);
                    else
                        dealercode = 0;
                }

                SalesmanCode = Convert.ToInt32(salesmancode);
                DealerCode = Convert.ToInt32(dealercode);

                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {

                    #region Query Insert Prospect Customer
                    query = @"
INSERT INTO ProspectCustomer([CustID]
      , [Name] --*
      , [Phone1] --*
      , [Phone2]
      , [Email1] --*
      , [Email2]
      , [CustIDAAB] --*
      , [SalesOfficerID] --*
      , [DealerCode] --*
      , [SalesDealer] --*
      , [BranchCode] --*
      , [isCompany] --*
      , [EntryDate]
      , [LastUpdatedTime]
      , [RowStatus]
      , [IdentityNo]
      , [CustGender]      
      , [ProspectID]
      , [CustBirthDay]
      , [CustAddress]
      , [PostalCode])
      SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1,@12,@13,@14,@15,@16,@17";
                    #endregion

                    //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                    db.Execute(query, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2, custInfo.Email1
                        , "", custInfo.CustIDAAB, salesOfficerID, DealerCode, SalesmanCode, SOBranchID
                        , custInfo.isCompany, custInfo.IdentityNo, custInfo.Gender, custInfo.ProspectID, custInfo.BirthDate
                        , custInfo.Address, custInfo.PostCode);


                    #region Query Insert Prospect Company
                    query = @"
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName] --*
      ,[FollowUpNo] --*
      ,[Email] --*
      ,[ModifiedDate]
      ,[RowStatus]
      ,[NPWPno]
      ,[NPWPdate]
      ,[NPWPaddress]
      ,[OfficeAddress]
      ,[PostalCode]
      ,[PICPhoneNo]
      ,[PICname]) VALUES(@0,@1,@2,@3,GETDATE(),1,@4,@5,@6,@7,@8,@9,@10)";
                    #endregion

                    if (custInfo.isCompany == 1)
                        db.Execute(query, custID, custInfo.Name, followUpID, custInfo.Email1
                            , custInfo.CompanyNPWPNo, custInfo.CompanyNPWPDate, custInfo.CompanyNPWPAddress, custInfo.Address, custInfo.PostCode
                            , custInfo.PICPhone, custInfo.PIC);


                    #region Query Insert Follow Up
                    query = @"
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[RemarkToSA]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsRenewal]) 
SELECT @0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,1";
                    #endregion

                    //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                    db.Execute(query, followUpID, 0, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2
                        , salesOfficerID, custInfo.Name, 1, 1, remarkToSA, SOBranchID);

                }

                #region Query Order Simulation
                //Update 105 Hotfix - ganti period policy ke range 1 tahun
                query = @"SELECT 
		NULL AS NewPolicyId,
		rn.New_Policy_No AS NewPolicyNo,
		rn.VANumber AS VANumber,
		'' AS QuotationNo,
		0 AS MultiYearF,
		1 AS YearCoverage,
		CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
		CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
		rn.Branch_distribution AS BranchCode,
		CASE WHEN mpr.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		END AS SalesOfficerID,
		'' AS PhoneSales,
		rn.Dealer_Code AS DealerCode,
		rn.Salesman_Dealer_Code AS SalesDealer,
		'GARDAOTO' AS ProductTypeCode,
		rp.Policy_Fee AS AdminFee,
		rp.gross_premium AS TotalPremium,
		mc.Home_Phone1 AS Phone1,
		mc.Home_Phone2 AS Phone2,
		mc.Email AS Email1,
		'' AS Email2,
		0 AS SendStatus,
		CASE WHEN mp.cob_id = '404' THEN 2 
		WHEN PDT.product_code is not null THEN 3 
        WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
        ELSE 1 END AS InsuranceType,
		1 AS ApplyF,
		0 AS SendF,
		(SELECT top 1 Interest_no from Ren_Dtl_Interest where policy_no = @0) AS LastInterestNo,
		(SELECT top 1 coverage_no from Ren_Dtl_Coverage where policy_no = @0) AS LastCoverageNo,
		rn.New_Product_code AS ProductCode,
        rn.Due_Date AS PeriodFrom,
        DATEADD(YEAR, 1,rn.Due_Date) AS PeriodTo,
        rn.segment_code AS SegmentCode,
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 
LEFT JOIN Ren_Dtl_Coverage rdc1 WITH ( NOLOCK ) ON  rdc1.policy_no = rn.policy_no
AND rdc1.interest_id = 'CASCO' AND rdc1.coverage_id = 'TLO'
LEFT JOIN Ren_Dtl_Coverage rdc2 WITH ( NOLOCK ) ON  rdc2.policy_no = rn.policy_no
AND rdc2.interest_id = 'CASCO' AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN Ren_premium rp WITH ( NOLOCK ) ON  rp.policy_no = rn.policy_no
INNER JOIN mst_product mp on mp.product_code = rn.new_product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.product_code = rn.new_product_code
left join dtl_address da on da.policy_id = a.policy_id AND da.address_type ='DELIVR'
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo);
                }

                query = @"SELECT TOP 1
NULL AS NewPolicyId,
@1 AS NewPolicyNo,
@2 AS VANumber,
'' AS QuotationNo,
0 AS MultiYearF,
1 AS YearCoverage,
CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
a.Branch_Id AS BranchCode,
CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END  AS SalesOfficerID,
'' AS PhoneSales,
a.Broker_Code AS DealerCode,
        mc.Cust_Id AS SalesDealer,
'GARDAOTO' AS ProductTypeCode,
rp.Policy_Fee AS AdminFee,
rp.gross_premium AS TotalPremium,
mc.Home_Phone1 AS Phone1,
mc.Home_Phone2 AS Phone2,
mc.Email AS Email1,
'' AS Email2,
0 AS SendStatus,
CASE WHEN mp.cob_id = '404' THEN 2 
WHEN PDT.product_code is not null THEN 3 
WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
ELSE 1 END AS InsuranceType,
1 AS ApplyF,
0 AS SendF,
(SELECT max(Interest_no) from Dtl_Interest where policy_id = a.policy_id) AS LastInterestNo,
(SELECT max(coverage_no) from Dtl_Coverage where policy_id = a.policy_id) AS LastCoverageNo,
a.Product_Code [ProductCode],
        a.Period_To AS PeriodFrom,
        DATEADD(YEAR,1,a.Period_To) AS PeriodTo,
a.Segment_Code [SegmentCode],
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc  ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms ON ms.salesman_id = a.Salesman_Id 
LEFT JOIN dbo.ComSales_Salesman AS css ON a.Broker_Code = css.Salesman_Code
LEFT JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
LEFT JOIN dtl_interest di ON di.policy_id = a.policy_id AND di.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc1 ON  rdc1.policy_id = a.policy_id 
AND rdc1.interest_no = di.interest_no AND rdc1.coverage_id = 'TLO'
LEFT JOIN dtl_interest di2 ON di2.policy_id = a.policy_id AND di2.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc2 ON  rdc2.policy_id = a.policy_id
AND rdc2.interest_no = di2.interest_no AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN premium rp ON  rp.policy_id = a.policy_id
INNER JOIN mst_product mp on mp.product_code = a.product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
left join dtl_address da on da.policy_id = a.policy_id AND da. address_type ='DELIVR'
WHERE a.policy_no = @0
AND a.Status = 'A'
AND a.ORDER_STATUS IN ( '11','9' )
AND a.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";

                #endregion

                if (orderSimulation == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        string VANumber = "", NewPolicyNo = "";
                        List<dynamic> listRen = db.Fetch<dynamic>(@"SELECT VANumber, Policy_No FROM dbo.Renewal_Notice WHERE Policy_No = @0 AND Status IN ('0', '1' )", PolicyNo);
                        if (listRen.Count > 0)
                        {
                            VANumber = listRen.First().VANumber;
                            NewPolicyNo = listRen.First().Policy_No;
                        }
                        else {
                            NewPolicyNo = Otosales.Logic.CommonLogic.GetPolicyNo();
                        }
                        orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo, NewPolicyNo, VANumber);
                    }
                }


                #region Query Insert Order Simulation
                query = @"
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[OldPolicyNo]
      ,[PolicyNo]
      ,[PolicyID]
      ,[VANumber]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[ProductCode]
      ,[SegmentCode]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
	  ,IsPolicyIssuedBeforePaying
        ) 
  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10, @11,@12,
        @13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    orderSimulation.SalesOfficerID = salesOfficerID;
                    //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                    db.Execute(query, orderID, custID, followUpID, orderSimulation.QuotationNo, orderSimulation.MultiYearF
                        , orderSimulation.YearCoverage, orderSimulation.TLOPeriod, orderSimulation.ComprePeriod, SOBranchID
                        , salesOfficerID, orderSimulation.PhoneSales, DealerCode, SalesmanCode
                        , orderSimulation.ProductTypeCode, orderSimulation.AdminFee, orderSimulation.TotalPremium, orderSimulation.Phone1
                        , orderSimulation.Phone2, orderSimulation.Email1, orderSimulation.Email2, orderSimulation.SendStatus
                        , orderSimulation.InsuranceType, orderSimulation.ApplyF, orderSimulation.SendF, orderSimulation.LastInterestNo
                        , orderSimulation.LastCoverageNo, PolicyNo, orderSimulation.NewPolicyNo, orderSimulation.NewPolicyId
                        , orderSimulation.VANumber, orderSimulation.PeriodFrom, orderSimulation.PeriodTo, orderSimulation.ProductCode
                        , orderSimulation.SegmentCode, orderSimulation.PolicyDeliverAddress, orderSimulation.PolicyDeliverPostalCode
                        , orderSimulation.PolicyDeliveryType, orderSimulation.PolicyDeliveryName
                        , isPSTB == 1 ? true : false);
                }

                #region Query Order Simulation MV
                query = @"SELECT rdm.Object_No AS ObjectNo,
		'GARDAOTO' AS ProductTypeCode,
		rdm.Vehicle_Code AS VehicleCode,
		rdm.Brand_Code AS BrandCode,
		rdm.Model_Code AS ModelCode,
		rdm.Series  AS Series,
		rdm.Vehicle_Type AS [Type],
		rdm.Sitting_Capacity AS Sitting,
		rdm.Mfg_Year AS [Year],
		rdm.Geo_Area_Code AS CityCode,
		rdm.Usage_Code AS UsageCode,
		rdi.Sum_Insured AS SumInsured,
		ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Ren_Dtl_Non_Standard_Accessories] where policy_no = @0),0) AS AccessSI,
		rdm.Registration_Number AS RegistrationNumber,
		rdm.Engine_Number AS EngineNumber,
		rdm.Chasis_Number AS ChasisNumber,
		rdm.New_Car AS IsNew,
        rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Interest rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.policy_no 
AND rdi.Object_no = rdm.object_no
AND rdi.interest_id = 'CASCO'
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);
                }

                query = @"SELECT rdm.Object_No AS ObjectNo,
'GARDAOTO' AS ProductTypeCode,
rdm.Vehicle_Code AS VehicleCode,
rdm.Brand_Code AS BrandCode,
rdm.Model_Code AS ModelCode,
rdm.Series  AS Series,
rdm.Vehicle_Type AS [Type],
rdm.Sitting_Capacity AS Sitting,
rdm.Mfg_Year AS [Year],
rdm.Geo_Area_Code AS CityCode,
rdm.Usage_Code AS UsageCode,
rdm.Market_Price AS SumInsured,
ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Dtl_Non_Standard_Accessories] where policy_id = a.policy_id),0) AS AccessSI,
rdm.Registration_Number AS RegistrationNumber,
rdm.Engine_Number AS EngineNumber,
rdm.Chasis_Number AS ChasisNumber,
rdm.New_Car AS IsNew,
rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'
AND a.ORDER_STATUS IN ( '11','9' )
AND a.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                #endregion

                if (orderSimulationMV == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);
                    }
                }


                #region Query Insert Order Simulation MV
                query = @"DELETE FROM OrdersimulationMV WHERE OrderNo=@0
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]
      ,[ColorOnBPKB]) 
  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17,@18";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, orderID, orderSimulationMV.ObjectNo, orderSimulationMV.ProductTypeCode
                        , orderSimulationMV.VehicleCode, orderSimulationMV.BrandCode, orderSimulationMV.ModelCode
                        , orderSimulationMV.Series, orderSimulationMV.Type, orderSimulationMV.Sitting
                        , orderSimulationMV.Year, orderSimulationMV.CityCode, orderSimulationMV.UsageCode
                        , orderSimulationMV.SumInsured, orderSimulationMV.AccessSI, orderSimulationMV.RegistrationNumber
                        , orderSimulationMV.EngineNumber, orderSimulationMV.ChasisNumber, orderSimulationMV.IsNew, orderSimulationMV.Color);
                }

                #region Query Order Simulation Interest
                //Update 105 Hotfix - ganti period policy ke range 1 tahun
                query = @"SELECT rdi.Object_No AS ObjectNo,
		rdi.Interest_No AS InterestNo,
		rdi.Interest_Id AS InterestID,
		1 AS [Year],
		(SELECT SUM(rdc.net_premium) FROM [dbo].[Ren_Dtl_Coverage] rdc WHERE rdc.policy_no = @0 
		and rdc.interest_no = rdi.Interest_No) AS Premium,
        rn.Due_Date AS PeriodFrom,
        DATEADD(YEAR,1,rn.Due_Date) AS PeriodTo,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND a.ORDER_STATUS IN ( '11','9' )
        AND a.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    listOSInterest = db.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(query, PolicyNo);
                }

                query = @"SELECT rdi.Object_No AS ObjectNo,
rdi.Interest_No AS InterestNo,
rdi.Interest_Id AS InterestID,
1 AS [Year],
(SELECT SUM(rdc.net_premium) FROM [dbo].[Dtl_Coverage] rdc WHERE rdc.policy_id = a.policy_id 
and rdc.interest_no = rdi.Interest_No) AS Premium,
        a.Period_To AS PeriodFrom,
        DATEADD(YEAR,1,a.Period_To) AS PeriodTo,
rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a 
INNER JOIN [Dtl_Interest] rdi ON rdi.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'
AND A.ORDER_STATUS IN ( '11','9' )
AND A.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                #endregion

                if (listOSInterest.Count == 0)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        listOSInterest = db.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(query, PolicyNo);
                    }
                }

                #region Insert Order Simulation Interest
                query = @"
DELETE FROM OrderSimulationInterest WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2
INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1,@8";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationInterestRenot osI in listOSInterest)
                    {
                        db.Execute(query, orderID, osI.ObjectNo, osI.InterestNo, osI.InterestID, osI.Year
                            , osI.Premium, osI.PeriodFrom, osI.PeriodTo, osI.DeductibleCode);
                    }
                }

                #region Query Order Simulation Coverage
                //Update 105 Hotfix - ganti period policy ke range 1 tahun
                query = @"SELECT rdc.Object_No AS ObjectNo,
		rdc.Interest_No AS InterestNo,
		rdc.Coverage_no AS CoverageNo,
		rdc.Coverage_ID AS CoverageID,
		rdc.Rate AS Rate,
		rdi.sum_insured AS SumInsured,
		rdc.Loading AS LoadingRate,
		(rdc.Loading*rdc.net_premium)/100 AS Loading,
		rdc.net_premium AS Premium,
		rn.Due_Date AS BeginDate,
		DATEADD(YEAR, 1,rn.Due_Date) AS EndDate,
		rdc.IsBundlingF AS IsBundling,
		100 AS EntryPct,
		DATEDIFF(day, rn.Due_Date, DATEADD(YEAR, 1,rn.Due_Date)) AS Ndays,
		rdc.Excess_Rate AS ExcessRate,
		CASE WHEN DATEDIFF(MONTH, rn.Due_Date, DATEADD(YEAR, 1,rn.Due_Date)) < 12 
		THEN 1 ELSE 0 END CalcMethod,
		rdc.Cover_Premium AS CoverPremium,
		rdc.Gross_Premium AS GrossPremium,
		rdc.Max_SI AS MaxSI,
		rdc.Net1 AS Net1,
		rdc.Net2 AS Net2,
		rdc.Net3 AS Net3,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Coverage] rdc WITH ( NOLOCK ) ON rdc.policy_no = rn.Policy_No AND rdi.interest_no = rdc.interest_no
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    listOSCoverage = db.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(query, PolicyNo);
                }

                query = @"SELECT rdc.Object_No AS ObjectNo,
rdc.Interest_No AS InterestNo,
rdc.Coverage_no AS CoverageNo,
rdc.Coverage_Id AS CoverageID,
rdc.Rate AS Rate,
rdc.Sum_Insured  AS SumInsured,
rdc.Loading AS LoadingRate,
(rdc.Loading*rdc.net_premium)/100 AS Loading,
rdc.net_premium AS Premium,
        a.Period_To AS BeginDate,
        DATEADD(YEAR,1,a.Period_To) AS EndDate,
CASE WHEN rdc.net_premium = 0 OR rdc.net_premium is null THEN 1 ELSE 0 END AS IsBundling,
rdc.Entry_Pct [EntryPct],
rdc.NDays [NDays],
Net1,
Net2,
Net3,
Gross_Premium [GrossPremium],
Cover_Premium [CoverPremium],
rdc.Calc_Method [CalcMethod],
Excess_Rate [ExcessRate],
Max_si [MaxSi],
Deductible_Code [DeductibleCode]
FROM Policy AS a 
INNER JOIN [Dtl_Coverage] rdc ON rdc.Policy_id = a.Policy_id
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.Policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if (listOSCoverage.Count == 0)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        listOSCoverage = db.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(query, PolicyNo);
                        foreach (Models.vWeb2.OrderSimulationCoverageRenot osC in listOSCoverage)
                        {
                            int IsBundling = 0;
                            string interestCode = "";
                            interestCode = db.ExecuteScalar<string>(@"DECLARE @@PolicyID VARCHAR(20)
SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
DECLARE @@InterestID VARCHAR(10) = ''
SELECT @@InterestID = COALESCE(Interest_Id,'') FROM dbo.Dtl_Interest WHERE Policy_Id = @@PolicyID AND Interest_No = @1
IF (@@InterestID <> '' AND @@InterestID IS NOT NULL)
BEGIN 
	SELECT @@InterestID
END
ELSE 
BEGIN
	SELECT @@InterestID = ''
END", PolicyNo, osC.InterestNo);
                            if (interestCode.Contains("CASCO") || interestCode.Contains("ACCESS"))
                            {
                                string squery = @"SELECT a.Coverage_Id, a.Scoring_Code, b.Auto_Apply, 1 Year  FROM 
                                         Prd_Interest_Coverage a INNER JOIN B2B_Interest_Coverage b ON 
                                         a.Coverage_Id=b.Coverage_Id AND a.Interest_ID=b.Interest_Id AND 
                                         a.Product_Code=b.Product_Code WHERE a.Product_Code=@0
										 AND a.Coverage_Id = @1 
										 AND b.Vehicle_Type='ALL' 
										 AND b.Interest_Id='CASCO'";
                                List<dynamic> ListTmp = db.Fetch<dynamic>(squery, orderSimulation.ProductCode, osC.CoverageID);
                                if (ListTmp.Count > 0)
                                {
                                    int AutoApply = Convert.ToInt32(ListTmp.First().Auto_Apply);
                                    if (AutoApply > 0)
                                    {
                                        IsBundling = 1;
                                    }
                                }
                            }
                            osC.IsBundling = IsBundling;
                        }
                    }
                }


                #region Insert Order Simulation Coverage
                query = @"DELETE FROM OrderSimulationCoverage WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2 AND CoverageNo = @3
    INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@22,@23,GETDATE(),1,@10,
            @11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationCoverageRenot osC in listOSCoverage)
                    {
                        db.Execute(query, orderID, osC.ObjectNo, osC.InterestNo, osC.CoverageNo
                            , osC.CoverageID, osC.Rate, osC.SumInsured, osC.LoadingRate, osC.Loading
                            , osC.Premium, osC.IsBundling, osC.EntryPct, osC.NDays, osC.ExcessRate
                            , osC.CalcMethod, osC.CoverPremium, osC.GrossPremium, osC.MaxSi
                            , osC.Net1, osC.Net2, osC.Net3, osC.DeductibleCode, osC.BeginDate, osC.EndDate);
                    }
                }

                query = @"DELETE FROM [dbo].[ImageData] WHERE DeviceID = @0
                INSERT INTO [dbo].[ImageData]
                ([ImageID],[DeviceID],[SalesOfficerID],[PathFile]
                ,[EntryDate],[LastUpdatedTime],[RowStatus],[Data]
                ,[ThumbnailData],[CoreImage_ID])
                SELECT tid.ImageID,tid.FollowUpNo,tid.SalesOfficerID,tid.PathFile,
                GETDATE(),GETDATE(),1,tid.Data,tid.ThumbnailData,NULL
                FROM dbo.TempImageData tid
                WHERE tid.FollowUpNo = @0";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, followUpID);
                }

                /*FORMRENEWAL
                RENEWALNOTICE
                BUKTIBAYAR
                KTP
                STNK
                SURVEY
                DOCNSA1
                DOCNSA2*/

                Otosales.Repository.v0213URF2019.MobileRepository.UpdateImageFollowUp(followUpID);

                query = @"UPDATE FollowUp
                          SET [FollowUpStatus] = 2
                          ,[FollowUpInfo] = 61
                           WHERE FollowUpNo = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, followUpID);
                }


                #region Update Status Exclude Renewal Policy
                query = @"UPDATE [Excluded_Renewal_Policy] SET IsSendToSA=1 WHERE Policy_No=@0";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    db.Execute(query, PolicyNo);
                }

            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static Otosales.Models.vWeb2.TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<dynamic> listDyn = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
            string query = "";

            try
            {
                Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
                query = @";DECLARE @@Admin DECIMAL
                        --DECLARE @@GrossPremium DECIMAL
                        --DECLARE @@NetPremi DECIMAL
                        DECLARE @@OrderNo VARCHAR(100)
                        DECLARE @@PolicyNo VARCHAR(100)
                        DECLARE @@PolicyOrderNo VARCHAR(100)
                        DECLARE @@SurveyNo VARCHAR(100)
                        DECLARE @@OldPolicyNo VARCHAR(100)

                        SELECT @@Admin=AdminFee,@@OrderNo=OrderNo, @@PolicyNo=PolicyNo, @@PolicyOrderNo=PolicyOrderNo, 
                        @@SurveyNo=SurveyNo, @@OldPolicyNo=OldPolicyNo
                        FROM dbo.OrderSimulation 
                        WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1
                        --SELECT @@GrossPremium=SUM(Gross_Premium) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @@OrderNo AND RowStatus = 1 
                        --SELECT @@NetPremi=@@GrossPremium+@@Admin

                        SELECT 0 GrossPremium, CAST(@@Admin AS DECIMAL) Admin, 0 NetPremi,
						@@PolicyNo AS PolicyNo, @@OrderNo AS OrderNo, @@PolicyOrderNo AS PolicyOrderNo, 
                        @@SurveyNo AS SurveyNo, COALESCE(@@OldPolicyNo,'') AS OldPolicyNo, 0 NoClaimBonus, NULL OldPolicyPeriodTo";
                Otosales.Models.vWeb2.FieldPremi premi = db.Query<Otosales.Models.vWeb2.FieldPremi>(query, CustID, FollowUpNo).FirstOrDefault();
                if (premi != null)
                {
                    if (!string.IsNullOrEmpty(premi.OldPolicyNo))
                    {
                        List<dynamic> ls = new List<dynamic>();
                        //query = @"SELECT COALESCE(SUM(Amount), 0) AS NCB FROM dbo.Ren_Cover_Disc WHERE Policy_No = @0";
                        //ls = AABdb.Fetch<dynamic>(query, listDyn.First().OldPolicyNo);
                        //if (ls.Count > 0)
                        //{
                        //    premi.NoClaimBonus = ls.First().NCB;
                        //}
                        query = @"SELECT Period_To FROM dbo.Policy WHERE Policy_No = @0";
                        ls = AABdb.Fetch<dynamic>(query, premi.OldPolicyNo);
                        if (ls.Count > 0)
                        {
                            premi.OldPolicyPeriodTo = ls.First().Period_To;
                        }
                    }
                    result.FieldPremi = premi;
                }
                query = @"SELECT p.Name,p.CustBirthDay,p.CustGender,p.CustAddress,p.PostalCode,
                        p.Email1,p.Phone1,p.IdentityNo, CAST(COALESCE(p.isCompany,0) AS BIT) isCompany, 
                        CAST(0 AS BIT) isNeedDocRep, 0 AmountRep, IsPayerCompany
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
                        WHERE p.CustID = @0 AND p.RowStatus = 1";
                Otosales.Models.vWeb2.PersonalData pd = db.Query<Otosales.Models.vWeb2.PersonalData>(query, CustID).FirstOrDefault();
                if (pd != null)
                {
                    query = @"SELECT CAST(COALESCE(os.IsNeedDocRep,0) AS BIT)IsNeedDocRep, 
                            COALESCE(os.AmountRep, 0) AmountRep
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
						INNER JOIN dbo.OrderSimulation os
						ON os.FollowUpNo = f.FollowUpNo
                        WHERE p.CustID = @0 AND p.RowStatus = 1
						AND os.RowStatus = 1 AND os.ApplyF = 1";
                    listDyn = db.Fetch<dynamic>(query, CustID);
                    if (listDyn.Count > 0)
                    {
                        pd.isNeedDocRep = listDyn.First().IsNeedDocRep;
                        pd.AmountRep = listDyn.First().AmountRep;
                    }
                    result.PersonalData = pd;
                }
                query = @";SELECT IdentityCard, id1.Data IdentityCardData,
                        f.STNK, id2.Data STNKData,
                        f.BSTB1 BSTB, id3.Data BSTBData,
                        f.KonfirmasiCust, id4.Data KonfirmasiCustData,
                        f.DocRep, id5.Data DocRepData
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON f.IdentityCard = id1.PathFile
                        LEFT JOIN dbo.ImageData id2
                        ON f.STNK = id2.PathFile
                        LEFT JOIN dbo.ImageData id3
                        ON f.BSTB1 = id3.PathFile
                        LEFT JOIN dbo.ImageData id4
                        ON f.KonfirmasiCust = id4.PathFile
                        LEFT JOIN dbo.ImageData id5
                        ON f.DocRep = id5.PathFile
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result.PersonalDocument = listDyn;
                }

                query = @"SELECT CompanyName,NPWPno,NPWPdate,NPWPaddress NPWPaddres,OfficeAddress
                            ,PostalCode,PICPhoneNo,PICname,Email PICEmail FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";
                Otosales.Models.vWeb2.CompanyData cd = db.Query<Otosales.Models.vWeb2.CompanyData>(query, CustID, FollowUpNo).FirstOrDefault();
                if (cd != null)
                {
                    result.CompanyData = cd;
                }
                query = @";IF EXISTS(SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1)
                        BEGIN
	                        SELECT pc.NPWP, id1.Data NPWPData,
	                        pc.SIUP, id2.Data SIUPData
	                        FROM dbo.ProspectCompany pc
	                        LEFT JOIN dbo.ImageData id1
	                        ON pc.NPWP = id1.PathFile
	                        LEFT JOIN dbo.ImageData id2
	                        ON pc.SIUP = id2.PathFile
	                        WHERE pc.CustID = @0 AND pc.FollowUpNo = @1 AND pc.RowStatus = 1
                        END
                        ELSE
                        BEGIN
	                        SELECT NULL NPWP, NULL NPWPData,
	                        NULL SIUP, NULL SIUPData
                        END";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result.CompanyDocument = listDyn;
                }
                query = @";SELECT c.Description + ' ' + d.Description + ' ' + omv.Year + ' ' + a.Series  AS Vehicle,
                        omv.VehicleCode, omv.BrandCode, os.ProductTypeCode, omv.ModelCode,  omv.Series, 
                        omv.Type, CAST(omv.Sitting AS DECIMAL) Sitting, omv.Year, omv.CityCode, CAST(omv.SumInsured AS DECIMAL) SumInsured, os.InsuranceType,
                        os.VANumber, p.DealerCode, p.SalesDealer, omv.UsageCode, omv.ColorOnBPKB, 
                        omv.RegistrationNumber, omv.EngineNumber, omv.ChasisNumber, omv.IsNew, os.OrderNo,
						os.PolicyNo, os.ProductCode, os.SegmentCode, os.PeriodFrom, os.PeriodTo,
						os.IsORDefectsRepair, IsAccessoriesChange, IsPolicyIssuedBeforePaying, 
                        os.Remarks, f.DocPendukung, id.Data AS DocPendukungData,
                        0 BasicCoverID, '' ORDefectsDesc, '' NoCover
                        FROM Vehicle a 
                        LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
                        AND c.ProductTypeCode = a.ProductTypeCode 
                        LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
                        AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
                        LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                        a.BrandCode=b.BrandCode
                        AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                        AND a.Series=b.Series AND a.Type=b.Type 
                        AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                        AND a.Year > b.Year 
                        INNER JOIN dbo.OrderSimulationMV omv ON omv.VehicleCode = a.VehicleCode
                        INNER JOIN dbo.OrderSimulation os ON os.OrderNo = omv.OrderNo
                        INNER JOIN dbo.ProspectCustomer p ON p.CustID = os.CustID
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						LEFT JOIN dbo.ImageData id ON id.PathFile = f.DocPendukung
                        WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND omv.RowStatus = 1
						AND f.RowStatus = 1 AND os.CustID = @0 AND os.FollowUpNo = @1 AND b.Year IS NULL 
                        ORDER BY a.VehicleCode";
                Otosales.Models.vWeb2.VehicleData vData = db.Query<Otosales.Models.vWeb2.VehicleData>(query, CustID, FollowUpNo).FirstOrDefault();
                if (vData != null)
                {
                    query = @";DECLARE @@TLOPer INT
                            DECLARE @@ComprePer INT
                            SELECT @@TLOPer=TLOPeriod,@@ComprePer=ComprePeriod FROM dbo.OrderSimulation WHERE OrderNo = @0
                            SELECT ID AS BasicCoverID FROM dbo.Mst_Basic_Cover 
                            WHERE ComprePeriod = @@ComprePer
                            AND TLOPeriod = @@TLOPer";
                    List<dynamic> bscCvr = db.Fetch<dynamic>(query, vData.OrderNo);
                    if (bscCvr.Count > 1)
                    {
                        query = @"SELECT RTRIM(LTRIM(CoverageID)) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                        List<string> ListCvrID = db.Fetch<string>(query, vData.OrderNo);
                        if (ListCvrID.Contains("TLO"))
                        {
                            query = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%TLO Others%'";
                            int id = db.ExecuteScalar<int>(query);
                            vData.BasicCoverID = id;
                        }
                        else if (ListCvrID.Contains("ALLRIK"))
                        {
                            query = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%Comprehensive Others%'";
                            int id = db.ExecuteScalar<int>(query);
                            vData.BasicCoverID = id;
                        }
                    }
                    else
                    {
                        if (bscCvr.Count > 0)
                        {
                            vData.BasicCoverID = bscCvr.First().BasicCoverID;
                        }
                        else
                        {
                            vData.BasicCoverID = 0;
                        }
                    }
                    if (!string.IsNullOrEmpty(result.FieldPremi.OldPolicyNo))
                    {
                        query = @";DECLARE @@PolicyID VARCHAR(100)
                                SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
                                SELECT STUFF(( SELECT ', ' + Name
                                FROM dbo.Dtl_Original_Defect 
                                where Policy_Id = @@PolicyID
                                FOR XML PATH('')
                                ), 1, 1, '')  AS OriginalDefect,
								STUFF(( SELECT ', ' + Name
                                FROM dbo.Ren_Dtl_NoCover 
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS NoCover,
								STUFF(( SELECT ', ' + RTRIM(CONCAT(AccsDescription, ' ', AccsCatDescription, ' ',
                                AccsPartDescription))
                                FROM    ( SELECT DISTINCT
                                                    RTRIM(ISNULL(wnsa.Policy_No, '')) AS PolicyNo ,
                                                    wnsa.Object_No AS ObjectNo ,
                                                    RTRIM(ISNULL(wnsa.Accs_Code, '')) AS AccsCode ,
                                                    RTRIM(ISNULL(AO.AccsDescription, '')) AS AccsDescription ,
                                                    RTRIM(ISNULL(wnsa.Accs_Part_Code, '')) AS AccsPartCode ,
                                                    CASE ao.AccsType
                                                      WHEN 4 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      WHEN 5 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      ELSE ''
                                                    END AS AccsCatDescription ,
                                                    RTRIM(ISNULL(ap.AccsPartDescription, '')) AS AccsPartDescription ,
                                                    Include_Tsi AS IncludeTsi ,
                                                    RTRIM(ISNULL(Brand, '')) AS Brand ,
                                                    Sum_Insured AS SumInsured ,
                                                    Quantity ,
                                                    Premi ,
                                                    RTRIM(ISNULL(Category, '')) AS Category ,
                                                    RTRIM(ISNULL(wnsa.Accs_Cat_Code, '')) AS AccsCatCode
                                          FROM      dbo.Ren_Dtl_Non_Standard_Accessories AS wnsa
                                                    LEFT JOIN dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.Accs_Code
                                                                                          AND ao.RowStatus = 1
                                                    LEFT JOIN AccessoriesPart AP ON Ap.AccsCode = wnsa.Accs_Code
                                                                                    AND AP.AccsPartCode = wnsa.Accs_Part_Code
                                                                                    AND ap.RowStatus = 1
                                                    LEFT JOIN ( SELECT  RTRIM(ISNULL(ac.AccsCode, '')) AS AccsCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatCode, '')) AS AccsCatCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatDescription, '')) AS AccsCatDescription ,
                                                                        RTRIM(ISNULL(acp.AccsPartCode, '')) AS AccsPartCode ,
                                                                        ac.MaxSICategory ,
                                                                        ac.IsEditable ,
                                                                        acp.QtyDefault ,
                                                                        ac.RowStatus
                                                                FROM    dbo.AccessoriesCategory AS ac
                                                                        LEFT JOIN dbo.AccessoriesCategoryPart
                                                                        AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                                                  AND acp.RowStatus = 1
                                                              ) x ON x.AccsCode = ao.AccsCode
                                                                     AND x.RowStatus = 1
                                                                     AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                                                              '')
                                                                     AND x.AccsCode = wnsa.Accs_Code
                                                                     AND x.AccsCatCode = wnsa.Accs_Cat_Code
                                          WHERE     Policy_No = @0
                                        ) AS X
                                FOR XML PATH('')
                                ), 1, 1, '')  AS Accessories";
                        List<dynamic> list = AABdb.Fetch<dynamic>(query, result.FieldPremi.OldPolicyNo);
                        if (list.Count > 0)
                        {
                            vData.ORDefectsDesc = list.First().OriginalDefect;
                            vData.NoCover = list.First().NoCover;
                            vData.Accessories = list.First().Accessories;
                        }
                    }
                    result.VehicleData = vData;
                }
                //result.PaymentInfo = GetPaymentInfo(CustID, FollowUpNo);
                result.PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo();
                query = @"SELECT PolicySentTo SentTo, PolicyDeliveryName Name, PolicyDeliveryAddress Address, PolicyDeliveryPostalCode PostalCode FROM dbo.OrderSimulation WHERE CustID = @0 
                            AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1";
                Otosales.Models.vWeb2.PolicyAddress pa = db.Query<Otosales.Models.vWeb2.PolicyAddress>(query, CustID, FollowUpNo).FirstOrDefault();
                if (pa != null)
                {
                    result.PolicyAddres = pa;
                }
                if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                {
                    query = @";SELECT odi.[Description] AS NoCover,
                        STUFF(( SELECT ', ' + [Description]
                                    FROM dbo.Ord_Object_Clause 
			                        WHERE Order_No = @0
								    AND Clause_Id = 'OMV99'
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS OriginalDefect,
                        STUFF(( SELECT ', ' + Item_Name
                                    FROM dbo.Ord_Dtl_Interest_Item 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS Accessories,
                        ms.[Description] AS SurveyStatus,
                        s.ResultRecommendation AS SurveyorRecomendation,
                        s.remarks AS Remaks,
                        mu.[User_Name] AS Surveyor
                        FROM dbo.Survey s 
						INNER JOIN dbo.Lnk_Survey_Order lso
						ON s.Survey_No = lso.Survey_No
                        INNER JOIN dbo.Mst_Order mo 
                        ON mo.Order_No = s.Reference_No
                        INNER JOIN dbo.Ord_Dtl_Interest odi
                        ON odi.Order_No = mo.Order_No
                        INNER JOIN dbo.Mst_State ms 
                        ON ms.[Type] = 'survey' 
                        AND ms.[State] = s.[Status]
                        INNER JOIN dbo.Mst_User mu
                        ON mu.[User_Id] = s.Surveyor
                        WHERE mo.Order_No = @0
						AND odi.Interest_Code = 'CASCO'";
                    Otosales.Models.vWeb2.SurveyData sd = AABdb.Query<Otosales.Models.vWeb2.SurveyData>(query, result.FieldPremi.PolicyOrderNo).FirstOrDefault();
                    if (listDyn.Count > 0)
                    {
                        result.SurveyData = sd;
                    }
                }
                query = @"SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc, CAST(COALESCE(IsManualSurvey,0) AS BIT) IsManualSurvey
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                Otosales.Models.vWeb2.SurveySchedule ss = db.Fetch<Otosales.Models.vWeb2.SurveySchedule>(query, CustID, FollowUpNo).FirstOrDefault();
                if (listDyn.Count > 0)
                {
                    result.SurveySchedule = ss;
                }
                query = @";SELECT f.SPPAKB, id1.Data SPPAKBData,
                        f.FAKTUR, id2.Data FAKTURData,
                        f.DocNSA1, id3.Data DocNSA1Data,
                        f.DocNSA2, id4.Data DocNSA2Data,
                        f.DocNSA3, id5.Data DocNSA3Data,
                        f.DocNSA4, id6.Data DocNSA4Data,
                        f.DocNSA5, id7.Data DocNSA5Data,
                        RemarkToSA, RemarkFromSA
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON id1.PathFile = f.SPPAKB
                        LEFT JOIN dbo.ImageData id2
                        ON id2.PathFile = f.FAKTUR
                        LEFT JOIN dbo.ImageData id3
                        ON id3.PathFile = f.DocNSA1
                        LEFT JOIN dbo.ImageData id4
                        ON id4.PathFile = f.DocNSA2
                        LEFT JOIN dbo.ImageData id5
                        ON id5.PathFile = f.DocNSA3
                        LEFT JOIN dbo.ImageData id6
                        ON id6.PathFile = f.DocNSA4
                        LEFT JOIN dbo.ImageData id7
                        ON id7.PathFile = f.DocNSA5
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                List<dynamic> listDoc = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDoc.Count > 0)
                {
                    query = @"SELECT TOP 1 mu.User_Name Name 
                                FROM dbo.Mst_Order_Mobile mom
                                INNER JOIN dbo.Mst_User mu
                                ON mu.User_Id = mom.EntryUsr
                                WHERE Order_No = @0 
                                AND (SA_State = 0 OR SA_State = 2) 
                                AND mom.EntryUsr <> 'OTOSL' ORDER BY mom.EntryDt DESC";
                    List<dynamic> ls = AABdb.Fetch<dynamic>(query, result.FieldPremi.PolicyOrderNo);
                    if (ls.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(listDoc.First().RemarkFromSA) && !string.IsNullOrWhiteSpace(listDoc.First().RemarkFromSA)
                            && !string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name + " - " + listDoc.First().RemarkFromSA;
                        }
                        else if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name;
                        }
                    }
                    result.Document = listDoc;
                }

                query = @"SELECT fu.FollowUpStatus,fu.FollowUpInfo,fu.IsRenewal,so.Name as Agency,'' as Upliner, fu.SalesOfficerID, so.Name
                        FROM dbo.FollowUp fu  
                        INNER JOIN dbo.SalesOfficer so 
                        ON so.SalesOfficerId=fu.SalesOfficerId
                        WHERE fu.FollowUpNo = @0";
                listDyn = db.Fetch<dynamic>(query, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    foreach (dynamic item in listDyn)
                    {
                        bool isAgency = AABdb.ExecuteScalar<int>(@";SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] eu  WITH ( NOLOCK ) INNER JOIN beyondmoss.AABMobile.dbo.SalesOfficer so ON eu.UserID=so.Email WHERE so.SalesOfficerId=@0", item.SalesOfficerID) > 0 ? true : false;
                        item.Agency = isAgency ? item.Agency : "";
                        item.Upliner = isAgency ? AABdb.ExecuteScalar<string>(@"SELECT TOP 1 RTRIM(d.Name) AS Upliner         
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT' AND b.Cust_Id=@0 OR a.Client_Code=@0", item.SalesOfficerID) : "";
                    }
                    result.FollowUpInfo = listDyn;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static Otosales.Models.vWeb2.CheckDoubleInsuredResult GetCheckDoubleInsuredDataKotor(string OldPolicyNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            Otosales.Models.vWeb2.CheckDoubleInsuredResult result = new Otosales.Models.vWeb2.CheckDoubleInsuredResult();
            try
            {
                string ChasisNo = "";
                string EngineNo = "";
                string PolicyNo = "";
                DateTime PeriodFrom = DateTime.MinValue;
                DateTime PeriodTo = DateTime.MinValue;
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = @";SELECT Chasis_Number,Engine_Number, rn.New_Policy_No, P.Period_To PeriodFrom, DATEADD(YEAR,1,Period_To) PeriodTo 
                        FROM dbo.Excluded_Renewal_Policy erp
                        INNER JOIN dbo.Renewal_Notice rn
                        ON erp.Policy_No = rn.Policy_No
                        inner JOIN dbo.Policy p
                        ON P.Policy_No = rn.Policy_No AND P.Order_Status IN ('11','9')
                        WHERE erp.Policy_No = @0";
                List<dynamic> listoldData = db.Fetch<dynamic>(query, OldPolicyNo);
                if (listoldData.Count > 0)
                {
                    ChasisNo = AppParameterAccess.SafeTrim(listoldData.First().Chasis_Number);
                    EngineNo = AppParameterAccess.SafeTrim(listoldData.First().Engine_Number);
                    PolicyNo = AppParameterAccess.SafeTrim(listoldData.First().New_Policy_No);
                    PeriodFrom = listoldData.First().PeriodFrom;
                    PeriodTo = listoldData.First().PeriodTo;
                }
                else {
                    query = @";SELECT Chasis_Number,Engine_Number, '' New_Policy_No, P.Period_To PeriodFrom, DATEADD(YEAR,1,Period_To) PeriodTo 
                        FROM dbo.Excluded_Renewal_Policy erp                        
                        inner JOIN dbo.Policy p
                        ON P.Policy_No = erp.Policy_No AND P.Order_Status IN ('11','9')
                        WHERE erp.Policy_No = @0";
                    listoldData = db.Fetch<dynamic>(query, OldPolicyNo);
                    if (listoldData.Count > 0)
                    {
                        ChasisNo = AppParameterAccess.SafeTrim(listoldData.First().Chasis_Number);
                        EngineNo = AppParameterAccess.SafeTrim(listoldData.First().Engine_Number);
                        PolicyNo = AppParameterAccess.SafeTrim(listoldData.First().New_Policy_No);
                        PeriodFrom = listoldData.First().PeriodFrom;
                        PeriodTo = listoldData.First().PeriodTo;
                    }
                }
                query = @";EXEC [dbo].[usp_CheckDoubleInsuredOtosales] @0, @1, @2, @3, @4, @5, @6";
                List<dynamic> res = db.Fetch<dynamic>(query, 2, ChasisNo, EngineNo, PolicyNo, "", PeriodFrom, PeriodTo);
                result.IsDoubleInsured = false;
                if (res.Count > 0)
                {
                    string DoubleInsuredType = Convert.ToString(res.First().DoubleInsuredType);
                    if (DoubleInsuredType.ToUpper().Equals("POLICY"))
                    {
                        int ThdDay = Convert.ToInt32(AppParameterAccess.GetApplicationParametersValue("DOUBLEINSUREDDAY-RENEW")[1]);
                        foreach (dynamic p in res)
                        {
                            result.IsDoubleInsured = true;
                            result.OrderNo = p.Order_No;
                            result.PolicyNo = p.Policy_No;
                            result.PeriodFrom = p.Period_From;
                            result.PeriodTo = p.Period_To;
                            //if (Convert.ToInt32(p.Days) > ThdDay)
                            //{
                            //    result.IsDoubleInsured = true;
                            //    result.OrderNo = p.Order_No;
                            //    result.PolicyNo = p.Policy_No;
                            //    result.PeriodFrom = p.Period_From;
                            //    result.PeriodTo = p.Period_To;
                            //}
                        }
                    }
                    else if (DoubleInsuredType.ToUpper().Equals("ORDER"))
                    {
                        result.IsDoubleInsured = true;
                        result.OrderNo = res.First().Order_No;
                        result.PolicyNo = res.First().Policy_No;
                        result.PeriodFrom = res.First().Period_From;
                        result.PeriodTo = res.First().Period_To;
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        #region 0001URF2019
        public static Otosales.Models.vWeb2.PaymentInfo GetPaymentInfo(string CustID, string FollowUpNo, int IsGenerateVA)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var BYNDdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
            List<dynamic> listDyn = new List<dynamic>();
            List<dynamic> listVANo = new List<dynamic>();
            Otosales.Models.vWeb2.PaymentInfo pf = new Otosales.Models.vWeb2.PaymentInfo();
            string VaNumber = "";
            int isReactive = 0;
            try
            {
                query = @";IF EXISTS(SELECT * FROM dbo.OrderSimulation os
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						WHERE os.ApplyF = 1 AND f.CustID = @0 AND f.FollowUpNo = @1)
						BEGIN
							SELECT os.VANumber, f.BuktiBayar AS BuktiBayar, 
							id.Data AS BuktiBayarData, os.PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data
							FROM dbo.OrderSimulation os 
							INNER JOIN dbo.FollowUp f 
							ON f.FollowUpNo = os.FollowUpNo
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE f.CustID = @0 AND f.FollowUpNo = @1 
							AND f.RowStatus = 1
						END
						ELSE
						BEGIN
							SELECT '' AS VANumber, BuktiBayar, 
                            id.Data AS BuktiBayarData, '' AS PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data							 
							FROM dbo.FollowUp f 
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE CustID = @0 AND FollowUpNo = @1
							AND f.RowStatus = 1
						END";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    query = @"SELECT OrderNo,COALESCE(ProductCode,'') AS ProductCode,isCompany,os.SalesOfficerID,os.PolicyOrderNo, VANumber, os.PolicyNo
                        ,CAST(COALESCE(f.IsRenewal,0) AS BIT) IsRenewal, PeriodFrom, PeriodTo
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        INNER JOIN dbo.ProspectCustomer ps
                        ON ps.CustID = f.CustID
                        WHERE os.CustID = @0 
                        AND os.FollowUpNo = @1
                        AND os.ApplyF = 1
                        AND os.RowStatus = 1";
                    List<dynamic> ordData = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                    string qUpdateVirtualAccountNo = @";DECLARE @@ID BIGINT
DECLARE @@SoID VARCHAR(16)
SELECT @@ID=VirtualAccountNumberID, @@SoId = SalesOfficerID
FROM Finance.VirtualAccountNumber a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNumber
WHERE a.VirtualAccountNumber = @0

UPDATE Finance.VirtualAccountNumber 
SET ValidTo = ValidFrom, ModifiedBy = @@SoId, ModifiedDate = GETDATE()
WHERE VirtualAccountNumberID = @@ID";
                    string qUpdateAAB2000VA = @";DECLARE @@ID BIGINT 
DECLARE @@SoID VARCHAR(16)
SELECT @@ID=AAB2000VirtualAccountID
FROM Finance.AAB2000VirtualAccount a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNo
WHERE VirtualAccountNo = @0 AND a.PolicyNo = @1

UPDATE Finance.AAB2000VirtualAccount 
SET PolicyPeriodTo = GETDATE(), ModifiedBy = @@SoID, ModifiedDate = GETDATE()
WHERE AAB2000VirtualAccountID = @@ID";
                    if (ordData.Count > 0)
                    {
                        pf.IsGenerateVA = false;
                        pf.IsReGenerateVA = false;
                        if (Convert.ToBoolean(ordData.First().isCompany))
                        {
                            query = @"SELECT a.branch_id ,
                                            a.accounting_code_sun ,
                                            b.accountno ,
                                            b.bankdescription ,
                                            b.name ,
                                            b.branchdescription
                                    FROM   mst_branch AS a WITH ( NOLOCK )
                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                            List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                            if (lists.Count > 0)
                            {
                                pf.RekeningPenampung = lists.First().accountno;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ordData.First().ProductCode))
                            {
                                query = @"select CAST(COALESCE(IsAllowVA,0) AS BIT) IsAllowVA, Biz_type from mst_product where Product_Code = @0";
                                List<dynamic> listBizType = AABdb.Fetch<dynamic>(query, ordData.First().ProductCode);
                                if (listBizType.Count > 0)
                                {
                                    int b = 0;
                                    b = listBizType.First().Biz_type;
                                    query = @"Select Salesman_Id,mb.Branch_id From Mst_Salesman ms
                                        INNER JOIN dbo.Mst_Branch mb
                                        ON mb.Branch_id = ms.Branch_Id
                                        Where User_id_otosales= @0 
                                        and ms.status=1 AND mb.Biz_type = @1";
                                    string qGetIsPayerCompany = @"SELECT CAST(COALESCE(IsPayerCompany,0) AS BIT) FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1";
                                    bool IsPayerCompany = db.ExecuteScalar<bool>(qGetIsPayerCompany, CustID);
                                    if (!listBizType.First().IsAllowVA || IsPayerCompany)
                                    {
                                        if (b == 2)
                                        {
                                            pf.RekeningPenampung = AppParameterAccess.GetApplicationParametersValue("REKPENAMPUNGSYARIAH").First();
                                        }
                                        else
                                        {
                                            query = @"SELECT a.branch_id ,
                                                            a.accounting_code_sun ,
                                                            b.accountno ,
                                                            b.bankdescription ,
                                                            b.name ,
                                                            b.branchdescription
                                                        FROM mst_branch AS a WITH ( NOLOCK )
                                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                                            List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                                            if (lists.Count > 0)
                                            {
                                                pf.RekeningPenampung = lists.First().accountno;
                                            }
                                        }
                                        query = @"SELECT OrderNo,VANumber,OldPolicyNo, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal
                                                FROM dbo.OrderSimulation os
                                                INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND os.RowStatus = 1";
                                        List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                        if (ordData.Count > 0)
                                        {
                                            string VaNo = Convert.ToString(ordDt.First().VANumber);
                                            if (!string.IsNullOrEmpty(VaNo))
                                            {
                                                string trnsDate = "";
                                                string exprDate = "";

                                                string qGetTransDate = @"SELECT EntryDate StartDate, DATEADD(DAY,CAST(ParamValue AS INT),EntryDate) EndDate FROM dbo.OrderSimulation os
	                                                                    INNER JOIN dbo.ApplicationParameters ap ON 1=1 AND ParamName = 'END-CHECKVAPAYMENT'
	                                                                    WHERE OrderNo = @0";
                                                List<dynamic> lsDate = db.Fetch<dynamic>(qGetTransDate, ordData.First().OrderNo);
                                                if (lsDate.Count > 0)
                                                {
                                                    trnsDate = lsDate.First().StartDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                    exprDate = lsDate.First().EndDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                    string cSQLGetTotalBayarVA = @";WITH cte
                                                                                        AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                                                    a.VI_TraceNo
                                                                                            FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                                                            WHERE    b.VI_VANumber IN (
                                                                                                        SELECT  value
                                                                                                        FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                                                        )
                                                                                        --AND a.PermataVirtualAccountPaymentSourceCode = '02'
		                                                                                AND a.VI_TransactionDate BETWEEN @1 AND @2
                                                                                            )
                                                                                SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                                                        SUM(pvas.VI_Amount) AS TotalBayar ,
                                                                                        MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                                                        MAX(pva.BillAmount) AS TotalBill
                                                                                FROM    cte
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                                                            AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                                                                WHERE   IsReversal = 0";
                                                    List<dynamic> ls = BYNDdb.Fetch<dynamic>(cSQLGetTotalBayarVA, VaNo, trnsDate, exprDate);
                                                    if (ls.Count > 0)
                                                    {
                                                        decimal TotalBayar = ls.First().TotalBayar == null ? 0 : Convert.ToDecimal(ls.First().TotalBayar);
                                                        if (TotalBayar == 0)
                                                        {
                                                            if (!ordDt.First().IsRenewal)
                                                            {
                                                                BYNDdb.Execute(qUpdateVirtualAccountNo, VaNo);
                                                                //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                            }
                                                            else
                                                            {
                                                                BYNDdb.Execute(qUpdateAAB2000VA
                                                                    , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                            }
                                                            listDyn.First().VANumber = "";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!ordDt.First().IsRenewal)
                                                        {
                                                            BYNDdb.Execute(qUpdateVirtualAccountNo, VaNo);
                                                            //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                        }
                                                        else
                                                        {
                                                            BYNDdb.Execute(qUpdateAAB2000VA
                                                                , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                        }
                                                        listDyn.First().VANumber = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(ordData.First().VANumber))
                                        {
                                            if (!ordData.First().IsRenewal)
                                            {
                                                List<dynamic> lists = BYNDdb.Fetch<dynamic>("SELECT RowStatus FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                    , ordData.First().VANumber);
                                                if (lists.Count > 0)
                                                {
                                                    DateTime dtF = BYNDdb.ExecuteScalar<DateTime>("SELECT ValidFrom FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                    , ordData.First().VANumber);
                                                    DateTime dtT = BYNDdb.ExecuteScalar<DateTime>("SELECT ValidTo FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                    , ordData.First().VANumber);
                                                    if (dtF.Date.AddDays(7) > DateTime.Now.Date && dtT.Date == dtF.Date)
                                                    {
                                                        BYNDdb.Execute(@";DECLARE @@ID BIGINT
                                                    DECLARE @@SoID VARCHAR(16)
													SELECT @@ID=VirtualAccountNumberID, @@SoID = SalesOfficerID
                                                    FROM Finance.VirtualAccountNumber a
                                                    INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
                                                    ON os.VANumber = a.VirtualAccountNumber
                                                    WHERE a.VirtualAccountNumber = @0

                                                    UPDATE Finance.VirtualAccountNumber 
                                                    SET ValidTo = ValidFrom+7, ModifiedBy = @@SoID, ModifiedDate = GETDATE()
                                                    WHERE VirtualAccountNumberID = @@ID"
                                                        , ordData.First().VANumber);
                                                        Otosales.Repository.v0232URF2019.MobileRepository.AddVA(ordData.First().OrderNo);
                                                    }
                                                }
                                                //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                            }
                                            else
                                            {
                                                BYNDdb.Execute(@";DECLARE @@ID BIGINT
                                                DECLARE @@PeriodTo DATETIME
												DECLARE @@SoId VARCHAR(16)
                                                SELECT @@ID=AAB2000VirtualAccountID, @@PeriodTo=os.PeriodTo, @@SoId = SalesOfficerID
                                                FROM Finance.AAB2000VirtualAccount a
                                                INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
                                                ON os.VANumber = a.VirtualAccountNo
                                                WHERE VirtualAccountNo = @0 AND a.PolicyNo = @1

                                                UPDATE Finance.AAB2000VirtualAccount 
                                                SET PolicyPeriodTo = @@PeriodTo, ModifiedBy = @@SoId, ModifiedDate = GETDATE()
                                                WHERE AAB2000VirtualAccountID = @@ID"
                                                , ordData.First().VANumber, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                            }
                                        }
                                        query = @"SELECT OrderNo,VANumber,OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";
                                        List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                        if (ordData.Count > 0)
                                        {
                                            bool isGenerate = true;
                                            VaNumber = Convert.ToString(ordDt.First().VANumber);
                                            int VAstatus = 0;
                                            DateTime validto = DateTime.Now;
                                            if (!string.IsNullOrEmpty(VaNumber))
                                            {
                                                listVANo = BYNDdb.Fetch<dynamic>("SELECT ValidTo, RowStatus FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0", VaNumber);
                                                isReactive = db.ExecuteScalar<int>("SELECT COALESCE(isVAReactive,0) isVAreactive FROM dbo.OrderSimulation WHERE OrderNo = @0", ordData.First().OrderNo);
                                                if (listVANo.Count > 0)
                                                {
                                                    validto = Convert.ToDateTime(listVANo.First().ValidTo);
                                                    VAstatus = listVANo.First().RowStatus;
                                                }
                                            }
                                            if (string.IsNullOrEmpty(VaNumber) || (validto.Date < DateTime.Now.Date && isReactive == 0))
                                            {
                                                if (!string.IsNullOrEmpty(ordDt.First().OldPolicyNo))
                                                {
                                                    query = @";SELECT CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal FROM dbo.OrderSimulation os 
                                                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                            WHERE os.OrderNo = @0";
                                                    List<dynamic> lsRen = db.Fetch<dynamic>(query, ordDt.First().OrderNo);
                                                    if (lsRen.Count > 0)
                                                    {
                                                        isGenerate = !lsRen.First().IsRenewal;
                                                    }
                                                    query = @";SELECT Cust_Type FROM dbo.Mst_Order mo
                                                            INNER JOIN dbo.Mst_Customer mc 
                                                            ON mo.Payer_Code = mc.Cust_Id
                                                            WHERE Policy_No = @0";
                                                    lsRen = AABdb.Fetch<dynamic>(query, ordDt.First().OldPolicyNo);
                                                    if (lsRen.Count > 0)
                                                    {
                                                        if (Convert.ToInt32(lsRen.First().Cust_Type) == 2)
                                                        {
                                                            db.Execute(@"UPDATE dbo.ProspectCustomer SET IsPayerCompany = 1 WHERE CustID = @0 AND RowStatus = 1", CustID);
                                                        }
                                                    }
                                                }
                                                if (isGenerate)
                                                {
                                                    if (ordData.First().PeriodFrom != null && ordData.First().PeriodTo != null)
                                                    {
                                                        if (string.IsNullOrEmpty(VaNumber))
                                                        {
                                                            pf.IsGenerateVA = true;
                                                        }
                                                        else if (isReactive == 0)
                                                        {
                                                            pf.IsReGenerateVA = true;
                                                        }

                                                        if (IsGenerateVA == 1)
                                                        {
                                                            VaNumber = Otosales.Repository.v0232URF2019.MobileRepository.GetBookingVANo();
                                                            query = @"UPDATE dbo.OrderSimulation 
                                                                SET VANumber=@2
                                                                WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";
                                                            db.Execute(query, CustID, FollowUpNo, VaNumber);
                                                            Otosales.Repository.v0232URF2019.MobileRepository.AddVA(ordData.First().OrderNo);
                                                            pf.IsGenerateVA = false;
                                                            if (pf.IsReGenerateVA) {
                                                                db.Execute(@";UPDATE dbo.OrderSimulation SET isVAReactive = 1 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1"
                                                                    , CustID, FollowUpNo);
                                                                pf.IsReGenerateVA = false;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            string pPermata = AppParameterAccess.GetApplicationParametersValue("PREFIXVAPERMATA").First();
                            string pMandiri = AppParameterAccess.GetApplicationParametersValue("PREFIXVAMANDIRI").First();
                            string pBCA = AppParameterAccess.GetApplicationParametersValue("PREFIXVABCA").First();
                            string vaold = db.ExecuteScalar<string>("SELECT COALESCE(VANumber,'') FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1", CustID, FollowUpNo);
                            VaNumber = string.IsNullOrEmpty(Convert.ToString(vaold)) ? VaNumber : Convert.ToString(vaold);
                            if (!string.IsNullOrEmpty(VaNumber))
                            {
                                pf.VAPermata = pPermata + VaNumber;
                                pf.VAMandiri = pMandiri + VaNumber;
                                pf.VABCA = pBCA + VaNumber;
                            }
                        }
                        pf.BuktiBayar = listDyn.First().BuktiBayar;
                        pf.BuktiBayarData = listDyn.First().BuktiBayarData;
                        pf.BuktiBayar2 = listDyn.First().BuktiBayar2;
                        pf.BuktiBayar2Data = listDyn.First().BuktiBayar2Data;
                        pf.BuktiBayar3 = listDyn.First().BuktiBayar3;
                        pf.BuktiBayar3Data = listDyn.First().BuktiBayar3Data;
                        pf.BuktiBayar4 = listDyn.First().BuktiBayar4;
                        pf.BuktiBayar4Data = listDyn.First().BuktiBayar4Data;
                        pf.BuktiBayar5 = listDyn.First().BuktiBayar5;
                        pf.BuktiBayar5Data = listDyn.First().BuktiBayar5Data;
                        string PolicyOrderNo = Convert.ToString(listDyn.First().PolicyOrderNo);
                        if (!string.IsNullOrEmpty(PolicyOrderNo))
                        {
                            query = @"SELECT DATEADD(DAY,13,Order_Date) AS DueDate FROM dbo.Mst_Order where Order_No = @0";
                            List<dynamic> listDate = AABdb.Fetch<dynamic>(query, PolicyOrderNo);
                            if (listDate.Count > 0)
                            {
                                pf.DueDate = listDate.First().DueDate;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ordData.First().ProductCode) && !string.IsNullOrEmpty(VaNumber))
                            {
                                decimal gp = AABdb.ExecuteScalar<decimal>("SELECT COALESCE(grace_period,0) FROM dbo.Mst_Product WHERE Product_Code = @0", ordData.First().ProductCode);
                                if (gp > 0)
                                {
                                    query = @"SELECT DATEADD(DAY,grace_period,os.EntryDate) DueDate
                                            FROM dbo.OrderSimulation os
                                            INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Product mp
                                            ON os.ProductCode = mp.Product_Code
                                            WHERE os.OrderNo = @0";
                                    List<dynamic> listDate = db.Fetch<dynamic>(query, ordData.First().OrderNo);
                                    if (listDate.Count > 0)
                                    {
                                        pf.DueDate = listDate.First().DueDate;
                                    }
                                }
                            }
                        }
                        pf.IsVAActive = null;
                        if (!string.IsNullOrEmpty(VaNumber) && string.IsNullOrEmpty(pf.RekeningPenampung) && string.IsNullOrEmpty(ordData.First().PolicyOrderNo))
                        {
                            if (listVANo.Count > 0)
                            {
                                if (isReactive == 0)
                                {
                                    DateTime validto = Convert.ToDateTime(listVANo.First().ValidTo);
                                    if (listVANo.First().RowStatus == 0 && (validto.Date >= DateTime.Now.Date))
                                    {
                                        pf.IsVAActive = true;
                                    }
                                    else
                                    {
                                        pf.IsVAActive = false;
                                    }
                                }
                            }
                        }
                        pf.VANo = VaNumber;
                    }
                }
                return pf;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                throw e;
            }
        }

        public static bool CheckVAExpiry(string OrderNo) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            try
            {
                var bynddb = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
                var mbldb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                bool IsPay = Otosales.Repository.v0232URF2019.MobileRepository.CheckVAPayment(OrderNo);
                if (IsPay)
                {
                    return false;
                }
                else {
                    List<dynamic> OrdList = mbldb.Fetch<dynamic>(@";SELECT COALESCE(os.VANumber,'') VANumber, CAST(COALESCE(IsPayerCompany, 0) AS BIT) IsPayerCompany, 
                                                                Biz_type, CAST(COALESCE(isCompany,0) AS BIT) isCompany FROM dbo.OrderSimulation os 
                                                                INNER JOIN dbo.ProspectCustomer pc ON os.CustID = pc.CustID
                                                                INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Product mp ON mp.Product_Code = os.ProductCode
                                                                WHERE OrderNo = @0 AND os.RowStatus = 1", OrderNo);
                    if (OrdList.Count > 0) {
                        string VANumber = OrdList.First().VANumber;
                        if (Convert.ToInt32(OrdList.First().Biz_type) == 2 || OrdList.First().IsPayerCompany || OrdList.First().isCompany)
                        {
                            return false;
                        }
                        else if (!string.IsNullOrEmpty(OrdList.First().VANumber))
                        {
                            List<dynamic> listVANo = bynddb.Fetch<dynamic>(@"SELECT ValidTo FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0 AND RowStatus = 0", VANumber);
                            if (listVANo.Count > 0)
                            {
                                DateTime ValidTo = listVANo.First().ValidTo;
                                if (ValidTo.Date < DateTime.Now)
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());                
                throw e;
            }
        }
        #endregion
    }
}