﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.dta;
using Otosales.Models.v0232URF2019;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Otosales.Repository.v0232URF2019
{
    public class MobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        public static Otosales.Models.vWeb2.PaymentInfo GetPaymentInfo(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var BYNDdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
            List<dynamic> listDyn = new List<dynamic>();
            Otosales.Models.vWeb2.PaymentInfo pf = new Otosales.Models.vWeb2.PaymentInfo();
            string VaNumber = "";

            try
            {
                query = @";IF EXISTS(SELECT * FROM dbo.OrderSimulation os
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						WHERE os.ApplyF = 1 AND f.CustID = @0 AND f.FollowUpNo = @1)
						BEGIN
							SELECT os.VANumber, f.BuktiBayar AS BuktiBayar, 
							id.Data AS BuktiBayarData, os.PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data
							FROM dbo.OrderSimulation os 
							INNER JOIN dbo.FollowUp f 
							ON f.FollowUpNo = os.FollowUpNo
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE f.CustID = @0 AND f.FollowUpNo = @1 
							AND f.RowStatus = 1
						END
						ELSE
						BEGIN
							SELECT '' AS VANumber, BuktiBayar, 
                            id.Data AS BuktiBayarData, '' AS PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data							 
							FROM dbo.FollowUp f 
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE CustID = @0 AND FollowUpNo = @1
							AND f.RowStatus = 1
						END";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    query = @"SELECT OrderNo,COALESCE(ProductCode,'') AS ProductCode,isCompany,os.SalesOfficerID,os.PolicyOrderNo, VANumber, os.PolicyNo
                        ,CAST(COALESCE(f.IsRenewal,0) AS BIT) IsRenewal, PeriodFrom, PeriodTo
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        INNER JOIN dbo.ProspectCustomer ps
                        ON ps.CustID = f.CustID
                        WHERE os.CustID = @0 
                        AND os.FollowUpNo = @1
                        AND os.ApplyF = 1";
                    List<dynamic> ordData = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                    string qUpdateVirtualAccountNo = @";DECLARE @@ID BIGINT
DECLARE @@SoID VARCHAR(16)
SELECT @@ID=VirtualAccountNumberID, @@SoId = SalesOfficerID
FROM Finance.VirtualAccountNumber a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNumber
WHERE a.VirtualAccountNumber = @0

UPDATE Finance.VirtualAccountNumber 
SET ValidTo = ValidFrom, ModifiedBy = @@SoId, ModifiedDate = GETDATE()
WHERE VirtualAccountNumberID = @@ID";
                    string qUpdateAAB2000VA = @";DECLARE @@ID BIGINT 
DECLARE @@SoID VARCHAR(16)
SELECT @@ID=AAB2000VirtualAccountID
FROM Finance.AAB2000VirtualAccount a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNo
WHERE VirtualAccountNo = @0 AND a.PolicyNo = @1

UPDATE Finance.AAB2000VirtualAccount 
SET PolicyPeriodTo = GETDATE(), ModifiedBy = @@SoID, ModifiedDate = GETDATE()
WHERE AAB2000VirtualAccountID = @@ID";
                    if (ordData.Count > 0)
                    {
                        if (Convert.ToBoolean(ordData.First().isCompany))
                        {
                            query = @"SELECT a.branch_id ,
                                            a.accounting_code_sun ,
                                            b.accountno ,
                                            b.bankdescription ,
                                            b.name ,
                                            b.branchdescription
                                    FROM   mst_branch AS a WITH ( NOLOCK )
                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                            List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                            if (lists.Count > 0)
                            {
                                pf.RekeningPenampung = lists.First().accountno;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ordData.First().ProductCode))
                            {
                                query = @"select CAST(COALESCE(IsAllowVA,0) AS BIT) IsAllowVA, Biz_type from mst_product where Product_Code = @0";
                                List<dynamic> listBizType = AABdb.Fetch<dynamic>(query, ordData.First().ProductCode);
                                if (listBizType.Count > 0)
                                {
                                    int b = 0;
                                    b = listBizType.First().Biz_type;
                                    query = @"Select Salesman_Id,mb.Branch_id From Mst_Salesman ms
                                        INNER JOIN dbo.Mst_Branch mb
                                        ON mb.Branch_id = ms.Branch_Id
                                        Where User_id_otosales= @0 
                                        and ms.status=1 AND mb.Biz_type = @1";
                                    string qGetIsPayerCompany = @"SELECT CAST(COALESCE(IsPayerCompany,0) AS BIT) FROM dbo.ProspectCustomer WHERE CustID = @0";
                                    bool IsPayerCompany = db.ExecuteScalar<bool>(qGetIsPayerCompany, CustID);
                                    if (!listBizType.First().IsAllowVA || IsPayerCompany)
                                    {
                                        if (b == 2)
                                        {
                                            pf.RekeningPenampung = AppParameterAccess.GetApplicationParametersValue("REKPENAMPUNGSYARIAH").First();
                                        }
                                        else
                                        {
                                            query = @"SELECT a.branch_id ,
                                                            a.accounting_code_sun ,
                                                            b.accountno ,
                                                            b.bankdescription ,
                                                            b.name ,
                                                            b.branchdescription
                                                        FROM mst_branch AS a WITH ( NOLOCK )
                                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                                            List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                                            if (lists.Count > 0)
                                            {
                                                pf.RekeningPenampung = lists.First().accountno;
                                            }
                                        }
                                        query = @"SELECT OrderNo,VANumber,OldPolicyNo, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal
                                                FROM dbo.OrderSimulation os
                                                INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                WHERE os.CustID = @0 AND os.FollowUpNo = @1";
                                        List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                        if (ordData.Count > 0)
                                        {
                                            string VaNo = Convert.ToString(ordDt.First().VANumber);
                                            if (!string.IsNullOrEmpty(VaNo))
                                            {
                                                string trnsDate = "";
                                                string exprDate = "";

                                                string qGetTransDate = @"SELECT EntryDate StartDate, DATEADD(DAY,CAST(ParamValue AS INT),EntryDate) EndDate FROM dbo.OrderSimulation os
	                                                                    INNER JOIN dbo.ApplicationParameters ap ON 1=1 AND ParamName = 'END-CHECKVAPAYMENT'
	                                                                    WHERE OrderNo = @0";
                                                List<dynamic> lsDate = db.Fetch<dynamic>(qGetTransDate, ordData.First().OrderNo);
                                                if (lsDate.Count > 0)
                                                {
                                                    trnsDate = lsDate.First().StartDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                    exprDate = lsDate.First().EndDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                                                    string cSQLGetTotalBayarVA = @";WITH cte
                                                                                        AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                                                    a.VI_TraceNo
                                                                                            FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                                                            WHERE    b.VI_VANumber IN (
                                                                                                        SELECT  value
                                                                                                        FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                                                        )
                                                                                        --AND a.PermataVirtualAccountPaymentSourceCode = '02'
		                                                                                AND a.VI_TransactionDate BETWEEN @1 AND @2
                                                                                            )
                                                                                SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                                                        SUM(pvas.VI_Amount) AS TotalBayar ,
                                                                                        MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                                                        MAX(pva.BillAmount) AS TotalBill
                                                                                FROM    cte
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                                                            AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                                                        INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                                                                WHERE   IsReversal = 0";
                                                    List<dynamic> ls = BYNDdb.Fetch<dynamic>(cSQLGetTotalBayarVA, VaNo, trnsDate, exprDate);
                                                    if (ls.Count > 0)
                                                    {
                                                        decimal TotalBayar = ls.First().TotalBayar == null ? 0 : Convert.ToDecimal(ls.First().TotalBayar);
                                                        if (TotalBayar == 0)
                                                        {
                                                            if (!ordDt.First().IsRenewal)
                                                            {
                                                                BYNDdb.Execute(qUpdateVirtualAccountNo, VaNo);
                                                                //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                            }
                                                            else
                                                            {
                                                                BYNDdb.Execute(qUpdateAAB2000VA
                                                                    , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                            }
                                                            listDyn.First().VANumber = "";
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!ordDt.First().IsRenewal)
                                                        {
                                                            BYNDdb.Execute(qUpdateVirtualAccountNo, VaNo);
                                                            //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                        }
                                                        else
                                                        {
                                                            BYNDdb.Execute(qUpdateAAB2000VA
                                                                , VaNo, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                                        }
                                                        listDyn.First().VANumber = "";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(ordData.First().VANumber))
                                        {
                                            if (!ordData.First().IsRenewal)
                                            {
                                                List<dynamic> lists = BYNDdb.Fetch<dynamic>("SELECT RowStatus FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                    , ordData.First().VANumber);
                                                if (lists.Count > 0)
                                                {
                                                    DateTime dtF = BYNDdb.ExecuteScalar<DateTime>("SELECT ValidFrom FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                    , ordData.First().VANumber);
                                                    DateTime dtT = BYNDdb.ExecuteScalar<DateTime>("SELECT ValidTo FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                                    , ordData.First().VANumber);
                                                    if (dtF.Date.AddDays(7) > DateTime.Now.Date && dtT.Date == dtF.Date)
                                                    {
                                                        BYNDdb.Execute(@";DECLARE @@ID BIGINT
                                                    DECLARE @@SoID VARCHAR(16)
													SELECT @@ID=VirtualAccountNumberID, @@SoID = SalesOfficerID
                                                    FROM Finance.VirtualAccountNumber a
                                                    INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
                                                    ON os.VANumber = a.VirtualAccountNumber
                                                    WHERE a.VirtualAccountNumber = @0

                                                    UPDATE Finance.VirtualAccountNumber 
                                                    SET ValidTo = ValidFrom+7, ModifiedBy = @@SoID, ModifiedDate = GETDATE()
                                                    WHERE VirtualAccountNumberID = @@ID"
                                                        , ordData.First().VANumber);
                                                        AddVA(ordData.First().OrderNo);
                                                    }
                                                }
                                                //db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                            }
                                            else
                                            {
                                                BYNDdb.Execute(@";DECLARE @@ID BIGINT
                                                DECLARE @@PeriodTo DATETIME
												DECLARE @@SoId VARCHAR(16)
                                                SELECT @@ID=AAB2000VirtualAccountID, @@PeriodTo=os.PeriodTo, @@SoId = SalesOfficerID
                                                FROM Finance.AAB2000VirtualAccount a
                                                INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
                                                ON os.VANumber = a.VirtualAccountNo
                                                WHERE VirtualAccountNo = @0 AND a.PolicyNo = @1

                                                UPDATE Finance.AAB2000VirtualAccount 
                                                SET PolicyPeriodTo = @@PeriodTo, ModifiedBy = @@SoId, ModifiedDate = GETDATE()
                                                WHERE AAB2000VirtualAccountID = @@ID"
                                                , ordData.First().VANumber, ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                                            } 
                                        }
                                        query = @"SELECT OrderNo,VANumber,OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1";
                                        List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                        if (ordData.Count > 0)
                                        {
                                            bool isGenerate = true;
                                            VaNumber = Convert.ToString(ordDt.First().VANumber);
                                            if (string.IsNullOrEmpty(VaNumber))
                                            {
                                                if (!string.IsNullOrEmpty(ordDt.First().OldPolicyNo))
                                                {
                                                    query = @";SELECT CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal FROM dbo.OrderSimulation os 
                                                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
                                                            WHERE os.OrderNo = @0";
                                                    List<dynamic> lsRen = db.Fetch<dynamic>(query, ordDt.First().OrderNo);
                                                    if (lsRen.Count > 0)
                                                    {
                                                        isGenerate = !lsRen.First().IsRenewal;
                                                    }
                                                    query = @";SELECT Cust_Type FROM dbo.Mst_Order mo
                                                            INNER JOIN dbo.Mst_Customer mc 
                                                            ON mo.Payer_Code = mc.Cust_Id
                                                            WHERE Policy_No = @0";
                                                    lsRen = AABdb.Fetch<dynamic>(query, ordDt.First().OldPolicyNo);
                                                    if (lsRen.Count > 0)
                                                    {
                                                        if (Convert.ToInt32(lsRen.First().Cust_Type) == 2)
                                                        {
                                                            db.Execute(@"UPDATE dbo.ProspectCustomer SET IsPayerCompany = 1 WHERE CustID = @0", CustID);
                                                        }
                                                    }
                                                }
                                                if (isGenerate)
                                                {
                                                    if (ordData.First().PeriodFrom != null && ordData.First().PeriodTo != null) {
                                                        VaNumber = GetBookingVANo();
                                                        query = @"UPDATE dbo.OrderSimulation 
                                                                SET VANumber=@2
                                                                WHERE CustID = @0 AND FollowUpNo = @1";
                                                        db.Execute(query, CustID, FollowUpNo, VaNumber);
                                                        AddVA(ordData.First().OrderNo);                                                    
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            string pPermata = AppParameterAccess.GetApplicationParametersValue("PREFIXVAPERMATA").First();
                            string pMandiri = AppParameterAccess.GetApplicationParametersValue("PREFIXVAMANDIRI").First();
                            string pBCA = AppParameterAccess.GetApplicationParametersValue("PREFIXVABCA").First();
                            VaNumber = string.IsNullOrEmpty(Convert.ToString(listDyn.First().VANumber)) ? VaNumber : Convert.ToString(listDyn.First().VANumber);
                            if (!string.IsNullOrEmpty(VaNumber))
                            {
                                pf.VAPermata = pPermata + VaNumber;
                                pf.VAMandiri = pMandiri + VaNumber;
                                pf.VABCA = pBCA + VaNumber;
                            }
                        }
                        pf.BuktiBayar = listDyn.First().BuktiBayar;
                        pf.BuktiBayarData = listDyn.First().BuktiBayarData;
                        pf.BuktiBayar2 = listDyn.First().BuktiBayar2;
                        pf.BuktiBayar2Data = listDyn.First().BuktiBayar2Data;
                        pf.BuktiBayar3 = listDyn.First().BuktiBayar3;
                        pf.BuktiBayar3Data = listDyn.First().BuktiBayar3Data;
                        pf.BuktiBayar4 = listDyn.First().BuktiBayar4;
                        pf.BuktiBayar4Data = listDyn.First().BuktiBayar4Data;
                        pf.BuktiBayar5 = listDyn.First().BuktiBayar5;
                        pf.BuktiBayar5Data = listDyn.First().BuktiBayar5Data;
                        string PolicyOrderNo = Convert.ToString(listDyn.First().PolicyOrderNo);
                        if (!string.IsNullOrEmpty(PolicyOrderNo))
                        {
                            query = @"SELECT DATEADD(DAY,13,Order_Date) AS DueDate FROM dbo.Mst_Order where Order_No = @0";
                            List<dynamic> listDate = AABdb.Fetch<dynamic>(query, PolicyOrderNo);
                            if (listDate.Count > 0)
                            {
                                pf.DueDate = listDate.First().DueDate;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ordData.First().ProductCode) && !string.IsNullOrEmpty(VaNumber))
                            {
                                decimal gp = AABdb.ExecuteScalar<decimal>("SELECT COALESCE(grace_period,0) FROM dbo.Mst_Product WHERE Product_Code = @0", ordData.First().ProductCode);
                                if (gp > 0)
                                {
                                    query = @"SELECT DATEADD(DAY,grace_period,os.EntryDate) DueDate
                                            FROM dbo.OrderSimulation os
                                            INNER JOIN BEYONDREPORT.AAB.dbo.Mst_Product mp
                                            ON os.ProductCode = mp.Product_Code
                                            WHERE os.OrderNo = @0";
                                    List<dynamic> listDate = db.Fetch<dynamic>(query, ordData.First().OrderNo);
                                    if (listDate.Count > 0)
                                    {
                                        pf.DueDate = listDate.First().DueDate;
                                    }
                                }
                            }
                        }
                        pf.IsVAActive = null;
                        if (!string.IsNullOrEmpty(VaNumber) && string.IsNullOrEmpty(pf.RekeningPenampung) && string.IsNullOrEmpty(ordData.First().PolicyOrderNo))
                        {
                            listDyn = BYNDdb.Fetch<dynamic>("SELECT ValidTo, RowStatus FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0"
                                , VaNumber);
                            if (listDyn.Count > 0)
                            {
                                int isReactive = db.ExecuteScalar<int>("SELECT COALESCE(isVAReactive,0) isVAreactive FROM dbo.OrderSimulation WHERE OrderNo = @0"
                                    , ordData.First().OrderNo);
                                if (isReactive == 0)
                                {
                                    DateTime validto = Convert.ToDateTime(listDyn.First().ValidTo);
                                    if (listDyn.First().RowStatus == 0 && (validto.Date>=DateTime.Now.Date))
                                    {
                                        pf.IsVAActive = true;
                                    }
                                    else
                                    {
                                        pf.IsVAActive = false;
                                    }
                                }
                            }
                        }
                        pf.VANo = VaNumber;
                    }
                }
                return pf;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                throw e;
            }
        }

        public static bool ReactiveVA(string VANo, string PolicyOrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(actionName + " Function start : ");
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
                var mbldb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                db.Execute(@";DECLARE @@ID BIGINT
DECLARE @@SoId VARCHAR(20)
SELECT @@ID=VirtualAccountNumberID, @@SoId = SalesOfficerID
FROM Finance.VirtualAccountNumber a
INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os
ON os.VANumber = a.VirtualAccountNumber
WHERE VirtualAccountNumber = @0

UPDATE Finance.VirtualAccountNumber 
SET ValidFrom = DATEADD(day, DATEDIFF(day, '19000101', GETDATE()), '19000101'),
ValidTo = DATEADD(day, DATEDIFF(day, '19000101', GETDATE()+7), '19000101'), ModifiedBy = @@SoId,
ModifiedDate = GETDATE()
WHERE VirtualAccountNumberID = @@ID", VANo);
                mbldb.Execute(@";UPDATE dbo.OrderSimulation SET isVAReactive = 1 WHERE VANumber = @0", VANo);
                string OrderNo = mbldb.ExecuteScalar<string>("SELECT OrderNo FROM dbo.OrderSimulation WHERE VANumber = @0 AND RowStatus = 1 AND ApplyF = 1",VANo);
                AddVA(OrderNo);
                return true;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                return false;
            }
        }

        public static bool CheckVAPayment(string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                logger.Debug(actionName + " Function start : ");
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
                var mbldb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string trnsDate = "";
                string exprDate = "";

                string qGetTransDate = @"SELECT EntryDate StartDate, DATEADD(DAY,CAST(ParamValue AS INT),EntryDate) EndDate, VANumber FROM dbo.OrderSimulation os
	                                                                    INNER JOIN dbo.ApplicationParameters ap ON 1=1 AND ParamName = 'END-CHECKVAPAYMENT'
	                                                                    WHERE OrderNo = @0";
                List<dynamic> lsDate = mbldb.Fetch<dynamic>(qGetTransDate, OrderNo);
                if (lsDate.Count > 0)
                {
                    if (!string.IsNullOrEmpty(lsDate.First().VANumber) && !string.IsNullOrWhiteSpace(lsDate.First().VANumber)) {
                        trnsDate = lsDate.First().StartDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                        exprDate = lsDate.First().EndDate.ToString("yyyy-MM-dd HH:mm:ss.ff");
                        string cSQLGetTotalBayarVA = @";WITH cte
                                                        AS ( SELECT PermataVirtualAccountSettlementID ,
                                                                    a.VI_TraceNo
                                                            FROM     PolicyInsurance.PermataVirtualAccountSettlement a
                                                                    INNER JOIN PolicyInsurance.PermataVirtualAccount b ON a.PermataVirtualAccountID = b.PermataVirtualAccountID
                                                            WHERE    b.VI_VANumber IN (
                                                                        SELECT  value
                                                                        FROM    PolicyInsurance.fn_Split(@0, ',') 
                                                                        )
                                                        --AND a.PermataVirtualAccountPaymentSourceCode = '02'
		                                                AND a.VI_TransactionDate BETWEEN @1 AND @2
                                                            )
                                                SELECT  MAX(pvas.PermataVirtualAccountSettlementID) as PermataVirtualAccountSettlementID,
                                                        SUM(pvas.VI_Amount) AS TotalBayar ,
                                                        MAX(pvas.VI_TransactionDate) AS MaxTransactionDate ,
                                                        MAX(pva.BillAmount) AS TotalBill
                                                FROM    cte
                                                        INNER JOIN PolicyInsurance.PermataVirtualAccountSettlement AS pvas ON pvas.PermataVirtualAccountSettlementID = cte.PermataVirtualAccountSettlementID
                                                                                                            AND pvas.VI_TraceNo = cte.VI_TraceNo
                                                        INNER JOIN PolicyInsurance.PermataVirtualAccount AS pva ON pva.PermataVirtualAccountID = pvas.PermataVirtualAccountID
                                                WHERE   IsReversal = 0";
                        List<dynamic> ls = db.Fetch<dynamic>(cSQLGetTotalBayarVA, lsDate.First().VANumber, trnsDate, exprDate);
                        if (ls.Count > 0)
                        {
                            decimal TotalBayar = ls.First().TotalBayar == null ? 0 : Convert.ToDecimal(ls.First().TotalBayar);
                            if (TotalBayar > 0)
                            {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                return false;
            }
        }

        #region private
        public static string GetBookingVANo()
        {
            string qGetVANo = @";DECLARE @@VANumber VARCHAR(MAX)
                                EXEC usp_GetVANumber @@VANumber OUTPUT
                                SELECT @@VANumber VirtualAccountNo";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                var result = db.Query<dynamic>(qGetVANo).FirstOrDefault();
                return result.VirtualAccountNo;
            }
        }
        public static bool AddVA(string OrderNo) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            var mbldDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var byndDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
            try
            {
                #region query
                string qInsertUpdate = @";IF EXISTS(SELECT * FROM Finance.VirtualAccountNumber WHERE VirtualAccountNumber = @0 AND RowStatus = 0)
BEGIN 
	UPDATE Finance.VirtualAccountNumber SET PolicyType = @1, PolicyNumber = @2, PolicyPeriodFrom = @3,
	PolicyPeriodTo = @4, AccountManager = @5, AccountManagerName = @6, SAAdmin = @7, SAAdminName = @8,
	PICMobileNumber = @9, CustomerName = @10, IDType = @11, IDNumber = @12, Remarks = @13,
	ValidTo = ValidFrom+7, IsNew = @16, ModifiedBy = @17, ModifiedDate = GETDATE() WHERE VirtualAccountNumber = @0
	AND RowStatus = 0
END
ELSE
BEGIN
	INSERT INTO Finance.VirtualAccountNumber
	        ( VirtualAccountNumber ,
	          PolicyType ,
	          PolicyNumber ,
	          PolicyPeriodFrom ,
	          PolicyPeriodTo ,
	          AccountManager ,
	          AccountManagerName ,
	          SAAdmin ,
	          SAAdminName ,
	          PICMobileNumber ,
	          CustomerName ,
	          IDType ,
	          IDNumber ,
	          Remarks ,
	          ValidFrom ,
	          ValidTo ,
	          IsNew ,
			  Ispay,
			  IsApprove,
	          CreatedBy ,
	          CreatedDate ,
	          RowStatus
	        )
	VALUES  ( @0 , -- VirtualAccountNumber - varchar(50)
	          @1 , -- PolicyType - varchar(15)
	          @2 , -- PolicyNumber - char(16)
	          @3 , -- PolicyPeriodFrom - datetime
	          @4 , -- PolicyPeriodTo - datetime
	          @5 , -- AccountManager - char(5)
	          @6 , -- AccountManagerName - varchar(100)
	          @7 , -- SAAdmin - char(5)
	          @8 , -- SAAdminName - varchar(100)
	          @9 , -- PICMobileNumber - char(50)
	          @10 , -- CustomerName - varchar(100)
	          @11 , -- IDType - varchar(30)
	          @12 , -- IDNumber - varchar(50)
	          @13 , -- Remarks - text
	          @14 , -- ValidFrom - datetime
	          @15 , -- ValidTo - datetime
	          @16 , -- IsNew - bit
	          0 , -- IsPay - bit
	          0 , -- IsApprove - bit
	          @17 , -- CreatedBy - varchar(50)
	          GETDATE() , -- CreatedDate - datetime
	          0  -- RowStatus - smallint
	        )	
END";
                #endregion
                PreBookVAModel VA = mbldDB.Query<PreBookVAModel>(@";EXEC usp_GetPreBookVATransactionOtosales @0",OrderNo).FirstOrDefault();
                if (VA != null) {
                    if (VA.PolicyPeriodFrom != null && VA.PolicyPeriodTo != null) {
                        byndDB.Execute(qInsertUpdate
                            , VA.VirtualAccountNumber
                            , VA.PolicyType
                            , VA.PolicyNumber
                            , VA.PolicyPeriodFrom
                            , VA.PolicyPeriodTo
                            , VA.AccountManager
                            , VA.AccountManagerName
                            , VA.SAAdmin
                            , VA.SAAdminName
                            , VA.PICMobileNumber
                            , VA.CustomerName
                            , VA.IDType
                            , VA.IDNumber
                            , VA.Remarks
                            , VA.ValidFrom
                            , VA.ValidTo
                            , VA.IsNew
                            , VA.CreatedBy);
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                throw e;
            }
        }
        #endregion
    }
}