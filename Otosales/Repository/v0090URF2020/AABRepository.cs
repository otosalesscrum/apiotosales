﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace Otosales.Repository.v0090URF2020
{
    public interface IAABRepository {
        Models.OtosalesAPIResult PopulateOrderApproval(string userID, int ReqType = 1);
        Models.OtosalesAPIResult detailApprovalComission(string OrderID, string Role, string OrderNumber);
        Models.OtosalesAPIResult detailApprovalAdjustment(string OrderID, string OrderNumber);
        Models.OtosalesAPIResult detailApprovalTsiLimit(string OrderID, string OrderNumber);
        Models.OtosalesAPIResult detailApprovalPayment(string OrderID, string OrderNumber);
        void InsertUpdatePhoneNo(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam param, string custid);
        void InsertUpdatePhoneNoOrderNew(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam param);
        Models.vWeb2.PhoneNoModel GetClaimeePhone(string OldPolicyNo, string custid);
    }
    public class AABRepository : IAABRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();
        
        #region private query
        private static string qUpdatePhoneCompanyABB = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @2 AND CustID = @0)
                                                    BEGIN
	                                                    UPDATE dbo.Mst_Customer_AdditionalInfo SET AdditionalInfoValue = @1 WHERE AdditionalCode = @2 AND CustID = @0
                                                    END
                                                    ELSE
                                                    BEGIN
	                                                    INSERT INTO dbo.Mst_Customer_AdditionalInfo
	                                                            ( CustID ,
	                                                              AdditionalCode ,
	                                                              AdditionalInfoValue ,
	                                                              CreatedBy ,
	                                                              CreatedDate ,
	                                                              RowStatus
	                                                            )
	                                                    VALUES  ( @0 , -- CustID - char(11)
	                                                              @2 , -- AdditionalCode - varchar(5)
	                                                              @1 , -- AdditionalInfoValue - varchar(255)
	                                                              'OTOSL' , -- CreatedBy - varchar(50)
	                                                              GETDATE() , -- CreatedDate - datetime
	                                                              0  -- RowStatus - smallint
	                                                            )
                                                    END";
        private static string qUpdatePhoneCompanyProspectABB = @";IF EXISTS(SELECT * FROM dbo.Mst_Prospect_AdditionalInfo WHERE AdditionalCode = @2 AND ProspectID = @0)
                                                    BEGIN
	                                                    UPDATE dbo.Mst_Prospect_AdditionalInfo SET AdditionalInfoValue = @1 WHERE AdditionalCode = @2 AND ProspectID = @0
                                                    END
                                                    ELSE
                                                    BEGIN
	                                                    INSERT INTO dbo.Mst_Prospect_AdditionalInfo
	                                                            ( ProspectID ,
	                                                              AdditionalCode ,
	                                                              AdditionalInfoValue ,
	                                                              CreatedBy ,
	                                                              CreatedDate ,
	                                                              RowStatus
	                                                            )
	                                                    VALUES  ( @0 , -- CustID - char(11)
	                                                              @2 , -- AdditionalCode - varchar(5)
	                                                              @1 , -- AdditionalInfoValue - varchar(255)
	                                                              'OTOSL' , -- CreatedBy - varchar(50)
	                                                              GETDATE() , -- CreatedDate - datetime
	                                                              0  -- RowStatus - smallint
	                                                            )
                                                    END";
        private static string qInsertUpdateValidation = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND App_Source = 'otosales')
                                    BEGIN
	                                    UPDATE dbo.Mst_Customer_Data_Validation SET Status = @2, RelationshipId = @4, PICName = @5,
										CallRejectReasonId = @8, Verified_Date = @7, Verified_By = @3, Created_Date = GETDATE()
	                                    WHERE Cust_Id = @0 AND Column_Name = @1 AND App_Source = 'otosales'
                                    END
                                    ELSE
                                    BEGIN
	                                    INSERT INTO dbo.Mst_Customer_Data_Validation
	                                            ( Cust_Id ,
	                                                Column_Name ,
	                                                App_Source ,
	                                                Status ,
	                                                Verified_By ,
	                                                Verified_Date,
													RelationshipId,
													PICName,
													Created_By,
													Created_Date,
													CallRejectReasonId 
	                                            )
	                                    VALUES  ( @0 , -- Cust_Id - char(11)
	                                                @1 , -- Column_Name - varchar(5)
	                                                'otosales' , -- App_Source - char(10)
	                                                @2 , -- Status - nchar(1)
	                                                @3 , -- Verified_By - char(5)
	                                                @7,  -- Verified_Date - datetime
													@4,
													@5,
													@6,
													GETDATE(),
													@8
	                                            )
                                    END";
        #endregion

        public Models.OtosalesAPIResult PopulateOrderApproval(string userID, int ReqType = 1) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Models.OtosalesAPIResult response = new Models.OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    // dynamic res = db.Fetch<dynamic>(@";EXEC usp_OtosalesGetApprovalList @0", userID);
                    dynamic res = db.Fetch<dynamic>(@";EXEC usp_GetOtosalesApprovalList @0, @1", userID , ReqType);
                    dynamic result = new ExpandoObject();
                    if (ReqType == 1)
                    {
                        result.List = res;
                        result.Count = res.Count;
                    }
                    else if (ReqType == 2)
                    {
                        result.List = null;
                        result.Count = res[0].ApprovalCount;
                    }
                    response.Status = true;
                    response.Data = result;
                    response.ResponseCode = "1";
                    response.Message = "Success!";
                }
                return response;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public Models.OtosalesAPIResult detailApprovalComission(string OrderID, string Role, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");

            switch (Role)
            {
                case "AO":
                    {
                        return detailComissionAO(OrderID, OrderNumber);
                    }
                case "MGO":
                    {
                        return detailComissionMGO(OrderID, OrderNumber);
                    }
                default:
                    {
                        throw new Exception("Invalid User!!!");
                    }
            }
        }
        public Models.OtosalesAPIResult detailApprovalAdjustment(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            Models.OtosalesAPIResult resp = new Models.OtosalesAPIResult();

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    string query = @";SELECT IIF(','+mom.Reason+',' like '%,PSTB,%',1, 0) as isPTSB, COALESCE(os.Remarks, '') as Remarks, '' as SendToBranch, '' as SendToName, 
                    '' as SendToAddress, '' as SendToPostal, msom.Order_ID [Order_ID],
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    , so.Name MarketingName, mb.Nama BranchName
					FROM [dbo].[Mst_Order] mo
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN [dbo].[Mst_Order_Mobile_Approval] mom ON mom.Order_No = mo.Order_No AND mom.ApprovalType = 'ADJUST'
                    LEFT JOIN BeyondMoss.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
                    LEFT JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = mo.Salesman_Id
					LEFT JOIN dbo.Mst_Branch mb ON mb.Branch_id = mo.Branch_Id
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    AND mo.Order_No = @0
					--AND msom.Order_ID = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY mo.Order_No, msom.Order_ID, mom.Reason, os.Remarks, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    ,so.Name, mb.Nama
					ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC , Order_ID DESC";

                    resp.Status = true;
                    resp.Message = "Success from detailApprovalAdjustment";
                    //resp.Data = db.FirstOrDefault<dynamic>(query, OrderID);
                    resp.Data = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    resp.ResponseCode = "1";
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
                //return Json(new { status = false, message = e.Message, data = "null" });
            }

            return resp;
        }
        public Models.OtosalesAPIResult detailApprovalPayment(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            Models.OtosalesAPIResult resp = new Models.OtosalesAPIResult();

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    string query = @";SELECT 0 as isPTSB, COALESCE(os.Remarks, '') as Remarks, '' as SendToBranch, '' as SendToName, 
                    '' as SendToAddress, '' as SendToPostal, msom.Order_ID [Order_ID],
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    , so.Name MarketingName, mb.Nama BranchName, IIF(COUNT(oa.OrderNo)=0,0,1) isInstallment
					FROM [dbo].[Mst_Order] mo
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN [dbo].[Mst_Order_Mobile_Approval] mom ON mom.Order_No = mo.Order_No AND mom.ApprovalType = 'ADJUST'
                    INNER JOIN BeyondMoss.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
					INNER JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulationApproval oa ON os.OrderNo = oa.OrderNo AND oa.RowStatus = 1
                    LEFT JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = mo.Salesman_Id
					LEFT JOIN dbo.Mst_Branch mb ON mb.Branch_id = mo.Branch_Id
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    AND mo.Order_No = @0
                    AND oa.ApprovalStatus = 0 
                    GROUP BY mo.Order_No, msom.Order_ID, mom.Reason, os.Remarks, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    ,so.Name, mb.Nama
					ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC , Order_ID DESC";

                    resp.Status = true;
                    resp.Message = "Success from detailApprovalPayment";
                    //resp.Data = db.FirstOrDefault<dynamic>(query, OrderID);
                    resp.Data = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    resp.ResponseCode = "1";
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
                //return Json(new { status = false, message = e.Message, data = "null" });
            }

            return resp;
        }
        public Models.OtosalesAPIResult detailApprovalTsiLimit(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            Models.OtosalesAPIResult resp = new Models.OtosalesAPIResult();

            #region Query
            string qGetOrderNum = @";SELECT Order_No FROM Mst_Order_Mobile WHERE Order_ID = @0";
            string qGetFromGen5SP = @";EXEC sp_Execute_Order_Analysis_0069URF2018 @0";
            #endregion

            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    string orderNumber = db.FirstOrDefault<string>(qGetOrderNum, OrderID);
                    if (orderNumber.Equals("") || orderNumber == null)
                    { // untuk order bukan dari otosales
                        orderNumber = OrderNumber;
                    }

                    dynamic data = db.FirstOrDefault<dynamic>(qGetFromGen5SP, OrderNumber);
                    if (data == null)
                    {
                        resp.Status = false;
                        resp.Message = "Order Not Exists!";
                        resp.ResponseCode = "6661";
                    }
                    else
                    {
                        resp.Data = data.Info_Text;
                        resp.Status = true;
                        resp.Message = "Success";
                        resp.ResponseCode = "1";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                resp.Status = false;
                resp.Message = ex.Message.ToString();
                resp.ResponseCode = "666";
            }

            return resp;
        }

        public void InsertUpdatePhoneNo(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam param, string custid)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelectOrd = @"SELECT CAST(COALESCE(isCompany,0) AS BIT) isCompany, os.CustID, COALESCE(pc.CustIDAAB,'') CustIDAAB 
								    FROM dbo.OrderSimulation os
                                    INNER JOIN dbo.ProspectCustomer pc
                                    ON pc.CustID = os.CustID
                                    WHERE os.OrderNo = @0 AND os.ApplyF = 1 AND os.Rowstatus = 1";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(qSelectOrd, param.OrderNo);
                if (ordData.Count > 0)
                {
                    if (!string.IsNullOrEmpty(custid))
                    {
                        ordData.First().CustIDAAB = custid;
                    }
                    string qUpdatePhoneABB = @"";

                    if (ordData.First().isCompany)
                    {
                        //param.PhoneNoParam = RemoveDuplicatePhone(param, param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID, ordData.First().isCompany);

                        string qSelectCompanyHP = @"SELECT AdditionalInfoValue NoHP, 'MPC1' ColumnName 
                                                        FROM dbo.Mst_Customer mc
														LEFT JOIN dbo.Mst_Customer_AdditionalInfo mca
														ON mc.Cust_Id = mca.CustID
                                                        WHERE AdditionalCode = 'MPC1' AND mc.Cust_Id = @0
                                                        UNION
                                                        SELECT AdditionalInfoValue NoHP, 'MPC2' ColumnName 
                                                        FROM dbo.Mst_Customer mc
														LEFT JOIN dbo.Mst_Customer_AdditionalInfo mca
														ON mc.Cust_Id = mca.CustID
                                                        WHERE AdditionalCode = 'MPC2' AND mc.Cust_Id = @0";
                        List<dynamic> lsPhoneNo = db.Fetch<dynamic>(qSelectCompanyHP, ordData.First().CustIDAAB);
                        List<string> listAvailColName = new List<string>();
                        foreach (dynamic no in lsPhoneNo)
                        {
                            if (string.IsNullOrEmpty(no.NoHP))
                            {
                                listAvailColName.Add(no.ColumnName);
                            }
                        }
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].IsWrongNumber)
                            {
                                SetWrongNumberCompany(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                            }
                            else if (param.PhoneNoParam[i].IsLandline && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                dynamic dateVerified = null;
                                if (param.PhoneNoParam[i].IsConnected)
                                {
                                    dateVerified = DateTime.Now;
                                }
                                db.Execute(qInsertUpdateValidation
                                    , ordData.First().CustIDAAB
                                    , "Office_Phone1"
                                    , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                    , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    , null
                                    , ""
                                    , param.SalesOfficerID
                                    , dateVerified
                                    , param.PhoneNoParam[i].CallRejectReasonId);
                            }
                            else
                            {
                                if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    UpdatePrimaryCompany(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                                }
                                else
                                {
                                    if (!param.PhoneNoParam[i].IsWrongNumber && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                                    {
                                        if (listAvailColName.Count > 0)
                                        {
                                            db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, listAvailColName[0]);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(@";DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )"
                                                , ordData.First().CustIDAAB
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , listAvailColName[0]
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , null
                                                , ""
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            if (listAvailColName[0].Contains("MPC1"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedBy = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            }
                                            listAvailColName.RemoveAt(0);
                                        }
                                        else
                                        {
                                            InsertCustHistory(ordData.First().CustIDAAB, "MPC2", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "", "");
                                            db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC2");
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(@";DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )"
                                                , ordData.First().CustIDAAB
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , "MPC2"
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , null
                                                , ""
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                        }
                                    }
                                    else
                                    {
                                        List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
                                        if (isverified.Count > 0)
                                        {
                                            param.PhoneNoParam[i].IsConnected = true;
                                        }
                                        dynamic dateVerified = null;
                                        if (param.PhoneNoParam[i].IsConnected)
                                        {
                                            dateVerified = DateTime.Now;
                                        }
                                        db.Execute(qInsertUpdateValidation
                                            , ordData.First().CustIDAAB
                                            , param.PhoneNoParam[i].ColumnName
                                            , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                            , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                            , param.PhoneNoParam[i].Relation
                                            , param.PhoneNoParam[i].PICName
                                            , param.SalesOfficerID
                                            , dateVerified
                                            , param.PhoneNoParam[i].CallRejectReasonId);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //param.PhoneNoParam = RemoveDuplicatePhone(param, param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID, ordData.First().isCompany);
                        string qSelectPersonalHP = @"SELECT NoHP, ColumnName FROM (
                                                    SELECT HP, HP_2, HP_3, HP_4 FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0
                                                    ) a
                                                    UNPIVOT (
	                                                    NoHP FOR ColumnName IN (
		                                                    a.HP, a.HP_2, a.HP_3, a.HP_4
	                                                    )
                                                    ) unpvt";
                        List<dynamic> lsPhoneNo = db.Fetch<dynamic>(qSelectPersonalHP, ordData.First().CustIDAAB);
                        List<string> listAvailColName = new List<string>();
                        foreach (dynamic no in lsPhoneNo)
                        {
                            if (string.IsNullOrEmpty(no.NoHP))
                            {
                                listAvailColName.Add(no.ColumnName);
                            }
                        }
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].IsWrongNumber)
                            {
                                SetWrongNumberPersonal(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                            }
                            else if (param.PhoneNoParam[i].IsLandline && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                dynamic dateVerified = null;
                                if (param.PhoneNoParam[i].IsConnected)
                                {
                                    dateVerified = DateTime.Now;
                                }
                                db.Execute(qInsertUpdateValidation
                                    , ordData.First().CustIDAAB
                                    , "Home_Phone1"
                                    , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                    , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    , null
                                    , ""
                                    , param.SalesOfficerID
                                    , dateVerified
                                    , param.PhoneNoParam[i].CallRejectReasonId);
                            }
                            else
                            {
                                if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    UpdatePrimaryPersonal(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                                }
                                else
                                {
                                    if (!param.PhoneNoParam[i].IsWrongNumber && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                                    {
                                        if (listAvailColName.Count > 0)
                                        {
                                            qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                            db.Execute(string.Format(qUpdatePhoneABB, listAvailColName[0])
                                                , ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , listAvailColName[0]
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , param.PhoneNoParam[i].Relation
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            if (listAvailColName[0].Equals("HP"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                            }
                                            else if (listAvailColName[0].Equals("HP_2"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone2 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                            }
                                            listAvailColName.RemoveAt(0);
                                        }
                                        else
                                        {
                                            InsertCustHistory(ordData.First().CustIDAAB, "HP_2", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "", "");
                                            qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET HP_2 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                            db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , "HP_2"
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , param.PhoneNoParam[i].Relation
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone2 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                            mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                        }
                                    }
                                    else
                                    {
                                        List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
                                        if (isverified.Count > 0)
                                        {
                                            param.PhoneNoParam[i].IsConnected = true;
                                        }
                                        dynamic dateVerified = null;
                                        if (param.PhoneNoParam[i].IsConnected)
                                        {
                                            dateVerified = DateTime.Now;
                                        }
                                        db.Execute(qInsertUpdateValidation
                                            , ordData.First().CustIDAAB
                                            , param.PhoneNoParam[i].ColumnName
                                            , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                            , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                            , param.PhoneNoParam[i].Relation
                                            , param.PhoneNoParam[i].PICName
                                            , param.SalesOfficerID
                                            , dateVerified
                                            , param.PhoneNoParam[i].CallRejectReasonId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace + "," + e.ToString());
                throw e;
            }
        }

        public void InsertUpdatePhoneNoOrderNew(Otosales.Models.vWeb2.InsertUpdatePhoneNoParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelectOrd = @"SELECT CAST(COALESCE(isCompany,0) AS BIT) isCompany, os.CustID, COALESCE(pc.CustIDAAB,'') CustIDAAB, 
									COALESCE(ProspectID,'') ProspectID, os.SalesOfficerID, LOWER(COALESCE(pc.Name,'')) Name
								    FROM dbo.OrderSimulation os
                                    INNER JOIN dbo.ProspectCustomer pc
                                    ON pc.CustID = os.CustID
                                    WHERE OrderNo = @0";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(qSelectOrd, param.OrderNo);
                if (ordData.Count > 0)
                {
                    string qUpdatePhoneABB = @"";
                    string qUpdatePhoneMobile = @"";

                    if (ordData.First().isCompany)
                    {
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].ColumnName.ToUpper().Equals("MPC1") || string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                if (param.PhoneNoParam[i].IsWrongNumber)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "MPC1", "", ordData.First().SalesOfficerID, "", "");
                                        db.Execute(@"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0", ordData.First().CustIDAAB);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "MPC1", "", ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        db.Execute(@"DELETE FROM dbo.Mst_Prospect_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0", ordData.First().CustIDAAB);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0
                                                        UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, "", ordData.First().SalesOfficerID);
                                }
                                else if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "MPC1", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@";DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )", ordData.First().CustIDAAB, param.PhoneNoParam[i].PICName
                                                                                         , ordData.First().SalesOfficerID);
                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "MPC1", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        db.Execute(qUpdatePhoneCompanyProspectABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@"UPDATE dbo.Mst_Prospect_Pic SET Name = @1, updatedt = GETDATE(), updateusr = @2
                                                    WHERE Prospect_Id = @0 AND Name = @3", ordData.First().ProspectID, param.PhoneNoParam[i].PICName
                                                                                             , ordData.First().SalesOfficerID, ordData.First().Name);
                                        }
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0
                                                        UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    mobiledb.Execute(@";UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0"
                                        , ordData.First().CustID, param.PhoneNoParam[i].PICName, ordData.First().SalesOfficerID);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@";DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )", ordData.First().CustIDAAB, param.PhoneNoParam[i].PICName
                                                                                         , ordData.First().SalesOfficerID);
                                        }
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        db.Execute(qUpdatePhoneCompanyProspectABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, "MPC1");
                                        if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName) && !param.PhoneNoParam[i].PICName.ToLower().Equals(ordData.First().Name))
                                        {
                                            db.Execute(@"UPDATE dbo.Mst_Prospect_Pic SET Name = @1, updatedt = GETDATE(), updateusr = @2
                                                    WHERE Prospect_Id = @0 AND Name = @3", ordData.First().ProspectID, param.PhoneNoParam[i].PICName
                                                                                             , ordData.First().SalesOfficerID, ordData.First().Name);
                                        }
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0
                                                        UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    mobiledb.Execute(@";UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0"
                                        , ordData.First().CustID, param.PhoneNoParam[i].PICName, ordData.First().SalesOfficerID);
                                }
                            }

                        }
                    }
                    else
                    {
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].ColumnName.ToUpper().Equals("HP") || string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                if (param.PhoneNoParam[i].IsWrongNumber)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", "", ordData.First().SalesOfficerID, "", "");
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, "", ordData.First().SalesOfficerID);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", "", ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Prospect_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Prospect_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().ProspectID, "", ordData.First().SalesOfficerID);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, "");
                                }
                                else if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Prospect_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Prospect_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ordData.First().CustIDAAB))
                                    {
                                        InsertCustHistory(ordData.First().CustIDAAB, "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", "");
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    else if (!string.IsNullOrEmpty(ordData.First().ProspectID) && !string.IsNullOrWhiteSpace(ordData.First().ProspectID))
                                    {
                                        //InsertCustHistory("", "HP", param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID, "", ordData.First().ProspectID);
                                        qUpdatePhoneABB = @";UPDATE dbo.Mst_Prospect_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Prospect_Id = @0";
                                        db.Execute(qUpdatePhoneABB, ordData.First().ProspectID, param.PhoneNoParam[i].PhoneNo, ordData.First().SalesOfficerID);
                                    }
                                    qUpdatePhoneMobile = @";UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                    mobiledb.Execute(qUpdatePhoneMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace + "," + e.ToString());
                throw e;
            }
        }

        public Models.vWeb2.PhoneNoModel GetClaimeePhone(string OldPolicyNo, string custid)
        {
            Otosales.Models.vWeb2.PhoneNoModel clmPhone = new Otosales.Models.vWeb2.PhoneNoModel();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string query = @";
SELECT * INTO #tempMCDV FROM (
SELECT Cust_Id, Status, Column_Name FROM dbo.Mst_Customer_Data_Validation mcdv
WHERE Cust_Id = @1 AND Column_Name = 'Claimee_Phone' 
EXCEPT
SELECT Cust_Id, Status, Column_Name FROM dbo.Mst_Customer_Data_Validation mcdv
WHERE Cust_Id = @1 AND Column_Name = 'Claimee_Phone' AND Status = 2) a

SELECT Claimee_Phone, Notes, lp.EntryDt, mcdv.status FROM dbo.Policy p 
                                    INNER JOIN dbo.Loss_Report lp
                                    ON lp.Policy_No = p.Policy_No
						            LEFT JOIN #tempMCDV mcdv
						            ON mcdv.Cust_Id = @1 AND Column_Name = 'Claimee_Phone' 
						            WHERE lp.Policy_No = @0
DROP TABLE #tempMCDV";
                List<dynamic> listClaimee = db.Fetch<dynamic>(query, OldPolicyNo, custid);
                List<dynamic> ClaimeePhones = new List<dynamic>();
                foreach (dynamic c in listClaimee)
                {
                    string notes = Convert.ToString(c.Notes);
                    dynamic mdl = new ExpandoObject();
                    if (notes.Contains("RelationshipInsured"))
                    {
                        List<string> noteList = notes.Split(';').ToList();
                        string relationship;
                        foreach (string s in noteList)
                        {
                            if (SafeTrim(s).StartsWith("RelationshipInsured"))
                            {
                                relationship = SafeTrim(s.Split('|').ToList()[1]).ToUpper();
                                if (relationship.Equals("KELUARGA", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mdl.NoHp = c.Claimee_Phone;
                                    mdl.Priority = 2;
                                    mdl.EntryDt = c.EntryDt;
                                    ClaimeePhones.Add(mdl);
                                }
                                else if (relationship.Equals("PEGAWAI", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mdl.NoHp = c.Claimee_Phone;
                                    mdl.Priority = 1;
                                    mdl.EntryDt = c.EntryDt;
                                    ClaimeePhones.Add(mdl);
                                }
                                else if (relationship.Equals("SUAMI", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mdl.NoHp = c.Claimee_Phone;
                                    mdl.Priority = 5;
                                    mdl.EntryDt = c.EntryDt;
                                    ClaimeePhones.Add(mdl);
                                }
                                else if (relationship.Equals("ISTRI", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mdl.NoHp = c.Claimee_Phone;
                                    mdl.Priority = 4;
                                    mdl.EntryDt = c.EntryDt;
                                    ClaimeePhones.Add(mdl);
                                }
                                else if (relationship.Equals("ANAK", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mdl.NoHp = c.Claimee_Phone;
                                    mdl.Priority = 3;
                                    mdl.EntryDt = c.EntryDt;
                                    ClaimeePhones.Add(mdl);
                                }
                                else if (relationship.Equals("SATU KK", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mdl.NoHp = c.Claimee_Phone;
                                    mdl.Priority = 6;
                                    mdl.EntryDt = c.EntryDt;
                                    ClaimeePhones.Add(mdl);
                                }
                                else if (relationship.Equals("TERTANGGUNG", StringComparison.InvariantCultureIgnoreCase))
                                {
                                    mdl.NoHp = c.Claimee_Phone;
                                    mdl.Priority = 7;
                                    mdl.EntryDt = c.EntryDt;
                                    ClaimeePhones.Add(mdl);
                                }
                            }
                        }
                    }
                }
                ClaimeePhones = ClaimeePhones.OrderByDescending(x => x.Priority).ThenByDescending(x => x.EntryDt).ToList();
                if (ClaimeePhones.Count > 0)
                {
                    clmPhone.NoHp = ClaimeePhones[0].NoHp;
                    clmPhone.Priority = 0;
                    clmPhone.HPStatus = string.IsNullOrEmpty(listClaimee.First().status) ? "0" : Convert.ToString(listClaimee.First().status);
                    clmPhone.Column_Name = "Claimee_Phone";
                }
                return clmPhone;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        #region Private
        public static string SafeTrim(string str)
        {
            return str != null ? str.Trim() : "";
        }
        private Models.OtosalesAPIResult detailComissionAO(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            Models.OtosalesAPIResult resp = new Models.OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    #region Query Select Data
                    string query = @";SELECT X.AgentCode as AgentCode, x.AgentName as AgentName, X.UplinerCode as UplinerCode, x.UplinerName as UplinerName,
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(msom.Order_ID)) as OrderID, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus, 
                    so.Name MarketingName, mb.Nama BranchName
					FROM [dbo].[Mst_Order] mo 
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN  ( SELECT b.Cust_Id AS CustomerID,
								a.Client_Code AS ClientCode,
								b.Name AS AgentName,
								a.Client_Code AS AgentCode,
                               a.Upliner_Client_Code AS UplinerCode ,
                               RTRIM(d.Name) AS UplinerName,
                               e.Cust_Id AS LeaderCustomerID,
                               a.Leader_Client_Code AS LeaderCode ,                  
                               RTRIM(f.Name) AS LeaderName                    
                                     FROM      dtl_cust_type a WITH ( NOLOCK )
                                               INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                               LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                               LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                               LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                               LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                     WHERE     a.Client_Type = 'AGENT') X ON X.CustomerID=mo.Broker_Code OR X.ClientCode=mo.Broker_Code
                    LEFT JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
					LEFT JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = mo.Salesman_Id
					LEFT JOIN dbo.Mst_Branch mb ON mb.Branch_id = mo.Branch_Id
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    --AND msom.Order_ID = @0
                    AND mo.Order_No = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY AgentCode, AgentName, UplinerCode, UplinerName, mo.Order_No, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Order_ID, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.NAME
					, so.Name, mb.Nama 
                    ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC, msom.Order_ID DESC";
                    #endregion

                    dynamic result = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    if (result == null)
                    {
                        resp.Status = false;
                        resp.Message = "Data Not Found!";
                        resp.ResponseCode = "6661";
                    }
                    else
                    {
                        resp.Status = true;
                        resp.Message = "Success from detailComissionAO";
                        resp.ResponseCode = "1";
                    }
                    resp.Data = result;
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
                //return Json(new { status = false, message = e.Message, data = "null" });
            }
            return resp;
        }
        private Models.OtosalesAPIResult detailComissionMGO(string OrderID, string OrderNumber)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            logger.Debug(actionName + " Function start : ");
            Models.OtosalesAPIResult resp = new Models.OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    #region Query Select Data
                    string query = @";SELECT X.AgentCode as AgentCode, x.AgentName as AgentName, X.UplinerCode as UplinerCode, x.UplinerName as UplinerName,
                    LTRIM(RTRIM(mo.Order_No)) as OrderNo, LTRIM(RTRIM(msom.Order_ID)) as OrderID, LTRIM(RTRIM(ms.Salesman_Id)) as SalesmanId, 
                    LTRIM(RTRIM(ms.Name)) as SalesmanName, LTRIM(RTRIM(dlr.Dealer)) as Dealer, 
                    LTRIM(RTRIM(mo.Broker_Code)) as PayToCode, LTRIM(RTRIM(mcp.Name)) as PayToName, 
                    mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, 
                    msom.Account_Info_BankName , msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder,
                    odc.Discount, odc.GrossPremium, odc.Net_Premium as Premium, odc.AdminFee, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, 
                    (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) as LastUpdatedTime, 1 as RowStatus 
                    , so.Name MarketingName, mb.Nama BranchName
					FROM [dbo].[Mst_Order] mo 
                    LEFT JOIN [dbo].[Mst_Salesman] ms WITH(NOLOCK) ON mo.Salesman_Id = ms.Salesman_Id 
                    LEFT JOIN [dbo].[Mst_User] mu ON mo.EntryUsr = mu.User_Id 
                    LEFT OUTER JOIN [dbo].[Mst_Customer] mcp WITH(NOLOCK) ON mo.Broker_Code = mcp.Cust_Id 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odi.Sum_Insured) TotalSI 
                        FROM mst_order mo WITH(NOLOCK)
                        INNER JOIN ord_Dtl_Insured odi WITH(NOLOCK) on mo.Order_no = odi.Order_No                 
                        GROUP BY mo.Order_No
                    ) odi ON mo.Order_No = odi.Order_No 
                    LEFT JOIN (
                        SELECT mo.Order_No, SUM(odc.Gross_Premium) as GrossPremium, SUM(odc.Gross_Premium)-SUM(odc.Net_Premium) as Discount, 
                        SUM(odc.Net_Premium)as Net_Premium, mo.Policy_Fee+mo.Stamp_Duty as AdminFee, sum(net_premium)+ mo.Policy_Fee+mo.Stamp_Duty as TotalPremium 
                        FROM mst_order mo WITH(NOLOCK) 
                        INNER JOIN ord_Dtl_Coverage odc WITH(NOLOCK) on mo.Order_no = odc.Order_No                
                        GROUP BY mo.Order_No, mo.Policy_Fee, mo.Stamp_Duty
                    ) odc ON mo.Order_No = odc.Order_No 
                    LEFT JOIN (
	                    SELECT mo.Order_No, SUM(occ.Amount) TotalCommission
	                    FROM mst_order mo WITH(NOLOCK) 
	                    INNER JOIN ord_cover_commission occ WITH(NOLOCK) on mo.Order_no = occ.Order_No 
		                    where commission_type=2 GROUP BY mo.Order_No
                    ) occ on mo.Order_No = occ.Order_No 
	                LEFt JOIN(
		                select order_no,sum(percentage) as CommissionPercentage from (
		                    select distinct order_no,disc_comm_no,percentage 
		                    from  Ord_Cover_Commission  where commission_type=2 
                        ) comm group by order_no
                    ) ocp on mo.Order_No = ocp.Order_No 
                    LEFT JOIN (
                        SELECT m.Order_No, b.Dealer_ID,mc.Name as Dealer FROM mst_order m 
                        LEFT JOIN ComSales_Salesman b ON m.broker_code=b.Cust_ID 
                        LEFT JOIN mst_customer mc ON mc.cust_id=b.Dealer_ID
                    ) dlr ON mo.Order_No = dlr.Order_No 
                    LEFT JOIN [dbo].[Mst_Order_Mobile] msom WITH(NOLOCK) ON msom.Order_No = mo.Order_No 
                    LEFT JOIN [dbo].[Mst_State] mst ON mst.State = mo.Order_Status 
                    LEFT JOIN [dbo].[Mst_Product] mp ON mo.Product_Code = mp.Product_Code 
					LEFT JOIN  ( SELECT b.Cust_Id AS CustomerID,
								a.Client_Code AS ClientCode,
								b.Name AS AgentName,
								a.Client_Code AS AgentCode,
                               a.Upliner_Client_Code AS UplinerCode ,
                               RTRIM(d.Name) AS UplinerName,
                               e.Cust_Id AS LeaderCustomerID,
                               a.Leader_Client_Code AS LeaderCode ,                  
                               RTRIM(f.Name) AS LeaderName                    
                                     FROM      dtl_cust_type a WITH ( NOLOCK )
                                               INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                               LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                               LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                               LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                               LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                     WHERE     a.Client_Type = 'AGENT') X ON X.CustomerID=mo.Broker_Code OR X.ClientCode=mo.Broker_Code
                    LEFT JOIN BEYONDMOSS.AABMobile.dbo.OrderSimulation os ON os.PolicyOrderNo = mo.Order_No
					LEFT JOIN BEYONDMOSS.AABMobile.dbo.SalesOfficer so ON so.SalesOfficerID = mo.Salesman_Id
					LEFT JOIN dbo.Mst_Branch mb ON mb.Branch_id = mo.Branch_Id
					WHERE mo.Status = 'A' AND mo.Order_Status not in ('9', '10', '11', '6') AND mst.[Type] = 'ORDER' AND mo.Order_Type in ('1','2') 
                    --AND msom.Order_ID = @0
                    AND mo.Order_No = @0
                    AND msom.Approval_Status = 0 
                    GROUP BY AgentCode, AgentName, UplinerCode, UplinerName, mo.Order_No, ms.Salesman_Id, ms.Name, mo.Broker_Code, mo.Policy_No, mo.Name_On_Policy, mp.Product_Code, mp.Description, --mcp.Name, 
                    odc.Discount, odc.GrossPremium, odc.Net_Premium, odc.AdminFee, dlr.Dealer, 
                    odi.TotalSI, odc.TotalPremium, occ.TotalCommission, ocp.CommissionPercentage, (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END), mo.Order_No, msom.Order_ID, msom.Account_Info_BankName, msom.Account_Info_AccountNo, msom.Account_Info_AccountHolder, mcp.Name 
                    , so.Name, mb.Nama
					ORDER BY (CASE WHEN mo.UpdateDt is null THEN mo.EntryDt ELSE mo.UpdateDt END) ASC, msom.Order_ID DESC";
                    #endregion
                    // 002 end
                    dynamic result = db.FirstOrDefault<dynamic>(query, OrderNumber);
                    if (result == null)
                    {
                        resp.Status = false;
                        resp.Message = "Data Not Found!";
                        resp.ResponseCode = "6661";
                    }
                    else
                    {
                        resp.Status = true;
                        resp.Message = "Success from detailComissionMGO";
                        resp.ResponseCode = "1";
                    }
                    resp.Data = result;
                    //return Json(new { status = true, message = "Success", data = db.FirstOrDefault<dynamic>(query, OrderID) }, Util.jsonSerializerSetting());
                }
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                resp.Status = false;
                resp.Message = e.Message;
            }
            return resp;
        }

        public static void InsertCustHistory(string CustId, string ColumnName, string PhoneNo, string SalesOfficerID, string Email, string ProspectID)
        {
            try
            {
                string qInsertHistory = @"INSERT INTO dbo.CustomerHistory
                                                    ( CustId ,
                                                      PhoneNumberNew ,
                                                      PhoneNumberOld ,
                                                      CreatedBy ,
                                                      CreatedDate,
													  EmailNew,
													  EmailOld,
                                                      Column_Name
                                                    )
                                            VALUES  ( @0 , -- CustId - char(11)
                                                      @1 , -- PhoneNumberNew - varchar(16)
                                                      @2 , -- PhoneNumberOld - varchar(16)
                                                      @3 , -- CreatedBy - varchar(50)
                                                      GETDATE(),  -- CreatedDate - datetime
													  @5,
													  @6,
                                                      @4
                                                    )";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string oldNumber = "";
                if (ColumnName.Contains("MPC"))
                {
                    if (!string.IsNullOrEmpty(CustId))
                    {
                        oldNumber = db.ExecuteScalar<string>(
            @"SELECT COALESCE(AdditionalInfoValue,'') AdditionalInfoValue 
            FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0"
            , CustId, ColumnName);
                    }
                    else
                    {
                        oldNumber = db.ExecuteScalar<string>(
            @"SELECT COALESCE(AdditionalInfoValue,'') AdditionalInfoValue 
            FROM dbo.Mst_Prospect_AdditionalInfo WHERE AdditionalCode = @1 AND ProspectID = @0"
            , ProspectID, ColumnName);

                    }
                }
                else if (ColumnName.Contains("HP"))
                {
                    if (!string.IsNullOrEmpty(CustId))
                    {
                        oldNumber = db.ExecuteScalar<string>(
                        string.Format(@"SELECT {0} FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0", ColumnName)
                        , CustId);
                    }
                    else
                    {
                        oldNumber = db.ExecuteScalar<string>(
                        string.Format(@"SELECT {0} FROM dbo.Mst_Prospect_Personal WHERE Prospect_Id = @0", ColumnName)
                        , ProspectID);
                    }
                }
                else if (ColumnName.Contains("Office_Phone") || ColumnName.Contains("Home_Phone"))
                {
                    oldNumber = db.ExecuteScalar<string>(
        string.Format(@"SELECT {0} FROM dbo.Mst_Customer WHERE Cust_Id = @0", ColumnName)
        , CustId);
                }
                string oldEmail = "";
                if (string.IsNullOrEmpty(ColumnName))
                {
                    oldEmail = db.ExecuteScalar<string>(@"SELECT COALESCE(Email,'') Email FROM mst_customer where Cust_Id  = @0", CustId);
                }
                else
                {
                    oldEmail = "";
                }
                if (!string.IsNullOrEmpty(oldNumber) || !string.IsNullOrEmpty(oldEmail))
                {
                    db.Execute(qInsertHistory
                       , CustId
                       , PhoneNo
                       , oldNumber
                       , SalesOfficerID
                       , ColumnName
                       , Email
                       , oldEmail);
                }

            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void UpdatePrimaryCompany(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                InsertCustHistory(CustIDAAB, "MPC1", param.PhoneNo, SalesOfficerID, "", "");
                db.Execute(qUpdatePhoneCompanyABB, CustIDAAB, param.PhoneNo, "MPC1");
                List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", CustIDAAB, param.ColumnName);
                if (isverified.Count > 0)
                {
                    param.IsConnected = true;
                }
                dynamic dateVerified = null;
                if (param.IsConnected)
                {
                    dateVerified = DateTime.Now;
                }
                db.Execute(@";DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )"
                    , CustIDAAB
                    , param.PICName
                    , SalesOfficerID);
                mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                    , param.PICName
                    , SalesOfficerID);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , param.ColumnName
                    , 2
                    , param.IsConnected ? SalesOfficerID : ""
                    , null
                    , ""
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , "MPC1"
                    , param.IsConnected ? 1 : 0
                    , param.IsConnected ? SalesOfficerID : ""
                    , null
                    , ""
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);
                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                mobiledb.Execute(qUpdateHpAABMobile, CustID, param.PhoneNo, SalesOfficerID);
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, "MPC2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void SetWrongNumberCompany(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC1") || param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                if (!string.IsNullOrEmpty(param.ColumnName))
                {
                    if (!string.IsNullOrEmpty(param.PICName))
                    {
                        db.Execute(@";DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )"
                            , CustIDAAB
                            , param.PICName
                            , SalesOfficerID);
                        mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                            , param.PICName
                            , SalesOfficerID);
                    }
                    dynamic dateVerified = null;
                    if (param.IsConnected)
                    {
                        dateVerified = DateTime.Now;
                    }
                    db.Execute(qInsertUpdateValidation
                        , CustIDAAB
                        , param.ColumnName
                        , 2
                        , param.IsConnected ? SalesOfficerID : ""
                        , null
                        , ""
                        , SalesOfficerID
                        , dateVerified
                        , param.CallRejectReasonId);
                    if (param.ColumnName.Contains("MPC1"))
                    {
                        string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                        mobiledb.Execute(qUpdateHpAABMobile, CustID, SalesOfficerID);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static void UpdatePrimaryPersonal(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string qUpdatePhoneABB = "";
                InsertCustHistory(CustIDAAB, "HP", param.PhoneNo, SalesOfficerID, "", "");
                qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                db.Execute(qUpdatePhoneABB, CustIDAAB, param.PhoneNo, SalesOfficerID);
                List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", CustIDAAB, param.ColumnName);
                if (isverified.Count > 0)
                {
                    param.IsConnected = true;
                }
                dynamic dateVerified = null;
                if (param.IsConnected)
                {
                    dateVerified = DateTime.Now;
                }

                db.Execute(qInsertUpdateValidation
                , CustIDAAB
                , param.ColumnName
                , 2
                , param.IsConnected ? SalesOfficerID : ""
                , null
                , ""
                , SalesOfficerID
                , dateVerified
                , param.CallRejectReasonId);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , "HP"
                    , param.IsConnected ? 1 : 0
                    , param.IsConnected ? SalesOfficerID : ""
                    , param.Relation
                    , param.PICName
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);
                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                mobiledb.Execute(qUpdateHpAABMobile, CustID, param.PhoneNo);
                if (param.ColumnName.Contains("HP_"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(string.Format(qUpdatePhoneABB, param.ColumnName)
                        , CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void SetWrongNumberPersonal(Otosales.Models.vWeb2.PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC1") || param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "", "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                if (!string.IsNullOrEmpty(param.ColumnName))
                {
                    if (!string.IsNullOrEmpty(param.PICName))
                    {
                        db.Execute(@";DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )"
                            , CustIDAAB
                            , param.PICName
                            , SalesOfficerID);
                        mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                            , param.PICName
                            , SalesOfficerID);
                    }
                    dynamic dateVerified = null;
                    if (param.IsConnected)
                    {
                        dateVerified = DateTime.Now;
                    }
                    db.Execute(qInsertUpdateValidation
                        , CustIDAAB
                        , param.ColumnName
                        , 2
                        , param.IsConnected ? SalesOfficerID : ""
                        , null
                        , ""
                        , SalesOfficerID
                        , dateVerified
                        , param.CallRejectReasonId);
                    if (param.ColumnName.Contains("MPC1"))
                    {
                        string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                        mobiledb.Execute(qUpdateHpAABMobile, CustID, SalesOfficerID);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        #endregion
    }
}