﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.dta;
using Otosales.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;

namespace Otosales.Repository.v0090URF2020
{
    public interface IMobileRepository
    {
        dynamic GenerateLinkPayment(string OrderNo, bool IsInstallment);
        void RollBackGenerateLinkPayment(string OrderNo);
        dynamic GetApprovalPaymentStatus(string OrderNo);
        Models.OtosalesAPIResult PopulateOrderMobileApproval(string userID, int ReqType = 1);
        string ProcessApprovalPayment(string OrderNo, string UserId, string ApprovalType, string ActionType);
        List<Models.v0219URF2019.BasicCover> GetBasicCover(int year, bool isBasic, bool isMVGodig = false, string productCode = "");
        List<Models.Product> GetProduct(string insurancetype, string salesofficerid, int isRenewal, bool isNew = false, bool isMVGodig = false);
        OtosalesAPIResult GetPenawaranList(string userID);
    }
    public class MobileRepository : IMobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        public List<Models.v0219URF2019.BasicCover> GetBasicCover(int year, bool isBasic, bool isMVGodig = false, string productCode = "")
        {
            List<Models.v0219URF2019.BasicCover> bcList = new List<Models.v0219URF2019.BasicCover>();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    if (DateTime.Now.Year - year > 5)
                    {
                        bcList = db.Fetch<Models.v0219URF2019.BasicCover>(@"SELECT ID,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE (ComprePeriod=1 AND TLOPeriod=0) or(ComprePeriod=0 AND TLOPeriod=1)");
                    }
                    else
                    {
                        bcList = db.Fetch<Models.v0219URF2019.BasicCover>(@"
SELECT Id,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE isBasic in (" + (isBasic ? "1,0" : "1") + @") AND RowStatus=1");
                    }

                    if (isMVGodig)
                    {
                        bcList = db.Fetch<Models.v0219URF2019.BasicCover>(@"SELECT ID,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE (ComprePeriod=1 AND TLOPeriod=0) or(ComprePeriod=0 AND TLOPeriod=1)");

                        if (!string.IsNullOrEmpty(productCode))
                        {
                            string pc = string.Concat(
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-USEDVEHICLE-PRODUCTCODE").First(),",",
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-NEWVEHICLE-PRODUCTCODE").First(),",",
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-USEDVEHICLE-PRODUCTCODE-AGENCY").First(),",",
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-NEWVEHICLE-PRODUCTCODE-AGENCY").First()
                                       );

                            if (pc.Contains(productCode))
                            {
                                bcList = db.Fetch<Models.v0219URF2019.BasicCover>(@"SELECT  ID ,
                                                CoverageId ,
                                                ComprePeriod ,
                                                TLOPeriod ,
                                                'Comprehensive Lite 1 Year' AS Description
                                        FROM    mst_basic_cover
                                        WHERE   ( ComprePeriod = 1
                                                  AND TLOPeriod = 0
                                                )
                                        ");
                            }
                        }
                    }
                }

                return bcList;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public List<Models.Product> GetProduct(string insurancetype, string salesofficerid, int isRenewal, bool isNew = false, bool isMVGodig = false)
        {
            List<Otosales.Models.Product> result = new List<Models.Product>();
            try
            {
                string query = @"
select DISTINCT  mp.Product_Code [ProductCode], mp.Description, 'GARDAOTO' [ProductTypeCode], @0 [InsuranceType], 1 [RowStatus], CASE WHEN mp.day_calculation_method =360 THEN 360 ELSE 366 END [NDays], Biz_type,mp.cob_id, IIF(PDT.Partner_ID is null, 0, 1) [IsLexus]

from mst_product mp

LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code AND PDT.Partner_ID = 'LEX'

where GETDATE() BETWEEN Valid_From and Valid_To AND mp.Product_Code !='' AND mp.isb2b=0

AND 1 = CASE WHEN @0 = 1 THEN IIF(mp.cob_id = '403' and Biz_type=1 and PDT.Partner_ID is null,1,0)

                      WHEN @0 = 2 THEN IIF(mp.cob_id= '404' and PDT.Partner_ID is null,1,0)

                     WHEN @0 = 3 THEN IIF(PDT.Partner_ID is not null,1,0) 

                     WHEN @0 = 4 THEN IIF(mp.cob_id = '403'and Biz_type=2 and PDT.Partner_ID is null,1,0 ) END

  ";                // Handle ProductAgency
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    bool isAgency = db.ExecuteScalar<int>(@" SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] eu INNER JOIN [BeyondMoss].[AABMobile].[dbo].[SalesOfficer] so ON so.Email = eu.UserID WHERE so.SalesOfficerId=@0", salesofficerid) > 0 ? true : false;
                    query = string.Concat(query, isAgency ? " AND mp.product_type=6" : " AND mp.product_type!=6");

                    if (isMVGodig)
                    {

                        if (isNew && !isAgency)
                        {
                            string pc = string.Concat(
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-NEWVEHICLE-PRODUCTCODE").First(),
                                        ",",
                                        db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-NEWVEHICLE-PRODUCTCODE'")
                                       );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            query = string.Concat(query, " AND  mp.Product_Code IN ("+pctemp+")");
                        }
                        else if (isNew && isAgency)
                        {
//                            query = string.Concat(query, @"AND  mp.Product_Code = (SELECT TOP 1 optionValue 
//FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions WHERE OptionName = 'GODIGITAL-NEWVEHICLE-PRODUCTCODE-AGENCY')");
                            string pc = string.Concat(
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-NEWVEHICLE-PRODUCTCODE-AGENCY").First(),
                                        ",",
                                        db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-NEWVEHICLE-PRODUCTCODE-AGENCY'")
                                       );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            query = string.Concat(query, " AND  mp.Product_Code IN (" + pctemp + ")");
                        }
                        else if (!isNew && !isAgency)
                        {
                            string pc = string.Concat(
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-USEDVEHICLE-PRODUCTCODE").First(),
                                        ",",
                                        db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-USEDVEHICLE-PRODUCTCODE'")
                                       );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            query = string.Concat(query, " AND  mp.Product_Code IN (" + pctemp + ")");
                        }
                        else if (!isNew && isAgency)
                        {
//                            query = string.Concat(query, @"AND  mp.Product_Code = (SELECT TOP 1 optionValue 
//FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions WHERE OptionName = 'GODIGITAL-USEDVEHICLE-PRODUCTCODE-AGENCY')");

                            string pc = string.Concat(
                                       AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-USEDVEHICLE-PRODUCTCODE-AGENCY").First(),
                                       ",",
                                       db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-USEDVEHICLE-PRODUCTCODE-AGENCY'")
                                      );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            query = string.Concat(query, " AND  mp.Product_Code IN (" + pctemp + ")");
                        }



                    }

                    result = db.Fetch<Otosales.Models.Product>(query, insurancetype, isRenewal);

                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        public dynamic GenerateLinkPayment(string OrderNo, bool IsInstallment)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string longUrl = ConfigurationManager.AppSettings["OtosalesGodigLinkPaymentURL"].ToString();
            string URLShortenerAPI = ConfigurationManager.AppSettings["URLShortenerAPI"].ToString();
            string dynamicLinkDomain = ConfigurationManager.AppSettings["DynamicLinkDomain"].ToString();
            string APIKey = ConfigurationManager.AppSettings["APIKey"].ToString();
            dynamic ShortenURL = string.Empty;
            dynamic res = new ExpandoObject();
            bool isNeedApproval = false;

            try
            {
                Uri url = new Uri(URLShortenerAPI + APIKey);
                string body = "{\"dynamicLinkInfo\":{\"dynamicLinkDomain\":\"" + dynamicLinkDomain + "\",\"link\":\"" + longUrl + OrderNo
                    + @"&IsInstallment="+ IsInstallment.ToString() + "\"},\"suffix\": {\"option\": \"SHORT\"}}";
                dynamic stringResponse = null;
                using (HttpClient client = new HttpClient())
                {
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json")).Result;
                    stringResponse = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);
                }
                
                ShortenURL = replaceShortenURLWithTemplate((string)stringResponse.shortLink, OrderNo);
                
                #region CheckApproval
                string actor = string.Empty;
                if (IsInstallment)
                {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                    List<dynamic> ls = db.Fetch<dynamic>(@"
SELECT os.ProductCode,os.BranchCode,os.SalesOfficerID, so.Role 
FROM dbo.OrderSimulation os INNER JOIN dbo.SalesOfficer so
ON so.SalesOfficerID = os.SalesOfficerID
WHERE OrderNo = @0 AND ApplyF = 1 AND os.RowStatus = 1", OrderNo);
                        if (ls.Count > 0)
                        {
                            actor = ls.First().SalesOfficerID;
                            int productF = db.ExecuteScalar<int>(@";
DECLARE @@Count INT = 0
SELECT @@Count=COUNT(1) FROM dbo.Fn_AABSplitString((
SELECT ParamValue FROM dbo.ApplicationParameters WHERE ParamName = 'OTOSALES-PAYMENT-PRODUCT-APPROVAL'),',')
WHERE Data = @0
SELECT @@Count
", ls.First().ProductCode);
                            int branchF = db.ExecuteScalar<int>(@";
DECLARE @@Count INT = 0
SELECT @@Count=COUNT(1) FROM dbo.AABHead WHERE Type = 'REGIONALMGR' AND Param = @0
SELECT @@Count", ls.First().BranchCode);
                            int roleF = db.ExecuteScalar<int>(@";
DECLARE @@Count INT = 0
SELECT @@Count=COUNT(1) FROM dbo.Fn_AABSplitString((
SELECT ParamValue FROM dbo.ApplicationParameters WHERE ParamName = 'OTOSALES-PAYMENT-ROLE-APPROVAL'),',')
WHERE Data = @0
SELECT @@Count
", ls.First().Role);
                            if (productF > 0)
                            {
                                string qInsert = @"
INSERT INTO dbo.OrderSimulationApproval
        ( OrderNo ,
          ApprovalType ,
          ApprovalStatus ,
          Reason ,
          RoleCode ,
          ApprovalUsr ,
          RowStatus ,
          CreatedBy ,
          CreatedDate
        )
VALUES  ( @0 , -- OrderNo - varchar(100)
          @1 , -- ApprovalType - char(7)
          0 , -- ApprovalStatus - int
          '' , -- Reason - varchar(500)
          @2 , -- RoleCode - varchar(100)
          '' , -- ApprovalUsr - char(5)
          1 , -- RowStatus - bit
          @3 , -- CreatedBy - varchar(50)
          GETDATE()  -- CreatedDate - datetime
        )";
                                isNeedApproval = true;
                                insertDataShortUrlLinkPayment(OrderNo, (string)stringResponse.shortLink, IsInstallment, isNeedApproval, actor);
                                db.Execute(qInsert, OrderNo, "PMT1", ls.First().Role, actor);
                                if (roleF > 0 && branchF > 0)
                                {
                                    db.Execute(qInsert, OrderNo, "PMT2", ls.First().Role, actor);
                                }
                            }
                            else {
                                insertDataShortUrlLinkPayment(OrderNo, (string)stringResponse.shortLink, IsInstallment, isNeedApproval, actor);
                            }
                        }
                    }
                }
                else
                {
                    insertDataShortUrlLinkPayment(OrderNo, (string)stringResponse.shortLink, IsInstallment, isNeedApproval, actor);
                }
                #endregion

                if (isNeedApproval)
                {
                    res.ShortenURL = "";
                }
                else {
                    res.ShortenURL = ShortenURL;
                }
                res.IsNeedApproval = isNeedApproval;

                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public void RollBackGenerateLinkPayment(string OrderNo) {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                string qDelete = @";
DELETE FROM dbo.OrderSimulationApproval WHERE OrderNo = @0 AND RowStatus = 1 
DELETE FROM dbo.LinkPayment WHERE OrderNo = @0 AND RowStatus = 1";
                db.Execute(qDelete, OrderNo);
            }
        }

        public dynamic GetApprovalPaymentStatus(string OrderNo) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            dynamic result = new ExpandoObject();
            /* 0 - Not in criteria - hide button
               1 - Need to check approval - show button 
               2 - Need Approval - hide button & hide shorten url
               3 - Approved Can Regenerate - show button & show shorten url
               4 - Reject - show pop up & show button & hide shorten url 
               5 - Approved Can't Regenerate - hide button & show shorten url */
            result.ApprovalStatus = 0;
            result.ShortenURL = "";
            result.IsInstallment = null;
            result.ApprovalUser = "";
            try
            {
                string qIsProcess = @";
DECLARE @@IsProcess INT = 0
SELECT @@IsProcess = IsProcess FROM (
SELECT 
CASE 
	WHEN (IsRenewal = 1 AND COALESCE(IsPolicyIssuedBeforePaying,0) = 0 AND FollowUpInfo = 57) 
	THEN 1
	WHEN (IsRenewal = 1 AND COALESCE(IsPolicyIssuedBeforePaying,0) = 1 AND FollowUpInfo IN (50,51,52))
	THEN 1
	WHEN (COALESCE(IsRenewal,0) = 0 AND FollowUpInfo IN (50,51,52))
	THEN 1
	ELSE 0
END IsProcess	 
FROM dbo.OrderSimulation os
INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND f.RowStatus = 1
AND os.OrderNo = @0) a
SELECT CAST(@@IsProcess AS BIT)";
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    List<dynamic> lsDt = db.Fetch<dynamic>(@";
SELECT PolicyOrderNo, ProductCode FROM dbo.OrderSimulation WHERE OrderNo = @0 AND RowStatus = 1 AND ApplyF = 1", OrderNo);
                    if (lsDt.Count > 0)
                    {
                        bool IsProcess = db.ExecuteScalar<bool>(qIsProcess, OrderNo);
                        bool IsConve = false;
                        using (var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                        {
                            IsConve = aabdb.ExecuteScalar<bool>(@";DECLARE @@IsProcess INT = 0
SELECT @@IsProcess=IIF(Biz_type=1,1,0) FROM dbo.Mst_Product where Product_Code = @0 AND Status = 1
SELECT CAST(@@IsProcess AS BIT)", lsDt.First().ProductCode);
                        }
                        if (IsProcess && IsConve)
                        {
                            result.ApprovalStatus = 1;
                            List<dynamic> ls = db.Fetch<dynamic>(@";
SELECT COALESCE(ShortenURL, '') ShortenURL,CAST(IsNeedApproval AS BIT) IsNeedApproval, 
CAST(COALESCE(IsInstallment,0) AS BIT) IsInstallment 
FROM dbo.LinkPayment WHERE OrderNo = @0 
AND RowStatus = 1 AND ShortenURLStatus = 1", OrderNo);
                            if (ls.Count > 0)
                            {
                                result.IsInstallment = ls.First().IsInstallment;
                                if (ls.First().IsNeedApproval)
                                {
                                    List<dynamic> lsApproval = db.Fetch<dynamic>(@"
SELECT COUNT(1) Total, COALESCE(SUM(IIF(ApprovalStatus = 1, 1, 0)),0) [SumApproved]
, COALESCE(SUM(IIF(ApprovalStatus = 2, 1, 0)),0) [SumReject]
FROM dbo.OrderSimulationApproval WHERE OrderNo = @0 AND RowStatus = 1", OrderNo);
                                    if (lsApproval.Count > 0)
                                    {
                                        if (lsApproval.First().Total > 0 && lsApproval.First().SumReject == 0 &&
                                            lsApproval.First().Total != lsApproval.First().SumApproved)
                                        {
                                            result.ApprovalStatus = 2;
                                            List<dynamic> lsApprovalUsr = db.Fetch<dynamic>(@";EXEC dbo.usp_GetOtosalesMobileApprovalUser @0", OrderNo);
                                            if (lsApprovalUsr.Count > 0) {
                                                result.ApprovalUser = string.Join(",", lsApprovalUsr.Select(x => x.SalesOfficerID).ToArray());
                                            }
                                        }
                                        else if (lsApproval.First().Total > 0 &&
                                            lsApproval.First().Total == lsApproval.First().SumApproved)
                                        {
                                            result.ShortenURL = ls.First().ShortenURL;
                                            result.ShortenURL = replaceShortenURLWithTemplate((string)result.ShortenURL, OrderNo);
                                            result.ApprovalStatus = 3;
                                        }
                                        else if (lsApproval.First().SumReject > 0)
                                        {
                                            result.ApprovalStatus = 4;
                                        }
                                    }
                                }
                                else
                                {
                                    result.ShortenURL = ls.First().ShortenURL;
                                    result.ShortenURL = replaceShortenURLWithTemplate((string)result.ShortenURL, OrderNo);
                                    result.ApprovalStatus = 3;
                                }
                                if (result.ApprovalStatus == 3)
                                {

                                    decimal totalBayar = Otosales.Logic.OrdLogic.GetTotalPaidPayment(lsDt.First().PolicyOrderNo, false);
                                    if (totalBayar > 0)
                                    {
                                        result.ApprovalStatus = 5;
                                    }
                                }
                            }

                            if (result.ApprovalStatus == 3 || result.ApprovalStatus == 1)
                            {

                                using (var aabdb = RepositoryBase.GetAABDB())
                                {
                                    List<dynamic> lsDtlProduct = aabdb.Fetch<dynamic>(@";
SELECT CAST(DATEADD(DAY,COALESCE(mp.Grace_Period,0), mo.Period_From) AS DATE) ExpDate  
FROM dbo.Mst_Order mo INNER JOIN dbo.Mst_Product mp ON mo.Product_Code = mp.Product_Code
where mo.Order_No = @0 AND mp.Product_Code = @1 AND mp.Status = 1", lsDt.First().PolicyOrderNo, lsDt.First().ProductCode);
                                    if (lsDtlProduct.Count > 0)
                                    {
                                        if (DateTime.Now.Date > lsDtlProduct.First().ExpDate)
                                        {
                                            result.ApprovalStatus = 5;
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " : " + e.StackTrace);
                throw e;
            }
        }
        public Models.OtosalesAPIResult PopulateOrderMobileApproval(string userID, int ReqType = 1)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Models.OtosalesAPIResult response = new Models.OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    // dynamic res = db.Fetch<dynamic>(@";EXEC usp_OtosalesGetApprovalList @0", userID);
                    dynamic res = db.Fetch<dynamic>(@";EXEC usp_GetOtosalesMobileApprovalList @0, @1", userID, ReqType);

                    dynamic result = new ExpandoObject();
                    if (ReqType == 1)
                    {
                        result.List = res;
                        result.Count = res.Count;
                    }
                    else if (ReqType == 2) {
                        result.List = null;
                        result.Count = res[0].ApprovalCount;
                    }

                    response.Status = true;
                    response.Data = result;
                    response.ResponseCode = "1";
                    response.Message = "Success!";
                }
                return response;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public string ProcessApprovalPayment(string OrderNo, string UserId, string ApprovalType, string ActionType)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string query = @";DECLARE @@ID BIGINT
DECLARE @@OrderNo VARCHAR(100)

SELECT TOP 1 @@OrderNo=OrderNo FROM dbo.OrderSimulation WHERE PolicyOrderNo = @0
AND ApplyF = 1 AND RowStatus = 1

SELECT @@ID=ID FROM dbo.OrderSimulationApproval 
WHERE OrderNo = @@OrderNo AND ApprovalType = @1
AND ApprovalStatus = 0 AND RowStatus = 1

UPDATE dbo.OrderSimulationApproval 
SET RowStatus = 0, ModifiedDate = GETDATE(), ModifiedBy = @3
WHERE ID = @@ID

INSERT INTO dbo.OrderSimulationApproval
        ( OrderNo ,
          ApprovalType ,
          ApprovalStatus ,
          Reason ,
          RoleCode ,
          ApprovalUsr ,
          RowStatus ,
          CreatedBy ,
          CreatedDate
        )
SELECT	OrderNo -- OrderNo - varchar(100)
		, ApprovalType -- ApprovalType - char(7)
		, @2 -- ApprovalStatus - int
		, Reason -- Reason - varchar(500)
		, RoleCode -- RoleCode - varchar(100)
		, @3 -- ApprovalUsr - char(5)
		, 1 -- RowStatus - bit
		, @3 -- CreatedBy - varchar(50)
		, GETDATE() -- CreatedDate - datetime	 
FROM dbo.OrderSimulationApproval
WHERE ID = @@ID";
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    db.Execute(query, OrderNo, ApprovalType, ActionType, UserId);
                }
                int actType = Convert.ToInt32(ActionType);
                if (actType == 1)
                {
                    return "Approved";
                }
                else if (actType == 2) {
                    return "Rejected";
                }
                return "";
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public OtosalesAPIResult GetPenawaranList(string userID) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            int isSendBid = 0;
            OtosalesAPIResult response = new OtosalesAPIResult();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    isSendBid = db.ExecuteScalar<int>("SELECT IsSendBid FROM SalesOfficer WHERE SalesOfficerID = @0", userID);
                }

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABAGENT))
                {

                    if (isSendBid > 0)
                    {
                        string query = @";IF EXISTS(SELECT policyID FROM Penawaran WHERE isHotProspect = 1 AND BidStatus = 0 AND fuByCSOBranch = 0 AND RowStatus = 1)
BEGIN
SELECT policyID, policyCode, policyGeneratedDate, agentCode, agentEmail, agentFullName, customerName, customerEmail, customerPhone, 
policeNumber, productCode, protectionType, vehicleCode, vehicleType, vehicleDescription, vehicleYear, usageCode, usageText, 
areaCode, totalSumInsured, baseRate, isTJH, tjhRate, tjhValue, isPaDriver, paDriverRate, paDriverValue, isPaPass, 
paPassRate, paPassQuantity, paPassValue, isAutoApply, srccRate, etvRate, fldRate, isCheckSrcc, autoApplyRate, isTRS, 
trsRate, isAcc, accRate, accValue, loadingRate, administrationPrice, basicPremi, accPremi, totalPremiKendaraanPlusAksesoris, 
premiLoading, totalPremiDasarPlusLoading, premiAutoApply, premiTRS, premiTJH, premiPADriver, premiPAPass, totalPremiPerluasan, 
totalPremiDasarPlusPerluasan, totalAllPremi, infoModified, BidStatus, LastUpdatedTime, Domisili, followUpDateTime, isHotProspect, fuByCSOBranch 
FROM Penawaran WHERE BidStatus = 0 AND fuByCSOBranch = 0 AND isHotProspect = 1 AND RowStatus = 1
AND ((followUpDateTime is NULL AND getdate() > DATEADD(HOUR, @0, infoModified)) OR (followUpDateTime < DATEADD(HOUR, @1, getdate()))) 
ORDER BY isHotProspect DESC, infoModified DESC, policyID ASC
END
ELSE
BEGIN

SELECT policyID, policyCode, policyGeneratedDate, agentCode, agentEmail, agentFullName, customerName, customerEmail, customerPhone, 
policeNumber, productCode, protectionType, vehicleCode, vehicleType, vehicleDescription, vehicleYear, usageCode, usageText, 
areaCode, totalSumInsured, baseRate, isTJH, tjhRate, tjhValue, isPaDriver, paDriverRate, paDriverValue, isPaPass, 
paPassRate, paPassQuantity, paPassValue, isAutoApply, srccRate, etvRate, fldRate, isCheckSrcc, autoApplyRate, isTRS, 
trsRate, isAcc, accRate, accValue, loadingRate, administrationPrice, basicPremi, accPremi, totalPremiKendaraanPlusAksesoris, 
premiLoading, totalPremiDasarPlusLoading, premiAutoApply, premiTRS, premiTJH, premiPADriver, premiPAPass, totalPremiPerluasan, 
totalPremiDasarPlusPerluasan, totalAllPremi, infoModified, BidStatus, LastUpdatedTime, Domisili, followUpDateTime, isHotProspect, fuByCSOBranch 
FROM Penawaran WHERE BidStatus = 0 AND fuByCSOBranch = 0 AND RowStatus = 1 
AND ((followUpDateTime is NULL AND getdate() > DATEADD(HOUR, @0, infoModified)) OR (followUpDateTime < DATEADD(HOUR, @1, getdate()))) 
ORDER BY isHotProspect DESC, infoModified DESC, policyID ASC

END

";
                        int wopt = Convert.ToInt32(ConfigurationManager.AppSettings["DelayWOPT"]);
                        int wpt = Convert.ToInt32(ConfigurationManager.AppSettings["DelayWPT"]);
                        List<Penawaran> penawaran = db.Fetch<Penawaran>(query, wopt, wpt);

                        response.Status = true;
                        response.Data = penawaran;
                    }
                    else
                    {
                        response.Status = false;
                        response.Message = "You are not authorized to perform this action.";
                    }
                }
                return response;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        #region Private
        private string replaceShortenURLWithTemplate(string ShortenURLres, string OrderNo)
        {
            string ShortenURL = "";

            List<string> Subjects = Otosales.Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALES-WORDING-TEMPLATE-LINK-PAYMENT");
            ShortenURL = Subjects[0];
            ShortenURL = (string)ShortenURL.Replace("#LINKPAYMENT#", ShortenURLres);

            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            string name = db.FirstOrDefault<string>(@";SELECT 
LTRIM(RTRIM(CASE WHEN COALESCE(pc.isCompany , 0)= 1 THEN pcc.PICname
ELSE pc.Name
END)) Name
FROM dbo.OrderSimulation os 
INNER JOIN dbo.FollowUp fu ON os.OrderNo = @0 AND os.FollowUpNo = fu.FollowUpNo AND os.ApplyF = 1
INNER JOIN dbo.ProspectCustomer pc ON os.CustID = pc.CustID
LEFT JOIN dbo.ProspectCompany pcc ON pc.CustID = pcc.CustID
", OrderNo);
            ShortenURL = ShortenURL.Replace("#NAMETERTANGGUNG#", name);



            return ShortenURL;
        }

        private void insertDataShortUrlLinkPayment(string OrderNo, string shortenURL, bool isInstallment, bool isNeedApproval, string Actor)
        {
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                string query = @"
IF EXISTS(SELECT OrderNo FROM dbo.LinkPayment WHERE OrderNo = @0 AND ShortenURLStatus = 1 AND RowStatus = 1)
BEGIN
	UPDATE dbo.OrderSimulationApproval SET RowStatus = 0, ModifiedBy = @4, ModifiedDate = GETDATE()
	WHERE OrderNo = @0 AND RowStatus = 1
	UPDATE dbo.LinkPayment SET ShortenURLStatus = 0, RowStatus = 0, ModifiedBy = @4, ModifiedDate = GETDATE()
	WHERE OrderNo = @0 AND ShortenURLStatus = 1 AND RowStatus = 1
END

INSERT INTO dbo.LinkPayment
		( OrderNo ,
			ShortenURL ,
			ShortenURLStatus ,
			IsNeedApproval ,
			IsInstallment ,
			RowStatus ,
			CreatedBy ,
			CreatedDate
		)
VALUES  ( @0 , -- OrderNo - varchar(100)
			@1 , -- ShortenURL - varchar(max)
			1 , -- ShortenURLStatus - smallint
			@2 , -- IsNeedApproval - bit
			@3 , -- IsInstallment - bit
			1 , -- RowStatus - smallint
			@4 , -- CreatedBy - varchar(5)
			GETDATE()  -- CreatedDate - datetime
		)";
                db.Execute(query, OrderNo, shortenURL, isNeedApproval, isInstallment, Actor);
            }
        }

        #endregion
    }
}