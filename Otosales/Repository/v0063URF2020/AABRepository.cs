﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

namespace Otosales.Repository.v0063URF2020
{
    public interface IAABRepository
    {
        dynamic GetEnableDisableSalesmanDealer(string ProductCode, string BranchCode);
    }
    public class AABRepository : IAABRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();
        public dynamic GetEnableDisableSalesmanDealer(string ProductCode, string BranchCode)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            try
            {
                dynamic res = new ExpandoObject();
                res.IsDealerEnable = false;
                res.IsSalesmanInfoEnable = false;

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = "SELECT REPLACE(code, ' ', '') AS code FROM dbo.Mst_General WHERE type = 'PSO'";
                List<string> ListPSO = db.Fetch<string>(query);
                query = "SELECT REPLACE(code, ' ', '') AS code FROM dbo.Mst_General WHERE type = 'BSO'";
                List<string> ListBSO = db.Fetch<string>(query);
                if (ListPSO.Contains(ProductCode.Trim()) && ListBSO.Contains(BranchCode.Trim()))
                {
                    res.IsDealerEnable = true;
                    res.IsSalesmanInfoEnable = true;
                }
                if (!res.IsDealerEnable) {
                    query = @";DECLARE @@Count INT = 0
SELECT @@Count=COUNT(1) FROM dbo.Mst_Product mp
INNER JOIN Prd_Commission pc ON mp.Product_Code = pc.Product_Code
WHERE mp.Product_Code = @0 AND pc.Pay_To_Party = 3 AND Product_Type <> 6
IF(@@Count>0)
BEGIN
	SELECT CAST(1 AS BIT)
END
ELSE 
BEGIN
	SELECT CAST(0 AS BIT)
END";
                    res.IsDealerEnable = db.ExecuteScalar<bool>(query, ProductCode);
                }
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
    }
}