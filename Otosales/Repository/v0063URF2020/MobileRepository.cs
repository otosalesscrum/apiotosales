﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Web;

namespace Otosales.Repository.v0063URF2020
{
    public interface IMobileRepository
    {
        List<Models.v0219URF2019.BasicCover> GetBasicCover(int year, bool isBasic, bool isMVGodig = false);
        string GenerateLinkPayment(string OrderNo, string TypeCC);
        string UpdateTaskListDetail(string CustID, string FollowUpNo,
Otosales.Models.vWeb2.PersonalData pd, Otosales.Models.vWeb2.CompanyData cd, Otosales.Models.vWeb2.VehicleData vd, Otosales.Models.vWeb2.PolicyAddress pa,
Otosales.Models.vWeb2.SurveySchedule ss, List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, Models.vWeb2.OrderSimulationModel OS, Models.vWeb2.OrderSimulationMVModel OSMV,
Models.vWeb2.CalculatePremiModel CalculatePremiModel, string FollowUpStatus, Otosales.Models.vWeb2.Remarks rm);
        dynamic GetProductType(string salesOfficerId, bool isMVGodig = false);
        Otosales.Models.vWeb2.TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo);
        dynamic GetTaksListDetailDocumentPersonal(string CustID, string FollowUpNo);
        dynamic GetTaksListDetailDocumentAll(string CustID, string FollowUpNo);
        dynamic GetTaksListDetailDocumentCompany(string CustID, string FollowUpNo);
    }
    public class MobileRepository : IMobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();
        public //static 
            string UpdateTaskListDetail(string CustID, string FollowUpNo,
Otosales.Models.vWeb2.PersonalData pd, Otosales.Models.vWeb2.CompanyData cd, Otosales.Models.vWeb2.VehicleData vd, Otosales.Models.vWeb2.PolicyAddress pa,
Otosales.Models.vWeb2.SurveySchedule ss, List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, Models.vWeb2.OrderSimulationModel OS, Models.vWeb2.OrderSimulationMVModel OSMV,
Models.vWeb2.CalculatePremiModel CalculatePremiModel, string FollowUpStatus, Otosales.Models.vWeb2.Remarks rm)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string result = "";

            string query = @"";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            try
            {
                //Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);

                Thread threadPd = new Thread(() =>
                {
                    if (pd != null)
                    {
                        try
                        {
                            // pd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PersonalData>(personalData);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @";IF EXISTS(SELECT * FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCustomer SET 
	                        Name = @1, CustBirthDay = @2, CustGender = @3, CustAddress = @4, 
                            PostalCode = @5, Email1 = @6, Phone1 = @7, IdentityNo = @8, 
                            isCompany = @9
                            WHERE CustID = @0 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @12
                        AND ApplyF = 1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.OrderSimulation 
	                        SET IsNeedDocRep = @10, AmountRep = @11
	                        WHERE CustID = @0 AND FollowUpNo = @12
	                        AND ApplyF = 1 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @12)
                        BEGIN
	                        UPDATE dbo.FollowUp 
	                        SET FollowUpName = @1, ProspectName = @1
	                        WHERE CustID = @0 AND FollowUpNo = @12
                        END ";
                            dbMobile.Execute(queryThread, CustID
                                , pd.Name, pd.CustBirthDay, pd.CustGender, pd.CustAddress
                                , pd.PostalCode, pd.Email1, pd.Phone1, pd.IdentityNo
                                , pd.isCompany, pd.isNeedDocRep, pd.AmountRep, FollowUpNo);
                            if (pd.IsPayerCompany != null)
                            {
                                queryThread = @";UPDATE dbo.ProspectCustomer SET IsPayerCompany = @1 WHERE CustID = @0";
                                dbMobile.Execute(queryThread, CustID, pd.IsPayerCompany);
                            }
                        }
                        catch (Exception e)
                        {
                            //throw;
                        }
                    }
                });

                Thread threadCd = new Thread(() =>
                {
                    if (cd != null)
                    {
                        try
                        {
                            //cd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.CompanyData>(companyData);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @";DECLARE @@IsCompany BIT
                            SELECT @@IsCompany=IsCompany FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1
                            IF @@IsCompany=1
                            BEGIN
	                            IF EXISTS (SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                                BEGIN
	                                UPDATE dbo.ProspectCompany SET CompanyName = @2, NPWPno = @3, NPWPdate = @4, 
	                                NPWPaddress = @5, OfficeAddress = @6, PostalCode = @7, PICPhoneNo = @8, PICname = @9, Email = @10 
	                                WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
		                            UPDATE dbo.ProspectCustomer SET Name = @9, Phone1 = @8, Email1 = @10
		                            WHERE CustID = @0
                                END
                            END";
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , cd.CompanyName, cd.NPWPno, cd.NPWPdate
                                , cd.NPWPaddres, cd.OfficeAddress, cd.PostalCode, cd.PICPhoneNo, cd.PICname, cd.PICEmail);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadOS = new Thread(() =>
                {
                    if (OS != null && OSMV != null)
                    {
                        try
                        {
                            InsertOrderSimulation(OS, OSMV);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadVd = new Thread(() =>
                {
                    if (vd != null)
                    {
                        try
                        {
                            // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = "";

                            if (!string.IsNullOrEmpty(vd.ProductCode))
                            {
                                queryThread = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET ProductTypeCode = @2,
	                            VehicleCode = @3, BrandCode = @4, ModelCode = @5,
	                            Series = @6, Type = @7, Sitting = @8, Year = @9,
	                            CityCode = @10, UsageCode = @11, AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET InsuranceType = @19, 
                                ProductCode = @20, 
								SegmentCode = @21, PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                            }
                            else
                            {
                                queryThread = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                            }
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , vd.ProductTypeCode
                                , vd.VehicleCode, vd.BrandCode, vd.ModelCode
                                , vd.Series, vd.Type, vd.Sitting, vd.Year
                                , vd.CityCode, vd.UsageCode, vd.AccessSI
                                , vd.RegistrationNumber, vd.EngineNumber
                                , vd.ChasisNumber, vd.IsNew, vd.SumInsured
                                , vd.ColorOnBPKB

                                , vd.InsuranceType
                                , vd.ProductCode
                                , vd.SegmentCode, vd.PeriodFrom, vd.PeriodTo
                                , vd.IsORDefectsRepair, vd.IsAccessoriesChange
                                , vd.IsPolicyIssuedBeforePaying
                                , vd.Remarks

                                , vd.DealerCode
                                , vd.SalesDealer);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadCalculatePremi = new Thread(() =>
                {
                    if (CalculatePremiModel != null)
                    {
                        try
                        {
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string OrderNo = dbMobile.ExecuteScalar<string>(
                                @";DECLARE @@Orderno VARCHAR(100) 
                        SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                    INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                    WHERE f.CustID = @0 AND f.FollowUpNo = @1 
                        AND o.RowStatus = 1 AND ApplyF = 1
                        SELECT @@Orderno"
                                , CustID, FollowUpNo);

                            // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                            InsertExtendedCover(OrderNo, CalculatePremiModel, vd.PeriodFrom, vd.PeriodTo);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadPa = new Thread(() =>
                {
                    if (pa != null)
                    {

                        try
                        {
                            //pa = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PolicyAddress>(policyAddress);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @"IF EXISTS (SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulation SET PolicySentTo = @2, PolicyDeliveryName = @3, 
	                            PolicyDeliveryAddress = @4, PolicyDeliveryPostalCode = @5 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , pa.SentTo, pa.Name
                                , pa.Address, pa.PostalCode);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadSs = new Thread(() =>
                {
                    if (ss != null)
                    {
                        //ss = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.SurveySchedule>(surveySchedule);
                        try
                        {
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @";DECLARE @@OrderNo VARCHAR(100);
                            SELECT @@OrderNo=OrderNo FROM dbo.OrderSimulation
                            WHERE CustID = @0 AND FollowUpNo = @1 AND 
                            RowStatus = 1 AND ApplyF = 1
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationSurvey oss
                            INNER JOIN dbo.OrderSimulation os ON os.OrderNo = oss.OrderNo
                            WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND 
                            os.RowStatus = 1 AND os.ApplyF = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulationSurvey SET CityCode = @2, LocationCode = @3, SurveyAddress = @4,
	                            SurveyPostalCode = @5, SurveyDate = @6, ScheduleTimeID = @7, ModifiedBy = 'OtosalesAPI',
								IsNeedSurvey = @8, IsNSASkipSurvey = @9, IsManualSurvey = @10,
								ModifiedDate = GETDATE() WHERE OrderNo = @@OrderNo
                            END
							ELSE
							BEGIN
								INSERT INTO dbo.OrderSimulationSurvey
								        ( OrderNo ,
								          CityCode ,
								          LocationCode ,
								          SurveyAddress ,
								          SurveyPostalCode ,
								          SurveyDate ,
								          ScheduleTimeID ,
								          RowStatus ,
								          CreatedBy ,
								          CreatedDate,
										  IsNeedSurvey,
										  IsNSASkipSurvey,
										  IsManualSurvey
								        )
								VALUES  ( @@OrderNo , -- OrderNo - varchar(100)
								          @2 , -- CityCode - int
								          @3 , -- LocationCode - int
								          @4 , -- SurveyAddress - varchar(500)
								          @5 , -- SurveyPostalCode - varchar(5)
								          @6 , -- SurveyDate - datetime
								          @7 , -- ScheduleTimeID - datetime
								          1 , -- RowStatus - bit
								          'OtosalesAPI' , -- CreatedBy - varchar(50)
								          GETDATE(), -- CreatedDate - datetime
										  @8,
										  @9,
										  @10
								        )
							END";
                            int maxLength = 150;
                            string SurveyAddress = "";
                            if (!string.IsNullOrEmpty(ss.SurveyAddress) && !string.IsNullOrWhiteSpace(ss.SurveyAddress))
                            {
                                SurveyAddress = ss.SurveyAddress.Length <= maxLength ? ss.SurveyAddress : ss.SurveyAddress.Substring(0, maxLength);
                            }
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , ss.CityCode, ss.LocationCode, ss.SurveyAddress
                                , ss.SurveyPostalCode, ss.SurveyDate, ss.ScheduleTimeID
                                , ss.IsNeedSurvey, ss.IsNSASkipSurvey, ss.IsManualSurvey);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                threadPd.Start();
                threadCd.Start();
                threadOS.Start();
                threadVd.Start();
                threadCalculatePremi.Start();
                threadPa.Start();
                threadSs.Start();


                threadPd.Join();
                threadCd.Join();
                threadOS.Join();
                threadVd.Join();
                threadCalculatePremi.Join();
                threadPa.Join();
                threadSs.Join();


                if (ListImgData.Count > 0)
                {
                    //ListImgData = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.ImageDataTaskDetail>>(imageData);
                    UpdateImageData(ListImgData, CustID, FollowUpNo);

                }
                if (!string.IsNullOrEmpty(FollowUpStatus))
                {
                    query = @"UPDATE dbo.FollowUp SET FollowUpStatus = @2 WHERE CustID = @0 AND FollowUpNo = @1";
                    db.Execute(query, CustID, FollowUpNo, FollowUpStatus);
                }
                query = @"SELECT OrderNo FROM dbo.OrderSimulation
                        WHERE CustID = @0 AND FollowUpNo = @1 AND 
                        RowStatus = 1 AND ApplyF = 1";
                List<dynamic> res = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (res.Count > 0)
                {
                    result = res.First().OrderNo;
                }
                if (rm != null)
                {
                    query = @"UPDATE dbo.FollowUp
                            SET RemarkToSA = @0 WHERE FollowUpNo = @1";
                    // rm = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.Remarks>(Remarks);
                    db.Execute(query, rm.RemarkToSA, FollowUpNo);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static void InsertOrderSimulation(Models.vWeb2.OrderSimulationModel OS, Models.vWeb2.OrderSimulationMVModel OSMV)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            // OrderSimulationModel OS = new OrderSimulationModel();
            // OrderSimulationMVModel OSMV = new OrderSimulationMVModel();
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                #region INSERT ORDERSIMULATION
                string OrderNoReturn = "";
                string FollowUpNoEnd = "";
                string CustIdEnd = "";
                // OS = JsonConvert.DeserializeObject<OrderSimulationModel>(OSData);
                query = "SELECT OrderNo FROM OrderSimulation WHERE CustID=@0 AND FollowUpNo=@1 AND ApplyF = 1";
                List<dynamic> ordno = db.Fetch<dynamic>(query, OS.CustID, OS.FollowUpNo);
                string OrderNo = "";
                if (ordno.Count > 0)
                {
                    OrderNo = ordno.First().OrderNo;
                }
                else
                {
                    OrderNo = System.Guid.NewGuid().ToString();
                }
                OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                OrderNoReturn = OS.OrderNo;
                OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;
                OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;
                OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[VANumber]
      ,[SurveyNo]
      ,[PeriodFrom]
      ,[PolicyNo]
      ,[AmountRep]
      ,[PolicyID]
      ,[SegmentCode]
) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25
,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36)
END";
                OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                db.Execute(query, OS.OrderNo
, OS.CustID
, OS.FollowUpNo
, OS.QuotationNo
, OS.MultiYearF
, OS.YearCoverage
, OS.TLOPeriod
, OS.ComprePeriod
, OS.BranchCode
, OS.SalesOfficerID
, OS.PhoneSales
, OS.DealerCode
, OS.SalesDealer
, OS.ProductTypeCode
, OS.AdminFee
, OS.TotalPremium
, OS.Phone1
, OS.Phone2
, OS.Email1
, OS.Email2
, OS.SendStatus
, OS.InsuranceType
, OS.ApplyF
, OS.SendF
, OS.LastInterestNo
, OS.LastCoverageNo
, OS.PolicySentTo
, OS.PolicyDeliveryName
, OS.PolicyDeliveryAddress
, OS.PolicyDeliveryPostalCode
, OS.VANumber
, OS.SurveyNo
, OS.PeriodFrom
, OS.PeriodTo
, OS.PolicyNo
, OS.AmountRep
, OS.PolicyID
, OS.SegmentCode);
                #endregion

                #region INSERT UPDATE ORDERSIMULATION MV
                // OSMV = JsonConvert.DeserializeObject<OrderSimulationMVModel>(OSMVData);
                OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;
                int objNo = db.ExecuteScalar<int>(@";DECLARE @@ObjectNo INT
SELECT @@ObjectNo=CAST(ObjectNo AS INT) FROM dbo.OrderSimulationMV WHERE OrderNo = @0
IF(@@ObjectNo IS NOT NULL AND @@ObjectNo <> 0)
BEGIN
	SELECT @@ObjectNo
END
ELSE
BEGIN
	SELECT 1
END", OS.OrderNo);

                query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                db.Execute(query, OSMV.OrderNo
, objNo
, OSMV.ProductTypeCode
, OSMV.VehicleCode
, OSMV.BrandCode
, OSMV.ModelCode
, OSMV.Series
, OSMV.Type
, OSMV.Sitting
, OSMV.Year
, OSMV.CityCode
, OSMV.UsageCode
, OSMV.SumInsured
, OSMV.AccessSI
, OSMV.RegistrationNumber
, OSMV.EngineNumber
, OSMV.ChasisNumber
//, OSMV.LastUpdatedTime
//, OSMV.RowStatus
, OSMV.IsNew);
                #endregion

            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static void InsertExtendedCover(string OrderNo, Models.vWeb2.CalculatePremiModel CalculatePremiModel, DateTime? PeriodFrom, DateTime? PeriodTo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            bool isexistSFE = false;
            int itr = 0;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    List<Models.OrderSimulationInterest> OSI = new List<Models.OrderSimulationInterest>();
                    List<Models.OrderSimulationCoverage> OSC = new List<Models.OrderSimulationCoverage>();
                    int a = 1;
                    int year = 1;
                    //CalculatePremiModel CalculatePremiModel = JsonConvert.DeserializeObject<CalculatePremiModel>(calculatedPremiItems);
                    List<Models.vWeb2.CalculatedPremiItems> calculatedPremiItem = CalculatePremiModel.ListCalculatePremi;
                    Models.vWeb2.OSData OS = CalculatePremiModel.OSData;
                    Models.vWeb2.OSMVData OSMV = CalculatePremiModel.OSMVData;
                    OS.OrderNo = OS.OrderNo;
                    OSMV.OrderNo = OSMV.OrderNo;
                    if (calculatedPremiItem.Count > 0)
                    {
                        int objNo = db.ExecuteScalar<int>(@";DECLARE @@ObjectNo INT
SELECT @@ObjectNo=CAST(ObjectNo AS INT) FROM dbo.OrderSimulationMV WHERE OrderNo = @0
IF(@@ObjectNo IS NOT NULL AND @@ObjectNo <> 0)
BEGIN
	SELECT @@ObjectNo
END
ELSE
BEGIN
	SELECT 1
END", OS.OrderNo);
                        DateTime? pt = null;
                        foreach (Models.vWeb2.CalculatedPremiItems cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (Models.OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (Models.vWeb2.CalculatedPremiItems cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                        pt = cpitem.PeriodTo;
                                    }
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                        pf = cpitem.PeriodFrom;
                                    }
                                }
                                Models.OrderSimulationInterest osi = new Models.OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = objNo;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (Models.vWeb2.CalculatedPremiItems cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;
                        foreach (Models.OrderSimulationInterest item in OSI)
                        {
                            foreach (Models.vWeb2.CalculatedPremiItems item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    Models.OrderSimulationCoverage oscItem = new Models.OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = objNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }

                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert

                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OrderNo);

                        int i = 1;
                        foreach (Models.OrderSimulationInterest data in OSI)
                        {
                            query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                            db.Execute(query, data.OrderNo, objNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                            i++;
                        }

                        #endregion
                        #endregion




                        #region Insert
                        //  OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

                        i = 1;
                        foreach (Models.OrderSimulationCoverage data in OSC)
                        {
                            query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                            db.Execute(query, data.OrderNo, objNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                            i++;

                        }
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        OS.TotalPremium = db.ExecuteScalar<decimal>("SELECT COALESCE((SELECT SUM(Premium) FROM OrderSimulationCoverage where OrderNo=@0)+(SELECT AdminFee FROM OrderSimulation where OrderNo=@0),0)", OS.OrderNo);
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = PeriodFrom ?? DateTime.Now;
                            DateTime end = PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                itr++;
                while (itr < 4) {
                    InsertExtendedCover(OrderNo, CalculatePremiModel, PeriodFrom, PeriodTo);
                }
            }
        }

        public static void UpdateImageData(List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, string CustID, string FollowUpNo)
        {
            string query = "";
            try
            {
                string SPPAKB = "";
                string FAKTUR = "";
                string KonfirmasiCust = "";
                string DocNSA1 = "";
                string DocNSA2 = "";
                string DocNSA3 = "";
                string DocNSA4 = "";
                string DocNSA5 = "";
                string IdentityCard = "";
                string STNK = "";
                string BSTB = "";
                string DocRep = "";
                string BuktiBayar = "";
                string BuktiBayar2 = "";
                string BuktiBayar3 = "";
                string BuktiBayar4 = "";
                string BuktiBayar5 = "";
                string DocPendukung = "";
                string NPWP = "";
                string SIUP = "";
                int lengthahdj = Convert.ToInt32(Math.Truncate(Convert.ToDouble(6 / 5)));

                Thread threadSetImage = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    var img = dbMobile.Fetch<dynamic>(@";SELECT SPPAKB,FAKTUR,KonfirmasiCust,DocNSA1,DocNSA2,DocNSA3,DocNSA4,DocNSA5,IdentityCard,STNK,
                                                        BSTB1, DocRep, BuktiBayar, DocPendukung, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
                                                        FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1", CustID, FollowUpNo);
                    if (img.Count > 0)
                    {
                        SPPAKB = Convert.ToString(img.First().SPPAKB);
                        FAKTUR = Convert.ToString(img.First().FAKTUR);
                        KonfirmasiCust = Convert.ToString(img.First().KonfirmasiCust);
                        DocNSA1 = Convert.ToString(img.First().DocNSA1);
                        DocNSA2 = Convert.ToString(img.First().DocNSA2);
                        DocNSA3 = Convert.ToString(img.First().DocNSA3);
                        DocNSA4 = Convert.ToString(img.First().DocNSA4);
                        DocNSA5 = Convert.ToString(img.First().DocNSA5);
                        IdentityCard = Convert.ToString(img.First().IdentityCard);
                        STNK = Convert.ToString(img.First().STNK);
                        BSTB = Convert.ToString(img.First().BSTB1);
                        DocRep = Convert.ToString(img.First().DocRep);
                        BuktiBayar = Convert.ToString(img.First().BuktiBayar);
                        BuktiBayar2 = Convert.ToString(img.First().BuktiBayar2);
                        BuktiBayar3 = Convert.ToString(img.First().BuktiBayar3);
                        BuktiBayar4 = Convert.ToString(img.First().BuktiBayar4);
                        BuktiBayar5 = Convert.ToString(img.First().BuktiBayar5);
                        DocPendukung = Convert.ToString(img.First().DocPendukung);
                    }
                    img = dbMobile.Fetch<dynamic>(@";SELECT NPWP, SIUP
                                                    FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1",
                                                    CustID, FollowUpNo);
                    if (img.Count > 0)
                    {
                        NPWP = Convert.ToString(img.First().NPWP);
                        SIUP = Convert.ToString(img.First().SIUP);
                    }
                });
                threadSetImage.Start();
                threadSetImage.Join();

                string qUpdateImgData = @";UPDATE dbo.ImageData SET RowStatus = 0 WHERE PathFile = @0";
                Thread ThreadSPPAKB = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail SPPAKABimg = ListImgData.Where(x => x.ImageType == "SPPAKB").FirstOrDefault();
                    if (!string.IsNullOrEmpty(SPPAKB))
                    {
                        if (!SPPAKB.Trim().Equals(SPPAKABimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, SPPAKB);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    SPPAKB = SPPAKABimg.PathFile;
                });
                Thread ThreadFAKTUR = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail FAKTURimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("FAKTUR")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(FAKTUR))
                    {
                        if (!FAKTUR.Trim().Equals(FAKTURimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, FAKTUR);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    FAKTUR = FAKTURimg.PathFile;
                });
                Thread ThreadKonfirmasiCust = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail KonfirmasiCustimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("KONFIRMASICUST")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(KonfirmasiCust))
                    {
                        if (!KonfirmasiCust.Trim().Equals(KonfirmasiCustimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, KonfirmasiCust);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    KonfirmasiCust = KonfirmasiCustimg.PathFile;
                });
                Thread ThreadDocNSA1 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA1img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA1")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA1))
                    {
                        if (!DocNSA1.Trim().Equals(DocNSA1img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, DocNSA1);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    DocNSA1 = DocNSA1img.PathFile;
                });
                Thread ThreadDocNSA2 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA2img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA2")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA2))
                    {
                        if (!DocNSA2.Trim().Equals(DocNSA2img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, DocNSA2);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    DocNSA2 = DocNSA2img.PathFile;
                });
                Thread ThreadDocNSA3 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA3img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA3")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA3))
                    {
                        if (!DocNSA3.Trim().Equals(DocNSA3img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, DocNSA3);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    DocNSA3 = DocNSA3img.PathFile;
                });
                Thread ThreadDocNSA4 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA4img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA4")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA4))
                    {
                        if (!DocNSA4.Trim().Equals(DocNSA4img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, DocNSA4);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    DocNSA4 = DocNSA4img.PathFile;
                });
                Thread ThreadDocNSA5 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocNSA5img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCNSA5")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocNSA5))
                    {
                        if (!DocNSA5.Trim().Equals(DocNSA5img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, DocNSA5);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    DocNSA5 = DocNSA5img.PathFile;
                });
                Thread ThreadIdentityCard = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail IdentityCardimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("IDENTITYCARD")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(IdentityCard))
                    {
                        if (!IdentityCard.Trim().Equals(IdentityCardimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, IdentityCard);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    IdentityCard = IdentityCardimg.PathFile;
                });
                Thread ThreadSTNK = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail STNKimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("STNK")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(STNK))
                    {
                        if (!STNK.Trim().Equals(STNKimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, STNK);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    STNK = STNKimg.PathFile;
                });
                Thread ThreadBSTB = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BSTBimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BSTB")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BSTB))
                    {
                        if (!BSTB.Trim().Equals(BSTBimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, BSTB);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    BSTB = BSTBimg.PathFile;
                });
                Thread ThreadDocRep = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail DocRepimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCREP")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(DocRep))
                    {
                        if (!DocRep.Trim().Equals(DocRepimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, DocRep);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    DocRep = DocRepimg.PathFile;
                });
                Thread ThreadNPWP = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail NPWPimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("NPWP")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(NPWP))
                    {
                        if (!NPWP.Trim().Equals(NPWPimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, NPWP);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    NPWP = NPWPimg.PathFile;
                });
                Thread ThreadSIUP = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail SIUPimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("SIUP")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(SIUP))
                    {
                        if (!SIUP.Trim().Equals(SIUPimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, SIUP);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    SIUP = SIUPimg.PathFile;
                });
                //Thread ThreadDocPendukung = new Thread(() => {
                //    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                //    Otosales.Models.vWeb2.ImageDataTaskDetail DocPendukungimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("DOCPENDUKUNG")).FirstOrDefault();
                //    if (!string.IsNullOrEmpty(DocPendukung))
                //    {
                //        if (!DocPendukung.Trim().Equals(DocPendukungimg.PathFile.Trim()))
                //        {
                //            dbMobile.Execute(qUpdateImgData, DocPendukung);
                //        }
                //    }
                //    DocPendukung = DocPendukungimg.PathFile;
                //});
                Thread ThreadBuktiBayar = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayarimg = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar))
                    {
                        if (!BuktiBayar.Trim().Equals(BuktiBayarimg.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, BuktiBayar);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    BuktiBayar = BuktiBayarimg.PathFile;
                });
                Thread ThreadBuktiBayar2 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar2img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR2")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar2))
                    {
                        if (!BuktiBayar2.Trim().Equals(BuktiBayar2img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, BuktiBayar2);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    BuktiBayar2 = BuktiBayar2img.PathFile;
                });
                Thread ThreadBuktiBayar3 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar3img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR3")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar3))
                    {
                        if (!BuktiBayar3.Trim().Equals(BuktiBayar3img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, BuktiBayar3);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    BuktiBayar3 = BuktiBayar3img.PathFile;
                });
                Thread ThreadBuktiBayar4 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar4img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR4")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar4))
                    {
                        if (!BuktiBayar4.Trim().Equals(BuktiBayar4img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, BuktiBayar4);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    BuktiBayar4 = BuktiBayar4img.PathFile;
                });
                Thread ThreadBuktiBayar5 = new Thread(() =>
                {
                    var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                    Otosales.Models.vWeb2.ImageDataTaskDetail BuktiBayar5img = ListImgData.Where(x => x.ImageType.ToUpper().Equals("BUKTIBAYAR5")).FirstOrDefault();
                    if (!string.IsNullOrEmpty(BuktiBayar5))
                    {
                        if (!BuktiBayar5.Trim().Equals(BuktiBayar5img.PathFile.Trim()))
                        {
                            try
                            {
                                dbMobile.Execute(qUpdateImgData, BuktiBayar5);
                            }
                            catch (Exception e)
                            {
                                //throw;
                            }
                        }
                    }
                    BuktiBayar5 = BuktiBayar5img.PathFile;
                });

                Thread threadMoveImage = new Thread(() =>
                {
                    try
                    {
                        #region SAVE IMAGE
                        var mblDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryThread = @";
SELECT [ImageID]
      ,[FollowUpNo]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                        List<dynamic> ImageData = mblDB.Fetch<dynamic>(queryThread, FollowUpNo);
                        foreach (dynamic imd in ImageData)
                        {
                            queryThread = @";IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,1,@6,@7)
END";
                            mblDB.Execute(queryThread, imd.ImageID, imd.DeviceID
        , imd.SalesOfficerID
        , imd.PathFile
        , imd.EntryDate
        , imd.LastUpdatedTime
        , imd.Data
        , imd.ThumbnailData);
                            mblDB.Execute(";UPDATE dbo.TempImageData SET RowStatus=0 WHERE FollowUpNo=@0 AND PathFile=@1", FollowUpNo, imd.PathFile);
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {

                        //throw;
                    }
                });

                ThreadSPPAKB.Start();
                ThreadFAKTUR.Start();
                ThreadKonfirmasiCust.Start();
                ThreadSPPAKB.Join();
                ThreadFAKTUR.Join();
                ThreadKonfirmasiCust.Join();

                ThreadDocNSA1.Start();
                ThreadDocNSA2.Start();
                ThreadDocNSA3.Start();
                ThreadDocNSA1.Join();
                ThreadDocNSA2.Join();
                ThreadDocNSA3.Join();

                ThreadDocNSA4.Start();
                ThreadDocNSA5.Start();
                ThreadIdentityCard.Start();
                ThreadDocNSA4.Join();
                ThreadDocNSA5.Join();
                ThreadIdentityCard.Join();

                ThreadSTNK.Start();
                ThreadBSTB.Start();
                ThreadDocRep.Start();
                ThreadSTNK.Join();
                ThreadBSTB.Join();
                ThreadDocRep.Join();

                ThreadNPWP.Start();
                ThreadSIUP.Start();
                //ThreadDocPendukung.Start();
                ThreadBuktiBayar.Start();
                ThreadNPWP.Join();
                ThreadSIUP.Join();
                //ThreadDocPendukung.Join();
                ThreadBuktiBayar.Join();

                ThreadBuktiBayar2.Start();
                ThreadBuktiBayar3.Start();
                ThreadBuktiBayar2.Join();
                ThreadBuktiBayar3.Join();

                ThreadBuktiBayar4.Start();
                ThreadBuktiBayar5.Start();
                ThreadBuktiBayar4.Join();
                ThreadBuktiBayar5.Join();

                threadMoveImage.Start();
                threadMoveImage.Join();


                Thread threadUpdateImage = new Thread(() =>
                {
                    try
                    {
                        var dbMobileThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        query = @";IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.FollowUp SET SPPAKB = @2, FAKTUR = @3, KonfirmasiCust = @4,
	                            DocNSA1 = @5, DocNSA2 = @6, DocNSA3 = @7, IdentityCard = @8, STNK = @9,
                                BSTB1 = @10, DocRep = @11, BuktiBayar = @12, DocPendukung = @13
                                , BuktiBayar2 = @14, BuktiBayar3 = @15, BuktiBayar4 = @16, BuktiBayar5 = @17
                                , DocNSA4 = @18, DocNSA5 = @19
	                            WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                        //                query = @"IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                        //                            BEGIN
                        //	                            UPDATE dbo.FollowUp SET SPPAKB = @2, FAKTUR = @3, KonfirmasiCust = @4,
                        //	                            DocNSA1 = @5, DocNSA2 = @6, DocNSA3 = @7, IdentityCard = @8, STNK = @9,
                        //                                BSTB1 = @10, DocRep = @11, BuktiBayar = @12, DocPendukung = @13
                        //	                            WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                        //                            END";

                        dbMobileThread.Execute(query, CustID, FollowUpNo
                            , SPPAKB, FAKTUR, KonfirmasiCust
                            , DocNSA1, DocNSA2, DocNSA3, IdentityCard, STNK
                            , BSTB, DocRep, BuktiBayar, DocPendukung
                            , BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5);

                        if (!string.IsNullOrEmpty(NPWP) || !string.IsNullOrEmpty(SIUP))
                        {
                            query = @";UPDATE dbo.ProspectCompany 
                                SET NPWP = @0, SIUP = @1
                                WHERE CustID = @2 AND FollowUpNo = @3";
                            dbMobileThread.Execute(query, NPWP, SIUP, CustID, FollowUpNo);
                        }

                        string qGetBktByr = @";SELECT a.ImageName BuktiBayar, id.Data, PolicyOrderNo, id.CoreImage_ID, id.ImageID FROM
(
SELECT
    FollowUpNo,ImageName, ImageType, PolicyOrderNo
FROM
(
select fu.FollowUpNo, PolicyOrderNo, BuktiBayar, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
from dbo.FollowUp fu WITH (NOLOCK)
INNER JOIN dbo.OrderSimulation os WITH (NOLOCK) ON os.FollowUpNo = fu.FollowUpNo
where fu.followupno = @0 AND fu.RowStatus = 1 AND ApplyF = 1 AND os.RowStatus = 1
)a
UNPIVOT (
    ImageName FOR ImageType IN (
        BuktiBayar, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
    )
) unpvt
where ImageName <> ''
) a
INNER JOIN dbo.ImageData id WITH (NOLOCK) on ImageName = id.PathFile
AND (CoreImage_ID IS NULL OR CoreImage_ID = '')";
                        List<dynamic> bktbyr = dbMobileThread.Fetch<dynamic>(qGetBktByr, FollowUpNo);
                        if (bktbyr.Count > 0)
                        {
                            foreach (dynamic item in bktbyr)
                            {
                                string outImageID = null;
                                string referenceImageType = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage("BuktiBayar", false);
                                if (!string.IsNullOrEmpty(item.PolicyOrderNo))
                                {
                                    if (Otosales.Logic.ImageLogic.UploadImage(item.Data, item.BuktiBayar, referenceImageType, 1, "OTOSL", item.PolicyOrderNo, out outImageID) && outImageID != null)
                                    {
                                        Otosales.Logic.ImageLogic.UpdateImageData(item.ImageID, outImageID);
                                    }
                                }
                            }
                            Otosales.Logic.ImageLogic.DeleteUnusedImages(FollowUpNo);
                        }
                        else
                        {
                            Otosales.Logic.ImageLogic.DeleteUnusedImages(FollowUpNo);
                        }
                    }
                    catch (Exception e)
                    {

                        //throw;
                    }
                });

                threadUpdateImage.Start();
                threadUpdateImage.Join();
                //Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public string GenerateLinkPayment(string OrderNo, string TypeCC)
        {
            string longUrl = ConfigurationManager.AppSettings["OtosalesGodigLinkPaymentURL"].ToString();
            string URLShortenerAPI = ConfigurationManager.AppSettings["URLShortenerAPI"].ToString();
            string dynamicLinkDomain = ConfigurationManager.AppSettings["DynamicLinkDomain"].ToString();
            string APIKey = ConfigurationManager.AppSettings["APIKey"].ToString();
            dynamic returnTemp = string.Empty;

            try
            {
                Uri url = new Uri(URLShortenerAPI + APIKey);
                string body = "{\"dynamicLinkInfo\":{\"dynamicLinkDomain\":\"" + dynamicLinkDomain + "\",\"link\":\"" + longUrl + OrderNo + "\"},\"suffix\": {\"option\": \"SHORT\"}}";
                dynamic stringResponse = null;
                using (HttpClient client = new HttpClient())
                {
                    ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json")).Result;
                    stringResponse = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);
                }

                List<string> Subjects = Otosales.Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALES-WORDING-TEMPLATE-LINK-PAYMENT");
                returnTemp = Subjects[0];
                returnTemp = (string)returnTemp.Replace("#LINKPAYMENT#", (string)stringResponse.shortLink);

                insertDataShortUrlLinkPayment(OrderNo, returnTemp);

                return returnTemp;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        void insertDataShortUrlLinkPayment(string OrderNo, string shortenURL)
        {

        }

        public //static 
            List<Models.v0219URF2019.BasicCover> GetBasicCover(int year, bool isBasic, bool isMVGodig = false)
        {
            List<Models.v0219URF2019.BasicCover> bcList = new List<Models.v0219URF2019.BasicCover>();
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    if (DateTime.Now.Year - year > 5)
                    {
                        bcList = db.Fetch<Models.v0219URF2019.BasicCover>(@"SELECT ID,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE (ComprePeriod=1 AND TLOPeriod=0) or(ComprePeriod=0 AND TLOPeriod=1)");
                    }
                    else
                    {
                        bcList = db.Fetch<Models.v0219URF2019.BasicCover>(@"
SELECT Id,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE isBasic in (" + (isBasic ? "1,0" : "1") + @") AND RowStatus=1");
                    }

                    if (isMVGodig)
                    {
                        bcList = db.Fetch<Models.v0219URF2019.BasicCover>(@"SELECT ID,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE (ComprePeriod=1 AND TLOPeriod=0) or(ComprePeriod=0 AND TLOPeriod=1)");
                    }
                }

                return bcList;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public dynamic GetTaksListDetailDocumentPersonal(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                dynamic result = null;

                string queryPersonalDocument = @";SELECT  IdentityCard ,
        id1.Data IdentityCardData ,
        f.STNK ,
        id2.Data STNKData ,
        f.BSTB1 BSTB ,
        id3.Data BSTBData ,
        f.KonfirmasiCust ,
        id4.Data KonfirmasiCustData ,
        f.DocRep ,
        id5.Data DocRepData
FROM    dbo.FollowUp f
        OUTER APPLY ( SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.IdentityCard ) id1
        OUTER APPLY ( SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.STNK ) id2
        OUTER APPLY ( SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.BSTB1 ) id3
        OUTER APPLY ( SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.KonfirmasiCust ) id4
        OUTER APPLY ( SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.DocRep ) id5
WHERE   f.CustID = @0
        AND f.FollowUpNo = @1
        AND f.RowStatus = 1";

                var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<dynamic> listDyn = dbFieldPremiMobile.Fetch<dynamic>(queryPersonalDocument, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result = listDyn;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public dynamic GetTaksListDetailDocumentCompany(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                dynamic result = null;

                string queryDocument = @";IF EXISTS ( SELECT  *
            FROM    dbo.ProspectCompany
            WHERE   CustID = @0
                    AND FollowUpNo = @1 )
    BEGIN
        SELECT  pc.NPWP ,
                id1.Data NPWPData ,
                pc.SIUP ,
                id2.Data SIUPData
        FROM    dbo.ProspectCompany pc
                OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = pc.NPWP ) id1
                OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = pc.SIUP ) id2
        WHERE   pc.CustID = @0
                AND pc.FollowUpNo = @1
                AND pc.RowStatus = 1
    END
ELSE
    BEGIN
        SELECT  NULL NPWP ,
                NULL NPWPData ,
                NULL SIUP ,
                NULL SIUPData
    END";

                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<dynamic> listDyn = dbMobile.Fetch<dynamic>(queryDocument, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result = listDyn;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public dynamic GetTaksListDetailDocumentAll(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                dynamic result = null;

                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var AABdbThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string queryDocument = @";SELECT  f.SPPAKB ,
        id1.Data SPPAKBData ,
        f.FAKTUR ,
        id2.Data FAKTURData ,
        f.DocNSA1 ,
        id3.Data DocNSA1Data ,
        f.DocNSA2 ,
        id4.Data DocNSA2Data ,
        f.DocNSA3 ,
        id5.Data DocNSA3Data ,
        f.DocNSA4 ,
        id6.Data DocNSA4Data ,
        f.DocNSA5 ,
        id7.Data DocNSA5Data ,
        RemarkToSA ,
        RemarkFromSA
FROM    dbo.FollowUp f
        OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.SPPAKB ) id1
        OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.FAKTUR ) id2
        OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.DocNSA1 ) id3
        OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.DocNSA2 ) id4
        OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.DocNSA3 ) id5
        OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.DocNSA4 ) id6
        OUTER APPLY (SELECT TOP 1 Data FROM dbo.ImageData WHERE PathFile = f.DocNSA5 ) id7
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                List<dynamic> listDoc = dbMobile.Fetch<dynamic>(queryDocument, CustID, FollowUpNo);
                if (listDoc.Count > 0)
                {
                    queryDocument = @";SELECT PolicyOrderNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1 ";

                    string policyOrderNo = dbMobile.FirstOrDefault<string>(queryDocument, CustID, FollowUpNo);

                    queryDocument = @"SELECT TOP 1 mu.User_Name Name 
                                FROM dbo.Mst_Order_Mobile mom
                                INNER JOIN dbo.Mst_User mu
                                ON mu.User_Id = mom.EntryUsr
                                WHERE Order_No = @0 
                                AND (SA_State = 0 OR SA_State = 2) 
                                AND mom.EntryUsr <> 'OTOSL' ORDER BY mom.EntryDt DESC";
                    List<dynamic> ls = AABdbThread.Fetch<dynamic>(queryDocument, policyOrderNo);
                    if (ls.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(listDoc.First().RemarkFromSA) && !string.IsNullOrWhiteSpace(listDoc.First().RemarkFromSA)
                            && !string.IsNullOrEmpty(policyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name + " - " + listDoc.First().RemarkFromSA;
                        }
                        else if (!string.IsNullOrEmpty(policyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name;
                        }
                    }
                    result = listDoc;
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }


        public dynamic GetProductType(string salesOfficerId, bool isMVGodig = false)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            dynamic res = null;

            try
            {

                string query = @"
                  SELECT ProductTypeCode, Description, InsuranceType, RowStatus FROM ProductType WHERE RowStatus = 1
                ";


                if (isMVGodig)
                {
                    query += " AND InsuranceType=1";
                }


                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    using (var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        bool isAllowSharia = aabdb.ExecuteScalar<bool>(@";
DECLARE @@Count INT = 0
SELECT @@Count=Count(*) from mst_salesman s WITH (NOLOCK)
inner join Mst_Branch b on s.Branch_id = b.Branch_id
where s.status = 1 and user_id_otosales = @0 and b.biz_type=2
IF (@@Count>0)
BEGIN
	SELECT CAST(1 AS BIT)
END
ELSE
BEGIN
	SELECT CAST(0 AS BIT)
END", salesOfficerId);
                        if (!isAllowSharia)
                        {
                            string branchCodeSharia = db.ExecuteScalar<string>(@";
DECLARE @@BranchCode VARCHAR(6) = ''
SELECT @@BranchCode = BranchCodeSharia FROM dbo.SalesOfficer so 
INNER JOIN dbo.MappingBranchConveToSharia mcs
ON REPLACE(so.BranchCode,'A','')  = mcs.BranchCodeConve
WHERE so.SalesOfficerID = @0 AND so.RowStatus = 1
SELECT @@BranchCode", salesOfficerId);
                            if (!string.IsNullOrEmpty(branchCodeSharia))
                            {
                                isAllowSharia = aabdb.ExecuteScalar<bool>(@";
DECLARE @@Count INT = 0
SELECT @@Count=COUNT(1) FROM dbo.Mst_Salesman WHERE Name = 'Stephanus Rumawas' AND Branch_Id = @0 AND Status = 1
IF (@@Count>0)
BEGIN
	SELECT CAST(1 AS BIT)
END
ELSE
BEGIN
	SELECT CAST(0 AS BIT)
END", branchCodeSharia);
                            }
                        }
                        query = string.Concat(query, (isAllowSharia ? " Order By SeqNo ASC" : " AND SeqNo NOT IN(2) Order By SeqNo ASC"));
                        res = db.Fetch<dynamic>(query);
                    }
                }
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Function " + actionName + " Error : " + e.ToString());
                throw e;
            }
        }
        public Otosales.Models.vWeb2.TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            //List<dynamic> listDyn = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
            string query = "";

            try
            {
                Thread threadPremi = new Thread(() =>
                {
                    //Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
                    try
                    {
                        string queryFieldPremi = @"SELECT 0 GrossPremium,	0 Admin, 0 NetPremi, PolicyNo, OrderNo, PolicyOrderNo, SurveyNo, OldPolicyNo, 0 NoClaimBonus, NULL OldPolicyPeriodTo 
FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND ApplyF = 1 AND RowStatus = 1";

                        var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                        var dbFieldPremiAABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                        Otosales.Models.vWeb2.FieldPremi premi = dbFieldPremiMobile.Query<Otosales.Models.vWeb2.FieldPremi>(queryFieldPremi, CustID, FollowUpNo).FirstOrDefault();
                        if (premi != null)
                        {
                            if (!string.IsNullOrEmpty(premi.OldPolicyNo))
                            {
                                List<dynamic> ls = new List<dynamic>();
                                queryFieldPremi = @";SELECT Period_To FROM dbo.Policy WHERE Policy_No = @0";

                                ls = dbFieldPremiAABdb.Fetch<dynamic>(queryFieldPremi, premi.OldPolicyNo);
                                if (ls.Count > 0)
                                {
                                    premi.OldPolicyPeriodTo = ls.First().Period_To;
                                }
                            }
                            result.FieldPremi = premi;
                            if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                            {
                                queryFieldPremi = @";SELECT STUFF(( SELECT ', ' + CONCAT(Name,' ',Description,' ',Quantity,' No Cover')
                                    FROM dbo.Ord_Dtl_NoCover 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '') AS NoCover,
                        STUFF(( SELECT ', ' + [Description]
                                    FROM dbo.Ord_Object_Clause 
			                        WHERE Order_No = @0
								    AND Clause_Id = 'OMV99'
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS OriginalDefect,
                        STUFF(( SELECT ', ' + Item_Name
                                    FROM dbo.Ord_Dtl_Interest_Item 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS Accessories,
                        ms.[Description] AS SurveyStatus,
                        s.ResultRecommendation AS SurveyorRecomendation,
                        s.remarks AS Remaks,
                        mu.[User_Name] AS Surveyor
                        FROM dbo.Survey s 
						INNER JOIN dbo.Lnk_Survey_Order lso
						ON s.Survey_No = lso.Survey_No
                        INNER JOIN dbo.Mst_Order mo 
                        ON mo.Order_No = s.Reference_No
                        INNER JOIN dbo.Mst_State ms 
                        ON ms.[Type] = 'survey' 
                        AND ms.[State] = s.[Status]
                        INNER JOIN dbo.Mst_User mu
                        ON mu.[User_Id] = s.Surveyor
                        WHERE mo.Order_No = @0";
                                Otosales.Models.vWeb2.SurveyData sd = dbFieldPremiAABdb.Query<Otosales.Models.vWeb2.SurveyData>(queryFieldPremi, result.FieldPremi.PolicyOrderNo).FirstOrDefault();
                                result.SurveyData = sd;
                            }
                        }

                        string queryFollowUpInfo = @";SELECT fu.FollowUpStatus,fu.FollowUpInfo,fu.IsRenewal,so.Name as Agency,'' as Upliner, fu.SalesOfficerID, so.Name
                        FROM dbo.FollowUp fu  
                        INNER JOIN dbo.SalesOfficer so 
                        ON so.SalesOfficerId=fu.SalesOfficerId
                        WHERE fu.FollowUpNo = @0";
                        List<dynamic> listDynFollowUpInfo = dbFieldPremiMobile.Fetch<dynamic>(queryFollowUpInfo, FollowUpNo);
                        if (listDynFollowUpInfo.Count > 0)
                        {
                            foreach (dynamic item in listDynFollowUpInfo)
                            {
                                bool isAgency = false;
                                string email = dbFieldPremiMobile.ExecuteScalar<string>(@";
DECLARE @@Email VARCHAR(512) = ''
SELECT @@Email=Email FROM dbo.SalesOfficer WHERE SalesOfficerID = @0 AND RowStatus = 1
SELECT @@Email", item.SalesOfficerID);
                                if (!string.IsNullOrEmpty(email))
                                {
                                    isAgency = dbFieldPremiAABdb.ExecuteScalar<bool>(@";
DECLARE @@Count INT = 0
SELECT @@Count = COUNT(1) 
FROM a2isAuthorizationDB.General.ExternalUsers 
WHERE UserID = @0 AND RowStatus = 0
IF(@@Count>0)
BEGIN SELECT CAST(1 AS BIT) END
ELSE 
BEGIN SELECT CAST(0 AS BIT) END", email);
                                }

                                item.Agency = isAgency ? item.Agency : "";
                                item.Upliner = isAgency ? dbFieldPremiAABdb.ExecuteScalar<string>(@";SELECT TOP 1 RTRIM(d.Name) AS Upliner         
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT' AND b.Cust_Id=@0 OR a.Client_Code=@0", item.SalesOfficerID) : "";
                            }
                            result.FollowUpInfo = listDynFollowUpInfo;
                        }
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadPersonalData = new Thread(() =>
                {
                    try
                    {
                        string queryPersonalData = @";SELECT p.Name,p.CustBirthDay,p.CustGender,p.CustAddress,p.PostalCode,
                        p.Email1,p.Phone1,p.IdentityNo, CAST(COALESCE(p.isCompany,0) AS BIT) isCompany, 
                        CAST(0 AS BIT) isNeedDocRep, 0 AmountRep, IsPayerCompany
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
                        WHERE p.CustID = @0 AND p.RowStatus = 1";

                        var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        result.PersonalData = dbFieldPremiMobile.Query<Otosales.Models.vWeb2.PersonalData>(queryPersonalData, CustID).FirstOrDefault();
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadCompanyData = new Thread(() =>
                {
                    try
                    {
                        string queryCompanyData = @";SELECT CompanyName,NPWPno,NPWPdate,NPWPaddress NPWPaddres,OfficeAddress
                            ,PostalCode,PICPhoneNo,PICname,Email PICEmail FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";

                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        result.CompanyData = dbMobile.Query<Otosales.Models.vWeb2.CompanyData>(queryCompanyData, CustID, FollowUpNo).FirstOrDefault();
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadVehicleData = new Thread(() =>
                {
                    try
                    {
                        Otosales.Models.vWeb2.VehicleData vData = new Models.vWeb2.VehicleData();
                        Thread threadVehicleData1 = new Thread(() =>
                        {
                            try
                            {
                                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                                string queryVehicleData = @";SELECT c.Description + ' ' + d.Description + ' ' + omv.Year + ' ' + a.Series  AS Vehicle,
                        omv.VehicleCode, omv.BrandCode, os.ProductTypeCode, omv.ModelCode,  omv.Series, 
                        omv.Type, CAST(omv.Sitting AS DECIMAL) Sitting, omv.Year, omv.CityCode, CAST(omv.SumInsured AS DECIMAL) SumInsured, os.InsuranceType,
                        os.VANumber, p.DealerCode, p.SalesDealer, omv.UsageCode, omv.ColorOnBPKB, 
                        omv.RegistrationNumber, omv.EngineNumber, omv.ChasisNumber, omv.IsNew, os.OrderNo,
						os.PolicyNo, os.ProductCode, os.SegmentCode, os.PeriodFrom, os.PeriodTo,
						os.IsORDefectsRepair, IsAccessoriesChange, IsPolicyIssuedBeforePaying, 
                        os.Remarks, 0 BasicCoverID, '' ORDefectsDesc, '' NoCover
                        FROM Vehicle a 
                        LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
                        AND c.ProductTypeCode = a.ProductTypeCode 
                        LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
                        AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
                        LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                        a.BrandCode=b.BrandCode
                        AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                        AND a.Series=b.Series AND a.Type=b.Type 
                        AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                        AND a.Year > b.Year 
                        INNER JOIN dbo.OrderSimulationMV omv ON omv.VehicleCode = a.VehicleCode
                        INNER JOIN dbo.OrderSimulation os ON os.OrderNo = omv.OrderNo
                        INNER JOIN dbo.ProspectCustomer p ON p.CustID = os.CustID
                        WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND omv.RowStatus = 1
						AND os.CustID = @0 AND os.FollowUpNo = @1 AND b.Year IS NULL 
                        ORDER BY a.VehicleCode";
                                vData = dbMobile.Query<Otosales.Models.vWeb2.VehicleData>(queryVehicleData, CustID, FollowUpNo).FirstOrDefault();
                                if (vData != null)
                                {
                                    queryVehicleData = @";SELECT mbc.ID BasicCoverID FROM dbo.OrderSimulation os 
                                                INNER JOIN dbo.Mst_Basic_Cover mbc
                                                ON mbc.ComprePeriod = os.ComprePeriod AND mbc.TLOPeriod = os.TLOPeriod
                                                WHERE OrderNo = @0 AND os.ApplyF = 1 AND os.RowStatus = 1";
                                    List<dynamic> bscCvr = dbMobile.Fetch<dynamic>(queryVehicleData, vData.OrderNo);
                                    if (bscCvr.Count > 1)
                                    {
                                        queryVehicleData = @"SELECT RTRIM(LTRIM(CoverageID)) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                                        List<string> ListCvrID = dbMobile.Fetch<string>(queryVehicleData, vData.OrderNo);
                                        if (ListCvrID.Contains("TLO"))
                                        {
                                            queryVehicleData = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description = 'TLO Others'";
                                            int id = dbMobile.ExecuteScalar<int>(queryVehicleData);
                                            vData.BasicCoverID = id;
                                        }
                                        else if (ListCvrID.Contains("ALLRIK"))
                                        {
                                            queryVehicleData = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description = 'Comprehensive Others'";
                                            int id = dbMobile.ExecuteScalar<int>(queryVehicleData);
                                            vData.BasicCoverID = id;
                                        }
                                    }
                                    else
                                    {
                                        if (bscCvr.Count > 0)
                                        {
                                            vData.BasicCoverID = bscCvr.First().BasicCoverID;
                                        }
                                        else
                                        {
                                            vData.BasicCoverID = 0;
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                //throw;
                            }
                        });

                        Thread threadVehicleData2 = new Thread(() =>
                        {
                            try
                            {
                                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                                string queryVehicleData = @";SELECT OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1 ";
                                string OldPolicyNo = dbMobile.FirstOrDefault<string>(queryVehicleData, CustID, FollowUpNo);

                                if (!string.IsNullOrEmpty(OldPolicyNo))
                                {
                                    queryVehicleData = @";DECLARE @@PolicyID VARCHAR(100)
                                SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
                                SELECT STUFF(( SELECT ', ' + Name
                                FROM dbo.Dtl_Original_Defect 
                                where Policy_Id = @@PolicyID
                                FOR XML PATH('')
                                ), 1, 1, '')  AS OriginalDefect,
								STUFF(( SELECT ', ' + Name
                                FROM dbo.Ren_Dtl_NoCover 
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS NoCover,
								STUFF(( SELECT ', ' + RTRIM(CONCAT(AccsDescription, ' ', AccsCatDescription, ' ',
                                AccsPartDescription))
                                FROM    ( SELECT DISTINCT
                                                    RTRIM(ISNULL(wnsa.Policy_No, '')) AS PolicyNo ,
                                                    wnsa.Object_No AS ObjectNo ,
                                                    RTRIM(ISNULL(wnsa.Accs_Code, '')) AS AccsCode ,
                                                    RTRIM(ISNULL(AO.AccsDescription, '')) AS AccsDescription ,
                                                    RTRIM(ISNULL(wnsa.Accs_Part_Code, '')) AS AccsPartCode ,
                                                    CASE ao.AccsType
                                                      WHEN 4 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      WHEN 5 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      ELSE ''
                                                    END AS AccsCatDescription ,
                                                    RTRIM(ISNULL(ap.AccsPartDescription, '')) AS AccsPartDescription ,
                                                    Include_Tsi AS IncludeTsi ,
                                                    RTRIM(ISNULL(Brand, '')) AS Brand ,
                                                    Sum_Insured AS SumInsured ,
                                                    Quantity ,
                                                    Premi ,
                                                    RTRIM(ISNULL(Category, '')) AS Category ,
                                                    RTRIM(ISNULL(wnsa.Accs_Cat_Code, '')) AS AccsCatCode
                                          FROM      dbo.Ren_Dtl_Non_Standard_Accessories AS wnsa
                                                    LEFT JOIN dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.Accs_Code
                                                                                          AND ao.RowStatus = 1
                                                    LEFT JOIN AccessoriesPart AP ON Ap.AccsCode = wnsa.Accs_Code
                                                                                    AND AP.AccsPartCode = wnsa.Accs_Part_Code
                                                                                    AND ap.RowStatus = 1
                                                    LEFT JOIN ( SELECT  RTRIM(ISNULL(ac.AccsCode, '')) AS AccsCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatCode, '')) AS AccsCatCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatDescription, '')) AS AccsCatDescription ,
                                                                        RTRIM(ISNULL(acp.AccsPartCode, '')) AS AccsPartCode ,
                                                                        ac.MaxSICategory ,
                                                                        ac.IsEditable ,
                                                                        acp.QtyDefault ,
                                                                        ac.RowStatus
                                                                FROM    dbo.AccessoriesCategory AS ac
                                                                        LEFT JOIN dbo.AccessoriesCategoryPart
                                                                        AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                                                  AND acp.RowStatus = 1
                                                              ) x ON x.AccsCode = ao.AccsCode
                                                                     AND x.RowStatus = 1
                                                                     AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                                                              '')
                                                                     AND x.AccsCode = wnsa.Accs_Code
                                                                     AND x.AccsCatCode = wnsa.Accs_Cat_Code
                                          WHERE     Policy_No = @0
                                        ) AS X
                                FOR XML PATH('')
                                ), 1, 1, '')  AS Accessories";

                                    var AABdbThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                                    List<dynamic> list = AABdbThread.Fetch<dynamic>(queryVehicleData, OldPolicyNo);
                                    if (list.Count > 0)
                                    {
                                        vData.ORDefectsDesc = list.First().OriginalDefect;
                                        vData.NoCover = list.First().NoCover;
                                        vData.Accessories = list.First().Accessories;
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                //throw;
                            }
                        });
                        threadVehicleData1.Start();
                        threadVehicleData2.Start();

                        threadVehicleData1.Join();
                        threadVehicleData2.Join();
                        result.VehicleData = vData;
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadPolicyAddress = new Thread(() =>
                {
                    try
                    {
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryPolicyAddress = @";SELECT PolicySentTo SentTo, PolicyDeliveryName Name, PolicyDeliveryAddress Address, PolicyDeliveryPostalCode PostalCode FROM dbo.OrderSimulation WHERE CustID = @0 
                            AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1";
                        Otosales.Models.vWeb2.PolicyAddress pa = dbMobile.Query<Otosales.Models.vWeb2.PolicyAddress>(queryPolicyAddress, CustID, FollowUpNo).FirstOrDefault();
                        if (pa != null)
                        {
                            result.PolicyAddres = pa;
                        }
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadSurveySchedule = new Thread(() =>
                {
                    try
                    {
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string querySurveySchedule = @";SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc, CAST(COALESCE(IsManualSurvey,0) AS BIT) IsManualSurvey
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                        Otosales.Models.vWeb2.SurveySchedule ss = dbMobile.Fetch<Otosales.Models.vWeb2.SurveySchedule>(querySurveySchedule, CustID, FollowUpNo).FirstOrDefault();
                        result.SurveySchedule = ss;
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });


                threadPremi.Start();
                threadPersonalData.Start();
                threadCompanyData.Start();
                threadVehicleData.Start();
                threadPolicyAddress.Start();
                threadSurveySchedule.Start();

                threadPremi.Join();
                threadPersonalData.Join();
                threadCompanyData.Join();
                threadVehicleData.Join();
                threadPolicyAddress.Join();
                threadSurveySchedule.Join();

                result.PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo();


                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

    }
}