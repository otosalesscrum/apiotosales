﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.Reflection;

namespace Otosales.Repository
{
    public class OtosalesRepositoryBase
    {

        #region Connection String
        protected const string CONNECTION_STRING_MOBILESURVEYDB = OtosalesConfig.CONNECTION_STRING_MOBILESURVEYDB;
        protected const string CONNECTION_STRING_AABDB = OtosalesConfig.CONNECTION_STRING_AABDB;
        protected const string CONNECTION_STRING_AABMobile = OtosalesConfig.CONNECTION_STRING_AABMobile;
        protected const string CONNECTION_STRING_CPSDB = OtosalesConfig.CONNECTION_STRING_CPSDB;
        protected const string CONNECTION_STRING_BEYONDDB = OtosalesConfig.CONNECTION_STRING_BEYONDDB;
        protected const string CONNECTION_STRING_AABImageDB = OtosalesConfig.CONNECTION_STRING_AABImageDB;
        protected const string CONNECTION_STRING_XOOMDB = OtosalesConfig.CONNECTION_STRING_XOOMDB;
        protected const string CONNECTION_STRING_AntrianDB = OtosalesConfig.CONNECTION_STRING_AntrianDB;
        protected const string CONNECTION_STRING_GASI = OtosalesConfig.CONNECTION_STRING_GASI;
        protected const string CONNECTION_STRING_a2isAuthorizationDB = OtosalesConfig.CONNECTION_STRING_a2isAuthorizationDB;
        protected const string CONNECTION_STRING_SURVEYMANAGEMENTDB = OtosalesConfig.CONNECTION_STRING_SURVEYMANAGEMENTDB;
        protected const string CONNECTION_STRING_GardaMobileDB = OtosalesConfig.CONNECTION_STRING_GARDAMOBILEDB;
        #endregion

        protected static readonly a2isLogHelper _logger = new a2isLogHelper();

        #region Get Connection String
        protected static a2isDBHelper.Database GetMobileSurveyDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_MOBILESURVEYDB);
        }
        protected static a2isDBHelper.Database GetGardaMobileDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_GardaMobileDB);
        }
        protected static a2isDBHelper.Database GetAABDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_AABDB);
        }
        protected static a2isDBHelper.Database GetAABMobileDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_AABMobile);
        }
        protected static a2isDBHelper.Database GetCPSDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_CPSDB);
        }
        protected static a2isDBHelper.Database GetBeyondDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_BEYONDDB);
        }
        protected static a2isDBHelper.Database GetAABImageDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_AABImageDB);
        }
        protected static a2isDBHelper.Database GetXOOMDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_XOOMDB);
        }
        protected static a2isDBHelper.Database GetAntrianDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_AntrianDB);
        }
        protected static a2isDBHelper.Database GetGASIDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_GASI);
        }
        protected static a2isDBHelper.Database Geta2isAuthorizationDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_a2isAuthorizationDB);
        }
        protected static a2isDBHelper.Database GetSurveyManagementDB()
        {
            return new a2isDBHelper.Database(CONNECTION_STRING_SURVEYMANAGEMENTDB);
        }
        #endregion

        [MethodImpl(MethodImplOptions.NoInlining)]
        protected static string GetCurrentMethod()
        {
            StackTrace st = new StackTrace();
            StackFrame sf = st.GetFrame(1);

            MethodBase method = sf.GetMethod();

            return string.Format("{0}.{1}", method.DeclaringType, method.Name);
        }
    }
}