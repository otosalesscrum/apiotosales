﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using Otosales.Models;

namespace Otosales.Repository.vWeb2
{
    public class DashboardRepository : ApiController
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        public static string qSPGetSubordinate = @";EXEC usp_GetOtosalesSubordinate @@SuperiorID = @0";

        public static string qGetParam = @"SELECT [Type],[Param] 
                                        FROM aabhead 
                                        WHERE Value = @0 
                                        AND RowStatus = 1";

        #region Query DefaultDashboard
        public static string qDefaultDashboardSummary = @";EXEC [dbo].[usp_GetDashboardSummaryOtosales] @@Role =@0,@@UserID =@1,@@Period =@2,@@StateDashboard=@3,@Param=@4";

        public static string qDefaultDashboardDetail = @";EXEC [dbo].[usp_GetDashboardDetailOtosales] @@Role =@0,@@UserID =@1,@@Period =@2,@@StateDashboard=@3,@Param=@4";
        #endregion

        #region Query Get Dashboard National
        private static string qGetDashboardSummaryNational = @"SELECT ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio, ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'NATIONALMGR' AS DetailDashboardState
FROM AABRegion ar INNER JOIN FollowUpSummary fu ON fu.Region = ar.RegionCode AND ar.RowStatus = 1 
INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
WHERE fu.[National] IN ({0}) 
AND fu.RowStatus = 1
AND Year(Period) = Year(GETDATE()) 
{1}
AND Frequency =@0";

        private static string qGetDashboardDetailNational = @"SELECT CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
REPLACE(ar.RegionCode, 'A', '') AS ID , ar.RegionDescription AS Label, 'REGIONALMGR' AS StateDashboard
FROM AABRegion ar INNER JOIN FollowUpSummary fu ON fu.Region = ar.RegionCode AND ar.RowStatus = 1 
INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
WHERE fu.[National] IN ({0}) 
AND fu.RowStatus = 1
AND Year(Period) = Year(GETDATE()) 
{1}
AND Frequency = @0
GROUP BY ar.RegionDescription,REPLACE(ar.RegionCode, 'A', '')";
        #endregion

        #region Query Get Dashboard National AGENT
        private static string qGetDashboardSummaryNationalAGT = @"SELECT ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio, ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'NATIONALMGR' AS DetailDashboardState
FROM AABRegion ar INNER JOIN FollowUpSummary fu ON fu.Region = ar.RegionCode AND ar.RowStatus = 1 
INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
WHERE fu.[National] IN ({0}) 
AND fu.RowStatus = 1
AND Year(Period) = Year(GETDATE()) 
{1}
AND Frequency =@0";

        private static string qGetDashboardDetailNationalAGT = @"SELECT CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
ar.RegionCode AS ID , ar.RegionDescription AS Label, 'REGIONALMGR' AS StateDashboard
FROM AABRegion ar INNER JOIN FollowUpSummary fu ON fu.Region = ar.RegionCode AND ar.RowStatus = 1
INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
WHERE fu.[National] IN ({0}) 
AND fu.RowStatus = 1
AND Year(Period) = Year(GETDATE()) 
{1}
AND Frequency = @0
GROUP BY ar.RegionDescription,ar.RegionCode";
        #endregion

        #region Query Get Dashboard Regional
        private static string qGetDashboardSummaryRegional = @"select  ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio,ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'REGIONALMGR' AS DetailDashboardState
		 from aabbranch ab inner join followupsummary fu 
on ab.branchcode = fu.branchcode inner join salesofficer so on so.salesofficerid = fu.salesofficerid AND so.RowStatus = 1
where fu.[Region] IN ({0}) 
AND fu.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0
and so.role not in ('AGENCYMGR','TELESALES','TELERENEWAL','TELEMGR')";

        private static string qGetDashboardDetailRegional = @"SELECT CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
'0' + SUBSTRING(REPLACE(ab.branchcode, 'A', ''),2,3) AS ID , (select Description from aabbranch where branchcode = '0' + SUBSTRING(REPLACE(ab.branchcode, 'A', ''),2,3)) AS Label, 'BRANCHMGR' AS StateDashboard
from aabbranch ab inner join followupsummary fu 
on ab.branchcode = fu.branchcode inner join salesofficer so on so.salesofficerid = fu.salesofficerid AND so.RowStatus = 1
where fu.[Region] IN ({0}) 
AND fu.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0
and so.role not in ('AGENCYMGR','TELESALES','TELERENEWAL','TELEMGR')
group by '0' + SUBSTRING(REPLACE(ab.branchcode, 'A', ''),2,3)";
        #endregion

        #region Query Get Dashboard Regional AGENT
        private static string qGetDashboardSummaryRegionalAGT = @"select  ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio,ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'REGIONALMGR' AS DetailDashboardState
		 from aabbranch ab inner join followupsummary fu 
on ab.branchcode = fu.branchcode inner join salesofficer so on so.salesofficerid = fu.salesofficerid AND so.RowStatus = 1
where fu.[Region] IN ({0}) 
AND fu.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0";

        private static string qGetDashboardDetailRegionalAGT = @"SELECT CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
ab.branchcode AS ID , (select Description from aabbranch where branchcode = '0' + SUBSTRING(REPLACE(ab.branchcode, 'A', ''),2,3)) AS Label, 'BRANCHMGR' AS StateDashboard
from aabbranch ab inner join followupsummary fu 
on ab.branchcode = fu.branchcode inner join salesofficer so on so.salesofficerid = fu.salesofficerid AND so.RowStatus = 1
where fu.[Region] IN ({0}) 
AND fu.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0
group by ab.branchcode";
        #endregion

        #region Query Get Dashboard Branch
        private static string qGetDashboardSummaryBranch = @"select  ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio,ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'BRANCHMGR' AS DetailDashboardState
		 from salesofficer so inner join 
followupsummary fu on so.salesofficerid = fu.salesofficerid 
where fu.[BranchCode] IN ({0}) 
AND fu.RowStatus = 1
AND so.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0
and so.role not in ('AGENCYMGR','TELESALES','TELERENEWAL','TELEMGR')";

        private static string qGetDashboardDetailBranch = @"select CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
so.SalesOfficerID AS ID , so.Name AS Label
		, CASE WHEN ISNULL((SELECT so3.Role FROM SalesOfficer so3 WHERE so3.SalesOfficerId = so.SalesOfficerId),'') = 'SALESSECHEAD' THEN 'SALESSECHEAD' ELSE 'SALESOFFICER' END AS StateDashboard
from salesofficer so inner join 
followupsummary fu on so.salesofficerid = fu.salesofficerid 
where fu.[BranchCode] IN ({0}) 
AND fu.RowStatus = 1
AND so.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0
and so.role not in ('AGENCYMGR','TELESALES','TELERENEWAL','TELEMGR')
group by so.SalesOfficerID, so.Name
";
        #endregion

        #region Query Get Dashboard Branch AGENT
        private static string qGetDashboardSummaryBranchAGT = @"select  ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio,ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'BRANCHMGR' AS DetailDashboardState
		 from salesofficer so inner join 
followupsummary fu on so.salesofficerid = fu.salesofficerid 
where fu.[BranchCode] IN ({0}) 
AND fu.RowStatus = 1
AND so.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0";

        private static string qGetDashboardDetailBranchAGT = @"select CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
so.SalesOfficerID AS ID , so.Name AS Label
		, CASE WHEN ISNULL((SELECT so3.Role FROM SalesOfficer so3 WHERE so3.SalesOfficerId = so.SalesOfficerId),'') = 'SALESSECHEAD' THEN 'SALESSECHEAD' ELSE 'SALESOFFICER' END AS StateDashboard
from salesofficer so inner join 
followupsummary fu on so.salesofficerid = fu.salesofficerid 
where fu.[BranchCode] IN ({0}) 
AND fu.RowStatus = 1
AND so.RowStatus = 1
AND Year(fu.Period) = Year(GETDATE()) 
{1}
AND fu.Frequency = @0
group by so.SalesOfficerID, so.Name
";
        #endregion

        #region Query Get Dashboard SalesSecHead
        private static string qGetDashboardSummarySalesSecHead= @"SELECT ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio,ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'SALESSECHEAD' AS DetailDashboardState
FROM Followupsummary fu 
INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
WHERE fu.salesofficerid in (select param from aabhead where value = @0)
AND fu.Frequency = @1
AND fu.RowStatus = 1
AND Year(Period) = Year(GETDATE()) 
{0}";

        private static string qGetDashboardDetailSalesSecHead = @"SELECT CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
fu.SalesOfficerID AS ID , fu.salesName AS Label, 'SALESOFFICER' AS StateDashboard
FROM Followupsummary fu 
INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
WHERE fu.salesofficerid in (select param from aabhead where value = @0)
AND fu.Frequency = @1
AND fu.RowStatus = 1
AND Year(Period) = Year(GETDATE()) 
{0}
group by fu.[SalesOfficerID], fu.salesName";
        #endregion

        #region Query Get Dashboard SalesOfficer
        private static string qGetDashboardSummarySalesOfficer = @"SELECT ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL(( SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio,ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'SALESOFFICER' AS DetailDashboardState
		from followupsummary fu 
        INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
        where fu.salesofficerid = @0
        AND fu.RowStatus = 1
        AND Year(Period) = Year(GETDATE()) 
        {0}
        AND Frequency = @1
        {1}";

        private static string qGetDashboardDetailSalesOfficer = @"SELECT CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
        fu.SalesOfficerID AS ID , fu.salesname AS Label, 'SALESOFFICER' AS StateDashboard
        from followupsummary fu 
        INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
        where fu.salesofficerid = @0
        AND fu.RowStatus = 1
        AND Year(Period) = Year(GETDATE()) 
        {0}
        AND Frequency = @1
        {1}
        group by fu.SalesOfficerID,fu.salesname";
        #endregion

        #region Query Get Dashboard Tele
        private static string qGetDashboardSummaryTeleMgr = @"
SELECT ISNULL((SUM(TotalDeal)),0) AS DealTotalOrder, ISNULL((SUM(TotalCallLater)),0) AS CallLaterTotalOrder, 
        ISNULL((SUM(TotalNeedFU)),0) AS NeedFUTotalOrder, ISNULL((SUM(TotalNotDeal)),0) AS NotDealTotalOrder, 
        ISNULL((SUM(TotalRejected)),0) AS RejectedTotalOrder, ISNULL((SUM(TotalPotential)),0) AS PotentialTotalOrder, 
        ISNULL((SUM(TotalPremiDeal)),0) AS DealTotalPremi, ISNULL((SUM(TotalPremiCallLater)),0) AS CallLaterTotalPremi, 
        ISNULL((SUM(TotalPremiNeedFU)),0) AS NeedFUTotalPremi, ISNULL((SUM(TotalPremiNotDeal)),0) AS NotDealTotalPremi, 
        ISNULL((SUM(TotalPremiRejected)),0) AS RejectedTotalPremi, ISNULL((SUM(TotalPremiPotential)),0) AS PotentialTotalPremi, 
        ISNULL((SUM(TotalPremiNew)),0) AS NewTotalPremi, ISNULL((SUM(TotalNew)),0) AS NewTotalPolicy, 
        ISNULL((SUM(TotalPremiRenew)),0) AS RenewTotalPremi, ISNULL((SUM(TotalRenew)),0) AS RenewTotalPolicy, 
        ISNULL((SUM(TotalPremiEndorse)),0) AS EndorseTotalPremi, ISNULL((SUM(TotalEndorse)),0) AS EndorseTotalPolicy, 
        ISNULL((SUM(TotalPremiCancel)),0) AS CancelTotalPremi, ISNULL((SUM(TotalCancel)),0) AS CancelTotalPolicy,
        ISNULL((SUM(RenewalRatio)),0) AS RenewalRatio,ISNULL((SUM(TotalPremiNew+TotalPremiRenew+TotalPremiEndorse+TotalPremiCancel)),0) AS TotalPremi,'TELEMGR' AS DetailDashboardState
FROM FollowUpSummary fu 
INNER JOIN SalesOfficer so ON so.SalesOfficerID = fu.SalesOfficerID AND so.RowStatus = 1
WHERE fu.[SalesOfficerID] IN (select param from aabhead where type = 'tele' and value = @1)
AND fu.RowStatus = 1
AND Year(Period) = Year(GETDATE()) AND Frequency = @0
{0}";

        private static string qGetDashboardDetailTeleMgr = @"
        SELECT CAST(( ISNULL((SUM(TotalPremiNew)),0) + ISNULL((SUM(TotalPremiReNew)),0) + ISNULL((SUM(TotalPremiEndorse)),0) + ISNULL((SUM(TotalPremiCancel)),0))AS decimal) AS Value, 
        so.SalesOfficerID AS ID , so.Name AS Label, 'SALESOFFICER' AS StateDashboard
        FROM SalesOfficer so
        left join FollowUpSummary fu on so.salesofficerid = fu.salesofficerid {0}
        WHERE so.[SalesOfficerID] IN (select param from aabhead where type = 'tele' and value = @1)
        AND (fu.RowStatus = 1 OR fu.RowStatus is null) 
        AND (fu.Frequency = @0 OR fu.Frequency is null)
        AND so.RowStatus = 1
        GROUP BY so.[SalesOfficerID], so.Name";
        #endregion
        public static DashboardResult GenerateDashboard(string role, string superiorID, string periodView, string stateDashboard,string param)
        {
            /* Dashboard National - Menampilkan semua data region [AAB,AABAGT],[Region, ar.RegionDescription]
             * Dashboard Region - Menampilkan semua data branch ,[]
             */
            
            DashboardResult result = new DashboardResult();
            List<dynamic> dataID;
            string ParamUser = string.Empty;
            string TypeUser = string.Empty;
            string baseQuerySummary = qDefaultDashboardSummary;
            string baseQueryDetail = qDefaultDashboardDetail;
            List<DashboardDetail> listDetail = new List<DashboardDetail>();
            DashboardSummary dataSummary = new DashboardSummary();

            string Access = string.Empty;
            string DKINonDKI = string.Empty;
            string query = string.Empty;
            string scriptPeriodNational = string.Empty;
            string scriptPeriod = string.Empty;

            try
            {

                /*
                 * DONE :
                 * National Manager - Fix
                 * Regional Manager - Fix
                 * Branch Manager - Fix
                 * Tele Manager - Fix 
                 * Sales Section Head - Fix
                 * Sales Officer - Fix
                 * Agency Manager - Fix
                 * Agency sales Officer - Fix
                 */
                Access = CheckAccess(role, superiorID);

                if (periodView.Trim().Equals("MONTHLY", StringComparison.InvariantCultureIgnoreCase))
                {
                    scriptPeriodNational = " AND MONTH(Period) = MONTH(GETDATE())";
                    scriptPeriod = " AND (Year(fu.Period) = Year(GETDATE()) AND MONTH(fu.Period) = MONTH(GETDATE()))";
                }
                else
                {
                    scriptPeriodNational = "";
                    scriptPeriod = "AND Year(fu.Period) = Year(GETDATE())";
                }
                

                if (Access.Equals("HO", StringComparison.InvariantCultureIgnoreCase))
                {
                    #region National Manager
                    if (role.Equals("NATIONALMGR", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (stateDashboard.Equals("NATIONALMGR", StringComparison.InvariantCultureIgnoreCase)) //DONE
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummaryNational,"'AAB','AABAGT'", scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query,periodView);
                                query = String.Format(qGetDashboardDetailNational, "'AAB','AABAGT'", scriptPeriodNational);
                                listDetail = db.Fetch<DashboardDetail>(query,periodView);
                            }
                        }
                        else if (stateDashboard.Equals("REGIONALMGR", StringComparison.InvariantCultureIgnoreCase)) //DONE
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummaryRegional, "'"+param+"','A"+param+"','1"+ param.Substring(1, 2) + "'", scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                query = String.Format(qGetDashboardDetailRegional, "'" + param + "','A" + param + "','1" + param.Substring(1, 2) + "'", scriptPeriodNational);
                                listDetail = db.Fetch<DashboardDetail>(query, periodView);
                            }
                        }
                        else if (stateDashboard.Equals("BRANCHMGR", StringComparison.InvariantCultureIgnoreCase)) //DONE
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummaryBranch, "'" + param + "','A" + param + "','1" + param.Substring(1, 2) + "'", scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                query = String.Format(qGetDashboardDetailBranch, "'" + param + "','A" + param + "','1" + param.Substring(1, 2) + "'", scriptPeriodNational);
                                listDetail = db.Fetch<DashboardDetail>(query, periodView);
                            }
                        }
                        else //SALESOFFICER
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile")) //DONE
                            {
                                query = String.Format(qGetDashboardSummarySalesOfficer, scriptPeriodNational, "");
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                query = String.Format(qGetDashboardDetailSalesOfficer, scriptPeriodNational, "");
                                listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                            }
                        }
                    }
                    #endregion
                    #region Regional Manager
                    else if (role.Equals("REGIONALMGR", StringComparison.InvariantCultureIgnoreCase))
                    {
                        DKINonDKI = CheckDKINonDKI(superiorID);
                        #region DKI
                        if (DKINonDKI.Equals("DKI", StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (stateDashboard.Equals("REGIONALMGR", StringComparison.InvariantCultureIgnoreCase)) //DONE
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummaryRegional, @"SELECT region FROM aabbranch WHERE RowStatus = 1
                                AND branchcode in (select branchcode from salesofficer where salesofficerid = '" + superiorID + @"' UNION ALL 
                                select '1' + SUBSTRING(BranchCode,2,3) from salesofficer where salesofficerid = '" + superiorID + "')", scriptPeriodNational + "");
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                    query = String.Format(qGetDashboardDetailRegional, @"SELECT region FROM aabbranch WHERE RowStatus = 1
                                AND branchcode in (select branchcode from salesofficer where salesofficerid = '" + superiorID + @"' UNION ALL 
                                select '1' + SUBSTRING(BranchCode,2,3) from salesofficer where salesofficerid = '" + superiorID + "')", scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, periodView);
                                }
                            }
                            else if (stateDashboard.Equals("BRANCHMGR", StringComparison.InvariantCultureIgnoreCase))
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummaryBranch, "'" + param + "','1" + param.Substring(1, 2) + "'",  scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                    query = String.Format(qGetDashboardDetailBranch, "'" + param + "','1" + param.Substring(1, 2) + "'", scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, periodView);
                                }
                            }
                            else if (stateDashboard.Equals("SALESSECHEAD", StringComparison.InvariantCultureIgnoreCase))
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesSecHead,scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, superiorID, periodView);
                                    query = String.Format(qGetDashboardDetailSalesSecHead, scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, superiorID, periodView);
                                }
                            }
                            else //SALESOFFICER
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesOfficer , scriptPeriodNational, @"AND fu.region in (select ab.region from salesofficer so 
                                    inner join aabbranch ab on ab.branchcode = so.branchcode where salesofficerid ='"+superiorID+"')");
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                    query = String.Format(qGetDashboardDetailSalesOfficer , scriptPeriodNational, @"AND fu.region in (select ab.region from salesofficer so 
                                    inner join aabbranch ab on ab.branchcode = so.branchcode where salesofficerid ='" + superiorID + "')");
                                    listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                                }
                            }
                        }
                        #endregion
                        #region Non DKI
                        else
                        {
                            if (stateDashboard.Equals("REGIONALMGR", StringComparison.InvariantCultureIgnoreCase)) //DONE
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummaryRegional, @"SELECT region FROM aabbranch WHERE RowStatus = 1
                                AND (branchcode in (select branchcode from salesofficer where salesofficerid = '" + superiorID + @"')
                                OR branchcode in (select 'A'+branchcode from salesofficer where salesofficerid = '" + superiorID + @"')
                                OR branchcode in (select '1' + SUBSTRING(BranchCode,2,3) from salesofficer where salesofficerid = '" + superiorID + "'))", scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                    query = String.Format(qGetDashboardDetailRegional, @"SELECT region FROM aabbranch WHERE RowStatus = 1
                                AND (branchcode in (select branchcode from salesofficer where salesofficerid = '" + superiorID + @"')
                                OR branchcode in (select 'A'+branchcode from salesofficer where salesofficerid = '" + superiorID + @"')
                                OR branchcode in (select '1' + SUBSTRING(BranchCode,2,3) from salesofficer where salesofficerid = '" + superiorID + "'))", scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, periodView);
                                }
                            }
                            else if (stateDashboard.Equals("BRANCHMGR", StringComparison.InvariantCultureIgnoreCase))
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummaryBranch, "'" + param + "','A"+param+ "','1" + param.Substring(1, 2) + "'", scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                    query = String.Format(qGetDashboardDetailBranch, "'" + param + "','A" + param + "','1" + param.Substring(1, 2) + "'", scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, periodView);
                                }
                            }
                            else if (stateDashboard.Equals("SALESSECHEAD", StringComparison.InvariantCultureIgnoreCase))
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesSecHead, scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, superiorID, periodView);
                                    query = String.Format(qGetDashboardDetailSalesSecHead, scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, superiorID, periodView);
                                }
                            }
                            else //SALESOFFICER
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesOfficer, scriptPeriodNational, @"AND fu.region in (select ab.region from salesofficer so 
                                    inner join aabbranch ab on ab.branchcode = so.branchcode where salesofficerid ='" + superiorID + "')");
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                    query = String.Format(qGetDashboardDetailSalesOfficer, scriptPeriodNational, @"AND fu.region in (select ab.region from salesofficer so 
                                    inner join aabbranch ab on ab.branchcode = so.branchcode where salesofficerid ='" + superiorID + "')");
                                    listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                                }
                            }
                        }
                        #endregion
                    }
                    #endregion
                    #region Branch Manager
                    else if (role.Equals("BRANCHMGR", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (stateDashboard.Equals("BRANCHMGR", StringComparison.InvariantCultureIgnoreCase))
                        {
                            DKINonDKI = CheckDKINonDKI(superiorID);
                            if (DKINonDKI.Equals("DKI", StringComparison.InvariantCultureIgnoreCase))
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummaryBranch, @"select [param] from AABHead where value = '" + superiorID + @"'UNION ALL 
                                        select '1' + SUBSTRING([param],2,3) from AABHead where value = '" + superiorID + @"'", scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                    query = String.Format(qGetDashboardDetailBranch, @"select [param] from AABHead where value = '" + superiorID + @"'UNION ALL 
                                        select '1' + SUBSTRING([param],2,3) from AABHead where value = '" + superiorID + @"'", scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, periodView);
                                }
                            }
                            else
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummaryBranch, @"select [param] from AABHead where value = '" + superiorID + @"' UNION ALL 
                                        select 'A'+[param] from AABHead where value = '" + superiorID + @"' UNION ALL 
                                        select '1' + SUBSTRING([param],2,3) from AABHead where value = '" + superiorID + @"'", scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                    query = String.Format(qGetDashboardDetailBranch, @"select [param] from AABHead where value = '" + superiorID + @"' UNION ALL 
                                        select 'A'+[param] from AABHead where value = '" + superiorID + @"' UNION ALL 
                                        select '1' + SUBSTRING([param],2,3) from AABHead where value = '" + superiorID + @"'", scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, periodView);
                                }
                            }
                        }
                        else if (stateDashboard.Equals("SALESSECHEAD", StringComparison.InvariantCultureIgnoreCase))
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummarySalesSecHead, scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, superiorID, periodView);
                                query = String.Format(qGetDashboardDetailSalesSecHead, scriptPeriodNational);
                                listDetail = db.Fetch<DashboardDetail>(query, superiorID, periodView);
                            }
                        }
                        else //SALESOFFICER
                        {
                            if (DKINonDKI.Equals("DKI", StringComparison.InvariantCultureIgnoreCase))
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesOfficer, scriptPeriodNational, @"
                                AND fu.branchcode in (select [param] from AABHead where value ='" + superiorID + @"' UNION ALL 
                                        select '1' + SUBSTRING([param], 2, 3) from AABHead where value = '" + superiorID + @"')");
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                    query = String.Format(qGetDashboardDetailSalesOfficer, scriptPeriodNational, @"
                                AND fu.branchcode in (select [param] from AABHead where value ='" + superiorID + @"' UNION ALL 
                                        select '1' + SUBSTRING([param], 2, 3) from AABHead where value = '" + superiorID + @"')");
                                    listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                                }
                            }
                            else
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesOfficer, scriptPeriodNational, @"
                                AND fu.branchcode in (select [param] from AABHead where value ='" + superiorID + @"' UNION ALL
                                select 'A'+[param] from AABHead where value = '" + superiorID + @"' UNION ALL 
                                        select '1' + SUBSTRING([param],2,3) from AABHead where value = '" + superiorID + @"')");
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                    query = String.Format(qGetDashboardDetailSalesOfficer, scriptPeriodNational, @"
                                AND fu.branchcode in (select [param] from AABHead where value ='" + superiorID + @"' UNION ALL
                                select 'A'+[param] from AABHead where value = '" + superiorID + @"' UNION ALL 
                                        select '1' + SUBSTRING([param],2,3) from AABHead where value = '" + superiorID + @"')");
                                    listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                                }
                            }
                            
                        }
                    }
                    #endregion
                    #region Sales Sec Head
                    else if (role.Equals("SALESSECHEAD", StringComparison.InvariantCultureIgnoreCase))
                    {
                            if (stateDashboard.Equals("SALESSECHEAD", StringComparison.InvariantCultureIgnoreCase))
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesSecHead, scriptPeriodNational);
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, superiorID, periodView);
                                    query = String.Format(qGetDashboardDetailSalesSecHead, scriptPeriodNational);
                                    listDetail = db.Fetch<DashboardDetail>(query, superiorID, periodView);
                                }
                            }
                            else //SALESOFFICER
                            {
                                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                                {
                                    query = String.Format(qGetDashboardSummarySalesOfficer, scriptPeriodNational, @"");
                                    dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                    query = String.Format(qGetDashboardDetailSalesOfficer, scriptPeriodNational, @"");
                                    listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                                }
                            }
                    }
                    #endregion
                    #region Tele Manager
                    else if (role.Equals("TELEMGR", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (stateDashboard.Equals("TELEMGR", StringComparison.InvariantCultureIgnoreCase))
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummaryTeleMgr,scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView, superiorID);
                                query = String.Format(qGetDashboardDetailTeleMgr,scriptPeriod);
                                listDetail = db.Fetch<DashboardDetail>(query, periodView, superiorID);
                            }
                        }
                        else //SALESOFFICER
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummarySalesOfficer, "", scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                query = String.Format(qGetDashboardDetailSalesOfficer, "", scriptPeriod);
                                listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                            }
                        }
                    }
                    #endregion
                    #region Agency Manager
                    else if (role.Equals("AGENCYMGR", StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (stateDashboard.Equals("AGENCYMGR", StringComparison.InvariantCultureIgnoreCase))
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummaryNationalAGT, "'AABAGT'",scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                query = String.Format(qGetDashboardDetailNationalAGT, "'AABAGT'",scriptPeriodNational);
                                listDetail = db.Fetch<DashboardDetail>(query, periodView);
                            }
                        }
                        else if (stateDashboard.Equals("REGIONALMGR", StringComparison.InvariantCultureIgnoreCase))
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummaryRegionalAGT, "'" + param + "'", scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                query = String.Format(qGetDashboardDetailRegionalAGT, "'" + param + "'", scriptPeriodNational);
                                listDetail = db.Fetch<DashboardDetail>(query, periodView);
                            }
                        }
                        else if (stateDashboard.Equals("BRANCHMGR", StringComparison.InvariantCultureIgnoreCase))
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummaryBranchAGT, "'" + param + "'", scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, periodView);
                                query = String.Format(qGetDashboardDetailBranchAGT, "'" + param + "'", scriptPeriodNational);
                                listDetail = db.Fetch<DashboardDetail>(query, periodView);
                            }
                        }
                        else //SALESOFFICER
                        {
                            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                            {
                                query = String.Format(qGetDashboardSummarySalesOfficer, "",scriptPeriodNational);
                                dataSummary = db.FirstOrDefault<DashboardSummary>(query, param, periodView);
                                query = String.Format(qGetDashboardDetailSalesOfficer, "",scriptPeriod);
                                listDetail = db.Fetch<DashboardDetail>(query, param, periodView);
                            }
                        }
                    }
                    #endregion
                    #region Sales Officer
                    else //SALESOFFICER
                    {
                        using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                        {
                            query = String.Format(qGetDashboardSummarySalesOfficer, "",scriptPeriodNational);
                            dataSummary = db.FirstOrDefault<DashboardSummary>(query, superiorID, periodView);
                            query = String.Format(qGetDashboardDetailSalesOfficer, "",scriptPeriod);
                            listDetail = db.Fetch<DashboardDetail>(query, superiorID, periodView);
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Agency Sales Officer
                    
                        using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                        {
                            query = String.Format(qGetDashboardSummarySalesOfficer, "",scriptPeriodNational);
                            dataSummary = db.FirstOrDefault<DashboardSummary>(query, superiorID, periodView);
                            query = String.Format(qGetDashboardDetailSalesOfficer, "",scriptPeriod);
                            listDetail = db.Fetch<DashboardDetail>(query, superiorID, periodView);
                        }
                    
                    #endregion
                }
                
                result.summary = dataSummary;
                result.detail = listDetail;
                return result;
            }
            catch(Exception e)
            {
                throw (e);
            }
        }

        private static string CheckAccess(string role , string userID)
        {
            try
            {
                string result = string.Empty;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    result = db.FirstOrDefault<string>(@"SELECT CASE WHEN substring(branchcode,0,2) = 'A' 
                    THEN 'AGENT' ELSE 'HO' END FROM SalesOfficer WHERE SalesOfficerID = @0",userID);
                }
                return result;
            }
            catch(Exception e)
            {
                throw (e);
            }
        }

        private static string CheckDKINonDKI(string userid)
        {
            try
            {
                string result = string.Empty;
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    result = db.FirstOrDefault<string>(@";DECLARE @@result varchar(6) = 'NONDKI'
                    select * 
	                INTO #TempJatabekBranch
	                from Fn_AABSplitString((select VALUE 
                    from BeyondReport.aab.dbo.GLOBAL_PARAMETER where [PARAMETER] = 'JatabekBranch'),',')
                    
                    SELECT @@result = 'DKI' FROM SalesOfficer INNER JOIN #TempJatabekBranch
					ON BranchCode = Data
                    WHERE SalesOfficerID = @0 

					select @@result
					drop table #TempJatabekBranch", userid);
                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}