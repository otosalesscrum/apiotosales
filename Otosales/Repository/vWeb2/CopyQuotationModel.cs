﻿using Otosales.Models;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otosales.Repository.vWeb2
{
    public class CopyQuotationModel
    {
        public OrderSimulationModel orderSimulation { get; set; }
        public OrderSimulationMV orderSimulationMV { get; set; }
        public List<OrderSimulationInterest> orderSimulationInterest { get; set; }
        public List<OrderSimulationCoverage> orderSimulationCoverage { get; set; }
    }
}
