﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Otosales.dta;
using System.Net.Http.Formatting;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Net;
using Microsoft.Reporting.WinForms;
using Otosales.Models;
using System.Globalization;
using A2isMessaging;
using Otosales.Controllers.vWeb2;
using PremiumCalculation.Model;
using PremiumCalculation.Manager;
using System.Text;
using System.Data.SqlClient;
using System.Dynamic;

namespace Otosales.Repository.vWeb2
{
    public interface IMobileRepository {
        BankInformationResult GetBankInformation(string SalesmanCode);
        dynamic getSalesmanDealerName(string dealercode);
        List<Models.Product> GetProduct(string insurancetype, string salesofficerid, int isRenewal);
        dynamic getBranchList(string search);
        List<dynamic> GetMappingVehiclePlateGeoArea(string VehiclePlateCode);
        List<dynamic> GetPolicyDeliveryName(string ID, string ParamSearch);
    }
    public class MobileRepository : IMobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        private static PremiumCalculationManager pm = new PremiumCalculationManager();
        public static TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<dynamic> listDyn = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            TaskListDetail result = new TaskListDetail();
            string query = "";

            try
            {
                updateFollowUpStatusToPolicyCreated(FollowUpNo);
                query = @";DECLARE @@Admin DECIMAL
                        --DECLARE @@GrossPremium DECIMAL
                        --DECLARE @@NetPremi DECIMAL
                        DECLARE @@OrderNo VARCHAR(100)
                        DECLARE @@PolicyNo VARCHAR(100)
                        DECLARE @@PolicyOrderNo VARCHAR(100)
                        DECLARE @@SurveyNo VARCHAR(100)
                        DECLARE @@OldPolicyNo VARCHAR(100)

                        SELECT @@Admin=AdminFee,@@OrderNo=OrderNo, @@PolicyNo=PolicyNo, @@PolicyOrderNo=PolicyOrderNo, 
                        @@SurveyNo=SurveyNo, @@OldPolicyNo=OldPolicyNo
                        FROM dbo.OrderSimulation 
                        WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1
                        --SELECT @@GrossPremium=SUM(Gross_Premium) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @@OrderNo AND RowStatus = 1 
                        --SELECT @@NetPremi=@@GrossPremium+@@Admin

                        SELECT 0 GrossPremium, CAST(@@Admin AS DECIMAL) Admin, 0 NetPremi,
						@@PolicyNo AS PolicyNo, @@OrderNo AS OrderNo, @@PolicyOrderNo AS PolicyOrderNo, 
                        @@SurveyNo AS SurveyNo, COALESCE(@@OldPolicyNo,'') AS OldPolicyNo, 0 NoClaimBonus, NULL OldPolicyPeriodTo";
                FieldPremi premi = db.Query<FieldPremi>(query, CustID, FollowUpNo).FirstOrDefault();
                if (premi != null)
                {
                    if (!string.IsNullOrEmpty(premi.OldPolicyNo))
                    {
                        List<dynamic> ls = new List<dynamic>();
                        //query = @"SELECT COALESCE(SUM(Amount), 0) AS NCB FROM dbo.Ren_Cover_Disc WHERE Policy_No = @0";
                        //ls = AABdb.Fetch<dynamic>(query, listDyn.First().OldPolicyNo);
                        //if (ls.Count > 0)
                        //{
                        //    premi.NoClaimBonus = ls.First().NCB;
                        //}
                        query = @"SELECT Period_To FROM dbo.Policy WHERE Policy_No = @0";
                        ls = AABdb.Fetch<dynamic>(query, premi.OldPolicyNo);
                        if (ls.Count > 0)
                        {
                            premi.OldPolicyPeriodTo = ls.First().Period_To;
                        }
                    }
                    result.FieldPremi = premi;
                }
                query = @"SELECT p.Name,p.CustBirthDay,p.CustGender,p.CustAddress,p.PostalCode,
                        p.Email1,p.Phone1,p.IdentityNo, CAST(COALESCE(p.isCompany,0) AS BIT) isCompany, 
                        CAST(0 AS BIT) isNeedDocRep, 0 AmountRep
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
                        WHERE p.CustID = @0 AND p.RowStatus = 1";
                PersonalData pd = db.Query<PersonalData>(query, CustID).FirstOrDefault();
                if (pd != null)
                {
                    query = @"SELECT CAST(COALESCE(os.IsNeedDocRep,0) AS BIT)IsNeedDocRep, 
                            COALESCE(os.AmountRep, 0) AmountRep
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
						INNER JOIN dbo.OrderSimulation os
						ON os.FollowUpNo = f.FollowUpNo
                        WHERE p.CustID = @0 AND p.RowStatus = 1
						AND os.RowStatus = 1 AND os.ApplyF = 1";
                    listDyn = db.Fetch<dynamic>(query, CustID);
                    if (listDyn.Count > 0)
                    {
                        pd.isNeedDocRep = listDyn.First().IsNeedDocRep;
                        pd.AmountRep = listDyn.First().AmountRep;
                    }
                    result.PersonalData = pd;
                }
                query = @";SELECT IdentityCard, id1.Data IdentityCardData,
                        f.STNK, id2.Data STNKData,
                        f.BSTB1 BSTB, id3.Data BSTBData,
                        f.KonfirmasiCust, id4.Data KonfirmasiCustData,
                        f.DocRep, id5.Data DocRepData
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON f.IdentityCard = id1.PathFile
                        LEFT JOIN dbo.ImageData id2
                        ON f.STNK = id2.PathFile
                        LEFT JOIN dbo.ImageData id3
                        ON f.BSTB1 = id3.PathFile
                        LEFT JOIN dbo.ImageData id4
                        ON f.KonfirmasiCust = id4.PathFile
                        LEFT JOIN dbo.ImageData id5
                        ON f.DocRep = id5.PathFile
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0) {
                    result.PersonalDocument = listDyn;
                }

                query = @"SELECT CompanyName,NPWPno,NPWPdate,NPWPaddress NPWPaddres,OfficeAddress
                            ,PostalCode,PICPhoneNo,PICname,Email PICEmail FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";
                CompanyData cd = db.Query<CompanyData>(query, CustID, FollowUpNo).FirstOrDefault();
                if (cd != null)
                {
                    result.CompanyData = cd;
                }
                query = @";IF EXISTS(SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1)
                        BEGIN
	                        SELECT pc.NPWP, id1.Data NPWPData,
	                        pc.SIUP, id2.Data SIUPData
	                        FROM dbo.ProspectCompany pc
	                        LEFT JOIN dbo.ImageData id1
	                        ON pc.NPWP = id1.PathFile
	                        LEFT JOIN dbo.ImageData id2
	                        ON pc.SIUP = id2.PathFile
	                        WHERE pc.CustID = @0 AND pc.FollowUpNo = @1 AND pc.RowStatus = 1
                        END
                        ELSE
                        BEGIN
	                        SELECT NULL NPWP, NULL NPWPData,
	                        NULL SIUP, NULL SIUPData
                        END";
                listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDyn.Count > 0)
                {
                    result.CompanyDocument = listDyn;
                }
                query = @";SELECT c.Description + ' ' + d.Description + ' ' + omv.Year + ' ' + a.Series  AS Vehicle,
                        omv.VehicleCode, omv.BrandCode, os.ProductTypeCode, omv.ModelCode,  omv.Series, 
                        omv.Type, CAST(omv.Sitting AS DECIMAL) Sitting, omv.Year, omv.CityCode, CAST(omv.SumInsured AS DECIMAL) SumInsured, os.InsuranceType,
                        os.VANumber, p.DealerCode, p.SalesDealer, omv.UsageCode, omv.ColorOnBPKB, 
                        omv.RegistrationNumber, omv.EngineNumber, omv.ChasisNumber, omv.IsNew, os.OrderNo,
						os.PolicyNo, os.ProductCode, os.SegmentCode, os.PeriodFrom, os.PeriodTo,
						os.IsORDefectsRepair, IsAccessoriesChange, IsPolicyIssuedBeforePaying, 
                        os.Remarks, f.DocPendukung, id.Data AS DocPendukungData,
                        0 BasicCoverID, '' ORDefectsDesc, '' NoCover
                        FROM Vehicle a 
                        LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
                        AND c.ProductTypeCode = a.ProductTypeCode 
                        LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
                        AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
                        LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                        a.BrandCode=b.BrandCode
                        AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                        AND a.Series=b.Series AND a.Type=b.Type 
                        AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                        AND a.Year > b.Year 
                        INNER JOIN dbo.OrderSimulationMV omv ON omv.VehicleCode = a.VehicleCode
                        INNER JOIN dbo.OrderSimulation os ON os.OrderNo = omv.OrderNo
                        INNER JOIN dbo.ProspectCustomer p ON p.CustID = os.CustID
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						LEFT JOIN dbo.ImageData id ON id.PathFile = f.DocPendukung
                        WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND omv.RowStatus = 1
						AND f.RowStatus = 1 AND os.CustID = @0 AND os.FollowUpNo = @1 AND b.Year IS NULL 
                        ORDER BY a.VehicleCode";
                VehicleData vData = db.Query<VehicleData>(query, CustID, FollowUpNo).FirstOrDefault();
                if (vData != null)
                {
                    query = @";DECLARE @@TLOPer INT
                            DECLARE @@ComprePer INT
                            SELECT @@TLOPer=TLOPeriod,@@ComprePer=ComprePeriod FROM dbo.OrderSimulation WHERE OrderNo = @0
                            SELECT ID AS BasicCoverID FROM dbo.Mst_Basic_Cover 
                            WHERE ComprePeriod = @@ComprePer
                            AND TLOPeriod = @@TLOPer";
                    List<dynamic> bscCvr = db.Fetch<dynamic>(query, vData.OrderNo);
                    if (bscCvr.Count > 1)
                    {
                        query = @"SELECT RTRIM(LTRIM(CoverageID)) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                        List<string> ListCvrID = db.Fetch<string>(query, vData.OrderNo);
                        if (ListCvrID.Contains("TLO"))
                        {
                            query = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%TLO Others%'";
                            int id = db.ExecuteScalar<int>(query);
                            vData.BasicCoverID = id;
                        }
                        else if (ListCvrID.Contains("ALLRIK"))
                        {
                            query = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description LIKE '%Comprehensive Others%'";
                            int id = db.ExecuteScalar<int>(query);
                            vData.BasicCoverID = id;
                        }
                    }
                    else {
                        vData.BasicCoverID = bscCvr.First().BasicCoverID;
                    }
                    if (!string.IsNullOrEmpty(result.FieldPremi.OldPolicyNo)) {
                        query = @";DECLARE @@PolicyID VARCHAR(100)
                                SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
                                SELECT STUFF(( SELECT ', ' + Name
                                FROM dbo.Dtl_Original_Defect 
                                where Policy_Id = @@PolicyID
                                FOR XML PATH('')
                                ), 1, 1, '')  AS OriginalDefect,
								STUFF(( SELECT ', ' + Name
                                FROM dbo.Ren_Dtl_NoCover 
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS NoCover,
								STUFF(( SELECT ', ' + ao.AccsDescription + (CASE WHEN rdna.Accs_Cat_Code != '' AND rdna.Accs_Cat_Code is not null
                                THEN ' '+ac.accscatdescription ELSE '' END) + (CASE WHEN rdna.Accs_part_Code != '' AND rdna.Accs_part_Code is not null
                                THEN ' '+ap.accspartdescription ELSE '' END)
                                FROM dbo.Ren_Dtl_Non_Standard_Accessories  rdna
                                INNER JOIN dbo.AccessoriesOnline ao
                                ON ao.AccsCode = rdna.Accs_Code
                                left join dbo.AccessoriesCategory ac
                                ON ac.AccsCatCode = rdna.Accs_Cat_Code
                                left join dbo.AccessoriesPart ap
                                on ap.accspartcode = rdna.accs_part_code
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS Accessories";
                        List<dynamic> list = AABdb.Fetch<dynamic>(query, result.FieldPremi.OldPolicyNo);
                        if (list.Count > 0)
                        {
                            vData.ORDefectsDesc = list.First().OriginalDefect;
                            vData.NoCover = list.First().NoCover;
                            vData.Accessories = list.First().Accessories;
                        }
                    }
                    result.VehicleData = vData;
                }
                result.PaymentInfo = GetPaymentInfo(CustID, FollowUpNo);
                query = @"SELECT PolicySentTo SentTo, PolicyDeliveryName Name, PolicyDeliveryAddress Address, PolicyDeliveryPostalCode PostalCode FROM dbo.OrderSimulation WHERE CustID = @0 
                            AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1";
                PolicyAddress pa = db.Query<PolicyAddress>(query, CustID, FollowUpNo).FirstOrDefault();
                if (pa != null)
                {
                    result.PolicyAddres = pa;
                }
                if(!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo)){
                    query = @";SELECT odi.[Description] AS NoCover,
                        STUFF(( SELECT ', ' + [Description]
                                    FROM dbo.Ord_Object_Clause 
			                        WHERE Order_No = @0
								    AND Clause_Id = 'OMV99'
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS OriginalDefect,
                        STUFF(( SELECT ', ' + Item_Name
                                    FROM dbo.Ord_Dtl_Interest_Item 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS Accessories,
                        ms.[Description] AS SurveyStatus,
                        s.ResultRecommendation AS SurveyorRecomendation,
                        s.remarks AS Remaks,
                        mu.[User_Name] AS Surveyor
                        FROM dbo.Survey s 
						INNER JOIN dbo.Lnk_Survey_Order lso
						ON s.Survey_No = lso.Survey_No
                        INNER JOIN dbo.Mst_Order mo 
                        ON mo.Order_No = s.Reference_No
                        INNER JOIN dbo.Ord_Dtl_Interest odi
                        ON odi.Order_No = mo.Order_No
                        INNER JOIN dbo.Mst_State ms 
                        ON ms.[Type] = 'survey' 
                        AND ms.[State] = s.[Status]
                        INNER JOIN dbo.Mst_User mu
                        ON mu.[User_Id] = s.Surveyor
                        WHERE mo.Order_No = @0
						AND odi.Interest_Code = 'CASCO'";
                    SurveyData sd = AABdb.Query<SurveyData>(query, result.FieldPremi.PolicyOrderNo).FirstOrDefault();
                    if (listDyn.Count > 0)
                    {
                        result.SurveyData = sd;
                    }
                }
                query = @"SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                SurveySchedule ss = db.Fetch<SurveySchedule>(query, CustID, FollowUpNo).FirstOrDefault();
                if (listDyn.Count > 0)
                {
                    result.SurveySchedule = ss;
                }
                query = @";SELECT f.SPPAKB, id1.Data SPPAKBData,
                        f.FAKTUR, id2.Data FAKTURData,
                        f.DocNSA1, id3.Data DocNSA1Data,
                        f.DocNSA2, id4.Data DocNSA2Data,
                        f.DocNSA3, id5.Data DocNSA3Data,
                        f.DocNSA4, id6.Data DocNSA4Data,
                        f.DocNSA5, id7.Data DocNSA5Data,
                        RemarkToSA, RemarkFromSA
                        FROM dbo.FollowUp f
                        LEFT JOIN dbo.ImageData id1
                        ON id1.PathFile = f.SPPAKB
                        LEFT JOIN dbo.ImageData id2
                        ON id2.PathFile = f.FAKTUR
                        LEFT JOIN dbo.ImageData id3
                        ON id3.PathFile = f.DocNSA1
                        LEFT JOIN dbo.ImageData id4
                        ON id4.PathFile = f.DocNSA2
                        LEFT JOIN dbo.ImageData id5
                        ON id5.PathFile = f.DocNSA3
                        LEFT JOIN dbo.ImageData id6
                        ON id6.PathFile = f.DocNSA4
                        LEFT JOIN dbo.ImageData id7
                        ON id7.PathFile = f.DocNSA5
                        WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND f.RowStatus = 1";
                List<dynamic> listDoc = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (listDoc.Count > 0)
                {
                    query = @"SELECT TOP 1 mu.User_Name Name 
                                FROM dbo.Mst_Order_Mobile mom
                                INNER JOIN dbo.Mst_User mu
                                ON mu.User_Id = mom.EntryUsr
                                WHERE Order_No = @0 
                                AND (SA_State = 0 OR SA_State = 2) 
                                AND mom.EntryUsr <> 'OTOSL' ORDER BY mom.EntryDt DESC";
                    List<dynamic> ls = AABdb.Fetch<dynamic>(query, result.FieldPremi.PolicyOrderNo);
                    if (ls.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(listDoc.First().RemarkFromSA) && !string.IsNullOrWhiteSpace(listDoc.First().RemarkFromSA)
                            && !string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                        {
                            listDoc.First().RemarkFromSA = ls.First().Name + " - " + listDoc.First().RemarkFromSA;
                        }
                        else if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo)) {
                            listDoc.First().RemarkFromSA = ls.First().Name;                        
                        }
                    }
                    result.Document = listDoc;
                }

                query = @"SELECT fu.FollowUpStatus,fu.FollowUpInfo,fu.IsRenewal,so.Name as Agency,'' as Upliner, fu.SalesOfficerID, so.Name
                        FROM dbo.FollowUp fu  
                        INNER JOIN dbo.SalesOfficer so 
                        ON so.SalesOfficerId=fu.SalesOfficerId
                        WHERE fu.FollowUpNo = @0";
                listDyn = db.Fetch<dynamic>(query, FollowUpNo);
                if (listDyn.Count > 0) {
                    foreach (dynamic item in listDyn)
                    {
                        bool isAgency = AABdb.ExecuteScalar<int>(@";SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] eu  WITH ( NOLOCK ) INNER JOIN beyondmoss.AABMobile.dbo.SalesOfficer so ON eu.UserID=so.Email WHERE so.SalesOfficerId=@0", item.SalesOfficerID) > 0?true:false;
                        item.Agency = isAgency?item.Agency:"";
                        item.Upliner = isAgency?AABdb.ExecuteScalar<string>(@"SELECT TOP 1 RTRIM(d.Name) AS Upliner         
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT' AND b.Cust_Id=@0 OR a.Client_Code=@0",item.SalesOfficerID):"";
                    }
                    result.FollowUpInfo = listDyn;   
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public static void ClearFailedData(string custID, string followUpID, string orderID)
        {
            #region Query Delete Fail GT
            string qDeleteFailGT = @"
                                               DELETE FROM OrderSimulationCoverage WHERE OrderNo=@2
                                               DELETE FROM OrderSimulationInterest WHERE OrderNo=@2
                                               DELETE FROM OrdersimulationMV WHERE OrderNo=@2
                                               DELETE FROM OrderSimulation WHERE CustId=@0 AND FollowUpNo=@1 AND OrderNo=@2
                                               DELETE FROM FollowUp WHERE CustId=@0 AND FollowUpNo=@1
                                               DELETE FROM ProspectCompany WHERE CustId=@0
                                               DELETE FROM ProspectCustomer WHERE CustId=@0";
            #endregion
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
            {
                db.Execute(qDeleteFailGT, custID, followUpID, orderID);
            }
        }
        private static string CheckDigit(string Number)
        {
            int lEven = 0, lOdd = 0, lNumber = 0, i = 0, retVal = 0;
            for (i = 0; i < (Number.Length - 1); i++)
            {
                string a = Number.Substring((Number.Length - 1) - i, 1);
                lNumber = string.IsNullOrWhiteSpace(a) ? 0 : Convert.ToInt32(a);
                if ((i + 1) % 2 == 0)
                    lEven = lEven + lNumber;
                else
                    lOdd = lOdd + lNumber;
            }
            lNumber = lOdd * 3 + lEven;
            lNumber = lNumber % 10;
            if (lNumber == 0)
                retVal = lNumber;
            else
                retVal = 10 - lNumber;
            return Convert.ToString(retVal);
        }
        public static string IDGenereation(string IDType, int IDLength)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string qInsertID = @"INSERT  INTO dtl_urut ( code, no_urut, entrydt ) VALUES  ( @0, @1, GETDATE() )";
            string qInsertDuplicate = @"insert into dtl_urut_duplicate (code, no_urut, entrydt) values (@0,@1,getdate())";
            string spGenerateID = @";EXEC sp_IDGeneration @0";
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
            {
                int IDNo = 0;
                try
                {
                    var temp = db.FirstOrDefault<dynamic>(spGenerateID, IDType);
                    IDNo = Convert.ToInt32(temp.no_urut);
                    db.Execute(qInsertID, IDType, IDNo);
                    return IDNo.ToString().PadLeft(IDLength, '0');
                }
                catch (SqlException sqle)
                {
                    int code = sqle.ErrorCode;
                    if (code == -2147217873)
                    {
                        db.Execute(qInsertDuplicate, IDType, IDNo);
                    }
                    return "";
                }
            }
        }
        public static string GetPolicyNo()
        {
            DateTime now = DateTime.Now;
            string yearnow = Convert.ToString(now.Year);
            string Code = string.Format("{0}{1}", "POLICY_NO", yearnow.Substring(yearnow.Length - 2));
            string TmpPolicyNo = IDGenereation(Code, 7);
            string PolicyNo = string.Format("{0}{1}{2}", yearnow.Substring(yearnow.Length - 2), TmpPolicyNo, CheckDigit(TmpPolicyNo));
            return PolicyNo;
        }

        public static string GetPolicyId()
        {
            string Code = "POLICY_ID";
            string PolicyID = IDGenereation(Code, 12);
            return PolicyID;
        }


        public static void GenerateOrderSimulation(string salesOfficerID, string custID, string followUpID, string orderID, string PolicyNo, string remarkToSA)
        {
            try
            {
                //Param
                string query = "";

                //Data
                CustomerInfo custInfo = new CustomerInfo();
                Models.vWeb2.OrderSimulationRenot orderSimulation = new Models.vWeb2.OrderSimulationRenot();
                Models.vWeb2.OrderSimulationMVRenot orderSimulationMV = new Models.vWeb2.OrderSimulationMVRenot();
                List<Models.vWeb2.OrderSimulationInterestRenot> listOSInterest = new List<Models.vWeb2.OrderSimulationInterestRenot>();
                List<Models.vWeb2.OrderSimulationCoverageRenot> listOSCoverage = new List<Models.vWeb2.OrderSimulationCoverageRenot>();

                #region Query Prospect Customer
                query = @"SELECT mc.Name AS Name,
		mc.Email AS Email1,
		mcp.HP AS Phone1,
		mcp.HP_2 AS Phone2,
		mc.Cust_Id AS CustIDAAB,
		CASE WHEN mpr.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		END AS SalesOfficerID,
		rn.Salesman_Dealer_Code AS SalesDealer,
		rn.Dealer_Code AS DealerCode,
		rn.Branch_Id AS BranchCode,
		CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany, 
		isnull(mcp.id_card,'') AS IdentityNo,
		CASE WHEN isnull(mcp.sex , '') = '' THEN '' 
		WHEN isnull(mcp.sex , '') = '1' THEN 'M'
		ELSE 'F' END AS Gender,
		mp.Prospect_Id AS ProspectID,
		mc.Birth_Date AS BirthDate,
		CASE WHEN mc.Cust_Type = 2 THEN mc.Office_Address ELSE mc.Home_Address END AS [Address],
		CASE WHEN mc.Cust_Type = 2 THEN mc.Office_PostCode ELSE mc.Home_PostCode END AS PostCode,
		isnull(mca.AdditionalInfoValue,'') AS CompanyNPWPAddress,
		isnull(mca2.AdditionalInfoValue,'') AS CompanyNPWPNo,
		CASE WHEN mca3.AdditionalInfoValue is not null 
        THEN SUBSTRING(mca3.AdditionalInfoValue, 7, 4)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 4, 2)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 1, 2) 
        ELSE '' END  AS CompanyNPWPDate,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mca4.AdditionalInfoValue,'') 
		ELSE '' END AS PICPhone,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mcpi.Name,'') 
		ELSE '' END AS PIC
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_prospect mp WITH ( NOLOCK ) ON mp.cust_id = a.policy_holder_code
LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 	
LEFT JOIN mst_cust_personal mcp WITH ( NOLOCK ) ON mcp.cust_id = a.policy_holder_code
inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.product_code = rn.new_product_code
    left join Mst_Customer_AdditionalInfo mca WITH ( NOLOCK ) ON mca.custid = a.policy_holder_code 
    AND mca.AdditionalCode IN ('NPAD')
    left join Mst_Customer_AdditionalInfo mca2 WITH ( NOLOCK ) ON mca2.custid = a.policy_holder_code 
    AND mca2.AdditionalCode IN ('NPWN')
    left join Mst_Customer_AdditionalInfo mca3 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca3.AdditionalCode IN ('NPDT')
    left join Mst_Customer_AdditionalInfo mca4 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca4.AdditionalCode IN ('MPC1')
	left join Mst_cust_pic mcpi WITH ( NOLOCK ) ON mcpi.cust_id = a.policy_holder_code    
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    custInfo = db.FirstOrDefault<CustomerInfo>(query, PolicyNo);
                }

                query = @"SELECT mc.Name AS Name,
        mc.Email AS Email1,
        mcp.HP AS Phone1,
		mcp.HP_2 AS Phone2,
        mc.Cust_Id AS CustIDAAB,
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END  AS SalesOfficerID,
        a.Broker_Code AS DealerCode,
        mc2.Cust_Id AS SalesDealer,
        COALESCE(rm.new_branch_id, a.branch_id) AS BranchCode,
        CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany,
        isnull(mcp.id_card, '') AS IdentityNo,
        CASE WHEN isnull(mcp.sex, '') = '' THEN ''
        WHEN isnull(mcp.sex, '') = '1' THEN 'M'
        ELSE 'F' END AS Gender,
		mc.Prospect_Id AS ProspectID,
		mc.Birth_Date AS BirthDate,
		mc.Home_Address AS [Address],
		mc.Home_PostCode AS PostCode,
		isnull(mca.AdditionalInfoValue,'') AS CompanyNPWPAddress,
		isnull(mca2.AdditionalInfoValue,'') AS CompanyNPWPNo,
		CASE WHEN mca3.AdditionalInfoValue is not null 
        THEN SUBSTRING(mca3.AdditionalInfoValue, 7, 4)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 4, 2)+'-'+SUBSTRING(mca3.AdditionalInfoValue, 1, 2) 
        ELSE '' END  AS CompanyNPWPDate,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mca4.AdditionalInfoValue,'') 
		ELSE '' END AS PICPhone,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(mcpi.Name,'') 
		ELSE '' END AS PIC
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc WITH(NOLOCK) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_cust_personal mcp WITH ( NOLOCK ) ON mcp.cust_id = a.policy_holder_code
INNER JOIN mst_product mp ON mp.product_code = a.product_code
LEFT JOIN mst_salesman ms WITH(NOLOCK) ON ms.salesman_id = a.Salesman_Id
LEFT JOIN dbo.ComSales_Salesman AS css ON a.Broker_Code = css.Salesman_Code
LEFT JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
LEFT OUTER JOIN renewal_map rm ON mp.product_code = rm.product_code
                                                          AND rm.status = 1
                                                          AND ( rm.old_branch_id = ''
                                                              OR rm.old_branch_id = a.branch_id
                                                              )
                                                          AND ( rm.old_segment_code = ''
                                                              OR rm.old_segment_code = a.segment_code
                                                              )

    left join Mst_Customer_AdditionalInfo mca WITH ( NOLOCK ) ON mca.custid = a.policy_holder_code 
    AND mca.AdditionalCode IN ('NPAD')
    left join Mst_Customer_AdditionalInfo mca2 WITH ( NOLOCK ) ON mca2.custid = a.policy_holder_code 
    AND mca2.AdditionalCode IN ('NPWN')
    left join Mst_Customer_AdditionalInfo mca3 WITH ( NOLOCK ) ON mca3.custid = a.policy_holder_code 
    AND mca3.AdditionalCode IN ('NPDT')
    left join Mst_Customer_AdditionalInfo mca4 WITH ( NOLOCK ) ON mca4.custid = a.policy_holder_code 
    AND mca4.AdditionalCode IN ('MPC1')
	left join Mst_cust_pic mcpi WITH ( NOLOCK ) ON mcpi.cust_id = a.policy_holder_code
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if(custInfo == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        custInfo = db.FirstOrDefault<CustomerInfo>(query, PolicyNo);
                    }
                }
                

                #region Sales Dealer dan Dealer Code
                int SalesmanCode = 0;
                int DealerCode = 0;
                int salesmancode = 0;
                int dealercode = 0;
                string querySalesman = "SELECT SalesmanCode FROM dbo.SalesmanDealer WHERE CustID = @0";
                string queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE CustID = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    if (custInfo.SalesDealer != null)
                        salesmancode = db.First<int>(querySalesman, custInfo.SalesDealer);
                    else
                        salesmancode = 0;
                    int value = 0;
                    if (custInfo.DealerCode != null && int.TryParse(custInfo.DealerCode, out value))
                        dealercode = db.First<int>(queryDealer, custInfo.DealerCode);
                    else
                        dealercode = 0;
                }
                
                SalesmanCode = Convert.ToInt32(salesmancode);
                DealerCode = Convert.ToInt32(dealercode);

                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {

                    #region Query Insert Prospect Customer
                    query = @"
INSERT INTO ProspectCustomer([CustID]
      , [Name] --*
      , [Phone1] --*
      , [Phone2]
      , [Email1] --*
      , [Email2]
      , [CustIDAAB] --*
      , [SalesOfficerID] --*
      , [DealerCode] --*
      , [SalesDealer] --*
      , [BranchCode] --*
      , [isCompany] --*
      , [EntryDate]
      , [LastUpdatedTime]
      , [RowStatus]
      , [IdentityNo]
      , [CustGender]      
      , [ProspectID]
      , [CustBirthDay]
      , [CustAddress]
      , [PostalCode])
      SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1,@12,@13,@14,@15,@16,@17";
                    #endregion

                    db.Execute(query, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2, custInfo.Email1
                        , "", custInfo.CustIDAAB, salesOfficerID, DealerCode, SalesmanCode, custInfo.BranchCode
                        , custInfo.isCompany, custInfo.IdentityNo, custInfo.Gender,custInfo.ProspectID,custInfo.BirthDate
                        , custInfo.Address,custInfo.PostCode);


                    #region Query Insert Prospect Company
                    query = @"
INSERT INTO ProspectCompany ([CustID]
      ,[CompanyName] --*
      ,[FollowUpNo] --*
      ,[Email] --*
      ,[ModifiedDate]
      ,[RowStatus]
      ,[NPWPno]
      ,[NPWPdate]
      ,[NPWPaddress]
      ,[OfficeAddress]
      ,[PostalCode]
      ,[PICPhoneNo]
      ,[PICname]) VALUES(@0,@1,@2,@3,GETDATE(),1,@4,@5,@6,@7,@8,@9,@10)";
                    #endregion

                    if (custInfo.isCompany == 1)
                        db.Execute(query, custID, custInfo.Name, followUpID, custInfo.Email1
                            , custInfo.CompanyNPWPNo, custInfo.CompanyNPWPDate, custInfo.CompanyNPWPAddress, custInfo.Address, custInfo.PostCode
                            , custInfo.PICPhone, custInfo.PIC);


                    #region Query Insert Follow Up
                    query = @"
    INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[RemarkToSA]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsRenewal]) 
SELECT @0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,GETDATE(),1,1";
                    #endregion

                    db.Execute(query, followUpID, 0, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2
                        , salesOfficerID, custInfo.Name, 1, 1, remarkToSA, custInfo.BranchCode);

                }

                #region Query Order Simulation
                query = @"SELECT 
		NULL AS NewPolicyId,
		rn.New_Policy_No AS NewPolicyNo,
		rn.VANumber AS VANumber,
		'' AS QuotationNo,
		0 AS MultiYearF,
		1 AS YearCoverage,
		CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
		CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
		rn.Branch_Id AS BranchCode,
		CASE WHEN mpr.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		END AS SalesOfficerID,
		'' AS PhoneSales,
		rn.Dealer_Code AS DealerCode,
		rn.Salesman_Dealer_Code AS SalesDealer,
		'GARDAOTO' AS ProductTypeCode,
		rp.Policy_Fee AS AdminFee,
		rp.net_premium AS TotalPremium,
		mc.Home_Phone1 AS Phone1,
		mc.Home_Phone2 AS Phone2,
		mc.Email AS Email1,
		'' AS Email2,
		0 AS SendStatus,
		CASE WHEN mp.cob_id = '404' THEN 2 
		WHEN PDT.product_code is not null THEN 3 
        WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
        ELSE 1 END AS InsuranceType,
		1 AS ApplyF,
		0 AS SendF,
		(SELECT top 1 Interest_no from Ren_Dtl_Interest where policy_no = @0) AS LastInterestNo,
		(SELECT top 1 coverage_no from Ren_Dtl_Coverage where policy_no = @0) AS LastCoverageNo,
		rn.New_Product_code AS ProductCode,
        a.Period_To AS PeriodFrom,
        DATEADD(DAY, DATEDIFF(DAY, a.Period_From,a.Period_To), a.Period_To) AS PeriodTo,
        rn.segment_code AS SegmentCode,
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 
LEFT JOIN Ren_Dtl_Coverage rdc1 WITH ( NOLOCK ) ON  rdc1.policy_no = rn.policy_no
AND rdc1.interest_id = 'CASCO' AND rdc1.coverage_id = 'TLO'
LEFT JOIN Ren_Dtl_Coverage rdc2 WITH ( NOLOCK ) ON  rdc2.policy_no = rn.policy_no
AND rdc2.interest_id = 'CASCO' AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN premium rp ON  rp.policy_id = a.policy_id
INNER JOIN mst_product mp on mp.product_code = rn.old_product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.new_product_code = rn.new_product_code
left join dtl_address da on da.policy_id = a.policy_id AND da. address_type ='DELIVR'
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo);
                }

                query = @"SELECT TOP 1
NULL AS NewPolicyId,
NULL AS NewPolicyNo,
NULL AS VANumber,
'' AS QuotationNo,
0 AS MultiYearF,
1 AS YearCoverage,
CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
a.Branch_Id AS BranchCode,
CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END  AS SalesOfficerID,
'' AS PhoneSales,
a.Broker_Code AS DealerCode,
        mc.Cust_Id AS SalesDealer,
'GARDAOTO' AS ProductTypeCode,
rp.Policy_Fee AS AdminFee,
rp.net_premium AS TotalPremium,
mc.Home_Phone1 AS Phone1,
mc.Home_Phone2 AS Phone2,
mc.Email AS Email1,
'' AS Email2,
0 AS SendStatus,
CASE WHEN mp.cob_id = '404' THEN 2 
WHEN PDT.product_code is not null THEN 3 
WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
ELSE 1 END AS InsuranceType,
1 AS ApplyF,
0 AS SendF,
(SELECT max(Interest_no) from Dtl_Interest where policy_id = a.policy_id) AS LastInterestNo,
(SELECT max(coverage_no) from Dtl_Coverage where policy_id = a.policy_id) AS LastCoverageNo,
a.Product_Code [ProductCode],
a.Period_To AS PeriodFrom,
DATEADD(DAY, DATEDIFF(DAY, a.Period_From,a.Period_To), a.Period_To) AS PeriodTo,
a.Segment_Code [SegmentCode],
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc  ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms ON ms.salesman_id = a.Salesman_Id 
LEFT JOIN dbo.ComSales_Salesman AS css ON a.Broker_Code = css.Salesman_Code
LEFT JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
LEFT JOIN dtl_interest di ON di.policy_id = a.policy_id AND di.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc1 ON  rdc1.policy_id = a.policy_id 
AND rdc1.interest_no = di.interest_no AND rdc1.coverage_id = 'TLO'
LEFT JOIN dtl_interest di2 ON di2.policy_id = a.policy_id AND di2.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc2 ON  rdc2.policy_id = a.policy_id
AND rdc2.interest_no = di2.interest_no AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN premium rp ON  rp.policy_id = a.policy_id
INNER JOIN mst_product mp on mp.product_code = a.product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
left join dtl_address da on da.policy_id = a.policy_id AND da. address_type ='DELIVR'
WHERE a.policy_no = @0
AND a.Status = 'A'";

                #endregion

                if (orderSimulation == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo);
                    }
                }
                

                #region Query Insert Order Simulation
                query = @"
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[OldPolicyNo]
      ,[PolicyNo]
      ,[PolicyID]
      ,[VANumber]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[ProductCode]
      ,[SegmentCode]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
        ) 
  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10, @11,@12,
        @13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    orderSimulation.SalesOfficerID = salesOfficerID;
                    db.Execute(query, orderID, custID, followUpID, orderSimulation.QuotationNo, orderSimulation.MultiYearF
                        , orderSimulation.YearCoverage, orderSimulation.TLOPeriod, orderSimulation.ComprePeriod, orderSimulation.BranchCode
                        , salesOfficerID, orderSimulation.PhoneSales, DealerCode, SalesmanCode
                        , orderSimulation.ProductTypeCode, orderSimulation.AdminFee, orderSimulation.TotalPremium, orderSimulation.Phone1
                        , orderSimulation.Phone2, orderSimulation.Email1, orderSimulation.Email2, orderSimulation.SendStatus
                        , orderSimulation.InsuranceType, orderSimulation.ApplyF, orderSimulation.SendF, orderSimulation.LastInterestNo
                        , orderSimulation.LastCoverageNo, PolicyNo, orderSimulation.NewPolicyNo, orderSimulation.NewPolicyId
                        , orderSimulation.VANumber, orderSimulation.PeriodFrom, orderSimulation.PeriodTo, orderSimulation.ProductCode
                        , orderSimulation.SegmentCode,orderSimulation.PolicyDeliverAddress,orderSimulation.PolicyDeliverPostalCode
                        , orderSimulation.PolicyDeliveryType,orderSimulation.PolicyDeliveryName);
                }

                #region Query Order Simulation MV
                query = @"SELECT rdm.Object_No AS ObjectNo,
		'GARDAOTO' AS ProductTypeCode,
		rdm.Vehicle_Code AS VehicleCode,
		rdm.Brand_Code AS BrandCode,
		rdm.Model_Code AS ModelCode,
		rdm.Series  AS Series,
		rdm.Vehicle_Type AS [Type],
		rdm.Sitting_Capacity AS Sitting,
		rdm.Mfg_Year AS [Year],
		rdm.Geo_Area_Code AS CityCode,
		rdm.Usage_Code AS UsageCode,
		rdi.Sum_Insured AS SumInsured,
		ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Ren_Dtl_Non_Standard_Accessories] where policy_no = @0),0) AS AccessSI,
		rdm.Registration_Number AS RegistrationNumber,
		rdm.Engine_Number AS EngineNumber,
		rdm.Chasis_Number AS ChasisNumber,
		rdm.New_Car AS IsNew,
        rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Interest rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.policy_no 
AND rdi.Object_no = rdm.object_no
AND rdi.interest_id = 'CASCO'
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);
                }

                query = @"SELECT rdm.Object_No AS ObjectNo,
'GARDAOTO' AS ProductTypeCode,
rdm.Vehicle_Code AS VehicleCode,
rdm.Brand_Code AS BrandCode,
rdm.Model_Code AS ModelCode,
rdm.Series  AS Series,
rdm.Vehicle_Type AS [Type],
rdm.Sitting_Capacity AS Sitting,
rdm.Mfg_Year AS [Year],
rdm.Geo_Area_Code AS CityCode,
rdm.Usage_Code AS UsageCode,
rdm.Market_Price AS SumInsured,
ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Dtl_Non_Standard_Accessories] where policy_id = a.policy_id),0) AS AccessSI,
rdm.Registration_Number AS RegistrationNumber,
rdm.Engine_Number AS EngineNumber,
rdm.Chasis_Number AS ChasisNumber,
rdm.New_Car AS IsNew,
rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if(orderSimulationMV == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);
                    }
                }
                

                #region Query Insert Order Simulation MV
                query = @"DELETE FROM OrdersimulationMV WHERE OrderNo=@0
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]
      ,[ColorOnBPKB]) 
  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17,@18";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, orderID, orderSimulationMV.ObjectNo, orderSimulationMV.ProductTypeCode
                        , orderSimulationMV.VehicleCode, orderSimulationMV.BrandCode, orderSimulationMV.ModelCode
                        , orderSimulationMV.Series, orderSimulationMV.Type, orderSimulationMV.Sitting
                        , orderSimulationMV.Year, orderSimulationMV.CityCode, orderSimulationMV.UsageCode
                        , orderSimulationMV.SumInsured, orderSimulationMV.AccessSI, orderSimulationMV.RegistrationNumber
                        , orderSimulationMV.EngineNumber, orderSimulationMV.ChasisNumber, orderSimulationMV.IsNew , orderSimulationMV.Color);
                }

                #region Query Order Simulation Interest
                query = @"SELECT rdi.Object_No AS ObjectNo,
		rdi.Interest_No AS InterestNo,
		rdi.Interest_Id AS InterestID,
		1 AS [Year],
		(SELECT SUM(rdc.net_premium) FROM [dbo].[Ren_Dtl_Coverage] rdc WHERE rdc.policy_no = @0 
		and rdc.interest_no = rdi.Interest_No) AS Premium,
        a.Period_To AS PeriodFrom,
        DATEADD(DAY, DATEDIFF(DAY, a.Period_From,a.Period_To), a.Period_To) AS PeriodTo,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    listOSInterest = db.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(query, PolicyNo);
                }

                query = @"SELECT rdi.Object_No AS ObjectNo,
rdi.Interest_No AS InterestNo,
rdi.Interest_Id AS InterestID,
1 AS [Year],
(SELECT SUM(rdc.net_premium) FROM [dbo].[Dtl_Coverage] rdc WHERE rdc.policy_id = a.policy_id 
and rdc.interest_no = rdi.Interest_No) AS Premium,
        a.Period_To AS PeriodFrom,
        DATEADD(DAY, DATEDIFF(DAY, a.Period_From,a.Period_To), a.Period_To) AS PeriodTo,
rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a 
INNER JOIN [Dtl_Interest] rdi ON rdi.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if(listOSInterest == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        listOSInterest = db.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(query, PolicyNo);
                    }
                }
                else
                {
                    if(listOSInterest.Count == 0)
                    {
                        using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                        {
                            listOSInterest = db.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(query, PolicyNo);
                        }
                    }
                }

                #region Insert Order Simulation Interest
                query = @"
DELETE FROM OrderSimulationInterest WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2
INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1,@8";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationInterestRenot osI in listOSInterest)
                    {
                        db.Execute(query, orderID, osI.ObjectNo, osI.InterestNo, osI.InterestID, osI.Year
                            , osI.Premium,osI.PeriodFrom,osI.PeriodTo,osI.DeductibleCode);
                    }
                }

                #region Query Order Simulation Coverage
                query = @"SELECT rdc.Object_No AS ObjectNo,
		rdc.Interest_No AS InterestNo,
		rdc.Coverage_no AS CoverageNo,
		rdc.Coverage_ID AS CoverageID,
		rdc.Rate AS Rate,
		rdi.sum_insured AS SumInsured,
		rdc.Loading AS LoadingRate,
		(rdc.Loading*rdc.net_premium)/100 AS Loading,
		rdc.net_premium AS Premium,
        a.Period_To AS BeginDate,
        DATEADD(DAY, DATEDIFF(DAY, a.Period_From,a.Period_To), a.Period_To) AS EndDate,
		CASE WHEN rdc.net_premium = 0 OR rdc.net_premium is null THEN 1 ELSE 0 END AS IsBundling,
		100 AS EntryPct,
		DATEDIFF(day, a.Period_To, DATEADD(YEAR, 1,a.Period_To)) AS Ndays,
		rdc.Excess_Rate AS ExcessRate,
		1 AS CalcMethod,
		rdc.Cover_Premium AS CoverPremium,
		rdc.Gross_Premium AS GrossPremium,
		rdc.Max_SI AS MaxSI,
		rdc.Net1 AS Net1,
		rdc.Net2 AS Net2,
		rdc.Net3 AS Net3,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Coverage] rdc WITH ( NOLOCK ) ON rdc.policy_no = rn.Policy_No AND rdi.interest_no = rdc.interest_no
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                {
                    listOSCoverage = db.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(query, PolicyNo);
                }

                query = @"SELECT rdc.Object_No AS ObjectNo,
rdc.Interest_No AS InterestNo,
rdc.Coverage_no AS CoverageNo,
rdc.Coverage_Id AS CoverageID,
rdc.Rate AS Rate,
rdc.Sum_Insured  AS SumInsured,
rdc.Loading AS LoadingRate,
(rdc.Loading*rdc.net_premium)/100 AS Loading,
rdc.net_premium AS Premium,
        a.Period_To AS BeginDate,
        DATEADD(DAY, DATEDIFF(DAY, a.Period_From,a.Period_To), a.Period_To) AS EndDate,
CASE WHEN rdc.net_premium = 0 OR rdc.net_premium is null THEN 1 ELSE 0 END AS IsBundling,
rdc.Entry_Pct [EntryPct],
rdc.NDays [NDays],
Net1,
Net2,
Net3,
Gross_Premium [GrossPremium],
Cover_Premium [CoverPremium],
rdc.Calc_Method [CalcMethod],
Excess_Rate [ExcessRate],
Max_si [MaxSi],
Deductible_Code [DeductibleCode]
FROM Policy AS a 
INNER JOIN [Dtl_Coverage] rdc ON rdc.Policy_id = a.Policy_id
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.Policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if(listOSCoverage == null)
                {
                    using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                    {
                        listOSCoverage = db.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(query, PolicyNo);
                    }
                }
                else
                {
                    if (listOSCoverage.Count == 0)
                    {
                        using (a2isDBHelper.Database db = new a2isDBHelper.Database("AAB"))
                        {
                            listOSCoverage = db.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(query, PolicyNo);
                        }
                    }
                }
                

                #region Insert Order Simulation Coverage
                query = @"DELETE FROM OrderSimulationCoverage WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2 AND CoverageNo = @3
    INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@22,@23,GETDATE(),1,@10,
            @11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationCoverageRenot osC in listOSCoverage)
                    {
                        db.Execute(query, orderID, osC.ObjectNo, osC.InterestNo, osC.CoverageNo
                            , osC.CoverageID, osC.Rate, osC.SumInsured, osC.LoadingRate, osC.Loading
                            , osC.Premium, osC.IsBundling, osC.EntryPct, osC.NDays, osC.ExcessRate
                            , osC.CalcMethod, osC.CoverPremium, osC.GrossPremium, osC.MaxSi
                            , osC.Net1, osC.Net2, osC.Net3, osC.DeductibleCode, osC.BeginDate,osC.EndDate);
                    }
                }

                query = @"DELETE FROM [dbo].[ImageData] WHERE DeviceID = @0
                INSERT INTO [dbo].[ImageData]
                ([ImageID],[DeviceID],[SalesOfficerID],[PathFile]
                ,[EntryDate],[LastUpdatedTime],[RowStatus],[Data]
                ,[ThumbnailData],[CoreImage_ID])
                SELECT tid.ImageID,tid.FollowUpNo,tid.SalesOfficerID,tid.PathFile,
                GETDATE(),GETDATE(),1,tid.Data,tid.ThumbnailData,NULL
                FROM dbo.TempImageData tid
                WHERE tid.FollowUpNo = @0";

                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query,followUpID);
                }

                /*FORMRENEWAL
                RENEWALNOTICE
                BUKTIBAYAR
                KTP
                STNK
                SURVEY
                DOCNSA1
                DOCNSA2*/

                UpdateImageFollowUp(followUpID);

                query = @"UPDATE FollowUp
                          SET [FollowUpStatus] = 2
                          ,[FollowUpInfo] = 61
                           WHERE FollowUpNo = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    db.Execute(query, followUpID);
                }


                #region Update Status Exclude Renewal Policy
                query = @"UPDATE [Excluded_Renewal_Policy] SET RowStatus=1 WHERE Policy_No=@0";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    db.Execute(query, PolicyNo);

                }

            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        public static void SendPushNotifEditedByOthers(string Editor, string SalesOfficerID,string FollowUpNo)
        {
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    if (!Editor.ToUpper().Trim().Equals(SalesOfficerID.ToUpper().Trim()))
                    {
                        string OrderNo = db.ExecuteScalar<string>("SELECT OrderNo FROM Ordersimulation WHERE FollowUpNo=@0", FollowUpNo);
                        Otosales.Logic.CommonLogic.CreatePushNotification(Otosales.Models.PushNotificationCategory.FOLLOWUPSTATUSCHANGEDBYOTHERS, OrderNo, Editor);
                    }
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        public static string UpdateTaskListDetail(string CustID, string FollowUpNo,
            string personalData, string companyData, string vehicleData, string policyAddress,
            string surveySchedule, string imageData, string OSData, string OSMVData,
            string calculatedPremiItems, string FollowUpStatus, string Remarks)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string result = "";
            PersonalData pd = new PersonalData();
            CompanyData cd = new CompanyData();
            VehicleData vd = new VehicleData();
            PolicyAddress pa = new PolicyAddress();
            SurveySchedule ss = new SurveySchedule();
            Remarks rm = new Remarks();
            List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData = new List<Otosales.Models.vWeb2.ImageDataTaskDetail>();

            string query = @"";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            try
            {
                updateFollowUpStatusToPolicyCreated(FollowUpNo);
                if (!string.IsNullOrEmpty(personalData))
                {
                    pd = JsonConvert.DeserializeObject<PersonalData>(personalData);
                    query = @";IF EXISTS(SELECT * FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCustomer SET 
	                        Name = @1, CustBirthDay = @2, CustGender = @3, CustAddress = @4, 
                            PostalCode = @5, Email1 = @6, Phone1 = @7, IdentityNo = @8, 
                            isCompany = @9
                            WHERE CustID = @0 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @12
                        AND ApplyF = 1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.OrderSimulation 
	                        SET IsNeedDocRep = @10, AmountRep = @11
	                        WHERE CustID = @0 AND FollowUpNo = @12
	                        AND ApplyF = 1 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @12)
                        BEGIN
	                        UPDATE dbo.FollowUp 
	                        SET FollowUpName = @1, ProspectName = @1
	                        WHERE CustID = @0 AND FollowUpNo = @12
                        END ";
                    db.Execute(query, CustID
                        , pd.Name, pd.CustBirthDay, pd.CustGender, pd.CustAddress
                        , pd.PostalCode, pd.Email1, pd.Phone1, pd.IdentityNo
                        , pd.isCompany, pd.isNeedDocRep, pd.AmountRep, FollowUpNo);
                }
                if (!string.IsNullOrEmpty(companyData))
                {
                    cd = JsonConvert.DeserializeObject<CompanyData>(companyData);
                    query = @";IF EXISTS (SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCompany SET CompanyName = @2, NPWPno = @3, NPWPdate = @4, 
	                        NPWPaddress = @5, OfficeAddress = @6, PostalCode = @7, PICPhoneNo = @8, PICname = @9, Email = @10 
	                        WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
							UPDATE dbo.ProspectCustomer SET Name = @9, Phone1 = @8, Email1 = @10
							WHERE CustID = @0
                        END";
                    db.Execute(query, CustID, FollowUpNo
                        , cd.CompanyName, cd.NPWPno, cd.NPWPdate
                        , cd.NPWPaddres, cd.OfficeAddress, cd.PostalCode, cd.PICPhoneNo, cd.PICname, cd.PICEmail);
                }
                if (!string.IsNullOrEmpty(OSData) && !string.IsNullOrEmpty(OSMVData))
                {
                    InsertOrderSimulation(OSData, OSMVData);
                }
                if (!string.IsNullOrEmpty(vehicleData))
                {
                    vd = JsonConvert.DeserializeObject<VehicleData>(vehicleData);
                    
                    if (!string.IsNullOrEmpty(vd.ProductCode))
                    {
                        query = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET ProductTypeCode = @2,
	                            VehicleCode = @3, BrandCode = @4, ModelCode = @5,
	                            Series = @6, Type = @7, Sitting = @8, Year = @9,
	                            CityCode = @10, UsageCode = @11, AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET InsuranceType = @19, 
                                ProductCode = @20, 
								SegmentCode = @21, PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                    }
                    else {
                        query = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                    }
                    db.Execute(query, CustID, FollowUpNo
                        , vd.ProductTypeCode
                        , vd.VehicleCode, vd.BrandCode, vd.ModelCode
                        , vd.Series, vd.Type, vd.Sitting, vd.Year
                        , vd.CityCode, vd.UsageCode, vd.AccessSI
                        , vd.RegistrationNumber, vd.EngineNumber
                        , vd.ChasisNumber, vd.IsNew, vd.SumInsured
                        , vd.ColorOnBPKB

                        , vd.InsuranceType
                        , vd.ProductCode
                        , vd.SegmentCode, vd.PeriodFrom, vd.PeriodTo
                        , vd.IsORDefectsRepair, vd.IsAccessoriesChange
                        , vd.IsPolicyIssuedBeforePaying
                        , vd.Remarks

                        , vd.DealerCode
                        , vd.SalesDealer);
                }
                if (!string.IsNullOrEmpty(calculatedPremiItems)) {
                    string OrderNo = db.ExecuteScalar<string>(
                        @";DECLARE @@Orderno VARCHAR(100) 
                        SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                    INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                    WHERE f.CustID = @0 AND f.FollowUpNo = @1 
                        AND o.RowStatus = 1 AND ApplyF = 1
                        SELECT @@Orderno"
                        , CustID, FollowUpNo);

                    vd = JsonConvert.DeserializeObject<VehicleData>(vehicleData);
                    InsertExtendedCover(OrderNo,calculatedPremiItems,vd.PeriodFrom,vd.PeriodTo);
                }
                if (!string.IsNullOrEmpty(policyAddress))
                {
                    pa = JsonConvert.DeserializeObject<PolicyAddress>(policyAddress);
                    query = @"IF EXISTS (SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulation SET PolicySentTo = @2, PolicyDeliveryName = @3, 
	                            PolicyDeliveryAddress = @4, PolicyDeliveryPostalCode = @5 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                    db.Execute(query, CustID, FollowUpNo
                        , pa.SentTo, pa.Name
                        , pa.Address, pa.PostalCode);
                }
                if (!string.IsNullOrEmpty(surveySchedule))
                {
                    ss = JsonConvert.DeserializeObject<SurveySchedule>(surveySchedule);
                    query = @";DECLARE @@OrderNo VARCHAR(100);
                            SELECT @@OrderNo=OrderNo FROM dbo.OrderSimulation
                            WHERE CustID = @0 AND FollowUpNo = @1 AND 
                            RowStatus = 1 AND ApplyF = 1
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationSurvey oss
                            INNER JOIN dbo.OrderSimulation os ON os.OrderNo = oss.OrderNo
                            WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND 
                            os.RowStatus = 1 AND os.ApplyF = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulationSurvey SET CityCode = @2, LocationCode = @3, SurveyAddress = @4,
	                            SurveyPostalCode = @5, SurveyDate = @6, ScheduleTimeID = @7, ModifiedBy = 'OtosalesAPI',
								IsNeedSurvey = @8, IsNSASkipSurvey = @9,
								ModifiedDate = GETDATE() WHERE OrderNo = @@OrderNo
                            END
							ELSE
							BEGIN
								INSERT INTO dbo.OrderSimulationSurvey
								        ( OrderNo ,
								          CityCode ,
								          LocationCode ,
								          SurveyAddress ,
								          SurveyPostalCode ,
								          SurveyDate ,
								          ScheduleTimeID ,
								          RowStatus ,
								          CreatedBy ,
								          CreatedDate,
										  IsNeedSurvey,
										  IsNSASkipSurvey
								        )
								VALUES  ( @@OrderNo , -- OrderNo - varchar(100)
								          @2 , -- CityCode - int
								          @3 , -- LocationCode - int
								          @4 , -- SurveyAddress - varchar(500)
								          @5 , -- SurveyPostalCode - varchar(5)
								          @6 , -- SurveyDate - datetime
								          @7 , -- ScheduleTimeID - datetime
								          1 , -- RowStatus - bit
								          'OtosalesAPI' , -- CreatedBy - varchar(50)
								          GETDATE(), -- CreatedDate - datetime
										  @8,
										  @9
								        )
							END";
                    db.Execute(query, CustID, FollowUpNo
                        , ss.CityCode, ss.LocationCode, ss.SurveyAddress
                        , ss.SurveyPostalCode, ss.SurveyDate, ss.ScheduleTimeID
                        , ss.IsNeedSurvey, ss.IsNSASkipSurvey);
                }
                if (!string.IsNullOrEmpty(imageData))
                {
                    ListImgData = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.ImageDataTaskDetail>>(imageData);
                    UpdateImageData(ListImgData, CustID, FollowUpNo);

                }
                if (!string.IsNullOrEmpty(FollowUpStatus)) {
                    query = @"UPDATE dbo.FollowUp SET FollowUpStatus = @2 WHERE CustID = @0 AND FollowUpNo = @1";
                    db.Execute(query, CustID, FollowUpNo, FollowUpStatus);
                }
                query = @"SELECT OrderNo FROM dbo.OrderSimulation
                        WHERE CustID = @0 AND FollowUpNo = @1 AND 
                        RowStatus = 1 AND ApplyF = 1";
                List<dynamic> res = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (res.Count > 0) {
                    result = res.First().OrderNo;
                }
                if (!string.IsNullOrEmpty(Remarks)) {
                    query = @"UPDATE dbo.FollowUp
                            SET RemarkToSA = @0 WHERE FollowUpNo = @1";
                    rm = JsonConvert.DeserializeObject<Remarks>(Remarks);
                    db.Execute(query, rm.RemarkToSA, FollowUpNo);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public static List<CalculatedPremi> CalculateACCESS(decimal vPrimarySI, decimal TSIInterest, string ProductCode, DateTime PeriodFrom, DateTime PeriodTo, string InterestID, string vtype, string vyear, string vsitting, string QuotationNo, List<CalculatedPremi> calculatePremiItems, List<DetailScoring> scoringList,
                                                            string OldPolicyNo, out List<decimal> RptDiscount, out List<decimal> DiscountTemp)
        {
            List<decimal> RDP = new List<decimal>();
            List<decimal> DP = new List<decimal>();
            int vResult = RetailRepository.ValidatePerInterest(InterestID, calculatePremiItems, PeriodFrom, PeriodTo, ProductCode);
            List<CalculatedPremi> calculateditems = new List<CalculatedPremi>();
            calculateditems= calculatePremiItems;
            List<CalculatedPremi> AccessResult = new List<CalculatedPremi>();
            if (vResult == 0)
            {
                foreach (CalculatedPremi data in calculateditems)
            {
                
                if (data.InterestID.TrimEnd().Equals("CASCO")&&(!data.IsBundling)) { 
                if (data.Premium > 0) {

                    scoringList.RemoveAll(x => x.FactorCode.Trim().Equals("VHCTYP"));
                    DetailScoring ds = new DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    if (!data.IsBundling && new List<string>() { "SRCC", "FLD", "ETV", "EQK" }.Contains(data.CoverageID))
                    {

                        ds.InsuranceCode = "ALL";
                    }
                    else
                    {

                        ds.InsuranceCode = vtype;
                    }
                    scoringList.Add(ds);
                ParamCalculatePremiPerCoverage param = new ParamCalculatePremiPerCoverage();
                param.ProductCode = ProductCode;
                param.InterestId = InterestID;
                param.CoverageId = data.CoverageID;
                param.TSICurrID = "IDR";
                param.TSInterest = TSIInterest;
                param.PrimarySI = vPrimarySI;
                //param.PeriodFrom = Convert.ToDateTime(data.PeriodFrom);
                //param.PeriodTo = Convert.ToDateTime(data.PeriodTo);
                //param.PeriodFromCover = Convert.ToDateTime(data.CoverageListPerInterest[i].CoverFromDT);
                //param.PeriodToCover = Convert.ToDateTime(data.CoverageListPerInterest[i].CoverToDT);
                param.PeriodFrom = PeriodFrom;
                param.PeriodTo = PeriodTo;
                param.PeriodFromCover = data.PeriodFrom;
                param.PeriodToCover = data.PeriodTo;
                //param.NDays = Convert.ToString((vPToCover - vPFromCover).Days);
                param.NDays = data.Ndays.ToString();
                param.VehicleType = vtype;
                param.YearManufacturing = vyear;
                param.AutoApply = data.AutoApply;
                param.NumberOfSeat = vsitting;
                param.CalculationMethod = data.CalcMethod;
                param.ScoringList = scoringList;
                // param.MouID = ;

                List<CalculateSumInsured> listPremi = new List<CalculateSumInsured>();
                List<CalculateSumInsured> listPremTemp = new List<CalculateSumInsured>();
                if (data.CoverageID.Trim().ToUpper() == "ALLRIK" || data.CoverageID.Trim().ToUpper() == "TLO")
                {
                    listPremTemp = pm.CalculateBasicPremium(param);
                    listPremi.AddRange(listPremTemp);
                    listPremTemp.Clear();
                }
                else if (string.IsNullOrWhiteSpace(param.AutoApply) || param.AutoApply == AutoApplyFlag.NonAutoApply
                    || param.AutoApply == AutoApplyFlag.CompreExtendedGroup || param.AutoApply == AutoApplyFlag.TLOExtendedGroup)
                {
                    listPremTemp = pm.CalculatePremiPerCoverage(param);
                    listPremi.AddRange(listPremTemp);
                    listPremTemp.Clear();

                }
                foreach (var item in listPremi)
                {
                    //For ACCESS is only CoverageId TLO or ALLRIK
                    if (!data.InterestID.TrimEnd().Equals("ACCESS") || (data.InterestID.TrimEnd().Equals("ACCESS") && (item.CoverageID.TrimEnd().Equals("TLO") || item.CoverageID.TrimEnd().Equals("ALLRIK"))))
                    {
                    var PIC = RetailRepository.GetProductInterestCoverage(ProductCode: param.ProductCode, InterestID: data.InterestID, CoverageID: item.CoverageID);
                    if (data.InterestID.TrimEnd().ToUpper() == "TPLPER" && item.Rate < 0)
                    {
                        var DevPriceCoverProgressive = RetailRepository.GetDefaultPriceCoverProgressive(param.ProductCode, data.InterestID, item.CoverageID);
                        if (DevPriceCoverProgressive != null)
                        {
                            if (DevPriceCoverProgressive.Count > 0)
                            {
                                decimal devRate = RetailRepository.GetDefaultRateProgressive(param.ProductCode, data.InterestID, item.CoverageID, item.SumInsured, param.ScoringList);
                                if (devRate <= 0)
                                {
                                    devRate = ((item.Premium / item.SumInsured) / (item.EntryPercentage / 100)) * 100;
                                    devRate = decimal.Round(devRate, 4);
                                }
                                item.Rate = devRate;
                                //item.ExcessRate = devRate;
                            }
                        }
                    }
                    var PD = RetailRepository.GetDeductible(ProductCode, data.InterestID, item.CoverageID);
                    if (PD != null)
                    {
                        data.DeductibleCode = PD.DeductibleCode;
                    }

                    PremiumScheme PS = new PremiumScheme();
                    #region Discount
                    try
                    {
                        var Discount = RetailRepository.GetWTDiscount( param.ProductCode, param.MouID);
                        if (!string.IsNullOrWhiteSpace(OldPolicyNo))
                        {
                            var RenDiscount = RetailRepository.GetRenewalDiscount(OldPolicyNo);
                            var Disc = RetailRepository.GetWTDiscount(ProductCode, ProductCode);// New
                            foreach (WTCommission ren in RenDiscount)
                            {
                                if (ren.CommissionID.Contains("RD"))
                                {
                                    RDP.Add(ren.Percentage);
                                }
                            }
                            foreach (WTCommission D in Disc)
                            {
                                DP.Add(D.Percentage);
                            }
                            Discount.AddRange(RenDiscount);
                        }
                        List<CommissionScheme> DiscountList = RetailRepository.InitDiscountList(item.CoverageID, Discount, false);
                        if (DiscountList.Count > 0)
                        {
                            DiscountList = RetailRepository.InitFlatDiscount(param.ProductCode, item.Premium, DiscountList, item.SumInsured, 1, "DISCOUNT", false);
                        }
                        PS = RetailRepository.CalculateDiscountPremium(DiscountList, item.Premium);
                    }
                    catch (Exception er)
                    {
                        logger.Error(string.Format("Method {0} Sub Discount Error : {1}", er.ToString()));
                    }
                    #endregion
                    #region Commission
                    try
                    {
                        var CommissionApplied = RetailRepository.GetWTCommission(param.ProductCode, param.MouID);
                        List<CommissionScheme> CommisionList = RetailRepository.InitCommisionList(item.CoverageID, CommissionApplied, true);
                        List<CommissionScheme> CommissionList2 = RetailRepository.InitFlatDiscount(param.ProductCode, item.Premium, CommisionList, item.SumInsured, 1, "COMMISSION", true);
                    }
                    catch (Exception er)
                    {
                        logger.Error(string.Format("Method {0} Sub Discount Error : {1}", er.ToString()));
                    }
                    #endregion
                    if (item.Premium > 0) {
                    int AutoApply = Convert.ToInt16(!string.IsNullOrWhiteSpace(item.AutoApply) ? item.AutoApply.ToString() : data.AutoApply);
                       
                    AccessResult.Add(new CalculatedPremi
                    {
                        InterestID = InterestID,
                        CoverageID = item.CoverageID,
                        //CoverageDesc = !string.IsNullOrWhiteSpace(item.Description) ? item.Description : PIC[0].Description,
                        //PeriodFrom = item.CoverFrom.ToString(),
                        //PeriodTo = item.CoverTo.ToString(),
                        PeriodFrom = item.CoverFrom,
                        PeriodTo = item.CoverTo,
                        DeductibleCode = data.DeductibleCode,
                        //DeductibleDesc = string.IsNullOrWhiteSpace(data.CoverageListPerInterest[i].DeductibleCode) ? string.Empty : string.Format("{0} - {1}", data.CoverageListPerInterest[i].DeductibleCode, data.CoverageListPerInterest[i].DeductibleDesc),
                        //Currency = data.CoverageListPerInterest[i].CurrencyID,
                        //AgreedValue = item.AgreedValue,
                        //Interest = PIC[0].InterestDescription,
                        Premium = Convert.ToDouble(PS.Net3),
                        //Premium = PS.NetPremium,
                        Rate = Convert.ToDouble(item.Rate),
                        //CoverageType = !string.IsNullOrWhiteSpace(item.CoverageType) ? item.CoverageType : data.CoverageListPerInterest[i].CoverType,
                        //CoverType = !string.IsNullOrWhiteSpace(item.CoverageType) ? item.CoverageType : data.CoverageListPerInterest[i].CoverType,
                        //BasicCoverageID = !string.IsNullOrWhiteSpace(item.BasicCoverageID) ? item.BasicCoverageID : data.CoverageListPerInterest[i].BasicCoverageID,
                        Net1 = PS.Net1,
                        Net2 = PS.Net2,
                        Net3 = PS.Net3,
                        GrossPremium = PS.GrossPremium,
                        CoverPremium = PS.GrossPremium,
                        ExcessRate = item.ExcessRate,
                        MaxSI = item.MaxSi,
                        Ndays = item.NDays,
                        EntryPrecentage = item.EntryPercentage,
                        LoadingRate = Convert.ToDouble(item.Loading),
                        Loading = Convert.ToDouble(item.Loading * PS.Net3 / 100),
                        SumInsured = Convert.ToDouble(item.SumInsured),
                        CalcMethod = data.CalcMethod,
                        AutoApply = (PS.GrossPremium > 0) ? AutoApply.ToString() : "0",
                        IsBundling = AutoApply > 0 ? true : false 
                    });
                    }
                    }

                }
                }
            }}
            }
            DiscountTemp = DP;
            RptDiscount = RDP;
            return AccessResult;
        }
       public static List<dynamic> GetSurveyCity()
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<dynamic> result = new List<dynamic>();
            try
            {
                #region Query City Provider
                string queryLoc = @";
                                    SELECT  result.ID ,
                                            result.NAME
                                    FROM    ( SELECT    c.ID ,
                                                        c.NAME
                                              FROM      Branch b
                                                        INNER JOIN city c ON b.CITYID = c.ID
                                              WHERE     b.type IN ( 2, 5, 6 )					
                                                        AND b.ISDELETED = 0
                                              UNION
                                              SELECT    a.ID ,
                                                        a.NAME
                                              FROM      city a
                                                        INNER JOIN Godigital.Fn_SplitString(@0, ',') b ON b.part = a.ID
                                            ) result		
                                    ORDER BY result.Name ASC";


                //queryLoc = String.IsNullOrWhiteSpace(AppParameterAccess.GetAppParamValue("SURVEY-CITY-LIMIT-LIST")) ?
                //   queryLoc.Replace("#additionalFilter#", "")
                //   : queryLoc.Replace("#additionalFilter#", " INNER JOIN Godigital.Fn_SplitString(@1, ',') d ON d.part = c.ID ");

                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB))
                {
                    //result = db.Fetch<dynamic>(queryLoc, AppParameterAccess.GetAppParamValue("SURVEY-CITY-DEFAULT-LIST"), AppParameterAccess.GetAppParamValue("SURVEY-CITY-LIMIT-LIST"));
                    //result = db.Fetch<dynamic>(queryLoc);
                    result = db.Fetch<dynamic>(queryLoc, AppParameterAccess.GetAppParamValue("SURVEY-CITY-DEFAULT-LIST"));
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static GetSurveyLocationResult GetSurveyLocation(FormDataCollection form)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            #region
            string queryData;
            #endregion
            int paramLoc = 0, paramType = 1;
            string temp, pageType;
            GetSurveyLocationResult res = new GetSurveyLocationResult();
            try
            {
                temp = form.Get("pageType");
                if (String.IsNullOrEmpty(temp)) pageType = "BRANCH";
                else
                {
                    pageType = temp;
                }

                temp = form.Get("location");

                if (String.IsNullOrEmpty(temp)) pageType = "BRANCH";
                else
                {
                    paramLoc = Int32.Parse(temp);
                }
                switch (pageType)
                {
                    case "PROVIDER":
                        paramType = 3;
                        queryData = "select branch.NAME NAME, branch.ADDRESS ADDRESS, branch.FAX FAX, branch.PHONE PHONE, type.NAME  TNAME from Branch branch inner join BranchType type on type.ID = branch.TYPE where (CITYID = @0 or @0 = 0) and  branch.isDeleted = 0 and branch.TYPE = 3  order by  branch.NAME asc";
                        break;
                    case "WORKSHOP":
                        paramType = 4;
                        queryData = "select branch.NAME NAME, branch.ADDRESS ADDRESS, branch.FAX FAX, branch.PHONE PHONE,type.NAME TNAME from Branch branch inner join BranchType type on type.ID = branch.TYPE where (CITYID = @0 or @0 = 0) and branch.isDeleted = 0 and branch.TYPE = 4 order by branch.NAME  asc";
                        break;
                    default:
                        temp = form.Get("type");
                        if (String.IsNullOrEmpty(temp)) paramType = 1;
                        else
                        {
                            paramType = Int32.Parse(temp);
                        }

                        if (paramType == 0)
                        {
                            queryData = "select branch.ID,branch.NAME NAME, branch.ADDRESS ADDRESS, branch.FAX FAX, branch.PHONE PHONE,type.NAME TNAME from Branch branch inner join BranchType type on type.ID = branch.TYPE  where (CITYID = @0 or @0 = 0) and branch.isDeleted = 0 and branch.TYPE in(2,5,6) order by branch.NAME asc";
                        }
                        else
                        {
                            queryData = "select branch.ID,branch.NAME NAME, branch.ADDRESS ADDRESS, branch.FAX FAX, branch.PHONE PHONE,type.NAME TNAME from Branch branch inner join BranchType type on type.ID = branch.TYPE  where (CITYID = @0 or @0 = 0) AND (TYPE = @1 or @1  = 0) and branch.isDeleted = 0 and branch.TYPE in(2,5,6) order by branch.NAME asc";
                        }
                        break;
                }

                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB))
                {
                    int pg = 1;
                    temp = form.Get("pageNum");

                    if (String.IsNullOrEmpty(temp)) pg = 1;
                    else
                    {
                        pg = Int32.Parse(temp);
                    }

                    res.data = db.Fetch<dynamic>(queryData, paramLoc, paramType);

                    res.spesific = false;
                    if (pageType.ToLower() == "branch")
                    {
                        res.spesific = db.SingleOrDefault<int>(@"
select sum([count]) from(
SELECT count(1) [count] FROM dbo.city WHERE id IN (SELECT CityId FROM dbo.Branch WHERE Type IN (2,5,6) AND CityId = @0 GROUP BY CityId) 
UNION
select count(1) [count] from Godigital.Fn_SplitString((select COALESCE(OptionValue,'') AS ParamValue from [AsuransiAstra].[dbo].[AsuransiAstraOptions] where OptionName = 'SURVEY-CITY-DEFAULT-LIST'),',') where part=@0) a"
, paramLoc) > 0 ? true : false;
                    }
                }
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static dynamic GetSurveyZipCode(string CityID)
        {
            dynamic result;
            try
            {
                #region QueryData
                string queryData = @"
SELECT PostalCode ZipCode,PostalDescription ZipCodeDescription
FROM dbo.Postal p
INNER JOIN dbo.City c 
ON p.CityID = c.ID
WHERE c.ID = @0 AND p.RowStatus = 0 
ORDER BY PostalDescription ASC";
                #endregion

                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB))
                {
                    result = db.Fetch<dynamic>(queryData, CityID);
                    logger.Debug("OK");
                    return result;
                }
            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }
        }

        public static void InsertQuotationPrintTable(string orderNo)
        {
            try
            {
                #region QueryData
                string query = @"";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                double AdminFee = db.ExecuteScalar<double>("SELECT AdminFee FROM OrderSimulation WHERE OrderNo=@0 ",orderNo);
                List<CalculatedPremi> cp = LoadCalculatedpremibyOrderNo(orderNo,"");
                query = @"select * from ordersimulation WHERE orderno=@0";
                OrderSimulation os = db.FirstOrDefault<OrderSimulation>(query, orderNo);
                query = @"select * from ordersimulationMV WHERE orderno=@0";
                OrderSimulationMV osmv = db.FirstOrDefault<OrderSimulationMV>(query, orderNo);
                #region detail scoring
                List<DetailScoring> dtlScoring = new List<DetailScoring>();
                DetailScoring ds = new DetailScoring();
                if (!string.IsNullOrEmpty(osmv.CityCode))
                {
                    ds.FactorCode = "GEOGRA";
                    ds.InsuranceCode = osmv.CityCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.UsageCode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "MVUSAG";
                    ds.InsuranceCode = osmv.UsageCode;
                    dtlScoring.Add(ds);
                }
                ds = new DetailScoring();
                ds.FactorCode = "VHCTYP";
                ds.InsuranceCode = string.IsNullOrEmpty(osmv.Type) ? "ALL   " : osmv.Type;
                dtlScoring.Add(ds);
                if (!string.IsNullOrEmpty(osmv.BrandCode))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "VBRAND";
                    ds.InsuranceCode = osmv.BrandCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.IsNew.ToString()))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "NEWVHC";
                    ds.InsuranceCode = osmv.IsNew.ToString();
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.ModelCode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "VMODEL";
                    ds.InsuranceCode = osmv.ModelCode;
                    dtlScoring.Add(ds);
                }
                #endregion
                DetailCoverageSummary rs = MobileRepository.GetDetailCoverageSummary(os.ProductCode, dtlScoring, cp, AdminFee, osmv.Year, osmv.UsageCode, Convert.ToInt16(cp[0].Ndays), os.PeriodFrom, os.PeriodTo
                                                                                      , osmv.Type, osmv.Sitting.ToString(), os.OldPolicyNo);
                List<string> IEP = GetApplicationParametersValue("OTOSALES-SEGMENT-IEP");
                var DiscountList = RetailRepository.GetWTDiscount(os.ProductCode, os.ProductCode);
                decimal Discount = DiscountList.Count() > 0 ? DiscountList[0].Percentage : 0;
                query = @" DECLARE @@id AS bigint
set @@id= (SELECT RptPrintQuotationID FROM [RptPrintQuotation] WHERE OrderNo=@0)
IF ( @@id is not null)
BEGIN
IF EXISTS(SELECT * FROM [RptPrintQuotationDetail] WHERE RptPrintQuotationID=@@id)
BEGIN
DELETE FROM RptPrintQuotationDetail WHERE RptPrintQuotationID=@@id
END
DELETE FROM [RptPrintQuotation] WHERE OrderNo=@0
END
";
                db.Execute(query,orderNo);

                query = @";Declare @@TEMPID TABLE (RptPrintQuotationID bigint)
INSERT INTO [dbo].[RptPrintQuotation]
           ([OrderNo]
           ,[AdminFee]
           ,[PADRVRTSI]
           ,[PAPASSTSI]
           ,[TPLTSI]
           ,[InsuranceType]
           ,[QuotationNo]
           ,[YearCoverage]
           ,[InsuredAddress]
           ,[CompanyAddress]
           ,[PolicyNo]
           ,[IsCompany]
           ,[CompanyName]
           ,[InsuredName]
           ,[PICName]
           ,[InsuredPhone]
           ,[PICPhoneNo]
           ,[InsuredEmail]
           ,[PICEmail]
           ,[PeriodFrom]
           ,[PeriodTo]
           ,[SalesOfficerID]
           ,[BranchCode]
           ,[SalesmanName]
           ,[SalesmanPhone]
           ,[SalesmanEmail]
           ,[SalesmanRole]
           ,[InsuredObject]
           ,[VehicleYear]
           ,[PassSeat]
           ,[ChasisNumber]
           ,[EngineNumber]
           ,[Usage]
           ,[RegistrationNumber]
           ,[GeoArea]
           ,[VAPermata]
           ,[VAMandiri]
           ,[VABCA]
           ,[ProductType]
           ,[City]
           ,[AccountNo]
           ,[BankDescription]
           ,[BankName]
           ,[NoCover]
           ,[OriginalDefect]
           ,[NonStandardAcc]
           ,[DiscountPct])
       output Inserted.RptPrintQuotationID INTO @@TEMPID(RptPrintQuotationID)
    select 
			@0
           ,@1
           ,@2
           ,@3
           ,@4
		   ,CASE WHEN mp.Biz_type = 2 THEN 4 
				ELSE os.InsuranceType END AS InsuranceType
			,os.QuotationNo
			,os.YearCoverage
			,os.PolicyDeliveryAddress AS InsuredAddress
			,COALESCE(pcy.OfficeAddress,pcy.NPWPAddress) AS CompanyAddress
			,COALESCE(os.PolicyNo,'-') AS PolicyNo
			,pc.IsCompany
			,COALESCE(pcy.CompanyName,'') as CompanyName
			,pc.Name as InsuredName
			,COALESCE(pcy.PICname ,'') as PICname
			,pc.Phone1 AS InsuredPhone
			,COALESCE(pcy.PICPhoneNo,'') as PICPhoneNo
			,pc.Email1 as InsuredEmail
			,COALESCE(pcy.Email,'') AS PICEmail
			,os.PeriodFrom
			,os.PeriodTo
			,os.SalesOfficerID
			,os.BranchCode
			,so.Name AS SalesmanName
			,COALESCE(geu.MobileNumber,COALESCE(so.Phone1,'')) AS SalesmanPhone
			,COALESCE(geu.UserId,COALESCE(so.email,'')) AS SalesmanEmail
			,so.Role AS SalesmanRole
			,CONCAT(vb.Description,' ',vm.Description,' ',osm.Series) AS InsuredObject
			,osm.Year AS VehicleYear
			,(osm.Sitting-1) AS PassSeat
			,osm.ChasisNumber
			,osm.EngineNumber
			,u.Description AS Usage
			,osm.RegistrationNumber
			,r.Description AS GeoArea
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVAPERMATA'),COALESCE(os.VANumber,'')) as VAPermata
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVAMANDIRI'),COALESCE(os.VANumber,'')) as VAMandiri
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVABCA'),COALESCE(os.VANumber,'')) as VABCA
			,CASE WHEN os.SegmentCode IN (SELECT ms.SegmentCode FROM [BeyondReport].[AAB].dbo.Mapping_Product_SOB mpsob
                                    INNER JOIN [BeyondReport].[AAB].dbo.Mapping_Segment ms
                                    ON mpsob.SOB_ID = ms.SegmentCode
                                    WHERE 
                                    (SegmentCode2 = 'SG2016' OR SegmentCode2 = 'SG2017')) THEN 'PROKHUS'
					WHEN os.ProductCode IN (" + IEP[0] + @") THEN 'IEP'
					ELSE ' ' END AS ProductType
			,ab.City
			, b.accountno 
			, b.bankdescription  
			, b.name as BankName
           ,STUFF((	SELECT ', ' +  name
				FROM [BeyondReport].[AAB].[dbo].ord_dtl_nocover where order_no=os.PolicyOrderNo
				FOR XML PATH('')
			), 1, 1, '') AS NoCover
           ,STUFF((	SELECT ', ' +  name
				FROM [BeyondReport].[AAB].[dbo].Ord_Dtl_Original_Defect where order_no=os.PolicyOrderNo
				FOR XML PATH('')
			), 1, 1, '') AS OriginalDefect
           ,STUFF((SELECT ', '+
case when Category = 1
then
CONCAT(AccsDescription,' ',Brand)
when Category = 2
then
CONCAT(AccsDescription,' ',AccsPartDescription,' ',Brand)
when Category = 3
then
CONCAT(AccsDescription,' ',AccsPartDescription,' ',Brand)
when Category = 4
then
CONCAT(AccsCatDescription,' ',AccsPartDescription,' ',Brand)
when Category = 5
then
CONCAT(AccsCatDescription,' ',AccsPartDescription,' ',Brand)
end 
FROM    ( SELECT DISTINCT
                    wnsa.Category Category,
					AO.AccsDescription AccsDescription,
					wnsa.Brand Brand,
					AP.AccsPartDescription,
					x.AccsCatDescription
          FROM      [BeyondReport].[AAB].dbo.Ord_Non_Standard_Accessories AS wnsa
                    LEFT JOIN [BeyondReport].[AAB].dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.Accs_Code
                                                          AND ao.RowStatus = 1
                    LEFT JOIN [BeyondReport].[AAB].dbo.AccessoriesPart AP ON Ap.AccsCode = wnsa.Accs_Code
                                                    AND AP.AccsPartCode = wnsa.Accs_Part_Code
                                                    AND ap.RowStatus = 1
                    LEFT JOIN ( SELECT  RTRIM(ISNULL(ac.AccsCode, '')) AS AccsCode ,
                                        RTRIM(ISNULL(ac.AccsCatCode, '')) AS AccsCatCode ,
                                        RTRIM(ISNULL(ac.AccsCatDescription, '')) AS AccsCatDescription ,
                                        RTRIM(ISNULL(acp.AccsPartCode, '')) AS AccsPartCode ,
                                        ac.MaxSICategory ,
                                        ac.IsEditable ,
                                        acp.QtyDefault ,
                                        ac.RowStatus
                                FROM    [BeyondReport].[AAB].dbo.AccessoriesCategory AS ac
                                        LEFT JOIN [BeyondReport].[AAB].dbo.AccessoriesCategoryPart
                                        AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                  AND acp.RowStatus = 1
                              ) x ON x.AccsCode = ao.AccsCode
                                     AND x.RowStatus = 1
                                     AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                              '')
                                     AND x.AccsCode = wnsa.Accs_Code
                                     AND x.AccsCatCode = wnsa.Accs_Cat_Code
          WHERE     wnsa.Order_No = os.PolicyOrderNo
        ) AS X
				FOR XML PATH('')
			), 1, 1, '')  AS NonStandardAcc
    ,@5
	From ordersimulation os 
	INNER JOIN OrderSimulationMV osm ON osm.orderno = os.orderno
	INNER JOIN [AABMobile].[dbo].[VehicleBrand] vb ON vb.BrandCode = osm.BrandCode
	INNER JOIN [AABMobile].[dbo].[VehicleModel] vm ON vm.ModelCode = osm.ModelCode
	INNER JOIN [AABMobile].[dbo].[Dtl_Ins_Factor] u ON u.insurance_code = osm.UsageCode 
	INNER JOIN [AABMobile].[dbo].[Region] r ON r.RegionCode = osm.CityCode
	INNER JOIN [AABMobile].[dbo].[SalesOfficer] so ON so.SalesOfficerID =os.SalesOfficerID
	INNER JOIN ProspectCustomer pc ON pc.CustID = os.CustID
	LEFT JOIN dbo.Mst_Product mp ON mp.Product_Code = os.ProductCode
	LEFT JOIN dbo.ProspectCompany pcy ON pcy.CustId=pc.CustID
	LEFT JOIN [AABMobile].[dbo].[OrderSimulationSurvey] oss ON oss.OrderNo = os.OrderNo
	LEFT JOIN [AABMobile].[dbo].[AABBranch] ab ON ab.BranchCode = os.BranchCode
	LEFT JOIN [BeyondReport].[AAB].[dbo].mst_branch AS a WITH ( NOLOCK ) ON a.Branch_Id = os.BranchCode
	LEFT JOIN [BeyondReport].[AAB].[dbo].branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun 
	LEFT JOIN [BeyondReport].[a2isAuthorizationDB].[General].[ExternalUsers] geu WITH (NOLOCK) ON geu.UserId=so.Email
    where os.orderno = @0

     SELECT TOP 1 RptPrintQuotationID FROM @@TEMPID;


";
                string RptPrintQuotationID = db.ExecuteScalar<string>(query, orderNo, AdminFee, rs.PADRVRSI, rs.PAPASSSI, rs.TPLSI, Discount);
                query = @"INSERT INTO RptPrintQuotationDetail ([RptPrintQuotationID]
      ,[Year]
      ,[BasicCoverName]
      ,[TSICasco]
      ,[TSIAccess]
      ,[TSITotal]
      ,[Rate]
      ,[Loading]
      ,[BasicPremi]
      ,[SRCCPremi]
      ,[TRSPremi]
      ,[TPLPremi]
      ,[PADRVRPremi]
      ,[PAPASSPremi]
      ,[ExtendedPremi]
      ,[TotalPremi]
      ,[NettPremiAmount]
      ,[IsBundling]
      ,[IsBundlingTRS]
      ,[DiscountAmount])
VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,COALESCE(@10,0),@11,@12,@13,@14,@15,@16,@17,@18,@19)";
                foreach (DetailCoverage item in rs.dtlCoverage)
                {
                    db.Execute(query, RptPrintQuotationID, item.Year, item.BasiCoverName, item.VehiclePremi, item.AccessPremi, item.VehicleAccessPremi, item.Rate / 100, item.LoadingPremi / 100, item.BasicPremi, item.SFEPremi, item.TSPremi, item.TPLCoverage, item.PADRVRCoverage, item.PAPASSCoverage, item.PremiPerluasan, item.PremiDasarPerluasan, item.TotalPremi, item.IsBundling, item.IsBundlingTRS, item.PremiDasarPerluasan * Convert.ToDouble(Discount) / 100);
                }

                #endregion
            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }
        }

        public static List<CalculatedPremi> LoadCalculatedpremibyOrderNo(string orderNo,string additionalquery)
        {
            try
            {
                
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string query = @"select a.InterestID, b.CoverageID, a.Year, b.Rate, b.Premium,
           b.SumInsured, b.Loading, b.LoadingRate, b.IsBundling,b.BeginDate AS PeriodFrom,b.EndDate AS PeriodTo,b.Calc_Method AS CalcMethod,b.Deductible_Code AS DeductibleCode,b.Ndays,b.Entry_Pct AS EntryPrecentage,b.Excess_Rate AS ExcessRate,b.Cover_Premium AS CoverPremium,b.Net1,b.Net2,b.Net3,b.Gross_Premium AS GrossPremium 
           From orderSimulationinterest a inner join orderSimulationcoverage b on 
           a.orderno=b.orderno and a.interestno=b.interestno and b.RowStatus=1 
           where a.RowStatus=1 AND a.orderno=@0";
                query = string.Concat(query,additionalquery);
                List<CalculatedPremi> result = db.Fetch<CalculatedPremi>(query,orderNo.Trim());
                return result; 
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static DownloadQuotationResult GetByteReport(string orderNo)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportServerURLBeyond = ConfigurationManager.AppSettings["ReportServerURLBeyond"].ToString();
            string ReportPathOne = ConfigurationManager.AppSettings["ReportPathOne"].ToString();
            string ReportPathTinsOne = ConfigurationManager.AppSettings["ReportPathTinsOne"].ToString();
            string ReportPathLexusOne = ConfigurationManager.AppSettings["ReportPathLexusOne"].ToString();
            string ReportPathTwo = ConfigurationManager.AppSettings["ReportPathTwo"].ToString();
            string ReportPathTinsTwo = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusTwo = ConfigurationManager.AppSettings["ReportPathLexusTwo"].ToString();
            string ReportPathThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathTinsThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusThree = ConfigurationManager.AppSettings["ReportPathLexusThree"].ToString();
            string CertificateQuotation = ConfigurationManager.AppSettings["CertificateQuotation"].ToString();
            string CertificateRenewalNotice = ConfigurationManager.AppSettings["CertificateRenewalNotice"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;
            string CredentialsRenewalUsername = ConfigurationManager.AppSettings["CredentialsRenewalUsername"].ToString(); ;
            string CredentialsRenewalPassword = ConfigurationManager.AppSettings["CredentialsRenewalPassword"].ToString(); ;
            string CredentialsRenewalDomain = ConfigurationManager.AppSettings["CredentialsRenewalDomain"].ToString(); ;
            
            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";
            int idReportRennot = 0;

            #region Insert Data to RPT_Renewal_Notice Dan RPT_Renewal_Notice_Detail
            string qReport = @"SELECT OldPolicyNo,SalesOfficerID FROM OrderSimulation WHERE OrderNo = @0 ANd RowStatus = 1";
            dynamic policyNo = "";
            using (a2isDBHelper.Database dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                policyNo = dbMobile.FirstOrDefault<dynamic>(qReport, orderNo);
            }
            if (!String.IsNullOrEmpty(Convert.ToString(policyNo.OldPolicyNo)))
            {
                qReport = @";EXECUTE [dbo].[usp_InsertPrintRenewalNoticeOtosales] @0, @1";
                using (a2isDBHelper.Database dbAAB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    idReportRennot = dbAAB.FirstOrDefault<int>(qReport, Convert.ToString(policyNo.OldPolicyNo), Convert.ToString(policyNo.SalesOfficerID));
                    MobileRepository.InsertPrintRennotExtended(idReportRennot,orderNo);
                }
            }
            else
            {

                MobileRepository.InsertQuotationPrintTable(orderNo);
            }
            #endregion
            DownloadQuotationResult result = new DownloadQuotationResult();
            NetworkCredential NetCredential = new NetworkCredential();
            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                int yearCoverage;
                int insuranceType;

                string query = @"";

                query = @"SELECT o.PolicyNo, p.Name, o.InsuranceType, o.YearCoverage, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f 
                        ON f.CustID = p.CustID
                        INNER JOIN dbo.OrderSimulation o
                        ON o.FollowUpNo = f.FollowUpNo
                        INNER JOIN dbo.OrderSimulationMV omv
                        ON omv.OrderNo = o.OrderNo
                        WHERE o.OrderNo = @0";
                List<dynamic> dt = db.Fetch<dynamic>(query, orderNo);
                if (dt.Count > 0)
                {
                    yearCoverage = Convert.ToInt32(dt.First().YearCoverage);
                    insuranceType = Convert.ToInt32(dt.First().InsuranceType);
                    using (ReportViewer rView = new ReportViewer())
                    {
                        rView.ProcessingMode = ProcessingMode.Remote;

                        ServerReport serverReport = rView.ServerReport;
                        if (!dt.First().IsRenewal)
                        {

                            serverReport.ReportServerUrl = new Uri(ReportServerURL);
                            serverReport.ReportPath = CertificateQuotation;
                            NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);

                        }
                        else
                        {
                            serverReport.ReportServerUrl = new Uri(ReportServerURLBeyond);
                            serverReport.ReportPath = CertificateRenewalNotice;                 
                            NetCredential = new NetworkCredential(CredentialsRenewalUsername, CredentialsRenewalPassword, CredentialsRenewalDomain);

            
                        }

                        serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;
                        #region Catat Print Quotation
                        query = @"INSERT INTO dbo.PrintQuotationHistoryLog
                                        ( OrderNo, RowStatus, EntryDate )
                                VALUES  ( @0, -- OrderNo - varchar(100)
                                          1, -- RowStatus - smallint
                                          GETDATE()  -- EntryDate - datetime
                                          )";
                        db.Execute(query, orderNo);
                        #endregion
                       
                        ReportParameter[] parameters = new ReportParameter[1];
                        ReportParameter reportParam = new ReportParameter();
                        if (!dt.First().IsRenewal)
                        {
                            reportParam.Name = "OrderNo";
                            reportParam.Values.Add(orderNo);
                        }
                        else
                        {

                            reportParam.Name = "PolicyNo";
                            reportParam.Values.Add(Convert.ToString(policyNo.OldPolicyNo));
                        }
                        parameters[0] = reportParam;
                        rView.ServerReport.SetParameters(parameters);
                        bytes = rView.ServerReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                        result.Bytes = bytes;

                        #region filename
                        int cetakanke = db.ExecuteScalar<int>("SELECT COUNT(*) FROM dbo.PrintQuotationHistoryLog WHERE OrderNo=@0", orderNo) + 1;

                        string fileName = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='ATTFILENAME' AND ParamType='TMP' AND ParamValue2=@0", dt.First().IsRenewal);
                        fileName = fileName.Replace("@CetakanKe", cetakanke.ToString()).Replace("@TanggalCetak", DateTime.Now.ToString("dd-MM-yyyy"));
                        if (dt.First().IsRenewal)
                        {
                            fileName = fileName.Replace("@NoPolis", dt.First().PolicyNo.Trim());
                       
                        }
                        #endregion
                   
                        result.FileName = fileName;
//                        query = @"INSERT INTO dbo.PrintQuotationHistoryLog
//                                                ( OrderNo, RowStatus, EntryDate )
//                                        VALUES  ( @0, -- OrderNo - varchar(100)
//                                                  1, -- RowStatus - smallint
//                                                  GETDATE()  -- EntryDate - datetime
//                                                  )";
//                        db.Execute(query, orderNo);
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();
                throw ex;
            }
        }

        internal static void InsertPrintRennotExtended(int idReportRennot, string orderNo)
        {
            try
            {

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<CalculatedPremi> cpremiitems = LoadCalculatedpremibyOrderNo(orderNo, " AND a.InterestId IN('PADRVR','PAPASS','TPLPER')");
                OrderSimulation os = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE OrderNo=@0", orderNo);
                OrderSimulationMV osmv = db.FirstOrDefault<OrderSimulationMV>("select * from ordersimulationMV WHERE orderno=@0", orderNo);
                #region detail scoring
                List<DetailScoring> dtlScoring = new List<DetailScoring>();
                DetailScoring ds = new DetailScoring();
                if (!string.IsNullOrEmpty(osmv.CityCode))
                {
                    ds.FactorCode = "GEOGRA";
                    ds.InsuranceCode = osmv.CityCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.UsageCode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "MVUSAG";
                    ds.InsuranceCode = osmv.UsageCode;
                    dtlScoring.Add(ds);
                }
                ds = new DetailScoring();
                ds.FactorCode = "VHCTYP";
                ds.InsuranceCode = string.IsNullOrEmpty(osmv.Type) ? "ALL   " : osmv.Type;
                dtlScoring.Add(ds);
                if (!string.IsNullOrEmpty(osmv.BrandCode))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "VBRAND";
                    ds.InsuranceCode = osmv.BrandCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.IsNew.ToString()))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "NEWVHC";
                    ds.InsuranceCode = osmv.IsNew.ToString();
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.ModelCode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "VMODEL";
                    ds.InsuranceCode = osmv.ModelCode;
                    dtlScoring.Add(ds);
                }
                #endregion

                #region get extended detail cover
                int year = 0;
                foreach (CalculatedPremi item in cpremiitems)
                {

                    DateTime pfcover = item.PeriodFrom;
                    DateTime ptcover = item.PeriodFrom;
                    CalculateAndInsertRennot(idReportRennot, pfcover, ptcover, item, os, osmv, dtlScoring);

                }
            }
                #endregion

            catch (Exception e)
            {
                throw (e);
            }
        }

        private static void CalculateAndInsertRennot(int idReportRennot,DateTime pfcover,DateTime ptcover,CalculatedPremi item, OrderSimulation os, OrderSimulationMV osmv, List<DetailScoring> dtlScoring)
        {
            try
            {

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                int Ndays = db.ExecuteScalar<int>("SELECT CASE WHEN day_calculation_method =360 THEN 360 ELSE 366 END [NDays] from Mst_Product WHERE Product_Code=@0",os.ProductCode);
                int year = 0;
                string query = @"INSERT INTO [dbo].[RptPrintRenewalNoticeDetail]
           ([RptPrintRenewalNoticeID]
           ,[InterestID]
           ,[InterestDescription]
           ,[InterestType]
           ,[CoverageID]
           ,[CoverageDescription]
           ,[CoverageType]
           ,[TSI]
           ,[Rate]
           ,[Premium]
           ,[OwnRisk]
           ,[Loading]
           ,[IsBundling]
           ,[Year])
     VALUES
           (@0
           ,@1
           ,@2
           ,'L'
           ,@3
           ,@4
           ,'EXD'
           ,@5
           ,@6
           ,@7
           ,@8
           ,@9
           ,@10
           ,@11)";
                while (pfcover < item.PeriodTo)
                {

                    year++;
                    ptcover = item.PeriodFrom.AddYears(year);
                    CoverageParam cp = new CoverageParam();
                    cp.CoverageId = item.CoverageID;
                    cp.PeriodFromCover = pfcover;
                    cp.PeriodToCover = (ptcover.Year==item.PeriodTo.Year)?item.PeriodTo:ptcover;
                    int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                    double premi = RecalculateExtendedCover(item.InterestID, cp, os.PeriodFrom, os.PeriodTo, os.ProductCode, dtlScoring, osmv.Year, osmv.UsageCode, Ndays
                                                                       , osmv.Type, osmv.Sitting.ToString(), Convert.ToDecimal(item.SumInsured), os.OldPolicyNo, calcmethod,true);
                    int yearcoverage=0;
                    DateTime PeriodFrom = os.PeriodFrom??DateTime.Now;
                    DateTime PeriodTo = os.PeriodTo??DateTime.Now;
                    yearcoverage= (PeriodTo.Year-PeriodFrom.Year)-(PeriodTo.Year-pfcover.Year)+1;
                    pfcover = item.PeriodFrom.AddYears(year);
                    string InterestDesc = db.ExecuteScalar<string>(@"SELECT TOP 1 DESCRIPTION FROM mst_interest WHERE interest_id=@0",item.InterestID);
                    string CoverageDesc = db.ExecuteScalar<string>(@"SELECT TOP 1 name FROM mst_coverage WHERE coverage_id=@0",item.CoverageID);
                    string DeductibleDesc = db.ExecuteScalar<string>(@"SELECT TOP 1 DESCRIPTION FROM prd_deductible WHERE product_code=@0 AND deductible_code=@1",os.ProductCode,item.DeductibleCode);
                 
                    db.Execute(query, idReportRennot, item.InterestID,InterestDesc,item.CoverageID,CoverageDesc,item.SumInsured,item.Rate,premi,DeductibleDesc,item.Loading,item.IsBundling,yearcoverage);
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static List<PolicySentTo> GetPolicySentTo()
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            List<PolicySentTo> list = new List<PolicySentTo>();
            try
            {
                query = @"SELECT PolicySentToId AS ID, PolicySentToDes AS Description
                        FROM dbo.Mst_PolicySentTo 
                        WHERE RowStatus = 1
                        ORDER BY PolicySentToId ASC";
                list = db.Query<PolicySentTo>(query).ToList();
                return list;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static CopyQuotationModel getAddCopyQuotation(string state, string orderNo, string followUpNo) {
            string query = "";
            TaskListDetail result = new TaskListDetail();
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    ProspectCustomer prospectCustomer = new ProspectCustomer();
                    FollowUp followUp = new FollowUp();
                    OrderSimulationModel orderSimulation = new OrderSimulationModel();
                    OrderSimulationMVModel orderSimulationMV = new OrderSimulationMVModel();
                    List<OrderSimulationInterest> orderSimulationInterest = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> orderSimulationCoverage = new List<OrderSimulationCoverage>();
                    SurveySchedule orderSimulationSurvey = null;

                    if (state.Equals("ADD"))
                    {
                        OrderSimulationModel oldOS = new OrderSimulationModel();
                        query = "SELECT * FROM OrderSimulation WHERE orderNo=@0";
                        OrderSimulationModel nowOS = db.FirstOrDefault<OrderSimulationModel>(query, orderNo);
                        if (nowOS != null)
                        {
                            oldOS = nowOS;
                        }
                        else
                        {
                            query = "SELECT * FROM OrderSimulation WHERE RowStatus=0 AND FollowUpNo=@0 AND ApplyF=1";
                            oldOS = db.FirstOrDefault<OrderSimulationModel>(query, followUpNo);
                        }
                        query = "UPDATE OrderSimulation SET ApplyF='false' WHERE OrderNo=@0";
                        db.Execute(query, oldOS.OrderNo);
                        //query = "SELECT * FROM ProspectCustomer WHERE CustId=@0";
                        //prospectCustomer = db.FirstOrDefault<ProspectCustomer>(query, oldOS.CustID);
                        //query = "SELECT * FROM FollowUp WHERE CustId=@0";
                        //followUp = db.FirstOrDefault<FollowUp>(query, oldOS.CustID);

                        //prospectCustomer.SalesOfficerID = prospectCustomer.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", prospectCustomer.SalesOfficerID) : prospectCustomer.SalesOfficerID;
                        //followUp.SalesOfficerID = followUp.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", followUp.SalesOfficerID) : followUp.SalesOfficerID;
                        //orderSimulation.SalesOfficerID = orderSimulation.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", orderSimulation.SalesOfficerID) : orderSimulation.SalesOfficerID;

                        result = GetTaksListDetail(nowOS.CustID, followUpNo);
                    }
                    else if (state.Equals("COPY"))
                    {

                        query = "SELECT * FROM OrderSimulation WHERE OrderNo=@0";
                        orderSimulation = db.FirstOrDefault<OrderSimulationModel>(query, orderNo);

                        query = "UPDATE OrderSimulation SET ApplyF='false' WHERE OrderNo=@0";
                        db.Execute(query, orderSimulation.OrderNo);


                        query = @"SELECT DISTINCT a.* , c.Description + ' ' + d.Description + ' ' + e.Series + ' ' + b.Year 'searchVehicle' FROM OrderSimulationMV a  
                                    Inner Join OrderSimulation os ON os.OrderNo=a.OrderNo
inner join Vehicle b on a.VehicleCode=b.VehicleCode and a.BrandCode=b.BrandCode and a.ProductTypeCode=b.ProductTypeCode
and a.ModelCode=b.ModelCode and a.Series=b.Series and a.Type=b.Type and a.Sitting=b.Sitting and a.Year=b.Year
inner join VehicleBrand c on a.BrandCode=c.BrandCode
inner join VehicleModel d on a.BrandCode=d.BrandCode and a.ModelCode=d.ModelCode
inner join VehicleSeries e on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series WHERE os.OrderNo=@0";
                        orderSimulationMV = db.FirstOrDefault<OrderSimulationMVModel>(query, orderNo);

                        query = @"SELECT * FROM OrderSimulationInterest WHERE RowStatus=0 AND OrderNo=@0
                ";
                        orderSimulationInterest = db.Fetch<OrderSimulationInterest>(query, orderNo);



                        query = @"SELECT * FROM OrderSimulationCoverage WHERE RowStatus=0 AND OrderNo=@0
                ";
                        orderSimulationCoverage = db.Fetch<OrderSimulationCoverage>(query, orderNo);


                        query = "SELECT * FROM ProspectCustomer WHERE CustId=@0";
                        prospectCustomer = db.FirstOrDefault<ProspectCustomer>(query, orderSimulation.CustID);

                        query = "SELECT * FROM FollowUp WHERE CustId=@0";
                        followUp = db.FirstOrDefault<FollowUp>(query, orderSimulation.CustID);

                        query = "SELECT CityCode, LocationCode, SurveyAddress,SurveyPostalCode, SurveyDate, ScheduleTimeID FROM dbo.OrderSimulationSurvey WHERE OrderNo = @0";
                        orderSimulationSurvey = db.FirstOrDefault<SurveySchedule>(query, orderNo);

                        OrderSimulationModel newOSModel = orderSimulation;
                        OrderSimulationMVModel newOSMVModel = orderSimulationMV;
                        List<OrderSimulationInterest> newOSIModel = orderSimulationInterest;
                        List<OrderSimulationCoverage> newOSCModel = orderSimulationCoverage;

                        prospectCustomer.SalesOfficerID = prospectCustomer.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", prospectCustomer.SalesOfficerID) : prospectCustomer.SalesOfficerID;
                        followUp.SalesOfficerID = followUp.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", followUp.SalesOfficerID) : followUp.SalesOfficerID;

                        newOSModel.OrderNo = System.Guid.NewGuid().ToString();
                        if (!string.IsNullOrEmpty(newOSModel.CustID))
                        {

                            newOSModel.CustID = prospectCustomer.CustID;
                            newOSModel.QuotationNo = null;
                            newOSModel.LastUpdatedTime = null;
                            newOSModel.ApplyF = true;
                            newOSModel.SendF = true;
                            newOSModel.SendStatus = 0;
                            newOSModel.SendDate = null;
                            newOSModel.SalesOfficerID = newOSModel.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", newOSModel.SalesOfficerID) : newOSModel.SalesOfficerID;

                            query = @";INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
      ,[PolicyOrderNo]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[VANumber]
      ,[SurveyNo]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[PolicyNo]
      ,[AmountRep]    
      ,[PolicyID]    
      ,[SegmentCode]    
    ) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25
,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36,@37,@38)";
                            db.Execute(query, newOSModel.OrderNo
          , newOSModel.CustID
          , newOSModel.FollowUpNo
          , newOSModel.QuotationNo
          , newOSModel.MultiYearF
          , newOSModel.YearCoverage
          , newOSModel.TLOPeriod
          , newOSModel.ComprePeriod
          , newOSModel.BranchCode
          , newOSModel.SalesOfficerID
          , newOSModel.PhoneSales
          , newOSModel.DealerCode
          , newOSModel.SalesDealer
          , newOSModel.ProductTypeCode
          , newOSModel.AdminFee
          , newOSModel.TotalPremium
          , newOSModel.Phone1
          , newOSModel.Phone2
          , newOSModel.Email1
          , newOSModel.Email2
          , newOSModel.SendStatus
          , newOSModel.InsuranceType
          , newOSModel.ApplyF
          , newOSModel.SendF
          , newOSModel.LastInterestNo
          , newOSModel.LastCoverageNo
          , newOSModel.PolicySentTo
          , newOSModel.PolicyDeliveryName
          , newOSModel.PolicyOrderNo
          , newOSModel.PolicyDeliveryAddress
          , newOSModel.PolicyDeliveryPostalCode
          , newOSModel.VANumber
          , newOSModel.SurveyNo
          , newOSModel.PeriodFrom
          , newOSModel.PeriodTo
          , newOSModel.PolicyNo
          , newOSModel.AmountRep
          , newOSModel.PolicyID
          , newOSModel.SegmentCode);


                            //Copy OSMV
                            newOSMVModel.OrderNo = newOSModel.OrderNo;
                            newOSMVModel.LastUpdatedTime = null;
                            newOSMVModel.FieldChanged = "";
                            query = @" INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)";
                            db.Execute(query, newOSMVModel.OrderNo
      , newOSMVModel.ObjectNo
      , newOSMVModel.ProductTypeCode
      , newOSMVModel.VehicleCode
      , newOSMVModel.BrandCode
      , newOSMVModel.ModelCode
      , newOSMVModel.Series
      , newOSMVModel.Type
      , newOSMVModel.Sitting
      , newOSMVModel.Year
      , newOSMVModel.CityCode
      , newOSMVModel.UsageCode
      , newOSMVModel.SumInsured
      , newOSMVModel.AccessSI
      , newOSMVModel.RegistrationNumber
      , newOSMVModel.EngineNumber
      , newOSMVModel.ChasisNumber
      , newOSMVModel.LastUpdatedTime
      , newOSMVModel.RowStatus
      , newOSMVModel.IsNew);

                            //Copy OrderSimulationInterest
                            foreach (OrderSimulationInterest osimodel in newOSIModel)
                            {
                                osimodel.OrderNo = newOSModel.OrderNo;
                                query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                                db.Execute(query, osimodel.OrderNo, osimodel.ObjectNo, osimodel.InterestNo, osimodel.InterestID, osimodel.Year, osimodel.Premium, osimodel.PeriodFrom, osimodel.PeriodTo);

                            }
                            foreach (OrderSimulationCoverage oscmodel in newOSCModel)
                            {

                                oscmodel.OrderNo = newOSModel.OrderNo;
                                query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12)";
                                db.Execute(query, oscmodel.OrderNo, oscmodel.ObjectNo, oscmodel.InterestNo, oscmodel.CoverageNo, oscmodel.CoverageID, oscmodel.Rate, oscmodel.SumInsured, oscmodel.LoadingRate, oscmodel.Loading, oscmodel.Premium, oscmodel.BeginDate, oscmodel.EndDate, oscmodel.IsBundling ? 1 : 0);

                            }
                            query = @"INSERT INTO dbo.OrderSimulationSurvey
                                            ( OrderNo ,
                                              CityCode ,
                                              LocationCode ,
                                              SurveyAddress ,
                                              SurveyPostalCode ,
                                              SurveyDate ,
                                              ScheduleTimeID ,
                                              RowStatus ,
                                              CreatedBy ,
                                              CreatedDate
                                            )
                                    VALUES  ( @0 , -- OrderNo - varchar(100)
                                              @1 , -- CityCode - int
                                              @2 , -- LocationCode - int
                                              @3 , -- SurveyAddress - varchar(500)
                                              @4 , -- SurveyPostalCode - varchar(5)
                                              @5 , -- SurveyDate - datetime
                                              @6 , -- ScheduleTimeID - datetime
                                              1 , -- RowStatus - bit
                                              'OtosalesAPI' , -- CreatedBy - varchar(50)
                                              GETDATE()
                                            )";
                            if (orderSimulationSurvey!=null) {
                                db.Execute(query
                                    , newOSModel.OrderNo
                                    , orderSimulationSurvey.CityCode
                                    , orderSimulationSurvey.LocationCode
                                    , orderSimulationSurvey.SurveyAddress
                                    , orderSimulationSurvey.SurveyPostalCode
                                    , orderSimulationSurvey.SurveyDate
                                    , orderSimulationSurvey.ScheduleTimeID);                            
                            }
                        }
                       // result = GetTaksListDetail(newOSModel.CustID, followUpNo);
                    }
                    CopyQuotationModel cp=  new CopyQuotationModel { orderSimulation=orderSimulation,orderSimulationMV=orderSimulationMV,orderSimulationInterest=orderSimulationInterest,orderSimulationCoverage=orderSimulationCoverage };
                    return cp;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static TaskListDetail getEditQuotation(string custid,string followupno,string orderNo) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string query = @"
                  select 
                    a.CustID, a.OrderNo, a.QuotationNo,  
                    a.EntryDate, a.TotalPremium, 
                    c.Description + ' ' + d.Description + ' ' + e.Series Vehicle, 
                    b.Year VehicleYear,  b.SumInsured, b.AccessSI, 
                    a.ApplyF, a.SendF, a.SendStatus, 
                    case 
	                    when a.ComprePeriod > 0 and a.TLOPeriod = 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod = 0 and a.TLOPeriod > 0 
		                    then 'TLO ' + CAST(a.TLOPeriod AS VARCHAR)+ 'Th' 
	                    when a.ComprePeriod > 0 and a.TLOPeriod > 0 
		                    then 'COMPREHENSIVE ' + CAST(a.ComprePeriod AS VARCHAR)
			                    + 'Th TLO ' + CAST( a.TLOPeriod AS VARCHAR)+ 'Th' 
                    end AS 'CoverageType' 
                    from OrderSimulation a 
                    inner join OrderSimulationMV b on a.OrderNo = b.OrderNo 
                    inner join VehicleBrand c on b.BrandCode = c.BrandCode 
                    AND b.ProductTypeCode = c.ProductTypeCode 
                    inner join VehicleModel d on d.BrandCode = c.BrandCode 
                    AND d.ProductTypeCode = c.ProductTypeCode AND d.ModelCode = b.ModelCode 
                    inner join VehicleSeries e on d.BrandCode = e.BrandCode 
                    AND d.ProductTypeCode = e.ProductTypeCode AND d.ModelCode = e.ModelCode 
                    AND b.Series = e.Series 
                    where a.FollowUpNo=@0
                    ORDER BY a.EntryDate DESC
                ";
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var quotationhistory = db.Fetch<dynamic>(query, followupno);
                    foreach (dynamic item in quotationhistory)
                    {
                        bool ApplyF = false;
                        if (item.OrderNo.Equals(orderNo))
                        {
                            ApplyF = true;
                        }
                        db.Execute("UPDATE OrderSimulation SET ApplyF=@1 WHERE OrderNo=@0", item.OrderNo, ApplyF);
                    }

                    #region ORDERSIMULATION
                    //                    query = @"
                    //					SELECT * FROM OrderSimulation WHERE OrderNo=@0
                    //                ";
                    //                    OrderSimulation OrderSimulation = db.FirstOrDefault<OrderSimulation>(query, orderNo);
                    #endregion


                    #region ORDERSIMULATIONMV
                    //                    query = @"SELECT b.* FROM OrderSimulation a INNER JOIN OrderSimulationMV b ON 
                    //                                a.OrderNo=b.OrderNo WHERE a.RowStatus =1 AND 
                    //                                b.RowStatus = 1 AND a.orderNo=@0";
                    //                    List<OrderSimulationMV> OrderSimulationMV = db.Fetch<OrderSimulationMV>(query, orderNo);
                    //                    foreach (OrderSimulationMV osmv in OrderSimulationMV)
                    //                    {
                    //                        if (OrderSimulation.ComprePeriod + OrderSimulation.TLOPeriod == 0)
                    //                        {
                    //                            osmv.ProductTypeCode = null;
                    //                            OrderSimulation.ProductCode = null;
                    //                        }
                    //                    }
                    #endregion


                    #region ORDERSIMULATION INTEREST
                    //                    query = @"SELECT * FROM OrderSimulationInterest WHERE RowStatus=1 AND OrderNo=@0
                    //                ";
                    //                    List<OrderSimulationInterest> OrderSimulationInterest = db.Fetch<OrderSimulationInterest>(query, orderNo);
                    #endregion


                    #region ORDERSIMULATION COVERAGE
                    //                    query = @"SELECT * FROM OrderSimulationCoverage WHERE RowStatus=1 AND OrderNo=@0
                    //                ";
                    //                    List<OrderSimulationCoverage> OrderSimulationCoverage = db.Fetch<OrderSimulationCoverage>(query, orderNo);
                    #endregion

                    TaskListDetail result = new TaskListDetail();
                    result = GetTaksListDetail(custid, followupno);
                    return result;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                
                throw e;
            }
        }

        public static List<ScheduleTimeList> GetScheduleTimeList(string BranchID, string SurveyDate, string PostalCodeID, string Language)
        {
            String ID = "";
            List<ScheduleTimeList> hasil = new List<ScheduleTimeList>();
            List<dynamic> all = new List<dynamic>();
            List<dynamic> booked = new List<dynamic>();
            string queryall = "";
            string querybooked = "";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_OTOCAREDB))
            {
                if ((PostalCodeID == "") || (PostalCodeID == null))
                {
                    //                    string query = @"select ScheduleTimeID,scheduletimeText" + Language + @" as SurveyTime,0 as IsBooked
                    //                            from scheduletime a
                    //                            where category =(select top 1 Category from gardamobilecms.dbo.nearme where nearmeid='" + BranchID + @"')
                    //                            and not exists(select ID from BookOtocare b
                    //                            inner join ClaimReport c on b.LKNumber=c.ReportNo
                    //                            where BookDate=CAST('" + SurveyDate + @"' AS DATETIME) + CAST(a.ScheduleTime AS DATETIME)  and c.SurveyLocation='" + BranchID + @"' )";

                    // change based on branch
                    queryall = @"select * from scheduletime a 
                                    where category =(select case when (select [type] from Asuransiastra..Branch b 
                                    where ID = @0) = 5 then 1 else 0 end)";

                    //queryall = @"select *
                    //            from scheduletime a
                    //            where category =(select top 1 Category from gardamobilecms.dbo.nearme where nearmeid=@0 )";

                    querybooked = @"select * from BookOtocare b
                            inner join ClaimReport c on b.LKNumber=c.ReportNo
                            where c.SurveyLocation=@0 ";
                    ID = BranchID;

                    all = db.Query<dynamic>(queryall, ID).ToList();
                    booked = db.Query<dynamic>(querybooked, ID).ToList();
                    for (int i = 0; i < all.Count; i++)
                    {
                        ScheduleTimeList stl = new ScheduleTimeList();
                        stl.ScheduleTimeID = all[i].ScheduleTimeID;
                        if (Language.ToUpper() == "ID")
                        {
                            stl.SurveyTime = all[i].ScheduleTimeTextID;
                        }
                        else
                        {
                            stl.SurveyTime = all[i].ScheduleTimeTextEN;
                        }

                        stl.IsBooked = 0;
                        String datetime = SurveyDate + " " + all[i].ScheduleTime;
                        queryall = datetime;
                        //DateTime dt = Convert.ToDateTime(datetime);
                        for (int a = 0; a < booked.Count; a++)
                        {
                            if (!string.IsNullOrEmpty(booked[a].BookDate.ToString("dd/MM/yyyy HH:mm")))
                            {
                                if (datetime.Equals(booked[a].BookDate.ToString("dd/MM/yyyy HH:mm")))
                                {
                                    stl.IsBooked = 1;
                                }
                            }

                        }
                        hasil.Add(stl);
                    }
                }
                // if survey luar
                else
                {
                    DateTime dt = Convert.ToDateTime(SurveyDate);
                    string svDt = dt.ToString("yyyy-MM-dd");
                    queryall = @"
                                    SELECT  
                                            Frequency ,
                                            CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
                                                 WHEN tc.TimeCategoryCode = 'TC2' THEN 40
                                                 WHEN tc.TimeCategoryCode = 'TC3' THEN 41
                                            END ScheduleTimeID ,
                                            ZipCode ,
                                            C.AreaCode SurveyareaCode ,
                                            CityId ,
                                            ISNULL(slotbooked, 0) slotbooked ,
                                            TC.TimeCategoryDescription ScheduleTimeTextID ,
                                            CASE WHEN tc.TimeCategoryCode = 'TC1'
                                                 THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
                                                 WHEN tc.TimeCategoryCode = 'TC2'
                                                 THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
                                                 WHEN tc.TimeCategoryCode = 'TC3'
                                                 THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
                                            END ScheduleTimeTextEN
                                    FROM    GardaAkses.GA.SurveyArea sa
                                            INNER JOIN ( SELECT DISTINCT
                                                                ZipCode ,
                                                                cityID ,
                                                                REPLACE(REPLACE(REPLACE(GeoAreaDescription,
                                                                                        GeoAreaCode, ''), ' ', ''),
                                                                        '-', '') AreaCode
                                                         FROM   AABmobile.dbo.surveyzipcode
                                                       ) c ON C.AreaCode = REPLACE(SA.SurveyareaCode, '-', '')
                                            INNER JOIN GardaAkses.GA.TimeCategory TC ON SA.TimeCategoryCode = TC.TimeCategoryCode
                                            LEFT JOIN ( SELECT  SurveyAreaCode ,
                                                                SurveyTimeCode ,
                                                                SurveyDate ,
                                                                COUNT(1) AS SlotBooked
                                                        FROM    GardaAkses.GA.BookingSurvey AS bs
                                                        WHERE   RowStatus = 0
                                                                AND CONVERT(VARCHAR(10), SURVEYDATE, 120) BETWEEN CONVERT(VARCHAR(10), GETDATE()
                                                                                                  + 1, 120)
                                                                                                  AND
                                                                                                  CONVERT(VARCHAR(10), GETDATE()
                                                                                                  + 6, 120)
                                                                AND CONVERT(VARCHAR(10), surveydate, 120) = @0
                                                                AND bs.RowStatus = 0
                                                        GROUP BY bs.surveydate ,
                                                                bs.SurveyAreaCode ,
                                                                SurveyTimeCode
                                                      ) a ON REPLACE(a.SurveyAreaCode, '-', '') = c.AreaCode
                                                             AND a.SurveyTimeCode = sa.TimeCategoryCode
                                    WHERE   
                                            -- CITYID = @0 AND
                                            ZipCode = @1
                                            AND frequency > ISNULL(slotbooked, 0)
                                            AND sa.RowStatus = 0
                                            AND tc.RowStatus = 0
                                    ORDER BY ScheduleTimeID";
                    ID = PostalCodeID;
                    all = db.Query<dynamic>(queryall, svDt, ID).ToList();

                    for (int i = 0; i < all.Count; i++)
                    {
                        ScheduleTimeList stl = new ScheduleTimeList();
                        stl.ScheduleTimeID = all[i].ScheduleTimeID;
                        if (Language.ToUpper() == "ID")
                        {
                            stl.SurveyTime = all[i].ScheduleTimeTextID;
                        }
                        else
                        {
                            stl.SurveyTime = all[i].ScheduleTimeTextEN;
                        }

                        stl.IsBooked = 1;
                        if (all[i].Frequency > all[i].slotbooked)
                        {
                            stl.IsBooked = 0;
                        }
                        hasil.Add(stl);
                    }
                }

            }
            return hasil;
        }

        public //static 
            List<dynamic> GetPolicyDeliveryName(string ID, string ParamSearch)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            List<dynamic> result = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                if (ID.ToUpper().Equals("CS") || ID.ToUpper().Equals("CH"))
                {
                    query = @";DECLARE @@IsCompany BIT
                            DECLARE @@Name VARCHAR(512)
                            DECLARE @@Address VARCHAR(512)
                            DECLARE @@PostalCode VARCHAR(10)
                            SELECT @@IsCompany=isCompany, @@Name=Name, @@Address=CustAddress, @@PostalCode=PostalCode 
                            FROM dbo.ProspectCustomer WHERE CustID = @0

                            IF(@@IsCompany=1)
                            BEGIN 
	                            SELECT @@Name=CompanyName, @@Address=NPWPaddress, @@PostalCode=PostalCode 
	                            FROM dbo.ProspectCompany WHERE CustID = @0
                            END

                            SELECT @@Name AS Name, @@Address AS Address, @@PostalCode AS PostalCode";
                    result = db.Fetch<dynamic>(query, ParamSearch);
                }
                else if (ID.ToUpper().Equals("BR"))
                {
                    query = @"SELECT so.Name AS Name, Address, PostCode AS PostalCode
                            FROM BEYONDMOSS.AABMobile.dbo.SalesOfficer so
                            INNER JOIN dbo.Mst_Branch mb
                            ON mb.Branch_id = REPLACE(so.BranchCode,'A','')
                            WHERE so.SalesOfficerID = @0";
                    result = AABdb.Fetch<dynamic>(query, ParamSearch);
                }
                else if (ID.ToUpper().Equals("GC"))
                {
                    query = @"SELECT Name, [Address], PostCode AS PostalCode 
                            FROM dbo.GardaCenter WHERE Name LIKE '%{0}%'";
                    result = AABdb.Fetch<dynamic>(string.Format(query, ParamSearch));
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        
        }

        public static List<string> GetApplicationParametersValue(string ParamName) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            List<string> result = new List<string>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                query = @"SELECT ParamValue, ParamValue2 FROM dbo.ApplicationParameters
                        WHERE RowStatus = 1 AND ParamName = @0";
                List<dynamic> listVal = db.Fetch<dynamic>(query, ParamName);
                if (listVal.Count > 0) {
                    result.Add(listVal.First().ParamValue);
                    result.Add(listVal.First().ParamValue2);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static List<dynamic> GetCheckNSA(string OrderNo) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            List<dynamic> result = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                query = @";EXEC dbo.usp_CheckNSAOtosales @0";
                result = db.Fetch<dynamic>(query,OrderNo);
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }        
        }

        public static TaskListDetail GetUpdateOrder(string OrderNo,string DeviceID,string SalesOfficerID)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            TaskListDetail result = new TaskListDetail();
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                query = @"SELECT PolicyOrderNo, ProspectID, FollowUpNo, CustIDAAB FROM dbo.OrderSimulation os
                        INNER JOIN dbo.ProspectCustomer pc
                        ON pc.CustID = os.CustID
                        WHERE OrderNo = @0";
                List<dynamic> OrderInfo = AABMobileDB.Fetch<dynamic>(query,OrderNo);
                string PolicyOrderNo = Convert.ToString(OrderInfo.First().PolicyOrderNo);
                string ProspectID = "";
                if (!string.IsNullOrEmpty(OrderInfo.First().CustIDAAB) && !string.IsNullOrWhiteSpace(OrderInfo.First().CustIDAAB))
                {
                    ProspectID = Convert.ToString(OrderInfo.First().CustIDAAB);
                }
                else {
                    ProspectID = Convert.ToString(OrderInfo.First().ProspectID);                
                }
                if (!string.IsNullOrEmpty(PolicyOrderNo))
                {
                    #region update order
//                    query = @"SELECT Object_No AS ObjectNo,Interest_No AS InterestNo,Interest_Code AS InterestID,
//DATEDIFF(YEAR,Period_From,Period_To) AS [YEAR], Period_From AS PeriodFrom,
//Period_To AS PeriodTo, Deductible_Code 
//FROM dbo.Ord_Dtl_Interest WHERE Order_no = @0";
//                    List<dynamic> newOSI = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
//                    if (newOSI.Count>0)
//                    {
//                        query = "DELETE FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
//                        AABMobileDB.Execute(query, OrderNo);

//                        query = "DELETE FROM dbo.OrderSimulationInterest WHERE OrderNo = @0";
//                        AABMobileDB.Execute(query, OrderNo);

//                        #region ordersimulationInterest
//                        foreach (dynamic d in newOSI)
//                        {
//                            query = @"SELECT odc.Object_No AS ObjectNo, odc.Interest_No AS InterestNo, Coverage_No AS CoverageNo, 
//                            Coverage_Id AS CoverageID, Rate, odc.Sum_Insured AS SumInsured, Gross_Premium AS Premium, 
//                            Loading AS LoadingRate, Entry_Pct, Ndays, Excess_Rate, odc.Calc_Method, Cover_Premium, 
//                            Gross_Premium, Max_si, Net1, Net2, Net3, odc.Deductible_Code, Begin_Date AS BeginDate, 
//                            End_Date AS EndDate, Interest_Code, Product_Code
//                            FROM dbo.Ord_Dtl_Coverage odc
//							INNER JOIN dbo.Ord_Dtl_Interest odi
//							ON odi.Order_no = odc.Order_No 
//                            AND odi.Interest_No = odc.Interest_No
//							INNER JOIN dbo.Mst_Order mo 
//							ON mo.Order_No = odc.Order_No 
//							WHERE odc.Order_no = @0 AND odc.Interest_No = @1";
//                            List<dynamic> newOSC = AABDB.Fetch<dynamic>(query, PolicyOrderNo, d.InterestNo);

//                            decimal Premium = AABDB.ExecuteScalar<decimal>(
//                                            @"SELECT SUM(Gross_Premium) FROM dbo.Ord_Dtl_Coverage
//                                            WHERE Order_No = @0
//                                            AND Interest_No = @1", PolicyOrderNo, d.InterestNo);
//                            query = @"INSERT INTO dbo.OrderSimulationInterest
//        ( OrderNo ,
//          ObjectNo ,
//          InterestNo ,
//          InterestID ,
//          Year ,
//          Premium ,
//          PeriodFrom ,
//          PeriodTo ,
//          LastUpdatedTime ,
//          RowStatus ,
//          Deductible_Code
//        )
//VALUES  ( @0 , -- OrderNo - varchar(100)
//          @1 , -- ObjectNo - numeric
//          @2 , -- InterestNo - numeric
//          @3 , -- InterestID - char(6)
//          @4 , -- Year - char(4)
//          @5 , -- Premium - numeric
//          @6 , -- PeriodFrom - datetime
//          @7 , -- PeriodTo - datetime
//          GETDATE() , -- LastUpdatedTime - datetime
//          1 , -- RowStatus - bit
//          @8  -- Deductible_Code - char(6)
//        )";
//                            AABMobileDB.Execute(query
//                                , OrderNo
//                                , d.ObjectNo
//                                , d.InterestNo
//                                , d.InterestID
//                                , d.YEAR
//                                , Premium
//                                , d.PeriodFrom
//                                , d.PeriodTo
//                                , d.Deductible_Code);
//                        #endregion

//                        #region ordersimulationcoverage
//                            query = @";INSERT INTO dbo.OrderSimulationCoverage
//                                        ( OrderNo ,
//                                          ObjectNo ,
//                                          InterestNo ,
//                                          CoverageNo ,
//                                          CoverageID ,
//                                          Rate ,
//                                          SumInsured ,
//                                          LoadingRate ,
//                                          Loading ,
//                                          Premium ,
//                                          BeginDate ,
//                                          EndDate ,
//                                          LastUpdatedTime ,
//                                          RowStatus ,
//                                          IsBundling ,
//                                          Entry_Pct ,
//                                          Ndays ,
//                                          Excess_Rate ,
//                                          Calc_Method ,
//                                          Cover_Premium ,
//                                          Gross_Premium ,
//                                          Max_si ,
//                                          Net1 ,
//                                          Net2 ,
//                                          Net3 ,
//                                          Deductible_Code
//                                        )
//                                VALUES  ( @0 , -- OrderNo - varchar(100)
//                                          @1 , -- ObjectNo - numeric
//                                          @2 , -- InterestNo - numeric
//                                          @3 , -- CoverageNo - numeric
//                                          @4 , -- CoverageID - char(6)
//                                          @5 , -- Rate - numeric
//                                          @6 , -- SumInsured - numeric
//                                          @7 , -- LoadingRate - numeric
//                                          @8 , -- Loading - numeric
//                                          @9 , -- Premium - numeric
//                                          @10 , -- BeginDate - datetime
//                                          @11 , -- EndDate - datetime
//                                          GETDATE() , -- LastUpdatedTime - datetime
//                                          1 , -- RowStatus - bit
//                                          @12 , -- IsBundling - bit
//                                          @13 , -- Entry_Pct - numeric
//                                          @14 , -- Ndays - numeric
//                                          @15 , -- Excess_Rate - numeric
//                                          @16 , -- Calc_Method - varchar(2)
//                                          @17 , -- Cover_Premium - numeric
//                                          @18 , -- Gross_Premium - numeric
//                                          @19 , -- Max_si - numeric
//                                          @20 , -- Net1 - numeric
//                                          @21 , -- Net2 - numeric
//                                          @22 , -- Net3 - numeric
//                                          @23  -- Deductible_Code - char(6)
//                                        )";
//                            foreach (dynamic osc in newOSC)
//                            {
//                                string squery = "";
//                                decimal loading = (Convert.ToDecimal(osc.LoadingRate) / 100) * Convert.ToDecimal(osc.Premium);
//                                int IsBundling = 0;
//                                string interestCode = Convert.ToString(osc.Interest_Code);
//                                if (interestCode.Contains("CASCO"))
//                                {
//                                    squery = @"SELECT a.Coverage_Id, a.Scoring_Code, b.Auto_Apply, 1 Year  FROM 
//                                         Prd_Interest_Coverage a INNER JOIN B2B_Interest_Coverage b ON 
//                                         a.Coverage_Id=b.Coverage_Id AND a.Interest_ID=b.Interest_Id AND 
//                                         a.Product_Code=b.Product_Code WHERE a.Product_Code=@0
//										 AND a.Coverage_Id = @1 
//										 AND b.Vehicle_Type='ALL' 
//										 AND b.Interest_Id='CASCO'";
//                                    List<dynamic> ListTmp = AABMobileDB.Fetch<dynamic>(squery, osc.Product_Code, osc.CoverageID);
//                                    if (ListTmp.Count > 0)
//                                    {
//                                        int AutoApply = Convert.ToInt32(ListTmp.First().Auto_Apply);
//                                        if (AutoApply > 0)
//                                        {
//                                            IsBundling = 1;
//                                        }
//                                    }
//                                }

//                                AABMobileDB.Execute(query
//                                    , OrderNo
//                                    , osc.ObjectNo
//                                    , osc.InterestNo
//                                    , osc.CoverageNo
//                                    , osc.CoverageID
//                                    , osc.Rate
//                                    , osc.SumInsured
//                                    , osc.LoadingRate
//                                    , loading
//                                    , osc.Premium
//                                    , osc.BeginDate
//                                    , osc.EndDate
//                                    , IsBundling
//                                    , osc.Entry_Pct
//                                    , osc.Ndays
//                                    , osc.Excess_Rate
//                                    , osc.Calc_Method
//                                    , osc.Cover_Premium
//                                    , osc.Gross_Premium
//                                    , osc.Max_si
//                                    , osc.Net1
//                                    , osc.Net2
//                                    , osc.Net3
//                                    , osc.Deductible_Code);
//                            }
//                        }
//                            #endregion
//                    }

//                    #region OrdersimulationMV
//                    query = @"SELECT Vehicle_Code AS VehicleCode, Brand_Code AS BrandCode, 
//                            Model_Code AS ModelCode, Series, Vehicle_Type AS Type, 
//                            Sitting_Capacity AS Sitting, Mfg_Year AS [Year], 
//                            Geo_Area_Code AS CityCode, Usage_Code AS UsageCode, 
//                            Registration_Number AS RegistrationNumber, 
//                            Engine_Number AS EngineNumber, Chasis_Number AS ChasisNumber,
//                            New_Car AS IsNew, Color_On_BPKB AS ColorOnBPKB
//                            FROM dbo.Ord_Dtl_MV
//                            WHERE Order_No = @0";
//                    List<dynamic> newOSMV = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
//                    query = @"SELECT * FROM dbo.Region WHERE RegionCode = @0";
//                    string CityCode = "";
//                    List<dynamic> isExist = AABMobileDB.Fetch<dynamic>(query, newOSMV.First().CityCode);
//                    if (isExist.Count > 0)
//                    {
//                        CityCode = newOSMV.First().CityCode;
//                    }
//                    else
//                    {
//                        query = @"SELECT aab_code AS Code FROM dbo.partner_aab_map 
//                                WHERE partner_code =@0 AND type = 'GEO' AND partner_id = 'OJK'";
//                        List<dynamic> geoAreaCode = AABDB.Fetch<dynamic>(query, newOSMV.First().CityCode);
//                        CityCode = geoAreaCode.First().aab_code;
//                    }
//                    query = @"UPDATE dbo.OrderSimulationMV
//                            SET VehicleCode = @1, BrandCode = @2,
//                            ModelCode = @3, Series = @4, [Type] = @5,
//                            Sitting = @6, [Year] = @7, 
//                            CityCode = @8, UsageCode = @9,
//                            RegistrationNumber = @10, 
//                            EngineNumber = @11, ChasisNumber = @12,
//                            IsNew = @13, ColorOnBPKB = @14
//                            WHERE OrderNo = @0";
//                    AABMobileDB.Execute(query, OrderNo
//                        , newOSMV.First().VehicleCode, newOSMV.First().BrandCode
//                        , newOSMV.First().ModelCode, newOSMV.First().Series, newOSMV.First().Type
//                        , newOSMV.First().Sitting, newOSMV.First().Year
//                        , CityCode, newOSMV.First().UsageCode
//                        , newOSMV.First().RegistrationNumber
//                        , newOSMV.First().EngineNumber, newOSMV.First().ChasisNumber
//                        , newOSMV.First().IsNew, newOSMV.First().ColorOnBPKB);
//                    #endregion

//                    #region OrderSimulation
//                    query = @"SELECT DATEDIFF(YEAR,Period_From, Period_To) AS YearCoverage,
//                            Branch_Id AS BranchCode, Product_Code AS ProductCode,
//                            VANumber, lso.Survey_No AS SurveyNo, mo.Policy_No AS PolicyNo,
//                            Policy_Id AS PolicyID, Segment_Code AS SegmentCode
//                            FROM dbo.Mst_Order mo
//							INNER JOIN dbo.Lnk_Survey_Order lso
//                            ON lso.Reference_No = mo.Order_No
//                            WHERE Order_No = @0";
//                    List<dynamic> newOS = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
//                    query = @";DECLARE @@AdminFee DECIMAL (20,4)
//                            SELECT @@AdminFee=AdminFee FROM dbo.OrderSimulation
//                            WHERE OrderNo = @0
//                            SELECT SUM(Premium)+@@AdminFee FROM dbo.OrderSimulationCoverage
//                            WHERE OrderNo = @0";
//                    decimal TotalPremium = AABMobileDB.ExecuteScalar<decimal>(query, OrderNo);
//                    query = @"SELECT COALESCE(SUM(Year),0) FROM (
//                            SELECT DISTINCT DATEDIFF(year,Begin_Date,End_Date) AS Year,Begin_Date, End_Date FROM dbo.Ord_Dtl_Coverage 
//                            WHERE Order_No = @0
//                            AND Coverage_Id = 'ALLRIK' AND Interest_Type = 'P') a";
//                    int ComprePeriod = AABDB.ExecuteScalar<int>(query, PolicyOrderNo);
//                    query = @"SELECT COALESCE(SUM(Year),0) FROM (
//                            SELECT DISTINCT DATEDIFF(year,Begin_Date,End_Date) AS Year,Begin_Date, End_Date FROM dbo.Ord_Dtl_Coverage 
//                            WHERE Order_No = @0
//                            AND Coverage_Id = 'TLO' AND Interest_Type = 'P') a";
//                    int TLOPeriod = AABDB.ExecuteScalar<int>(query, PolicyOrderNo);
//                    query = @"UPDATE dbo.OrderSimulation 
//                            SET YearCoverage = @1, TLOPeriod  = @2, ComprePeriod = @3,
//                            BranchCode = @4, ProductCode = @5, TotalPremium = @6,
//                            VANumber = @7, SurveyNo = @8, PolicyNo = @9, 
//                            PolicyID = @10, SegmentCode = @11
//                            WHERE OrderNo = @0";
//                    AABMobileDB.Execute(query, OrderNo
//                        , newOS.First().YearCoverage
//                        , TLOPeriod
//                        , ComprePeriod
//                        , newOS.First().BranchCode
//                        , newOS.First().ProductCode
//                        , TotalPremium
//                        , newOS.First().VANumber
//                        , newOS.First().SurveyNo
//                        , newOS.First().PolicyNo
//                        , newOS.First().PolicyID
//                        , newOS.First().SegmentCode);
//                    #endregion
                    #endregion

                    Otosales.Logic.OrdLogic.UpdateOrderMobile(OrderNo);
                    #region AAB Image Sync
                    query = @"SELECT mi.[Image],mi.[Thumbnail],mi.File_Name AS FileName
                    ,mi.image_id AS ImageId,dsc.doc_description AS Category
					,CoreImage_ID
                    FROM [dbo].[survey] s
                    inner JOIN  [dbo].[dtl_survey_acceptance] dsc WITH (NOLOCK)
                    ON s.survey_no = dsc.survey_no
                    inner JOIN [dbo].[dtl_survey_acceptance_Image] dsci WITH (NOLOCK) 
                    ON dsc.Survey_No = dsci.Survey_No
                    and dsc.Doc_No = dsci.Doc_No
                    left JOIN beyondmoss.aabmobile.dbo.imagedata di  WITH (NOLOCK)
                    ON di.coreimage_id = dsci.image_id
                    inner JOIN [dbo].[mst_image] mi WITH (NOLOCK) 
                    ON mi.image_id = dsci.image_id
                    where dsc.document_type = 'acceptance'
                    AND dsc.Doc_description in ('KTP/KITAS','STNK','BSTB','NPWP')
                    AND s.reference_no = @0
                    AND (s.[Status] = '7' OR s.[Status] = '6')
					AND ISNULL(mi.image_id,'') <> ''";
                    string qIsertAABImg = @"IF NOT EXISTS(SELECT * FROM dbo.Dtl_Image WHERE Image_Id = @0 AND Reference_No = @1)
                                            BEGIN
                                            INSERT INTO dbo.Dtl_Image
                                                    ( Reference_No ,
                                                      Reference_Type ,
                                                      Number ,
                                                      Object_No ,
                                                      Image_Id ,
                                                      EntryUsr ,
                                                      EntryDt
                                                    )
                                            VALUES  ( @1 , -- Reference_No - varchar(20)
                                                      @2 , -- Reference_Type - char(6)
                                                      0 , -- Number - int
                                                      1 , -- Object_No - int
                                                      @0 , -- Image_Id - char(12)
                                                      'OTOSL' , -- EntryUsr - char(5)
                                                      GETDATE()  -- EntryDt - datetime
                                                    )
                                            END";
                    string qInsertImg = @"IF NOT EXISTS (SELECT * FROM dbo.ImageData WHERE PathFile = @3 AND CoreImage_ID = @6)
                                            BEGIN
                                            INSERT INTO dbo.ImageData
                                                    ( ImageID ,
                                                        DeviceID ,
                                                        SalesOfficerID ,
                                                        PathFile ,
                                                        EntryDate ,
                                                        RowStatus ,
                                                        Data ,
                                                        ThumbnailData ,
			                                            CoreImage_ID
                                                    )
                                            VALUES  ( @0 , -- ImageID - varchar(100)
                                                        @1 , -- DeviceID - varchar(100)
                                                        @2 , -- SalesOfficerID - varchar(20)
                                                        @3 , -- PathFile - varchar(512)
                                                        GETDATE() , -- EntryDate - datetime
                                                        1 , -- RowStatus - bit
                                                        @4 , -- Data - varbinary(max)
                                                        @5,   -- ThumbnailData - varbinary(max)
                                                        @6
                                                    )
                                            END";
                    List<dynamic> ImageAAB = AABDB.Fetch<dynamic>(query,PolicyOrderNo);

                    List<dynamic> ImgMobile = GetImageSurveyMobile(OrderInfo.First().FollowUpNo);
                    string qUpdateFollowUp = @"UPDATE dbo.FollowUp SET {0} = @0 WHERE FollowUpNo = @1";
                    string qUpdateProspect = @"UPDATE dbo.ProspectCompany SET {0} = @0 WHERE FollowUpNo = @1";

                    foreach (dynamic d in ImageAAB)
                    {
                        bool isexist = false;
                        string[] parts = d.FileName.Split('.');
                        foreach (dynamic e in ImgMobile)
                        {
                            string type = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage(e.ImageType,true);
                            string type2 = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage(e.ImageType, false);
                            string typeAAB = Convert.ToString(d.Category);
                            if (typeAAB.Trim().ToUpper().Equals(type))
                            {
                                isexist = true;
                                int idAAB = Convert.ToInt32(d.ImageId);
                                int idMobile = Convert.ToInt32(d.CoreImage_ID);
                                if (idAAB != idMobile) {
                                    AABMobileDB.Execute("UPDATE dbo.ImageData SET RowStatus=0 WHERE PathFile=@0 AND CoreImage_ID = @1"
                                        , e.ImageName, e.CoreImage_ID);
                                    string ImageID = Guid.NewGuid().ToString();

                                    byte[] imgData = Otosales.Logic.ImageLogic.DecodeFromAAB2000ImageBytes(d.Image);
                                    byte[] thumbnail = Otosales.Logic.ImageLogic.DecodeFromAAB2000ImageBytes(d.Thumbnail);

                                    string refno = "";
                                    if (type2.ToUpper().Equals("KTP") || type2.ToUpper().Equals("NPWP"))
                                    {
                                        refno = ProspectID;
                                    }
                                    else
                                    {
                                        refno = PolicyOrderNo;
                                    }
                                    AABDB.Execute(qIsertAABImg, d.ImageId, refno, type2);
                                    AABMobileDB.Execute(qInsertImg
                                        , ImageID
                                        , DeviceID
                                        , SalesOfficerID
                                        , ImageID + "." + parts[1]
                                        , imgData
                                        , thumbnail
                                        , d.ImageId);
                                    string imgtype = Convert.ToString(e.ImageType);
                                    if (type.ToUpper().Equals("NPWP") || type.ToUpper().Equals("SIUP"))
                                    {
                                        query = string.Format(qUpdateProspect, e.ImageType);
                                        AABMobileDB.Execute(query, ImageID + "." + parts[1], OrderInfo.First().FollowUpNo);
                                    }
                                    else
                                    {
                                        query = string.Format(qUpdateFollowUp, e.ImageType);
                                        AABMobileDB.Execute(query, ImageID + "." + parts[1], OrderInfo.First().FollowUpNo);
                                    }

                                }
                            }
                        }
                        if (!isexist) {
                            var ImageType = Otosales.Logic.ImageLogic.GetRevertMappedReferenceTypeImage(d.Category.Trim());
                            var ImgType = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage(ImageType,false);
                            string ImageID = Guid.NewGuid().ToString();
                            byte[] imgData = Otosales.Logic.ImageLogic.DecodeFromAAB2000ImageBytes(d.Image);
                            byte[] thumbnail = Otosales.Logic.ImageLogic.DecodeFromAAB2000ImageBytes(d.Thumbnail);
                            string refno = "";
                            if (ImgType.ToUpper().Equals("KTP") || ImgType.ToUpper().Equals("NPWP"))
                            {
                                refno = ProspectID;
                            }else{
                                refno = PolicyOrderNo;
                            }
                            AABDB.Execute(qIsertAABImg, d.ImageId, refno, ImgType);
                            AABMobileDB.Execute(qInsertImg
                                , ImageID
                                , DeviceID
                                , SalesOfficerID
                                , ImageID + "." + parts[1]
                                , imgData
                                , thumbnail
                                , d.ImageId);
                            string type = Convert.ToString(ImageType);
                            if (type.ToUpper().Equals("NPWP") || type.ToUpper().Equals("SIUP"))
                            {
                                AABMobileDB.Execute(string.Format(qUpdateProspect, ImageType), ImageID + "." + parts[1], OrderInfo.First().FollowUpNo);
                            }
                            else
                            {
                                AABMobileDB.Execute(string.Format(qUpdateFollowUp, ImageType), ImageID + "." + parts[1], OrderInfo.First().FollowUpNo);
                            }

                        }
                    }
                    foreach (dynamic mbl in ImgMobile)
                    {
                        bool isexist = false;
                        foreach (dynamic aab in ImageAAB)
                        {
                            string type = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage(mbl.ImageType,true);
                                                        string typeAAB = Convert.ToString(aab.Category);
                            if (typeAAB.Trim().ToUpper().Equals(type))
                            {
                                isexist = true;
                            }
                        }
                        if (!isexist) {
                            AABMobileDB.Execute("UPDATE dbo.ImageData SET RowStatus=0 WHERE PathFile=@0 AND CoreImage_ID = @1"
                                , mbl.ImageName, mbl.CoreImage_ID);
                            if (mbl.ImageType.ToUpper().Equals("NPWP") || mbl.ImageType.ToUpper().Equals("SIUP"))
                            {
                                AABMobileDB.Execute(string.Format(qUpdateProspect, mbl.ImageType), "", OrderInfo.First().FollowUpNo);
                            }
                            else
                            {
                                AABMobileDB.Execute(string.Format(qUpdateFollowUp, mbl.ImageType), "", OrderInfo.First().FollowUpNo);
                            }
                        }
                    }
                    #endregion
                    query = "SELECT CustID,FollowUpNo FROM dbo.OrderSimulation os WHERE OrderNo = @0";
                    List<dynamic> tmp = AABMobileDB.Fetch<dynamic>(query, OrderNo);
                    if (tmp.Count > 0)
                    {
                        result = GetTaksListDetail(tmp.First().CustID, tmp.First().FollowUpNo);
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }                
        }

        public static bool GetCheckSurveyResult(string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            bool resData = true;
            bool resImg = true;
            bool resPros = true;
            int diffData = 0;
            int diffImg = 0;
            try
            {
                query = @"SELECT PolicyOrderNo,FollowUpNo, CAST(COALESCE(isCompany,0) AS BIT) isCompany, ProspectID, pc.CustID, CustIDAAB    
						FROM dbo.OrderSimulation os 
                        INNER JOIN dbo.ProspectCustomer pc
                        ON pc.CustID = os.CustID
                        WHERE OrderNo = @0";
                List<dynamic> ord = AABMobileDB.Fetch<dynamic>(query, OrderNo);
                string PolicyOrderNo = ord.First().PolicyOrderNo;
                string FollowUpNo = ord.First().FollowUpNo;
                bool isCompany = ord.First().isCompany;
                List<dynamic> osiMobile = new List<dynamic>();
                List<dynamic> osiAAB = new List<dynamic>();
                List<dynamic> oscMobile = new List<dynamic>();
                List<dynamic> oscAAB = new List<dynamic>();
                List<dynamic> diffImage; 
                if (!string.IsNullOrEmpty(PolicyOrderNo))
                {
                    query = @"SELECT * FROM dbo.OrderSimulationInterest WHERE OrderNo = @0";
                    osiMobile = AABMobileDB.Fetch<dynamic>(query, OrderNo);
                    query = @"SELECT * FROM dbo.Ord_Dtl_Interest WHERE Order_No = @0";
                    osiAAB = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    if (osiMobile.Count == osiAAB.Count)
                    {
                        query = @"SELECT * FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                        oscMobile = AABMobileDB.Fetch<dynamic>(query, OrderNo);
                        query = @"SELECT * FROM dbo.Ord_Dtl_Coverage WHERE Order_No = @0";
                        oscAAB = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                        if (oscMobile.Count == oscAAB.Count) {
                            foreach (dynamic d in osiMobile)
                            {
                                query = "SELECT * FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0 AND InterestNo = @1";
                                oscMobile = AABMobileDB.Fetch<dynamic>(query, OrderNo, d.InterestNo);
                                query = "SELECT * FROM dbo.Ord_Dtl_Coverage WHERE Order_No = @0 AND Interest_No = @1";
                                oscAAB = AABDB.Fetch<dynamic>(query, PolicyOrderNo, d.InterestNo);
                                if (oscMobile.Count == oscAAB.Count)
                                {
                                    decimal PremiumAABMobile = AABMobileDB.ExecuteScalar<decimal>(
                                                @"SELECT COALESCE(SUM(Premium),0) 
                                                FROM dbo.OrderSimulationCoverage 
                                                WHERE OrderNo = @0 AND InterestNo = @1", OrderNo, d.InterestNo);
                                    decimal PremiumAAB = AABDB.ExecuteScalar<decimal>(
                                                @"SELECT COALESCE(SUM(Net_Premium),0) 
                                                FROM dbo.Ord_Dtl_Coverage
                                                WHERE Order_No = @0 AND Interest_No = @1", PolicyOrderNo, d.InterestNo);
                                    decimal GrossPremiumAABMobile = AABMobileDB.ExecuteScalar<decimal>(
                                                @"SELECT COALESCE(SUM(Gross_Premium),0) 
                                                FROM dbo.OrderSimulationCoverage 
                                                WHERE OrderNo = @0 AND InterestNo = @1", OrderNo, d.InterestNo);
                                    decimal GrossPremiumAAB = AABDB.ExecuteScalar<decimal>(
                                                @"SELECT COALESCE(SUM(Gross_Premium),0) 
                                                FROM dbo.Ord_Dtl_Coverage
                                                WHERE Order_No = @0 AND Interest_No = @1", PolicyOrderNo, d.InterestNo);

                                    if (PremiumAABMobile == PremiumAAB && GrossPremiumAABMobile == GrossPremiumAAB)
                                    {
                                        diffData++;
                                    }
                                }
                            }
                            if (diffData == osiAAB.Count) {
                                resData = false;
                            }
                        }
                    }

                    #region CheckSurveyImage
                    query = @"SELECT DISTINCT mi.File_Name AS FileName
                    ,mi.image_id AS ImageId,dsc.doc_description AS Category
					,CoreImage_ID
                    FROM [dbo].[survey] s
                    inner JOIN  [dbo].[dtl_survey_acceptance] dsc WITH (NOLOCK)
                    ON s.survey_no = dsc.survey_no
                    inner JOIN [dbo].[dtl_survey_acceptance_Image] dsci WITH (NOLOCK) 
                    ON dsc.Survey_No = dsci.Survey_No
                    and dsc.Doc_No = dsci.Doc_No
                    left JOIN beyondmoss.aabmobile.dbo.imagedata di  WITH (NOLOCK)
                    ON di.coreimage_id = dsci.image_id
                    inner JOIN [dbo].[mst_image] mi WITH (NOLOCK) 
                    ON mi.image_id = dsci.image_id
                    where dsc.document_type = 'acceptance'
                    AND dsc.Doc_description in ('KTP/KITAS','STNK','BSTB','NPWP')
                    AND s.reference_no = @0
                    AND s.[Status] IN ('7','6')
					AND ISNULL(mi.image_id,'') <> ''";
                    diffImage = AABDB.Fetch<dynamic>(query, PolicyOrderNo);
                    foreach (dynamic img in diffImage)
                    {
                        int idAAB = Convert.ToInt32(img.ImageId);
                        int idMobile = 0;
                        if (img.CoreImage_ID != null)
                        {
                            idMobile = Convert.ToInt32(img.CoreImage_ID);
                        }
                        if (idAAB == idMobile)
                        {
                            diffImg++;
                        }
                    }
                    if (diffImg == diffImage.Count) {
                        resImg = false;
                    }
                    #endregion
                    List<dynamic> ls = new List<dynamic>();
                    if (ord.First().isCompany)
                    {
                        string qGetAABCompany = "";
                        if (!string.IsNullOrEmpty(ord.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ord.First().CustIDAAB))
                        {
                            qGetAABCompany = @";SELECT RTRIM(mc.Name) CompanyName,
                                            RTRIM((SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m
                                            WHERE AdditionalCode = 'NPWN' AND m.CustID = @0)) NPWPno,
                                            RTRIM(NULLIF((SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m
                                            WHERE AdditionalCode = 'NPDT' AND m.CustID = @0),'')) NPWPdate,
                                            RTRIM((SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m 
                                            WHERE AdditionalCode = 'NPAD' AND m.CustID = @0)) NPWPaddress,
                                            RTRIM(Office_Address) OfficeAddress,
                                            RTRIM(Office_PostCode) PostalCode,
                                            RTRIM((SELECT AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo m 
                                            WHERE AdditionalCode = 'MPC1' AND m.CustID = @0)) PICPhoneNo,
                                            RTRIM(mcp.Name) PICname,
                                            RTRIM(Email) Email
                                            FROM dbo.Mst_Customer mc
                                            LEFT JOIN dbo.Mst_Cust_Pic mcp
                                            ON mcp.Cust_Id = mc.Cust_Id
                                            WHERE mc.Cust_Id = @0 ORDER BY mcp.entrydt DESC";
                            ls = AABDB.Fetch<dynamic>(qGetAABCompany, ord.First().CustIDAAB);
                        }
                        else {
                            qGetAABCompany = @";SELECT RTRIM(mc.Name) CompanyName,
                                    RTRIM((SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m
                                    WHERE AdditionalCode = 'NPWN' AND m.ProspectID = @0)) NPWPno,
                                    RTRIM(NULLIF((SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m
                                    WHERE AdditionalCode = 'NPDT' AND m.ProspectID = @0),'')) NPWPdate,
                                    RTRIM((SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m 
                                    WHERE AdditionalCode = 'NPAD' AND m.ProspectID = @0)) NPWPaddress,
                                    RTRIM(Office_Address) OfficeAddress,
                                    RTRIM(Office_PostCode) PostalCode,
                                    RTRIM((SELECT AdditionalInfoValue FROM dbo.Mst_Prospect_AdditionalInfo m 
                                    WHERE AdditionalCode = 'MPC1' AND m.ProspectID = @0)) PICPhoneNo,
                                    RTRIM(mcp.Name) PICname,
                                    RTRIM(Email) Email
                                    FROM dbo.Mst_Prospect mc
                                    LEFT JOIN dbo.Mst_Prospect_Pic mcp
                                    ON mcp.Prospect_Id = mc.Prospect_Id
                                    WHERE mc.Prospect_Id = @0 ORDER BY mcp.entrydt DESC";
                            ls = AABDB.Fetch<dynamic>(qGetAABCompany, ord.First().ProspectID);                        
                        }
                        string qGetMobileCompany = @"SELECT RTRIM(CompanyName) CompanyName, RTRIM(NPWPno) NPWPno, RTRIM(CONVERT(VARCHAR,NULLIF(NPWPdate,''),103)) NPWPdate, RTRIM(NPWPaddress) NPWPaddress 
                                                , RTRIM(OfficeAddress) OfficeAddress, RTRIM(PostalCode) PostalCode, RTRIM(PICPhoneNo) PICPhoneNo
                                                , RTRIM(PICname) PICname, RTRIM(Email) Email
                                                FROM dbo.ProspectCompany WHERE CustID = @0";
                        dynamic dataCompanyAAB = null;
                        dynamic dataCompanyMbl = null;
                        if (ls.Count > 0)
                        {
                            dataCompanyAAB = ls.First();
                        }
                        ls = AABMobileDB.Fetch<dynamic>(qGetMobileCompany, ord.First().CustID);
                        if (ls.Count > 0)
                        {
                            dataCompanyMbl = ls.First();
                        }
                        string jsonAAB = JsonConvert.SerializeObject(dataCompanyAAB);
                        string jsonMbl = JsonConvert.SerializeObject(dataCompanyMbl);
                        if (jsonAAB.ToLower().Equals(jsonMbl.ToLower())) {
                            resPros = false;
                        }
                    }
                    else {
                        string qGetProsAAB = "";
                        if (!string.IsNullOrEmpty(ord.First().CustIDAAB) && !string.IsNullOrWhiteSpace(ord.First().CustIDAAB))
                        {
                            qGetProsAAB = @"SELECT RTRIM(Name) Name,RTRIM(HP) Phone1,RTRIM(HP_2) Phone2,RTRIM(Email) Email1,
                                            RTRIM(CAST(Birth_Date AS DATE)) CustBirthDay,RTRIM(IIF(Sex='1','M',IIF(Sex='2','F',NULL))) CustGender, RTRIM(mp.Home_Address) CustAddress, 
                                            RTRIM(Home_PostCode) PostalCode,RTRIM(id_card) IdentityNo FROM dbo.Mst_Customer mp
                                            INNER JOIN dbo.Mst_Cust_Personal mpp ON mpp.Cust_Id = mp.Cust_Id
                                            INNER JOIN dbo.Mst_Order mo ON mo.Cust_Id = mp.Cust_Id
                                            WHERE Order_No = @0";
                        }
                        else {
                            qGetProsAAB = @"SELECT RTRIM(Name) Name,RTRIM(HP) Phone1,RTRIM(HP_2) Phone2,RTRIM(Email) Email1,
                                            RTRIM(CAST(Birth_Date AS DATE)) CustBirthDay,RTRIM(IIF(Sex='1','M',IIF(Sex='2','F',NULL))) CustGender, RTRIM(mp.Home_Address) CustAddress, 
                                            RTRIM(Home_PostCode) PostalCode,RTRIM(id_card) IdentityNo FROM dbo.Mst_Prospect mp
                                            INNER JOIN dbo.Mst_Prospect_Personal mpp ON mpp.Prospect_Id = mp.Prospect_Id
                                            INNER JOIN dbo.Mst_Order mo ON mo.Prospect_Id = mp.Prospect_Id
                                            WHERE Order_No = @0";
                        }
                        string qGetProsMbl = @"SELECT RTRIM(Name) Name,RTRIM(Phone1) Phone1, RTRIM(Phone2) Phone2, RTRIM(Email1) Email1,
                                            RTRIM(CAST(CustBirthDay AS DATE)) CustBirthDay, RTRIM(CustGender) CustGender, RTRIM(CustAddress) CustAddress, RTRIM(PostalCode) PostalCode,
                                            RTRIM(IdentityNo) IdentityNo FROM dbo.ProspectCustomer 
                                            WHERE CustID = @0";
                        dynamic dataProsAAB = null;
                        dynamic dataProsMbl = null;
                        ls = AABDB.Fetch<dynamic>(qGetProsAAB, PolicyOrderNo);
                        if (ls.Count > 0)
                        {
                            dataProsAAB = ls.First();
                        }
                        ls = AABMobileDB.Fetch<dynamic>(qGetProsMbl, ord.First().CustID);
                        if (ls.Count > 0)
                        {
                            dataProsMbl = ls.First();
                        }
                        string jsonAAB = JsonConvert.SerializeObject(dataProsAAB);
                        string jsonMbl = JsonConvert.SerializeObject(dataProsMbl);
                        if (jsonAAB.ToLower().Equals(jsonMbl.ToLower()))
                        {
                            resPros = false;
                        }
                    }
                }
                if (!resImg && !resData && !resPros)
                {
                    return false;
                }
                else {
                    return true;
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public //static 
            BankInformationResult GetBankInformation(string SalesmanCode)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var BeyondDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB);
            BankInformationResult result = new BankInformationResult();
            try
            {
                query = @";SELECT SalesmanCode,Description,CustID FROM dbo.SalesmanDealer WHERE SalesmanCode = @0";
                List<dynamic> salesmandealer = AABMobileDB.Fetch<dynamic>(query, SalesmanCode);
                if(salesmandealer.Count>0){
                    query = ";EXEC General.usp_DealerValidation @0";
                    List<dynamic> info = BeyondDB.Fetch<dynamic>(query, salesmandealer.First().CustID);
                    if (info.Count > 0)
                    {
                        if (info.First().IsValid)
                        {
                            result.BankName = info.First().Account_Info_BankName;
                            result.AccountNo = info.First().Account_Info_AccountNo;
                            result.ErrorMessage = info.First().ErrorMessage;
                        }
                        else {
                            result.ErrorMessage = info.First().ErrorMessage;                        
                        }
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public static void UpdateImageFollowUp(string followUpID)
        {
            try
            {
                string query = @"SELECT ImageType,PathFile FROM tempimagedata WHERE FollowUpNo = @0";

                List<dynamic> listImageData = new List<dynamic>();
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    listImageData = db.Fetch<dynamic>(query, followUpID);
                }

                string qUpdateFUImg = @"UPDATE FollowUp ";
                #region
                foreach (dynamic a in listImageData)
                {
                    qUpdateFUImg = @"UPDATE FollowUp ";
                    string imageType = Convert.ToString(a.ImageType);
                    switch (imageType)
                    {
                        case "FORMRENEWAL":
                            qUpdateFUImg += @"SET KonfirmasiCust= @0 WHERE FollowUpNo =@1";
                            break;
                        case "RENEWALNOTICE":
                            qUpdateFUImg += @"SET RenewalNotice= @0 WHERE FollowUpNo =@1";
                            break;
                        case "BUKTIBAYAR":
                            qUpdateFUImg += @"SET BuktiBayar= @0 WHERE FollowUpNo =@1";
                            break;
                        case "KTP":
                            qUpdateFUImg += @"SET IdentityCard= @0 WHERE FollowUpNo =@1";
                            break;
                        case "STNK":
                            qUpdateFUImg += @"SET STNK= @0 WHERE FollowUpNo =@1";
                            break;
                        case "SURVEY":
                            qUpdateFUImg += @"SET Survey= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA1":
                            qUpdateFUImg += @"SET DocNSA1= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA2":
                            qUpdateFUImg += @"SET DocNSA2= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA3":
                            qUpdateFUImg += @"SET DocNSA3= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA4":
                            qUpdateFUImg += @"SET DocNSA4= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA5":
                            qUpdateFUImg += @"SET DocNSA5= @0 WHERE FollowUpNo =@1";
                            break;
                        case "DOCNSA6":
                            qUpdateFUImg += @"SET DocNSA6= @0 WHERE FollowUpNo =@1";
                            break;
                        default:
                            qUpdateFUImg = "";
                            break;
                    }

                    if (!String.IsNullOrEmpty(qUpdateFUImg))
                    {
                        using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                        {
                            db.Execute(qUpdateFUImg, a.PathFile, followUpID);
                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        public static A2isCommonResult SendQuotationEmail(string OrderNo, string Email)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            A2isCommonResult rslt = new A2isCommonResult();
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();
                string DSOrder = "select a.OrderNo, a.EntryDate,b.Name 'CustName',b.Phone1 + case when isnull(b.Phone2,'')='' then'' else '/' + b.Phone2 end 'CustPhone', ";
                DSOrder += " b.Email1 'CustEmail', so.Name 'SalesName', sr.Description 'SalesRole',sr.Role, so.Phone1 + case when isnull(so.Phone2,'')='' then'' else '/' + so.Phone2 end 'SalesPhone',so.Email 'SalesEmail', so.Fax 'SalesFax',";
                DSOrder += " so.OfficePhone 'SalesOfficePhone', so.Ext 'SalesOfficePhoneExt', a.QuotationNo, ab.AccountName, ab.AccountNo, ab.Bank, ab.Description 'Branch', ab.City 'BrachCity',";
                DSOrder += " d.Sequence 'Region', cast(isnull(a.TLOPeriod,0)+isnull(a.ComprePeriod,0) as varchar(10)) + ' Tahun' 'Period',";
                DSOrder += " a.MultiYearF, case when isnull(a.TLOPeriod,0)>0 and isnull(a.ComprePeriod,0)=0 then 'TLO'";
                DSOrder += " when  isnull(a.TLOPeriod,0)=0 and isnull(a.ComprePeriod,0)>0 then 'Comprehensive'";
                DSOrder += " else 'Kombinasi' end 'Pertanggungan', CAST(DAY(GETDATE()) AS VARCHAR(2)) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS [ExecTime], a.InsuranceType, pt.Description AS Product";
                DSOrder += " from OrderSimulation a";
                DSOrder += " inner join ProspectCustomer b on a.CustID=b.CustID";
                DSOrder += " inner join SalesOfficer so on a.SalesOfficerID=so.SalesOfficerID";
                DSOrder += " inner join SysUserRole sr on so.Role=sr.Role";
                DSOrder += " inner join AABBranch ab on so.BranchCode=ab.BranchCode";
                DSOrder += " inner join OrderSimulationMV c on a.OrderNo=c.OrderNo";
                DSOrder += " inner join Region d on c.CityCode=d.RegionCode";
                DSOrder += " inner join ProductType pt on pt.InsuranceType=a.InsuranceType";
                DSOrder += " where a.OrderNo=@0";

                dynamic dtOrder = db.FirstOrDefault<dynamic>(DSOrder, OrderNo);

                string DSVehicle = "select  c.Description + ' ' + d.Description + ' ' + e.Series + ' ' + b.Year 'VehicleDescription',";
                DSVehicle += " a.RegistrationNumber, a.Year, f.Description 'Usage', a.sitting, a.SumInsured, o.YearCoverage,c.Description AS Brand, d.Description AS Model,b.Type,vt.Description AS TypeDesc";
                DSVehicle += " from OrderSimulation o ";
                DSVehicle += " inner join OrderSimulationMV a on a.OrderNo=o.OrderNo";
                DSVehicle += " inner join Vehicle b on a.VehicleCode=b.VehicleCode and a.BrandCode=b.BrandCode and a.ProductTypeCode=b.ProductTypeCode";
                DSVehicle += " and a.ModelCode=b.ModelCode and a.Series=b.Series and a.Type=b.Type and a.Sitting=b.Sitting and a.Year=b.Year";
                DSVehicle += " inner join VehicleBrand c on a.BrandCode=c.BrandCode";
                DSVehicle += " inner join VehicleModel d on a.BrandCode=d.BrandCode and a.ModelCode=d.ModelCode";
                DSVehicle += " inner join VehicleSeries e on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series";
                DSVehicle += " inner join VehicleType vt on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series AND vt.VehicleTypeCode=a.Type";
                DSVehicle += " inner join Dtl_Ins_Factor f on a.UsageCode=f.insurance_code and f.Factor_Code='MVUSAG'";
                DSVehicle += " where a.OrderNo=@0";

                dynamic dtVehicle = db.FirstOrDefault<dynamic>(DSVehicle, OrderNo);

                string NameCust = dtOrder.CustName;
                string NameProduct = dtOrder.Product;
                string PeriodePertanggungan = dtOrder.Period;
                string NamaSales = dtOrder.SalesName;
                string NomorSales = dtOrder.SalesPhone;
                string EmailSales = dtOrder.SalesEmail;
                string Brand = dtVehicle.Brand;
                string Model = dtVehicle.Model;
                string Type = dtVehicle.TypeDesc;
                string Kontak1 = ConfigurationManager.AppSettings["ContactSales"+dtOrder.Role].ToString();
                Kontak1 = Kontak1.Replace("@Nama", NamaSales).Replace("@Email", EmailSales).Replace("@Phone", NomorSales);

                string Kontak2 = ConfigurationManager.AppSettings["ContactInfo" + dtOrder.InsuranceType].ToString();
                //int yearCoverage = Convert.ToInt32(dtVehicle.YearCoverage);
                int insuranceType = Convert.ToInt32(dtOrder.InsuranceType);
                 List<A2isMessagingAttachment> attFile = new List<A2isMessagingAttachment>();
               // int yearCoverage = 1;
                attFile.Add(Otosales.Repository.v0219URF2019.MobileRepository.GenerateReportFile(OrderNo, insuranceType));
                EmailSubject = EmailSubject.Replace("@Product", dtOrder.Product).Replace("@Customer", NameCust);
                if (!string.IsNullOrEmpty(EmailSubject))
                {
                    string EmailBody = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='CONTENT-QUOTATIONEMAIL' AND ParamType='TMP'");
                    string emailText = EmailBody.Replace("@Product", NameProduct).Replace("@Contact1", Kontak1).Replace("@Contact2", Kontak2).Replace("@Brand", Brand).Replace("@Type", Type).Replace("@Model", Model);
                    rslt = MessageController.SendEmail(Email, "", "", emailText, EmailSubject, attFile);

                }
                if (rslt.ResultCode)
                {
                    string query = @"UPDATE ORDERSIMULATION SET SendStatus=1, SendDate=GETDATE() WHERE OrderNo=@0";
                    db.Execute(query, OrderNo);
//                    bool isInfoChanged = false;
//                    FollowUp FU = db.FirstOrDefault<FollowUp>("SELECT * FROM FollowUp WHERE FollowUpNo=(SELECT TOP 1 FollowUpNo From OrderSimulation WHERE OrderNo=@0)", OrderNo);
//                    if (FU.FollowUpStatus < 4 || FU.FollowUpStatus == 9)
//                    {

////                        int lastseqno = db.ExecuteScalar<int>(@"IF EXISTS(SELECT *  FROM FollowUpHistory WHERE FollowUpNo=@0)
////BEGIN
////SELECT TOP 1 SeqNo FROM FollowUpHistory WHERE FollowUpNo=@0 ORDER BY SeqNo DESC
////END
////ELSE
////BEGIN
////SELECT 1 AS SeqNo
////END", FU.FollowUpNo);
////                        if (FU.FollowUpInfo != 3)
////                        {
////                            isInfoChanged = true;
////                            lastseqno++;
////                        }
////                        FU.LastSeqNo = lastseqno;
////                        FU.FollowUpStatus = 2;
////                        FU.FollowUpInfo = 3;
////                        query = @"UPDATE FollowUp SET 
////                           LastSeqNo=@1,
////                           FollowUpStatus=@2,
////                           FollowUpInfo=@3,
////                           NextFollowUpDate=GETDATE(),
////                           LastFollowUpDate=GETDATE()
////                           WHERE FollowUpNo=@0
////                           ";
////                        db.Execute(query, FU.FollowUpNo, FU.LastSeqNo, FU.FollowUpStatus, FU.FollowUpInfo);
////                        if (isInfoChanged)
////                        {
////                            #region INSERT FOLLOW UP HISTORY
////                            query = @";
////    INSERT INTO FollowUpHistory (
////       [FollowUpNo]
////      ,[SeqNo]
////      ,[CustID]
////      ,[ProspectName]
////      ,[Phone1]
////      ,[Phone2]
////      ,[SalesOfficerID]
////      ,[EntryDate]
////      ,[FollowUpName]
////      ,[NextFollowUpDate]
////      ,[LastFollowUpDate]
////      ,[FollowUpStatus]
////      ,[FollowUpInfo]
////      ,[Remark]
////      ,[BranchCode]
////      ,[LastUpdatedTime]
////      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,GETDATE(),@7,@8,@9,@10,@11,@12,@13,GETDATE(),0)
////";

////                            db.Execute(query, FU.FollowUpNo, lastseqno, FU.CustID, FU.ProspectName, FU.Phone1, FU.Phone2, FU.SalesOfficerID, FU.FollowUpName, FU.NextFollowUpDate, FU.LastFollowUpDate, FU.FollowUpStatus, FU.FollowUpInfo, FU.Remark, FU.BranchCode);
////                            #endregion
////                        }
//                    }
                }
                return rslt;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static A2isCommonResult SendQuotationSMS(string OrderNo, string Phone) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                OrderSimulation os = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE OrderNo=@0", OrderNo);
                OrderSimulationMV osmv = db.FirstOrDefault<OrderSimulationMV>("SELECT TOP 1 * FROM OrderSimulationMV WHERE OrderNo=@0", OrderNo);
                bool isRenewal = Convert.ToBoolean(db.ExecuteScalar<int>("SELECT COALESCE(IsRenewal,0)  FROM FollowUp WHERE FollowUpNo=@0", os.FollowUpNo));
                string vehicle = db.ExecuteScalar<string>("SELECT TOP 1 VB.Description+' '+VM.Description+' '+ @3+' TH '+@2 FROM  VehicleBrand VB  LEFT JOIN VehicleModel VM ON VM.BrandCode=@1 AND VM.ModelCode=@0 WHERE VB.BrandCode=@1 ", osmv.ModelCode, osmv.BrandCode, osmv.Year, osmv.Series);
                if (os.ComprePeriod == 0 && os.TLOPeriod == 0)
                {
                    os.ComprePeriod= db.ExecuteScalar<int>("select COUNT(*) from ordersimulationcoverage  where orderno=@0 AND CoverageID='ALLRIK' AND InterestNo=1",OrderNo);
                    os.TLOPeriod = db.ExecuteScalar<int>("select COUNT(*) from ordersimulationcoverage  where orderno=@0 AND CoverageID='TLO' AND InterestNo=1", OrderNo);
                }
                string coveragedesc = (os.ComprePeriod) > 0 ? "Comprehensive " + os.ComprePeriod + ((os.ComprePeriod == 1) ? " Year," : " Years,") : "";
                coveragedesc = os.TLOPeriod == 0 ? coveragedesc : string.IsNullOrEmpty(coveragedesc) ? "" : (coveragedesc+ " ");
                coveragedesc = coveragedesc + ((os.TLOPeriod) > 0 ? "TLO " + os.TLOPeriod + ((os.TLOPeriod == 1) ? " Year" : " Years") : "");
                string message = (isRenewal?"PENAWARAN: ":"SIMULASI: ")+ os.QuotationNo + " " + coveragedesc + " " + (os.ComprePeriod + os.TLOPeriod);
                message += " " + vehicle.ToUpper();
                message += " Premi Rp " + Util.ToThousand(os.TotalPremium);

                A2isCommonResult statusMessage = MessageController.SendSMS(message, Phone);
                return statusMessage;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                
                throw e;
            }
        }

        public //static 
            List<dynamic> GetMappingVehiclePlateGeoArea(string VehiclePlateCode) {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            List<dynamic> result = new List<dynamic>();
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                query = @"SELECT mp.VehiclePlateCode, mp.VehiclePlateDescription, RegionCode, Description 
                        FROM dbo.MappingVehiclePlateGeoArea mvpg WITH (NOLOCK)
                        INNER JOIN dbo.MstVehiclePlate mp WITH (NOLOCK)
                        ON mp.VehiclePlateCode = mvpg.VehiclePlateCode
                        INNER JOIN BEYONDMOSS.AABMobile.dbo.Region r WITH (NOLOCK)
						ON mvpg.GeoAreaCode = r.RegionCode
                        WHERE mvpg.VehiclePlateCode = @0";
                result = AABDB.Fetch<dynamic>(query, VehiclePlateCode);
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                
                throw e;
            }
        }

        #region private method
        public static void InsertOrderSimulation(string OSData, string OSMVData)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            OrderSimulationModel OS = new OrderSimulationModel();
            OrderSimulationMVModel OSMV = new OrderSimulationMVModel();
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                #region INSERT ORDERSIMULATION
                string OrderNoReturn = "";
                string FollowUpNoEnd = "";
                string CustIdEnd = "";
                OS = JsonConvert.DeserializeObject<OrderSimulationModel>(OSData);
                query = "SELECT OrderNo FROM OrderSimulation WHERE CustID=@0 AND FollowUpNo=@1 AND ApplyF = 1";
                List<dynamic> ordno = db.Fetch<dynamic>(query, OS.CustID, OS.FollowUpNo);
                string OrderNo = "";
                if (ordno.Count > 0)
                {
                    OrderNo = ordno.First().OrderNo;
                }else{
                    OrderNo = System.Guid.NewGuid().ToString();
                }
                OS.OrderNo = string.IsNullOrEmpty(OS.OrderNo) ? OrderNo : OS.OrderNo;
                OrderNoReturn = OS.OrderNo;
                OS.FollowUpNo = string.IsNullOrEmpty(OS.FollowUpNo) ? FollowUpNoEnd : OS.FollowUpNo;
                OS.CustID = string.IsNullOrEmpty(OS.CustID) ? CustIdEnd : OS.CustID;
                OS.InsuranceType = (OS.InsuranceType == null || OS.InsuranceType == 0) ? 1 : OS.InsuranceType;
                OS.ProductCode = string.IsNullOrEmpty(OS.ProductCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductCode From Product") : OS.ProductCode;
                OS.ProductTypeCode = string.IsNullOrEmpty(OS.ProductTypeCode) ? db.ExecuteScalar<string>("SELECT TOP 1 ProductTypeCode From Product") : OS.ProductTypeCode;
                query = @"IF NOT EXISTS(SELECT * FROM OrderSimulation WHERE OrderNo=@0)
BEGIN
    INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[VANumber]
      ,[SurveyNo]
      ,[PeriodFrom]
      ,[PolicyNo]
      ,[AmountRep]
      ,[PolicyID]
      ,[SegmentCode]
) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,NULL,GETDATE(),GETDATE(),1,@21,@22,@23,@24,@25
,@26,@27,@28,@29,@30,@31,@32,@33,@34,@35,@36)
END";
                OS.SalesOfficerID = OS.SalesOfficerID.Contains("@") ? db.ExecuteScalar<string>("SELECT SalesOfficerID FROM SalesOfficer where Email=@0", OS.SalesOfficerID) : OS.SalesOfficerID;

                db.Execute(query, OS.OrderNo
, OS.CustID
, OS.FollowUpNo
, OS.QuotationNo
, OS.MultiYearF
, OS.YearCoverage
, OS.TLOPeriod
, OS.ComprePeriod
, OS.BranchCode
, OS.SalesOfficerID
, OS.PhoneSales
, OS.DealerCode
, OS.SalesDealer
, OS.ProductTypeCode
, OS.AdminFee
, OS.TotalPremium
, OS.Phone1
, OS.Phone2
, OS.Email1
, OS.Email2
, OS.SendStatus
, OS.InsuranceType
, OS.ApplyF
, OS.SendF
, OS.LastInterestNo
, OS.LastCoverageNo
, OS.PolicySentTo
, OS.PolicyDeliveryName
, OS.PolicyDeliveryAddress
, OS.PolicyDeliveryPostalCode
, OS.VANumber
, OS.SurveyNo
, OS.PeriodFrom
, OS.PeriodTo
, OS.PolicyNo
, OS.AmountRep
, OS.PolicyID
, OS.SegmentCode);
                #endregion

                #region INSERT UPDATE ORDERSIMULATION MV
                OSMV = JsonConvert.DeserializeObject<OrderSimulationMVModel>(OSMVData);
                OSMV.OrderNo = string.IsNullOrEmpty(OSMV.OrderNo) ? OrderNo : OSMV.OrderNo;

                query = @"IF EXISTS(SELECT * FROM OrdersimulationMV WHERE OrderNo=@0)
BEGIN
    UPDATE OrdersimulationMV SET  [ObjectNo] =@1
      ,[ProductTypeCode]=@2
      ,[RowStatus]=1
      ,[VehicleCode]=@3
      ,[BrandCode]=@4
      ,[ModelCode]=@5
      ,[Series]=@6
      ,[Type]=@7
      ,[Sitting]=@8
      ,[Year]=@9
      ,[CityCode]=@10
      ,[UsageCode]=@11
      ,[SumInsured]=@12
      ,[AccessSI]=@13
      ,[RegistrationNumber]=@14
      ,[EngineNumber]=@15
      ,[ChasisNumber]=@16
      ,[LastUpdatedTime]=GETDATE()
      ,[IsNew]=@17 WHERE [OrderNo]=@0
END
        ELSE
BEGIN
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,GETDATE(),1,@17)
END";

                db.Execute(query, OSMV.OrderNo
, OSMV.ObjectNo
, OSMV.ProductTypeCode
, OSMV.VehicleCode
, OSMV.BrandCode
, OSMV.ModelCode
, OSMV.Series
, OSMV.Type
, OSMV.Sitting
, OSMV.Year
, OSMV.CityCode
, OSMV.UsageCode
, OSMV.SumInsured
, OSMV.AccessSI
, OSMV.RegistrationNumber
, OSMV.EngineNumber
, OSMV.ChasisNumber
//, OSMV.LastUpdatedTime
//, OSMV.RowStatus
, OSMV.IsNew);
                #endregion

            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        //TODO Test calculatedpremi baru
        public static void InsertExtendedCover(string OrderNo, string calculatedPremiItems,DateTime? PeriodFrom, DateTime? PeriodTo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            bool isexistSFE = false;
            try
            {
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {

                    List<OrderSimulationInterest> OSI = new List<OrderSimulationInterest>();
                    List<OrderSimulationCoverage> OSC = new List<OrderSimulationCoverage>();
                    int a = 1;
                    int year = 1;
                    CalculatePremiModel CalculatePremiModel = JsonConvert.DeserializeObject<CalculatePremiModel>(calculatedPremiItems);
                    List<CalculatedPremiItems> calculatedPremiItem = CalculatePremiModel.ListCalculatePremi;
                    OSData OS = CalculatePremiModel.OSData;
                    OSMVData OSMV = CalculatePremiModel.OSMVData;
                    OS.OrderNo = OS.OrderNo;
                    OSMV.OrderNo = OSMV.OrderNo;
                    if (calculatedPremiItem.Count > 0)
                    {
                        DateTime? pt = null;
                        foreach (CalculatedPremiItems cp in calculatedPremiItem)
                        {
                            bool isexist = false;
                            if (cp.CoverageID.Contains("SRCC") || cp.CoverageID.Contains("FLD") || cp.CoverageID.Contains("ETV"))
                            {
                                isexistSFE = true;
                            }
                            foreach (OrderSimulationInterest item in OSI)
                            {

                                if (item.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()))
                                {
                                    isexist = true;
                                    pt = null;

                                }
                            }
                            if (!isexist)
                            {
                                //get last periodfrom
                                DateTime? pf = null;
                                foreach (CalculatedPremiItems cpitem in calculatedPremiItem)
                                {
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pt == null || cpitem.PeriodTo > pt))
                                    {
                                            pt = cpitem.PeriodTo;
                                    } 
                                    if (cpitem.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && (pf == null || cpitem.PeriodFrom < pf))
                                    {
                                            pf = cpitem.PeriodFrom;
                                    }
                                }
                                OrderSimulationInterest osi = new OrderSimulationInterest();
                                DateTime periodf = Convert.ToDateTime(pf);
                                DateTime periodt = Convert.ToDateTime(pt);
                                osi.OrderNo = OS.OrderNo;
                                osi.ObjectNo = 1;
                                osi.InterestNo = a;
                                osi.InterestID = cp.InterestID;
                                osi.Year = Convert.ToString(periodt.Year - periodf.Year);//year.ToString();//Convert.ToString(cp.Year);
                                osi.RowStatus = true;
                                osi.Premium = 0;
                                osi.DeductibleCode = cp.DeductibleCode;
                                osi.PeriodFrom = cp.PeriodFrom;
                                osi.PeriodTo = pt;

                                foreach (CalculatedPremiItems cp2 in calculatedPremiItem)
                                {
                                    if (cp2.InterestID.TrimEnd().Equals(cp.InterestID.TrimEnd()) && cp2.PeriodFrom == cp.PeriodFrom && cp2.PeriodTo == cp.PeriodTo)
                                    {
                                        osi.Premium = osi.Premium + Convert.ToDecimal(cp2.Premium);
                                    }
                                }
                                OSI.Add(osi);
                                a++;
                            }
                        }
                        a = 1;
                        foreach (OrderSimulationInterest item in OSI)
                        {
                            foreach (CalculatedPremiItems item2 in calculatedPremiItem)
                            {
                                if (item2.InterestID.TrimEnd().Equals(item.InterestID.TrimEnd()))
                                {
                                    OrderSimulationCoverage oscItem = new OrderSimulationCoverage();
                                    oscItem.OrderNo = item.OrderNo;
                                    oscItem.ObjectNo = item.ObjectNo;
                                    oscItem.InterestNo = item.InterestNo;
                                    oscItem.CoverageNo = a;
                                    oscItem.CoverageID = item2.CoverageID;
                                    oscItem.Rate = Convert.ToDecimal(item2.Rate);
                                    oscItem.SumInsured = Convert.ToDecimal(item2.SumInsured);
                                    oscItem.LoadingRate = Convert.ToDecimal(item2.LoadingRate);
                                    oscItem.Loading = Convert.ToDecimal(item2.Loading);
                                    oscItem.Premium = Convert.ToDecimal(item2.Premium);
                                    oscItem.IsBundling = item2.IsBundling;
                                    oscItem.EntryPct = item2.EntryPrecentage;
                                    oscItem.Ndays = item2.Ndays;
                                    oscItem.ExcessRate = item2.ExcessRate;
                                    oscItem.CalcMethod = item2.CalcMethod;
                                    oscItem.CoverPremium = item2.CoverPremium;
                                    oscItem.GrossPremium = item2.GrossPremium;
                                    oscItem.Maxsi = item2.MaxSI;
                                    oscItem.Net1 = item2.Net1;
                                    oscItem.Net2 = item2.Net2;
                                    oscItem.Net3 = item2.Net3;
                                    oscItem.DeductibleCode = item2.DeductibleCode;
                                    oscItem.BeginDate = item2.PeriodFrom;
                                    oscItem.EndDate = item2.PeriodTo;
                                    OSC.Add(oscItem);
                                    a++;

                                }
                            }
                        }
           
                        #region DELETE UPDATE ORDERSIMULATION INTEREST
                        #region Insert

                        db.Execute("DELETE FROM OrderSimulationCoverage WHERE OrderNo=@0", OrderNo);
                        db.Execute("DELETE FROM OrderSimulationInterest WHERE OrderNo=@0", OrderNo);

                            int i = 1;
                            foreach (OrderSimulationInterest data in OSI)
                            {
                                query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[Deductible_Code]
      ,[LastUpdatedTime]
      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,GETDATE(),1)";
                                db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo, data.DeductibleCode);
                                i++;
                            }
                        
                        //                        else
                        //                        {

                        //                            #region Update
                        //                            List<OrderSimulationInterest> TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0", OSI[0].OrderNo);

                        //                            foreach (OrderSimulationInterest data in TempAllOSI)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in OSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) && data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                        query = @"UPDATE OrderSimulationInterest SET 
                        //                                Premium=@5,
                        //                                LastUpdatedTime=GETDATE()
                        //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4 AND Deductible_Code=@6";
                        //                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year, data2.Premium, data.DeductibleCode);
                        //                                    }
                        //                                }

                        //                                if (!isExist)
                        //                                {
                        //                                    //soft delete coverage

                        //                                    query = @"DELETE OrderSimulationCoverage " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 ";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo);


                        //                                    //soft delete interest
                        //                                    query = @"DELETE OrderSimulationInterest " +
                        //                                    //SET 
                        //                                    //RowStatus=0,
                        //                                    //LastUpdatedTime=GETDATE()
                        //                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion
                        #endregion

                        //                                }
                        //                            }
                        //                            //merge data from new collection to old collection
                        //                            //insert new data if not exist in interestItems

                        //                            TempAllOSI = db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);
                        //                            List<OrderSimulationInterest> OSI2 = OSI;//db.Fetch<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND RowStatus=0",OSI[0].OrderNo);
                        //                            foreach (OrderSimulationInterest data in OSI2)
                        //                            {
                        //                                bool isExist = false;
                        //                                foreach (OrderSimulationInterest data2 in TempAllOSI)
                        //                                {
                        //                                    if (data.InterestID.TrimEnd().Equals(data2.InterestID.TrimEnd()) &&
                        //                           data.Year.TrimEnd().Equals(data2.Year.TrimEnd()))
                        //                                    {
                        //                                        isExist = true;
                        //                                    }
                        //                                }
                        //                                if (!isExist)
                        //                                {
                        //                                    //check if exist in deleted Collection
                        //                                    //if exist, update rowstate from D to U
                        //                                    //                                OrderSimulationInterest osiDeletedItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestID=@2 AND Year=@3 AND RowStatus=0", data.OrderNo, data.ObjectNo, data.InterestID, data.Year);

                        //                                    //                                if (osiDeletedItem != null)
                        //                                    //                                {
                        //                                    //                                    query = @"UPDATE OrderSimulationInterest SET 
                        //                                    //                                RowStatus=1,
                        //                                    //                                LastUpdatedTime=GETDATE()
                        //                                    //                                WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND InterestID=@3 AND Year=@4";
                        //                                    //                                    db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.InterestID, data.Year);

                        //                                    //                                }
                        //                                    //                                else
                        //                                    //                                {
                        //                                    int i = db.ExecuteScalar<int>("SELECT max(InterestNo) FROM OrderSimulationInterest WHERE OrderNo=@0", data.OrderNo);
                        //                                    i++;
                        //                                    query = @"INSERT INTO OrderSimulationInterest ([OrderNo]
                        //      ,[ObjectNo]
                        //      ,[InterestNo]
                        //      ,[InterestID]
                        //      ,[Year]
                        //      ,[Premium]
                        //      ,[PeriodFrom]
                        //      ,[PeriodTo]
                        //      ,[LastUpdatedTime]
                        //      ,[RowStatus]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1)";
                        //                                    db.Execute(query, data.OrderNo, data.ObjectNo, i, data.InterestID, data.Year, data.Premium, data.PeriodFrom, data.PeriodTo);

                        //                                    // }
                        //                                }
                        //                            }
                        //                            #endregion
                        //                        }
                        #endregion   
                        
                        #region DELETE UPDATE ORDERSIMULATION COVERAGE

                        #region Insert
                      //  OrderSimulationCoverage countC = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);
                      
                            i = 1;
                            foreach (OrderSimulationCoverage data in OSC)
                            {
                                query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling] 
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
                                db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, i, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
                                    data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);
                                i++;
                            
                        }
//                        else
//                        {

//                            #region Update
//                            List<OrderSimulationCoverage> TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0", OSI[0].OrderNo);

//                            foreach (OrderSimulationCoverage data in TempAllOSC)
//                            {

//                                OrderSimulationInterest osiItem = db.FirstOrDefault<OrderSimulationInterest>("SELECT * FROM OrderSimulationInterest WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2", data.OrderNo, data.ObjectNo, data.InterestNo);
//                                if (osiItem != null)
//                                {
//                                    bool isExist = false;
//                                    foreach (OrderSimulationCoverage item in OSC)
//                                    {
//                                        if (item.CoverageID.TrimEnd().Equals(data.CoverageID.TrimEnd()) && item.InterestNo.Equals(data.InterestNo) && item.CoverageNo.Equals(data.CoverageNo))
//                                        {
//                                            isExist = true;
//                                            query = @"UPDATE OrderSimulationCoverage SET 
//      [Rate]=@5
//      ,[SumInsured]=@6
//      ,[LoadingRate]=@7
//      ,[Loading]=@8
//      ,[Premium]=@9
//      ,[BeginDate]=@10
//      ,[EndDate]=@11
//      ,[LastUpdatedTime]=GETDATE()
//      ,[RowStatus]=1
//      ,[IsBundling]=@12
//      ,[Entry_Pct]=@13
//      ,[Ndays]=@14
//      ,[Excess_Rate]=@15
//      ,[Calc_Method]=@16
//      ,[Cover_Premium]=@17
//      ,[Gross_Premium]=@18
//      ,[Max_si]=@19
//      ,[Net1]=@20
//      ,[Net2]=@21
//      ,[Net3]=@22
//      ,[Deductible_Code]=@23 WHERE [OrderNo]=@0 AND [ObjectNo]=@1 AND [InterestNo]=@2 AND [CoverageNo] =@3 AND [CoverageID]=@4";
//                                            db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID, data.Rate, data.SumInsured, data.LoadingRate, data.Loading, data.Premium, data.BeginDate, data.EndDate, data.IsBundling ? 1 : 0,
//                                            data.EntryPct, data.Ndays, data.ExcessRate, data.CalcMethod, data.CoverPremium, data.GrossPremium, data.Maxsi, data.Net1, data.Net2, data.Net3, data.DeductibleCode);

//                                        }
//                                    }
//                                    //delete data
//                                    if (!isExist)
//                                    {
//                                        //soft delete coverage

//                                        query = @"DELETE OrderSimulationCoverage " +
//                                    //SET 
//                                    //RowStatus=0,
//                                    //LastUpdatedTime=GETDATE()
//                                    "WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageId=@4";
//                                        db.Execute(query, data.OrderNo, data.ObjectNo, data.InterestNo, data.CoverageNo, data.CoverageID);


//                                    }
//                                }

//                            }
//                            //merge data from new collection to old collection
//                            //insert new data if not exist in interestItems
//                            TempAllOSC = db.Fetch<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND RowStatus=1", OSI[0].OrderNo);

//                            foreach (OrderSimulationCoverage item in OSC)
//                            {
//                                bool isExist = false;
//                                foreach (OrderSimulationCoverage data in TempAllOSC)
//                                {
//                                    if (data.InterestNo.Equals(item.InterestNo) &&
//                           data.CoverageNo.Equals(item.CoverageNo) && data.CoverageID.TrimEnd().Equals(item.CoverageID.TrimEnd()))
//                                    {
//                                        isExist = true;
//                                    }
//                                }
//                                if (isExist)
//                                {
//                                    OrderSimulationCoverage oscitem = db.FirstOrDefault<OrderSimulationCoverage>("SELECT * FROM OrderSimulationCoverage WHERE OrderNo=@0 AND ObjectNo=@1 AND InterestNo=@2 AND CoverageNo=@3 AND CoverageID=@4", item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.CoverageID);
//                                    if (oscitem != null)
//                                    {
//                                        query = @"UPDATE OrderSimulationCoverage SET 
//                                RowStatus=1
//                                ,Rate=@4
//                                ,SumInsured=@5
//                                ,LoadingRate=@6
//                                ,Loading=@7
//                                ,Premium=@8
//                                ,IsBundling=@9     
//                                ,LastUpdatedTime=GETDATE()  
//                                ,[Entry_Pct]=@10
//                                ,[Ndays]=@11
//                                ,[Excess_Rate]=@12
//                                ,[Calc_Method]=@13
//                                ,[Cover_Premium]=@14
//                                ,[Gross_Premium]=@15
//                                ,[Max_si]=@16
//                                ,[Net1]=@17
//                                ,[Net2]=@18
//                                ,[Net3]=@19
//                                ,[Deductible_Code]=@20                      
//WHERE OrderNo=@0 AND ObjectNo = @1 AND InterestNo=@2 AND CoverageNo=@3";
//                                        db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, item.CoverageNo, item.Rate, item.SumInsured, item.LoadingRate, item.LoadingRate, item.Premium, item.IsBundling ? 1 : 0,
//                                             item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
//                                    }
//                                }
//                                else
//                                {

//                                    int i = db.ExecuteScalar<int>("SELECT max(CoverageNo) FROM OrderSimulationCoverage WHERE OrderNo=@0 ", item.OrderNo);
//                                    i++;
//                                    query = @"INSERT INTO OrderSimulationCoverage ([OrderNo]
//      ,[ObjectNo]
//      ,[InterestNo]
//      ,[CoverageNo]
//      ,[CoverageID]
//      ,[Rate]
//      ,[SumInsured]
//      ,[LoadingRate]
//      ,[Loading]
//      ,[Premium]
//      ,[BeginDate]
//      ,[EndDate]
//      ,[LastUpdatedTime]
//      ,[RowStatus]
//      ,[IsBundling]    
//      ,[Entry_Pct]
//      ,[Ndays]
//      ,[Excess_Rate]
//      ,[Calc_Method]
//      ,[Cover_Premium]
//      ,[Gross_Premium]
//      ,[Max_si]
//      ,[Net1]
//      ,[Net2]
//      ,[Net3]
//      ,[Deductible_Code])) VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),1,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22,@23)";
//                                    db.Execute(query, item.OrderNo, item.ObjectNo, item.InterestNo, i, item.CoverageID, item.Rate, item.SumInsured, item.LoadingRate, item.Loading, item.Premium, item.BeginDate, item.EndDate, item.IsBundling ? 1 : 0,
//                                                      item.EntryPct, item.Ndays, item.ExcessRate, item.CalcMethod, item.CoverPremium, item.GrossPremium, item.Maxsi, item.Net1, item.Net2, item.Net3, item.DeductibleCode);
//                                    i++;
//                                }


//                            }
//                            #endregion
//                        }
                        #endregion

                        #region UPDATE ORDERSIMULATION
                        OS.LastInterestNo = db.ExecuteScalar<int>("SELECT MAX(InterestNo) From OrderSimulationInterest Where OrderNo=@0", OS.OrderNo);
                        OS.LastCoverageNo = db.ExecuteScalar<int>("SELECT MAX(CoverageNo) From OrderSimulationCoverage Where OrderNo=@0", OS.OrderNo);
                        OS.YearCoverage = OS.ComprePeriod + OS.TLOPeriod;
                        OS.TotalPremium = db.ExecuteScalar<decimal>("SELECT COALESCE((SELECT SUM(Premium) FROM OrderSimulationCoverage where OrderNo=@0)+(SELECT AdminFee FROM OrderSimulation where OrderNo=@0),0)", OS.OrderNo);
                        if (OS.YearCoverage == 0)
                        {
                            DateTime zeroTime = new DateTime(1, 1, 1);

                            DateTime start = PeriodFrom ?? DateTime.Now;
                            DateTime end = PeriodTo ?? DateTime.Now;

                            TimeSpan span = end - start;
                            // Because we start at year 1 for the Gregorian
                            // calendar, we must subtract a year here.
                            OS.YearCoverage = (zeroTime + span).Year ;

                        }
                        query = @"UPDATE OrderSimulation SET 
                        ProductTypeCode=@1
                        ,InsuranceType=@2
                        ,ProductCode=@3
                        ,AdminFee=@4
                        ,MultiYearF=@5
                        ,YearCoverage=@6
                        ,ComprePeriod=@7
                        ,TLOPeriod=@8
                        ,TotalPremium=@9
                        ,LastInterestNo=@10
                        ,LastCoverageNo=@11
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OS.OrderNo, OS.ProductTypeCode, OS.InsuranceType, OS.ProductCode, OS.AdminFee, OS.MultiYearF, OS.YearCoverage, OS.ComprePeriod, OS.TLOPeriod, OS.TotalPremium, OS.LastInterestNo, OS.LastCoverageNo);
                        #endregion

                        #region UPDATE ORDERSIMULATIONMV
                        query = @"UPDATE OrderSimulationMV SET 
                        ProductTypeCode=@1
                        ,SumInsured=@2
                        ,AccessSI=@3
                        ,LastUpdatedTime=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OSMV.OrderNo, OSMV.ProductTypeCode, OSMV.SumInsured, OSMV.AccessSI);
                        #endregion
                    }
                }
            }
            catch (Exception e)
            {
                InsertExtendedCover(OrderNo,calculatedPremiItems,PeriodFrom,PeriodTo);
            }
        }
        public static string GenerateGuid(int TypeGuid)
        {
            bool isexist= false;
            string guid = System.Guid.NewGuid().ToString();
            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    switch (TypeGuid)
                    {
                        case GenGuid.OrderNo:
                            isexist = db.ExecuteScalar<int>("SELECT COUNT(*) FROM Ordersimulation WHERE OrderNo=@0", guid) > 0 ? true : false;
                            break;
                        case GenGuid.FollowUpNo:
                            isexist = db.ExecuteScalar<int>("SELECT COUNT(*) FROM FollowUp WHERE FollowUpNo=@0", guid) > 0 ? true : false;
                            break;
                        case GenGuid.CustId:
                            isexist = db.ExecuteScalar<int>("SELECT COUNT(*) FROM ProspectCustomer WHERE custid=@0", guid) > 0 ? true : false;
                            break;
                        case GenGuid.ImageID:
                            isexist = db.ExecuteScalar<int>("SELECT COUNT(*) FROM ImageData WHERE ImageID=@0", guid) > 0 ? true : false;
                            isexist = db.ExecuteScalar<int>("SELECT COUNT(*) FROM TempImageData WHERE ImageID=@0", guid) > 0 ? true : false;
                            break;
                    }
                    }
                if (isexist)
                {
                   return GenerateGuid(TypeGuid);
                }
                else
                {
                    return guid;
                }
            }catch(Exception e){
                throw (e);

            }
        }
        public static A2isMessagingAttachment GenerateReportFile(string orderNo, int insuranceType)
        {

            A2isMessagingAttachment att = new A2isMessagingAttachment();
            string filePath = "";
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportServerURLBeyond = ConfigurationManager.AppSettings["ReportServerURLBeyond"].ToString();
            string ReportPathOne = ConfigurationManager.AppSettings["ReportPathOne"].ToString();
            string ReportPathTinsOne = ConfigurationManager.AppSettings["ReportPathTinsOne"].ToString();
            string ReportPathLexusOne = ConfigurationManager.AppSettings["ReportPathLexusOne"].ToString();
            string ReportPathTwo = ConfigurationManager.AppSettings["ReportPathTwo"].ToString();
            string ReportPathTinsTwo = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusTwo = ConfigurationManager.AppSettings["ReportPathLexusTwo"].ToString();
            string ReportPathThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathTinsThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusThree = ConfigurationManager.AppSettings["ReportPathLexusThree"].ToString();
            string CertificateQuotation = ConfigurationManager.AppSettings["CertificateQuotation"].ToString();
            string CertificateRenewalNotice = ConfigurationManager.AppSettings["CertificateRenewalNotice"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;
            string CredentialsRenewalUsername = ConfigurationManager.AppSettings["CredentialsRenewalUsername"].ToString(); ;
            string CredentialsRenewalPassword = ConfigurationManager.AppSettings["CredentialsRenewalPassword"].ToString(); ;
            string CredentialsRenewalDomain = ConfigurationManager.AppSettings["CredentialsRenewalDomain"].ToString(); ;

            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";
            int idReportRennot = 0;

            #region Insert Data to RPT_Renewal_Notice Dan RPT_Renewal_Notice_Detail
            string qReport = @"SELECT OldPolicyNo,SalesOfficerID FROM OrderSimulation WHERE OrderNo = @0 ANd RowStatus = 1";
            dynamic policyNo = "";
            using (a2isDBHelper.Database dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                policyNo = dbMobile.FirstOrDefault<dynamic>(qReport, orderNo);
            }
            if (!String.IsNullOrEmpty(Convert.ToString(policyNo.OldPolicyNo)))
            {
                qReport = @";EXECUTE [dbo].[usp_InsertPrintRenewalNoticeOtosales] @0, @1";
                using (a2isDBHelper.Database dbAAB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    idReportRennot = dbAAB.FirstOrDefault<int>(qReport, Convert.ToString(policyNo.OldPolicyNo), Convert.ToString(policyNo.SalesOfficerID));
                    MobileRepository.InsertPrintRennotExtended(idReportRennot, orderNo);
                }
            }
            else
            {

                MobileRepository.InsertQuotationPrintTable(orderNo);
            }
            #endregion
            DownloadQuotationResult result = new DownloadQuotationResult();
            NetworkCredential NetCredential = new NetworkCredential();
            try
            {
                byte[] bytes;
                Warning[] warnings;
                string mimeType;
                string encoding;
                string filenameExtension;
                string[] streamids;
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                int yearCoverage;

                string query = @"";

                query = @"SELECT o.PolicyNo, p.Name, o.InsuranceType, o.YearCoverage, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f 
                        ON f.CustID = p.CustID
                        INNER JOIN dbo.OrderSimulation o
                        ON o.FollowUpNo = f.FollowUpNo
                        INNER JOIN dbo.OrderSimulationMV omv
                        ON omv.OrderNo = o.OrderNo
                        WHERE o.OrderNo = @0";
                List<dynamic> dt = db.Fetch<dynamic>(query, orderNo);
                if (dt.Count > 0)
                {
                    yearCoverage = Convert.ToInt32(dt.First().YearCoverage);
                    insuranceType = Convert.ToInt32(dt.First().InsuranceType);
                    using (ReportViewer rView = new ReportViewer())
                    {
                        rView.ProcessingMode = ProcessingMode.Remote;

                        ServerReport serverReport = rView.ServerReport;

                        //switch (yearCoverage)
                        //{
                        //    case 1:
                        //        switch (insuranceType)
                        //        {
                        //            case 1: serverReport.ReportPath = ReportPathOne; break;
                        //            case 2: serverReport.ReportPath = ReportPathTinsOne; break;
                        //            case 3: serverReport.ReportPath = ReportPathLexusOne; break;
                        //        }; break;
                        //    case 2:
                        //        switch (insuranceType)
                        //        {
                        //            case 1: serverReport.ReportPath = ReportPathTwo; break;
                        //            case 2: serverReport.ReportPath = ReportPathTinsTwo; break;
                        //            case 3: serverReport.ReportPath = ReportPathLexusTwo; break;
                        //        }; break;
                        //    case 3:
                        //        switch (insuranceType)
                        //        {
                        //            case 1: serverReport.ReportPath = ReportPathThree; break;
                        //            case 2: serverReport.ReportPath = ReportPathTinsThree; break;
                        //            case 3: serverReport.ReportPath = ReportPathLexusThree; break;
                        //        }; break;
                        //}
                        if (!dt.First().IsRenewal)
                        {
                            serverReport.ReportServerUrl = new Uri(ReportServerURL);
                            serverReport.ReportPath = CertificateQuotation;
                            NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);

                      
                      }
                        else
                        {
                            serverReport.ReportServerUrl = new Uri(ReportServerURLBeyond);
                            serverReport.ReportPath = CertificateRenewalNotice;
                            NetCredential = new NetworkCredential(CredentialsRenewalUsername, CredentialsRenewalPassword, CredentialsRenewalDomain);
  }

                    serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;

                    #region Catat Print Quotation
                    query = @"INSERT INTO dbo.PrintQuotationHistoryLog
                                        ( OrderNo, RowStatus, EntryDate )
                                VALUES  ( @0, -- OrderNo - varchar(100)
                                          1, -- RowStatus - smallint
                                          GETDATE()  -- EntryDate - datetime
                                          )";
                    db.Execute(query, orderNo);
                    #endregion
                    ReportParameter[] parameters = new ReportParameter[1];
                    ReportParameter reportParam = new ReportParameter();
                    if (!dt.First().IsRenewal)
                    {
                        reportParam.Name = "OrderNo";
                        reportParam.Values.Add(orderNo);
                    }
                    else
                    {

                        reportParam.Name = "PolicyNo";
                        reportParam.Values.Add(Convert.ToString(policyNo.OldPolicyNo));
                    }
                    parameters[0] = reportParam;
                    rView.ServerReport.SetParameters(parameters);
                    bytes = rView.ServerReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

                    #region filename
                    int cetakanke = db.ExecuteScalar<int>("SELECT COUNT(*) FROM dbo.PrintQuotationHistoryLog WHERE OrderNo=@0", orderNo) + 1;
               
                    string fileName = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='ATTFILENAME' AND ParamType='TMP' AND ParamValue2=@0", Convert.ToInt16(dt.First().IsRenewal));
                    fileName = fileName.Replace("@CetakanKe", cetakanke.ToString()).Replace("@TanggalCetak", DateTime.Now.ToString("dd-MM-yyyy"));
                    if (dt.First().IsRenewal)
                    {
                        fileName = fileName.Replace("@NoPolis", dt.First().PolicyNo.Trim());

                    }
                    #endregion
                    filePath = ReportFiles + fileName + ".pdf"; //"QUOTATION-" + OrderNo + ".pdf";

                    if (File.Exists(filePath))
                        File.Delete(filePath);
                    // 1. Read lock information from file   2. If locked by us, delete the file
                    //using (FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Delete))
                    //{
                    //    File.Delete(filePath);
                    //}

                    //using (FileStream fs = File.Create(filePath)) { }

                    att.FileByte = bytes;
                    att.FileContentType = "application/pdf";
                    att.FileExtension = ".pdf";
                    att.AttachmentDescription = fileName;
                }
              }
            }
            catch (Exception ex)
            {
                filePath = "";
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();

            }

            return att;
        }

        public static void UpdateImageData(List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, string CustID, string FollowUpNo)
        {
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                var img = db.Fetch<dynamic>(@"SELECT SPPAKB,FAKTUR,KonfirmasiCust,DocNSA1,DocNSA2,DocNSA3,DocNSA4,DocNSA5,IdentityCard,STNK,
                                                        BSTB1, DocRep, BuktiBayar, DocPendukung, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5
                                                        FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1", CustID, FollowUpNo);
                string SPPAKB = "";
                string FAKTUR = "";
                string KonfirmasiCust = "";
                string DocNSA1 = "";
                string DocNSA2 = "";
                string DocNSA3 = "";
                string DocNSA4 = "";
                string DocNSA5 = "";
                string IdentityCard = "";
                string STNK = "";
                string BSTB = "";
                string DocRep = "";
                string BuktiBayar = "";
                string BuktiBayar2 = "";
                string BuktiBayar3 = "";
                string BuktiBayar4 = "";
                string BuktiBayar5 = "";
                string DocPendukung = "";
                if (img.Count > 0)
                {
                    SPPAKB = Convert.ToString(img.First().SPPAKB);
                    FAKTUR = Convert.ToString(img.First().FAKTUR);
                    KonfirmasiCust = Convert.ToString(img.First().KonfirmasiCust);
                    DocNSA1 = Convert.ToString(img.First().DocNSA1);
                    DocNSA2 = Convert.ToString(img.First().DocNSA2);
                    DocNSA3 = Convert.ToString(img.First().DocNSA3);
                    DocNSA4 = Convert.ToString(img.First().DocNSA4);
                    DocNSA5 = Convert.ToString(img.First().DocNSA5);
                    IdentityCard = Convert.ToString(img.First().IdentityCard);
                    STNK = Convert.ToString(img.First().STNK);
                    BSTB = Convert.ToString(img.First().BSTB1);
                    DocRep = Convert.ToString(img.First().DocRep);
                    BuktiBayar = Convert.ToString(img.First().BuktiBayar);
                    BuktiBayar2 = Convert.ToString(img.First().BuktiBayar2);
                    BuktiBayar3 = Convert.ToString(img.First().BuktiBayar3);
                    BuktiBayar4 = Convert.ToString(img.First().BuktiBayar4);
                    BuktiBayar5 = Convert.ToString(img.First().BuktiBayar5);
                    DocPendukung = Convert.ToString(img.First().DocPendukung);
                }
                img = db.Fetch<dynamic>(@"SELECT NPWP, SIUP
                                                    FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1",
                                                CustID, FollowUpNo);
                string NPWP = "";
                string SIUP = "";
                if (img.Count > 0)
                {
                    NPWP = Convert.ToString(img.First().NPWP);
                    SIUP = Convert.ToString(img.First().SIUP);
                }
                string qUpdateImgData = @";IF EXISTS (SELECT * FROM dbo.ImageData WHERE PathFile = @0)
                                            BEGIN 
	                                            UPDATE dbo.ImageData SET RowStatus = 0
	                                            WHERE PathFile = @0
                                            END";
                foreach (Otosales.Models.vWeb2.ImageDataTaskDetail id in ListImgData)
                {
                    if (id.ImageType.ToUpper().Equals("SPPAKB"))
                    {
                        if (!string.IsNullOrEmpty(SPPAKB))
                        {
                            if (!SPPAKB.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, SPPAKB);
                            }
                        }
                        SPPAKB = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("FAKTUR"))
                    {
                        if (!string.IsNullOrEmpty(FAKTUR))
                        {
                            if (!FAKTUR.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, FAKTUR);
                            }
                        }
                        FAKTUR = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("KONFIRMASICUST"))
                    {
                        if (!string.IsNullOrEmpty(KonfirmasiCust))
                        {
                            if (!KonfirmasiCust.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, KonfirmasiCust);
                            }
                        }
                        KonfirmasiCust = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("DOCNSA1"))
                    {
                        if (!string.IsNullOrEmpty(DocNSA1))
                        {
                            if (!DocNSA1.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, DocNSA1);
                            }
                        }
                        DocNSA1 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("DOCNSA2"))
                    {
                        if (!string.IsNullOrEmpty(DocNSA2))
                        {
                            if (!DocNSA2.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, DocNSA2);
                            }
                        }
                        DocNSA2 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("DOCNSA3"))
                    {
                        if (!string.IsNullOrEmpty(DocNSA3))
                        {
                            if (!DocNSA3.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, DocNSA3);
                            }
                        }
                        DocNSA3 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("DOCNSA4"))
                    {
                        if (!string.IsNullOrEmpty(DocNSA4))
                        {
                            if (!DocNSA4.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, DocNSA4);
                            }
                        }
                        DocNSA4 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("DOCNSA5"))
                    {
                        if (!string.IsNullOrEmpty(DocNSA5))
                        {
                            if (!DocNSA5.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, DocNSA5);
                            }
                        }
                        DocNSA5 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("IDENTITYCARD"))
                    {
                        if (!string.IsNullOrEmpty(IdentityCard))
                        {
                            if (!IdentityCard.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, IdentityCard);
                            }
                        }
                        IdentityCard = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("STNK"))
                    {
                        if (!string.IsNullOrEmpty(STNK))
                        {
                            if (!STNK.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, STNK);
                            }
                        }
                        STNK = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("BSTB"))
                    {
                        if (!string.IsNullOrEmpty(BSTB))
                        {
                            if (!BSTB.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, BSTB);
                            }
                        }
                        BSTB = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("DOCREP"))
                    {
                        if (!string.IsNullOrEmpty(DocRep))
                        {
                            if (!DocRep.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, DocRep);
                            }
                        }
                        DocRep = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("NPWP"))
                    {
                        if (!string.IsNullOrEmpty(NPWP))
                        {
                            if (!NPWP.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, NPWP);
                            }
                        }
                        NPWP = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("SIUP"))
                    {
                        if (!string.IsNullOrEmpty(SIUP))
                        {
                            if (!SIUP.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, SIUP);
                            }
                        }
                        SIUP = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR"))
                    {
                        if (!string.IsNullOrEmpty(BuktiBayar))
                        {
                            if (!BuktiBayar.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, BuktiBayar);
                            }
                        }
                        BuktiBayar = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("DOCPENDUKUNG"))
                    {
                        if (!string.IsNullOrEmpty(DocPendukung))
                        {
                            if (!DocPendukung.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, DocPendukung);
                            }
                        }
                        DocPendukung = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR2"))
                    {
                        if (!string.IsNullOrEmpty(BuktiBayar2))
                        {
                            if (!BuktiBayar2.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, BuktiBayar2);
                            }
                        }
                        BuktiBayar2 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR3"))
                    {
                        if (!string.IsNullOrEmpty(BuktiBayar3))
                        {
                            if (!BuktiBayar3.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, BuktiBayar3);
                            }
                        }
                        BuktiBayar3 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR4"))
                    {
                        if (!string.IsNullOrEmpty(BuktiBayar4))
                        {
                            if (!BuktiBayar4.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, BuktiBayar4);
                            }
                        }
                        BuktiBayar4 = id.PathFile;
                    }
                    if (id.ImageType.ToUpper().Equals("BUKTIBAYAR5"))
                    {
                        if (!string.IsNullOrEmpty(BuktiBayar5))
                        {
                            if (!BuktiBayar5.Trim().Equals(id.PathFile.Trim()))
                            {
                                db.Execute(qUpdateImgData, BuktiBayar5);
                            }
                        }
                        BuktiBayar5 = id.PathFile;
                    }
                    #region SAVE IMAGE
                    query = @";
SELECT [ImageID]
      ,[FollowUpNo]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                    List<dynamic> ImageData = db.Fetch<dynamic>(query, FollowUpNo);
                    foreach (dynamic imd in ImageData)
                    {
                        query = @"IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,1,@6,@7)
END";
                        db.Execute(query, imd.ImageID, imd.DeviceID
  , imd.SalesOfficerID
  , imd.PathFile
  , imd.EntryDate
  , imd.LastUpdatedTime
  , imd.Data
  , imd.ThumbnailData);
                        db.Execute("UPDATE dbo.TempImageData SET RowStatus=0 WHERE FollowUpNo=@0 AND PathFile=@1", FollowUpNo, imd.PathFile);
                    }
                    #endregion
                }
                query = @"IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.FollowUp SET SPPAKB = @2, FAKTUR = @3, KonfirmasiCust = @4,
	                            DocNSA1 = @5, DocNSA2 = @6, DocNSA3 = @7, IdentityCard = @8, STNK = @9,
                                BSTB1 = @10, DocRep = @11, BuktiBayar = @12, DocPendukung = @13
                                , BuktiBayar2 = @14, BuktiBayar3 = @15, BuktiBayar4 = @16, BuktiBayar5 = @17
                                , DocNSA4 = @18, DocNSA5 = @19
	                            WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
//                query = @"IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
//                            BEGIN
//	                            UPDATE dbo.FollowUp SET SPPAKB = @2, FAKTUR = @3, KonfirmasiCust = @4,
//	                            DocNSA1 = @5, DocNSA2 = @6, DocNSA3 = @7, IdentityCard = @8, STNK = @9,
//                                BSTB1 = @10, DocRep = @11, BuktiBayar = @12, DocPendukung = @13
//	                            WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
//                            END";

                db.Execute(query, CustID, FollowUpNo
                    , SPPAKB, FAKTUR, KonfirmasiCust
                    , DocNSA1, DocNSA2, DocNSA3, IdentityCard, STNK
                    , BSTB, DocRep, BuktiBayar, DocPendukung
                    , BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5);

                if (!string.IsNullOrEmpty(NPWP) || !string.IsNullOrEmpty(SIUP))
                {
                    query = @"UPDATE dbo.ProspectCompany 
                                SET NPWP = @0, SIUP = @1
                                WHERE CustID = @2 AND FollowUpNo = @3";
                    db.Execute(query, NPWP, SIUP, CustID, FollowUpNo);
                }

                string qGetBktByr = @";SELECT BuktiBayar, Data, PolicyOrderNo, CoreImage_ID, ImageID
                                    FROM dbo.FollowUp f
                                    INNER JOIN dbo.ImageData id ON f.BuktiBayar = id.PathFile
                                    INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
                                    WHERE f.FollowUpNo = @0 AND
                                    (CoreImage_ID = '' OR CoreImage_ID IS NULL)
                                    AND f.RowStatus = 1
                                    UNION
                                    SELECT BuktiBayar2, Data, PolicyOrderNo, CoreImage_ID, ImageID
                                    FROM dbo.FollowUp f
                                    INNER JOIN dbo.ImageData id ON f.BuktiBayar2 = id.PathFile
                                    INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
                                    WHERE f.FollowUpNo = @0 AND
                                    (CoreImage_ID = '' OR CoreImage_ID IS NULL)
                                    AND f.RowStatus = 1
                                    UNION
                                    SELECT BuktiBayar3, Data, PolicyOrderNo, CoreImage_ID, ImageID
                                    FROM dbo.FollowUp f
                                    INNER JOIN dbo.ImageData id ON f.BuktiBayar3 = id.PathFile
                                    INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
                                    WHERE f.FollowUpNo = @0 AND
                                    (CoreImage_ID = '' OR CoreImage_ID IS NULL)
                                    AND f.RowStatus = 1
                                    UNION
                                    SELECT BuktiBayar4, Data, PolicyOrderNo, CoreImage_ID, ImageID
                                    FROM dbo.FollowUp f
                                    INNER JOIN dbo.ImageData id ON f.BuktiBayar4 = id.PathFile
                                    INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
                                    WHERE f.FollowUpNo = @0 AND
                                    (CoreImage_ID = '' OR CoreImage_ID IS NULL)
                                    AND f.RowStatus = 1
                                    UNION
                                    SELECT BuktiBayar5, Data, PolicyOrderNo, CoreImage_ID, ImageID
                                    FROM dbo.FollowUp f
                                    INNER JOIN dbo.ImageData id ON f.BuktiBayar5 = id.PathFile
                                    INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = f.FollowUpNo
                                    WHERE f.FollowUpNo = @0 AND
                                    (CoreImage_ID = '' OR CoreImage_ID IS NULL)
                                    AND f.RowStatus = 1";
                List<dynamic> bktbyr = db.Fetch<dynamic>(qGetBktByr, FollowUpNo);
                if (bktbyr.Count > 0)
                {
                    foreach (dynamic item in bktbyr)
                    {
                        string outImageID = null;
                        string referenceImageType = Otosales.Logic.ImageLogic.GetMappedReferenceTypeImage("BuktiBayar", false);
                        if (!string.IsNullOrEmpty(item.PolicyOrderNo))
                        {
                            if (Otosales.Logic.ImageLogic.UploadImage(item.Data, item.BuktiBayar, referenceImageType, 1, "OTOSL", item.PolicyOrderNo, out outImageID) && outImageID != null)
                            {
                                Otosales.Logic.ImageLogic.UpdateImageData(item.ImageID, outImageID);
                            }
                        }                        
                    }
                    Otosales.Logic.ImageLogic.DeleteUnusedImages(FollowUpNo);
                }
                else {
                    Otosales.Logic.ImageLogic.DeleteUnusedImages(FollowUpNo);                
                }
                updateFollowUpStatusToPolicyCreated(FollowUpNo);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        //public static string GetMappedReferenceTypeImage(string imageTypeOtosales)
        //{
        //    switch (imageTypeOtosales)
        //    {
        //        case "IdentityCard": return ReferenceTypeImage.KTP;
        //        case "STNK": return ReferenceTypeImage.STNK;
        //        case "SPPAKB": return ReferenceTypeImage.SPPAKB;
        //        case "BSTB1": return ReferenceTypeImage.BSTB;
        //        case "NPWP": return ReferenceTypeImage.NPWP;
        //        case "SIUP": return ReferenceTypeImage.SIUP;
        //        case "Faktur": return ReferenceTypeImage.FAKTUR;
        //        case "KonfirmasiCust": return ReferenceTypeImage.CONFIRMATIONCUSTOMER;
        //        case "DocNSA1": return ReferenceTypeImage.NSA;
        //        case "DocNSA2": return ReferenceTypeImage.NSA;
        //        case "DocNSA3": return ReferenceTypeImage.NSA;
        //        case "DocRep": return ReferenceTypeImage.DOCREP;
        //        case "DocPendukung": return ReferenceTypeImage.DOCPENDUKUNG;

        //        default: return string.Empty;
        //    }
        //}

        #endregion
        public static void InsertMstOrderMobile(FollowUp FU)
        {
            try
            {
                #region insert to MST_ORDER_MOBILE

                if (FU.FollowUpStatus == 2 && (FU.FollowUpInfo == 61 || FU.FollowUpInfo==57 )) //Status Send To SA dan FollowUpPembayaran
                {

                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        using (var db2 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                        {
                            string OrderNo = db2.ExecuteScalar<string>("SELECT PolicyOrderNo FROM OrderSimulation WHERE FollowUpNo=@0", FU.FollowUpNo);
                            if (!string.IsNullOrEmpty(OrderNo))
                            {
                                string prospectId = db2.ExecuteScalar<string>("SELECT prospectId FROM ProspectCustomer WHERE CustId=@0", FU.CustID);
                                string PolicyOrderNo = db2.ExecuteScalar<string>("SELECT PolicyOrderNo FROM OrderSimulation WHERE CustId=@0 AND FollowUpNo=@1", FU.CustID,FU.FollowUpNo);
                                int SAState = 1;
                                if (FU.FollowUpInfo==57 && Convert.ToBoolean(FU.IsRenewal) && (string.IsNullOrEmpty(FU.DocNSA1) && string.IsNullOrEmpty(FU.DocNSA2) && string.IsNullOrEmpty(FU.DocNSA3)
                                    && string.IsNullOrEmpty(FU.DocNSA4) && string.IsNullOrEmpty(FU.DocNSA5)))
                                {
                                    SAState = 3;
                                }
                                string entryusr = FU.SalesOfficerID.Length > 5 ? "OTOSL" : FU.SalesOfficerID;
                                db.Execute(@"INSERT INTO [AAB].[dbo].[Mst_Order_Mobile] (Order_No,SA_State,Approval_Status,Approval_Process,Approval_Type,isSO,Remarks,Actual_Date,EntryDt,EntryUsr) VALUES(@0,@1,0,0,0,0,@2,GETDATE(),getdate(),@3)
                                    ", OrderNo,SAState, FU.RemarkToSA, entryusr);
                                //RetailRepository.CopyImageDataToMstImage(FU.FollowUpNo, prospectId, "");
                                //RetailRepository.CopyImageDataToMstImage(FU.FollowUpNo, "", PolicyOrderNo);
                                string transOrderNo = db2.ExecuteScalar<string>("SELECT OrderNo FROM OrderSimulation WHERE FollowUpNo=@0", FU.FollowUpNo);
                                Otosales.Logic.OrdLogic.UpdateAABOrderData(transOrderNo);
                            }

                        }
                    }
                }
                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        public static List<dynamic> GetVehicleFullTextTerm(string search, string istlo)
        {
            List<dynamic> dataVehicle = new List<dynamic>();
            try
            {

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    dataVehicle = db.Fetch<dynamic>(@";DECLARE @@TempResult TABLE
                                                        (
                                                          VehicleFullTextObjectID INT ,
                                                          VehicleCode VARCHAR(MAX) ,
                                                          BrandCode VARCHAR(MAX) ,
                                                          VehicleTypeCode VARCHAR(MAX) ,
                                                          ModelCode VARCHAR(MAX) ,
                                                          VehicleYear VARCHAR(MAX) ,
                                                          BrandDescription VARCHAR(MAX) ,
                                                          VehicleTypeDescription VARCHAR(MAX) ,
                                                          ModelDescription VARCHAR(MAX) ,
                                                          Series VARCHAR(MAX) ,
                                                          Terms VARCHAR(MAX) ,
                                                          VehicleDescription VARCHAR(MAX) ,
                                                          CreatedBy VARCHAR(MAX) ,
                                                          CreatedDate DATETIME ,
                                                          ModifiedBy VARCHAR(MAX) ,
                                                          ModifiedDate DATETIME
                                                        )
                                            INSERT @@TempResult exec usp_VehicleFulltextSearchOtosales  @0 ,  'AND' ,  0
                                                    SELECT DISTINCT TOP 1500 CASE WHEN VehicleYear < YEAR(GETDATE())  THEN 0 ELSE 1 END AS IsNew, VehicleFullTextObjectID AS VehicleID,VehicleDescription as VehicleDescription, tr.VehicleCode, tr.BrandCode ,tr.ModelCode ,VehicleYear AS Year, VehicleTypeCode as Type,tr.Series , Sitting,v.ProductTypeCode
                                    FROM @@TempResult tr
                                            INNER JOIN Vehicle v ON V.VehicleCode = tr.VehicleCode 
                                                AND V.BrandCode = tr.BrandCode 
                                                AND V.ModelCode = tr.ModelCode 
                                                AND v.year = tr.VehicleYear 
                                                AND v.Series = tr.Series
                                                AND v.RowStatus=1
                                            INNER JOIN VehicleModel vm ON vm.ModelCode=tr.ModelCode AND vm.Rowstatus=1
                                            INNER JOIN VehicleBrand vb ON vb.BrandCode=tr.BrandCode AND vb.Rowstatus=1
                                            INNER JOIN VehicleSeries vs ON vs.Series=tr.Series AND vs.Rowstatus=1", search);
                    logger.Debug("OK");
                    
//                    #region data sementara
//                    if (!string.IsNullOrEmpty(search))
//                    {
//                        search = "%" + search + "%";
//                        dataVehicle = db.Fetch<dynamic>(@"SELECT c.Description + ' ' + d.Description + ' ' + a.Series + ' ' + a.Year AS VehicleDescription,a.VehicleCode, a.BrandCode, a.ProductTypeCode, a.ModelCode,  a.Series, 
//                              a.Type, a.Sitting, a.Year, a.CityCode, a.Price, 
//                              a.LastUpdatedTime, a.RowStatus INTO #TempVehicle FROM Vehicle a 
//							  LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
//							  AND c.ProductTypeCode = a.ProductTypeCode 
//							  LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
//						      AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
//                              LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
//                              a.BrandCode=b.BrandCode
//                              AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
//                              AND a.Series=b.Series AND a.Type=b.Type 
//                              AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
//                              AND a.Year > b.Year WHERE b.Year IS NULL And UPPER(a.VehicleCode) like 'V0%'  ORDER BY a.VehicleCode
//
//							  SELECT * FROM #TempVehicle WHERE VehicleDescription like @0
//							  drop table #TempVehicle", search);
//                    }
//                    #endregion


                }
                return dataVehicle;
            }
            catch (Exception e)
            {
                throw (e);

            }
        }

        public static string GetBookingVANo()
        {
            string qGetVANo = @";DECLARE @@VANumber VARCHAR(MAX)
                                EXEC usp_GetVANumber @@VANumber OUTPUT
                                SELECT @@VANumber VirtualAccountNo";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                var result = db.Query<dynamic>(qGetVANo).FirstOrDefault();
                return result.VirtualAccountNo;
            }
        }


        public static DetailCoverageSummary GetDetailCoverageSummaryOld(string ProductCode, List<DetailScoring> dtlScoring, List<CalculatedPremi> CalculatedPremiItems, double AdminFee, string vyear, string UsageCode, int Ndays
                                                                       , DateTime? PeriodFrom, DateTime? PeriodTo, string vtype, string vsitting, string OldPolicyNo)
        {
            double PADRVRSI = 0;
            double PAPASSSI = 0;
            double TPLSI = 0;
            double basiccoverpremi = 0;
            double TSPremi = 0;
            bool islexus = false;
            bool issrccts = false;
            DetailCoverageSummary result = new DetailCoverageSummary();
            try
            {
                #region get basic detail cover
                List<DetailCoverage> dtlcoverage = new List<DetailCoverage>();
                List<DateTime> period = new List<DateTime>();
                List<CalculatedPremi> extendedPremiItems = new List<CalculatedPremi>();
                var DiscountList = RetailRepository.GetWTDiscount(ProductCode, ProductCode);
                double Discount = Convert.ToDouble(DiscountList.Count() > 0 ? DiscountList[0].Percentage : 0);
                foreach (CalculatedPremi item in CalculatedPremiItems)
                {
                    if (item.InterestID.TrimEnd().Equals("CASCO") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                    {
                        period.Add(item.PeriodFrom);
                    }
                }
                int year = 1;
                DateTime PeriodToPADRVR, PeriodToTS, PeriodToPAPASS, PeriodToTPL = Convert.ToDateTime("1900-01-01");
                foreach (DateTime pitem in period)
                {
                    double sumInsured = 0;
                    string basiccover = StatusCoverage.NoCover;
                    DetailCoverage dtl = new DetailCoverage();
                    foreach (CalculatedPremi item in CalculatedPremiItems)
                    {
                        if (pitem.Year.Equals(item.PeriodFrom.Year))
                        {
                            if (item.InterestID.TrimEnd().Equals("CASCO") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                            {
                                dtl.VehiclePremi = item.SumInsured;
                                dtl.LoadingPremi += item.LoadingRate;
                                dtl.Rate = dtl.Rate + item.Rate;
                                sumInsured = item.SumInsured;
                                dtl.PeriodFrom = item.PeriodFrom;
                                dtl.PeriodTo = item.PeriodTo;
                                dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                dtl.BasiCoverName = item.CoverageID.TrimEnd().Equals("ALLRIK") ? "Comprehensive" : item.CoverageID.TrimEnd().Equals("TLO") ? "Total Loss Only" : "";

                            }
                            else if (item.InterestID.TrimEnd().Equals("ACCESS") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                            {
                                dtl.AccessPremi = item.SumInsured;
                                dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                            }
                            else if ((item.CoverageID.TrimEnd().Contains("SRC") || item.CoverageID.TrimEnd().Contains("FLD") || item.CoverageID.TrimEnd().Contains("ETV") || item.CoverageID.TrimEnd().Equals("EQK")))
                            {
                                if (item.IsBundling) {
                                    basiccover = StatusCoverage.Include;
                                    dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                    if (item.InterestID.Trim().Equals("CASCO"))
                                    {
                                        dtl.Rate = dtl.Rate + item.Rate;
                                    }
                                    dtl.LoadingPremi += item.LoadingRate;
                                    dtl.IsBundling = Convert.ToInt16(item.IsBundling);
                                }
                                else
                                {
                                    basiccoverpremi += Convert.ToDouble(item.GrossPremium);
                                    basiccover = Convert.ToString(basiccoverpremi);
                                    dtl.ExtendedSFEPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.SFERate += item.Rate;
                                }
                                dtl.SFEPremi += Convert.ToDouble(item.GrossPremium);
                             if ((item.CoverageID.TrimEnd().Contains("SRCCTS") ))
                             {
                                 dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                 dtl.TSCoverage = Convert.ToString(Convert.ToDecimal(dtl.TSPremi));
                                 issrccts = true;
                                 if (dtl.TSPremi == 0)
                                 {
                                     dtl.TSCoverage = StatusCoverage.NoCover;
                                 }
                            }
                            }
                            else if ((item.CoverageID.TrimEnd().Contains("TRS") || item.CoverageID.TrimEnd().Contains("TRRTLO")))
                            {

                                TSPremi += Convert.ToDouble(item.GrossPremium);
                                dtl.TSRate = item.Rate;
                                if (item.IsBundling)
                                {

                                    dtl.LoadingPremi += item.LoadingRate;
                                    dtl.IsBundlingTRS = Convert.ToInt16(item.IsBundling);
                                    dtl.TSCoverage = StatusCoverage.Include;
                                    if (item.InterestID.Trim().Equals("CASCO"))
                                    {
                                        dtl.Rate = dtl.Rate + item.Rate;
                                    }
                                    dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.LoadingPremi = dtl.LoadingPremi + item.Loading;
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                }
                                else
                                {
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.TSCoverage = Convert.ToString(Convert.ToDecimal(dtl.TSPremi));
                                    if (dtl.TSPremi == 0)
                                    {
                                        dtl.TSCoverage = StatusCoverage.NoCover;
                                    }
                                }
                            }
                            else
                            {
                                extendedPremiItems.Add(item);
                            }
                        }
                    }
                    if (dtl.TSPremi == 0)
                    {
                        dtl.TSCoverage = StatusCoverage.NoCover;
                    }
                    double sias = sumInsured + dtl.AccessPremi;
                   // dtl.BasicPremi = (sias * dtl.Rate/100) + (sias * dtl.Loading);
                    dtl.VehicleAccessPremi= dtl.VehiclePremi + dtl.AccessPremi;
                    dtl.BasiCoverage = basiccover;
                    dtl.Year = year++;
                    dtlcoverage.Add(dtl);
                }
                #endregion

                #region get extended detail cover

                foreach (CalculatedPremi item in extendedPremiItems)
                {
                    if (item.InterestID.Trim().Equals("PADRVR"))
                    {
                        
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year>=cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.PADRVRCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod,false);
                                dtl.PADRVRRate = item.Rate;
                            }
                        }
                        PADRVRSI = item.SumInsured;

                    } if (item.InterestID.Trim().Equals("PADDR1"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            if (dtl.PeriodFrom <= item.PeriodFrom && dtl.PeriodTo >= item.PeriodTo)
                            {
                                dtl.PADRVRCoverage = Convert.ToDouble(item.GrossPremium);
                                dtl.PADRVRRate = item.Rate;
                            }
                        }

                        PADRVRSI = PADRVRSI==0?  item.SumInsured:PADRVRSI;
                        islexus = true;
                    }
                    if (item.InterestID.Trim().Equals("PAPASS"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year==item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.PAPASSCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod,false);
                                dtl.PAPASSRate = item.Rate;
                            }
                        }

                        PAPASSSI = item.SumInsured;

                    }
                    if (item.InterestID.Trim().Equals("PA24AV"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            if (dtl.PeriodFrom <= item.PeriodFrom && dtl.PeriodTo >= item.PeriodTo)
                            {
                                dtl.PAPASSCoverage = Convert.ToDouble(item.GrossPremium);
                                dtl.PAPASSRate = item.Rate;
                            }
                        }

                        PAPASSSI = PAPASSSI == 0 ? item.SumInsured : PAPASSSI;
                        islexus = true;
                    }
                    if (item.InterestID.Trim().Equals("TPLPER"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.TPLCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod,false);
                                dtl.TPLRate = item.Rate;
                            }
                        }

                        TPLSI = item.SumInsured;
                    }
                    //if (item.InterestID.Equals("TRS") || item.InterestID.Equals("TRRTLO"))
                    //{
                    //    foreach (DetailCoverage dtl in dtlcoverage)
                    //    {
                    //        if (item.PeriodTo >= dtl.PeriodTo)
                    //        {
                    //            dtl.TSCoverage = Convert.ToString(item.Premium);
                    //        }
                    //    }

                    //}

                }
                #endregion
                double PDP = 0;
                foreach (DetailCoverage dtl in dtlcoverage)
                {
                    double PADRVRCoverage =  dtl.PADRVRCoverage;
                    double PAPASSCoverage =  dtl.PAPASSCoverage;
                    dtl.PremiPerluasan = (islexus?0:(issrccts?0:dtl.TSPremi)) + dtl.TPLCoverage + PADRVRCoverage + PAPASSCoverage+dtl.ExtendedSFEPremi;
                    dtl.PremiDasarPerluasan = dtl.PremiPerluasan + dtl.BasicPremi;
                    double TotalPremi =PDP+dtl.PremiDasarPerluasan ;
                    double diskonpremi = TotalPremi * Discount / 100;
                    dtl.TotalPremi = TotalPremi - diskonpremi+AdminFee;
                    PDP += dtl.PremiDasarPerluasan;
                }
                result.AdminFee = AdminFee;
                result.dtlCoverage = dtlcoverage;
                result.PADRVRSI = PADRVRSI;
                result.PAPASSSI = PAPASSSI;
                result.TPLSI = TPLSI;
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static DetailCoverageSummary GetDetailCoverageSummary(string ProductCode, List<DetailScoring> dtlScoring, List<CalculatedPremi> CalculatedPremiItems, double AdminFee, string vyear, string UsageCode, int Ndays
                                                               , DateTime? PeriodFrom, DateTime? PeriodTo, string vtype, string vsitting, string OldPolicyNo)
        {
            double PADRVRSI = 0;
            double PAPASSSI = 0;
            double TPLSI = 0;
            double basiccoverpremi = 0;
            double TSPremi = 0;
            bool islexus = false;
            bool issrccts = false;
            DetailCoverageSummary result = new DetailCoverageSummary();
            try
            {
                #region get basic detail cover
                List<DetailCoverage> dtlcoverage = new List<DetailCoverage>();
                List<DateTime> period = new List<DateTime>();
                List<CalculatedPremi> extendedPremiItems = new List<CalculatedPremi>();
                var DiscountList = RetailRepository.GetWTDiscount(ProductCode, ProductCode);
                double Discount = Convert.ToDouble(DiscountList.Count() > 0 ? DiscountList[0].Percentage : 0);
                foreach (CalculatedPremi item in CalculatedPremiItems)
                {
                    if (item.InterestID.TrimEnd().Equals("CASCO") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                    {
                        period.Add(item.PeriodFrom);
                    }
                }
                int year = 1;
                DateTime PeriodToPADRVR, PeriodToTS, PeriodToPAPASS, PeriodToTPL = Convert.ToDateTime("1900-01-01");
                foreach (DateTime pitem in period)
                {
                    double sumInsured = 0;
                    string basiccover = StatusCoverage.NoCover;
                    DetailCoverage dtl = new DetailCoverage();
                    foreach (CalculatedPremi item in CalculatedPremiItems)
                    {
                        if (pitem.Year.Equals(item.PeriodFrom.Year))
                        {
                            if (item.InterestID.TrimEnd().Equals("CASCO") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                            {
                                dtl.VehiclePremi = item.SumInsured;
                                dtl.LoadingPremi += item.LoadingRate;
                                dtl.Rate = dtl.Rate + item.Rate;
                                sumInsured = item.SumInsured;
                                dtl.PeriodFrom = item.PeriodFrom;
                                dtl.PeriodTo = item.PeriodTo;
                                dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                dtl.BasiCoverName = item.CoverageID.TrimEnd().Equals("ALLRIK") ? "Comprehensive" : item.CoverageID.TrimEnd().Equals("TLO") ? "Total Loss Only" : "";

                            }
                            else if (item.InterestID.TrimEnd().Equals("ACCESS") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                            {
                                dtl.AccessPremi = item.SumInsured;
                                dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                            }
                            else if ((item.CoverageID.TrimEnd().Contains("SRC") || item.CoverageID.TrimEnd().Contains("FLD") || item.CoverageID.TrimEnd().Contains("ETV") || item.CoverageID.TrimEnd().Equals("EQK")))
                            {
                                if (item.IsBundling)
                                {
                                    basiccover = StatusCoverage.Include;
                                    dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                    if (item.InterestID.Trim().Equals("CASCO"))
                                    {
                                        dtl.Rate = dtl.Rate + item.Rate;
                                    }
                                    dtl.LoadingPremi += item.LoadingRate;
                                    dtl.IsBundling = Convert.ToInt16(item.IsBundling);
                                }
                                else
                                {
                                    basiccoverpremi += Convert.ToDouble(item.GrossPremium);
                                    basiccover = Convert.ToString(basiccoverpremi);
                                    dtl.ExtendedSFEPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.SFERate += item.Rate;
                                }
                                dtl.SFEPremi += Convert.ToDouble(item.GrossPremium);
                                if ((item.CoverageID.TrimEnd().Contains("SRCCTS")))
                                {
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.TSCoverage = Convert.ToString(Convert.ToDecimal(dtl.TSPremi));
                                    issrccts = true;
                                    if (dtl.TSPremi == 0)
                                    {
                                        dtl.TSCoverage = StatusCoverage.NoCover;
                                    }
                                }
                            }
                            else if ((item.CoverageID.TrimEnd().Contains("TRS") || item.CoverageID.TrimEnd().Contains("TRRTLO")))
                            {

                                TSPremi += Convert.ToDouble(item.GrossPremium);
                                dtl.TSRate = item.Rate;
                                if (item.IsBundling)
                                {

                                    dtl.LoadingPremi += item.LoadingRate;
                                    dtl.IsBundlingTRS = Convert.ToInt16(item.IsBundling);
                                    dtl.TSCoverage = StatusCoverage.Include;
                                    if (item.InterestID.Trim().Equals("CASCO"))
                                    {
                                        dtl.Rate = dtl.Rate + item.Rate;
                                    }
                                    dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.LoadingPremi = dtl.LoadingPremi + item.Loading;
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                }
                                else
                                {
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.TSCoverage = Convert.ToString(Convert.ToDecimal(dtl.TSPremi));
                                    if (dtl.TSPremi == 0)
                                    {
                                        dtl.TSCoverage = StatusCoverage.NoCover;
                                    }
                                }
                            }
                            else
                            {
                                extendedPremiItems.Add(item);
                            }
                        }
                    }
                    if (dtl.TSPremi == 0)
                    {
                        dtl.TSCoverage = StatusCoverage.NoCover;
                    }
                    double sias = sumInsured + dtl.AccessPremi;
                    // dtl.BasicPremi = (sias * dtl.Rate/100) + (sias * dtl.Loading);
                    dtl.VehicleAccessPremi = dtl.VehiclePremi + dtl.AccessPremi;
                    dtl.BasiCoverage = basiccover;
                    dtl.Year = year++;
                    dtlcoverage.Add(dtl);
                }
                #endregion

                #region get extended detail cover

                foreach (CalculatedPremi item in extendedPremiItems)
                {
                    if (item.InterestID.Trim().Equals("PADRVR"))
                    {

                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.PADRVRCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod, false);
                                dtl.PADRVRRate = item.Rate;
                            }
                        }
                        PADRVRSI = item.SumInsured;

                    } if (item.InterestID.Trim().Equals("PADDR1"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            if (dtl.PeriodFrom <= item.PeriodFrom && dtl.PeriodTo >= item.PeriodTo)
                            {
                                dtl.PADRVRCoverage = Convert.ToDouble(item.GrossPremium);
                                dtl.PADRVRRate = item.Rate;
                            }
                        }
                        islexus = true;
                        PADRVRSI = PADRVRSI == 0 ? item.SumInsured : PADRVRSI;
                    }
                    if (item.InterestID.Trim().Equals("PAPASS"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.PAPASSCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod, false);
                                dtl.PAPASSRate = item.Rate;
                            }
                        }

                        PAPASSSI = item.SumInsured;

                    }
                    if (item.InterestID.Trim().Equals("PA24AV"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            if (dtl.PeriodFrom <= item.PeriodFrom && dtl.PeriodTo >= item.PeriodTo)
                            {
                                dtl.PAPASSCoverage = Convert.ToDouble(item.GrossPremium);
                                dtl.PAPASSRate = item.Rate;
                            }
                        }
                        islexus = true;
                        PAPASSSI = PAPASSSI == 0 ? item.SumInsured : PAPASSSI;
                    }
                    if (item.InterestID.Trim().Equals("TPLPER"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.TPLCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod, false);
                                dtl.TPLRate = item.Rate;
                            }
                        }

                        TPLSI = item.SumInsured;
                    }
                    //if (item.InterestID.Equals("TRS") || item.InterestID.Equals("TRRTLO"))
                    //{
                    //    foreach (DetailCoverage dtl in dtlcoverage)
                    //    {
                    //        if (item.PeriodTo >= dtl.PeriodTo)
                    //        {
                    //            dtl.TSCoverage = Convert.ToString(item.Premium);
                    //        }
                    //    }

                    //}

                }
                #endregion
                double PDP = 0;
                foreach (DetailCoverage dtl in dtlcoverage)
                {
                    double PADRVRCoverage = dtl.PADRVRCoverage;
                    double PAPASSCoverage = dtl.PAPASSCoverage;
                    dtl.PremiPerluasan = (islexus ? 0 : (issrccts ? 0 : dtl.TSPremi)) + dtl.TPLCoverage + PADRVRCoverage + PAPASSCoverage + dtl.ExtendedSFEPremi;
                    dtl.PremiDasarPerluasan = dtl.PremiPerluasan + dtl.BasicPremi;
                    double TotalPremi = PDP + dtl.PremiDasarPerluasan;
                    double diskonpremi = TotalPremi * Discount / 100;
                    dtl.TotalPremi = TotalPremi - diskonpremi + AdminFee;
                    PDP += dtl.PremiDasarPerluasan;
                }
                result.AdminFee = AdminFee;
                result.dtlCoverage = dtlcoverage;
                result.PADRVRSI = PADRVRSI;
                result.PAPASSSI = PAPASSSI;
                result.TPLSI = TPLSI;
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static PaymentInfo GetPaymentInfo(string CustID, string FollowUpNo)
        {
            string query = "";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            List<dynamic> listDyn = new List<dynamic>();
            PaymentInfo pf = new PaymentInfo();

            try
            {
                query = @";IF EXISTS(SELECT * FROM dbo.OrderSimulation os
						INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
						WHERE os.ApplyF = 1 AND f.CustID = @0 AND f.FollowUpNo = @1)
						BEGIN
							SELECT os.VANumber, f.BuktiBayar AS BuktiBayar, 
							id.Data AS BuktiBayarData, os.PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data
							FROM dbo.OrderSimulation os 
							INNER JOIN dbo.FollowUp f 
							ON f.FollowUpNo = os.FollowUpNo
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE f.CustID = @0 AND f.FollowUpNo = @1 
							AND f.RowStatus = 1
						END
						ELSE
						BEGIN
							SELECT '' AS VANumber, BuktiBayar, 
                            id.Data AS BuktiBayarData, '' AS PolicyOrderNo,
							BuktiBayar2, id2.Data BuktiBayar2Data,
							BuktiBayar3, id3.Data BuktiBayar3Data,
							BuktiBayar4, id4.Data BuktiBayar4Data,
							BuktiBayar5, id5.Data BuktiBayar5Data							 
							FROM dbo.FollowUp f 
							LEFT JOIN dbo.ImageData id
							ON id.PathFile = f.BuktiBayar
							LEFT JOIN dbo.ImageData id2
							ON id2.PathFile = f.BuktiBayar2
							LEFT JOIN dbo.ImageData id3
							ON id3.PathFile = f.BuktiBayar3
							LEFT JOIN dbo.ImageData id4
							ON id4.PathFile = f.BuktiBayar4
							LEFT JOIN dbo.ImageData id5
							ON id5.PathFile = f.BuktiBayar5
							WHERE CustID = @0 AND FollowUpNo = @1
							AND f.RowStatus = 1
						END";
                    listDyn = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                    if (listDyn.Count > 0)
                    {
                        query = @"SELECT OrderNo,COALESCE(ProductCode,'') AS ProductCode,isCompany,os.SalesOfficerID,os.PolicyOrderNo
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        INNER JOIN dbo.ProspectCustomer ps
                        ON ps.CustID = f.CustID
                        WHERE os.CustID = @0 
                        AND os.FollowUpNo = @1
                        AND os.ApplyF = 1";
                        List<dynamic> ordData = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                        if (ordData.Count > 0)
                        {
                            if (Convert.ToBoolean(ordData.First().isCompany))
                            {
                                query = @"SELECT a.branch_id ,
                                            a.accounting_code_sun ,
                                            b.accountno ,
                                            b.bankdescription ,
                                            b.name ,
                                            b.branchdescription
                                    FROM   mst_branch AS a WITH ( NOLOCK )
                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                                List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                                if (lists.Count > 0)
                                {
                                    pf.RekeningPenampung = lists.First().accountno;
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(ordData.First().ProductCode)) {
                                    query = @"select CAST(COALESCE(IsAllowVA,0) AS BIT) IsAllowVA, Biz_type from mst_product where Product_Code = @0";
                                    List<dynamic> listBizType = AABdb.Fetch<dynamic>(query, ordData.First().ProductCode);
                                    if (listBizType.Count > 0)
                                    {
                                        int b = 0;
                                        b = listBizType.First().Biz_type;
                                        query = @"Select Salesman_Id,mb.Branch_id From Mst_Salesman ms
                                        INNER JOIN dbo.Mst_Branch mb
                                        ON mb.Branch_id = ms.Branch_Id
                                        Where User_id_otosales= @0 
                                        and ms.status=1 AND mb.Biz_type = @1";
                                        if (!listBizType.First().IsAllowVA)
                                        {
                                            if (b == 2)
                                            {
                                                pf.RekeningPenampung = GetApplicationParametersValue("REKPENAMPUNGSYARIAH").First();
                                            }
                                            else {
                                                query = @"SELECT a.branch_id ,
                                                            a.accounting_code_sun ,
                                                            b.accountno ,
                                                            b.bankdescription ,
                                                            b.name ,
                                                            b.branchdescription
                                                        FROM mst_branch AS a WITH ( NOLOCK )
                                                            INNER JOIN branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
		                                                    WHERE  a.branch_id = (SELECT REPLACE(BranchCode,'A','') FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0)";
                                                List<dynamic> lists = AABdb.Fetch<dynamic>(query, ordData.First().OrderNo);
                                                if (lists.Count > 0)
                                                {
                                                    pf.RekeningPenampung = lists.First().accountno;
                                                }                                            
                                            }
                                            query = @"SELECT OrderNo,VANumber,OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1";
                                            List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                            if (ordData.Count > 0)
                                            {
                                                string VaNo = Convert.ToString(ordDt.First().VANumber);
                                                if (!string.IsNullOrEmpty(VaNo))
                                                {
                                                    db.Execute("UPDATE dbo.OrderSimulation SET VANumber = '' WHERE OrderNo = @0", ordDt.First().OrderNo);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            query = @"SELECT OrderNo,VANumber,OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1";
                                            List<dynamic> ordDt = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                                            if (ordData.Count > 0)
                                            {
                                                bool isGenerate = true;
                                                string VaNo = Convert.ToString(ordDt.First().VANumber);
                                                if (string.IsNullOrEmpty(VaNo))
                                                {
                                                    if (!string.IsNullOrEmpty(ordDt.First().OldPolicyNo))
                                                    {
                                                        query = "SELECT * FROM dbo.Excluded_Renewal_Policy WHERE Policy_No = @0";
                                                        List<dynamic> lsRen = AABdb.Fetch<dynamic>(query, ordDt.First().OldPolicyNo);
                                                        if (lsRen.Count > 0)
                                                        {
                                                            isGenerate = false;
                                                        }
                                                    }
                                                    if (isGenerate)
                                                    {
                                                        VaNo = GetBookingVANo();
                                                        query = @"UPDATE dbo.OrderSimulation 
                                                                SET VANumber=@2
                                                                WHERE CustID = @0 AND FollowUpNo = @1";
                                                        db.Execute(query, CustID, FollowUpNo, VaNo);
                                                    }
                                                }
                                            }
                                        }                                
                                    }
                                }
                                string pPermata = GetApplicationParametersValue("PREFIXVAPERMATA").First();
                                string pMandiri = GetApplicationParametersValue("PREFIXVAMANDIRI").First();
                                string pBCA = GetApplicationParametersValue("PREFIXVABCA").First();
                                string VANo = Convert.ToString(listDyn.First().VANumber);
                                if (!string.IsNullOrEmpty(VANo))
                                {
                                    pf.VAPermata = pPermata + VANo;
                                    pf.VAMandiri = pMandiri + VANo;
                                    pf.VABCA = pBCA + VANo;
                                }
                            }
                            pf.BuktiBayar = listDyn.First().BuktiBayar;
                            pf.BuktiBayarData = listDyn.First().BuktiBayarData;
                            pf.BuktiBayar2 = listDyn.First().BuktiBayar2;
                            pf.BuktiBayar2Data = listDyn.First().BuktiBayar2Data;
                            pf.BuktiBayar3 = listDyn.First().BuktiBayar3;
                            pf.BuktiBayar3Data = listDyn.First().BuktiBayar3Data;
                            pf.BuktiBayar4 = listDyn.First().BuktiBayar4;
                            pf.BuktiBayar4Data = listDyn.First().BuktiBayar4Data;
                            pf.BuktiBayar5 = listDyn.First().BuktiBayar5;
                            pf.BuktiBayar5Data = listDyn.First().BuktiBayar5Data;
                            string PolicyOrderNo = Convert.ToString(listDyn.First().PolicyOrderNo);
                            if (!string.IsNullOrEmpty(PolicyOrderNo))
                            {
                                query = @"SELECT DATEADD(DAY,13,Order_Date) AS DueDate FROM dbo.Mst_Order where Order_No = @0";
                                List<dynamic> listDate = AABdb.Fetch<dynamic>(query, PolicyOrderNo);
                                if (listDate.Count > 0)
                                {
                                    pf.DueDate = listDate.First().DueDate;
                                }
                            }
                        }
                    }
                return pf;
            }
            catch (Exception e)
            {

                throw e;
            }
        }


        internal static List<double> RetrieveExtSI(string InterestId)
        {
            List<double> result = new List<double>();
            try
            {

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                double start = db.ExecuteScalar<double>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName=@0", "TSI" + InterestId + "LIST");
                double add = db.ExecuteScalar<double>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName=@0", "TSI" + InterestId + "LIST");
                double end = db.ExecuteScalar<double>("SELECT ParamValue2 FROM ApplicationParameters WHERE ParamName=@0", "TSI" + InterestId + "LIST");
                for (double i = start; start <= end; i++)
                {
                    result.Add(start);

                    start = start + add;
                }
                    return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal static object GetPostalCode(string search, string citycode)
        {
            try
            {
                List<dynamic> result = new List<dynamic>();
                  using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_BEYONDDB))
                    {
                      result = db.Fetch<dynamic>(@"select PostalID,PostalCode,PostalDescription,CityID from general.postal where PostalCode like @0 AND rowstatus=0 "+(string.IsNullOrEmpty(citycode)?"":" AND CityId=@1 ")+"ORDER BY PostalCode ASC", search);
                  }
                  return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal static dynamic GetSurveySchedule(string cityid, string zipcode, DateTime d)
        {
            dynamic result = null;
            string QueryGetGAScheduleWithDate = @"
SELECT  Frequency ,
        CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
             WHEN tc.TimeCategoryCode = 'TC2' THEN 40
             WHEN tc.TimeCategoryCode = 'TC3' THEN 41
        END ScheduleTime ,
        ZipCode ,
        C.AreaCode SurveyareaCode ,
        CityId ,
        ISNULL(slotbooked, 0) slotbooked ,
        TC.TimeCategoryDescription ScheduleTimeTextID ,
        CASE WHEN tc.TimeCategoryCode = 'TC1'
             THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
             WHEN tc.TimeCategoryCode = 'TC2'
             THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
             WHEN tc.TimeCategoryCode = 'TC3'
             THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
        END ScheduleTimeTextEN
FROM    GA.SurveyArea sa
        INNER JOIN ( SELECT DISTINCT
                            ZipCode ,
                            cityID ,
                            REPLACE(REPLACE(REPLACE(GeoAreaDescription,
                                                    GeoAreaCode, ''), ' ', ''),
                                    '-', '') AreaCode
                     FROM   AABmobile.dbo.surveyzipcode
                   ) c ON C.AreaCode = REPLACE(SA.SurveyareaCode, '-', '')
        INNER JOIN GA.TimeCategory TC ON SA.TimeCategoryCode = TC.TimeCategoryCode
        LEFT JOIN ( SELECT  SurveyAreaCode ,
                            SurveyTimeCode ,
                            SurveyDate ,
                            COUNT(1) AS SlotBooked
                    FROM    GA.BookingSurvey AS bs
                    WHERE   RowStatus = 0
                            AND CONVERT(VARCHAR(10), SURVEYDATE, 120) BETWEEN CONVERT(VARCHAR(10), GETDATE()
                                                              + 1, 120)
                                                              AND
                                                              CONVERT(VARCHAR(10), GETDATE()
                                                              + 6, 120)
                            AND CONVERT(VARCHAR(10), surveydate, 120) = @AvailableDate
                            AND bs.RowStatus = 0
                    GROUP BY bs.surveydate ,
                            bs.SurveyAreaCode ,
                            SurveyTimeCode
                  ) a ON REPLACE(a.SurveyAreaCode, '-', '') = c.AreaCode
                         AND a.SurveyTimeCode = sa.TimeCategoryCode
WHERE   CITYID = @CityId
        AND ZipCode = @ZipCode
        AND frequency > ISNULL(slotbooked, 0)
        AND sa.RowStatus = 0
        AND tc.RowStatus = 0";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAAKSES))
            {
                string AvailableDate = d.ToString("yyyy-MM-dd");
                result = db.Fetch<dynamic>(QueryGetGAScheduleWithDate, new { ZipCode = zipcode, CityId = cityid, AvailableDate });
            }

            return result;
        }

        internal static dynamic GetSurveyScheduleSurveyManagement(string cityid, string zipcode, DateTime d)
        {
            dynamic result = null;
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            string qGetSurveyAddress = @"SELECT AreaCode GeoAreaCode FROM SurveyManagement.dbo.MappingSurveyAreaZipCode where ZipCode = @0 AND RowStatus = 0";
            List<dynamic> addresses = AABDB.Fetch<dynamic>(qGetSurveyAddress, zipcode);
            if (addresses.Count > 0) {
                string GeoAreaCode = addresses.First().GeoAreaCode;
                if (!string.IsNullOrEmpty(GeoAreaCode)) {
                    string QueryGetGAScheduleWithDate = @";SELECT  @0 AS SurveyDate
        INTO    #SurveyDate

            SELECT  CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
						 WHEN tc.TimeCategoryCode = 'TC2' THEN 40
						 WHEN tc.TimeCategoryCode = 'TC3' THEN 41
					END ScheduleTime ,
					ZipCode ,
					msast.AreaCode SurveyareaCode ,
					CityId ,
					TC.TimeCategoryDescription ScheduleTimeTextID ,
					CASE WHEN tc.TimeCategoryCode = 'TC1'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
						 WHEN tc.TimeCategoryCode = 'TC2'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
						 WHEN tc.TimeCategoryCode = 'TC3'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
					END ScheduleTimeTextEN ,
                    msast.Quota ,
                    ( SELECT    COUNT(SurveyOrderID)
                      FROM      SurveyManagement.dbo.SurveyOrder AS so
                      WHERE     so.RowStatus = 0
                                AND so.IsRescheduleF = 0
                                AND so.SurveyDate = sd.SurveyDate
                                AND so.AreaCode = msast.AreaCode
                                AND so.SurveyTime >= msast.Start
                                AND so.SurveyTime < msast.[End]
                    ) AS slotbooked
            INTO    #DateSlot
            FROM    #SurveyDate AS sd
                    FULL JOIN SurveyManagement.dbo.MappingSurveyAreaSlotTime AS msast ON 1 = 1
					INNER JOIN ( SELECT DISTINCT
                            PostalCode ZipCode,
                            CityID,
							AreaCode
                     FROM   BEYONDMOSS.Asuransiastra.dbo.Postal p
					 INNER JOIN SurveyManagement.dbo.MappingSurveyAreaZipCode msazc
					 ON p.PostalCode = msazc.ZipCode AND p.RowStatus = 0
                   ) c ON c.AreaCode = msast.AreaCode
				   LEFT JOIN BEYONDMOSS.GardaAkses.GA.TimeCategory tc ON TC.ScheduleTime =  SUBSTRING(CONVERT(VARCHAR, msast.Start), 1, 5)
            WHERE   msast.AreaCode = @1
                    AND msast.RowStatus = 0
					AND c.ZipCode = @2

			SELECT * FROM #DateSlot
			WHERE Quota - slotbooked >0
    DROP TABLE #SurveyDate
    DROP TABLE #DateSlot";
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        string AvailableDate = d.ToString("yyyy-MM-dd");
                        result = db.Fetch<dynamic>(QueryGetGAScheduleWithDate, AvailableDate, GeoAreaCode, zipcode);
                    }                
                }
            }

            return result;
        }

        internal static bool isValidMappingSegment(string ProductCode)
        {
            bool isvalid = false;
            try
            { using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                isvalid = db.ExecuteScalar<int>(@"SELECT  count(*)
                            FROM    [AAB].[dbo].Mapping_Product_SOB A
                                    INNER JOIN [dbo].Mst_SOB B ON A.SOB_ID = B.SOB_ID
                            WHERE   A.Product_code = @0 AND A.RowStatus = 0",ProductCode)>0?true:false;
                }
                return isvalid;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal static List<BasicCover> GetBasicCover( int year, bool isBasic)
        {
            List<BasicCover> bcList = new List<BasicCover>();
            try
            {
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                    {
                        if (DateTime.Now.Year - year < 5)
                        {
                            bcList = db.Fetch<BasicCover>(@"
SELECT Id,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE isBasic in (" + (isBasic ? "1,0" : "1") + @") AND RowStatus=1");
                        }
                        else
                        {
                            bcList = db.Fetch<BasicCover>(@"SELECT ID,CoverageId,ComprePeriod,TLOPeriod,Description FROM mst_basic_cover WHERE (ComprePeriod=1 AND TLOPeriod=0) or(ComprePeriod=0 AND TLOPeriod=1)");
                  
                        }
                    }
                
                return bcList;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal static bool IsProductLexus(string ProductCode)
        {
            bool isTrue = false;
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                isTrue = db.ExecuteScalar<int>("SELECT count(*) FROM Partner_Document_Template where Product_Code=@0 AND Partner_Id='LEX'", ProductCode) > 0 ? true : false;
                return isTrue;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal static string GetSpecialCoverageId(string ProductCode, CoverageTypeNonBasic cType, string vInterestId, string vTPLCoverageId)
        {
            try
            {
                //string unpamv = "UNPAMV";
                //string pap1 = "PAP1";
                string result = "";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                if (cType.Equals(CoverageTypeNonBasic.TPLPER)&&!string.IsNullOrEmpty(vTPLCoverageId))
                {
                    result = vTPLCoverageId;
                }
                else
                {
                    string query = @";;DECLARE @@ProductCode VARCHAR(10) ,

                                @@InterestID VARCHAR(10) ,

                                @@CoverageID VARCHAR(10) ,

                                @@ComprehensiveFlag BIT

                            SELECT  @@ProductCode = @0

                                                ,

                                    @@InterestID = @1

                            SELECT DISTINCT  
								
                                    a.Coverage_ID AS CoverageID 

                            FROM    dbo.Prd_Interest_Coverage a

                                    LEFT OUTER JOIN rate_scoring z ON z.score_code = a.scoring_code

                                                                      AND z.coverage_id = a.coverage_id

                                    LEFT OUTER JOIN b2b_interest_coverage c ON A.Product_code = C.Product_Code

                                                                               AND a.interest_id = c.interest_id

                                                                               AND a.coverage_id = c.coverage_id

                                                                               AND c.Vehicle_Type = 'ALL'

                                                                               AND c.Usage = 'ALL'

                                    INNER JOIN dbo.Mst_Coverage AS b ON A.Coverage_ID = b.Coverage_ID

                                    INNER JOIN dbo.Mst_Interest AS d ON A.Interest_ID = d.Interest_Id

                            WHERE   a.Product_Code = @@ProductCode

                                    AND a.IC_STATUS <> 0
									AND a.interest_id=@@InterestID
									

 
 ";
                    result = db.ExecuteScalar<string>(query, ProductCode, vInterestId);
                }
               
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal static string GetSpecialInterestId(string ProductCode, CoverageTypeNonBasic cType, string vInterestId)
        {
            try
            {
                string result = vInterestId;
                if (MobileRepository.IsProductLexus(ProductCode))
                {
                    if (cType.Equals(CoverageTypeNonBasic.PADRVR))
                    {
                        result = GetApplicationParametersValue("OTOSALES-LEXUS-PADRVR-INTEREST")[0];
                    }
                    else if (cType.Equals(CoverageTypeNonBasic.PAPASS))
                    {
                        result = GetApplicationParametersValue("OTOSALES-LEXUS-PAPASS-INTEREST")[0];
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }

        }

        internal static List<string> GetPhoneNumberList(string OrderNo, int IsRenewal)
        {
            try
            {
                List<string> result = new List<string>();
                //if (MobileRepository.IsProductLexus(ProductCode))
                //{
                //    if (cType.Equals(CoverageTypeNonBasic.PADRVR))
                //    {
                //        result = "PADDR1";
                //    }
                //    else if (cType.Equals(CoverageTypeNonBasic.PAPASS))
                //    {
                //        result = "PA24AV";
                //    }
                //}
                 using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {

                    result = db.Fetch<string>(";EXEC [dbo].[usp_PhonePriorityList] @0,@1", OrderNo, IsRenewal);
                 }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }

        }
        public //static 
            List<Models.Product> GetProduct(string insurancetype, string salesofficerid, int isRenewal)
        {
            List<Otosales.Models.Product> result = new List<Models.Product>();
            try
            {
                string query = @"
select DISTINCT  mp.Product_Code [ProductCode], mp.Description, 'GARDAOTO' [ProductTypeCode], @0 [InsuranceType], 1 [RowStatus], CASE WHEN mp.day_calculation_method =360 THEN 360 ELSE 366 END [NDays], Biz_type,mp.cob_id, IIF(PDT.Partner_ID is null, 0, 1) [IsLexus]

from mst_product mp

LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code AND PDT.Partner_ID = 'LEX'

where GETDATE() BETWEEN Valid_From and Valid_To AND mp.Product_Code !='' AND mp.isb2b=0

AND 1 = CASE WHEN @0 = 1 THEN IIF(mp.cob_id = '403' and Biz_type=1 and PDT.Partner_ID is null,1,0)

                      WHEN @0 = 2 THEN IIF(mp.cob_id= '404' and PDT.Partner_ID is null,1,0)

                     WHEN @0 = 3 THEN IIF(PDT.Partner_ID is not null,1,0) 

                     WHEN @0 = 4 THEN IIF(mp.cob_id = '403'and Biz_type=2 and PDT.Partner_ID is null,1,0 ) END

  ";                // Handle ProductAgency
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                bool isAgency = db.ExecuteScalar<int>(@" SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] eu INNER JOIN [BeyondMoss].[AABMobile].[dbo].[SalesOfficer] so ON so.Email = eu.UserID WHERE so.SalesOfficerId=@0",salesofficerid) > 0 ? true : false;
                query = string.Concat(query, isAgency ? " AND mp.product_type=6" : " AND mp.product_type!=6");
                result = db.Fetch<Otosales.Models.Product>(query,insurancetype,isRenewal);

                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public static double RecalculateExtendedCover(string interestid, CoverageParam cp, DateTime? PeriodFrom, DateTime? PeriodTo, string ProductCode, List<DetailScoring> dtlScoring, string vyear, string UsageCode, int Ndays
                                                       , string vtype, string vsitting, decimal vTSInterest, string OldPolicyNo, int calcmethod,bool isrenew)
        {
            try
            {
                List<CalculatedPremi> result = new List<CalculatedPremi>();
                DateTime pf = PeriodFrom ?? DateTime.Now;
                DateTime pt = PeriodTo ?? DateTime.Now;
                TimeSpan ts = new TimeSpan(0, 0, 0);
                pf = pf.Date + ts;
                pt = pt.Date + ts;
                cp.PeriodFromCover = cp.PeriodFromCover.Date + ts;
                cp.PeriodToCover = cp.PeriodToCover.Date + ts;
                List<decimal> RenDiscountPct = new List<decimal>();
                List<decimal> DiscountPct = new List<decimal>();
                result = calculatepremicalculation(ProductCode, interestid, cp, pf, pt, vtype, vyear, vsitting, dtlScoring, Ndays.ToString(), vTSInterest, ProductCode, OldPolicyNo, result, calcmethod, out RenDiscountPct, out DiscountPct);
                if (result.Count > 0)
                {
                    return Convert.ToDouble(result[0].GrossPremium);
                }
                else {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        internal static List<CalculatedPremi> calculatepremicalculation(string ProductCode, string vInterestId, CoverageParam cp, DateTime PeriodFrom, DateTime PeriodTo, string vtype, string vyear
                                                              , string vsitting, List<DetailScoring> dtlScoring, string Ndays, decimal vTSInterest, string vMouID, string OldPolicyNo, List<CalculatedPremi> CalculatedPremiItems
                                                               , int calcmethod, out List<decimal> RenDiscountPct, out List<decimal> DiscountPct)
        {
            List<decimal> RDP = new List<decimal>();
            List<decimal> DP = new List<decimal>();
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                ParamCalculatePremiPerCoverage pcppc = new ParamCalculatePremiPerCoverage();// 2 : scale, 1 : prorate
                pcppc.CalculationMethod = calcmethod;
                pcppc.PeriodFrom = PeriodFrom;
                pcppc.PeriodTo = PeriodTo;
                pcppc.PeriodFromCover = cp.PeriodFromCover;
                pcppc.PeriodToCover = cp.PeriodToCover;
                pcppc.VehicleType = vtype;
                pcppc.YearManufacturing = vyear;
                pcppc.TSICurrID = "IDR";
                pcppc.NumberOfSeat = vsitting;
                //pcppc.MouID = vMouID;
                pcppc.ProductCode = ProductCode;
                pcppc.TSInterest = vTSInterest;
                //pcppc.PrimarySI = vPrimarySI;
                pcppc.CoverageId = cp.CoverageId;
                pcppc.ScoringList = dtlScoring;
                pcppc.InterestId = vInterestId;
                pcppc.NDays = Ndays;


                //#Sprint5 BEGIN                
                var PRDType = RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: vInterestId, CoverageID: cp.CoverageId);
                string ConfigName = "EXTENDED_COVER_GROUP_COMPRE";
                string AutoApplyExtGroupFlag = AutoApplyFlag.CompreExtendedGroup;
                if (PRDType.Count != 0)
                {
                    if (PRDType[0].TLOFlag != null)
                    {
                        if (PRDType[0].TLOFlag)
                        {
                            ConfigName = "EXTENDED_COVER_GROUP_TLO";
                            AutoApplyExtGroupFlag = AutoApplyFlag.TLOExtendedGroup;
                        }
                    }
                }
                List<string> CoverGrp = RetailRepository.GetGeneralConfig(ConfigName, "PRODUCT", ProductCode, cp.CoverageId);

                if (CoverGrp.Count == 0) CoverGrp.Add(cp.CoverageId);
                // string vCoverageIdPADRVR = db.ExecuteScalar<string>("SELECT TOP 1 Coverage_Id FROM Prd_Interest_Coverage WHERE Product_Code=@0 AND Interest_Id=@1", ProductCode, CoverageTypeNonBasic.PADRVR.ToString());
                // bool isAutoApplyPADRVR = false;
                //   if (!string.IsNullOrEmpty(cp.CoverageIdPADRVR) && !CoverGrp.Contains(cp.CoverageIdPADRVR) && vInterestId.Equals(CoverageTypeNonBasic.PAPASS.ToString(), StringComparison.OrdinalIgnoreCase) && !CalculatedPremiItems.Select(x => x.InterestID.Trim()).Contains(CoverageTypeNonBasic.PADRVR.ToString()))
                //       CoverGrp.Add(cp.CoverageIdPADRVR);
                //       isAutoApplyPADRVR = true;
                for (int i = 0; i < CoverGrp.Count; i++)
                {
                    pcppc.CoverageId = cp.CoverageId = CoverGrp[i];
                    //if (i == CoverGrp.Count-1 && isAutoApplyPADRVR && cp.CoverageId.Equals(vCoverageIdPADRVR))
                    //{
                    //    vInterestId = CoverageTypeNonBasic.PADRVR.ToString();
                    //    pcppc.InterestId = vInterestId;
                    //}
                    pcppc.YearManufacturing = string.IsNullOrEmpty(pcppc.YearManufacturing) ? "0" : pcppc.YearManufacturing;
                    var CalculationResult = pm.CalculatePremiPerCoverage(pcppc);

                    //#Sprint5 END
                    var PIC = RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: vInterestId, CoverageID: cp.CoverageId);
                    var PD = RetailRepository.GetDeductible(ProductCode, vInterestId, cp.CoverageId);
                    string DeductibleCode = string.Empty;
                    string DeductibleDesc = string.Empty;
                    if (PD != null)
                    {
                        DeductibleCode = PD.DeductibleCode;
                        DeductibleDesc = PD.Description;
                    }
                    if (CalculationResult != null)
                    {

                        int years = 0;
                        string TempCoverageId = "";
                        foreach (var item in CalculationResult)
                        {
                            years++;
                            if (!string.IsNullOrEmpty(TempCoverageId) && !TempCoverageId.Equals(item.CoverageID))
                            {
                                years = 1;
                            }
                            TempCoverageId = item.CoverageID;

                            PremiumScheme PS = new PremiumScheme();
                            var Discount = RetailRepository.GetWTDiscount(ProductCode, vMouID);// New
                            if (!string.IsNullOrWhiteSpace(OldPolicyNo))
                            {
                                var RenDiscount = RetailRepository.GetRenewalDiscount(OldPolicyNo);
                                var Disc = RetailRepository.GetWTDiscount(ProductCode, ProductCode);// New
                                foreach (WTCommission ren in RenDiscount)
                                {
                                    if (ren.CommissionID.Contains("RD"))
                                    {
                                        RDP.Add(ren.Percentage);
                                    }
                                }
                                foreach (WTCommission D in Disc)
                                {
                                    DP.Add(D.Percentage);
                                }
                                Discount.AddRange(RenDiscount);
                            }
                            List<CommissionScheme> DiscountList = RetailRepository.InitDiscountList(item.CoverageID, Discount, false);
                            if (DiscountList.Count > 0)
                            {
                                DiscountList = RetailRepository.InitFlatDiscount(ProductCode, item.Premium, DiscountList, item.SumInsured, 1, "DISCOUNT", false);
                            }
                            PS = RetailRepository.CalculateDiscountPremium(DiscountList, item.Premium);
                            #region remove same items
                            CalculatedPremiItems.RemoveAll(X => X.CoverageID.TrimEnd().Equals(item.CoverageID.TrimEnd()) && X.InterestID.TrimEnd().Equals(vInterestId) && X.Year == item.YearNumber);
                            #endregion

                            int AutoApply = string.IsNullOrEmpty(item.AutoApply) ? 0 : Convert.ToInt16(CoverGrp.Count > 1 ? AutoApplyExtGroupFlag : (!string.IsNullOrWhiteSpace(item.AutoApply) ? item.AutoApply.ToString() : string.Empty));
                            if (((PS.GrossPremium > 0)||(PS.GrossPremium==0&&item.Rate==-1))&& !string.IsNullOrEmpty(DeductibleCode))
                            {
                                CalculatedPremiItems.Add(new CalculatedPremi
                        {
                            InterestID = vInterestId,
                            CoverageID = cp.CoverageId,
                            //CoverageDesc = !string.IsNullOrWhiteSpace(item.Description) ? item.Description : PIC[0].Description,
                            //PeriodFrom = item.CoverFrom.ToString(),
                            //PeriodTo = item.CoverTo.ToString(),
                            PeriodFrom = item.CoverFrom,
                            PeriodTo = item.CoverTo,
                            DeductibleCode = DeductibleCode,
                            //DeductibleDesc = DeductibleDesc,
                            //Currency = PIC[0].DefaultCurrency,
                            //AgreedValue = item.AgreedValue,
                            //Interest = PIC[0].InterestDescription,
                            //Premium = item.Premium,
                            Premium = Convert.ToDouble(PS.Net3),
                            Rate = Convert.ToDouble(item.Rate),
                            //CoverageType = !string.IsNullOrWhiteSpace(item.CoverageType) ? item.CoverageType : string.Empty,
                            //BasicCoverageID = PIC[0].ComprehensiveFlag == true ? "ALLRIK" : "TLO",
                            AutoApply = (PS.GrossPremium > 0) ? AutoApply.ToString() : "0",
                            IsBundling = AutoApply > 0 ? true : false,
                            Net1 = PS.Net1,
                            Net2 = PS.Net2,
                            Net3 = PS.Net3,
                            GrossPremium = PS.GrossPremium,
                            CoverPremium = PS.GrossPremium,
                            ExcessRate = item.ExcessRate,
                            MaxSI = item.MaxSi,
                            Ndays = item.NDays,
                            EntryPrecentage = item.EntryPercentage,
                            LoadingRate = Convert.ToDouble(item.Loading),
                            Loading = Convert.ToDouble(item.Loading * PS.Net3 / 100),
                            SumInsured = Convert.ToDouble(item.SumInsured),
                            CalcMethod = calcmethod,
                            Year = years

                        });
                            }
                        }
                    }
                }
                DiscountPct = DP;
                RenDiscountPct = RDP;
                return CalculatedPremiItems;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        public static void updateFollowUpStatusToPolicyCreated(string FollowUpNo) {
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<dynamic> ls = db.Fetch<dynamic>("SELECT DocRep FROM dbo.FollowUp WHERE FollowUpNo = @0", FollowUpNo);
                if (ls.Count > 0) {
                    if (!string.IsNullOrEmpty(ls.First().DocRep)) {
                        string qGetStatus = @"SELECT FollowUpStatus, FollowUpInfo FROM dbo.FollowUp WHERE FollowUpNo = @0";
                        List<dynamic> sts = db.Fetch<dynamic>(qGetStatus, FollowUpNo);
                        string qUpdt = @"UPDATE dbo.FollowUp SET FollowUpInfo = @0 WHERE FollowUpNo = @1";
                        if (sts.Count > 0)
                        {
                            int info = Convert.ToInt32(sts.First().FollowUpInfo);
                            if (info == 53)
                            {
                                db.Execute(qUpdt, 50, FollowUpNo);
                            }
                            if (info == 54)
                            {
                                db.Execute(qUpdt, 51, FollowUpNo);
                            }
                            if (info == 55)
                            {
                                db.Execute(qUpdt, 52, FollowUpNo);
                            }
                        }
                    }
                }
            }
            catch (Exception e )
            {
       
                throw e;
            }
        }

        internal static List<dynamic> GetImageMobile(string followUpNo) {
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string query = @"SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data, COALESCE(id.CoreImage_ID, '') CoreImage_ID FROM 
                                (
                                SELECT 
	                                FollowUpNo,ImageName, ImageType
                                FROM 
                                (
                                select fu.FollowUpNo, IdentityCard, NPWP, SIUP, STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung, RenewalNotice, Survey, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5
                                from FollowUp fu
                                LEFT JOIN ProspectCompany pcm ON fu.CustID = pcm.CustID
                                INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = fu.FollowUpNo
                                where fu.followupno = @0 AND os.ApplyF = 1 AND os.RowStatus = 1
                                )a 
                                UNPIVOT (
	                                ImageName FOR ImageType IN (
		                                IdentityCard, NPWP, SIUP, STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung, RenewalNotice, Survey, BuktiBayar2, BuktiBayar3, BuktiBayar4, BuktiBayar5, DocNSA4, DocNSA5
	                                )
                                ) unpvt
                                where ImageName <> ''
                                ) a 
                                INNER JOIN ImageData id on ImageName = id.PathFile ";
                return AABMobileDB.Fetch<dynamic>(query, followUpNo);
            }
            catch (Exception e)
            {
                
                throw e;
            }
        }

        internal static List<dynamic> GetImageSurveyMobile(string followUpNo)
        {
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string query = @"SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data, COALESCE(id.CoreImage_ID, '') CoreImage_ID FROM 
                                (
                                SELECT 
	                                FollowUpNo,ImageName, ImageType
                                FROM 
                                (
                                select fu.FollowUpNo, IdentityCard, NPWP, STNK, BSTB1
                                from FollowUp fu
                                LEFT JOIN ProspectCompany pcm ON fu.CustID = pcm.CustID
                                INNER JOIN dbo.OrderSimulation os ON os.FollowUpNo = fu.FollowUpNo
                                where fu.followupno = @0 AND os.ApplyF = 1 AND os.RowStatus = 1
                                )a 
                                UNPIVOT (
	                                ImageName FOR ImageType IN (
		                                IdentityCard, NPWP, STNK, BSTB1
	                                )
                                ) unpvt
                                where ImageName <> ''
                                ) a 
                                INNER JOIN ImageData id on ImageName = id.PathFile ";
                return AABMobileDB.Fetch<dynamic>(query, followUpNo);
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public static List<string> GetSurveyDays(string BranchID)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                int svyMinDate = 1, svyMaxDate = 1;
                Int32.TryParse(GetApplicationParametersValue("OTOSALES-MIN-SURVEY-DATEADD")[0], out svyMinDate);
                Int32.TryParse(GetApplicationParametersValue("OTOSALES-MAX-SURVEY-DATEADD")[0], out svyMaxDate);
                CultureInfo ci = new CultureInfo("en-US");
                DateTime datefrom = DateTime.Now.AddDays(svyMinDate);
                DateTime dateto = DateTime.Now.AddDays(svyMaxDate);
                int days = (dateto - datefrom).Days;

                string mindate = datefrom.ToString("dd-MMM-yyyy", ci);
                string maxdate = dateto.ToString("yyyy/MM/dd", ci);

                List<string> dates = new List<string>();
                dates.Add(datefrom.ToString("dd-MM-yyyy", ci));
                for (int cal = 1; cal <= days; cal++)
                {
                    dates.Add(datefrom.AddDays(cal).ToString("dd-MM-yyyy", ci));
                }
                var asuransiastraDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB);
                string qGetBranchType = @"SELECT [Type] FROM dbo.Branch WHERE ID = @0";
//                string qGetBranchType = @"SELECT DISTINCT b.Type
//                                FROM Branch b
//                                INNER JOIN city c ON b.CITYID = c.ID
//							    WHERE CityId = @0
//                                AND b.type IN ( 2, 5, 6 )					
//                                AND b.ISDELETED = 0";
                List<dynamic> ls = asuransiastraDB.Fetch<dynamic>(qGetBranchType, BranchID);
                int type = 0;
                if (ls.Count > 0) {
                    type = Convert.ToInt32(ls.First().Type);
                }

                List<string> DateConfig = GetApplicationParametersValue("OTOSALES-SURVEYDATE-ALLOWHOLIDAY");
                string[] listBranchType = DateConfig[0].Split(',');
                string[] listFlagAllow = DateConfig[1].Split(',');
                bool isAllowHoliday = false;
                for (int i = 0; i < listBranchType.Length; i++)
                {
                    if (Convert.ToInt32(listBranchType[i]) == type) {
                        if (Convert.ToInt32(listFlagAllow[i]) == 1) {
                            isAllowHoliday = true;
                        }
                    }
                }
                if (!isAllowHoliday) {
                    List<dynamic> offDates = GetOffDayDate(datefrom, dateto);
                    foreach (dynamic offDate in offDates)
                    {
                        if (dates.Contains(offDate.OffDate.ToString()))
                        {

                            dates.Remove(offDate.OffDate.ToString());
                        }
                    }                
                }

                return dates;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace +","+e.ToString());
                throw e;
            }
        }

        public static List<dynamic> GetOffDayDate(DateTime dateform, DateTime dateto)
        {
            string QueryGetHoliday = @";DECLARE @@dt1 DateTime = @0
;DECLARE @@dt2 DateTime = @1
;WITH    ctedaterange
          AS ( SELECT   [Dates] = @@dt1
               UNION ALL
               SELECT   [dates] + 1
               FROM     ctedaterange
               WHERE    [dates] + 1 <= @@dt2
             )
    SELECT 
                CAST(DAY([dates]) AS VARCHAR(2)) + '-'            
                + CAST(MONTH([dates]) AS VARCHAR(2)) + '-'                
                + RIGHT(CAST(YEAR([dates]) AS VARCHAR(4)), 4)
            AS OffDate            
    FROM    ctedaterange
    WHERE   DATENAME(DW, dates) IN ( 'Sunday', 'Saturday' )
    UNION
    SELECT    
                CAST(DAY([HolidayDate]) AS VARCHAR(2)) + '-'
                + CAST(MONTH([HolidayDate]) AS VARCHAR(2)) + '-'                
                + RIGHT(CAST(YEAR([HolidayDate]) AS VARCHAR(4)), 4)
            AS OffDate                
    FROM    aabmobile.dbo.holiday
    WHERE   HolidayDate BETWEEN @@dt1 AND @@dt2
OPTION  ( MAXRECURSION 0 )";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            List<dynamic> offDate = new List<dynamic>();
            offDate = db.Fetch<dynamic>(
                QueryGetHoliday,
                dateform.ToString("yyyy-M-d", new CultureInfo("en-US")),
                dateto.ToString("yyyy-M-d", new CultureInfo("en-US")));
            return offDate;
        }
        internal static decimal CalculateNCB(double GrossPremi, List<decimal> RenDiscountPct)
        {

            if (RenDiscountPct.Count > 0)
            {

            GrossPremi = GrossPremi * Convert.ToDouble(RenDiscountPct[0]) / 100;

            return Convert.ToDecimal(GrossPremi);
            }
            else
            {

            return Convert.ToDecimal(0);
            }
        }
        internal static decimal CalculateDiscountPremi(double GrossPremi, List<decimal> DiscountPct)
        {

            if (DiscountPct.Count > 0)
            {

                GrossPremi = GrossPremi * Convert.ToDouble(DiscountPct[0]) / 100;

                return Convert.ToDecimal(GrossPremi);
            }
            else
            {

                return Convert.ToDecimal(0);
            }
        }

        //internal static bool isAccessInclude(string calculateditems)
        //{
        //    try
        //    {
        //        bool isexist = false;
        //        List<CalculatedPremi> temp = JsonConvert.DeserializeObject<List<CalculatedPremi>>(calculateditems);
        //        for (int a = temp.Count(); a >= temp.Count(); a--)
        //        {
        //            if (temp[a = 1].InterestID.Equals("ACCESS"))
        //            {
        //                isexist = true;
        //            }
        //        }
        //        return isexist;
        //        }
        //    catch (Exception e)
        //    {
        //        throw (e);
        //    }
        //}

        public static List<CalculatedPremi> CalculateBasicPremi(List<CoverageParam> CoverageList,string ProductCode, string vcitycode, string vusagecode,string vBrand,string vtype, string vModel,string vIsNew,
                                                                   string vsitting, string vyear, DateTime PeriodFrom, DateTime PeriodTo, string Ndays, decimal vTSInterest, decimal vPrimarySI, string vInterestId,
                                                                    string OldPolicyNo, out List<decimal> RenDiscountPct,out bool isProductSupported)
        {
            List<CalculatedPremi> result = new List<CalculatedPremi>();
            List<DetailScoring> dtlScoring = new List<DetailScoring>();
            List<decimal> RDPCT = new List<decimal>();
            bool IPS = true;
            try {
                foreach (CoverageParam coverage in CoverageList)
                {

                    #region Get rate scoring
                    DetailScoring ds = new DetailScoring();
                    if (!string.IsNullOrEmpty(vcitycode))
                    {
                        ds.FactorCode = "GEOGRA";
                        ds.InsuranceCode = vcitycode;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vusagecode))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "MVUSAG";
                        ds.InsuranceCode = vusagecode;
                        dtlScoring.Add(ds);
                    }
                    ds = new DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    ds.InsuranceCode = !string.IsNullOrEmpty(vBrand) ? string.IsNullOrEmpty(vtype) ? "ALL   " : vtype : "";
                    dtlScoring.Add(ds);
                    if (!string.IsNullOrEmpty(vBrand))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "VBRAND";
                        ds.InsuranceCode = vBrand;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vIsNew))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "NEWVHC";
                        ds.InsuranceCode = vIsNew;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vModel))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "VMODEL";
                        ds.InsuranceCode = vModel;
                        dtlScoring.Add(ds);
                    }

                    #endregion
                    #region rate calculation
                    int calcmethod = PeriodTo.Date < PeriodFrom.Date.AddMonths(12) ? 1 : 0;// 2 : scale, 1 : prorate
                    ParamCalculatePremiPerCoverage pcppc = new ParamCalculatePremiPerCoverage();
                    pcppc.CalculationMethod = calcmethod;
                    pcppc.PeriodFrom = PeriodFrom;
                    pcppc.PeriodTo = PeriodTo;
                    pcppc.PeriodFromCover = coverage.PeriodFromCover;
                    pcppc.PeriodToCover = coverage.PeriodToCover;
                    pcppc.VehicleType = vtype;
                    pcppc.YearManufacturing = vyear;
                    pcppc.TSICurrID = "IDR";
                    pcppc.NumberOfSeat = vsitting;
                    //pcppc.MouID = vMouID;
                    pcppc.ProductCode = ProductCode;
                    pcppc.TSInterest = vTSInterest;
                    pcppc.PrimarySI = vPrimarySI;
                    pcppc.CoverageId = coverage.CoverageId;
                    pcppc.ScoringList = dtlScoring;
                    pcppc.InterestId = vInterestId;
                    pcppc.NDays = Ndays;

                    List<CalculateSumInsured> CalculationResult = pm.CalculateBasicPremium(pcppc);
                    int years = 0;
                    string TempCoverageId = "";
                    foreach (var item in CalculationResult)
                    {
                        years++;
                        if (!string.IsNullOrEmpty(TempCoverageId) && !TempCoverageId.Equals(item.CoverageID))
                        {
                            years = 1;
                        }
                        TempCoverageId = item.CoverageID;
                        var PIC = RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: vInterestId, CoverageID: item.CoverageID);
                        var PD = RetailRepository.GetDeductible(ProductCode, vInterestId, item.CoverageID);
                        string DeductibleCode = string.Empty;
                        string DeductibleDesc = string.Empty;
                        if (PD != null)
                        {
                            DeductibleCode = PD.DeductibleCode;
                            DeductibleDesc = PD.Description;
                        }
                        PremiumScheme PS = new PremiumScheme();
                        #region Discount

                        var Discount = RetailRepository.GetWTDiscount(ProductCode, ProductCode);
                        if (!string.IsNullOrWhiteSpace(OldPolicyNo))
                        {
                            var RenDiscount = RetailRepository.GetRenewalDiscount(OldPolicyNo);
                            foreach (WTCommission perc in RenDiscount)
                            {
                                RDPCT.Add(perc.Percentage);
                            }
                            Discount.AddRange(RenDiscount);
                        }
                        List<CommissionScheme> DiscountList = RetailRepository.InitDiscountList(item.CoverageID, Discount, false);
                        if (DiscountList.Count > 0)
                        {
                            DiscountList = RetailRepository.InitFlatDiscount(ProductCode, item.Premium, DiscountList, item.SumInsured, 1, "DISCOUNT", false);
                        }
                        PS = RetailRepository.CalculateDiscountPremium(DiscountList, item.Premium);
                        #endregion
                        // CalculatePremiPerCoverage(item.CoverageID, Convert.ToDouble(item.Premium));
                        
                        if (PS.NetPremium < 0)
                        {
                            IPS = false;
                            isProductSupported = IPS;
                            RenDiscountPct = RDPCT;
                            return result;
                        }

                        if (PS.GrossPremium > 0)
                        {
                            result.Add(new CalculatedPremi
                            {
                                InterestID = vInterestId,
                                CoverageID = item.CoverageID,
                                PeriodFrom = item.CoverFrom,
                                PeriodTo = item.CoverTo,
                                Rate = Convert.ToDouble(item.Rate),
                                ExcessRate = item.ExcessRate,
                                MaxSI = item.MaxSi,
                                Ndays = item.NDays,
                                Net1 = PS.Net1,
                                Net2 = PS.Net2,
                                Net3 = PS.Net3,
                                GrossPremium = PS.GrossPremium,
                                CoverPremium = PS.GrossPremium,
                                Premium = Convert.ToDouble(PS.Net3),
                                DeductibleCode = DeductibleCode,
                                EntryPrecentage = item.EntryPercentage,
                                Loading = Convert.ToDouble(item.Loading * PS.GrossPremium / 100),
                                LoadingRate = Convert.ToDouble(item.Loading),
                                SumInsured = Convert.ToDouble(item.SumInsured),
                                IsBundling = (PS.GrossPremium > 0) ? !string.IsNullOrWhiteSpace(item.AutoApply) ? Convert.ToInt16(item.AutoApply) > 0 ? true : false : false : false,
                                AutoApply = (PS.GrossPremium > 0) ? !string.IsNullOrWhiteSpace(item.AutoApply) ? item.AutoApply.ToString() : string.Empty : string.Empty,
                                CalcMethod = calcmethod,
                                Year = years
                                //LoadingRate 
                            });
                        }
                    }
                    #endregion
                }
                RenDiscountPct = RDPCT;
                isProductSupported = IPS;
                return result;
                
            }
            catch (Exception e) {
                throw (e);
            }
        }

        public static bool IsAllowedIEP(string productcode, bool isexistSFE)
        {
            bool isallowed = false;
            try
            {   var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            bool isProductIEP = db.ExecuteScalar<int>(@";select COUNT(*) from applicationparameters where paramvalue like '%" + productcode + "%' AND paramname='Otosales-segment-IEP'") > 0 ? true : false;
            if (isProductIEP)
            {

                if (isexistSFE)
                    {
                        return true;
                    }
                
            }
            else
            {
                isallowed = true;
            }
                return isallowed;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        internal static string isProductIEP(string ProductCode)
        {
            try
            {
                string result = "";
                List<string> MappingProductIEP=GetApplicationParametersValue("OTOSALES-IEP");
                if (MappingProductIEP[0].Contains(ProductCode))
                {
                    result = MappingProductIEP[1];
                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        #region 0198urf2019
        public static string GetCurrentMethod()
        {
            return System.Reflection.MethodBase.GetCurrentMethod().Name;
        }
        public static dynamic GetEmailTemplate(string OrderNo, int flag) {
            string actionName = GetCurrentMethod();
            dynamic res = new ExpandoObject();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qSelect = @";SELECT TOP 1 COALESCE(IsRenewal,0) IsRenewal, pt.Description ProductName,
                                f.ProspectName CustName, vb.Description Brand, vm.Description Model,
                                vt.Description [Type], so.Name, so.Email, so.Phone1
                                FROM dbo.OrderSimulation os 
                                INNER JOIN dbo.FollowUp f
                                ON f.FollowUpNo = os.FollowUpNo
                                INNER JOIN dbo.ProductType pt 
                                ON os.InsuranceType = pt.InsuranceType
                                INNER JOIN dbo.OrderSimulationMV omv
                                ON omv.OrderNo = os.OrderNo
                                LEFT JOIN dbo.VehicleBrand vb 
                                ON omv.BrandCode = vb.BrandCode
                                LEFT JOIN dbo.VehicleModel vm
                                ON omv.BrandCode = vm.BrandCode AND omv.ModelCode = vm.ModelCode
                                LEFT JOIN dbo.VehicleType vt 
                                ON vt.VehicleTypeCode = omv.Type
                                LEFT JOIN dbo.SalesOfficer so
                                ON so.SalesOfficerID = os.SalesOfficerID
                                WHERE os.OrderNo =@0";
                List<dynamic> ordData = db.Fetch<dynamic>(qSelect,OrderNo);
                if (ordData.Count > 0) {
                    int isNew = Convert.ToInt32(ordData.First().IsRenewal);
                    List<string> Subjects = GetApplicationParametersValue("SUBJECT-SEND-EMAIL");
                    string subject = Subjects[isNew];
                    subject = subject.Replace("#0", ordData.First().ProductName);
                    subject = subject.Replace("#1", ordData.First().CustName);
                    string body = "";
                    if (flag == 0)
                    {
                        List<string> Bodys = GetApplicationParametersValue("BODY-SEND-EMAIL");
                        body = Bodys[isNew];
                        body = body.Replace("#ProductName", ordData.First().ProductName);
                        body = body.Replace("#BrandName", ordData.First().Brand);
                        body = body.Replace("#Model", ordData.First().Model);
                        body = body.Replace("#Type", ordData.First().Type);
                        body = body.Replace("#SalesOfficerName", ordData.First().Name);
                        body = body.Replace("#SalesOfficerEmail", ordData.First().Email);
                        body = body.Replace("#SalesOfficerPhoneno", ordData.First().Phone1);
                    }
                    res.Subject = subject;
                    res.Body = body;
                    return res;
                }
                return null;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                
                throw e;
            }
        }
        public static A2isCommonResult SendViaEmail(string EmailTo, string EmailCC, string EmailBCC
            , string body, string subject, List<A2isMessagingAttachment> att, string OrderNo, bool IsAddAtt)
        {
            string actionName = GetCurrentMethod();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            A2isCommonResult res = new A2isCommonResult();
            try
            {
                string qGetInsType = @"SELECT COALESCE(InsuranceType,0) InsuranceType FROM dbo.OrderSimulation WHERE OrderNo = @0";
                int insuranceType = db.ExecuteScalar<int>(qGetInsType,OrderNo);
                if (att == null) {
                    att = new List<A2isMessagingAttachment>();
                }
                if (IsAddAtt) {
                    att.Add(Otosales.Repository.v0219URF2019.MobileRepository.GenerateReportFile(OrderNo, insuranceType));
                }
                res = MessageController.SendEmail(EmailTo, EmailCC, EmailBCC, body, subject, att);
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                                
                throw e;
            }
        }
        public static dynamic GetTemplate(string OrderNo, string Type)
        {
            string actionName = GetCurrentMethod();
            dynamic res = new ExpandoObject();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelect = @";SELECT TOP 1 COALESCE(IsRenewal,0) IsRenewal, RTRIM(pt.Description) ProductName,
								CASE WHEN isCompany = 1 THEN RTRIM(pcm.CompanyName)
								ELSE RTRIM(pc.Name) END CustName
								, RTRIM(PolicyOrderNo) PolicyOrderNo,RTRIM(os.PolicyNo) PolicyNo, 
								COALESCE(TotalPremium,0) AmountPremi, RTRIM(so.Name) NamaMarketing,
								RTRIM(Ext) ExtMarketing, RTRIM(COALESCE(pcm.PICname,'')) PICName, COALESCE(isCompany,0) isCompany
                                FROM dbo.OrderSimulation os 
                                INNER JOIN dbo.FollowUp f
                                ON f.FollowUpNo = os.FollowUpNo
                                INNER JOIN dbo.ProductType pt 
                                ON os.InsuranceType = pt.InsuranceType
								INNER JOIN dbo.SalesOfficer so
								ON so.SalesOfficerID = os.SalesOfficerID
								LEFT JOIN dbo.ProspectCustomer pc
								ON pc.CustID = os.CustID
                                LEFT JOIN dbo.ProspectCompany pcm
								ON pcm.CustID = os.CustID
								WHERE os.OrderNo = @0";
                List<dynamic> ordData = db.Fetch<dynamic>(qSelect, OrderNo);
                if (ordData.Count > 0)
                {
                    string query = "";
                    decimal AmountPremi = 0;
                    DateTime tglPembayaran = DateTime.MinValue;
                    List<dynamic> lsOrder = new List<dynamic>();
                    if (!string.IsNullOrEmpty(ordData.First().PolicyOrderNo)) {
                        lsOrder = aabdb.Fetch<dynamic>("SELECT * FROM dbo.Mst_Order WHERE Order_No = @0", ordData.First().PolicyOrderNo);
                    }
                    if (lsOrder.Count > 0)
                    {
                        query = @";DECLARE @@PolicyFee AS NUMERIC(20, 4)
                                    DECLARE @@PolicyID AS VARCHAR(12)
                                    DECLARE @@EndorsementNo AS INT
                                    SELECT @@PolicyFee =ISNULL(Policy_Fee+Stamp_Duty,0),@@PolicyID=Policy_Id,@@EndorsementNo =Endorsement_No 
                                    FROM dbo.Mst_Order where Order_No = @0
                                    SELECT  COALESCE(ISNULL(SUM(net_premium), 0)
                                            + @@PolicyFee
                                            + ( SELECT  ISNULL(SUM(Net_Premium
                                                                    + Policy_Fee
                                                                    + Stamp_Duty), 0)
                                                FROM    dbo.Premium AS p ( NOLOCK )
                                                WHERE   policy_id = @@PolicyID
                                                        AND Endorsement_No <> @@EndorsementNo
                                                ),0) PremiumAmount
                                    FROM    dbo.Ord_Dtl_Coverage AS odc ( NOLOCK )
                                    WHERE   Order_No = @0";
                        AmountPremi = aabdb.ExecuteScalar<decimal>(query, ordData.First().PolicyOrderNo);
                        if (AmountPremi == 0)
                        {
                            AmountPremi = Convert.ToDecimal(ordData.First().AmountPremi);
                        }
                        query = @"SELECT DATEADD(DAY,13,Order_Date) AS DueDate FROM dbo.Mst_Order where Order_No = @0";
                        tglPembayaran = aabdb.ExecuteScalar<DateTime>(query, ordData.First().PolicyOrderNo);
                    }
                    else
                    {
                        AmountPremi = Convert.ToDecimal(ordData.First().AmountPremi);
                    }
                    query = @"SELECT ChasisNumber,EngineNumber FROM dbo.OrderSimulationMV WHERE OrderNo = @0";
                    List<dynamic> ls = db.Fetch<dynamic>(query, OrderNo);
                    if (ls.Count > 0)
                    {
                        query = @"SELECT TOP 1 os.CreatedDate FROM Asuransiastra.GODigital.OrderSimulationMV omv
                                    INNER JOIN Asuransiastra.GODigital.OrderSimulation os
                                    ON os.OrderNo = omv.OrderNo
                                    WHERE ChasisNumber = @0 AND EngineNumber = @1
                                    ORDER BY os.CreatedDate DESC";
                        List<dynamic> lsgodig = db.Fetch<dynamic>(query, ls.First().ChasisNumber, ls.First().EngineNumber);
                        if (lsgodig.Count > 0)
                        {
                            tglPembayaran = lsgodig.First().CreatedDate;
                        }
                    }
                    switch (Type) {
                        case "SendWA":
                            res = GetWATemplate(ordData, OrderNo, AmountPremi, tglPembayaran, 0);
                        break;
                        case "SendSMS":
                            res = GetSMSTemplate(ordData, OrderNo, AmountPremi, tglPembayaran, 0);
                        break;
                        case "ButtonWA":
                        res = GetWATemplate(ordData, OrderNo, AmountPremi, tglPembayaran, 1);
                        break;
                        case "ButtonSMS":
                        res = GetSMSTemplate(ordData, OrderNo, AmountPremi, tglPembayaran, 1);
                        break;
                    }
                    return res;
                }
                return null;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static dynamic GetWATemplate(List<dynamic> ordData, string OrderNo, decimal AmountPremi, DateTime tglPembayaran, int flag)
        {
            string actionName = GetCurrentMethod();
            try
            {
                string query = "";
                dynamic res = new ExpandoObject();
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                int isNew = Convert.ToInt32(ordData.First().IsRenewal);
                int isCompany = Convert.ToInt32(ordData.First().isCompany);
                List<string> Subjects = GetApplicationParametersValue("SUBJECT-SEND-WA");
                List<string> Templates = new List<string>();
                List<string> Bodys = GetApplicationParametersValue("BODY-SEND-WA");
                string EmailTo = GetApplicationParametersValue("EMAILTO-SEND-WA")[0];
                string subject = Subjects[isNew];
                if (isNew == 0)
                {
                    subject = subject.Replace("#NamaCustomer", ordData.First().CustName);
                    subject = subject.Replace("#OrderNo", ordData.First().PolicyOrderNo);
                }
                else
                {
                    subject = subject.Replace("#NamaCustomer", ordData.First().CustName);
                    subject = subject.Replace("#PolicyNo", ordData.First().PolicyNo);
                }
                query = @"SELECT OrderNo FROM dbo.OrderSimulation os
                            INNER JOIN dbo.Fn_AABSplitString(
                            (SELECT ParamValue 
                            FROM Asuransiastra.GODigital.ApplicationParameters 
                            WHERE ParamName = 'godig-segment-code'),',') gdg
                            ON gdg.Data = os.SegmentCode AND os.ApplyF = 1
                            WHERE os.OrderNo = @0";
                List<dynamic> isGodig = db.Fetch<dynamic>(query, OrderNo);
                string template = "";
                if (isGodig.Count > 0)
                {
                    Templates = GetApplicationParametersValue("TEMPLATE-GODIG-SEND-WA");
                    template = Templates[0];
                }
                else
                {
                    Templates = GetApplicationParametersValue("TEMPLATE-NONGODIG-SEND-WA");
                    template = Templates[isCompany];
                }
                if (isCompany == 0)
                {
                    template = template.Replace("#NamaCustomer", ordData.First().CustName);
                    template = template.Replace("#Produk", ordData.First().ProductName);
                    template = template.Replace("#AmountPremi", ToMoney(AmountPremi));
                    template = template.Replace("#ExtMarketing", ordData.First().ExtMarketing);
                    template = template.Replace("#NamaMarketing", ordData.First().NamaMarketing);
                    if (tglPembayaran != DateTime.MinValue)
                    {
                        template = template.Replace("#TanggalPembayaran", tglPembayaran.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")));
                    }
                    else
                    {
                        template = template.Replace("#TanggalPembayaran", "");
                    }
                }
                else
                {
                    template = template.Replace("#NamaCustomer", ordData.First().PICName);
                    template = template.Replace("#NamaPerusahaan", ordData.First().CustName);
                    template = template.Replace("#Produk", ordData.First().ProductName);
                    template = template.Replace("#AmountPremi", ToMoney(AmountPremi));
                    template = template.Replace("#ExtMarketing", ordData.First().ExtMarketing);
                    template = template.Replace("#NamaMarketing", ordData.First().NamaMarketing);
                    if (tglPembayaran != DateTime.MinValue)
                    {
                        template = template.Replace("#TanggalPembayaran", tglPembayaran.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")));
                    }
                    else
                    {
                        template = template.Replace("#TanggalPembayaran", "");
                    }
                }

                string body = Bodys[isNew];
                if (isNew == 0)
                {
                    body = body.Replace("#OrderNo", ordData.First().PolicyOrderNo);
                    body = body.Replace("#NamaMarketing", ordData.First().NamaMarketing);
                    if (flag == 0)
                    {
                        body = body.Replace("#TemplateWA", template);
                    }
                    else {
                        body = body.Replace("#TemplateWA", "<br/>");
                    }
                }
                else
                {
                    body = body.Replace("#PolicyNo", ordData.First().PolicyNo);
                    body = body.Replace("#NamaMarketing", ordData.First().NamaMarketing);
                    if (flag == 0)
                    {
                        body = body.Replace("#TemplateWA", template);
                    }
                    else
                    {
                        body = body.Replace("#TemplateWA", "<br/>");
                    }
                }
                res.EmailTo = EmailTo;
                res.Subject = subject;
                res.Body = body;
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                
                throw e;
            }
        }

        public static dynamic GetSMSTemplate(List<dynamic> ordData, string OrderNo, decimal AmountPremi, DateTime tglPembayaran, int flag)
        {
            string actionName = GetCurrentMethod(); 
            try
            {
                string query = "";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                dynamic res = new ExpandoObject();
                int isCompany = Convert.ToInt32(ordData.First().isCompany);
                List<string> Bodys = GetApplicationParametersValue("BODY-SEND-SMS");
                query = @"SELECT OrderNo FROM dbo.OrderSimulation os
                            INNER JOIN dbo.Fn_AABSplitString(
                            (SELECT ParamValue 
                            FROM Asuransiastra.GODigital.ApplicationParameters 
                            WHERE ParamName = 'godig-segment-code'),',') gdg
                            ON gdg.Data = os.SegmentCode AND os.ApplyF = 1
                            WHERE os.OrderNo = @0";
                List<dynamic> isGodig = db.Fetch<dynamic>(query, OrderNo);
                string body = "";
                if (isGodig.Count > 0)
                {
                    body = "";
                }
                else
                {
                    if (flag == 0) {
                        body = Bodys[isCompany];
                        if (isCompany == 0)
                        {
                            body = body.Replace("#NamaCustomer", ordData.First().CustName);
                            body = body.Replace("#Produk", ordData.First().ProductName);
                            body = body.Replace("#AmountPremi", ToMoney(AmountPremi));
                            body = body.Replace("#ExtMarketing", ordData.First().ExtMarketing);
                            body = body.Replace("#NamaMarketing", ordData.First().NamaMarketing);
                            if (tglPembayaran != DateTime.MinValue)
                            {
                                body = body.Replace("#TanggalPembayaran", tglPembayaran.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")));
                            }
                            else
                            {
                                body = body.Replace("#TanggalPembayaran", "");
                            }
                        }
                        else
                        {
                            body = body.Replace("#NamaCustomer", ordData.First().PICName);
                            body = body.Replace("#NamaPerusahaan", ordData.First().CustName);
                            body = body.Replace("#Produk", ordData.First().ProductName);
                            body = body.Replace("#AmountPremi", ToMoney(AmountPremi));
                            body = body.Replace("#ExtMarketing", ordData.First().ExtMarketing);
                            body = body.Replace("#NamaMarketing", ordData.First().NamaMarketing);
                            if (tglPembayaran != DateTime.MinValue)
                            {
                                body = body.Replace("#TanggalPembayaran", tglPembayaran.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID")));
                            }
                            else
                            {
                                body = body.Replace("#TanggalPembayaran", "");
                            }
                        }
                    }
                }
                res.Body = body;
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                
                throw e;
            }
        }
        public static string ToMoney(decimal quantity)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string result = string.Empty;
            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB))
                {
                    string query = @"SELECT format(@0,'#,###', 'DE-de')";
                    result = db.Query<string>(query, quantity).SingleOrDefault();
                    result = string.IsNullOrWhiteSpace(result) ? "0" : result;
                    result = "Rp " + result + ",00";
                }
            }
            catch (Exception Ex)
            {

                logger.Error(String.Format("Function {0} Error {1}", CurrentMethod, Ex));
            }
            return result;
        }

        public static void UpdateEmail(string orderno, string email)
        {
            string actionName = GetCurrentMethod();
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(@"
                                SELECT CAST(COALESCE(isCompany,0) AS BIT) isCompany, os.CustID, COALESCE(pc.CustIDAAB,'') CustIDAAB, os.SalesOfficerID
								FROM dbo.OrderSimulation os
                                INNER JOIN dbo.ProspectCustomer pc
                                ON pc.CustID = os.CustID
                                WHERE OrderNo = @0", orderno);
                string qUpdate = @"";
                if (ordData.Count > 0)
                {
                    AABRepository.InsertCustHistory(ordData.First().CustIDAAB, "", "", ordData.First().SalesOfficerID, email);
                    bool isCompany = ordData.First().isCompany;
                    if (isCompany)
                    {
                        qUpdate = @"UPDATE dbo.ProspectCustomer
                                        SET Email1 = @1
                                        WHERE CustID = @0

                                        UPDATE dbo.ProspectCompany 
                                        SET Email = @1
                                        WHERE CustID = @0";
                        mobiledb.Execute(qUpdate, ordData.First().CustID, email);
                    }
                    else {
                        qUpdate = @"UPDATE dbo.ProspectCustomer
                                        SET Email1 = @1
                                        WHERE CustID = @0";
                        mobiledb.Execute(qUpdate, ordData.First().CustID, email);
                    }
                    if (!string.IsNullOrEmpty(ordData.First().CustIDAAB)) {
                        qUpdate = @"UPDATE dbo.Mst_Customer 
                                    SET Email = @1
                                    WHERE Cust_Id = @0";
                        aabdb.Execute(qUpdate, ordData.First().CustIDAAB, email);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);                                
                throw e;
            }
        }

        public static dynamic GetAVAYAUserTelerenewal(string SalesOfficerID) {
            string actionName = GetCurrentMethod();
            try
            {
                using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    string query = @"SELECT SalesOfficerId,Name,Password FROM dbo.Mst_AVAYAUserTelerenewal WHERE RowStatus = 1 AND SalesOfficerId = @0";
                    List<dynamic> result = db.Fetch<dynamic>(query, SalesOfficerID);
                    return result;
                }
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Function {0} Error {1}", actionName, e));
                throw e;
            }
        }
        #endregion



        internal static List<CalculatedPremi> RecalculateAccess(decimal vTSIInterest,string vCoverageId, List<CalculatedPremi> calpremiitems, string ProductCode,DateTime PeriodFrom, DateTime PeriodTo,
                                                                string vtype, string vyear, string vsitting, string pQuotationNo, List<DetailScoring> dtlScoring, string OldPolicyNo, out List<decimal> RenDiscountPct, out  List<decimal> DiscountPct)
        {
            try
            {
                List<decimal> RPT = new List<decimal>();
                List<decimal> DT = new List<decimal>();
                bool isaccessapplied = false;
                double suminsuredaccess = 0;
                foreach (CalculatedPremi item in calpremiitems)
                {
                    if (item.InterestID.Equals("ACCESS") && (item.CoverageID.Trim().Equals("ALLRIK") || item.CoverageID.Trim().Equals("TLO")))
                    {
                        isaccessapplied = true;
                        suminsuredaccess = item.SumInsured;
                        break;
                    }
                }
                if (isaccessapplied)
                {
                    //GET ACCESS ALLRIK AND TLO
                    List<CalculatedPremi> cascoaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(JsonConvert.SerializeObject(calpremiitems));
                    cascoaccess.RemoveAll(x => !x.InterestID.Trim().Equals("ACCESS") && (!x.CoverageID.Trim().Equals("ALLRIK") || !x.CoverageID.Trim().Equals("TLO")));
                   //
                    // GET NON ACCESS ITEM
                    calpremiitems.RemoveAll(x => x.InterestID.Trim().Equals("ACCESS"));
                    //
                    List<CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(JsonConvert.SerializeObject(calpremiitems));
                    List<CalculatedPremi> cItems = MobileRepository.CalculateACCESS(vTSIInterest, Convert.ToDecimal(suminsuredaccess), ProductCode, PeriodFrom, PeriodTo, "ACCESS", vtype, vyear, vsitting, pQuotationNo, calculateaccess, dtlScoring, OldPolicyNo, out RenDiscountPct, out DiscountPct);
                    if (new List<string>() { "SRCC", "FLD", "ETV", "EQK" }.Contains(vCoverageId))
                    {
                        cItems.RemoveAll(x => x.InterestID.Equals("ACCESS") && x.IsBundling);
                    }
                    else
                    {
                        cItems.RemoveAll(x => x.InterestID.Equals("ACCESS") && (!x.CoverageID.Trim().Equals("TRS") && !x.CoverageID.Trim().Equals("TRRTLO")));
                    }
                    //REMOVE ACCESS ALLRIK OR TLO RESULT
                    cItems.RemoveAll(x => x.InterestID.Equals("ACCESS") && (x.CoverageID.Trim().Equals("ALLRIK") || x.CoverageID.Trim().Equals("TLO")));
                    
                    //JOIN ACCESS
                    cascoaccess.AddRange(cItems);
                    // JOIN ALL
                    calpremiitems.AddRange(cascoaccess);
                    return calpremiitems ;
                }
                else
                {
                    RenDiscountPct = RPT;
                    DiscountPct = DT;
                    return calpremiitems;
                }
                
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public dynamic getSalesmanDealerName(string dealercode) {
            try
            {
                string query = @"
                      SELECT * FROM SalesmanDealer where DealerCode = @0 AND RowStatus = 1 ORDER BY Description ASC
                    ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, dealercode);

                    return result; 
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        public dynamic getBranchList(string search) {
            try
            {
                string query = @"
                 SELECT * FROM Branch WHERE Address <> '' 
                 AND (Name like @0 OR City like @0) ORDER BY Name ASC
                ";

                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                {
                    var result = db.Fetch<dynamic>(query, search);

                    return result; 
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}