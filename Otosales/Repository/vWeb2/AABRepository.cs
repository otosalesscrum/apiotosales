﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using Otosales.Codes;
using Otosales.dta;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;

namespace Otosales.Repository.vWeb2
{
    public interface IAABRepository {
        List<dynamic> GetSegmentCode(string param);
    }
    public class AABRepository : IAABRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        #region private query
        private static string qUpdatePhoneCompanyABB = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @2 AND CustID = @0)
                                                    BEGIN
	                                                    UPDATE dbo.Mst_Customer_AdditionalInfo SET AdditionalInfoValue = @1 WHERE AdditionalCode = @2 AND CustID = @0
                                                    END
                                                    ELSE
                                                    BEGIN
	                                                    INSERT INTO dbo.Mst_Customer_AdditionalInfo
	                                                            ( CustID ,
	                                                              AdditionalCode ,
	                                                              AdditionalInfoValue ,
	                                                              CreatedBy ,
	                                                              CreatedDate ,
	                                                              RowStatus
	                                                            )
	                                                    VALUES  ( @0 , -- CustID - char(11)
	                                                              @2 , -- AdditionalCode - varchar(5)
	                                                              @1 , -- AdditionalInfoValue - varchar(255)
	                                                              'OTOSL' , -- CreatedBy - varchar(50)
	                                                              GETDATE() , -- CreatedDate - datetime
	                                                              0  -- RowStatus - smallint
	                                                            )
                                                    END";
        private static string qInsertUpdateValidation = @"IF EXISTS(SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND App_Source = 'otosales')
                                    BEGIN
	                                    UPDATE dbo.Mst_Customer_Data_Validation SET Status = @2, RelationshipId = @4, PICName = @5,
										CallRejectReasonId = @8, Verified_Date = @7, Verified_By = @3, Created_Date = GETDATE()
	                                    WHERE Cust_Id = @0 AND Column_Name = @1 AND App_Source = 'otosales'
                                    END
                                    ELSE
                                    BEGIN
	                                    INSERT INTO dbo.Mst_Customer_Data_Validation
	                                            ( Cust_Id ,
	                                                Column_Name ,
	                                                App_Source ,
	                                                Status ,
	                                                Verified_By ,
	                                                Verified_Date,
													RelationshipId,
													PICName,
													Created_By,
													Created_Date,
													CallRejectReasonId 
	                                            )
	                                    VALUES  ( @0 , -- Cust_Id - char(11)
	                                                @1 , -- Column_Name - varchar(5)
	                                                'otosales' , -- App_Source - char(10)
	                                                @2 , -- Status - nchar(1)
	                                                @3 , -- Verified_By - char(5)
	                                                @7,  -- Verified_Date - datetime
													@4,
													@5,
													@6,
													GETDATE(),
													@8
	                                            )
                                    END";
        #endregion
        public static List<dynamic> GetBank(string ParamSearch)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = "SELECT Bank_Id,BankName FROM dbo.Mst_Bank WHERE Status = 1 AND BankName LIKE '%{0}%'";
                List<dynamic> result = db.Fetch<dynamic>(string.Format(query, ParamSearch));
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public //static 
            List<dynamic> GetSegmentCode(string param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = @"SELECT mps.SOB_ID, ms.Name FROM dbo.Mst_SOB ms 
                        INNER JOIN dbo.Mapping_Product_SOB mps
                        ON mps.SOB_ID = ms.SOB_ID WHERE Product_code = @0 AND  ms.status=1  and mps.RowStatus=0
                        ORDER BY mps.SOB_ID";
                List<dynamic> result = db.Fetch<dynamic>(query, param);
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static Dictionary<string, List<SurveyDocument>> GetSurveyDocument(string param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = @"SELECT dsa.Survey_No, dsai.Image_ID,dsai.[image_Name] AS File_Name, Doc_DESCRIPTION, CASE 
						WHEN (dsai.Image_ID  = '' OR  dsai.Image_ID is null)  THEN '' 
						ELSE mi.[Image] END AS [Image], Document_Type 
						FROM dbo.dtl_survey_acceptance dsa
                        INNER JOIN dbo.dtl_survey_acceptance_Image dsai on dsa.Survey_No = dsai.Survey_No
                        AND dsa.Doc_No = dsai.Doc_No 
                        INNER JOIN dbo.Mst_Image mi on mi.Image_Id = dsai.Image_ID 
                        WHERE dsa.Survey_No = @0
                        ORDER BY Document_Type";
                List<SurveyDocument> list = db.Query<SurveyDocument>(query, param).ToList();
                Dictionary<string, List<SurveyDocument>> res = new Dictionary<string, List<SurveyDocument>>();
                List<SurveyDocument> listTemp = new List<SurveyDocument>();
                string temp = "";
                int count = 0;
                foreach (SurveyDocument sd in list)
                {
                    count = count + 1;
                    if (!sd.Document_Type.Contains("acceptance"))
                    {
                        sd.Doc_DESCRIPTION = "";
                    }
                    sd.Image = DecodeFromAAB2000ImageBytes(sd.Image);
                    if (!temp.ToLower().Equals(sd.Document_Type.ToLower()))
                    {
                        if (!string.IsNullOrEmpty(temp))
                        {
                            res.Add(temp, listTemp);
                        }
                        temp = sd.Document_Type;
                        listTemp = new List<SurveyDocument>();
                    }
                    listTemp.Add(sd);
                    if (count == list.Count)
                    {
                        res.Add(temp, listTemp);
                    }
                }
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static bool GetEnableDisableSalesmanDealer(string ProductCode, string BranchCode)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = "SELECT REPLACE(code, ' ', '') AS code FROM dbo.Mst_General WHERE type = 'PSO'";
                List<string> ListPSO = db.Fetch<string>(query);
                query = "SELECT REPLACE(code, ' ', '') AS code FROM dbo.Mst_General WHERE type = 'BSO'";
                List<string> ListBSO = db.Fetch<string>(query);
                if (ListPSO.Contains(ProductCode.Trim()) && ListBSO.Contains(BranchCode.Trim()))
                {
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static CheckDoubleInsuredResult GetCheckDoubleInsured(string ChasisNo, string EngineNo, string OrderNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            CheckDoubleInsuredResult result = new CheckDoubleInsuredResult();
            try
            {
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                query = @"SELECT CAST(COALESCE(f.IsRenewal,0) AS BIT) AS IsRenewal, 
						PeriodFrom, PeriodTo, PolicyOrderNo, COALESCE(os.PolicyNo,'') PolicyNo
						FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        WHERE os.OrderNo = @0";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(query, OrderNo);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                query = @";SELECT mo.Order_No, mo.Policy_No, 
                        p.Period_From, p.Period_To, 
						DATEDIFF(DAY,GETDATE(),p.Period_To) AS Days,
                        mo.Order_Type
                        FROM dbo.Dtl_MV dm
						INNER JOIN dbo.Policy p
						ON p.Policy_Id = dm.Policy_Id
						INNER JOIN dbo.Mst_Order mo
						ON mo.Order_No = p.Order_No
                        WHERE (dm.Chasis_Number = @0
                        OR dm.Engine_Number = @1)
						AND p.Status = 'A'
						AND p.Policy_No NOT IN (@2)
						AND p.Order_Status NOT IN('6','7','8')
						AND DATEDIFF(DAY,GETDATE(),p.Period_To) > 0";
                List<dynamic> res = db.Fetch<dynamic>(query, ChasisNo, EngineNo
                    , ordData.First().PolicyNo == null ? "" : ordData.First().PolicyNo);
                result.IsDoubleInsured = false;
                if (res.Count > 0)
                {
                    if (!ordData.First().IsRenewal)
                    {
                        result.IsDoubleInsured = true;
                    }
                    else
                    {
                        //if (Convert.ToInt32(res.First().Days) < 31)
                        //{
                        //    result.IsDoubleInsured = false;
                        //}
                        //else
                        //{
                        //    result.IsDoubleInsured = true;
                        //}
                        foreach (dynamic p in res)
                        {
                            if (Convert.ToInt32(p.Days) > 30)
                            {
                                result.IsDoubleInsured = true;
                                result.OrderNo = p.Order_No;
                                result.PolicyNo = p.Policy_No;
                                result.PeriodFrom = p.Period_From;
                                result.PeriodTo = p.Period_To;
                            }
                        }
                    }
                }
                else
                {
                    query = @"SELECT mo.Order_No, mo.Policy_No, 
                        mo.Period_From, mo.Period_To,
                        mo.Order_Type
                        FROM dbo.Mst_Order mo
                        INNER JOIN dbo.Ord_Dtl_MV odm
                        ON odm.Order_No = mo.Order_No						
                        WHERE (odm.Chasis_Number = @0
                        OR odm.Engine_Number = @1)
                        AND mo.Order_Status NOT IN (9,6)
						AND (mo.Period_To BETWEEN @2 AND @3
                        OR mo.Period_From BETWEEN @2 AND @3)
						AND mo.Order_No NOT IN (@4)
						AND Order_Status NOT IN('6','7','8')
						AND Order_Type NOT IN('4','6')
						AND Endorsement_No = (SELECT MAX(Endorsement_No) FROM 
						dbo.Mst_Order WHERE Policy_No = mo.Policy_No)";
                    res = db.Fetch<dynamic>(query, ChasisNo, EngineNo, ordData.First().PeriodFrom
                        , ordData.First().PeriodTo, ordData.First().PolicyOrderNo == null ? "" : ordData.First().PolicyOrderNo);
                    if (res.Count > 0)
                    {
                        result.IsDoubleInsured = true;
                        result.OrderNo = res.First().Order_No;
                        result.PolicyNo = res.First().Policy_No;
                        result.PeriodFrom = res.First().Period_From;
                        result.PeriodTo = res.First().Period_To;
                    }
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static CheckDoubleInsuredResult GetCheckDoubleInsuredDirtyData(string PolicyNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string query = "";
            CheckDoubleInsuredResult result = new CheckDoubleInsuredResult();

            try
            {
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);

                OrderSimulationMVRenot orderSimulationMV = new OrderSimulationMVRenot();
                OrderSimulationRenot orderSimulation = new OrderSimulationRenot();


                #region Query Order Simulation
                query = @"SELECT 
		NULL AS NewPolicyId,
		rn.New_Policy_No AS NewPolicyNo,
		rn.VANumber AS VANumber,
		'' AS QuotationNo,
		0 AS MultiYearF,
		1 AS YearCoverage,
		CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
		CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
		rn.Branch_Id AS BranchCode,
		CASE WHEN mpr.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END 
		END AS SalesOfficerID,
		'' AS PhoneSales,
		rn.Dealer_Code AS DealerCode,
		rn.Salesman_Dealer_Code AS SalesDealer,
		'GARDAOTO' AS ProductTypeCode,
		rp.Policy_Fee AS AdminFee,
		rp.gross_premium AS TotalPremium,
		mc.Home_Phone1 AS Phone1,
		mc.Home_Phone2 AS Phone2,
		mc.Email AS Email1,
		'' AS Email2,
		0 AS SendStatus,
		CASE WHEN mp.cob_id = '404' THEN 2 
		WHEN PDT.product_code is not null THEN 3 ELSE 1 END AS InsuranceType,
		1 AS ApplyF,
		0 AS SendF,
		(SELECT top 1 Interest_no from Ren_Dtl_Interest where policy_no = @0) AS LastInterestNo,
		(SELECT top 1 coverage_no from Ren_Dtl_Coverage where policy_no = @0) AS LastCoverageNo,
		rn.New_Product_code AS ProductCode,
        DATEADD(YEAR, 1,a.Period_From) AS PeriodFrom,
        DATEADD(YEAR, 1,a.Period_To) AS PeriodTo,
        rn.segment_code AS SegmentCode,
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms WITH ( NOLOCK ) ON ms.salesman_id = rn.Salesman_Id 
LEFT JOIN Ren_Dtl_Coverage rdc1 WITH ( NOLOCK ) ON  rdc1.policy_no = rn.policy_no
AND rdc1.interest_id = 'CASCO' AND rdc1.coverage_id = 'TLO'
LEFT JOIN Ren_Dtl_Coverage rdc2 WITH ( NOLOCK ) ON  rdc2.policy_no = rn.policy_no
AND rdc2.interest_id = 'CASCO' AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN Ren_premium rp WITH ( NOLOCK ) ON  rp.policy_no = rn.policy_no
INNER JOIN mst_product mp on mp.product_code = rn.old_product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
inner JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.new_product_code = rn.new_product_code
left join dtl_address da on da.policy_id = a.policy_id AND da. address_type ='DELIVR'
WHERE a.policy_no = @0
		AND a.Status = 'A'";


                orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo);

                query = @"SELECT TOP 1
NULL AS NewPolicyId,
NULL AS NewPolicyNo,
NULL AS VANumber,
'' AS QuotationNo,
0 AS MultiYearF,
1 AS YearCoverage,
CASE WHEN rdc1.Coverage_Id is null THEN 0 ELSE 1 END AS TLOPeriod,
CASE WHEN rdc2.Coverage_Id is null THEN 0 ELSE 1 END AS ComprePeriod,
a.Branch_Id AS BranchCode,
CASE WHEN ms.User_Id_Otosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(ms.User_Id_Otosales)) END  AS SalesOfficerID,
'' AS PhoneSales,
a.Broker_Code AS DealerCode,
        mc.Cust_Id AS SalesDealer,
'GARDAOTO' AS ProductTypeCode,
rp.Policy_Fee AS AdminFee,
rp.net_premium AS TotalPremium,
mc.Home_Phone1 AS Phone1,
mc.Home_Phone2 AS Phone2,
mc.Email AS Email1,
'' AS Email2,
0 AS SendStatus,
CASE WHEN mp.cob_id = '404' THEN 2 
WHEN PDT.product_code is not null THEN 3 ELSE 1 END AS InsuranceType,
1 AS ApplyF,
0 AS SendF,
(SELECT max(Interest_no) from Dtl_Interest where policy_id = a.policy_id) AS LastInterestNo,
(SELECT max(coverage_no) from Dtl_Coverage where policy_id = a.policy_id) AS LastCoverageNo,
a.Product_Code [ProductCode],
DATEADD(YEAR, 1,a.Period_From) AS PeriodFrom,
DATEADD(YEAR, 1,a.Period_To) AS PeriodTo,
a.Segment_Code [SegmentCode],
da.Address AS PolicyDeliverAddress,
da.Postal_Code AS PolicyDeliverPostalCode,
da.Delivery_Code AS PolicyDeliveryType,
da.ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc  ON a.policy_holder_code = mc.cust_id
LEFT JOIN mst_salesman ms ON ms.salesman_id = a.Salesman_Id 
LEFT JOIN dbo.ComSales_Salesman AS css ON a.Broker_Code = css.Salesman_Code
LEFT JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
LEFT JOIN dtl_interest di ON di.policy_id = a.policy_id AND di.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc1 ON  rdc1.policy_id = a.policy_id 
AND rdc1.interest_no = di.interest_no AND rdc1.coverage_id = 'TLO'
LEFT JOIN dtl_interest di2 ON di2.policy_id = a.policy_id AND di2.interest_id = 'CASCO'
LEFT JOIN Dtl_Coverage rdc2 ON  rdc2.policy_id = a.policy_id
AND rdc2.interest_no = di2.interest_no AND rdc2.coverage_id = 'ALLRIK'
LEFT JOIN premium rp ON  rp.policy_id = a.policy_id
INNER JOIN mst_product mp on mp.product_code = a.product_code AND mp.Status = 1
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code
 AND PDT.Partner_ID = 'LEX' 
left join dtl_address da on da.policy_id = a.policy_id AND da. address_type ='DELIVR'
WHERE a.policy_no = @0
AND a.Status = 'A'";

                #endregion

                if (orderSimulation == null)
                {
                    orderSimulation = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(query, PolicyNo);
                }

                #region Query Order Simulation MV
                query = @";SELECT rdm.Object_No AS ObjectNo,
		'GARDAOTO' AS ProductTypeCode,
		rdm.Vehicle_Code AS VehicleCode,
		rdm.Brand_Code AS BrandCode,
		rdm.Model_Code AS ModelCode,
		rdm.Series  AS Series,
		rdm.Vehicle_Type AS [Type],
		rdm.Sitting_Capacity AS Sitting,
		rdm.Mfg_Year AS [Year],
		rdm.Geo_Area_Code AS CityCode,
		rdm.Usage_Code AS UsageCode,
		rdm.Market_Price AS SumInsured,
		ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Ren_Dtl_Non_Standard_Accessories] where policy_no = @0),0) AS AccessSI,
		rdm.Registration_Number AS RegistrationNumber,
		rdm.Engine_Number AS EngineNumber,
		rdm.Chasis_Number AS ChasisNumber,
		rdm.New_Car AS IsNew,
        rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_no = rn.Policy_No
WHERE a.policy_no = @0
		AND a.Status = 'A'";

                orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);


                query = @";SELECT rdm.Object_No AS ObjectNo,
'GARDAOTO' AS ProductTypeCode,
rdm.Vehicle_Code AS VehicleCode,
rdm.Brand_Code AS BrandCode,
rdm.Model_Code AS ModelCode,
rdm.Series  AS Series,
rdm.Vehicle_Type AS [Type],
rdm.Sitting_Capacity AS Sitting,
rdm.Mfg_Year AS [Year],
rdm.Geo_Area_Code AS CityCode,
rdm.Usage_Code AS UsageCode,
rdm.Market_Price AS SumInsured,
ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Dtl_Non_Standard_Accessories] where policy_id = a.policy_id),0) AS AccessSI,
rdm.Registration_Number AS RegistrationNumber,
rdm.Engine_Number AS EngineNumber,
rdm.Chasis_Number AS ChasisNumber,
rdm.New_Car AS IsNew,
rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                #endregion

                if (orderSimulationMV == null)
                {
                    orderSimulationMV = db.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(query, PolicyNo);
                }


                query = @";SELECT CAST(COALESCE(fu.IsRenewal,0) AS BIT) AS IsRenewal,
                        PeriodFrom, PeriodTo, PolicyOrderNo
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.OrderSimulationMV osmv
                        ON osmv.OrderNo = os.OrderNo
                        INNER JOIN dbo.FollowUp fu
                        ON fu.FollowUpNo = os.FollowUpNo
                        WHERE 
                        os.OldPolicyNo = @0";

                var ordData = mobiledb.FirstOrDefault<dynamic>(query, PolicyNo);

                query = @";SELECT mo.Order_No, mo.Policy_No, 
                        p.Period_From, p.Period_To, 
						DATEDIFF(DAY,GETDATE(),p.Period_To) AS Days,
                        mo.Order_Type
                        FROM dbo.Mst_Order mo
                        INNER JOIN dbo.Ord_Dtl_MV odm
                        ON odm.Order_No = mo.Order_No
						INNER JOIN dbo.Policy p
						ON p.Policy_No = mo.Policy_No
                        WHERE odm.Chasis_Number = @0
                        OR odm.Engine_Number = @1
                        AND mo.Order_Status = 9
						AND p.Status = 'A'";
                var res = db.FirstOrDefault<dynamic>(query, orderSimulationMV.ChasisNumber.Trim(), orderSimulationMV.EngineNumber.Trim());

                if (res != null)
                {
                    if (ordData != null && ordData.IsRenewal)
                    {
                        result.IsDoubleInsured = true;
                    }
                    else
                    {
                        if (Convert.ToInt32(res.Days) < 31)
                        {
                            result.IsDoubleInsured = false;
                        }
                        else
                        {
                            result.IsDoubleInsured = true;
                        }
                    }

                    result.OrderNo = res.Order_No;
                    result.PolicyNo = res.Policy_No;
                    result.PeriodFrom = res.Period_From;
                    result.PeriodTo = res.Period_To;
                }
                else
                {
                    query = @"SELECT mo.Order_No, mo.Policy_No, 
                        mo.Period_From, mo.Period_To,
                        mo.Order_Type
                        FROM dbo.Mst_Order mo
                        INNER JOIN dbo.Ord_Dtl_MV odm
                        ON odm.Order_No = mo.Order_No						
                        WHERE (odm.Chasis_Number = @0
                        OR odm.Engine_Number = @1)
                        AND mo.Order_Status NOT IN (9,6)
						AND mo.Period_To BETWEEN @2 AND @3
                        AND mo.Order_No NOT IN (@4)";
                    res = db.Fetch<dynamic>(query, orderSimulationMV.ChasisNumber.Trim(), orderSimulationMV.EngineNumber.Trim(), ordData.PeriodFrom
                        , ordData.PeriodTo, ordData.PolicyOrderNo);
                    if (res.Count > 0)
                    {
                        result.IsDoubleInsured = true;
                        result.OrderNo = res.Order_No;
                        result.PolicyNo = res.Policy_No;
                        result.PeriodFrom = res.Period_From;
                        result.PeriodTo = res.Period_To;
                    }
                    else
                    {
                        result.IsDoubleInsured = false;
                    }
                }
                return result;

            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public static List<PhoneNoModel> GetPhoneList(string OrderNo)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            try
            {
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                PhoneNoModel clmPhone = new PhoneNoModel();
                string query = @"";
                query = @";EXEC [dbo].[usp_GetPhoneList] @0";
                List<PhoneNoModel> ListPhoneNo = db.Fetch<PhoneNoModel>(query, OrderNo);
                query = @"SELECT CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal,OldPolicyNo, CustIDAAB, 
                        CAST(COALESCE(isCompany,0) AS BIT) isCompany
                        FROM dbo.OrderSimulation os
                        INNER JOIN dbo.FollowUp f
                        ON f.FollowUpNo = os.FollowUpNo
                        INNER JOIN dbo.ProspectCustomer pc
                        ON pc.CustID = os.CustID
                        WHERE OrderNo = @0 AND ApplyF  = 1";
                string custid = "";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(query, OrderNo);
                if (ordData.Count > 0)
                {
                    if (ordData.First().IsRenewal)
                    {
                        if (!string.IsNullOrEmpty(ordData.First().OldPolicyNo)) {
                            string qgetCustID = @";DECLARE @@CustID VARCHAR(100)
                            DECLARE @@PolicyNo VARCHAR(100)
                            DECLARE @@SegmenID VARCHAR(100)

                            SELECT @@PolicyNo=OldPolicyNo, @@SegmenID = SegmentCode FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0

                            IF(@@SegmenID = 'P3A200' OR @@SegmenID = 'P3A500')
                            BEGIN
	                            SELECT @@CustID=Cust_Id FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            ELSE
                            BEGIN
	                            SELECT @@CustID=Policy_Holder_Code FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            SELECT COALESCE(@@CustID,'')";
                            custid = db.ExecuteScalar<string>(qgetCustID, OrderNo);
                            query = @"SELECT Claimee_Phone, Notes, lp.EntryDt, mcdv.status FROM dbo.Policy p 
                                    INNER JOIN dbo.Loss_Report lp
                                    ON lp.Policy_No = p.Policy_No
						            LEFT JOIN dbo.Mst_Customer_Data_Validation mcdv
						            ON mcdv.Cust_Id = @1 AND Column_Name = 'Claimee_Phone' 
						            WHERE lp.Policy_No = @0 AND @1 NOT IN(
						            SELECT Cust_Id FROM dbo.Mst_Customer_Data_Validation 
						            WHERE Cust_Id = @1 AND Status = 2 AND Column_Name = 'Claimee_Phone')";
                            List<dynamic> listClaimee = db.Fetch<dynamic>(query, ordData.First().OldPolicyNo, custid);
                            List<dynamic> ClaimeePhones = new List<dynamic>();
                            foreach (dynamic c in listClaimee)
                            {
                                string notes = Convert.ToString(c.Notes);
                                dynamic mdl = new ExpandoObject();
                                if (notes.Contains("RelationshipInsured"))
                                {
                                    List<string> noteList = notes.Split(';').ToList();
                                    string relationship;
                                    foreach (string s in noteList)
                                    {
                                        if (SafeTrim(s).StartsWith("RelationshipInsured"))
                                        {
                                            relationship = SafeTrim(s.Split('|').ToList()[1]).ToUpper();
                                            if (relationship.Equals("KELUARGA", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 2;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("PEGAWAI", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 1;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("SUAMI", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 5;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("ISTRI", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 4;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("ANAK", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 3;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("SATU KK", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 6;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                            else if (relationship.Equals("TERTANGGUNG", StringComparison.InvariantCultureIgnoreCase))
                                            {
                                                mdl.NoHp = c.Claimee_Phone;
                                                mdl.Priority = 7;
                                                mdl.EntryDt = c.EntryDt;
                                                ClaimeePhones.Add(mdl);
                                            }
                                        }
                                    }
                                }
                            }
                            ClaimeePhones = ClaimeePhones.OrderByDescending(x => x.Priority).ThenByDescending(x => x.EntryDt).ToList();
                            if (ClaimeePhones.Count > 0)
                            {
                                clmPhone.NoHp = ClaimeePhones[0].NoHp;
                                clmPhone.Priority = 0;
                                clmPhone.HPStatus = string.IsNullOrEmpty(listClaimee.First().status) ? "0" : Convert.ToString(listClaimee.First().status);
                                clmPhone.Column_Name = "Claimee_Phone";
                                ListPhoneNo.Add(clmPhone);
                            }
                        }
                    }
                }

                string PrefixNo = MobileRepository.GetApplicationParametersValue("PREFIX-PHONENO")[0];
                List<string> LisPrefixNo = PrefixNo.Split(',').ToList();

                if (ordData.First().isCompany)
                {
                    #region company
                    foreach (PhoneNoModel phoneNo in ListPhoneNo)
                    {
                        phoneNo.Priority = 0;
                        if (SafeTrim(phoneNo.Column_Name).Equals("MPC1"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 7;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("MPC2"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 6;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone1"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 5;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone2"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 4;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Claimee_Phone"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 3;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("AWO HP"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 2;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("AWO Off"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 1;
                        }
                        foreach (string s in LisPrefixNo)
                        {
                            if (SafeTrim(phoneNo.NoHp).StartsWith(s))
                            {
                                phoneNo.Priority = phoneNo.Priority + 8;
                            }
                        }
                        if (Convert.ToInt32(phoneNo.HPStatus) == 1)
                        {
                            phoneNo.Priority = phoneNo.Priority + 16;
                        }
                        string qGetRelation = @"SELECT COALESCE(RelationshipId,0) RelationshipId, COALESCE(PICName,'') PICName FROM dbo.Mst_Customer_Data_Validation WHERE Column_Name = @1 AND Cust_Id = @0 ORDER BY Created_Date DESC";
                        if (!string.IsNullOrEmpty(custid))
                        {
                            List<dynamic> relations = db.Fetch<dynamic>(qGetRelation, custid, phoneNo.Column_Name);
                            if (relations.Count > 0)
                            {
                                phoneNo.RelationshipId = relations.First().RelationshipId;
                                phoneNo.PICName = relations.First().PICName;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region personal
                    foreach (PhoneNoModel phoneNo in ListPhoneNo)
                    {
                        phoneNo.Priority = 0;
                        if (SafeTrim(phoneNo.Column_Name).Equals("HP"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 13;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("HP_2"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 12;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("HP_4"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 11;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("HP_3"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 10;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("InsuredPhoneNo"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 9;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("AWO HP"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 8;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Claimee_Phone"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 7;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Home_Phone1"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 6;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone1"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 5;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Home_Phone2"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 4;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("Office_Phone2"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 3;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("AWO Home"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 2;
                        }
                        else if (SafeTrim(phoneNo.Column_Name).Equals("AWO Off"))
                        {
                            phoneNo.Priority = phoneNo.Priority + 1;
                        }
                        foreach (string s in LisPrefixNo)
                        {
                            if (SafeTrim(phoneNo.NoHp).StartsWith(s))
                            {
                                phoneNo.Priority = phoneNo.Priority + 14;
                            }
                        }
                        if (Convert.ToInt32(phoneNo.HPStatus) == 1)
                        {
                            phoneNo.Priority = phoneNo.Priority + 28;
                        }
                        string qGetRelation = @"SELECT COALESCE(RelationshipId,0) RelationshipId, COALESCE(PICName,'') PICName FROM dbo.Mst_Customer_Data_Validation WHERE Column_Name = @1 AND Cust_Id = @0 ORDER BY Created_Date DESC";
                        if (!string.IsNullOrEmpty(custid))
                        {
                            List<dynamic> relations = db.Fetch<dynamic>(qGetRelation, custid, phoneNo.Column_Name);
                            if (relations.Count > 0)
                            {
                                phoneNo.RelationshipId = relations.First().RelationshipId;
                                phoneNo.PICName = relations.First().PICName;
                            }
                        }
                    }
                    #endregion
                }
                ListPhoneNo = ListPhoneNo.OrderByDescending(x => x.Priority).ToList();
                return ListPhoneNo;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :"+ e.StackTrace +"," + e.ToString());
                throw e;
            }
        }

        public static void InsertUpdatePhoneNo(InsertUpdatePhoneNoParam param)
        {
            string actionName = MobileRepository.GetCurrentMethod();
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelectOrd = @"SELECT CAST(COALESCE(isCompany,0) AS BIT) isCompany, os.CustID, COALESCE(pc.CustIDAAB,'') CustIDAAB 
								    FROM dbo.OrderSimulation os
                                    INNER JOIN dbo.ProspectCustomer pc
                                    ON pc.CustID = os.CustID
                                    WHERE OrderNo = @0";
                List<dynamic> ordData = mobiledb.Fetch<dynamic>(qSelectOrd, param.OrderNo);
                if (ordData.Count > 0)
                {
                    string qgetCustID = @";DECLARE @@CustID VARCHAR(100)
                            DECLARE @@PolicyNo VARCHAR(100)
                            DECLARE @@SegmenID VARCHAR(100)

                            SELECT @@PolicyNo=OldPolicyNo, @@SegmenID = SegmentCode FROM BEYONDMOSS.AABMobile.dbo.OrderSimulation WHERE OrderNo = @0

                            IF(@@SegmenID = 'P3A200' OR @@SegmenID = 'P3A500')
                            BEGIN
	                            SELECT @@CustID=Cust_Id FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            ELSE
                            BEGIN
	                            SELECT @@CustID=Policy_Holder_Code FROM dbo.Policy WHERE Policy_No = @@PolicyNo
                            END
                            SELECT COALESCE(@@CustID,'')";
                    string custid = db.ExecuteScalar<string>(qgetCustID, param.OrderNo);
                    if (!string.IsNullOrEmpty(custid)) {
                        ordData.First().CustIDAAB = custid;
                    }
                    string qUpdatePhoneABB = @"";

                    if (ordData.First().isCompany)
                    {
                        //param.PhoneNoParam = RemoveDuplicatePhone(param, param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID, ordData.First().isCompany);

                        string qSelectCompanyHP = @"SELECT AdditionalInfoValue NoHP, 'MPC1' ColumnName 
                                                        FROM dbo.Mst_Customer mc
														LEFT JOIN dbo.Mst_Customer_AdditionalInfo mca
														ON mc.Cust_Id = mca.CustID
                                                        WHERE AdditionalCode = 'MPC1' AND mc.Cust_Id = @0
                                                        UNION
                                                        SELECT AdditionalInfoValue NoHP, 'MPC2' ColumnName 
                                                        FROM dbo.Mst_Customer mc
														LEFT JOIN dbo.Mst_Customer_AdditionalInfo mca
														ON mc.Cust_Id = mca.CustID
                                                        WHERE AdditionalCode = 'MPC2' AND mc.Cust_Id = @0";
                        List<dynamic> lsPhoneNo = db.Fetch<dynamic>(qSelectCompanyHP, ordData.First().CustIDAAB);
                        List<string> listAvailColName = new List<string>();
                        foreach (dynamic no in lsPhoneNo)
                        {
                            if (string.IsNullOrEmpty(no.NoHP))
                            {
                                listAvailColName.Add(no.ColumnName);
                            }
                        }
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].IsWrongNumber) {
                                #region old
//                                if (param.PhoneNoParam[i].ColumnName.Contains("MPC1") || param.PhoneNoParam[i].ColumnName.Contains("MPC2"))
//                                {
//                                    InsertCustHistory(ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName, "", param.SalesOfficerID, "");
//                                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
//                                    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
//                                }
//                                else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone1"))
//                                {
//                                    InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone1", "", param.SalesOfficerID, "");
//                                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                }
//                                else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone2"))
//                                {
//                                    InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone2", "", param.SalesOfficerID, "");
//                                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                }
//                                else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone1"))
//                                {
//                                    InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone1", "", param.SalesOfficerID, "");
//                                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                }
//                                else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone2"))
//                                {
//                                    InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone2", "", param.SalesOfficerID, "");
//                                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                }
//                                if (!string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
//                                {
//                                    if (!string.IsNullOrEmpty(param.PhoneNoParam[i].PICName))
//                                    {
//                                        db.Execute("UPDATE dbo.Mst_Cust_Pic SET Name = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0"
//                                            , ordData.First().CustIDAAB
//                                            , param.PhoneNoParam[i].PICName
//                                            , param.SalesOfficerID);
//                                        mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
//                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
//                                            , param.PhoneNoParam[i].PICName
//                                            , param.SalesOfficerID);
//                                    }
//                                    dynamic dateVerified = null;
//                                    if (param.PhoneNoParam[i].IsConnected)
//                                    {
//                                        dateVerified = DateTime.Now;
//                                    }
//                                    db.Execute(qInsertUpdateValidation
//                                        , ordData.First().CustIDAAB
//                                        , param.PhoneNoParam[i].ColumnName
//                                        , 2
//                                        , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
//                                        , null
//                                        , ""
//                                        , param.SalesOfficerID
//                                        , dateVerified
//                                        , param.PhoneNoParam[i].CallRejectReasonId);
//                                    if (param.PhoneNoParam[i].ColumnName.Contains("MPC1"))
//                                    {
//                                        string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedDate = GETDATE() WHERE CustID = @0
//                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
//                                        mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.SalesOfficerID);
//                                    }
//                                }
                                #endregion
                                SetWrongNumberCompany(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                            }
                            else if (param.PhoneNoParam[i].IsLandline && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                dynamic dateVerified = null;
                                if (param.PhoneNoParam[i].IsConnected)
                                {
                                    dateVerified = DateTime.Now;
                                }
                                db.Execute(qInsertUpdateValidation
                                    , ordData.First().CustIDAAB
                                    , "Office_Phone1"
                                    , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                    , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    , null
                                    , ""
                                    , param.SalesOfficerID
                                    , dateVerified
                                    , param.PhoneNoParam[i].CallRejectReasonId);
                            }
                            else
                            {
                                if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    #region old
//                                    InsertCustHistory(ordData.First().CustIDAAB, "MPC1", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "");
//                                    db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC1");
//                                    List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
//                                    if (isverified.Count > 0)
//                                    {
//                                        param.PhoneNoParam[i].IsConnected = true;
//                                    }
//                                    dynamic dateVerified = null;
//                                    if(param.PhoneNoParam[i].IsConnected){
//                                        dateVerified = DateTime.Now;
//                                    }
//                                    db.Execute("UPDATE dbo.Mst_Cust_Pic SET Name = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0"
//                                        , ordData.First().CustIDAAB
//                                        , param.PhoneNoParam[i].PICName
//                                        , param.SalesOfficerID);
//                                    mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
//                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
//                                        , param.PhoneNoParam[i].PICName
//                                        , param.SalesOfficerID);
                                    
//                                    db.Execute(qInsertUpdateValidation
//                                        , ordData.First().CustIDAAB
//                                        , param.PhoneNoParam[i].ColumnName
//                                        , 2
//                                        , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
//                                        , null
//                                        , ""
//                                        , param.SalesOfficerID
//                                        , dateVerified
//                                        , param.PhoneNoParam[i].CallRejectReasonId);

//                                    db.Execute(qInsertUpdateValidation
//                                        , ordData.First().CustIDAAB
//                                        , "MPC1"
//                                        , param.PhoneNoParam[i].IsConnected ? 1 : 0
//                                        , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
//                                        , null
//                                        , ""
//                                        , param.SalesOfficerID
//                                        , dateVerified
//                                        , param.PhoneNoParam[i].CallRejectReasonId);
//                                    string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
//                                                                UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
//                                    mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
//                                    if (param.PhoneNoParam[i].ColumnName.Contains("MPC2"))
//                                    {
//                                        InsertCustHistory(ordData.First().CustIDAAB, "MPC2", "", param.SalesOfficerID, "");
//                                        qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
//                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
//                                    }
//                                    else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone1"))
//                                    {
//                                        InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone1", "", param.SalesOfficerID, "");
//                                        qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                    }
//                                    else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone2"))
//                                    {
//                                        InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone2", "", param.SalesOfficerID, "");
//                                        qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                    }
//                                    else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone1"))
//                                    {
//                                        InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone1", "", param.SalesOfficerID, "");
//                                        qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                    }
//                                    else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone2"))
//                                    {
//                                        InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone2", "", param.SalesOfficerID, "");
//                                        qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
//                                        db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
//                                    }
                                    #endregion
                                    UpdatePrimaryCompany(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                                }
                                else
                                {
                                    if (!param.PhoneNoParam[i].IsWrongNumber && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                                    {
                                        if (listAvailColName.Count > 0)
                                        {
                                            db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, listAvailColName[0]);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                                                , ordData.First().CustIDAAB
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , listAvailColName[0]
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , null
                                                , ""
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            if (listAvailColName[0].Contains("MPC1"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedBy = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            }
                                            listAvailColName.RemoveAt(0);
                                        }
                                        else
                                        {
                                            InsertCustHistory(ordData.First().CustIDAAB, "MPC2", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "");
                                            db.Execute(qUpdatePhoneCompanyABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, "MPC2");
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                                                , ordData.First().CustIDAAB
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", ordData.First().CustID
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID);
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , "MPC2"
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , null
                                                , ""
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                        }
                                    }
                                    else
                                    {
                                        List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
                                        if (isverified.Count > 0)
                                        {
                                            param.PhoneNoParam[i].IsConnected = true;
                                        } 
                                        dynamic dateVerified = null;
                                        if (param.PhoneNoParam[i].IsConnected)
                                        {
                                            dateVerified = DateTime.Now;
                                        }
                                        db.Execute(qInsertUpdateValidation
                                            , ordData.First().CustIDAAB
                                            , param.PhoneNoParam[i].ColumnName
                                            , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                            , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                            , param.PhoneNoParam[i].Relation
                                            , param.PhoneNoParam[i].PICName
                                            , param.SalesOfficerID
                                            , dateVerified
                                            , param.PhoneNoParam[i].CallRejectReasonId);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //param.PhoneNoParam = RemoveDuplicatePhone(param, param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID, ordData.First().isCompany);
                        string qSelectPersonalHP = @"SELECT HP NoHP, 'HP' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0
                                            UNION
                                            SELECT HP_2 NoHP, 'HP_2' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0
                                            UNION
                                            SELECT HP_3 NoHP, 'HP_3' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0
                                            UNION
                                            SELECT HP_4 NoHP, 'HP_4' ColumnName FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0";
                        List<dynamic> lsPhoneNo = db.Fetch<dynamic>(qSelectPersonalHP, ordData.First().CustIDAAB);
                        List<string> listAvailColName = new List<string>();
                        foreach (dynamic no in lsPhoneNo)
                        {
                            if (string.IsNullOrEmpty(no.NoHP))
                            {
                                listAvailColName.Add(no.ColumnName);
                            }
                        }
                        for (int i = 0; i < param.PhoneNoParam.Count; i++)
                        {
                            if (param.PhoneNoParam[i].IsWrongNumber)
                            {
                                #region old
                                //if (param.PhoneNoParam[i].ColumnName.StartsWith("HP"))
                                //{
                                //    InsertCustHistory(ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName, "", param.SalesOfficerID, "");
                                //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                //    db.Execute(string.Format(qUpdatePhoneABB, param.PhoneNoParam[i].ColumnName)
                                //        , ordData.First().CustIDAAB, param.SalesOfficerID);
                                //}
                                //else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone1"))
                                //{
                                //    InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone1", "", param.SalesOfficerID, "");
                                //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                //}
                                //else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone2"))
                                //{
                                //    InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone2", "", param.SalesOfficerID, "");
                                //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                //}
                                //else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone1"))
                                //{
                                //    InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone1", "", param.SalesOfficerID, "");
                                //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                //}
                                //else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone2"))
                                //{
                                //    InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone2", "", param.SalesOfficerID, "");
                                //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                //}
                                //if (!string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                                //{
                                //    dynamic dateVerified = null;
                                //    if (param.PhoneNoParam[i].IsConnected)
                                //    {
                                //        dateVerified = DateTime.Now;
                                //    }
                                //    db.Execute(qInsertUpdateValidation
                                //        , ordData.First().CustIDAAB
                                //        , param.PhoneNoParam[i].ColumnName
                                //        , 2
                                //        , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                //        , param.PhoneNoParam[i].Relation
                                //        , param.PhoneNoParam[i].PICName
                                //        , param.SalesOfficerID
                                //        , dateVerified
                                //        , param.PhoneNoParam[i].CallRejectReasonId);
                                //}
                                //if (param.PhoneNoParam[i].ColumnName.Equals("HP"))
                                //{
                                //    string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                //    mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID);
                                //}
                                //else if (param.PhoneNoParam[i].ColumnName.Contains("HP_2"))
                                //{
                                //    string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone2 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                //    mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID);
                                //}
                                #endregion
                                SetWrongNumberPersonal(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                            }
                            else if (param.PhoneNoParam[i].IsLandline && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                            {
                                qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                dynamic dateVerified = null;
                                if (param.PhoneNoParam[i].IsConnected)
                                {
                                    dateVerified = DateTime.Now;
                                }
                                db.Execute(qInsertUpdateValidation
                                    , ordData.First().CustIDAAB
                                    , "Home_Phone1"
                                    , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                    , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    , null
                                    , ""
                                    , param.SalesOfficerID
                                    , dateVerified
                                    , param.PhoneNoParam[i].CallRejectReasonId);
                            }
                            else
                            {
                                if (param.PhoneNoParam[i].IsPrimary)
                                {
                                    #region old
                                    //InsertCustHistory(ordData.First().CustIDAAB, "HP", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "");
                                    //qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                    //db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                    //List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
                                    //if (isverified.Count > 0) {
                                    //    param.PhoneNoParam[i].IsConnected = true;
                                    //}
                                    //dynamic dateVerified = null;
                                    //if (param.PhoneNoParam[i].IsConnected)
                                    //{
                                    //    dateVerified = DateTime.Now;
                                    //}

                                    //db.Execute(qInsertUpdateValidation
                                    //, ordData.First().CustIDAAB
                                    //, param.PhoneNoParam[i].ColumnName
                                    //, 2
                                    //, param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    //, null
                                    //, ""
                                    //, param.SalesOfficerID
                                    //, dateVerified
                                    //, param.PhoneNoParam[i].CallRejectReasonId);

                                    //db.Execute(qInsertUpdateValidation
                                    //    , ordData.First().CustIDAAB
                                    //    , "HP"
                                    //    , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                    //    , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                    //    , param.PhoneNoParam[i].Relation
                                    //    , param.PhoneNoParam[i].PICName
                                    //    , param.SalesOfficerID
                                    //    , dateVerified
                                    //    , param.PhoneNoParam[i].CallRejectReasonId);
                                    //string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                    //mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                    //if (param.PhoneNoParam[i].ColumnName.Contains("HP_"))
                                    //{
                                    //    InsertCustHistory(ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName, "", param.SalesOfficerID, "");
                                    //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                    //    db.Execute(string.Format(qUpdatePhoneABB, param.PhoneNoParam[i].ColumnName)
                                    //        , ordData.First().CustIDAAB, param.SalesOfficerID);
                                    //}
                                    //else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone1"))
                                    //{
                                    //    InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone1", "", param.SalesOfficerID, "");
                                    //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                    //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                    //}
                                    //else if (param.PhoneNoParam[i].ColumnName.Contains("Home_Phone2"))
                                    //{
                                    //    InsertCustHistory(ordData.First().CustIDAAB, "Home_Phone2", "", param.SalesOfficerID, "");
                                    //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                    //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                    //}
                                    //else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone1"))
                                    //{
                                    //    InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone1", "", param.SalesOfficerID, "");
                                    //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                    //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                    //}
                                    //else if (param.PhoneNoParam[i].ColumnName.Contains("Office_Phone2"))
                                    //{
                                    //    InsertCustHistory(ordData.First().CustIDAAB, "Office_Phone2", "", param.SalesOfficerID, "");
                                    //    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                                    //    db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.SalesOfficerID);
                                    //}
                                    #endregion
                                    UpdatePrimaryPersonal(param.PhoneNoParam[i], param.SalesOfficerID, ordData.First().CustIDAAB, ordData.First().CustID);
                                }
                                else
                                {
                                    if (!param.PhoneNoParam[i].IsWrongNumber && string.IsNullOrEmpty(param.PhoneNoParam[i].ColumnName))
                                    {
                                        if (listAvailColName.Count > 0)
                                        {
                                            qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                            db.Execute(string.Format(qUpdatePhoneABB, listAvailColName[0])
                                                , ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , listAvailColName[0]
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , param.PhoneNoParam[i].Relation
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            if (listAvailColName[0].Equals("HP"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                            }
                                            else if (listAvailColName[0].Equals("HP_2"))
                                            {
                                                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone2 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                                mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                            }
                                            listAvailColName.RemoveAt(0);
                                        }
                                        else
                                        {
                                            InsertCustHistory(ordData.First().CustIDAAB, "HP_2", param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID, "");
                                            qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET HP_2 = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                                            db.Execute(qUpdatePhoneABB, ordData.First().CustIDAAB, param.PhoneNoParam[i].PhoneNo, param.SalesOfficerID);
                                            dynamic dateVerified = null;
                                            if (param.PhoneNoParam[i].IsConnected)
                                            {
                                                dateVerified = DateTime.Now;
                                            }
                                            db.Execute(qInsertUpdateValidation
                                                , ordData.First().CustIDAAB
                                                , "HP_2"
                                                , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                                , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                                , param.PhoneNoParam[i].Relation
                                                , param.PhoneNoParam[i].PICName
                                                , param.SalesOfficerID
                                                , dateVerified
                                                , param.PhoneNoParam[i].CallRejectReasonId);
                                            string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone2 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                                            mobiledb.Execute(qUpdateHpAABMobile, ordData.First().CustID, param.PhoneNoParam[i].PhoneNo);
                                        }
                                    }
                                    else {
                                        List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", ordData.First().CustIDAAB, param.PhoneNoParam[i].ColumnName);
                                        if (isverified.Count > 0)
                                        {
                                            param.PhoneNoParam[i].IsConnected = true;
                                        } 
                                        dynamic dateVerified = null;
                                        if (param.PhoneNoParam[i].IsConnected)
                                        {
                                            dateVerified = DateTime.Now;
                                        }
                                        db.Execute(qInsertUpdateValidation
                                            , ordData.First().CustIDAAB
                                            , param.PhoneNoParam[i].ColumnName
                                            , param.PhoneNoParam[i].IsConnected ? 1 : 0
                                            , param.PhoneNoParam[i].IsConnected ? param.SalesOfficerID : ""
                                            , param.PhoneNoParam[i].Relation
                                            , param.PhoneNoParam[i].PICName
                                            , param.SalesOfficerID
                                            , dateVerified
                                            , param.PhoneNoParam[i].CallRejectReasonId);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace + ","+ e.ToString());
                throw e;
            }
        }

        public static List<dynamic> GetRelationshipWithInsured()
        {
            string actionName = MobileRepository.GetCurrentMethod();
            List<dynamic> res = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelect = @"SELECT RelationshipId,RelationshipDes FROM dbo.Mst_RelationshipWithInsured WHERE RowStatus = 1";
                res = db.Fetch<dynamic>(qSelect);
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public static List<dynamic> GetCallRejectReason()
        {
            string actionName = MobileRepository.GetCurrentMethod();
            List<dynamic> res = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            try
            {
                string qSelect = @"SELECT CallRejectReasonId,CallRejectReasonDes,IsValid 
                                FROM dbo.Mst_CallRejectReason WHERE RowStatus = 1";
                res = db.Fetch<dynamic>(qSelect);
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        #region priivate method
        private static byte[] DecodeFromAAB2000ImageBytes(byte[] sourceBytes)
        {
            byte[] encodedBytes = Encoding.Convert(Encoding.Unicode, Encoding.Default, sourceBytes);
            return encodedBytes;
        }
        public static string SafeTrim(string str)
        {
            return str != null ? str.Trim() : "";
        }
        public static void InsertCustHistory(string CustId, string ColumnName, string PhoneNo, string SalesOfficerID, string Email)
        {
            try
            {
                string qInsertHistory = @"INSERT INTO dbo.CustomerHistory
                                                    ( CustId ,
                                                      PhoneNumberNew ,
                                                      PhoneNumberOld ,
                                                      CreatedBy ,
                                                      CreatedDate,
													  EmailNew,
													  EmailOld,
                                                      Column_Name
                                                    )
                                            VALUES  ( @0 , -- CustId - char(11)
                                                      @1 , -- PhoneNumberNew - varchar(16)
                                                      @2 , -- PhoneNumberOld - varchar(16)
                                                      @3 , -- CreatedBy - varchar(50)
                                                      GETDATE(),  -- CreatedDate - datetime
													  @5,
													  @6,
                                                      @4
                                                    )";
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                string oldNumber = "";
                if (ColumnName.Contains("MPC"))
                {
                    oldNumber = db.ExecuteScalar<string>(
        @"SELECT COALESCE(AdditionalInfoValue,'') AdditionalInfoValue 
                                                FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0"
        , CustId, ColumnName);
                }
                else if(ColumnName.Contains("HP")){
                    oldNumber = db.ExecuteScalar<string>(
        string.Format(@"SELECT {0} FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @0",ColumnName)
        , CustId);
                }
                else if (ColumnName.Contains("Office_Phone") || ColumnName.Contains("Home_Phone"))
                {
                    oldNumber = db.ExecuteScalar<string>(
        string.Format(@"SELECT {0} FROM dbo.Mst_Customer WHERE Cust_Id = @0", ColumnName)
        , CustId);
                }
                string oldEmail = "";
                if (string.IsNullOrEmpty(ColumnName))
                {
                    oldEmail = db.ExecuteScalar<string>(@"SELECT COALESCE(Email,'') Email FROM mst_customer where Cust_Id  = @0", CustId);
                }
                else {
                    oldEmail = "";
                }
                if (!string.IsNullOrEmpty(oldNumber) || !string.IsNullOrEmpty(oldEmail))
                                    {
                                        db.Execute(qInsertHistory
                                           , CustId
                                           , PhoneNo
                                           , oldNumber
                                           , SalesOfficerID
                                           , ColumnName
                                           , Email
                                           , oldEmail);
                                    } 

            }
            catch (Exception e)
            {
                
                throw e;
            }
        }

        public static List<PhoneNoParam> RemoveDuplicatePhone(InsertUpdatePhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID, bool isCompany)
        {
            try
            {
                List<PhoneNoModel> res = AABRepository.GetPhoneList(param.OrderNo);
                List<PhoneNoParam> tempList = new List<PhoneNoParam>(param.PhoneNoParam);
                //tempList = param.PhoneNoParam;
                foreach (PhoneNoParam p in param.PhoneNoParam)
                {
                    var listDuplicate = res.FindAll(x => x.NoHp == p.PhoneNo);
                    if (listDuplicate.Count > 1) {
                        foreach (PhoneNoModel pm in listDuplicate)
                        {
                            PhoneNoParam pp = new PhoneNoParam();
                            pp.IsPrimary = false;
                            pp.IsConnected = p.IsConnected;
                            pp.IsLandline = p.IsLandline;
                            pp.IsWrongNumber = true;
                            pp.PhoneNo = pm.NoHp;
                            pp.ColumnName = pm.Column_Name;
                            pp.CallRejectReasonId = p.CallRejectReasonId;
                            pp.PICName = pm.PICName;
                            pp.Relation = Convert.ToString(pm.RelationshipId);
                            if (p.IsWrongNumber) {
                                if (isCompany)
                                {
                                    SetWrongNumberCompany(pp, SalesOfficerID, CustIDAAB, CustID);
                                }
                                else
                                {
                                    SetWrongNumberPersonal(pp, SalesOfficerID, CustIDAAB, CustID);
                                }
                            }
                        }
                        if (p.IsPrimary)
                        {
                            foreach (PhoneNoModel pm in listDuplicate)
                            {
                                PhoneNoParam pp = new PhoneNoParam();
                                pp.IsPrimary = false;
                                pp.IsConnected = p.IsConnected;
                                pp.IsLandline = p.IsLandline;
                                pp.IsWrongNumber = true;
                                pp.PhoneNo = pm.NoHp;
                                pp.ColumnName = pm.Column_Name;
                                pp.CallRejectReasonId = p.CallRejectReasonId;
                                pp.PICName = pm.PICName;
                                pp.Relation = Convert.ToString(pm.RelationshipId);
                                if (isCompany)
                                {
                                    SetWrongNumberCompany(pp, SalesOfficerID, CustIDAAB, CustID);
                                }
                                else
                                {
                                    SetWrongNumberPersonal(pp, SalesOfficerID, CustIDAAB, CustID);
                                }
                            } 
                            if (isCompany)
                            {
                                UpdatePrimaryCompany(p, SalesOfficerID, CustIDAAB, CustID);
                            }
                            else
                            {
                                UpdatePrimaryPersonal(p, SalesOfficerID, CustIDAAB, CustID);
                            }
                        }
                        tempList.RemoveAll(x => x.PhoneNo == p.PhoneNo);
                        res.RemoveAll(x => x.NoHp == p.PhoneNo);
                    }
                }
                return tempList;
            }
            catch (Exception e)
            {
                
                throw e;
            }
        }

        public static void UpdatePrimaryCompany(PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                InsertCustHistory(CustIDAAB, "MPC1", param.PhoneNo, SalesOfficerID, "");
                db.Execute(qUpdatePhoneCompanyABB, CustIDAAB, param.PhoneNo, "MPC1");
                List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", CustIDAAB, param.ColumnName);
                if (isverified.Count > 0)
                {
                    param.IsConnected = true;
                }
                dynamic dateVerified = null;
                if (param.IsConnected)
                {
                    dateVerified = DateTime.Now;
                }
                db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                    , CustIDAAB
                    , param.PICName
                    , SalesOfficerID);
                mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                    , param.PICName
                    , SalesOfficerID);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , param.ColumnName
                    , 2
                    , param.IsConnected ? SalesOfficerID : ""
                    , null
                    , ""
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , "MPC1"
                    , param.IsConnected ? 1 : 0
                    , param.IsConnected ? SalesOfficerID : ""
                    , null
                    , ""
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);
                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                mobiledb.Execute(qUpdateHpAABMobile, CustID, param.PhoneNo, SalesOfficerID);
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, "MPC2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
            }
            catch (Exception e)
            {
                
                throw e;
            }
        }

        public static void SetWrongNumberCompany(PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC1") || param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                if (!string.IsNullOrEmpty(param.ColumnName))
                {
                    if (!string.IsNullOrEmpty(param.PICName))
                    {
                        db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                            , CustIDAAB
                            , param.PICName
                            , SalesOfficerID);
                        mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                            , param.PICName
                            , SalesOfficerID);
                    }
                    dynamic dateVerified = null;
                    if (param.IsConnected)
                    {
                        dateVerified = DateTime.Now;
                    }
                    db.Execute(qInsertUpdateValidation
                        , CustIDAAB
                        , param.ColumnName
                        , 2
                        , param.IsConnected ? SalesOfficerID : ""
                        , null
                        , ""
                        , SalesOfficerID
                        , dateVerified
                        , param.CallRejectReasonId);
                    if (param.ColumnName.Contains("MPC1"))
                    {
                        string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                        mobiledb.Execute(qUpdateHpAABMobile, CustID, SalesOfficerID);
                    }
                }
            }
            catch (Exception e)
            {
                
                throw e;
            }
        }

        public static void UpdatePrimaryPersonal(PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string qUpdatePhoneABB = "";
                InsertCustHistory(CustIDAAB, "HP", param.PhoneNo, SalesOfficerID, "");
                qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET HP = @1, updateusr = @2, updatedt = GETDATE() WHERE Cust_Id = @0";
                db.Execute(qUpdatePhoneABB, CustIDAAB, param.PhoneNo, SalesOfficerID);
                List<dynamic> isverified = db.Fetch<dynamic>("SELECT * FROM dbo.Mst_Customer_Data_Validation WHERE Cust_Id = @0 AND Column_Name = @1 AND Status = 1", CustIDAAB, param.ColumnName);
                if (isverified.Count > 0)
                {
                    param.IsConnected = true;
                }
                dynamic dateVerified = null;
                if (param.IsConnected)
                {
                    dateVerified = DateTime.Now;
                }

                db.Execute(qInsertUpdateValidation
                , CustIDAAB
                , param.ColumnName
                , 2
                , param.IsConnected ? SalesOfficerID : ""
                , null
                , ""
                , SalesOfficerID
                , dateVerified
                , param.CallRejectReasonId);

                db.Execute(qInsertUpdateValidation
                    , CustIDAAB
                    , "HP"
                    , param.IsConnected ? 1 : 0
                    , param.IsConnected ? SalesOfficerID : ""
                    , param.Relation
                    , param.PICName
                    , SalesOfficerID
                    , dateVerified
                    , param.CallRejectReasonId);
                string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCustomer SET Phone1 = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0";
                mobiledb.Execute(qUpdateHpAABMobile, CustID, param.PhoneNo);
                if (param.ColumnName.Contains("HP_"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Cust_Personal SET {0} = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(string.Format(qUpdatePhoneABB, param.ColumnName)
                        , CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static void SetWrongNumberPersonal(PhoneNoParam param, string SalesOfficerID, string CustIDAAB, string CustID)
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var mobiledb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                string qUpdatePhoneABB = "";
                if (param.ColumnName.Contains("MPC1") || param.ColumnName.Contains("MPC2"))
                {
                    InsertCustHistory(CustIDAAB, param.ColumnName, "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"DELETE FROM dbo.Mst_Customer_AdditionalInfo WHERE AdditionalCode = @1 AND CustID = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, param.ColumnName);
                }
                else if (param.ColumnName.Contains("Office_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Office_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Office_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Office_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone1"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone1", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone1 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                else if (param.ColumnName.Contains("Home_Phone2"))
                {
                    InsertCustHistory(CustIDAAB, "Home_Phone2", "", SalesOfficerID, "");
                    qUpdatePhoneABB = @"UPDATE dbo.Mst_Customer SET Home_Phone2 = '', updateusr = @1, updatedt = GETDATE() WHERE Cust_Id = @0";
                    db.Execute(qUpdatePhoneABB, CustIDAAB, SalesOfficerID);
                }
                if (!string.IsNullOrEmpty(param.ColumnName))
                {
                    if (!string.IsNullOrEmpty(param.PICName))
                    {
                        db.Execute(@";IF EXISTS(SELECT * FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0)
										BEGIN
											DELETE FROM dbo.Mst_Cust_Pic WHERE Cust_Id = @0
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
										END
										ELSE
                                        BEGIN
	                                        INSERT INTO dbo.Mst_Cust_Pic
	                                                ( Cust_Id ,
	                                                  Name ,
	                                                  entryusr ,
	                                                  entrydt
	                                                )
	                                        VALUES  ( @0 , -- Prospect_Id - char(11)
	                                                  @1 , -- Name - varchar(50)
	                                                  @2 , -- entryusr - char(5)
	                                                  GETDATE()  -- entrydt - datetime
	                                                )
                                        END"
                            , CustIDAAB
                            , param.PICName
                            , SalesOfficerID);
                        mobiledb.Execute(@"UPDATE dbo.ProspectCompany SET PICname = @1, ModifiedBy = @2, ModifiedDate = GETDATE() WHERE CustID = @0
                                                    UPDATE dbo.ProspectCustomer SET Name = @1, LastUpdatedTime = GETDATE() WHERE CustID = @0", CustID
                            , param.PICName
                            , SalesOfficerID);
                    }
                    dynamic dateVerified = null;
                    if (param.IsConnected)
                    {
                        dateVerified = DateTime.Now;
                    }
                    db.Execute(qInsertUpdateValidation
                        , CustIDAAB
                        , param.ColumnName
                        , 2
                        , param.IsConnected ? SalesOfficerID : ""
                        , null
                        , ""
                        , SalesOfficerID
                        , dateVerified
                        , param.CallRejectReasonId);
                    if (param.ColumnName.Contains("MPC1"))
                    {
                        string qUpdateHpAABMobile = @"UPDATE dbo.ProspectCompany SET PICPhoneNo = '', ModifiedBy = @1, ModifiedDate = GETDATE() WHERE CustID = @0
                                                                UPDATE dbo.ProspectCustomer SET Phone1 = '', LastUpdatedTime = GETDATE() WHERE CustID = @0";
                        mobiledb.Execute(qUpdateHpAABMobile, CustID, SalesOfficerID);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        #endregion

        internal static CheckDoubleInsuredResult GetCheckDoubleInsuredRenNote(string PolicyNo)
        {
            CheckDoubleInsuredResult result = new CheckDoubleInsuredResult();
            try
            {
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }
    }
}