﻿using a2is.Framework.DataAccess;
using Otosales.Codes;
using Otosales.Models;
using Otosales.Models.vWeb2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Otosales.Models.vWeb2;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Data.SqlClient;
using System.Text;
using PremiumCalculation.Model;
using a2is.Framework.Monitoring;
namespace Otosales.Repository.vWeb2
{
    public class RetailRepository
    {

        private static readonly a2isLogHelper logger = new a2isLogHelper();
        public static string IsNA(string value)
        {
            return string.IsNullOrWhiteSpace(value) ? "" : value.TrimEnd();
        }
        public static List<dynamic> GetProductIEP()
        {
            string query = @"select rtrim(isnull(a.Product_Code,'')) AS ProductCode
                            from mst_product a with(nolock)
                            inner join global_parameter b with(nolock) on parameter like 'PROGKHUS.AAB.PRODUCTCODE'
                            where patindex('''%' + a.product_code + '%''','''' + b.value + '''') > 0 ";

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {

                List<dynamic> result = db.Query<dynamic>(query).ToList();

                return result;
            }
        }
        public static List<WTCommission> GetRenewalDiscount(string OldPolicyNo)
        {


            string qGetRenDisc = ";Exec dbo.usp_GetPolicyNCB @@OldPolicyNo=@0";

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {

                return db.Query<WTCommission>(qGetRenDisc, OldPolicyNo).ToList();
            }
        }
   
        public static int ValidatePerInterest(string InterestID, List<CalculatedPremi> CoverageList, DateTime PolicyPeriodFrom, DateTime PolicyPeriodTo, string ProductCode)
        {

            int vStatus = 0;
            try
            {
                switch (InterestID.Trim().ToUpper())
                {
                    case "CASCO":
                        DateTime PeriodFromBSC = new DateTime();
                        DateTime PeriodToBSC = new DateTime();
                        int BasicCoverageCount = 0;

                        foreach (CalculatedPremi item in CoverageList)
                        {

                            //DateTime CoverFrom = Convert.ToDateTime(item.CoverFromDT);
                            //DateTime CoverTo = Convert.ToDateTime(item.CoverToDT);
                            DateTime CoverFrom = item.PeriodFrom;//DateTime.ParseExact(item.PeriodFrom, DateFormatCustom.Format1, CultureInfo.InvariantCulture);
                            DateTime CoverTo = item.PeriodTo;//DateTime.ParseExact(item.Per, DateFormatCustom.Format1, CultureInfo.InvariantCulture);

                            if (PeriodFromBSC == default(DateTime))
                            {
                                PeriodFromBSC = CoverFrom;
                            }
                            if (PeriodToBSC == default(DateTime))
                            {
                                PeriodToBSC = CoverTo;
                            }
                            //Out of Policy Period Validation
                            if (DateTime.Compare(CoverFrom, PolicyPeriodFrom) < 0 || DateTime.Compare(CoverTo, PolicyPeriodTo) > 0)
                            {
                                vStatus = 1;

                                goto Outer;

                            }
                            if (IsNA(item.CoverType).Trim().ToUpper() == "BSC")
                            {
                                BasicCoverageCount = BasicCoverageCount + 1;
                                if (DateTime.Compare(CoverFrom, PeriodFromBSC) <= 0)
                                {
                                    PeriodFromBSC = CoverFrom;
                                }
                                if (DateTime.Compare(CoverTo, PeriodToBSC) >= 0)
                                {
                                    PeriodToBSC = CoverTo;
                                }
                            }
                        }
                        //Basic Coverage not fully applied
                        if (DateTime.Compare(PolicyPeriodFrom, PeriodFromBSC) < 0 || DateTime.Compare(PolicyPeriodTo, PeriodToBSC) > 0)
                        {
                            vStatus = 2;
                            goto Outer;
                        }
                        if (BasicCoverageCount <= 0)
                        {
                            vStatus = 3;
                            goto Outer;
                        }

                        var prdIEP = GetProductIEP();
                        if (prdIEP.Count > 0)
                        {
                            if (ProductCode.ToUpper().TrimEnd() == prdIEP[0].ProductCode)
                            {
                                var bdl = GetGeneralConfig("PRODUCT_BASIC_COVER_BUNDLING", prdIEP[0].ProductCode, "CASCO");
                                if (bdl.Count > 0)
                                {
                                    var spltT = bdl[0].Split(';');

                                    var splt = new List<string>(spltT);
                                    if (splt.Count() > 0)
                                    {
                                        splt = splt.Distinct().ToList();
                                        var bdlState = new List<CoverageIEP>();
                                        foreach (var item in CoverageList)
                                        {
                                            if (splt.Contains(item.CoverageID))
                                            {
                                                if (!bdlState.Select(e => e.CoverageID).Contains(item.CoverageID))
                                                {
                                                    bdlState.Add(new CoverageIEP
                                                    {
                                                        CoverageID = item.CoverageID,
                                                        IsApply = true
                                                    });
                                                }
                                            }
                                        }

                                        if (!(bdlState.Count == splt.Count() && !bdlState.Select(e => e.IsApply).Contains(false)))
                                        {
                                            vStatus = 4;
                                            goto Outer;
                                        }
                                    }
                                }
                            }
                        }
                        break;

                }
            }
            catch (Exception Ex)
            {
                logger.Error(Ex.ToString());
                vStatus = 0;
            }
        Outer:
            return vStatus;
        }

        public static List<CommissionScheme> InitCommisionList(string CoverageID, List<WTCommission> CommissionList, bool AutoApply = false)
        {
            int C_FLAG_SPECIALCOMMISSION = 4096;
            int g_DiscountMap = 1;
            int i;

            int level;
            List<CommissionScheme> DiscountScheme = new List<CommissionScheme>();
            // List<WTCommission> CommissionList = new List<WTCommission>();
            var ApplyFlags = 0;

            for (i = 0; i <= 4; i++)
            {
                CommissionScheme CScheme = new CommissionScheme
                {
                    TotalFlatAmount = 0,
                    TotalPercentage = 0,
                    Level = i,
                    Schemes = new List<CScheme>()
                };
                CommissionScheme CMScheme = new CommissionScheme();
                DiscountScheme.Add(CMScheme);
                //DiscountScheme[i].Schemes = new List<CScheme>();
            }

            for (i = 0; i < CommissionList.Count; i++)
            {
                ApplyFlags = CommissionList[i].ApplyFlags;
                if ((ApplyFlags & g_DiscountMap) == g_DiscountMap)
                {
                    if (string.IsNullOrWhiteSpace(CommissionList[i].CoverageID) || CommissionList[i].CoverageID.Trim().ToUpper() == CoverageID)
                    {
                        level = 0;
                        level = CommissionList[i].Level;

                        if (AutoApply)
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = CommissionList[i].CommissionID,
                                Name = CommissionList[i].CommissionName,
                                Type = CommissionList[i].CommissionType,
                                Percentage = CommissionList[i].Percentage,
                                FlatAmount = 0,
                                Amount = CommissionList[i].Percentage,
                                //PayToCode = "1",
                                PayToCode = CommissionList[i].PayToParty,
                                ApplyFlags = CommissionList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);
                        }

                        else
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = CommissionList[i].CommissionID,
                                Name = CommissionList[i].CommissionName,
                                Type = CommissionList[i].CommissionType,
                                Percentage = CommissionList[i].Percentage,
                                FlatAmount = 0,
                                Amount = 0,
                                //PayToCode = "1",
                                PayToCode = CommissionList[i].PayToParty,
                                ApplyFlags = CommissionList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);

                        }
                    }
                }
            }

            return DiscountScheme;
        }

    
        public static List<WTCommission> GetWTCommission(string ProductCode, string MouID)
        {

            string query = @"SELECT DISTINCT
                                    *
                            FROM    ( SELECT    pc.Apply_Flags AS ApplyFlags ,
                                                PC.Comm_Id AS CommissionID ,
                                                mc.Name AS CommissionName ,
                                                mc.Percentage AS Percentage ,
                                                pc.Pay_To_Party AS PayToParty ,
                                                mc.level_Disc AS Level ,
                                                mc.Tax_Indiv AS IndividualTax ,
                                                mc.Tax_Corp AS CorporateTax ,
                                                mc.Commission_Type AS CommissionType
                                      FROM      Prd_Commission pc
                                                INNER JOIN Mst_Commission mc ON pc.Comm_ID = mc.COMM_ID
                                      WHERE     pc.Status = 1
                                                AND mc.Status = 1
                                                AND mc.Commission_Type IN ( '2', '4' )
                                                AND pc.Product_Code = @0
                                      UNION ALL
                                      SELECT    pc.Apply_Flags AS ApplyFlags ,
                                                PC.Commission_Code AS CommissionID ,
                                                mc.Name AS CommissionName ,
                                                mc.Percentage AS Percentage ,
                                                pc.Pay_To_Party AS PayToParty ,
                                                mc.level_Disc AS Level ,
                                                mc.Tax_Indiv AS IndividualTax ,
                                                mc.Tax_Corp AS CorporateTax ,
                                                mc.Commission_Type AS CommissionType
                                      FROM      dbo.MOU_Commission AS pc
                                                INNER JOIN Mst_Commission mc ON pc.Commission_Code = mc.COMM_ID
                                      WHERE     mc.Status = 1
                                                AND mc.Commission_Type IN ( '2', '4' )
                                                AND pc.MOU_ID = @1
                                    ) AS X";
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                if (string.IsNullOrWhiteSpace(MouID))
                {
                    MouID = ProductCode;
                }
                var result = db.Query<WTCommission>(query, ProductCode, MouID).ToList();
                return result;
            }
        }
  
        public static dynamic GetDefaultPriceCoverProgressive(string ProductCode, string InterestID, string CoverageID = "")
        {
            string queryAllCoverage = @"SELECT DISTINCT PIC.Product_Code, PIC.Interest_Id, PIC.Coverage_Id, DRS.BC_Up_To_SI, MCP.Lock_TSI 
                            FROM dbo.Prd_Interest_Coverage AS PIC 
                            INNER JOIN dbo.Dtl_Rate_Scoring AS DRS ON drs.Coverage_ID = PIC.Coverage_Id 
                            INNER JOIN dbo.Mapping_Cover_Progressive AS MCP ON MCP.Product_Code = PIC.Product_Code 
                            AND MCP.Interest_Id = PIC.Interest_ID 
                            AND MCP.Coverage_Id = PIC.Coverage_Id 
                            WHERE  PIC.Product_Code = @0 
                            AND MCP.Status = 1
                            AND MCP.Interest_Id =@1                            
                            GROUP BY PIC.Product_Code, PIC.Interest_Id, PIC.Coverage_Id, DRS.BC_Up_To_SI, Lock_TSI";
            string query = @"SELECT DISTINCT PIC.Product_Code, PIC.Interest_Id, PIC.Coverage_Id, DRS.BC_Up_To_SI, MCP.Lock_TSI 
                            FROM dbo.Prd_Interest_Coverage AS PIC 
                            INNER JOIN dbo.Dtl_Rate_Scoring AS DRS ON drs.Coverage_ID = PIC.Coverage_Id 
                            INNER JOIN dbo.Mapping_Cover_Progressive AS MCP ON MCP.Product_Code = PIC.Product_Code 
                            AND MCP.Interest_Id = PIC.Interest_ID 
                            AND MCP.Coverage_Id = PIC.Coverage_Id 
                            WHERE  PIC.Product_Code = @0 
                            AND MCP.Status = 1
                            AND MCP.Interest_Id =@1
                            AND MCP.Coverage_Id = @2
                            GROUP BY PIC.Product_Code, PIC.Interest_Id, PIC.Coverage_Id, DRS.BC_Up_To_SI, Lock_TSI";



            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                dynamic result = null;
                if (!string.IsNullOrWhiteSpace(CoverageID))
                {
                    result = db.Query<dynamic>(query, ProductCode, InterestID, CoverageID).ToList();
                }
                else
                {
                    result = db.Query<dynamic>(queryAllCoverage, ProductCode, InterestID).ToList();
                }
                return result;
            }
        }

        public static decimal GetDefaultRateProgressive(string ProductCode, string InterestID, string CoverageID, decimal SumInsured, List<DetailScoring> ListScoring)
        {
            string query1 = @"SELECT Top 1 Scoring_Code FROM dbo.Prd_Interest_Coverage AS PIC 
                            WHERE Product_Code = @0
                            AND Interest_ID = @1
                            AND Coverage_Id = @2";
            string query2 = @"SELECT  B.Interest_ID,A.sequence_no ,
                                    A.FACTOR_CODE AS FactorCode ,
                                    b.insurance_type AS InsuranceType
                            FROM    DTL_RATE_FACTOR A ,
                                    MST_INTEREST AS B
                            WHERE   A.INSURANCE_TYPE = B.INSURANCE_TYPE
                                    AND B.INTEREST_ID LIKE @0
                                    AND A.COVERAGE_ID = @1
                                    AND A.SCORE_CODE = @2
                            ORDER BY A.SEQUENCE_NO";
            //string query3 = @"SELECT insurance_type as InsuranceType,coverage_id as CoverageID, 
            //                Seq_No as RateNumber,BC_Up_To_SI as BCUpToSI, 
            //                Rate,BC_PREMIUM as BCPremium, 
            //                EXCESS_RATE as ExcessRate 
            //                FROM Mapping_Cover_Progressive 
            //                WHERE  Interest_Id =@0
            //                 AND Coverage_Id = @1
            //                 AND Insurance_Type =@2
            //                 AND BC_Up_To_SI <=  @3  And Rate>0  
            //                 {0}                        
            //                 ORDER BY BC_Up_To_SI DESC
            //                ";
            string query3 = @"SELECT insurance_type as InsuranceType,coverage_id as CoverageID, 
                            Seq_No as RateNumber,BC_Up_To_SI as BCUpToSI, 
                            Rate,BC_PREMIUM as BCPremium, 
                            EXCESS_RATE as ExcessRate 
                            FROM Mapping_Cover_Progressive 
                            WHERE  Interest_Id =@0
                             AND Coverage_Id = @1
                             AND Insurance_Type =@2
                             AND BC_Up_To_SI <=  @3 
                             {0}                        
                             ORDER BY BC_Up_To_SI DESC
                            ";

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                //List<DefaultRateProgressive> a = new List<DefaultRateProgressive>();
                decimal DefaultRate = 0;
                string ScoringCode = db.Query<string>(query1, ProductCode, InterestID, CoverageID).SingleOrDefault();
                if (!string.IsNullOrWhiteSpace(ScoringCode))
                {
                    var FactorList = db.Query<dynamic>(query2, InterestID, CoverageID, ScoringCode).ToList();
                    string WhereClasue = string.Empty;
                    string InsuranceType = string.Empty;
                    foreach (var fList in FactorList)
                    {
                        foreach (var sList in ListScoring)
                        {
                            if (fList.FactorCode.Trim().ToUpper() == sList.FactorCode.Trim().ToUpper())
                            {
                                WhereClasue = string.Format("{0} AND Rating_value{1}='{2}'", WhereClasue, fList.sequence_no, sList.InsuranceCode);
                            }
                            InsuranceType = fList.InsuranceType;
                        }
                    }
                    query3 = string.Format(query3, WhereClasue);
                    var a = db.Fetch<DefaultRateProgressive>(query3, InterestID, CoverageID, InsuranceType, SumInsured);


                    if (a != null)
                    {
                        if (a.Count > 0)
                        {
                            DefaultRate = a[0].Rate;
                        }
                    }
                }
                return DefaultRate;
            }
        }

  
        #region GetDeductible
        private static string qGetDeductible = @";DECLARE @@InterestID VARCHAR(20)  = @1;
                                        DECLARE @@CoverageID VARCHAR(20) = @2;
                                        DECLARE @@ProductCode VARCHAR(20) = @0;
                                        Declare @@BasicInterestID Char(10)
                                            SELECT TOP 1 @@BasicInterestID=mi.interest_id
                                            FROM    dbo.Prd_Interest AS pi
                                                    INNER JOIN dbo.Mst_Interest AS mi ON mi.Interest_ID = PI.Interest_ID
                                            WHERE   pi.Product_Code = @@ProductCode
                                                    AND pi.Status = 1
                                                    AND mi.status = 1        
		                                            AND mi.interest_type='P'

                                            SELECT  @@InterestID = CASE COALESCE(interest_type, '')
                                                                     WHEN 'A' THEN @@BasicInterestID
                                                                     ELSE mi.Interest_ID
                                                                   END 
                                            FROM    dbo.Prd_Interest AS pi
                                                    INNER JOIN dbo.Mst_Interest AS mi ON mi.Interest_ID = PI.Interest_ID
                                            WHERE   pi.Product_Code = @@ProductCode
                                                    AND pi.Status = 1
                                                    AND mi.status = 1
                                                    AND pi.interest_id = @@InterestID	

                                            IF ( COALESCE(@@CoverageID, '') = '' )
                                                BEGIN
                                                    SELECT TOP 1
                                                            @@CoverageID = mc.Coverage_Id
                                                    FROM    dbo.Prd_Interest_Coverage
                                                            AS pic
                                                            INNER JOIN dbo.Mst_Coverage
                                                            AS mc ON pic.Coverage_Id = mc.Coverage_Id AND mc.status=1 
                                                    WHERE   Product_Code = @@ProductCode
                                                            AND Interest_ID = @@InterestID
                                                            AND Cover_Type = 'BSC'
                                                END

                                        SELECT TOP 1
                                                DeductibleCode, pd.Description
                                        FROM    (
                                                                              SELECT TOP 1
                                                                                        a.Product_Code AS ProductCode ,
                                                                                        a.Product_Group AS ProductGroup ,
                                                                                        b.Deductible_Code AS DeductibleCode
                                                                              FROM      dbo.OR_Clause_Group_Product a
                                                                                        INNER JOIN dbo.OR_Clause_Interest_Coverage b ON a.Product_Group = b.Product_Group
                                                                              WHERE     b.Status = 1
                                                                                        AND b.Interest_Id = @@InterestID
                                                                                        AND b.Coverage_Id = @@CoverageID
                                                                                        AND a.Product_Code = @@ProductCode
                                                  UNION  


                                                  SELECT TOP 1
                                                            a.Product_Code AS ProductCode ,
                                                            a.Product_Group AS ProductGroup ,
                                                            b.Deductible_Code AS DeductibleCode
                                                  FROM      dbo.OR_Clause_Group_Product a
                                                            INNER JOIN dbo.OR_Clause_Interest_Coverage b ON a.Product_Group = b.Product_Group
                                                            INNER JOIN dbo.Mapping_Interest_Coverage_Group c ON a.Product_Group = b.Product_Group
                                                                                                      AND c.Interest_Coverage_Group_ID = b.Coverage_Id
                                                                                                      AND c.Mapping_Type = 'C'
                                                  WHERE     b.Status = 1
                                                            AND c.Status = 1
                                                            AND b.Interest_Id = @@InterestID
                                                            AND c.Interest_Coverage_ID = @@CoverageID
                                                            AND a.Product_Code = @@ProductCode
                                                  UNION 

                                                  SELECT TOP 1
                                                            a.Product_Code AS ProductCode ,
                                                            a.Product_Group AS ProductGroup ,
                                                            b.Deductible_Code AS DeductibleCode
                                                  FROM      dbo.OR_Clause_Group_Product a
                                                            INNER JOIN dbo.OR_Clause_Interest_Coverage b ON a.Product_Group = b.Product_Group
                                                            INNER JOIN dbo.Mapping_Interest_Coverage_Group c ON a.Product_Group = b.Product_Group
                                                                                                      AND c.Interest_Coverage_Group_ID = b.Interest_Id
                                                                                                      AND c.Mapping_Type = 'I'
                                                  WHERE     b.Status = 1
                                                            AND c.Status = 1
                                                            AND a.Product_Code = @@ProductCode
                                                            AND c.Interest_Coverage_ID = @@InterestID
                                                            AND b.coverage_id = ''
                                                UNION

		                                        SELECT TOP 1
                                                                                        a.Product_Code AS ProductCode ,
                                                                                        a.Product_Group AS ProductGroup ,
                                                                                        b.Deductible_Code AS DeductibleCode
                                                                              FROM      dbo.OR_Clause_Group_Product a
                                                                                        INNER JOIN dbo.OR_Clause_Interest_Coverage b ON a.Product_Group = b.Product_Group
                                                                              WHERE     b.Status = 1
                                                                                        AND b.Interest_Id = @@InterestID
                                                                                        AND b.Coverage_Id = ''
                                                                                        AND a.Product_Code = @@ProductCode          
                                                ) aa
                                            Inner join dbo.Prd_Deductible pd on  pd.Deductible_Code=aa.DeductibleCOde
                                        ";
        #endregion
        public static string GetGenericInterestID(string InterestID)
        {
            dynamic result = null;
            string query = @";DECLARE @@Value AS VARCHAR(100)
                            SELECT  TOP 1 @@Value=ConfigurationValue
                            FROM    dbo.GeneralConfiguration AS gc
                            WHERE   ConfigurationName = 'GENERIC_INTEREST_ID_MAPPING'
                                AND ReferenceType = 'INTEREST_ID'
	                            AND ReferenceID=@0
                            SELECT COALESCE (@@Value,@0) AS Result";
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                result = db.Query<string>(query, InterestID).SingleOrDefault();
                return result;
            }
        }

        public static string GetGenericCoverageID(string CoverageID)
        {
            dynamic result = null;
            string query = @";DECLARE @@Value AS VARCHAR(100)
                            SELECT  TOP 1 @@Value=ConfigurationValue
                            FROM    dbo.GeneralConfiguration AS gc
                            WHERE   ConfigurationName = 'GENERIC_COVERAGE_ID_MAPPING'
                                AND ReferenceType = 'COVERAGE_ID'
	                            AND ReferenceID=@0
                            SELECT COALESCE (@@Value,@0) AS Result";
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                result = db.Query<string>(query, CoverageID).SingleOrDefault();
                return result;
            }
        }


        public static dynamic GetDeductible(string ProductCode, string InterestId, string CoverageId)
        {
            InterestId = GetGenericInterestID(InterestId);
            CoverageId = GetGenericCoverageID(CoverageId);
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                var result = db.Query<dynamic>(qGetDeductible, ProductCode, InterestId, CoverageId).ToList();
                if (result != null)
                    return result.FirstOrDefault();
                else
                    return null;
            }
        }
     
        private static decimal mf_CLevel_TotalPercentage(List<CScheme> Schemes)
        {
            decimal Total = 0;
            var i = 0;

            for (i = 0; i < Schemes.Count; i++)
            {
                if (Schemes[i].Active && Schemes[i].Percentage != 0)
                {
                    Total += Schemes[i].Percentage;
                }
            }
            return (Total);
        }
        private static decimal mf_CLevel_TotalFlatAmount(List<CScheme> Schemes)
        {
            decimal Total = 0;
            var i = 0;

            for (i = 0; i < Schemes.Count; i++)
            {
                if (Schemes[i].Active && Schemes[i].Percentage == 0)
                {
                    Total += Schemes[i].Amount;
                }
            }
            return (Total);
        }
        public static PremiumScheme CPremiumScheme(decimal CoverPremium)
        {
            PremiumScheme PS = new PremiumScheme()
            {
                GrossPremium = CoverPremium,
                NetPremium = CoverPremium,
                Net1 = CoverPremium,
                Net2 = CoverPremium,
                Net3 = CoverPremium,
                CoverPremium = CoverPremium
            };
            return PS;

        }
        public static dynamic GetProductInterestCoverage(string ProductCode, string InterestID = "", string CoverageID = "", string ComprehensiveFlag = "", string TLOFlag = "")
        {
            bool UseBasicFlag = true;
            int Atemp = 0;
        BSCFiltering:
            Atemp = Atemp + 1;
            string query = @";DECLARE @@ProductCode VARCHAR(10) ,
                                @@InterestID VARCHAR(10) ,
                                @@CoverageID VARCHAR(10) ,
                                @@ComprehensiveFlag BIT
                            SELECT  @@ProductCode = @0 ,
                                    @@InterestID = @1 ,
                                    @@CoverageID = @2
                            SELECT DISTINCT  a.Product_Code AS ProductCode ,
                                    a.Coverage_ID AS CoverageID ,
                                    a.Rate ,
                                    a.Scoring_Code AS ScoringCode ,
                                    a.Change_Rate AS ChangeRate ,
                                    b.[Name] AS Description ,
                                    d.Description AS InterestDescription,
                                    b.Cover_Type AS CoverType ,
                                    b.Claim_Mode AS ClaimMode ,
                                    Premi_Rate AS PremiumRate ,
                                    ISNULL(z.scoring_flags, 0) AS ScoringFlags ,
                                    b.Status ,
                                    Bc_Max_Si AS BaseCurrencyMaximumSumInsured ,
                                    Excess_Rate AS ExcessRate ,
                                    Bc_Min_Premium AS BaseCurrencyMinimumPremium ,
                                    Discounts AS Discounts ,
                                    ISNULL(Given_SI, 0) AS GivenSumInsured ,
                                    ISNULL(Given_Curr_ID, '') AS GivenCurrencyID ,
                                    b.Cover_Insurance AS CoverInsurance ,
                                    ISNULL(C.Auto_apply, 0) AS Auto_apply ,
                                    ISNULL(C.Deductible_Code, '') AS AutoDeductibleCode ,
                                    ISNULL(C.Deductible_Amount, 0) AS AutoDeductibleAmount ,
                                    c.Default_Currency AS DefaultCurrency,
                                    c.Comprehensive_Flag AS ComprehensiveFlag,
                                    c.TLO_Flag AS TLOFlag
                            FROM    dbo.Prd_Interest_Coverage a
                                    LEFT OUTER JOIN rate_scoring z ON z.score_code = a.scoring_code
                                                                      AND z.coverage_id = a.coverage_id
                                    LEFT OUTER JOIN b2b_interest_coverage c ON A.Product_code = C.Product_Code
                                                                               AND a.interest_id = c.interest_id
                                                                               AND a.coverage_id = c.coverage_id
                                                                               AND c.Vehicle_Type = 'ALL'
                                                                               AND c.Usage = 'ALL'
                                    INNER JOIN dbo.Mst_Coverage AS b ON A.Coverage_ID = b.Coverage_ID
                                    INNER JOIN dbo.Mst_Interest AS d ON A.Interest_ID = d.Interest_Id
                            WHERE   a.Product_Code = @@ProductCode
                                    AND a.IC_STATUS <> 0
                           ";
            if (!string.IsNullOrWhiteSpace(InterestID))
            {
                query += " AND a.INTEREST_ID = @@InterestID ";
            }
            if (!string.IsNullOrWhiteSpace(CoverageID))
            {
                query += " AND a.COVERAGE_ID = @@CoverageID ";
            }

            if (UseBasicFlag)
            {

                if (!string.IsNullOrWhiteSpace(ComprehensiveFlag))
                {
                    if (ComprehensiveFlag == "1" && TLOFlag == "0")
                        query += " AND c.Comprehensive_Flag = 1 ";
                    else if (ComprehensiveFlag == "0" && TLOFlag == "1")
                        query += " AND c.TLO_Flag = 1 ";
                    else if (ComprehensiveFlag == "1" && TLOFlag == "1")
                        query += " AND (c.TLO_Flag = 1 OR c.Comprehensive_Flag = 1) ";
                }
            }
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                dynamic result = db.Query<dynamic>(query, ProductCode, InterestID, CoverageID).ToList();
                if (result.Count <= 0 && Atemp < 3)
                {
                    UseBasicFlag = false;
                    goto BSCFiltering;
                }
                return result;
            }
        }
        public static List<string> GetGeneralConfig(string ConfigName, string RefType, string RefID, string ConfigValue = "")
        {
            string query = @";EXEC [dbo].usp_GetGeneralConfig
                            @@ConfigurationName =@0,
                            @@ReferenceType =@1,
                            @@ReferenceID =@2,                            
	                        @@ConfigurationValue=@3";

            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                return db.Query<string>(query, ConfigName, RefType, RefID, ConfigValue).ToList();
            }
        }

        public static PremiumScheme CalculateDiscountPremium(List<CommissionScheme> Discounts, decimal CoverPremium)
        {
            PremiumScheme PS = CPremiumScheme(CoverPremium);
            PS.CoverPremium = CoverPremium;
            for (int i = 0; i < Discounts.Count; i++)
            {
                decimal TotalPercentage = 0;
                decimal TotalFlatAmount = 0;
                if (Discounts[i].Schemes != null)
                {
                    TotalPercentage = mf_CLevel_TotalPercentage(Discounts[i].Schemes);
                    TotalFlatAmount = mf_CLevel_TotalFlatAmount(Discounts[i].Schemes);
                }
                switch (i)
                {
                    case 0:
                        PS.GrossPremium = (PS.CoverPremium * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 1:
                        PS.Net1 = (PS.GrossPremium * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 2:
                        PS.Net2 = (PS.Net1 * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 3:
                        PS.Net3 = (PS.Net2 * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                    case 4:
                        PS.NetPremium = (PS.Net3 * (100 - TotalPercentage) / 100) - TotalFlatAmount;
                        break;
                }
                PS.CoverPremium = PS.NetPremium;

            }
            return (PS);
        }

        public static List<CommissionScheme> InitDiscountList(string CoverageID, List<WTCommission> DiscountList, bool AutoApply = false)
        {
            int g_DiscountMap = 1;
            int i;

            int level;
            List<CommissionScheme> DiscountScheme = new List<CommissionScheme>();
            var ApplyFlags = 0;

            for (i = 0; i <= 4; i++)
            {
                CommissionScheme CScheme = new CommissionScheme
                {
                    TotalFlatAmount = 0,
                    TotalPercentage = 0,
                    Level = i,
                    Schemes = new List<CScheme>()
                };
                CommissionScheme CMScheme = new CommissionScheme();
                DiscountScheme.Add(CMScheme);
            }

            for (i = 0; i < DiscountList.Count; i++)
            {
                ApplyFlags = DiscountList[i].ApplyFlags;

                if ((ApplyFlags & g_DiscountMap) == g_DiscountMap)
                {
                    if (string.IsNullOrWhiteSpace(DiscountList[i].CoverageID) || DiscountList[i].CoverageID.Trim().ToUpper() == CoverageID)
                    {
                        level = 0;
                        level = DiscountList[i].Level;


                        if (AutoApply)
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = DiscountList[i].CommissionID,
                                Name = DiscountList[i].CommissionName,
                                Type = DiscountList[i].CommissionType,
                                Percentage = DiscountList[i].Percentage,
                                FlatAmount = 0,
                                Amount = DiscountList[i].Percentage,
                                PayToCode = "1",
                                ApplyFlags = DiscountList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);
                        }
                        else
                        {
                            CScheme C = new CScheme()
                            {
                                Active = true,
                                ID = DiscountList[i].CommissionID,
                                Name = DiscountList[i].CommissionName,
                                Type = DiscountList[i].CommissionType,
                                Percentage = DiscountList[i].Percentage,
                                FlatAmount = 0,
                                Amount = 0,
                                PayToCode = "1",
                                ApplyFlags = DiscountList[i].ApplyFlags
                            };
                            if (DiscountScheme[level].Schemes == null)
                            {
                                DiscountScheme[level].Schemes = new List<CScheme>();
                            }
                            DiscountScheme[level].Schemes.Add(C);
                        }
                    }
                }
            }

            return DiscountScheme;
        }

        public static List<CommissionScheme> InitFlatDiscount(string g_ProductCode, decimal CoverPremium, List<CommissionScheme> CommissionScheme, decimal SumInsured, decimal ExchangeRate, string IDType, bool AutoApply = false)
        {
            int i, j;

            for (i = 0; i < CommissionScheme.Count; i++)
            {
                if (CommissionScheme[i] != null)
                {
                    if (CommissionScheme[i].Schemes != null)
                    {
                        for (j = 0; j < CommissionScheme[i].Schemes.Count; j++)
                        {
                            if (CommissionScheme[i].Schemes[j] != null)
                            {
                                if (CommissionScheme[i].Schemes[j].Percentage == 0)
                                {
                                    CommissionScheme[i].Schemes[j].SI = SumInsured;

                                    CommissionScheme[i].Schemes[j].OriginalFlatAmount = GetFlatAmount(i, CommissionScheme, CoverPremium, g_ProductCode, CommissionScheme[i].Schemes[j].ID, CommissionScheme[i].Schemes[j].SI, ExchangeRate, IDType);
                                    CommissionScheme[i].Schemes[j].FlatAmount = CommissionScheme[i].Schemes[j].OriginalFlatAmount;

                                    if (AutoApply)
                                    {
                                        CommissionScheme[i].Schemes[j].Amount = CommissionScheme[i].Schemes[j].FlatAmount;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return CommissionScheme;
        }
        public static dynamic Product_GetFlatCommission(string ProductCode, string ID, decimal BCSI, string IDType)
        {

            string query = @"SELECT  a.comm_id AS CommID ,
                                    a.bc_from_si AS BaseCurrencySumInsured ,
                                    a.percentage AS Percentage ,
                                    a.bc_amount AS BaseCurrencyAmount ,
                                    a.status AS Status
                            FROM    mst_flat_commission a
                            WHERE   a.status <> 0
                                    AND a.comm_id = @0
                                    AND a.bc_from_si =@1
                            ORDER BY a.bc_from_si DESC	";
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("a2isRetailDB"))
            {
                var result = db.Query<dynamic>(query, ID, BCSI).FirstOrDefault();
                return result;
            }
        }

        public static decimal GetFlatAmount(int Level, List<CommissionScheme> DiscCommList, decimal CoverPremium, string ProductCode, string ID, decimal SumInsured, decimal ExchangeRate, string IDType)
        {
            decimal tmpAmount = 0;

            decimal tmpBCAmount = 0;
            decimal tmpPercentage = 0;


            var Result = Product_GetFlatCommission(ProductCode, ID, SumInsured * ExchangeRate, IDType);

            if (Result != null)
            {
                foreach (var item in Result)
                {
                    if (item.CommissionPercentage == 0)
                    {
                        tmpBCAmount = item.FlatCommissionAmount;
                        tmpPercentage = item.CommissionPercentage;
                    }
                }
                var PS = CalculateDiscountPremium(DiscCommList, CoverPremium);
                switch (Level)
                {
                    case 0:
                        tmpAmount = (PS.GrossPremium * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 1:
                        tmpAmount = (PS.Net1 * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 2:
                        tmpAmount = (PS.Net2 * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 3:
                        tmpAmount = (PS.Net3 * tmpPercentage / 100) - tmpBCAmount;
                        break;
                    case 4:
                        tmpAmount = (PS.NetPremium * tmpPercentage / 100) - tmpBCAmount;
                        break;
                }

                tmpAmount = tmpBCAmount / ExchangeRate;


            }
            return (tmpAmount);
        }

        //public static void CopyImageDataToMstImage(string followUpNo, string prospectId, string orderNo)
        //{
        //    string UserID = ConfigurationManager.AppSettings["UserID"].ToString();
        //    //UPLOAD IMAGE

        //    string referenceNo = "";
        //    List<dynamic> followUpImages = new List<dynamic>();
        //    if (!string.IsNullOrEmpty(prospectId))
        //    {
        //        followUpImages = GetNotUploadedFollowUpPersonalImages(followUpNo);
        //        referenceNo = prospectId;
        //    }
        //    else
        //    {
        //        followUpImages = GetNotUploadedFollowUpOrderImages(followUpNo);
        //        referenceNo = orderNo;
        //    }
        //    foreach (var fui in followUpImages)
        //    {
        //        //tambahin dlu validasi
        //        string referenceImageType = GetMappedReferenceTypeImage(fui.ImageType, false);
        //        string outImageID = null;
        //        if (UploadImage(fui.Data, fui.ImageName, referenceImageType, 1, UserID, referenceNo, out outImageID) && outImageID != null)
        //        {
        //            // setelah upload, update ke AABMobile.dbo.Mst_Image kolom CoreImage_Id, 
        //            UpdateImageData(fui.ImageID, outImageID);
        //        }
        //    }
        //    DeleteUnusedImages(followUpNo);
        //}
        #region Copy Image Otosales
        public static bool UploadImage(byte[] image, string imageFilename, string referenceImageType, int objectNo, string UserID, string referenceNo, out string outImageID)
        {
            string qInsertImage = @"INSERT INTO dbo.Mst_Image
                                            ( Image_Id ,
                                              Image ,
                                              Description ,
                                              EntryUsr ,
                                              EntryDt ,
                                              Thumbnail ,
                                              Status ,
                                              File_Name ,
                                              File_Size
                                            )
                                    VALUES  ( @0 , -- Image_Id - char(12)
                                              @1 , -- Image - image
                                              @2 , -- Description - varchar(255)
                                              @3 , -- EntryUsr - char(5)
                                              GETDATE() , -- EntryDt - datetime
                                              @4 , -- Thumbnail - image
                                              0 , -- Status - smallint
                                              @5 , -- File_Name - varchar(512)
                                              @6 -- File_Size - int
                                    ) ";
            string qInsertDtlImage = @"INSERT INTO dbo.Dtl_Image
                                                ( Reference_No ,
                                                  Reference_Type ,
                                                  Number ,
                                                  Object_No ,
                                                  Image_Id ,
                                                  EntryUsr ,
                                                  EntryDt
                                                )
                                        VALUES  ( @0 , -- Reference_No - varchar(20)
                                                  @1 , -- Reference_Type - char(6)
                                                  @2 , -- Number - int
                                                  @3 , -- Object_No - int
                                                  @4 , -- Image_Id - char(12)
                                                  @5 , -- EntryUsr - char(5)
                                                  GETDate()  -- EntryDt - datetime
                                                )";
            List<bool> listResult = new List<bool>();
            string referenceNoTemp = referenceNo;

            int newNumber = 0;
            if (!string.IsNullOrEmpty(referenceNo))
                newNumber = GetNewNumberImage(referenceNo);

            string imageID = IDGenereation("IMAGE", 12);//GetImageID();
            if (string.IsNullOrEmpty(referenceNoTemp))
                referenceNoTemp = "TempRef-" + imageID;
            byte[] byteImgThum = null;
            byte[] byteImg = null;

            byte[] imageFileCode = image;
            byteImgThum = EncodeToAAB2000ImageBytes(getResizedImage(imageFileCode, 95, 71));
            byteImg = EncodeToAAB2000ImageBytes(getResizedImage(imageFileCode));


            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            { 
                var mst = 0;
                mst = db.Execute(qInsertImage,
                                        imageID,
                                        byteImg,
                                        imageFilename + "(" + referenceImageType + ")",
                                        UserID,
                                        byteImgThum,
                                        imageFilename,
                                        byteImg.Length);
                if (mst > 0)
                {
                    var dtl = db.Execute(qInsertDtlImage,
                                            referenceNoTemp,
                                            referenceImageType,
                                            newNumber,
                                            objectNo,
                                            imageID,
                                            UserID);
                    if (dtl > 0)
                    {
                        listResult.Add(true);
                    }
                    newNumber++;
                }
            }
            //referenceNoOut = referenceNoTemp;
            outImageID = imageID;
            if (!listResult.Contains(false))
            {
                return true;
            }
            else
                return false;
        }
        private static byte[] EncodeToAAB2000ImageBytes(byte[] sourceBytes)
        {
            byte[] encodedBytes = Encoding.Convert(Encoding.Default, Encoding.Unicode, sourceBytes);
            return encodedBytes;
        }
        public static string IDGenereation(string IDType, int IDLength)
        {
            string CurrentMethod = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string qInsertID = @"INSERT  INTO dtl_urut ( code, no_urut, entrydt ) VALUES  ( @0, @1, GETDATE() )";
            string qInsertDuplicate = @"insert into dtl_urut_duplicate (code, no_urut, entrydt) values (@0,@1,getdate())";
            string spGenerateID = @";EXEC sp_IDGeneration @0";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            int IDNo = 0;
            try
            {
                var temp = db.FirstOrDefault<dynamic>(spGenerateID, IDType);
                IDNo = Convert.ToInt32(temp.no_urut);
                db.Execute(qInsertID, IDType, IDNo);
                return IDNo.ToString().PadLeft(IDLength, '0');
            }
            catch (SqlException sqle)
            {
                int code = sqle.ErrorCode;
                if (code == -2147217873)
                {
                    db.Execute(qInsertDuplicate, IDType, IDNo);
                }
                return "";
            }
        }

        private static byte[] getResizedImage(Byte[] data, int width = 0, int height = 0)
        {
            Bitmap imgIn = null;
            Bitmap imgOut = null;
            try
            {
                using (var ms = new MemoryStream(data))
                {
                    imgIn = new Bitmap(ms);
                }

                double y = imgIn.Height;
                double x = imgIn.Width;

                double factor = 1;
                if (width > 0)
                {
                    factor = width / x;
                }
                else if (height > 0)
                {
                    factor = height / y;
                }
                System.IO.MemoryStream outStream = new System.IO.MemoryStream();
                imgOut = new Bitmap((int)(x * factor), (int)(y * factor));

                // Set DPI of image (xDpi, yDpi)
                imgOut.SetResolution(72, 72);

                Graphics g = Graphics.FromImage(imgOut);
                g.Clear(Color.White);
                g.DrawImage(imgIn, new Rectangle(0, 0, (int)(factor * x), (int)(factor * y)),
                  new Rectangle(0, 0, (int)x, (int)y), GraphicsUnit.Pixel);

                //imgOut.Save(outStream, getImageFormat(""));
                imgOut.Save(outStream, ImageFormat.Jpeg);
                return outStream.ToArray();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            finally
            {
                imgIn.Dispose();
                imgOut.Dispose();
                imgIn = null;
                imgOut = null;
            }

        }

    
        public static int GetNewNumberImage(string refNo)
        {
            int ret = 0;
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            { 
                string q = @"SELECT TOP 1 Number FROM dbo.Dtl_Image WHERE Reference_No = @0 ORDER BY Number DESC";
                var dtl = db.Fetch<dynamic>(q, refNo);
                if (dtl.Count() > 0)
                {
                    int l = dtl.FirstOrDefault().Number != null ? dtl.FirstOrDefault().Number : 0;
                    ret = l + 1;
                }
            }
            return ret;
        }
        //public static string GetMappedReferenceTypeImage(string imageTypeOtosales, bool IsSurvey)
        //{
        //    switch (imageTypeOtosales)
        //    {
        //        case "IdentityCard": return IsSurvey ? ReferenceTypeImage.KTPSURVEY : ReferenceTypeImage.KTP;
        //        case "STNK": return ReferenceTypeImage.STNK;
        //        case "SPPAKB": return ReferenceTypeImage.SPPAKB;
        //        case "BSTB1": return ReferenceTypeImage.BSTB;
        //        case "NPWP": return ReferenceTypeImage.NPWP;
        //        case "SIUP": return ReferenceTypeImage.SIUP;
        //        case "Faktur": return ReferenceTypeImage.FAKTUR;
        //        case "KonfirmasiCust": return ReferenceTypeImage.CONFIRMATIONCUSTOMER;
        //        case "DocNSA1": return ReferenceTypeImage.NSA;
        //        case "DocNSA2": return ReferenceTypeImage.NSA;
        //        case "DocNSA3": return ReferenceTypeImage.NSA;
        //        case "DocRep": return ReferenceTypeImage.DOCREP;
        //        case "DocPendukung": return ReferenceTypeImage.DOCPENDUKUNG;
        //        case "BuktiBayar": return ReferenceTypeImage.BUKTIBAYAR;

        //        default: return string.Empty;
        //    }
        //}

      
        public static int UpdateImageData(string imageID, string CoreImage_ID)
        {
            string qUpdate = @"UPDATE ImageData SET LastUpdatedTime = GETDATE(), CoreImage_ID = @0 WHERE ImageID = @1";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                return db.Execute(qUpdate, CoreImage_ID, imageID);
            }
        }

        public static List<dynamic> GetNotUploadedFollowUpImages(string followUpNo)
        {
            string q = @"
SELECT a.FollowUpNo, a.ImageID, a.ImageName, a.ImageType, id.Data FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, IdentityCard, STNK, SPPAKB, BSTB1, NPWP, SIUP, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung --DocRep, CustomerConfirmation, *
from FollowUp fu
LEFT JOIN ProspectCompany pcm ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		IdentityCard, STNK, SPPAKB, BSTB1, NPWP, SIUP, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id on ImageName = id.PathFile 
and CoreImage_ID is null' ";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                return db.Fetch<dynamic>(q, followUpNo);
            }
        }
        #endregion
        public static List<WTCommission> GetWTDiscount( string ProductCode, string MouID)
        {

            string query = @"SELECT DISTINCT
                                *
                        FROM    ( SELECT    pc.Apply_Flags AS ApplyFlags ,
                                            PC.Comm_Id AS CommissionID ,
                                            mc.Name AS CommissionName ,
                                            mc.Percentage AS Percentage ,
                                            pc.Pay_To_Party AS PayToParty ,
                                            mc.level_Disc AS Level ,
                                            mc.Tax_Indiv AS IndividualTax ,
                                            mc.Tax_Corp AS CorporateTax ,
                                            mc.Commission_Type AS CommissionType
                                  FROM      Prd_Commission pc
                                            INNER JOIN Mst_Commission mc ON pc.Comm_ID = mc.COMM_ID
                                            LEFT JOIN dbo.Prd_Profit_Renewal_Disc AS rcd ON rcd.Comm_ID = pc.COMM_ID and pc.product_code=rcd.Product_Code
                                  WHERE     pc.Status = 1
                                            AND mc.Status = 1
                                            AND mc.Commission_Type IN ( '1' )
                                            AND COALESCE(rcd.Comm_ID, '') = ''
                                            AND pc.Product_Code = @0
                                  UNION ALL
                                  SELECT    pc.Apply_Flags AS ApplyFlags ,
                                            PC.Commission_Code AS CommissionID ,
                                            mc.Name AS CommissionName ,
                                            mc.Percentage AS Percentage ,
                                            pc.Pay_To_Party AS PayToParty ,
                                            mc.level_Disc AS Level ,
                                            mc.Tax_Indiv AS IndividualTax ,
                                            mc.Tax_Corp AS CorporateTax ,
                                            mc.Commission_Type AS CommissionType
                                  FROM      dbo.MOU_Commission AS pc
                                            INNER JOIN Mst_Commission mc ON pc.Commission_Code = mc.COMM_ID
                                            LEFT JOIN dbo.Prd_Profit_Renewal_Disc AS rcd ON rcd.Comm_ID = pc.Commission_Code and pc.mou_id=rcd.Product_Code
                                  WHERE     mc.Status = 1
                                            AND COALESCE(rcd.Comm_ID, '') = ''
                                            AND mc.Commission_Type IN ( '1' )
                                            AND pc.MOU_ID = @1
                                ) AS X Where 1=1";
            string qGetRenDisc = "Select LTRIM(RTRIM(CODE)) From Mst_General Where type='CRD'";

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                List<string> RenDisc = db.Query<string>(qGetRenDisc).ToList();
                foreach (var vRenDisc in RenDisc)
                {
                    query = string.Format("{0} AND X.CommissionID NOT LIKE '{1}%'", query, vRenDisc);
                }
                if (string.IsNullOrWhiteSpace(MouID))
                {
                    MouID = ProductCode;
                }
                var result = db.Query<WTCommission>(query, ProductCode, MouID).ToList();
                return result;
            
        }

        public static List<dynamic> GetNotUploadedFollowUpPersonalImages(string followUpNo)
        {
            string q = @"
SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, IdentityCard, NPWP, SIUP
from FollowUp fu
LEFT JOIN ProspectCompany pcm ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		IdentityCard, NPWP, SIUP
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id on ImageName = id.PathFile 
and CoreImage_ID is null ";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                return db.Fetch<dynamic>(q, followUpNo);
            }
        }

        public static List<dynamic> GetNotUploadedFollowUpOrderImages(string followUpNo)
        {
            string q = @"
SELECT a.FollowUpNo, id.ImageID, a.ImageName, a.ImageType, id.Data FROM 
(
SELECT 
	FollowUpNo,ImageName, ImageType
FROM 
(
select fu.FollowUpNo, STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung --DocRep, CustomerConfirmation, *
from FollowUp fu
LEFT JOIN ProspectCompany pcm ON fu.CustID = pcm.CustID
where fu.followupno = @0
)a 
UNPIVOT (
	ImageName FOR ImageType IN (
		STNK, SPPAKB, BSTB1, Faktur, KonfirmasiCust, DocNSA1, DocNSA2, DocNSA3, DocRep, BuktiBayar, DocPendukung
	)
) unpvt
where ImageName <> ''
) a 
INNER JOIN ImageData id on ImageName = id.PathFile 
and CoreImage_ID is null ";
            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                return db.Fetch<dynamic>(q, followUpNo);
            }
        }
        public static bool DeleteUnusedImages(string FollowUpNo)
        {
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            var MobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            string query = @"";
            int res = 0;
            try
            {
                query = @"SELECT PolicyOrderNo,ProspectID FROM dbo.OrderSimulation os
                        INNER JOIN dbo.ProspectCustomer p
                        ON p.CustID = os.CustID
                        WHERE os.FollowUpNo = @0
                        AND os.ApplyF = 1 AND os.RowStatus = 1";
                List<dynamic> data = MobileDB.Fetch<dynamic>(query, FollowUpNo);
                if (data.Count > 0)
                {
                    string PolicyOrderNo = data.First().PolicyOrderNo;
                    string ProspectID = data.First().ProspectID;
                    if (!string.IsNullOrEmpty(PolicyOrderNo))
                    {
                        query = @"SELECT  di.Image_Id, id.RowStatus
                                FROM dbo.Dtl_Image di
                                INNER JOIN dbo.Mst_Image 
                                mi ON mi.Image_Id = di.Image_Id
							    INNER JOIN BEYONDMOSS.AABMobile.dbo.ImageData id
							    ON id.CoreImage_ID = di.Image_Id
                                WHERE di.Reference_No = @0 OR di.Reference_No = @1";
                        List<dynamic> ListImage = AABDB.Fetch<dynamic>(query, PolicyOrderNo, ProspectID);
                        foreach (dynamic item in ListImage)
                        {
                            if (Convert.ToInt32(item.RowStatus) == 0)
                            {
                                query = @";
                                DELETE FROM AAB_Image.dbo.Dtl_Image WHERE Image_Id = @0
                                DELETE FROM AAB_Image.dbo.Mst_Image WHERE Image_Id = @0";
                                AABDB.Execute(query, item.Image_Id);
                                res++;
                            }
                        }
                    }
                }
                return res > 0;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}