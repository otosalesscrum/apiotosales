﻿using a2is.Framework.DataAccess;
using a2is.Framework.Monitoring;
using A2isMessaging;
using Microsoft.Reporting.WinForms;
using Newtonsoft.Json;
using Otosales.Codes;
using Otosales.dta;
using Otosales.Models;
using Otosales.Models.v0107URF2020;
using PremiumCalculation.Manager;
using PremiumCalculation.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using System.Web;

namespace Otosales.Repository.v0107URF2020
{
    public interface IMobileRepository
    {
        List<CalculatedPremi> CalculateBasicPremi(List<CoverageParam> CoverageList, string ProductCode, string vMouID, string vcitycode, string vusagecode, string vBrand, string vtype, string vModel, string vIsNew,
                                                                      string vsitting, string vyear, DateTime PeriodFrom, DateTime PeriodTo, string Ndays, decimal vTSInterest, decimal vPrimarySI, string vInterestId,
                                                                       string OldPolicyNo, out List<decimal> RenDiscountPct, out bool isProductSupported);
        List<CalculatedPremi> CalculateACCESS(decimal vPrimarySI, decimal TSIInterest, string ProductCode, string vMouID, DateTime PeriodFrom, DateTime PeriodTo, string InterestID, string vtype, string vyear, string vsitting, string QuotationNo, List<CalculatedPremi> calculatePremiItems, List<DetailScoring> scoringList,
                                                    string OldPolicyNo, out List<decimal> RptDiscount, out List<decimal> DiscountTemp);
        List<Models.v0107URF2020.Product> GetProduct(string insurancetype, string salesofficerid, string searchParam, int isRenewal, bool isNew = false, bool isMVGodig = false);
        List<CalculatedPremi> calculatepremicalculation(string ProductCode, string vInterestId, CoverageParam cp, DateTime PeriodFrom, DateTime PeriodTo, string vtype, string vyear
                                                      , string vsitting, List<DetailScoring> dtlScoring, string Ndays, decimal vTSInterest, string vMouID, string OldPolicyNo, List<CalculatedPremi> CalculatedPremiItems
                                                       , int calcmethod, out List<decimal> RenDiscountPct, out List<decimal> DiscountPct);
        List<CalculatedPremi> RecalculateAccess(decimal vTSIInterest, string vCoverageId, List<CalculatedPremi> calpremiitems, string ProductCode, string MouID, DateTime PeriodFrom, DateTime PeriodTo,
                                                        string vtype, string vyear, string vsitting, string pQuotationNo, List<DetailScoring> dtlScoring, string OldPolicyNo, out List<decimal> RenDiscountPct, out List<decimal> DiscountPct);
        double RecalculateExtendedCover(string interestid, CoverageParam cp, DateTime? PeriodFrom, DateTime? PeriodTo, string ProductCode, string MouID, List<DetailScoring> dtlScoring, string vyear, string UsageCode, int Ndays
                                               , string vtype, string vsitting, decimal vTSInterest, string OldPolicyNo, int calcmethod, bool isrenew);
        DetailCoverageSummary GetDetailCoverageSummary(string ProductCode, string MouID, List<DetailScoring> dtlScoring, List<CalculatedPremi> CalculatedPremiItems, double AdminFee, string vyear, string UsageCode, int Ndays
                                                       , DateTime? PeriodFrom, DateTime? PeriodTo, string vtype, string vsitting, string OldPolicyNo, out double DiscAmount);
        Otosales.Models.vWeb2.TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo);
        string UpdateTaskListDetail(string CustID, string FollowUpNo,
Otosales.Models.vWeb2.PersonalData pd, Otosales.Models.vWeb2.CompanyData cd, Otosales.Models.vWeb2.VehicleData vd, Otosales.Models.vWeb2.PolicyAddress pa,
Otosales.Models.vWeb2.SurveySchedule ss, List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, Models.vWeb2.OrderSimulationModel OS, Models.vWeb2.OrderSimulationMVModel OSMV,
Models.vWeb2.CalculatePremiModel CalculatePremiModel, string FollowUpStatus, Otosales.Models.vWeb2.Remarks rm);
        Models.v0219URF2019.DownloadQuotationResult GetByteReport(string orderNo);
        A2isCommonResult SendQuotationEmail(string OrderNo, string Email);
        bool SendViaWA(Models.v0219URF2019.SendViaWAParam param);
        A2isCommonResult SendViaEmail(string EmailTo, string EmailCC, string EmailBCC
    , string body, string subject, List<A2isMessagingAttachment> att, string OrderNo, bool IsAddAtt);
        void GenerateOrderSimulation(string salesOfficerID, string custID, string followUpID, string orderID, string PolicyNo, string remarkToSA, int isPSTB);
        dynamic GetSurveyScheduleSurveyManagement(string cityid, string zipcode, DateTime d);
        dynamic GetScheduleSurveyDalam(List<string> TimeCodeSurveyDalam);
        List<string> getSurveyCategoryInternalSurvey(string BranchID, DateTime d);
        List<string> GetSurveyDays(string BranchID);
        void ClearFailedData(string custID, string followUpID, string orderID, string PolicyNo);
        void SaveImageProspect(string FollowUpNo);
    }
    public class MobileRepository : IMobileRepository
    {
        private static readonly a2isLogHelper logger = new a2isLogHelper();

        private static PremiumCalculationManager pm = new PremiumCalculationManager();

        public List<Models.v0107URF2020.Product> GetProduct(string insurancetype, string salesofficerid, string searchParam, int isRenewal, bool isNew = false, bool isMVGodig = false)
        {
            List<Models.v0107URF2020.Product> result = new List<Models.v0107URF2020.Product>();
            try
            {
                string query = @"";
                using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    bool isAgency = db.ExecuteScalar<int>(@";DECLARE @@Email VARCHAR(512)
SELECT @@Email=Email FROM [BeyondMoss].[AABMobile].[dbo].[SalesOfficer] WHERE SalesOfficerID = @0
SELECT COUNT(*) FROM [a2isAuthorizationDB].[General].[ExternalUsers] WHERE UserID = @@Email", salesofficerid) > 0 ? true : false;
                    //query = string.Concat(query, isAgency ? " AND mp.product_type=6 " : " AND mp.product_type!=6 ");
                    if (string.IsNullOrEmpty(searchParam))
                    {
                        query = @"
select DISTINCT  RTRIM(mp.Product_Code) MouID, RTRIM(mp.Product_Code) [ProductCode], mp.Description, 'GARDAOTO' [ProductTypeCode], @0 [InsuranceType], 1 [RowStatus], CASE WHEN mp.day_calculation_method =360 THEN 360 ELSE 366 END [Ndays], Biz_type,mp.cob_id, IIF(PDT.Partner_ID is null, 0, 1) [IsLexus]
from mst_product mp
LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code AND PDT.Partner_ID = 'LEX'
where GETDATE() BETWEEN Valid_From and Valid_To AND mp.Product_Code !='' AND mp.isb2b=0
AND 1 = CASE WHEN @0 = 1 THEN IIF(mp.cob_id = '403' and Biz_type=1 and PDT.Partner_ID is null,1,0)
WHEN @0 = 2 THEN IIF(mp.cob_id= '404' and PDT.Partner_ID is null,1,0)
WHEN @0 = 3 THEN IIF(PDT.Partner_ID is not null,1,0) 
WHEN @0 = 4 THEN IIF(mp.cob_id = '403'and Biz_type=2 and PDT.Partner_ID is null,1,0 ) END
#qIsProductAgency";
                    }
                    else
                    {
                        query = @";
IF EXISTS(SELECT * FROM dbo.Mst_MOU WHERE MOU_ID = '{0}' AND GETDATE() BETWEEN Valid_From and Valid_To)
BEGIN
	Select DISTINCT RTRIM(MouID) MouID, RTRIM(mp.Product_Code) ProductCode, Description, 'GARDAOTO' [ProductTypeCode], @0 [InsuranceType], 1 [RowStatus], Ndays, Biz_type, cob_id, IIF(PDT.Partner_ID is null, 0, 1) [IsLexus] 
	From (SELECT  a.Mou_id AS MouID ,
			a.Mou_no AS MouNo ,
			a.Product_Code ,
			a.Description AS Description ,
			b.cob_id,
			a.valid_from AS ValidFrom ,
			a.valid_to AS ValidTo ,
			b.grace_period AS GracePeriod,
			ISNULL(a.branch_id, '') AS BranchID,
			b.Biz_Type AS BizType
			, CASE WHEN b.day_calculation_method =360 THEN 360 ELSE 366 END [Ndays]
			, b.isB2B
			, b.Biz_type
			, product_type
	FROM    mst_mou a WITH ( NOLOCK ) ,
			mst_product b WITH ( NOLOCK )
	WHERE   a.product_code = b.product_code
			) AS mp 
	LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code AND PDT.Partner_ID = 'LEX'
	where GETDATE() BETWEEN ValidFrom and ValidTo AND mp.Product_Code !='' AND mp.isB2B=0
	AND 1 = CASE WHEN @0 = 1 THEN IIF(mp.cob_id = '403' and Biz_type=1 and PDT.Partner_ID is null,1,0)
	WHEN @0 = 2 THEN IIF(mp.cob_id= '404' and PDT.Partner_ID is null,1,0)
	WHEN @0 = 3 THEN IIF(PDT.Partner_ID is not null,1,0) 
	WHEN @0 = 4 THEN IIF(mp.cob_id = '403'and Biz_type=2 and PDT.Partner_ID is null,1,0 ) END
	AND mp.MouID = '{0}'
	#qIsProductAgency
END
ELSE
BEGIN
	select DISTINCT RTRIM(mp.Product_Code) MouID, RTRIM(mp.Product_Code) [ProductCode], mp.Description, 'GARDAOTO' [ProductTypeCode], @0 [InsuranceType], 1 [RowStatus], CASE WHEN mp.day_calculation_method =360 THEN 360 ELSE 366 END [Ndays], Biz_type,mp.cob_id, IIF(PDT.Partner_ID is null, 0, 1) [IsLexus]
	from mst_product mp
	LEFT JOIN dbo.Partner_Document_Template PDT ON PDT.Product_Code = mp.Product_Code AND PDT.Partner_ID = 'LEX'
	where GETDATE() BETWEEN Valid_From and Valid_To AND mp.Product_Code !='' AND mp.isb2b=0
	AND 1 = CASE WHEN @0 = 1 THEN IIF(mp.cob_id = '403' and Biz_type=1 and PDT.Partner_ID is null,1,0)
	WHEN @0 = 2 THEN IIF(mp.cob_id= '404' and PDT.Partner_ID is null,1,0)
	WHEN @0 = 3 THEN IIF(PDT.Partner_ID is not null,1,0) 
	WHEN @0 = 4 THEN IIF(mp.cob_id = '403'and Biz_type=2 and PDT.Partner_ID is null,1,0 ) END
	AND mp.Description LIKE '%{0}%'
	#qIsProductAgency
END";
                        query = string.Format(query, searchParam);
                    }
                    //query = query.Replace("#qIsProductAgency", isAgency ? " AND mp.product_type=6 " : " AND mp.product_type!=6 ");

                    string qWhere = isAgency ? " AND mp.product_type=6 " : " AND mp.product_type!=6 ";

                    if (isMVGodig)
                    {

                        if (isNew && !isAgency)
                        {
                            string pc = string.Concat(
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-NEWVEHICLE-PRODUCTCODE").First(),
                                        ",",
                                        db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-NEWVEHICLE-PRODUCTCODE'")
                                       );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            qWhere = string.Concat(qWhere, " AND  mp.Product_Code IN (" + pctemp + ")");
                        }
                        else if (isNew && isAgency)
                        {
                            string pc = string.Concat(
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-NEWVEHICLE-PRODUCTCODE-AGENCY").First(),
                                        ",",
                                        db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-NEWVEHICLE-PRODUCTCODE-AGENCY'")
                                       );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            qWhere = string.Concat(qWhere, " AND  mp.Product_Code IN (" + pctemp + ")");
                        }
                        else if (!isNew && !isAgency)
                        {
                            string pc = string.Concat(
                                        AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-USEDVEHICLE-PRODUCTCODE").First(),
                                        ",",
                                        db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-USEDVEHICLE-PRODUCTCODE'")
                                       );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            qWhere = string.Concat(qWhere, " AND  mp.Product_Code IN (" + pctemp + ")");
                        }
                        else if (!isNew && isAgency)
                        {
                            string pc = string.Concat(
                                       AppParameterAccess.GetApplicationParametersValue("GODIGITAL-COMPRE-LITE-USEDVEHICLE-PRODUCTCODE-AGENCY").First(),
                                       ",",
                                       db.FirstOrDefault<string>(@"SELECT TOP 1 optionValue 
                                                FROM BEYONDMOSS.Asuransiastra.dbo.Asuransiastraoptions
                                                WHERE OptionName = 'GODIGITAL-USEDVEHICLE-PRODUCTCODE-AGENCY'")
                                      );
                            string pctemp = string.Empty;
                            int i = 0;
                            foreach (var prd in pc.Split(','))
                            {
                                pctemp += (i++ == 0 ? "'" : ",'") + prd + "'";
                            }

                            qWhere = string.Concat(qWhere, " AND  mp.Product_Code IN (" + pctemp + ")");
                        }

                    }

                    query = query.Replace("#qIsProductAgency", qWhere);

                    result = db.Fetch<Models.v0107URF2020.Product>(query, insurancetype, isRenewal);

                }
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }


        public List<CalculatedPremi> CalculateBasicPremi(List<CoverageParam> CoverageList, string ProductCode, string vMouID, string vcitycode, string vusagecode, string vBrand, string vtype, string vModel, string vIsNew,
                                                                   string vsitting, string vyear, DateTime PeriodFrom, DateTime PeriodTo, string Ndays, decimal vTSInterest, decimal vPrimarySI, string vInterestId,
                                                                    string OldPolicyNo, out List<decimal> RenDiscountPct, out bool isProductSupported)
        {
            List<CalculatedPremi> result = new List<CalculatedPremi>();
            List<DetailScoring> dtlScoring = new List<DetailScoring>();
            List<decimal> RDPCT = new List<decimal>();
            bool IPS = true;
            try
            {
                foreach (CoverageParam coverage in CoverageList)
                {

                    #region Get rate scoring
                    DetailScoring ds = new DetailScoring();
                    if (!string.IsNullOrEmpty(vcitycode))
                    {
                        ds.FactorCode = "GEOGRA";
                        ds.InsuranceCode = vcitycode;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vusagecode))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "MVUSAG";
                        ds.InsuranceCode = vusagecode;
                        dtlScoring.Add(ds);
                    }
                    ds = new DetailScoring();
                    ds.FactorCode = "VHCTYP";
                    ds.InsuranceCode = !string.IsNullOrEmpty(vBrand) ? string.IsNullOrEmpty(vtype) ? "ALL   " : vtype : "";
                    dtlScoring.Add(ds);
                    if (!string.IsNullOrEmpty(vBrand))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "VBRAND";
                        ds.InsuranceCode = vBrand;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vIsNew))
                    {

                        ds = new DetailScoring();
                        ds.FactorCode = "NEWVHC";
                        ds.InsuranceCode = vIsNew;
                        dtlScoring.Add(ds);
                    }
                    if (!string.IsNullOrEmpty(vModel))
                    {
                        ds = new DetailScoring();
                        ds.FactorCode = "VMODEL";
                        ds.InsuranceCode = vModel;
                        dtlScoring.Add(ds);
                    }

                    #endregion
                    #region rate calculation
                    int calcmethod = PeriodTo.Date < PeriodFrom.Date.AddMonths(12) ? 1 : 0;// 2 : scale, 1 : prorate
                    ParamCalculatePremiPerCoverage pcppc = new ParamCalculatePremiPerCoverage();
                    pcppc.CalculationMethod = calcmethod;
                    pcppc.PeriodFrom = PeriodFrom;
                    pcppc.PeriodTo = PeriodTo;
                    pcppc.PeriodFromCover = coverage.PeriodFromCover;
                    pcppc.PeriodToCover = coverage.PeriodToCover;
                    pcppc.VehicleType = vtype;
                    pcppc.YearManufacturing = vyear;
                    pcppc.TSICurrID = "IDR";
                    pcppc.NumberOfSeat = vsitting;
                    pcppc.MouID = vMouID;
                    pcppc.ProductCode = ProductCode;
                    pcppc.TSInterest = vTSInterest;
                    pcppc.PrimarySI = vPrimarySI;
                    pcppc.CoverageId = coverage.CoverageId;
                    pcppc.ScoringList = dtlScoring;
                    pcppc.InterestId = vInterestId;
                    pcppc.NDays = Ndays;

                    List<CalculateSumInsured> CalculationResult = pm.CalculateBasicPremium(pcppc);
                    int years = 0;
                    string TempCoverageId = "";
                    foreach (var item in CalculationResult)
                    {
                        years++;
                        if (!string.IsNullOrEmpty(TempCoverageId) && !TempCoverageId.Equals(item.CoverageID))
                        {
                            years = 1;
                        }
                        TempCoverageId = item.CoverageID;
                        var PIC = vWeb2.RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: vInterestId, CoverageID: item.CoverageID);
                        var PD = vWeb2.RetailRepository.GetDeductible(ProductCode, vInterestId, item.CoverageID);
                        string DeductibleCode = string.Empty;
                        string DeductibleDesc = string.Empty;
                        if (PD != null)
                        {
                            DeductibleCode = PD.DeductibleCode;
                            DeductibleDesc = PD.Description;
                        }
                        Models.vWeb2.PremiumScheme PS = new Models.vWeb2.PremiumScheme();
                        #region Discount

                        var Discount = vWeb2.RetailRepository.GetWTDiscount(ProductCode, vMouID);
                        if (!string.IsNullOrWhiteSpace(OldPolicyNo))
                        {
                            var RenDiscount = vWeb2.RetailRepository.GetRenewalDiscount(OldPolicyNo);
                            foreach (WTCommission perc in RenDiscount)
                            {
                                RDPCT.Add(perc.Percentage);
                            }
                            Discount.AddRange(RenDiscount);
                        }
                        List<CommissionScheme> DiscountList = vWeb2.RetailRepository.InitDiscountList(item.CoverageID, Discount, false);
                        if (DiscountList.Count > 0)
                        {
                            DiscountList = vWeb2.RetailRepository.InitFlatDiscount(ProductCode, item.Premium, DiscountList, item.SumInsured, 1, "DISCOUNT", false);
                        }
                        PS = vWeb2.RetailRepository.CalculateDiscountPremium(DiscountList, item.Premium);
                        #endregion
                        // CalculatePremiPerCoverage(item.CoverageID, Convert.ToDouble(item.Premium));

                        if (PS.NetPremium < 0)
                        {
                            IPS = false;
                            isProductSupported = IPS;
                            RenDiscountPct = RDPCT;
                            return result;
                        }

                        if (PS.GrossPremium > 0)
                        {
                            result.Add(new CalculatedPremi
                            {
                                InterestID = vInterestId,
                                CoverageID = item.CoverageID,
                                PeriodFrom = item.CoverFrom,
                                PeriodTo = item.CoverTo,
                                Rate = Convert.ToDouble(item.Rate),
                                ExcessRate = item.ExcessRate,
                                MaxSI = item.MaxSi,
                                Ndays = item.NDays,
                                Net1 = PS.Net1,
                                Net2 = PS.Net2,
                                Net3 = PS.Net3,
                                GrossPremium = PS.GrossPremium,
                                CoverPremium = PS.GrossPremium,
                                Premium = Convert.ToDouble(PS.Net3),
                                DeductibleCode = DeductibleCode,
                                EntryPrecentage = item.EntryPercentage,
                                Loading = Convert.ToDouble(item.Loading * PS.GrossPremium / 100),
                                LoadingRate = Convert.ToDouble(item.Loading),
                                SumInsured = Convert.ToDouble(item.SumInsured),
                                IsBundling = (PS.GrossPremium > 0) ? !string.IsNullOrWhiteSpace(item.AutoApply) ? Convert.ToInt16(item.AutoApply) > 0 ? true : false : false : false,
                                AutoApply = (PS.GrossPremium > 0) ? !string.IsNullOrWhiteSpace(item.AutoApply) ? item.AutoApply.ToString() : string.Empty : string.Empty,
                                CalcMethod = calcmethod,
                                Year = years
                                //LoadingRate 
                            });
                        }
                    }
                    #endregion
                }
                RenDiscountPct = RDPCT;
                isProductSupported = IPS;
                return result;

            }
            catch (Exception e)
            {
                throw (e);
            }
        }
        public List<CalculatedPremi> CalculateACCESS(decimal vPrimarySI, decimal TSIInterest, string ProductCode, string vMouID, DateTime PeriodFrom, DateTime PeriodTo, string InterestID, string vtype, string vyear, string vsitting, string QuotationNo, List<CalculatedPremi> calculatePremiItems, List<DetailScoring> scoringList,
                                                    string OldPolicyNo, out List<decimal> RptDiscount, out List<decimal> DiscountTemp)
        {
            List<decimal> RDP = new List<decimal>();
            List<decimal> DP = new List<decimal>();
            int vResult = vWeb2.RetailRepository.ValidatePerInterest(InterestID, calculatePremiItems, PeriodFrom, PeriodTo, ProductCode);
            List<CalculatedPremi> calculateditems = new List<CalculatedPremi>();
            calculateditems = calculatePremiItems;
            List<CalculatedPremi> AccessResult = new List<CalculatedPremi>();
            if (vResult == 0)
            {
                foreach (CalculatedPremi data in calculateditems)
                {

                    if (data.InterestID.TrimEnd().Equals("CASCO") && (!data.IsBundling))
                    {
                        if (data.Premium > 0)
                        {

                            scoringList.RemoveAll(x => x.FactorCode.Trim().Equals("VHCTYP"));
                            DetailScoring ds = new DetailScoring();
                            ds.FactorCode = "VHCTYP";
                            if (!data.IsBundling && new List<string>() { "SRCC", "FLD", "ETV", "EQK" }.Contains(data.CoverageID))
                            {

                                ds.InsuranceCode = "ALL";
                            }
                            else
                            {

                                ds.InsuranceCode = vtype;
                            }
                            scoringList.Add(ds);
                            ParamCalculatePremiPerCoverage param = new ParamCalculatePremiPerCoverage();
                            param.ProductCode = ProductCode;
                            param.InterestId = InterestID;
                            param.CoverageId = data.CoverageID;
                            param.TSICurrID = "IDR";
                            param.TSInterest = TSIInterest;
                            param.PrimarySI = vPrimarySI;
                            //param.PeriodFrom = Convert.ToDateTime(data.PeriodFrom);
                            //param.PeriodTo = Convert.ToDateTime(data.PeriodTo);
                            //param.PeriodFromCover = Convert.ToDateTime(data.CoverageListPerInterest[i].CoverFromDT);
                            //param.PeriodToCover = Convert.ToDateTime(data.CoverageListPerInterest[i].CoverToDT);
                            param.PeriodFrom = PeriodFrom;
                            param.PeriodTo = PeriodTo;
                            param.PeriodFromCover = data.PeriodFrom;
                            param.PeriodToCover = data.PeriodTo;
                            //param.NDays = Convert.ToString((vPToCover - vPFromCover).Days);
                            param.NDays = data.Ndays.ToString();
                            param.VehicleType = vtype;
                            param.YearManufacturing = vyear;
                            param.AutoApply = data.AutoApply;
                            param.NumberOfSeat = vsitting;
                            param.CalculationMethod = data.CalcMethod;
                            param.ScoringList = scoringList;
                            param.MouID = vMouID;

                            List<CalculateSumInsured> listPremi = new List<CalculateSumInsured>();
                            List<CalculateSumInsured> listPremTemp = new List<CalculateSumInsured>();
                            if (data.CoverageID.Trim().ToUpper() == "ALLRIK" || data.CoverageID.Trim().ToUpper() == "TLO")
                            {
                                listPremTemp = pm.CalculateBasicPremium(param);
                                listPremi.AddRange(listPremTemp);
                                listPremTemp.Clear();
                            }
                            else if (string.IsNullOrWhiteSpace(param.AutoApply) || param.AutoApply == AutoApplyFlag.NonAutoApply
                                || param.AutoApply == AutoApplyFlag.CompreExtendedGroup || param.AutoApply == AutoApplyFlag.TLOExtendedGroup)
                            {
                                listPremTemp = pm.CalculatePremiPerCoverage(param);
                                listPremi.AddRange(listPremTemp);
                                listPremTemp.Clear();

                            }
                            foreach (var item in listPremi)
                            {
                                //For ACCESS is only CoverageId TLO or ALLRIK
                                if (!data.InterestID.TrimEnd().Equals("ACCESS") || (data.InterestID.TrimEnd().Equals("ACCESS") && (item.CoverageID.TrimEnd().Equals("TLO") || item.CoverageID.TrimEnd().Equals("ALLRIK"))))
                                {
                                    var PIC = vWeb2.RetailRepository.GetProductInterestCoverage(ProductCode: param.ProductCode, InterestID: data.InterestID, CoverageID: item.CoverageID);
                                    if (data.InterestID.TrimEnd().ToUpper() == "TPLPER" && item.Rate < 0)
                                    {
                                        var DevPriceCoverProgressive = vWeb2.RetailRepository.GetDefaultPriceCoverProgressive(param.ProductCode, data.InterestID, item.CoverageID);
                                        if (DevPriceCoverProgressive != null)
                                        {
                                            if (DevPriceCoverProgressive.Count > 0)
                                            {
                                                decimal devRate = vWeb2.RetailRepository.GetDefaultRateProgressive(param.ProductCode, data.InterestID, item.CoverageID, item.SumInsured, param.ScoringList);
                                                if (devRate <= 0)
                                                {
                                                    devRate = ((item.Premium / item.SumInsured) / (item.EntryPercentage / 100)) * 100;
                                                    devRate = decimal.Round(devRate, 4);
                                                }
                                                item.Rate = devRate;
                                                //item.ExcessRate = devRate;
                                            }
                                        }
                                    }
                                    var PD = vWeb2.RetailRepository.GetDeductible(ProductCode, data.InterestID, item.CoverageID);
                                    if (PD != null)
                                    {
                                        data.DeductibleCode = PD.DeductibleCode;
                                    }

                                    Models.vWeb2.PremiumScheme PS = new Models.vWeb2.PremiumScheme();
                                    #region Discount
                                    try
                                    {
                                        var Discount = vWeb2.RetailRepository.GetWTDiscount(param.ProductCode, param.MouID);
                                        if (!string.IsNullOrWhiteSpace(OldPolicyNo))
                                        {
                                            var RenDiscount = vWeb2.RetailRepository.GetRenewalDiscount(OldPolicyNo);
                                            var Disc = vWeb2.RetailRepository.GetWTDiscount(ProductCode, vMouID);// New
                                            foreach (WTCommission ren in RenDiscount)
                                            {
                                                if (ren.CommissionID.Contains("RD"))
                                                {
                                                    RDP.Add(ren.Percentage);
                                                }
                                            }
                                            foreach (WTCommission D in Disc)
                                            {
                                                DP.Add(D.Percentage);
                                            }
                                            Discount.AddRange(RenDiscount);
                                        }
                                        List<CommissionScheme> DiscountList = vWeb2.RetailRepository.InitDiscountList(item.CoverageID, Discount, false);
                                        if (DiscountList.Count > 0)
                                        {
                                            DiscountList = vWeb2.RetailRepository.InitFlatDiscount(param.ProductCode, item.Premium, DiscountList, item.SumInsured, 1, "DISCOUNT", false);
                                        }
                                        PS = vWeb2.RetailRepository.CalculateDiscountPremium(DiscountList, item.Premium);
                                    }
                                    catch (Exception er)
                                    {
                                        logger.Error(string.Format("Method {0} Sub Discount Error : {1}", er.ToString()));
                                    }
                                    #endregion
                                    #region Commission
                                    try
                                    {
                                        var CommissionApplied = vWeb2.RetailRepository.GetWTCommission(param.ProductCode, param.MouID);
                                        List<CommissionScheme> CommisionList = vWeb2.RetailRepository.InitCommisionList(item.CoverageID, CommissionApplied, true);
                                        List<CommissionScheme> CommissionList2 = vWeb2.RetailRepository.InitFlatDiscount(param.ProductCode, item.Premium, CommisionList, item.SumInsured, 1, "COMMISSION", true);
                                    }
                                    catch (Exception er)
                                    {
                                        logger.Error(string.Format("Method {0} Sub Discount Error : {1}", er.ToString()));
                                    }
                                    #endregion
                                    if (item.Premium > 0)
                                    {
                                        int AutoApply = Convert.ToInt16(!string.IsNullOrWhiteSpace(item.AutoApply) ? item.AutoApply.ToString() : data.AutoApply);

                                        AccessResult.Add(new CalculatedPremi
                                        {
                                            InterestID = InterestID,
                                            CoverageID = item.CoverageID,
                                            //CoverageDesc = !string.IsNullOrWhiteSpace(item.Description) ? item.Description : PIC[0].Description,
                                            //PeriodFrom = item.CoverFrom.ToString(),
                                            //PeriodTo = item.CoverTo.ToString(),
                                            PeriodFrom = item.CoverFrom,
                                            PeriodTo = item.CoverTo,
                                            DeductibleCode = data.DeductibleCode,
                                            //DeductibleDesc = string.IsNullOrWhiteSpace(data.CoverageListPerInterest[i].DeductibleCode) ? string.Empty : string.Format("{0} - {1}", data.CoverageListPerInterest[i].DeductibleCode, data.CoverageListPerInterest[i].DeductibleDesc),
                                            //Currency = data.CoverageListPerInterest[i].CurrencyID,
                                            //AgreedValue = item.AgreedValue,
                                            //Interest = PIC[0].InterestDescription,
                                            Premium = Convert.ToDouble(PS.Net3),
                                            //Premium = PS.NetPremium,
                                            Rate = Convert.ToDouble(item.Rate),
                                            //CoverageType = !string.IsNullOrWhiteSpace(item.CoverageType) ? item.CoverageType : data.CoverageListPerInterest[i].CoverType,
                                            //CoverType = !string.IsNullOrWhiteSpace(item.CoverageType) ? item.CoverageType : data.CoverageListPerInterest[i].CoverType,
                                            //BasicCoverageID = !string.IsNullOrWhiteSpace(item.BasicCoverageID) ? item.BasicCoverageID : data.CoverageListPerInterest[i].BasicCoverageID,
                                            Net1 = PS.Net1,
                                            Net2 = PS.Net2,
                                            Net3 = PS.Net3,
                                            GrossPremium = PS.GrossPremium,
                                            CoverPremium = PS.GrossPremium,
                                            ExcessRate = item.ExcessRate,
                                            MaxSI = item.MaxSi,
                                            Ndays = item.NDays,
                                            EntryPrecentage = item.EntryPercentage,
                                            LoadingRate = Convert.ToDouble(item.Loading),
                                            Loading = Convert.ToDouble(item.Loading * PS.Net3 / 100),
                                            SumInsured = Convert.ToDouble(item.SumInsured),
                                            CalcMethod = data.CalcMethod,
                                            AutoApply = (PS.GrossPremium > 0) ? AutoApply.ToString() : "0",
                                            IsBundling = AutoApply > 0 ? true : false
                                        });
                                    }
                                }

                            }
                        }
                    }
                }
            }
            DiscountTemp = DP;
            RptDiscount = RDP;
            return AccessResult;
        }

        public List<CalculatedPremi> calculatepremicalculation(string ProductCode, string vInterestId, CoverageParam cp, DateTime PeriodFrom, DateTime PeriodTo, string vtype, string vyear
                                                      , string vsitting, List<DetailScoring> dtlScoring, string Ndays, decimal vTSInterest, string vMouID, string OldPolicyNo, List<CalculatedPremi> CalculatedPremiItems
                                                       , int calcmethod, out List<decimal> RenDiscountPct, out List<decimal> DiscountPct)
        {
            List<decimal> RDP = new List<decimal>();
            List<decimal> DP = new List<decimal>();
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                ParamCalculatePremiPerCoverage pcppc = new ParamCalculatePremiPerCoverage();// 2 : scale, 1 : prorate
                pcppc.CalculationMethod = calcmethod;
                pcppc.PeriodFrom = PeriodFrom;
                pcppc.PeriodTo = PeriodTo;
                pcppc.PeriodFromCover = cp.PeriodFromCover;
                pcppc.PeriodToCover = cp.PeriodToCover;
                pcppc.VehicleType = vtype;
                pcppc.YearManufacturing = vyear;
                pcppc.TSICurrID = "IDR";
                pcppc.NumberOfSeat = vsitting;
                pcppc.MouID = vMouID;
                pcppc.ProductCode = ProductCode;
                pcppc.TSInterest = vTSInterest;
                //pcppc.PrimarySI = vPrimarySI;
                pcppc.CoverageId = cp.CoverageId;
                pcppc.ScoringList = dtlScoring;
                pcppc.InterestId = vInterestId;
                pcppc.NDays = Ndays;


                //#Sprint5 BEGIN                
                var PRDType = vWeb2.RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: vInterestId, CoverageID: cp.CoverageId);
                string ConfigName = "EXTENDED_COVER_GROUP_COMPRE";
                string AutoApplyExtGroupFlag = AutoApplyFlag.CompreExtendedGroup;
                if (PRDType.Count != 0)
                {
                    if (PRDType[0].TLOFlag != null)
                    {
                        if (PRDType[0].TLOFlag)
                        {
                            ConfigName = "EXTENDED_COVER_GROUP_TLO";
                            AutoApplyExtGroupFlag = AutoApplyFlag.TLOExtendedGroup;
                        }
                    }
                }
                List<string> CoverGrp = vWeb2.RetailRepository.GetGeneralConfig(ConfigName, "PRODUCT", ProductCode, cp.CoverageId);

                if (CoverGrp.Count == 0) CoverGrp.Add(cp.CoverageId);
                // string vCoverageIdPADRVR = db.ExecuteScalar<string>("SELECT TOP 1 Coverage_Id FROM Prd_Interest_Coverage WHERE Product_Code=@0 AND Interest_Id=@1", ProductCode, CoverageTypeNonBasic.PADRVR.ToString());
                // bool isAutoApplyPADRVR = false;
                //   if (!string.IsNullOrEmpty(cp.CoverageIdPADRVR) && !CoverGrp.Contains(cp.CoverageIdPADRVR) && vInterestId.Equals(CoverageTypeNonBasic.PAPASS.ToString(), StringComparison.OrdinalIgnoreCase) && !CalculatedPremiItems.Select(x => x.InterestID.Trim()).Contains(CoverageTypeNonBasic.PADRVR.ToString()))
                //       CoverGrp.Add(cp.CoverageIdPADRVR);
                //       isAutoApplyPADRVR = true;
                for (int i = 0; i < CoverGrp.Count; i++)
                {
                    pcppc.CoverageId = cp.CoverageId = CoverGrp[i];
                    //if (i == CoverGrp.Count-1 && isAutoApplyPADRVR && cp.CoverageId.Equals(vCoverageIdPADRVR))
                    //{
                    //    vInterestId = CoverageTypeNonBasic.PADRVR.ToString();
                    //    pcppc.InterestId = vInterestId;
                    //}
                    pcppc.YearManufacturing = string.IsNullOrEmpty(pcppc.YearManufacturing) ? "0" : pcppc.YearManufacturing;
                    var CalculationResult = pm.CalculatePremiPerCoverage(pcppc);

                    //#Sprint5 END
                    var PIC = vWeb2.RetailRepository.GetProductInterestCoverage(ProductCode: ProductCode, InterestID: vInterestId, CoverageID: cp.CoverageId);
                    var PD = vWeb2.RetailRepository.GetDeductible(ProductCode, vInterestId, cp.CoverageId);
                    string DeductibleCode = string.Empty;
                    string DeductibleDesc = string.Empty;
                    if (PD != null)
                    {
                        DeductibleCode = PD.DeductibleCode;
                        DeductibleDesc = PD.Description;
                    }
                    if (CalculationResult != null)
                    {

                        int years = 0;
                        string TempCoverageId = "";
                        foreach (var item in CalculationResult)
                        {
                            years++;
                            if (!string.IsNullOrEmpty(TempCoverageId) && !TempCoverageId.Equals(item.CoverageID))
                            {
                                years = 1;
                            }
                            TempCoverageId = item.CoverageID;

                            Models.vWeb2.PremiumScheme PS = new Models.vWeb2.PremiumScheme();
                            var Discount = vWeb2.RetailRepository.GetWTDiscount(ProductCode, vMouID);// New
                            if (!string.IsNullOrWhiteSpace(OldPolicyNo))
                            {
                                var RenDiscount = vWeb2.RetailRepository.GetRenewalDiscount(OldPolicyNo);
                                var Disc = vWeb2.RetailRepository.GetWTDiscount(ProductCode, vMouID);// New
                                foreach (WTCommission ren in RenDiscount)
                                {
                                    if (ren.CommissionID.Contains("RD"))
                                    {
                                        RDP.Add(ren.Percentage);
                                    }
                                }
                                foreach (WTCommission D in Disc)
                                {
                                    DP.Add(D.Percentage);
                                }
                                Discount.AddRange(RenDiscount);
                            }
                            List<CommissionScheme> DiscountList = vWeb2.RetailRepository.InitDiscountList(item.CoverageID, Discount, false);
                            if (DiscountList.Count > 0)
                            {
                                DiscountList = vWeb2.RetailRepository.InitFlatDiscount(ProductCode, item.Premium, DiscountList, item.SumInsured, 1, "DISCOUNT", false);
                            }
                            PS = vWeb2.RetailRepository.CalculateDiscountPremium(DiscountList, item.Premium);
                            #region remove same items
                            CalculatedPremiItems.RemoveAll(X => X.CoverageID.TrimEnd().Equals(item.CoverageID.TrimEnd()) && X.InterestID.TrimEnd().Equals(vInterestId) && X.Year == item.YearNumber);
                            #endregion

                            int AutoApply = string.IsNullOrEmpty(item.AutoApply) ? 0 : Convert.ToInt16(CoverGrp.Count > 1 ? AutoApplyExtGroupFlag : (!string.IsNullOrWhiteSpace(item.AutoApply) ? item.AutoApply.ToString() : string.Empty));
                            if (((PS.GrossPremium > 0) || (PS.GrossPremium == 0 && item.Rate == -1)) && !string.IsNullOrEmpty(DeductibleCode))
                            {
                                CalculatedPremiItems.Add(new CalculatedPremi
                                {
                                    InterestID = vInterestId,
                                    CoverageID = cp.CoverageId,
                                    //CoverageDesc = !string.IsNullOrWhiteSpace(item.Description) ? item.Description : PIC[0].Description,
                                    //PeriodFrom = item.CoverFrom.ToString(),
                                    //PeriodTo = item.CoverTo.ToString(),
                                    PeriodFrom = item.CoverFrom,
                                    PeriodTo = item.CoverTo,
                                    DeductibleCode = DeductibleCode,
                                    //DeductibleDesc = DeductibleDesc,
                                    //Currency = PIC[0].DefaultCurrency,
                                    //AgreedValue = item.AgreedValue,
                                    //Interest = PIC[0].InterestDescription,
                                    //Premium = item.Premium,
                                    Premium = Convert.ToDouble(PS.Net3),
                                    Rate = Convert.ToDouble(item.Rate),
                                    //CoverageType = !string.IsNullOrWhiteSpace(item.CoverageType) ? item.CoverageType : string.Empty,
                                    //BasicCoverageID = PIC[0].ComprehensiveFlag == true ? "ALLRIK" : "TLO",
                                    AutoApply = (PS.GrossPremium > 0) ? AutoApply.ToString() : "0",
                                    IsBundling = AutoApply > 0 ? true : false,
                                    Net1 = PS.Net1,
                                    Net2 = PS.Net2,
                                    Net3 = PS.Net3,
                                    GrossPremium = PS.GrossPremium,
                                    CoverPremium = PS.GrossPremium,
                                    ExcessRate = item.ExcessRate,
                                    MaxSI = item.MaxSi,
                                    Ndays = item.NDays,
                                    EntryPrecentage = item.EntryPercentage,
                                    LoadingRate = Convert.ToDouble(item.Loading),
                                    Loading = Convert.ToDouble(item.Loading * PS.Net3 / 100),
                                    SumInsured = Convert.ToDouble(item.SumInsured),
                                    CalcMethod = calcmethod,
                                    Year = years

                                });
                            }
                        }
                    }
                }
                DiscountPct = DP;
                RenDiscountPct = RDP;
                return CalculatedPremiItems;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public List<CalculatedPremi> RecalculateAccess(decimal vTSIInterest, string vCoverageId, List<CalculatedPremi> calpremiitems, string ProductCode, string MouID, DateTime PeriodFrom, DateTime PeriodTo,
                                                        string vtype, string vyear, string vsitting, string pQuotationNo, List<DetailScoring> dtlScoring, string OldPolicyNo, out List<decimal> RenDiscountPct, out List<decimal> DiscountPct)
        {
            try
            {
                List<decimal> RPT = new List<decimal>();
                List<decimal> DT = new List<decimal>();
                bool isaccessapplied = false;
                double suminsuredaccess = 0;
                foreach (CalculatedPremi item in calpremiitems)
                {
                    if (item.InterestID.Equals("ACCESS") && (item.CoverageID.Trim().Equals("ALLRIK") || item.CoverageID.Trim().Equals("TLO")))
                    {
                        isaccessapplied = true;
                        suminsuredaccess = item.SumInsured;
                        break;
                    }
                }
                if (isaccessapplied)
                {
                    //GET ACCESS ALLRIK AND TLO
                    List<CalculatedPremi> cascoaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(JsonConvert.SerializeObject(calpremiitems));
                    cascoaccess.RemoveAll(x => !x.InterestID.Trim().Equals("ACCESS") && (!x.CoverageID.Trim().Equals("ALLRIK") || !x.CoverageID.Trim().Equals("TLO")));
                    //
                    // GET NON ACCESS ITEM
                    calpremiitems.RemoveAll(x => x.InterestID.Trim().Equals("ACCESS"));
                    //
                    List<CalculatedPremi> calculateaccess = JsonConvert.DeserializeObject<List<CalculatedPremi>>(JsonConvert.SerializeObject(calpremiitems));
                    List<CalculatedPremi> cItems = CalculateACCESS(vTSIInterest, Convert.ToDecimal(suminsuredaccess), ProductCode, MouID, PeriodFrom, PeriodTo, "ACCESS", vtype, vyear, vsitting, pQuotationNo, calculateaccess, dtlScoring, OldPolicyNo, out RenDiscountPct, out DiscountPct);
                    if (new List<string>() { "SRCC", "FLD", "ETV", "EQK" }.Contains(vCoverageId))
                    {
                        cItems.RemoveAll(x => x.InterestID.Equals("ACCESS") && x.IsBundling);
                    }
                    else
                    {
                        cItems.RemoveAll(x => x.InterestID.Equals("ACCESS") && (!x.CoverageID.Trim().Equals("TRS") && !x.CoverageID.Trim().Equals("TRRTLO")));
                    }
                    //REMOVE ACCESS ALLRIK OR TLO RESULT
                    cItems.RemoveAll(x => x.InterestID.Equals("ACCESS") && (x.CoverageID.Trim().Equals("ALLRIK") || x.CoverageID.Trim().Equals("TLO")));

                    //JOIN ACCESS
                    cascoaccess.AddRange(cItems);
                    // JOIN ALL
                    calpremiitems.AddRange(cascoaccess);
                    return calpremiitems;
                }
                else
                {
                    RenDiscountPct = RPT;
                    DiscountPct = DT;
                    return calpremiitems;
                }

            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public DetailCoverageSummary GetDetailCoverageSummary(string ProductCode, string MouID, List<DetailScoring> dtlScoring, List<CalculatedPremi> CalculatedPremiItems, double AdminFee, string vyear, string UsageCode, int Ndays
                                                       , DateTime? PeriodFrom, DateTime? PeriodTo, string vtype, string vsitting, string OldPolicyNo, out double DiscAmount)
        {
            double PADRVRSI = 0;
            double PAPASSSI = 0;
            double TPLSI = 0;
            double basiccoverpremi = 0;
            double TSPremi = 0;
            bool islexus = false;
            bool issrccts = false;
            DetailCoverageSummary result = new DetailCoverageSummary();
            try
            {
                #region get basic detail cover
                List<DetailCoverage> dtlcoverage = new List<DetailCoverage>();
                List<DateTime> period = new List<DateTime>();
                List<CalculatedPremi> extendedPremiItems = new List<CalculatedPremi>();
                //var DiscountList = vWeb2.RetailRepository.GetWTDiscount(ProductCode, MouID);
                //double Discount = Convert.ToDouble(DiscountList.Count() > 0 ? DiscountList[0].Percentage : 0);
                double Discount = 0;
                double grossPremium = 0;
                double netPremium = 0;
                foreach (CalculatedPremi item in CalculatedPremiItems)
                {
                    if (item.InterestID.TrimEnd().Equals("CASCO") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                    {
                        period.Add(item.PeriodFrom);
                    }
                    double gp = Convert.ToDouble(item.GrossPremium);
                    double np = Convert.ToDouble(item.Net3);
                    grossPremium += gp;
                    netPremium += np;
                    Discount += gp - np;
                }
                int year = 1;
                DateTime PeriodToPADRVR, PeriodToTS, PeriodToPAPASS, PeriodToTPL = Convert.ToDateTime("1900-01-01");
                foreach (DateTime pitem in period)
                {
                    double sumInsured = 0;
                    string basiccover = StatusCoverage.NoCover;
                    DetailCoverage dtl = new DetailCoverage();
                    foreach (CalculatedPremi item in CalculatedPremiItems)
                    {
                        if (pitem.Year.Equals(item.PeriodFrom.Year))
                        {
                            if (item.InterestID.TrimEnd().Equals("CASCO") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                            {
                                dtl.VehiclePremi = item.SumInsured;
                                dtl.LoadingPremi += item.LoadingRate;
                                dtl.Rate = dtl.Rate + item.Rate;
                                sumInsured = item.SumInsured;
                                dtl.PeriodFrom = item.PeriodFrom;
                                dtl.PeriodTo = item.PeriodTo;
                                dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                dtl.BasiCoverName = item.CoverageID.TrimEnd().Equals("ALLRIK") ? "Comprehensive" : item.CoverageID.TrimEnd().Equals("TLO") ? "Total Loss Only" : "";

                            }
                            else if (item.InterestID.TrimEnd().Equals("ACCESS") && (item.CoverageID.TrimEnd().Equals("ALLRIK") || item.CoverageID.TrimEnd().Equals("TLO")))
                            {
                                dtl.AccessPremi = item.SumInsured;
                                dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                            }
                            else if ((item.CoverageID.TrimEnd().Contains("SRC") || item.CoverageID.TrimEnd().Contains("FLD") || item.CoverageID.TrimEnd().Contains("ETV") || item.CoverageID.TrimEnd().Equals("EQK")))
                            {
                                if (item.IsBundling)
                                {
                                    basiccover = StatusCoverage.Include;
                                    dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                    if (item.InterestID.Trim().Equals("CASCO"))
                                    {
                                        dtl.Rate = dtl.Rate + item.Rate;
                                    }
                                    dtl.LoadingPremi += item.LoadingRate;
                                    dtl.IsBundling = Convert.ToInt16(item.IsBundling);
                                }
                                else
                                {
                                    basiccoverpremi += Convert.ToDouble(item.GrossPremium);
                                    basiccover = Convert.ToString(basiccoverpremi);
                                    dtl.ExtendedSFEPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.SFERate += item.Rate;
                                }
                                dtl.SFEPremi += Convert.ToDouble(item.GrossPremium);
                                if ((item.CoverageID.TrimEnd().Contains("SRCCTS")))
                                {
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.TSCoverage = Convert.ToString(Convert.ToDecimal(dtl.TSPremi));
                                    issrccts = true;
                                    if (dtl.TSPremi == 0)
                                    {
                                        dtl.TSCoverage = StatusCoverage.NoCover;
                                    }
                                }
                            }
                            else if ((item.CoverageID.TrimEnd().Contains("TRS") || item.CoverageID.TrimEnd().Contains("TRRTLO")))
                            {

                                TSPremi += Convert.ToDouble(item.GrossPremium);
                                dtl.TSRate = item.Rate;
                                if (item.IsBundling)
                                {

                                    dtl.LoadingPremi += item.LoadingRate;
                                    dtl.IsBundlingTRS = Convert.ToInt16(item.IsBundling);
                                    dtl.TSCoverage = StatusCoverage.Include;
                                    if (item.InterestID.Trim().Equals("CASCO"))
                                    {
                                        dtl.Rate = dtl.Rate + item.Rate;
                                    }
                                    dtl.BasicPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.LoadingPremi = dtl.LoadingPremi + item.Loading;
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                }
                                else
                                {
                                    dtl.TSPremi += Convert.ToDouble(item.GrossPremium);
                                    dtl.TSCoverage = Convert.ToString(Convert.ToDecimal(dtl.TSPremi));
                                    if (dtl.TSPremi == 0)
                                    {
                                        dtl.TSCoverage = StatusCoverage.NoCover;
                                    }
                                }
                            }
                            else
                            {
                                extendedPremiItems.Add(item);
                            }
                        }
                    }
                    if (dtl.TSPremi == 0)
                    {
                        dtl.TSCoverage = StatusCoverage.NoCover;
                    }
                    double sias = sumInsured + dtl.AccessPremi;
                    // dtl.BasicPremi = (sias * dtl.Rate/100) + (sias * dtl.Loading);
                    dtl.VehicleAccessPremi = dtl.VehiclePremi + dtl.AccessPremi;
                    dtl.BasiCoverage = basiccover;
                    dtl.Year = year++;
                    dtlcoverage.Add(dtl);
                }
                #endregion

                #region get extended detail cover

                foreach (CalculatedPremi item in extendedPremiItems)
                {
                    if (item.InterestID.Trim().Equals("PADRVR"))
                    {

                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.PADRVRCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, MouID, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod, false);
                                dtl.PADRVRRate = item.Rate;
                            }
                        }
                        PADRVRSI = item.SumInsured;

                    }
                    if (item.InterestID.Trim().Equals("PADDR1"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            if (dtl.PeriodFrom <= item.PeriodFrom && dtl.PeriodTo >= item.PeriodTo)
                            {
                                dtl.PADRVRCoverage = Convert.ToDouble(item.GrossPremium);
                                dtl.PADRVRRate = item.Rate;
                            }
                        }
                        islexus = true;
                        PADRVRSI = PADRVRSI == 0 ? item.SumInsured : PADRVRSI;
                    }
                    if (item.InterestID.Trim().Equals("PAPASS"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.PAPASSCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, MouID, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod, false);
                                dtl.PAPASSRate = item.Rate;
                            }
                        }

                        PAPASSSI = item.SumInsured;

                    }
                    if (item.InterestID.Trim().Equals("PA24AV"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            if (dtl.PeriodFrom <= item.PeriodFrom && dtl.PeriodTo >= item.PeriodTo)
                            {
                                dtl.PAPASSCoverage = Convert.ToDouble(item.GrossPremium);
                                dtl.PAPASSRate = item.Rate;
                            }
                        }
                        islexus = true;
                        PAPASSSI = PAPASSSI == 0 ? item.SumInsured : PAPASSSI;
                    }
                    if (item.InterestID.Trim().Equals("TPLPER"))
                    {
                        foreach (DetailCoverage dtl in dtlcoverage)
                        {
                            CoverageParam cp = new CoverageParam();
                            cp.CoverageId = item.CoverageID;
                            cp.PeriodFromCover = item.PeriodFrom.AddYears(dtl.Year - 1);
                            cp.PeriodToCover = dtl.PeriodTo.Year == item.PeriodTo.Year ? item.PeriodTo : dtl.PeriodTo;
                            if (cp.PeriodToCover.Year >= cp.PeriodFromCover.Year && dtl.PeriodTo.Year <= item.PeriodTo.Year)
                            {
                                int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                                dtl.TPLCoverage = RecalculateExtendedCover(item.InterestID, cp, PeriodFrom, PeriodTo, ProductCode, MouID, dtlScoring, vyear, UsageCode, Ndays
                                                                               , vtype, vsitting, Convert.ToDecimal(item.SumInsured), OldPolicyNo, calcmethod, false);
                                dtl.TPLRate = item.Rate;
                            }
                        }

                        TPLSI = item.SumInsured;
                    }
                }
                #endregion
                double PDP = 0;
                foreach (DetailCoverage dtl in dtlcoverage)
                {
                    double PADRVRCoverage = dtl.PADRVRCoverage;
                    double PAPASSCoverage = dtl.PAPASSCoverage;
                    dtl.PremiPerluasan = (islexus ? 0 : (issrccts ? 0 : dtl.TSPremi)) + dtl.TPLCoverage + PADRVRCoverage + PAPASSCoverage + dtl.ExtendedSFEPremi;
                    dtl.PremiDasarPerluasan = dtl.PremiPerluasan + dtl.BasicPremi;
                    double TotalPremi = PDP + dtl.PremiDasarPerluasan;
                    //double diskonpremi = TotalPremi * Discount / 100;
                    dtl.TotalPremi = TotalPremi - Discount + AdminFee;
                    PDP += dtl.PremiDasarPerluasan;
                }
                result.AdminFee = AdminFee;
                result.dtlCoverage = dtlcoverage;
                result.PADRVRSI = PADRVRSI;
                result.PAPASSSI = PAPASSSI;
                result.TPLSI = TPLSI;
                DiscAmount = Discount;
                return result;
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public double RecalculateExtendedCover(string interestid, CoverageParam cp, DateTime? PeriodFrom, DateTime? PeriodTo, string ProductCode, string MouID, List<DetailScoring> dtlScoring, string vyear, string UsageCode, int Ndays
                                               , string vtype, string vsitting, decimal vTSInterest, string OldPolicyNo, int calcmethod, bool isrenew)
        {
            try
            {
                List<CalculatedPremi> result = new List<CalculatedPremi>();
                DateTime pf = PeriodFrom ?? DateTime.Now;
                DateTime pt = PeriodTo ?? DateTime.Now;
                TimeSpan ts = new TimeSpan(0, 0, 0);
                pf = pf.Date + ts;
                pt = pt.Date + ts;
                cp.PeriodFromCover = cp.PeriodFromCover.Date + ts;
                cp.PeriodToCover = cp.PeriodToCover.Date + ts;
                List<decimal> RenDiscountPct = new List<decimal>();
                List<decimal> DiscountPct = new List<decimal>();
                result = calculatepremicalculation(ProductCode, interestid, cp, pf, pt, vtype, vyear, vsitting, dtlScoring, Ndays.ToString(), vTSInterest, MouID, OldPolicyNo, result, calcmethod, out RenDiscountPct, out DiscountPct);
                if (result.Count > 0)
                {
                    return Convert.ToDouble(result[0].GrossPremium);
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public Otosales.Models.vWeb2.TaskListDetail GetTaksListDetail(string CustID, string FollowUpNo)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            //List<dynamic> listDyn = new List<dynamic>();
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            Otosales.Models.vWeb2.TaskListDetail result = new Otosales.Models.vWeb2.TaskListDetail();
            string query = "";

            try
            {
                Thread threadPremi = new Thread(() =>
                {
                    //Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);
                    try
                    {
                        string queryFieldPremi = @"SELECT 0 GrossPremium,	0 Admin, 0 NetPremi, PolicyNo, OrderNo, PolicyOrderNo, SurveyNo, OldPolicyNo, 0 NoClaimBonus, NULL OldPolicyPeriodTo 
FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND ApplyF = 1 AND RowStatus = 1";

                        var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                        var dbFieldPremiAABdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                        Otosales.Models.vWeb2.FieldPremi premi = dbFieldPremiMobile.Query<Otosales.Models.vWeb2.FieldPremi>(queryFieldPremi, CustID, FollowUpNo).FirstOrDefault();
                        if (premi != null)
                        {
                            if (!string.IsNullOrEmpty(premi.OldPolicyNo))
                            {
                                List<dynamic> ls = new List<dynamic>();
                                queryFieldPremi = @";SELECT Period_To FROM dbo.Policy WHERE Policy_No = @0";

                                ls = dbFieldPremiAABdb.Fetch<dynamic>(queryFieldPremi, premi.OldPolicyNo);
                                if (ls.Count > 0)
                                {
                                    premi.OldPolicyPeriodTo = ls.First().Period_To;
                                }
                            }
                            result.FieldPremi = premi;
                            if (!string.IsNullOrEmpty(result.FieldPremi.PolicyOrderNo))
                            {
                                queryFieldPremi = @";SELECT STUFF(( SELECT ', ' + CONCAT(Name,' ',Description,' ',Quantity,' No Cover')
                                    FROM dbo.Ord_Dtl_NoCover 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '') AS NoCover,
                        STUFF(( SELECT ', ' + [Description]
                                    FROM dbo.Ord_Object_Clause 
			                        WHERE Order_No = @0
								    AND Clause_Id = 'OMV99'
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS OriginalDefect,
                        STUFF(( SELECT ', ' + Item_Name
                                    FROM dbo.Ord_Dtl_Interest_Item 
			                        WHERE Order_No = @0
                                    FOR XML PATH('')
                                    ), 1, 1, '')  AS Accessories,
                        ms.[Description] AS SurveyStatus,
                        s.ResultRecommendation AS SurveyorRecomendation,
                        s.remarks AS Remaks,
                        mu.[User_Name] AS Surveyor
                        FROM dbo.Survey s 
						INNER JOIN dbo.Lnk_Survey_Order lso
						ON s.Survey_No = lso.Survey_No
                        INNER JOIN dbo.Mst_Order mo 
                        ON mo.Order_No = s.Reference_No
                        INNER JOIN dbo.Mst_State ms 
                        ON ms.[Type] = 'survey' 
                        AND ms.[State] = s.[Status]
                        INNER JOIN dbo.Mst_User mu
                        ON mu.[User_Id] = s.Surveyor
                        WHERE mo.Order_No = @0";
                                Otosales.Models.vWeb2.SurveyData sd = dbFieldPremiAABdb.Query<Otosales.Models.vWeb2.SurveyData>(queryFieldPremi, result.FieldPremi.PolicyOrderNo).FirstOrDefault();
                                result.SurveyData = sd;
                            }
                        }

                        string queryFollowUpInfo = @";SELECT fu.FollowUpStatus,fu.FollowUpInfo,fu.IsRenewal,so.Name as Agency,'' as Upliner, fu.SalesOfficerID, so.Name
                        FROM dbo.FollowUp fu  
                        INNER JOIN dbo.SalesOfficer so 
                        ON so.SalesOfficerId=fu.SalesOfficerId
                        WHERE fu.FollowUpNo = @0";
                        List<dynamic> listDynFollowUpInfo = dbFieldPremiMobile.Fetch<dynamic>(queryFollowUpInfo, FollowUpNo);
                        if (listDynFollowUpInfo.Count > 0)
                        {
                            foreach (dynamic item in listDynFollowUpInfo)
                            {
                                bool isAgency = false;
                                string email = dbFieldPremiMobile.ExecuteScalar<string>(@";
DECLARE @@Email VARCHAR(512) = ''
SELECT @@Email=Email FROM dbo.SalesOfficer WHERE SalesOfficerID = @0 AND RowStatus = 1
SELECT @@Email", item.SalesOfficerID);
                                if (!string.IsNullOrEmpty(email))
                                {
                                    isAgency = dbFieldPremiAABdb.ExecuteScalar<bool>(@";
DECLARE @@Count INT = 0
SELECT @@Count = COUNT(1) 
FROM a2isAuthorizationDB.General.ExternalUsers 
WHERE UserID = @0 AND RowStatus = 0
IF(@@Count>0)
BEGIN SELECT CAST(1 AS BIT) END
ELSE 
BEGIN SELECT CAST(0 AS BIT) END", email);
                                }

                                item.Agency = isAgency ? item.Agency : "";
                                item.Upliner = isAgency ? dbFieldPremiAABdb.ExecuteScalar<string>(@";SELECT TOP 1 RTRIM(d.Name) AS Upliner         
                                      FROM      dtl_cust_type a WITH ( NOLOCK )
                                                INNER JOIN mst_customer b WITH ( NOLOCK ) ON a.cust_id = b.cust_id                    
                                LEFT JOIN dtl_cust_type c WITH ( NOLOCK ) ON a.Upliner_Client_Code=c.Client_Code
                                                LEFT JOIN mst_customer d WITH ( NOLOCK ) ON c.Cust_Id = d.cust_id                    
                                LEFT JOIN dtl_cust_type e WITH ( NOLOCK ) ON a.Leader_Client_Code=e.Client_Code
                                                LEFT JOIN mst_customer f WITH ( NOLOCK ) ON e.Cust_Id = f.cust_id                    
                                      WHERE     a.Client_Type = 'AGENT' AND b.Cust_Id=@0 OR a.Client_Code=@0", item.SalesOfficerID) : "";
                            }
                            result.FollowUpInfo = listDynFollowUpInfo;
                        }
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadPersonalData = new Thread(() =>
                {
                    try
                    {
                        string queryPersonalData = @";SELECT p.Name,p.CustBirthDay,p.CustGender,p.CustAddress,p.PostalCode,
                        p.Email1,p.Phone1,p.IdentityNo, CAST(COALESCE(p.isCompany,0) AS BIT) isCompany, 
                        CAST(0 AS BIT) isNeedDocRep, 0 AmountRep, IsPayerCompany
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f
                        ON f.CustID = p.CustID
                        WHERE p.CustID = @0 AND p.RowStatus = 1";

                        var dbFieldPremiMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        result.PersonalData = dbFieldPremiMobile.Query<Otosales.Models.vWeb2.PersonalData>(queryPersonalData, CustID).FirstOrDefault();
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadCompanyData = new Thread(() =>
                {
                    try
                    {
                        string queryCompanyData = @";SELECT CompanyName,NPWPno,NPWPdate,NPWPaddress NPWPaddres,OfficeAddress
                            ,PostalCode,PICPhoneNo,PICname,Email PICEmail FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1";

                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        result.CompanyData = dbMobile.Query<Otosales.Models.vWeb2.CompanyData>(queryCompanyData, CustID, FollowUpNo).FirstOrDefault();
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadVehicleData = new Thread(() =>
                {
                    try
                    {
                        Otosales.Models.vWeb2.VehicleData vData = new Models.vWeb2.VehicleData();
                        Thread threadVehicleData1 = new Thread(() =>
                        {
                            try
                            {
                                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                                string queryVehicleData = @";SELECT c.Description + ' ' + d.Description + ' ' + omv.Year + ' ' + a.Series  AS Vehicle,
                        omv.VehicleCode, omv.BrandCode, os.ProductTypeCode, omv.ModelCode,  omv.Series, 
                        omv.Type, CAST(omv.Sitting AS DECIMAL) Sitting, omv.Year, omv.CityCode, CAST(omv.SumInsured AS DECIMAL) SumInsured, os.InsuranceType,
                        os.VANumber, p.DealerCode, p.SalesDealer, omv.UsageCode, omv.ColorOnBPKB, 
                        omv.RegistrationNumber, omv.EngineNumber, omv.ChasisNumber, omv.IsNew, os.OrderNo,
						os.PolicyNo, os.ProductCode, os.SegmentCode, os.PeriodFrom, os.PeriodTo,
						os.IsORDefectsRepair, IsAccessoriesChange, IsPolicyIssuedBeforePaying, 
                        os.Remarks, 0 BasicCoverID, '' ORDefectsDesc, '' NoCover, RTRIM(os.MouID) MouID
                        FROM Vehicle a 
                        LEFT JOIN VehicleBrand c ON c.BrandCode = a.BrandCode 
                        AND c.ProductTypeCode = a.ProductTypeCode 
                        LEFT JOIN VehicleModel d ON d.BrandCode = a.BrandCode 
                        AND d.ProductTypeCode = a.ProductTypeCode AND d.ModelCode = a.ModelCode
                        LEFT JOIN Vehicle b ON a.VehicleCode=b.VehicleCode AND 
                        a.BrandCode=b.BrandCode
                        AND a.ProductTypeCode=b.ProductTypeCode AND a.ModelCode=b.ModelCode 
                        AND a.Series=b.Series AND a.Type=b.Type 
                        AND isnull(a.Sitting,0)=isnull(b.Sitting,0) AND a.CityCode=b.CityCode 
                        AND a.Year > b.Year 
                        INNER JOIN dbo.OrderSimulationMV omv ON omv.VehicleCode = a.VehicleCode
                        INNER JOIN dbo.OrderSimulation os ON os.OrderNo = omv.OrderNo
                        INNER JOIN dbo.ProspectCustomer p ON p.CustID = os.CustID
                        WHERE os.ApplyF = 1 AND os.RowStatus = 1 AND omv.RowStatus = 1
						AND os.CustID = @0 AND os.FollowUpNo = @1 AND b.Year IS NULL 
                        ORDER BY a.VehicleCode";
                                vData = dbMobile.Query<Otosales.Models.vWeb2.VehicleData>(queryVehicleData, CustID, FollowUpNo).FirstOrDefault();
                                if (vData != null)
                                {
                                    queryVehicleData = @";SELECT mbc.ID BasicCoverID FROM dbo.OrderSimulation os 
                                                INNER JOIN dbo.Mst_Basic_Cover mbc
                                                ON mbc.ComprePeriod = os.ComprePeriod AND mbc.TLOPeriod = os.TLOPeriod
                                                WHERE OrderNo = @0 AND os.ApplyF = 1 AND os.RowStatus = 1";
                                    List<dynamic> bscCvr = dbMobile.Fetch<dynamic>(queryVehicleData, vData.OrderNo);
                                    if (bscCvr.Count > 1)
                                    {
                                        queryVehicleData = @"SELECT RTRIM(LTRIM(CoverageID)) FROM dbo.OrderSimulationCoverage WHERE OrderNo = @0";
                                        List<string> ListCvrID = dbMobile.Fetch<string>(queryVehicleData, vData.OrderNo);
                                        if (ListCvrID.Contains("TLO"))
                                        {
                                            queryVehicleData = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description = 'TLO Others'";
                                            int id = dbMobile.ExecuteScalar<int>(queryVehicleData);
                                            vData.BasicCoverID = id;
                                        }
                                        else if (ListCvrID.Contains("ALLRIK"))
                                        {
                                            queryVehicleData = "SELECT ID FROM dbo.Mst_Basic_Cover WHERE Description = 'Comprehensive Others'";
                                            int id = dbMobile.ExecuteScalar<int>(queryVehicleData);
                                            vData.BasicCoverID = id;
                                        }
                                    }
                                    else
                                    {
                                        if (bscCvr.Count > 0)
                                        {
                                            vData.BasicCoverID = bscCvr.First().BasicCoverID;
                                        }
                                        else
                                        {
                                            vData.BasicCoverID = 0;
                                        }
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                //throw;
                            }
                        });

                        Thread threadVehicleData2 = new Thread(() =>
                        {
                            try
                            {
                                var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

                                string queryVehicleData = @";SELECT OldPolicyNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1 ";
                                string OldPolicyNo = dbMobile.FirstOrDefault<string>(queryVehicleData, CustID, FollowUpNo);

                                if (!string.IsNullOrEmpty(OldPolicyNo))
                                {
                                    queryVehicleData = @";DECLARE @@PolicyID VARCHAR(100)
                                SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
                                SELECT STUFF(( SELECT ', ' + Name
                                FROM dbo.Dtl_Original_Defect 
                                where Policy_Id = @@PolicyID
                                FOR XML PATH('')
                                ), 1, 1, '')  AS OriginalDefect,
								STUFF(( SELECT ', ' + Name
                                FROM dbo.Ren_Dtl_NoCover 
                                where Policy_No = @0
                                FOR XML PATH('')
                                ), 1, 1, '')  AS NoCover,
								STUFF(( SELECT ', ' + RTRIM(CONCAT(AccsDescription, ' ', AccsCatDescription, ' ',
                                AccsPartDescription))
                                FROM    ( SELECT DISTINCT
                                                    RTRIM(ISNULL(wnsa.Policy_No, '')) AS PolicyNo ,
                                                    wnsa.Object_No AS ObjectNo ,
                                                    RTRIM(ISNULL(wnsa.Accs_Code, '')) AS AccsCode ,
                                                    RTRIM(ISNULL(AO.AccsDescription, '')) AS AccsDescription ,
                                                    RTRIM(ISNULL(wnsa.Accs_Part_Code, '')) AS AccsPartCode ,
                                                    CASE ao.AccsType
                                                      WHEN 4 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      WHEN 5 THEN RTRIM(ISNULL(x.AccsCatDescription, ''))
                                                      ELSE ''
                                                    END AS AccsCatDescription ,
                                                    RTRIM(ISNULL(ap.AccsPartDescription, '')) AS AccsPartDescription ,
                                                    Include_Tsi AS IncludeTsi ,
                                                    RTRIM(ISNULL(Brand, '')) AS Brand ,
                                                    Sum_Insured AS SumInsured ,
                                                    Quantity ,
                                                    Premi ,
                                                    RTRIM(ISNULL(Category, '')) AS Category ,
                                                    RTRIM(ISNULL(wnsa.Accs_Cat_Code, '')) AS AccsCatCode
                                          FROM      dbo.Ren_Dtl_Non_Standard_Accessories AS wnsa
                                                    LEFT JOIN dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.Accs_Code
                                                                                          AND ao.RowStatus = 1
                                                    LEFT JOIN AccessoriesPart AP ON Ap.AccsCode = wnsa.Accs_Code
                                                                                    AND AP.AccsPartCode = wnsa.Accs_Part_Code
                                                                                    AND ap.RowStatus = 1
                                                    LEFT JOIN ( SELECT  RTRIM(ISNULL(ac.AccsCode, '')) AS AccsCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatCode, '')) AS AccsCatCode ,
                                                                        RTRIM(ISNULL(ac.AccsCatDescription, '')) AS AccsCatDescription ,
                                                                        RTRIM(ISNULL(acp.AccsPartCode, '')) AS AccsPartCode ,
                                                                        ac.MaxSICategory ,
                                                                        ac.IsEditable ,
                                                                        acp.QtyDefault ,
                                                                        ac.RowStatus
                                                                FROM    dbo.AccessoriesCategory AS ac
                                                                        LEFT JOIN dbo.AccessoriesCategoryPart
                                                                        AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                                                  AND acp.RowStatus = 1
                                                              ) x ON x.AccsCode = ao.AccsCode
                                                                     AND x.RowStatus = 1
                                                                     AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                                                              '')
                                                                     AND x.AccsCode = wnsa.Accs_Code
                                                                     AND x.AccsCatCode = wnsa.Accs_Cat_Code
                                          WHERE     Policy_No = @0
                                        ) AS X
                                FOR XML PATH('')
                                ), 1, 1, '')  AS Accessories";

                                    var AABdbThread = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                                    List<dynamic> list = AABdbThread.Fetch<dynamic>(queryVehicleData, OldPolicyNo);
                                    if (list.Count > 0)
                                    {
                                        vData.ORDefectsDesc = list.First().OriginalDefect;
                                        vData.NoCover = list.First().NoCover;
                                        vData.Accessories = list.First().Accessories;
                                    }
                                }
                            }
                            catch (Exception)
                            {

                                //throw;
                            }
                        });
                        threadVehicleData1.Start();
                        threadVehicleData2.Start();

                        threadVehicleData1.Join();
                        threadVehicleData2.Join();
                        result.VehicleData = vData;
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadPolicyAddress = new Thread(() =>
                {
                    try
                    {
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string queryPolicyAddress = @";SELECT PolicySentTo SentTo, PolicyDeliveryName Name, PolicyDeliveryAddress Address, PolicyDeliveryPostalCode PostalCode FROM dbo.OrderSimulation WHERE CustID = @0 
                            AND FollowUpNo = @1 AND RowStatus = 1 AND ApplyF = 1";
                        Otosales.Models.vWeb2.PolicyAddress pa = dbMobile.Query<Otosales.Models.vWeb2.PolicyAddress>(queryPolicyAddress, CustID, FollowUpNo).FirstOrDefault();
                        if (pa != null)
                        {
                            result.PolicyAddres = pa;
                        }
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });

                Thread threadSurveySchedule = new Thread(() =>
                {
                    try
                    {
                        var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string querySurveySchedule = @";SELECT CityCode,LocationCode,SurveyAddress,SurveyPostalCode,SurveyDate, COALESCE(oss.ScheduleTimeID,0) ScheduleTimeID,
                        CAST(COALESCE(IsNeedSurvey,0) AS BIT) IsNeedSurvey, CAST(COALESCE(IsNSASkipSurvey,0) AS BIT) IsNSASkipSurvey, 
						ScheduleTimeTextID ScheduleTimeDesc, CAST(COALESCE(IsManualSurvey,0) AS BIT) IsManualSurvey
                        FROM dbo.OrderSimulationSurvey oss
                        INNER JOIN dbo.OrderSimulation o ON o.OrderNo = oss.OrderNo
						LEFT JOIN Otocare.dbo.ScheduleTime st ON oss.ScheduleTimeID = st.ScheduleTimeID
                        WHERE o.CustID = @0 AND o.FollowUpNo = @1 AND o.RowStatus = 1 AND o.ApplyF = 1";
                        Otosales.Models.vWeb2.SurveySchedule ss = dbMobile.Fetch<Otosales.Models.vWeb2.SurveySchedule>(querySurveySchedule, CustID, FollowUpNo).FirstOrDefault();
                        result.SurveySchedule = ss;
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });


                threadPremi.Start();
                threadPersonalData.Start();
                threadCompanyData.Start();
                threadVehicleData.Start();
                threadPolicyAddress.Start();
                threadSurveySchedule.Start();

                threadPremi.Join();
                threadPersonalData.Join();
                threadCompanyData.Join();
                threadVehicleData.Join();
                threadPolicyAddress.Join();
                threadSurveySchedule.Join();

                result.PaymentInfo = new Otosales.Models.vWeb2.PaymentInfo();


                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public string UpdateTaskListDetail(string CustID, string FollowUpNo,
Otosales.Models.vWeb2.PersonalData pd, Otosales.Models.vWeb2.CompanyData cd, Otosales.Models.vWeb2.VehicleData vd, Otosales.Models.vWeb2.PolicyAddress pa,
Otosales.Models.vWeb2.SurveySchedule ss, List<Otosales.Models.vWeb2.ImageDataTaskDetail> ListImgData, Models.vWeb2.OrderSimulationModel OS, Models.vWeb2.OrderSimulationMVModel OSMV,
Models.vWeb2.CalculatePremiModel CalculatePremiModel, string FollowUpStatus, Otosales.Models.vWeb2.Remarks rm)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string result = "";

            string query = @"";
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);

            try
            {
                //Otosales.Repository.vWeb2.MobileRepository.updateFollowUpStatusToPolicyCreated(FollowUpNo);

                Thread threadPd = new Thread(() =>
                {
                    if (pd != null)
                    {
                        try
                        {
                            // pd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PersonalData>(personalData);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @";IF EXISTS(SELECT * FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.ProspectCustomer SET 
	                        Name = @1, CustBirthDay = @2, CustGender = @3, CustAddress = @4, 
                            PostalCode = @5, Email1 = @6, Phone1 = @7, IdentityNo = @8, 
                            isCompany = @9
                            WHERE CustID = @0 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @12
                        AND ApplyF = 1 AND RowStatus = 1)
                        BEGIN
	                        UPDATE dbo.OrderSimulation 
	                        SET IsNeedDocRep = @10, AmountRep = @11
	                        WHERE CustID = @0 AND FollowUpNo = @12
	                        AND ApplyF = 1 AND RowStatus = 1
                        END
                        IF EXISTS(SELECT * FROM dbo.FollowUp WHERE CustID = @0 AND FollowUpNo = @12)
                        BEGIN
	                        UPDATE dbo.FollowUp 
	                        SET FollowUpName = @1, ProspectName = @1
	                        WHERE CustID = @0 AND FollowUpNo = @12
                        END ";
                            dbMobile.Execute(queryThread, CustID
                                , pd.Name, pd.CustBirthDay, pd.CustGender, pd.CustAddress
                                , pd.PostalCode, pd.Email1, pd.Phone1, pd.IdentityNo
                                , pd.isCompany, pd.isNeedDocRep, pd.AmountRep, FollowUpNo);
                            if (pd.IsPayerCompany != null)
                            {
                                queryThread = @";UPDATE dbo.ProspectCustomer SET IsPayerCompany = @1 WHERE CustID = @0";
                                dbMobile.Execute(queryThread, CustID, pd.IsPayerCompany);
                            }
                        }
                        catch (Exception e)
                        {
                            //throw;
                        }
                    }
                });

                Thread threadCd = new Thread(() =>
                {
                    if (cd != null)
                    {
                        try
                        {
                            //cd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.CompanyData>(companyData);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @";DECLARE @@IsCompany BIT
                            SELECT @@IsCompany=IsCompany FROM dbo.ProspectCustomer WHERE CustID = @0 AND RowStatus = 1
                            IF @@IsCompany=1
                            BEGIN
	                            IF EXISTS (SELECT * FROM dbo.ProspectCompany WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                                BEGIN
	                                UPDATE dbo.ProspectCompany SET CompanyName = @2, NPWPno = @3, NPWPdate = @4, 
	                                NPWPaddress = @5, OfficeAddress = @6, PostalCode = @7, PICPhoneNo = @8, PICname = @9, Email = @10 
	                                WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
		                            UPDATE dbo.ProspectCustomer SET Name = @9, Phone1 = @8, Email1 = @10
		                            WHERE CustID = @0
                                END
                            END";
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , cd.CompanyName, cd.NPWPno, cd.NPWPdate
                                , cd.NPWPaddres, cd.OfficeAddress, cd.PostalCode, cd.PICPhoneNo, cd.PICname, cd.PICEmail);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadOS = new Thread(() =>
                {
                    if (OS != null && OSMV != null)
                    {
                        try
                        {
                            v0063URF2020.MobileRepository.InsertOrderSimulation(OS, OSMV);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadVd = new Thread(() =>
                {
                    if (vd != null)
                    {
                        try
                        {
                            // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = "";

                            if (!string.IsNullOrEmpty(vd.ProductCode))
                            {
                                queryThread = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET ProductTypeCode = @2,
	                            VehicleCode = @3, BrandCode = @4, ModelCode = @5,
	                            Series = @6, Type = @7, Sitting = @8, Year = @9,
	                            CityCode = @10, UsageCode = @11, AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET InsuranceType = @19, 
                                ProductCode = @20, 
								SegmentCode = @21, PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29, MouID = @30
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                            }
                            else
                            {
                                queryThread = @";DECLARE @@Orderno VARCHAR(100)
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationMV mv 
                            INNER JOIN dbo.OrderSimulation o ON o.OrderNo = mv.OrderNo
                            INNER JOIN dbo.FollowUp f ON o.FollowUpNo = f.FollowUpNo
                            INNER JOIN dbo.ProspectCustomer p ON p.CustID = f.CustID
                            WHERE f.CustID = @0 AND f.FollowUpNo = @1)
                            BEGIN
	                            SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation o 
	                            INNER JOIN dbo.FollowUp f ON f.FollowUpNo = o.FollowUpNo
	                            WHERE f.CustID = @0 AND f.FollowUpNo = @1 AND o.RowStatus = 1
                                AND ApplyF = 1
	 
                                UPDATE dbo.OrderSimulationMV SET AccessSI = @12,
	                            RegistrationNumber = @13, EngineNumber = @14,
	                            ChasisNumber = @15, IsNew = @16, SumInsured = @17, 
                                ColorOnBPKB = @18
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1

                                UPDATE dbo.OrderSimulation SET PeriodFrom = @22, PeriodTo = @23,
								IsORDefectsRepair = @24, IsAccessoriesChange = @25,
								IsPolicyIssuedBeforePaying = @26,
                                Remarks = @27, DealerCode = @28,
                                SalesDealer = @29, MouID = @30
	                            WHERE OrderNo = @@Orderno AND RowStatus = 1 
								AND ApplyF = 1

	                            UPDATE dbo.ProspectCustomer SET DealerCode = @28,
	                            SalesDealer = @29 WHERE CustID = @0 AND RowStatus = 1
                            END";
                            }
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , vd.ProductTypeCode
                                , vd.VehicleCode, vd.BrandCode, vd.ModelCode
                                , vd.Series, vd.Type, vd.Sitting, vd.Year
                                , vd.CityCode, vd.UsageCode, vd.AccessSI
                                , vd.RegistrationNumber, vd.EngineNumber
                                , vd.ChasisNumber, vd.IsNew, vd.SumInsured
                                , vd.ColorOnBPKB

                                , vd.InsuranceType
                                , vd.ProductCode
                                , vd.SegmentCode, vd.PeriodFrom, vd.PeriodTo
                                , vd.IsORDefectsRepair, vd.IsAccessoriesChange
                                , vd.IsPolicyIssuedBeforePaying
                                , vd.Remarks

                                , vd.DealerCode
                                , vd.SalesDealer
                                , vd.MouID);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadCalculatePremi = new Thread(() =>
                {
                    if (CalculatePremiModel != null)
                    {
                        try
                        {
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string OrderNo = dbMobile.ExecuteScalar<string>(@";
DECLARE @@Orderno VARCHAR(100) = ''
SELECT @@Orderno=OrderNo FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND ApplyF = 1 AND RowStatus = 1
SELECT @@Orderno", CustID, FollowUpNo);

                            // vd = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.VehicleData>(vehicleData);
                            v0063URF2020.MobileRepository.InsertExtendedCover(OrderNo, CalculatePremiModel, vd.PeriodFrom, vd.PeriodTo);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadPa = new Thread(() =>
                {
                    if (pa != null)
                    {

                        try
                        {
                            //pa = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.PolicyAddress>(policyAddress);
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @"IF EXISTS (SELECT * FROM dbo.OrderSimulation WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulation SET PolicySentTo = @2, PolicyDeliveryName = @3, 
	                            PolicyDeliveryAddress = @4, PolicyDeliveryPostalCode = @5 WHERE CustID = @0 AND FollowUpNo = @1 AND RowStatus = 1
                            END";
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , pa.SentTo, pa.Name
                                , pa.Address, pa.PostalCode);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                Thread threadSs = new Thread(() =>
                {
                    if (ss != null)
                    {
                        //ss = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.SurveySchedule>(surveySchedule);
                        try
                        {
                            var dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                            string queryThread = @";DECLARE @@OrderNo VARCHAR(100);
                            SELECT @@OrderNo=OrderNo FROM dbo.OrderSimulation
                            WHERE CustID = @0 AND FollowUpNo = @1 AND 
                            RowStatus = 1 AND ApplyF = 1
                            IF EXISTS(SELECT * FROM dbo.OrderSimulationSurvey oss
                            INNER JOIN dbo.OrderSimulation os ON os.OrderNo = oss.OrderNo
                            WHERE os.CustID = @0 AND os.FollowUpNo = @1 AND 
                            os.RowStatus = 1 AND os.ApplyF = 1)
                            BEGIN
	                            UPDATE dbo.OrderSimulationSurvey SET CityCode = @2, LocationCode = @3, SurveyAddress = @4,
	                            SurveyPostalCode = @5, SurveyDate = @6, ScheduleTimeID = @7, ModifiedBy = 'OtosalesAPI',
								IsNeedSurvey = @8, IsNSASkipSurvey = @9, IsManualSurvey = @10,
								ModifiedDate = GETDATE() WHERE OrderNo = @@OrderNo
                            END
							ELSE
							BEGIN
								INSERT INTO dbo.OrderSimulationSurvey
								        ( OrderNo ,
								          CityCode ,
								          LocationCode ,
								          SurveyAddress ,
								          SurveyPostalCode ,
								          SurveyDate ,
								          ScheduleTimeID ,
								          RowStatus ,
								          CreatedBy ,
								          CreatedDate,
										  IsNeedSurvey,
										  IsNSASkipSurvey,
										  IsManualSurvey
								        )
								VALUES  ( @@OrderNo , -- OrderNo - varchar(100)
								          @2 , -- CityCode - int
								          @3 , -- LocationCode - int
								          @4 , -- SurveyAddress - varchar(500)
								          @5 , -- SurveyPostalCode - varchar(5)
								          @6 , -- SurveyDate - datetime
								          @7 , -- ScheduleTimeID - datetime
								          1 , -- RowStatus - bit
								          'OtosalesAPI' , -- CreatedBy - varchar(50)
								          GETDATE(), -- CreatedDate - datetime
										  @8,
										  @9,
										  @10
								        )
							END";
                            int maxLength = 150;
                            string SurveyAddress = "";
                            if (!string.IsNullOrEmpty(ss.SurveyAddress) && !string.IsNullOrWhiteSpace(ss.SurveyAddress))
                            {
                                SurveyAddress = ss.SurveyAddress.Length <= maxLength ? ss.SurveyAddress : ss.SurveyAddress.Substring(0, maxLength);
                            }
                            dbMobile.Execute(queryThread, CustID, FollowUpNo
                                , ss.CityCode, ss.LocationCode, ss.SurveyAddress
                                , ss.SurveyPostalCode, ss.SurveyDate, ss.ScheduleTimeID
                                , ss.IsNeedSurvey, ss.IsNSASkipSurvey, ss.IsManualSurvey);
                        }
                        catch (Exception e)
                        {

                            //throw;
                        }
                    }
                });

                threadPd.Start();
                threadCd.Start();
                threadOS.Start();
                threadVd.Start();
                threadCalculatePremi.Start();
                threadPa.Start();
                threadSs.Start();


                threadPd.Join();
                threadCd.Join();
                threadOS.Join();
                threadVd.Join();
                threadCalculatePremi.Join();
                threadPa.Join();
                threadSs.Join();


                if (ListImgData.Count > 0)
                {
                    //ListImgData = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.ImageDataTaskDetail>>(imageData);
                    v0063URF2020.MobileRepository.UpdateImageData(ListImgData, CustID, FollowUpNo);

                }
                if (!string.IsNullOrEmpty(FollowUpStatus))
                {
                    query = @"UPDATE dbo.FollowUp SET FollowUpStatus = @2 WHERE CustID = @0 AND FollowUpNo = @1";
                    db.Execute(query, CustID, FollowUpNo, FollowUpStatus);
                }
                query = @"SELECT OrderNo FROM dbo.OrderSimulation
                        WHERE CustID = @0 AND FollowUpNo = @1 AND 
                        RowStatus = 1 AND ApplyF = 1";
                List<dynamic> res = db.Fetch<dynamic>(query, CustID, FollowUpNo);
                if (res.Count > 0)
                {
                    result = res.First().OrderNo;
                }
                if (rm != null)
                {
                    query = @"UPDATE dbo.FollowUp
                            SET RemarkToSA = @0 WHERE FollowUpNo = @1";
                    // rm = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.Remarks>(Remarks);
                    db.Execute(query, rm.RemarkToSA, FollowUpNo);
                }
                return result;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public Models.v0219URF2019.DownloadQuotationResult GetByteReport(string orderNo)
        {

            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportServerURLBeyond = ConfigurationManager.AppSettings["ReportServerURLBeyond"].ToString();
            string ReportPathOne = ConfigurationManager.AppSettings["ReportPathOne"].ToString();
            string ReportPathTinsOne = ConfigurationManager.AppSettings["ReportPathTinsOne"].ToString();
            string ReportPathLexusOne = ConfigurationManager.AppSettings["ReportPathLexusOne"].ToString();
            string ReportPathTwo = ConfigurationManager.AppSettings["ReportPathTwo"].ToString();
            string ReportPathTinsTwo = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusTwo = ConfigurationManager.AppSettings["ReportPathLexusTwo"].ToString();
            string ReportPathThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathTinsThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusThree = ConfigurationManager.AppSettings["ReportPathLexusThree"].ToString();
            string CertificateQuotation = ConfigurationManager.AppSettings["CertificateQuotation"].ToString();
            string CertificateRenewalNotice = ConfigurationManager.AppSettings["CertificateRenewalNotice"].ToString();
            string CertificateQuotationGODIG = ConfigurationManager.AppSettings["CertificateQuotationGODIG"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;
            string CredentialsRenewalUsername = ConfigurationManager.AppSettings["CredentialsRenewalUsername"].ToString(); ;
            string CredentialsRenewalPassword = ConfigurationManager.AppSettings["CredentialsRenewalPassword"].ToString(); ;
            string CredentialsRenewalDomain = ConfigurationManager.AppSettings["CredentialsRenewalDomain"].ToString(); ;

            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";
            int idReportRennot = 0;

            byte[] bytes;
            Warning[] warnings;
            string mimeType;
            string encoding;
            string filenameExtension;
            string[] streamids;
            int yearCoverage;
            int insuranceType;
            Models.v0219URF2019.DownloadQuotationResult result = new Models.v0219URF2019.DownloadQuotationResult();
            NetworkCredential NetCredential = new NetworkCredential();
            ReportViewer rView = new ReportViewer();
            rView.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = rView.ServerReport;

            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            string qGetDt = @"SELECT o.PolicyNo, p.Name, o.InsuranceType, o.YearCoverage, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal,
                        CASE 
	                        WHEN f.referenceno IS NOT NULL AND LEN(f.referenceno) > 10
		                        THEN 1
	                        ELSE 0
                        END AS
	                        IsMVGodig
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f 
                        ON f.CustID = p.CustID
                        INNER JOIN dbo.OrderSimulation o
                        ON o.FollowUpNo = f.FollowUpNo
                        INNER JOIN dbo.OrderSimulationMV omv
                        ON omv.OrderNo = o.OrderNo
                        WHERE o.OrderNo = @0";
            List<dynamic> dt = db.Fetch<dynamic>(qGetDt, orderNo);
            string qGetPlcyNo = @";SELECT os.OldPolicyNo, os.SalesOfficerID, CAST(IIF(LEN(f.ReferenceNo)>10,1,0) AS BIT) IsMVGodig 
FROM OrderSimulation os INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo 
WHERE OrderNo = @0 ANd os.RowStatus = 1 AND f.RowStatus = 1";
            dynamic policyNo = "";
            using (a2isDBHelper.Database dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                policyNo = dbMobile.FirstOrDefault<dynamic>(qGetPlcyNo, orderNo);
            }

            Thread tRenew = new Thread(() =>
            {
                try
                {
                    #region Insert Data to RPT_Renewal_Notice Dan RPT_Renewal_Notice_Detail
                    if (!policyNo.IsMVGodig)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(policyNo.OldPolicyNo)))
                        {
                            string qSPPrint = @";EXECUTE [dbo].[usp_InsertPrintRenewalNoticeOtosales] @0, @1";
                            using (a2isDBHelper.Database dbAAB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                            {
                                idReportRennot = dbAAB.FirstOrDefault<int>(qSPPrint, Convert.ToString(policyNo.OldPolicyNo), Convert.ToString(policyNo.SalesOfficerID));
                                InsertPrintRennotExtended(idReportRennot, orderNo);
                            }
                        }
                        else
                        {
                            InsertQuotationPrintTable(orderNo);
                        }
                    }
                    else
                    {
                        using (var db1 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                        {
                            string qSPTele = @";EXEC dbo.usp_InsertDataOtosalesForQuotationTelesales @0";
                            db.Execute(qSPTele, orderNo);
                        }
                    }
                    #endregion

                }
                catch (Exception e)
                {

                    //throw;
                }
            });

            Thread tRview = new Thread(() =>
            {
                try
                {
                    if (dt.Count > 0)
                    {
                        yearCoverage = Convert.ToInt32(dt.First().YearCoverage);
                        insuranceType = Convert.ToInt32(dt.First().InsuranceType);

                        if (!dt.First().IsRenewal)
                        {
                            if (dt.First().IsMVGodig == 1) //Generate Quotation With RDL 0219URF2019
                            {
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotationGODIG;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }
                            else
                            {                      //Generate Quotation With Old RDL
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotation;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }

                        }
                        else
                        {
                            serverReport.ReportServerUrl = new Uri(ReportServerURLBeyond);
                            serverReport.ReportPath = CertificateRenewalNotice;
                            NetCredential = new NetworkCredential(CredentialsRenewalUsername, CredentialsRenewalPassword, CredentialsRenewalDomain);


                        }

                        serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;

                        ReportParameter[] parameters = new ReportParameter[1];
                        ReportParameter reportParam = new ReportParameter();
                        if (!dt.First().IsRenewal)
                        {
                            reportParam.Name = "OrderNo";
                            reportParam.Values.Add(orderNo);
                        }
                        else
                        {

                            reportParam.Name = "PolicyNo";
                            reportParam.Values.Add(Convert.ToString(policyNo.OldPolicyNo));
                        }
                        parameters[0] = reportParam;
                        rView.ServerReport.SetParameters(parameters);
                    }
                }
                catch (Exception ex)
                {
                    //string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();
                    throw ex;
                }
            });

            tRenew.Start();
            tRview.Start();

            tRenew.Join();
            tRview.Join();

            try
            {
                if (dt.Count > 0)
                {
                    #region Catat Print Quotation
                    string qInsertLog = @"INSERT INTO dbo.PrintQuotationHistoryLog
                                        ( OrderNo, RowStatus, EntryDate )
                                VALUES  ( @0, -- OrderNo - varchar(100)
                                          1, -- RowStatus - smallint
                                          GETDATE()  -- EntryDate - datetime
                                          )";
                    db.Execute(qInsertLog, orderNo);
                    #endregion
                    bytes = rView.ServerReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);
                    result.Bytes = bytes;

                    #region filename
                    int cetakanke = db.ExecuteScalar<int>("SELECT COUNT(*) FROM dbo.PrintQuotationHistoryLog WHERE OrderNo=@0", orderNo) + 1;

                    string fileName = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='ATTFILENAME' AND ParamType='TMP' AND ParamValue2=@0", dt.First().IsRenewal);
                    fileName = fileName.Replace("@CetakanKe", cetakanke.ToString()).Replace("@TanggalCetak", DateTime.Now.ToString("dd-MM-yyyy"));
                    if (dt.First().IsRenewal)
                    {
                        fileName = fileName.Replace("@NoPolis", dt.First().PolicyNo.Trim());

                    }
                    #endregion

                    result.FileName = fileName;
                }
            }
            catch (Exception e)
            {
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + e.Message.ToString();
                throw e;
                //throw;
            }
            return result;
        }

        internal void InsertPrintRennotExtended(int idReportRennot, string orderNo)
        {
            try
            {

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                List<CalculatedPremi> cpremiitems = vWeb2.MobileRepository.LoadCalculatedpremibyOrderNo(orderNo, " AND a.InterestId IN('PADRVR','PAPASS','TPLPER')");
                OrderSimulation os = db.FirstOrDefault<OrderSimulation>("SELECT * FROM OrderSimulation WHERE OrderNo=@0", orderNo);
                OrderSimulationMV osmv = db.FirstOrDefault<OrderSimulationMV>("select * from ordersimulationMV WHERE orderno=@0", orderNo);
                #region detail scoring
                List<DetailScoring> dtlScoring = new List<DetailScoring>();
                DetailScoring ds = new DetailScoring();
                if (!string.IsNullOrEmpty(osmv.CityCode))
                {
                    ds.FactorCode = "GEOGRA";
                    ds.InsuranceCode = osmv.CityCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.UsageCode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "MVUSAG";
                    ds.InsuranceCode = osmv.UsageCode;
                    dtlScoring.Add(ds);
                }
                ds = new DetailScoring();
                ds.FactorCode = "VHCTYP";
                ds.InsuranceCode = string.IsNullOrEmpty(osmv.Type) ? "ALL   " : osmv.Type;
                dtlScoring.Add(ds);
                if (!string.IsNullOrEmpty(osmv.BrandCode))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "VBRAND";
                    ds.InsuranceCode = osmv.BrandCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.IsNew.ToString()))
                {

                    ds = new DetailScoring();
                    ds.FactorCode = "NEWVHC";
                    ds.InsuranceCode = osmv.IsNew.ToString();
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.ModelCode))
                {
                    ds = new DetailScoring();
                    ds.FactorCode = "VMODEL";
                    ds.InsuranceCode = osmv.ModelCode;
                    dtlScoring.Add(ds);
                }
                #endregion

                #region get extended detail cover
                int year = 0;
                foreach (CalculatedPremi item in cpremiitems)
                {

                    DateTime pfcover = item.PeriodFrom;
                    DateTime ptcover = item.PeriodFrom;
                    CalculateAndInsertRennot(idReportRennot, pfcover, ptcover, item, os, osmv, dtlScoring);

                }
            }
            #endregion

            catch (Exception e)
            {
                throw (e);
            }
        }
        private void CalculateAndInsertRennot(int idReportRennot, DateTime pfcover, DateTime ptcover, CalculatedPremi item, OrderSimulation os, OrderSimulationMV osmv, List<DetailScoring> dtlScoring)
        {
            try
            {

                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                int Ndays = db.ExecuteScalar<int>("SELECT CASE WHEN day_calculation_method =360 THEN 360 ELSE 366 END [NDays] from Mst_Product WHERE Product_Code=@0", os.ProductCode);
                int year = 0;
                string query = @"INSERT INTO [dbo].[RptPrintRenewalNoticeDetail]
           ([RptPrintRenewalNoticeID]
           ,[InterestID]
           ,[InterestDescription]
           ,[InterestType]
           ,[CoverageID]
           ,[CoverageDescription]
           ,[CoverageType]
           ,[TSI]
           ,[Rate]
           ,[Premium]
           ,[OwnRisk]
           ,[Loading]
           ,[IsBundling]
           ,[Year])
     VALUES
           (@0
           ,@1
           ,@2
           ,'L'
           ,@3
           ,@4
           ,'EXD'
           ,@5
           ,@6
           ,@7
           ,@8
           ,@9
           ,@10
           ,@11)";
                while (pfcover < item.PeriodTo)
                {

                    year++;
                    ptcover = item.PeriodFrom.AddYears(year);
                    CoverageParam cp = new CoverageParam();
                    cp.CoverageId = item.CoverageID;
                    cp.PeriodFromCover = pfcover;
                    cp.PeriodToCover = (ptcover.Year == item.PeriodTo.Year) ? item.PeriodTo : ptcover;
                    int calcmethod = item.PeriodTo.Date < item.PeriodFrom.Date.AddMonths(12) ? 1 : 0;
                    double premi = RecalculateExtendedCover(item.InterestID, cp, os.PeriodFrom, os.PeriodTo, os.ProductCode, os.MouID, dtlScoring, osmv.Year, osmv.UsageCode, Ndays
                                                                       , osmv.Type, osmv.Sitting.ToString(), Convert.ToDecimal(item.SumInsured), os.OldPolicyNo, calcmethod, true);
                    int yearcoverage = 0;
                    DateTime PeriodFrom = os.PeriodFrom ?? DateTime.Now;
                    DateTime PeriodTo = os.PeriodTo ?? DateTime.Now;
                    yearcoverage = (PeriodTo.Year - PeriodFrom.Year) - (PeriodTo.Year - pfcover.Year) + 1;
                    pfcover = item.PeriodFrom.AddYears(year);
                    string InterestDesc = db.ExecuteScalar<string>(@"SELECT TOP 1 DESCRIPTION FROM mst_interest WHERE interest_id=@0", item.InterestID);
                    string CoverageDesc = db.ExecuteScalar<string>(@"SELECT TOP 1 name FROM mst_coverage WHERE coverage_id=@0", item.CoverageID);
                    string DeductibleDesc = db.ExecuteScalar<string>(@"SELECT TOP 1 DESCRIPTION FROM prd_deductible WHERE product_code=@0 AND deductible_code=@1", os.ProductCode, item.DeductibleCode);

                    db.Execute(query, idReportRennot, item.InterestID, InterestDesc, item.CoverageID, CoverageDesc, item.SumInsured, item.Rate, premi, DeductibleDesc, item.Loading, item.IsBundling, yearcoverage);
                }
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public void InsertQuotationPrintTable(string orderNo)
        {
            try
            {
                #region QueryData
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                var aabdb = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                double AdminFee = db.ExecuteScalar<double>("SELECT AdminFee FROM OrderSimulation WHERE OrderNo=@0 ", orderNo);
                List<Otosales.Models.CalculatedPremi> cp = Otosales.Repository.vWeb2.MobileRepository.LoadCalculatedpremibyOrderNo(orderNo, "");
                string qGetOS = @"select * from ordersimulation WHERE orderno=@0";
                Otosales.Models.OrderSimulation os = db.FirstOrDefault<Otosales.Models.OrderSimulation>(qGetOS, orderNo);
                string qGetMV = @"select * from ordersimulationMV WHERE orderno=@0";
                Otosales.Models.OrderSimulationMV osmv = db.FirstOrDefault<Otosales.Models.OrderSimulationMV>(qGetMV, orderNo);
                #region detail scoring
                List<PremiumCalculation.Model.DetailScoring> dtlScoring = new List<PremiumCalculation.Model.DetailScoring>();
                PremiumCalculation.Model.DetailScoring ds = new PremiumCalculation.Model.DetailScoring();
                if (!string.IsNullOrEmpty(osmv.CityCode))
                {
                    ds.FactorCode = "GEOGRA";
                    ds.InsuranceCode = osmv.CityCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.UsageCode))
                {
                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "MVUSAG";
                    ds.InsuranceCode = osmv.UsageCode;
                    dtlScoring.Add(ds);
                }
                ds = new PremiumCalculation.Model.DetailScoring();
                ds.FactorCode = "VHCTYP";
                ds.InsuranceCode = string.IsNullOrEmpty(osmv.Type) ? "ALL   " : osmv.Type;
                dtlScoring.Add(ds);
                if (!string.IsNullOrEmpty(osmv.BrandCode))
                {

                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "VBRAND";
                    ds.InsuranceCode = osmv.BrandCode;
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.IsNew.ToString()))
                {

                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "NEWVHC";
                    ds.InsuranceCode = osmv.IsNew.ToString();
                    dtlScoring.Add(ds);
                }
                if (!string.IsNullOrEmpty(osmv.ModelCode))
                {
                    ds = new PremiumCalculation.Model.DetailScoring();
                    ds.FactorCode = "VMODEL";
                    ds.InsuranceCode = osmv.ModelCode;
                    dtlScoring.Add(ds);
                }
                #endregion
                double Discount = 0;
                Otosales.Models.DetailCoverageSummary rs = GetDetailCoverageSummary(os.ProductCode, os.MouID, dtlScoring, cp, AdminFee, osmv.Year, osmv.UsageCode, Convert.ToInt16(cp[0].Ndays), os.PeriodFrom, os.PeriodTo
                                                                                      , osmv.Type, osmv.Sitting.ToString(), os.OldPolicyNo, out Discount);
                List<string> IEP = Otosales.Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALES-SEGMENT-IEP");
                //var DiscountList = Otosales.Repository.vWeb2.RetailRepository.GetWTDiscount(os.ProductCode, os.MouID);
                //decimal Discount = DiscountList.Count() > 0 ? DiscountList[0].Percentage : 0;

                string qDltRPt = @";
DELETE FROM RptPrintQuotationDetail WHERE RptPrintQuotationID IN (SELECT RptPrintQuotationID FROM [RptPrintQuotation] WHERE OrderNo=@0)
DELETE FROM [RptPrintQuotation] WHERE OrderNo=@0";
                db.Execute(qDltRPt, orderNo);
                RptPrintQuotationModel mdl = new RptPrintQuotationModel();
                string ProducType = "", SalesmanPhone = "", SalesmanEmail = "", AccountNo = "", BankDescription = "", BankName = "",
                    NoCover = "", OriginalDefect = "", NonStandardAcc = "";
                List<dynamic> ls = db.Fetch<dynamic>(@";
SELECT SegmentCode, ProductCode, so.Email, so.Phone1, os.BranchCode, os.PolicyOrderNo 
FROM dbo.OrderSimulation os 
INNER JOIN dbo.SalesOfficer so ON so.SalesOfficerID = os.SalesOfficerID
WHERE OrderNo = @0", orderNo);

                Thread tAABMobileData = new Thread(() =>
                {
                    try
                    {
                        var db1 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        mdl = db1.Query<RptPrintQuotationModel>(@";
    select 
		   CASE WHEN mp.Biz_type = 2 THEN 4 
				ELSE os.InsuranceType END AS InsuranceType
			,os.QuotationNo
			,CEILING(CAST(DATEDIFF(MONTH,PeriodFrom,PeriodTo)/12.0 AS DECIMAL(10,2))) YearCoverage
			,os.PolicyDeliveryAddress AS InsuredAddress
			,COALESCE(pcy.OfficeAddress,pcy.NPWPAddress) AS CompanyAddress
			,COALESCE(os.PolicyNo,'-') AS PolicyNo
			,CAST(COALESCE(pc.IsCompany, 0) AS BIT) IsCompany
			,COALESCE(pcy.CompanyName,'') as CompanyName
			,pc.Name as InsuredName
			,COALESCE(pcy.PICname ,'') as PICname
			,pc.Phone1 AS InsuredPhone
			,COALESCE(pcy.PICPhoneNo,'') as PICPhoneNo
			,pc.Email1 as InsuredEmail
			,COALESCE(pcy.Email,'') AS PICEmail
			,os.PeriodFrom
			,os.PeriodTo
			,os.SalesOfficerID
			,os.BranchCode
			,so.Name AS SalesmanName
			,so.Role AS SalesmanRole
			,CONCAT(vb.Description,' ',vm.Description,' ',osm.Series) AS InsuredObject
			,osm.Year AS VehicleYear
			,(osm.Sitting-1) AS PassSeat
			,osm.ChasisNumber
			,osm.EngineNumber
			,u.Description AS Usage
			,osm.RegistrationNumber
			,r.Description AS GeoArea
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVAPERMATA'),COALESCE(os.VANumber,'')) as VAPermata
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVAMANDIRI'),COALESCE(os.VANumber,'')) as VAMandiri
			,CONCAT((SELECT ParamValue FROM [AABMobile].[dbo].[ApplicationParameters] WHERE ParamName = 'PREFIXVABCA'),COALESCE(os.VANumber,'')) as VABCA
			,ab.City
	From ordersimulation os 
	INNER JOIN OrderSimulationMV osm ON osm.orderno = os.orderno
	INNER JOIN [AABMobile].[dbo].[VehicleBrand] vb ON vb.BrandCode = osm.BrandCode
	INNER JOIN [AABMobile].[dbo].[VehicleModel] vm ON vm.ModelCode = osm.ModelCode
	INNER JOIN [AABMobile].[dbo].[Dtl_Ins_Factor] u ON u.insurance_code = osm.UsageCode 
	INNER JOIN [AABMobile].[dbo].[Region] r ON r.RegionCode = osm.CityCode
	INNER JOIN [AABMobile].[dbo].[SalesOfficer] so ON so.SalesOfficerID =os.SalesOfficerID
	INNER JOIN ProspectCustomer pc ON pc.CustID = os.CustID
	LEFT JOIN dbo.Mst_Product mp ON mp.Product_Code = os.ProductCode
	LEFT JOIN dbo.ProspectCompany pcy ON pcy.CustId=pc.CustID
	LEFT JOIN [AABMobile].[dbo].[OrderSimulationSurvey] oss ON oss.OrderNo = os.OrderNo
	LEFT JOIN [AABMobile].[dbo].[AABBranch] ab ON ab.BranchCode = os.BranchCode
    where os.orderno = @0", orderNo).FirstOrDefault();
                    }
                    catch (Exception)
                    {

                        //throw;
                    }
                });
                Thread tProducType = new Thread(() =>
                {
                    try
                    {
                        var adb1 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                        if (ls.Count > 0)
                        {
                            ProducType = adb1.ExecuteScalar<string>(@";
SELECT CASE WHEN @0 IN (SELECT ms.SegmentCode FROM dbo.Mapping_Product_SOB mpsob
                        INNER JOIN dbo.Mapping_Segment ms
                        ON mpsob.SOB_ID = ms.SegmentCode
                        WHERE 
                        (SegmentCode2 = 'SG2016' OR SegmentCode2 = 'SG2017')) THEN 'PROKHUS'
		WHEN @1 IN (@2) THEN 'IEP'
		ELSE ' ' END ", ls.First().SegmentCode, ls.First().ProductCode, IEP[0]);
                        }
                    }
                    catch (Exception e)
                    {

                        //throw;
                    }
                });
                Thread tSOInfo = new Thread(() =>
                {
                    try
                    {
                        var adb2 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                        if (ls.Count > 0)
                        {
                            List<dynamic> ExUsr = adb2.Fetch<dynamic>(@";SELECT UserID, MobileNumber FROM a2isAuthorizationDB.General.ExternalUsers WHERE UserID = @0",
                                ls.First().Email);
                            if (ExUsr.Count > 0)
                            {
                                SalesmanPhone = ExUsr.First().MobileNumber;
                                SalesmanEmail = ExUsr.First().UserID;
                            }
                            else
                            {
                                SalesmanPhone = ls.First().Phone1;
                                SalesmanEmail = ls.First().Email;
                            }
                        }
                    }
                    catch (Exception e)
                    {

                        //throw;
                    }
                });
                Thread tBranchInfo = new Thread(() =>
                {
                    try
                    {
                        var adb3 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                        if (ls.Count > 0)
                        {
                            List<dynamic> lsB = adb3.Fetch<dynamic>(@";
SELECT b.accountno, b.bankdescription, b.name as BankName
FROM [dbo].mst_branch AS a WITH ( NOLOCK ) 
INNER JOIN [dbo].branchaccount AS b WITH ( NOLOCK ) ON b.branchcode = a.accounting_code_sun
WHERE a.Branch_id = @0", ls.First().BranchCode);
                            if (lsB.Count > 0)
                            {
                                AccountNo = lsB.First().accountno;
                                BankDescription = lsB.First().bankdescription;
                                BankName = lsB.First().BankName;
                            }
                        }
                    }
                    catch (Exception e)
                    {

                        //throw;
                    }
                });
                Thread tAccinfo = new Thread(() =>
                {
                    try
                    {
                        var adb4 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
                        if (ls.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(ls.First().PolicyOrderNo))
                            {
                                List<dynamic> lsA = adb4.Fetch<dynamic>(@";
SELECT STUFF((	SELECT ', ' +  name
				FROM [dbo].ord_dtl_nocover where order_no=@0
				FOR XML PATH('')
			), 1, 1, '') AS NoCover
           ,STUFF((	SELECT ', ' +  name
				FROM [dbo].Ord_Dtl_Original_Defect where order_no=@0
				FOR XML PATH('')
			), 1, 1, '') AS OriginalDefect
           ,STUFF((SELECT ', '+
case when Category = 1
then
CONCAT(AccsDescription,' ',Brand)
when Category = 2
then
CONCAT(AccsDescription,' ',AccsPartDescription,' ',Brand)
when Category = 3
then
CONCAT(AccsDescription,' ',AccsPartDescription,' ',Brand)
when Category = 4
then
CONCAT(AccsCatDescription,' ',AccsPartDescription,' ',Brand)
when Category = 5
then
CONCAT(AccsCatDescription,' ',AccsPartDescription,' ',Brand)
end 
FROM    ( SELECT DISTINCT
                    wnsa.Category Category,
					AO.AccsDescription AccsDescription,
					wnsa.Brand Brand,
					AP.AccsPartDescription,
					x.AccsCatDescription
          FROM      dbo.Ord_Non_Standard_Accessories AS wnsa
                    LEFT JOIN dbo.AccessoriesOnline AO ON AO.AccsCode = wnsa.Accs_Code
                                                          AND ao.RowStatus = 1
                    LEFT JOIN dbo.AccessoriesPart AP ON Ap.AccsCode = wnsa.Accs_Code
                                                    AND AP.AccsPartCode = wnsa.Accs_Part_Code
                                                    AND ap.RowStatus = 1
                    LEFT JOIN ( SELECT  RTRIM(ISNULL(ac.AccsCode, '')) AS AccsCode ,
                                        RTRIM(ISNULL(ac.AccsCatCode, '')) AS AccsCatCode ,
                                        RTRIM(ISNULL(ac.AccsCatDescription, '')) AS AccsCatDescription ,
                                        RTRIM(ISNULL(acp.AccsPartCode, '')) AS AccsPartCode ,
                                        ac.MaxSICategory ,
                                        ac.IsEditable ,
                                        acp.QtyDefault ,
                                        ac.RowStatus
                                FROM    dbo.AccessoriesCategory AS ac
                                        LEFT JOIN dbo.AccessoriesCategoryPart
                                        AS acp ON acp.AccsCatCode = ac.AccsCatCode
                                                  AND acp.RowStatus = 1
                              ) x ON x.AccsCode = ao.AccsCode
                                     AND x.RowStatus = 1
                                     AND x.AccsPartCode = COALESCE(ap.AccsPartCode,
                                                              '')
                                     AND x.AccsCode = wnsa.Accs_Code
                                     AND x.AccsCatCode = wnsa.Accs_Cat_Code
          WHERE     wnsa.Order_No = @0
        ) AS X
				FOR XML PATH('')
			), 1, 1, '')  AS NonStandardAcc", ls.First().PolicyOrderNo);
                                if (lsA.Count > 0)
                                {
                                    NoCover = lsA.First().NoCover;
                                    OriginalDefect = lsA.First().OriginalDefect;
                                    NonStandardAcc = lsA.First().NonStandardAcc;
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {

                        //throw;
                    }
                });

                tAABMobileData.Start();
                tProducType.Start();
                tSOInfo.Start();
                tBranchInfo.Start();
                tAccinfo.Start();

                tAABMobileData.Join();
                tProducType.Join();
                tSOInfo.Join();
                tBranchInfo.Join();
                tAccinfo.Join();

                mdl.AdminFee = AdminFee;
                mdl.PADRVRTSI = rs.PADRVRSI;
                mdl.PAPASSTSI = rs.PAPASSSI;
                mdl.DiscountPct = Discount;
                mdl.ProductType = ProducType;
                mdl.SalesmanPhone = SalesmanPhone;
                mdl.SalesmanEmail = SalesmanEmail;
                mdl.AccountNo = AccountNo;
                mdl.BankDescription = BankName;
                mdl.BankName = BankName;
                mdl.NoCover = NoCover;
                mdl.OriginalDefect = OriginalDefect;
                mdl.NonStandardAcc = NonStandardAcc;

                string qInsertRpt = @";Declare @@TEMPID TABLE (RptPrintQuotationID bigint)
INSERT INTO dbo.RptPrintQuotation
        ( OrderNo ,
          AdminFee ,
          PADRVRTSI ,
          PAPASSTSI ,
          TPLTSI ,
          InsuranceType ,
          QuotationNo ,
          YearCoverage ,
          InsuredAddress ,
          CompanyAddress ,
          PolicyNo ,
          IsCompany ,
          CompanyName ,
          InsuredName ,
          PICName ,
          InsuredPhone ,
          PICPhoneNo ,
          InsuredEmail ,
          PICEmail ,
          PeriodFrom ,
          PeriodTo ,
          SalesOfficerID ,
          BranchCode ,
          SalesmanName ,
          SalesmanPhone ,
          SalesmanEmail ,
          SalesmanRole ,
          InsuredObject ,
          VehicleYear ,
          PassSeat ,
          ChasisNumber ,
          EngineNumber ,
          Usage ,
          RegistrationNumber ,
          GeoArea ,
          VAPermata ,
          VAMandiri ,
          VABCA ,
          ProductType ,
          City ,
          AccountNo ,
          BankDescription ,
          BankName ,
          NoCover ,
          OriginalDefect ,
          NonStandardAcc ,
          DiscountPct
        ) output Inserted.RptPrintQuotationID INTO @@TEMPID(RptPrintQuotationID)
VALUES  ( @0 , -- OrderNo - varchar(100)
          @1 , -- AdminFee - decimal
          @2 , -- PADRVRTSI - numeric
          @3 , -- PAPASSTSI - numeric
          @4 , -- TPLTSI - numeric
          @5 , -- InsuranceType - int
          @6 , -- QuotationNo - varchar(100)
          @7 , -- YearCoverage - int
          @8 , -- InsuredAddress - varchar(255)
          @9 , -- CompanyAddress - varchar(255)
          @10 , -- PolicyNo - varchar(16)
          @11 , -- IsCompany - bit
          @12 , -- CompanyName - varchar(100)
          @13 , -- InsuredName - varchar(100)
          @14 , -- PICName - varchar(100)
          @15 , -- InsuredPhone - varchar(16)
          @16 , -- PICPhoneNo - varchar(16)
          @17 , -- InsuredEmail - varchar(512)
          @18 , -- PICEmail - varchar(150)
          @19 , -- PeriodFrom - datetime
          @20 , -- PeriodTo - datetime
          @21 , -- SalesOfficerID - varchar(20)
          @22 , -- BranchCode - varchar(6)
          @23 , -- SalesmanName - varchar(100)
          @24 , -- SalesmanPhone - varchar(16)
          @25 , -- SalesmanEmail - varchar(150)
          @26 , -- SalesmanRole - varchar(50)
          @27 , -- InsuredObject - varchar(512)
          @28 , -- VehicleYear - char(4)
          @29 , -- PassSeat - int
          @30 , -- ChasisNumber - varchar(30)
          @31 , -- EngineNumber - varchar(30)
          @32 , -- Usage - varchar(30)
          @33 , -- RegistrationNumber - varchar(16)
          @34 , -- GeoArea - varchar(512)
          @35 , -- VAPermata - varchar(35)
          @36 , -- VAMandiri - varchar(35)
          @37 , -- VABCA - varchar(35)
          @38 , -- ProductType - varchar(10)
          @39 , -- City - varchar(100)
          @40 , -- AccountNo - varchar(100)
          @41 , -- BankDescription - varchar(300)
          @42 , -- BankName - varchar(300)
          @43 , -- NoCover - varchar(3000)
          @44 , -- OriginalDefect - varchar(3000)
          @45 , -- NonStandardAcc - varchar(3000)
          @46  -- DiscountPct - numeric
        )

     SELECT TOP 1 RptPrintQuotationID FROM @@TEMPID;";
                string RptPrintQuotationID = db.ExecuteScalar<string>(qInsertRpt, orderNo, mdl.AdminFee, mdl.PADRVRTSI, mdl.PAPASSTSI
                    , mdl.TPLTSI, mdl.InsuranceType, mdl.QuotationNo, mdl.YearCoverage, mdl.InsuredAddress, mdl.CompanyAddress
                    , mdl.PolicyNo, mdl.IsCompany, mdl.CompanyName, mdl.InsuredName, mdl.PICName, mdl.InsuredPhone, mdl.PICPhoneNo, mdl.InsuredEmail
                    , mdl.PICEmail, mdl.PeriodFrom, mdl.PeriodTo, mdl.SalesOfficerID, mdl.BranchCode, mdl.SalesmanName, mdl.SalesmanPhone
                    , mdl.SalesmanEmail, mdl.SalesmanRole, mdl.InsuredObject, mdl.VehicleYear, mdl.PassSeat, mdl.ChasisNumber, mdl.EngineNumber
                    , mdl.Usage, mdl.RegistrationNumber, mdl.GeoArea, mdl.VAPermata, mdl.VAMandiri, mdl.VABCA, mdl.ProductType
                    , mdl.City, mdl.AccountNo, mdl.BankDescription, mdl.BankName, mdl.NoCover, mdl.OriginalDefect, mdl.NonStandardAcc
                    , mdl.DiscountPct);

                string qInsertRPTDetail = @"INSERT INTO RptPrintQuotationDetail ([RptPrintQuotationID]
      ,[Year]
      ,[BasicCoverName]
      ,[TSICasco]
      ,[TSIAccess]
      ,[TSITotal]
      ,[Rate]
      ,[Loading]
      ,[BasicPremi]
      ,[SRCCPremi]
      ,[TRSPremi]
      ,[TPLPremi]
      ,[PADRVRPremi]
      ,[PAPASSPremi]
      ,[ExtendedPremi]
      ,[TotalPremi]
      ,[NettPremiAmount]
      ,[IsBundling]
      ,[IsBundlingTRS]
      ,[DiscountAmount])
VALUES(@0,@1,@2,@3,@4,@5,@6,@7,@8,@9,COALESCE(@10,0),@11,@12,@13,@14,@15,@16,@17,@18,@19)";
                foreach (Otosales.Models.DetailCoverage item in rs.dtlCoverage)
                {
                    db.Execute(qInsertRPTDetail, RptPrintQuotationID, item.Year, item.BasiCoverName, item.VehiclePremi, item.AccessPremi, item.VehicleAccessPremi, item.Rate / 100, item.LoadingPremi / 100, item.BasicPremi, item.SFEPremi, item.TSPremi, item.TPLCoverage, item.PADRVRCoverage, item.PAPASSCoverage, item.PremiPerluasan, item.PremiDasarPerluasan, item.TotalPremi, item.IsBundling, item.IsBundlingTRS, Discount);
                }

                #endregion
            }
            catch (Exception e)
            {
                logger.ErrorFormat(e.ToString());
                throw e;
            }
        }

        public A2isCommonResult SendQuotationEmail(string OrderNo, string Email)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            A2isCommonResult rslt = new A2isCommonResult();
            try
            {
                var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                string EmailSubject = ConfigurationManager.AppSettings["EmailSubject"].ToString();
                List<dynamic> lsOrd = new List<dynamic>();
                List<dynamic> lsVhc = new List<dynamic>();

                Thread tOrd = new Thread(() =>
                {
                    try
                    {
                        var db1 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string DSOrder = "select a.OrderNo, a.EntryDate,b.Name 'CustName',b.Phone1 + case when isnull(b.Phone2,'')='' then'' else '/' + b.Phone2 end 'CustPhone', ";
                        DSOrder += " b.Email1 'CustEmail', so.Name 'SalesName', sr.Description 'SalesRole',sr.Role, so.Phone1 + case when isnull(so.Phone2,'')='' then'' else '/' + so.Phone2 end 'SalesPhone',so.Email 'SalesEmail', so.Fax 'SalesFax',";
                        DSOrder += " so.OfficePhone 'SalesOfficePhone', so.Ext 'SalesOfficePhoneExt', a.QuotationNo, ab.AccountName, ab.AccountNo, ab.Bank, ab.Description 'Branch', ab.City 'BrachCity',";
                        DSOrder += " d.Sequence 'Region', cast(isnull(a.TLOPeriod,0)+isnull(a.ComprePeriod,0) as varchar(10)) + ' Tahun' 'Period',";
                        DSOrder += " a.MultiYearF, case when isnull(a.TLOPeriod,0)>0 and isnull(a.ComprePeriod,0)=0 then 'TLO'";
                        DSOrder += " when  isnull(a.TLOPeriod,0)=0 and isnull(a.ComprePeriod,0)>0 then 'Comprehensive'";
                        DSOrder += " else 'Kombinasi' end 'Pertanggungan', CAST(DAY(GETDATE()) AS VARCHAR(2)) + ' ' + DATENAME(MONTH, GETDATE()) + ' ' + CAST(YEAR(GETDATE()) AS VARCHAR(4)) AS [ExecTime], a.InsuranceType, pt.Description AS Product";
                        DSOrder += " from OrderSimulation a";
                        DSOrder += " inner join ProspectCustomer b on a.CustID=b.CustID";
                        DSOrder += " inner join SalesOfficer so on a.SalesOfficerID=so.SalesOfficerID";
                        DSOrder += " inner join SysUserRole sr on so.Role=sr.Role";
                        DSOrder += " inner join AABBranch ab on so.BranchCode=ab.BranchCode";
                        DSOrder += " inner join OrderSimulationMV c on a.OrderNo=c.OrderNo";
                        DSOrder += " inner join Region d on c.CityCode=d.RegionCode";
                        DSOrder += " inner join ProductType pt on pt.InsuranceType=a.InsuranceType";
                        DSOrder += " where a.OrderNo=@0";

                        lsOrd = db1.Fetch<dynamic>(DSOrder, OrderNo);
                    }
                    catch (Exception e)
                    {

                        //throw;
                    }
                });

                Thread tVehicle = new Thread(() =>
                {
                    try
                    {
                        var db2 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
                        string DSVehicle = "select  c.Description + ' ' + d.Description + ' ' + e.Series + ' ' + b.Year 'VehicleDescription',";
                        DSVehicle += " a.RegistrationNumber, a.Year, f.Description 'Usage', a.sitting, a.SumInsured, o.YearCoverage,c.Description AS Brand, d.Description AS Model,b.Type,vt.Description AS TypeDesc";
                        DSVehicle += " from OrderSimulation o ";
                        DSVehicle += " inner join OrderSimulationMV a on a.OrderNo=o.OrderNo";
                        DSVehicle += " inner join Vehicle b on a.VehicleCode=b.VehicleCode and a.BrandCode=b.BrandCode and a.ProductTypeCode=b.ProductTypeCode";
                        DSVehicle += " and a.ModelCode=b.ModelCode and a.Series=b.Series and a.Type=b.Type and a.Sitting=b.Sitting and a.Year=b.Year";
                        DSVehicle += " inner join VehicleBrand c on a.BrandCode=c.BrandCode";
                        DSVehicle += " inner join VehicleModel d on a.BrandCode=d.BrandCode and a.ModelCode=d.ModelCode";
                        DSVehicle += " inner join VehicleSeries e on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series";
                        DSVehicle += " inner join VehicleType vt on a.BrandCode=e.BrandCode and a.ModelCode=e.ModelCode and a.Series=e.Series AND vt.VehicleTypeCode=a.Type";
                        DSVehicle += " inner join Dtl_Ins_Factor f on a.UsageCode=f.insurance_code and f.Factor_Code='MVUSAG'";
                        DSVehicle += " where a.OrderNo=@0";

                        lsVhc = db2.Fetch<dynamic>(DSVehicle, OrderNo);
                    }
                    catch (Exception e)
                    {

                        //throw e;
                    }
                });

                tOrd.Start();
                tVehicle.Start();
                tOrd.Join();
                tVehicle.Join();

                if (lsOrd.Count > 0 && lsVhc.Count > 0)
                {
                    dynamic dtOrder = lsOrd.FirstOrDefault();
                    dynamic dtVehicle = lsVhc.FirstOrDefault();

                    string NameCust = dtOrder.CustName;
                    string NameProduct = dtOrder.Product;
                    string PeriodePertanggungan = dtOrder.Period;
                    string NamaSales = dtOrder.SalesName;
                    string NomorSales = dtOrder.SalesPhone;
                    string EmailSales = dtOrder.SalesEmail;
                    string Brand = dtVehicle.Brand;
                    string Model = dtVehicle.Model;
                    string Type = dtVehicle.TypeDesc;
                    string Kontak1 = ConfigurationManager.AppSettings["ContactSales" + dtOrder.Role].ToString();
                    Kontak1 = Kontak1.Replace("@Nama", NamaSales).Replace("@Email", EmailSales).Replace("@Phone", NomorSales);

                    string Kontak2 = ConfigurationManager.AppSettings["ContactInfo" + dtOrder.InsuranceType].ToString();
                    //int yearCoverage = Convert.ToInt32(dtVehicle.YearCoverage);
                    int insuranceType = Convert.ToInt32(dtOrder.InsuranceType);
                    List<A2isMessagingAttachment> attFile = new List<A2isMessagingAttachment>();
                    // int yearCoverage = 1;
                    attFile.Add(GenerateReportFile(OrderNo, insuranceType));

                    EmailSubject = EmailSubject.Replace("@Product", dtOrder.Product).Replace("@Customer", NameCust);
                    if (!string.IsNullOrEmpty(EmailSubject))
                    {
                        string EmailBody = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='CONTENT-QUOTATIONEMAIL' AND ParamType='TMP'");
                        string emailText = EmailBody.Replace("@Product", NameProduct).Replace("@Contact1", Kontak1).Replace("@Contact2", Kontak2).Replace("@Brand", Brand).Replace("@Type", Type).Replace("@Model", Model);
                        rslt = Controllers.vWeb2.MessageController.SendEmail(Email, "", "", emailText, EmailSubject, attFile);

                    }
                    if (rslt.ResultCode)
                    {
                        string query = @"UPDATE ORDERSIMULATION SET SendStatus=1, SendDate=GETDATE() WHERE OrderNo=@0";
                        db.Execute(query, OrderNo);
                    }
                }
                return rslt;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }
        public A2isMessagingAttachment GenerateReportFile(string orderNo, int insuranceType)
        {

            A2isMessagingAttachment att = new A2isMessagingAttachment();
            string filePath = "";
            string ReportServerURL = ConfigurationManager.AppSettings["ReportServerURL"].ToString();
            string ReportServerURLBeyond = ConfigurationManager.AppSettings["ReportServerURLBeyond"].ToString();
            string ReportPathOne = ConfigurationManager.AppSettings["ReportPathOne"].ToString();
            string ReportPathTinsOne = ConfigurationManager.AppSettings["ReportPathTinsOne"].ToString();
            string ReportPathLexusOne = ConfigurationManager.AppSettings["ReportPathLexusOne"].ToString();
            string ReportPathTwo = ConfigurationManager.AppSettings["ReportPathTwo"].ToString();
            string ReportPathTinsTwo = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusTwo = ConfigurationManager.AppSettings["ReportPathLexusTwo"].ToString();
            string ReportPathThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathTinsThree = ConfigurationManager.AppSettings["ReportPathTinsTwo"].ToString();
            string ReportPathLexusThree = ConfigurationManager.AppSettings["ReportPathLexusThree"].ToString();
            string CertificateQuotation = ConfigurationManager.AppSettings["CertificateQuotation"].ToString();
            string CertificateRenewalNotice = ConfigurationManager.AppSettings["CertificateRenewalNotice"].ToString();
            string CertificateQuotationGODIG = ConfigurationManager.AppSettings["CertificateQuotationGODIG"].ToString();

            string CredentialsUsername = ConfigurationManager.AppSettings["CredentialsUsername"].ToString(); ;
            string CredentialsPassword = ConfigurationManager.AppSettings["CredentialsPassword"].ToString(); ;
            string CredentialsDomain = ConfigurationManager.AppSettings["CredentialsDomain"].ToString(); ;
            string CredentialsRenewalUsername = ConfigurationManager.AppSettings["CredentialsRenewalUsername"].ToString(); ;
            string CredentialsRenewalPassword = ConfigurationManager.AppSettings["CredentialsRenewalPassword"].ToString(); ;
            string CredentialsRenewalDomain = ConfigurationManager.AppSettings["CredentialsRenewalDomain"].ToString(); ;

            string ReportFiles = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + @"\ReportFiles\";
            int idReportRennot = 0;

            byte[] bytes;
            Warning[] warnings;
            string mimeType;
            string encoding;
            string filenameExtension;
            string[] streamids;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            int yearCoverage;
            ReportViewer rView = new ReportViewer();
            rView.ProcessingMode = ProcessingMode.Remote;
            ServerReport serverReport = rView.ServerReport;
            Models.v0219URF2019.DownloadQuotationResult result = new Models.v0219URF2019.DownloadQuotationResult();
            NetworkCredential NetCredential = new NetworkCredential();

            string qGetDT = @"SELECT o.PolicyNo, p.Name, o.InsuranceType, o.YearCoverage, CAST(COALESCE(IsRenewal,0) AS BIT) IsRenewal,
                        CASE 
	                        WHEN f.referenceno IS NOT NULL AND LEN(f.referenceno) > 10
		                        THEN 1
	                        ELSE 0
                        END AS
	                        IsMVGodig
                        FROM dbo.ProspectCustomer p
                        INNER JOIN dbo.FollowUp f 
                        ON f.CustID = p.CustID
                        INNER JOIN dbo.OrderSimulation o
                        ON o.FollowUpNo = f.FollowUpNo
                        INNER JOIN dbo.OrderSimulationMV omv
                        ON omv.OrderNo = o.OrderNo
                        WHERE o.OrderNo = @0";
            List<dynamic> dt = db.Fetch<dynamic>(qGetDT, orderNo);
            string qGetPlcyNo = @"SELECT os.OldPolicyNo, os.SalesOfficerID, CAST(IIF(LEN(f.ReferenceNo)>10,1,0) AS BIT) IsMVGodig 
FROM OrderSimulation os INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo 
WHERE OrderNo = @0 ANd os.RowStatus = 1 AND f.RowStatus = 1";
            dynamic policyNo = "";
            using (a2isDBHelper.Database dbMobile = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
            {
                policyNo = dbMobile.FirstOrDefault<dynamic>(qGetPlcyNo, orderNo);
            }

            Thread tRenewalData = new Thread(() =>
            {
                try
                {
                    #region Insert Data to RPT_Renewal_Notice Dan RPT_Renewal_Notice_Detail
                    if (!policyNo.IsMVGodig)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(policyNo.OldPolicyNo)))
                        {
                            string qspRPT = @";EXECUTE [dbo].[usp_InsertPrintRenewalNoticeOtosales] @0, @1";
                            using (a2isDBHelper.Database dbAAB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                            {
                                idReportRennot = dbAAB.FirstOrDefault<int>(qspRPT, Convert.ToString(policyNo.OldPolicyNo), Convert.ToString(policyNo.SalesOfficerID));
                                InsertPrintRennotExtended(idReportRennot, orderNo);
                            }
                        }
                        else
                        {

                            InsertQuotationPrintTable(orderNo);
                        }
                    }
                    else
                    {
                        using (var db1 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE))
                        {
                            string qSpTele = @";EXEC dbo.usp_InsertDataOtosalesForQuotationTelesales @0";
                            db1.Execute(qSpTele, orderNo);
                        }
                    }
                    #endregion
                }
                catch (Exception e)
                {

                    //throw;
                }
            });

            Thread tRview = new Thread(() =>
            {
                try
                {
                    if (dt.Count > 0)
                    {
                        yearCoverage = Convert.ToInt32(dt.First().YearCoverage);
                        insuranceType = Convert.ToInt32(dt.First().InsuranceType);

                        if (!dt.First().IsRenewal)
                        {
                            if (dt.First().IsMVGodig == 1) //Generate Quotation With RDL 0219URF2019
                            {
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotationGODIG;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }
                            else
                            {                      //Generate Quotation With Old RDL
                                serverReport.ReportServerUrl = new Uri(ReportServerURL);
                                serverReport.ReportPath = CertificateQuotation;
                                NetCredential = new NetworkCredential(CredentialsUsername, CredentialsPassword, CredentialsDomain);
                            }


                        }
                        else
                        {
                            serverReport.ReportServerUrl = new Uri(ReportServerURLBeyond);
                            serverReport.ReportPath = CertificateRenewalNotice;
                            NetCredential = new NetworkCredential(CredentialsRenewalUsername, CredentialsRenewalPassword, CredentialsRenewalDomain);
                        }

                        serverReport.ReportServerCredentials.NetworkCredentials = NetCredential;

                        ReportParameter[] parameters = new ReportParameter[1];
                        ReportParameter reportParam = new ReportParameter();
                        if (!dt.First().IsRenewal)
                        {
                            reportParam.Name = "OrderNo";
                            reportParam.Values.Add(orderNo);
                        }
                        else
                        {

                            reportParam.Name = "PolicyNo";
                            reportParam.Values.Add(Convert.ToString(policyNo.OldPolicyNo));
                        }
                        parameters[0] = reportParam;
                        rView.ServerReport.SetParameters(parameters);

                    }
                }
                catch (Exception ex)
                {
                    filePath = "";
                    string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();
                }
            });

            tRenewalData.Start();
            tRview.Start();
            tRenewalData.Join();
            tRview.Join();

            try
            {
                if (dt.Count > 0)
                {
                    #region Catat Print Quotation
                    string qInsertLog = @"INSERT INTO dbo.PrintQuotationHistoryLog
                                        ( OrderNo, RowStatus, EntryDate )
                                VALUES  ( @0, -- OrderNo - varchar(100)
                                          1, -- RowStatus - smallint
                                          GETDATE()  -- EntryDate - datetime
                                          )";
                    db.Execute(qInsertLog, orderNo);
                    #endregion

                    bytes = rView.ServerReport.Render("PDF", null, out mimeType, out encoding, out filenameExtension, out streamids, out warnings);

                    #region filename
                    int cetakanke = db.ExecuteScalar<int>("SELECT COUNT(*) FROM dbo.PrintQuotationHistoryLog WHERE OrderNo=@0", orderNo) + 1;

                    string fileName = db.ExecuteScalar<string>("SELECT ParamValue FROM ApplicationParameters WHERE ParamName='ATTFILENAME' AND ParamType='TMP' AND ParamValue2=@0", Convert.ToInt16(dt.First().IsRenewal));
                    fileName = fileName.Replace("@CetakanKe", cetakanke.ToString()).Replace("@TanggalCetak", DateTime.Now.ToString("dd-MM-yyyy"));
                    if (dt.First().IsRenewal)
                    {
                        fileName = fileName.Replace("@NoPolis", dt.First().PolicyNo.Trim());

                    }
                    #endregion
                    filePath = ReportFiles + fileName + ".pdf"; //"QUOTATION-" + OrderNo + ".pdf";

                    if (File.Exists(filePath))
                        File.Delete(filePath);

                    att.FileByte = bytes;
                    att.FileContentType = "application/pdf";
                    att.FileExtension = ".pdf";
                    att.AttachmentDescription = fileName;
                }
            }
            catch (Exception ex)
            {
                filePath = "";
                string errorMessage = DateTime.Now.ToString("[dd-MM-yyyy HH:mm:ss]") + " Error when generate quotation. Message : " + ex.Message.ToString();
            }

            return att;
        }

        public bool SendViaWA(Models.v0219URF2019.SendViaWAParam param)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var mblDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                if (param.ListAtt == null)
                {
                    param.ListAtt = new List<A2isMessagingAttachment>();
                }
                if (param.IsAddAtt)
                {
                    string qGetInsType = @"SELECT COALESCE(InsuranceType,0) InsuranceType FROM dbo.OrderSimulation WHERE OrderNo = @0";
                    int insuranceType = mblDB.ExecuteScalar<int>(qGetInsType, param.OrderNo);
                    param.ListAtt.Add(GenerateReportFile(param.OrderNo, insuranceType));
                }
                string qGetData = @";SELECT os.PolicyOrderNo, CASE WHEN COALESCE(isCompany,0) = 0 THEN pc.Name ELSE pcy.CompanyName END Name, 
mf.FollowUpTypeDes OrderType, 
os.SalesOfficerID, pc.Phone1 FROM dbo.OrderSimulation os
INNER JOIN dbo.ProspectCustomer pc ON pc.CustID = os.CustID
LEFT JOIN dbo.ProspectCompany pcy ON pcy.CustID = os.CustID
INNER JOIN dbo.FollowUp f ON f.FollowUpNo = os.FollowUpNo
INNER JOIN dbo.MstFollowUpType mf ON mf.FollowUpTypeID = f.FollowUpTypeID
WHERE os.OrderNo = @0 AND os.RowStatus = 1 AND os.ApplyF = 1";
                List<dynamic> ordData = mblDB.Fetch<dynamic>(qGetData, param.OrderNo);
                if (ordData.Count > 0)
                {
                    string AttachmentID = "";
                    if (param.ListAtt.Count > 0)
                    {
                        int ac = 0;
                        foreach (A2isMessagingAttachment a in param.ListAtt)
                        {
                            if (a.FileByte != null)
                            {
                                ac++;
                            }
                        }
                        if (ac > 0)
                        {
                            AttachmentID = System.Guid.NewGuid().ToString();
                        }
                    }
                    string qInsertWAmsg = @"
INSERT INTO dbo.WAMessaging
        ( OrderNo ,
          PolicyOrderNo ,
          CustomerName ,
          PhoneNo ,
          OrderType ,
          SalesOfficerID ,
          TextMessage ,
          RowStatus ,
		  AttachmentID,
          CreatedBy ,
          CreatedDate
        )
VALUES  ( @0 , -- OrderNo - varchar(100)
          @1 , -- PolicyOrderNo - varchar(16)
          @2 , -- CustomerName - varchar(100)
          @3 , -- PhoneNo - varchar(30)
          @4 , -- OrderType - varchar(10)
          @5 , -- SalesOfficerID - varchar(16)
          @6 , -- TextMessage - varchar(max)
          1 , -- RowStatus - bit
		  @7, -- AttachmentID - varchar(100)
          @5 , -- CreatedBy - varchar(50)
          GETDATE()  -- CreatedDate - datetime
        )";
                    string qInsertWAAttMsg = @";INSERT INTO dbo.WAMessagingAttachment
        ( AttachmentID ,
          AttachmentDescription ,
          FileByte ,
          FileExtension ,
          FileContentType ,
          CreatedBy ,
          CreatedDate ,
          RowStatus
        )
VALUES  ( @0 , -- AttachmentID - varchar(100)
          @1 , -- AttachmentDescription - varchar(255)
          @2 , -- FileByte - varbinary(max)
          @3 , -- FileExtension - varchar(10)
          @4 , -- FileContentType - varchar(max)
          @5 , -- CreatedBy - varchar(50)
          GETDATE(),  -- CreatedDate - datetime
          1  -- RowStatus - smallint
        )";
                    mblDB.Execute(qInsertWAmsg, param.OrderNo, ordData.First().PolicyOrderNo, ordData.First().Name
                        , param.PhoneNo, ordData.First().OrderType, ordData.First().SalesOfficerID, param.WAMessage, AttachmentID);
                    foreach (A2isMessagingAttachment att in param.ListAtt)
                    {
                        if (att.FileByte != null)
                        {
                            mblDB.Execute(qInsertWAAttMsg
                                , AttachmentID
                                , att.AttachmentDescription
                                , att.FileByte
                                , att.FileExtension
                                , att.FileContentType
                                , ordData.First().SalesOfficerID);
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }


        public A2isCommonResult SendViaEmail(string EmailTo, string EmailCC, string EmailBCC
    , string body, string subject, List<A2isMessagingAttachment> att, string OrderNo, bool IsAddAtt)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            A2isCommonResult res = new A2isCommonResult();
            try
            {
                string qGetInsType = @"SELECT COALESCE(InsuranceType,0) InsuranceType FROM dbo.OrderSimulation WHERE OrderNo = @0";
                int insuranceType = db.ExecuteScalar<int>(qGetInsType, OrderNo);
                if (att == null)
                {
                    att = new List<A2isMessagingAttachment>();
                }
                if (IsAddAtt)
                {
                    att.Add(GenerateReportFile(OrderNo, insuranceType));
                }
                res = Controllers.vWeb2.MessageController.SendEmail(EmailTo, EmailCC, EmailBCC, body, subject, att);
                return res;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

        public void GenerateOrderSimulation(string salesOfficerID, string custID, string followUpID, string orderID, string PolicyNo, string remarkToSA, int isPSTB)
        {
            //Param
            string SOBranchID = "";
            //Data
            Otosales.Models.vWeb2.CustomerInfo custInfo = new Otosales.Models.vWeb2.CustomerInfo();
            Models.vWeb2.OrderSimulationRenot orderSimulation = new Models.vWeb2.OrderSimulationRenot();
            Models.vWeb2.OrderSimulationMVRenot orderSimulationMV = new Models.vWeb2.OrderSimulationMVRenot();
            List<Models.vWeb2.OrderSimulationInterestRenot> listOSInterest = new List<Models.vWeb2.OrderSimulationInterestRenot>();
            List<Models.vWeb2.OrderSimulationCoverageRenot> listOSCoverage = new List<Models.vWeb2.OrderSimulationCoverageRenot>();
            int SalesmanCode = 0;
            int DealerCode = 0;
            int salesmancode = 0;
            int dealercode = 0;
            bool isRenot = false;
            try
            {

                //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                string qgetSO = @"SELECT TOP 1 BranchCode FROM SalesOfficer WHERE SalesOfficerID = @0";
                using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
                {
                    SOBranchID = db.FirstOrDefault<String>(qgetSO, salesOfficerID);
                }
                using (var aabDB = new a2isDBHelper.Database("AAB"))
                {
                    int isRN = aabDB.ExecuteScalar<int>(@"SELECT COUNT(1) FROM dbo.Renewal_Notice WHERE Policy_No = @0 AND Status IN ('0', '1' )", PolicyNo);
                    if (isRN>0)
                    {
                        isRenot = true;
                    }
                }
                Thread tGetCustomer = new Thread(() =>
                {
                    try
                    {
                        #region Query Prospect Customer
                        a2isDBHelper.Database db1 = new a2isDBHelper.Database("AAB");
                        if (isRenot)
                        {
                            string qGetCustRN = @";
DECLARE @@PolicyHC VARCHAR(11)
DECLARE @@ProspectID VARCHAR(11)
DECLARE @@UserIdOtosales VARCHAR(20)
DECLARE @@NPWPAddress VARCHAR(255)
DECLARE @@NPWPNo VARCHAR(255)
DECLARE @@NPWPDate VARCHAR(255)
DECLARE @@PICNo VARCHAR(255)
DECLARE @@PICName VARCHAR(255)
DECLARE @@HP VARCHAR(16)
DECLARE @@HP2 VARCHAR(16)
DECLARE @@IdCard VARCHAR(16)
DECLARE @@Sex VARCHAR(1)

SELECT @@PolicyHC=Policy_Holder_Code FROM dbo.Policy WHERE Policy_No = @0
SELECT @@ProspectID=Prospect_Id FROM dbo.Mst_Prospect WHERE Cust_Id = @@PolicyHC
SELECT @@UserIdOtosales=User_Id_Otosales FROM dbo.Renewal_Notice rn
INNER JOIN dbo.Mst_Salesman ms
ON ms.Salesman_Id = rn.Salesman_Id
WHERE Policy_No = @0
SELECT @@HP=HP, @@HP2=HP_2, @@IdCard=id_card, @@Sex=Sex
FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @@PolicyHC
SELECT @@NPWPAddress=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'NPAD'
SELECT @@NPWPNo=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'NPWN'
SELECT @@NPWPDate=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'NPDT'
SELECT @@PICNo=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'MPC1'
SELECT @@PICName=Name FROM dbo.Mst_Cust_Pic mpc WHERE Cust_Id = @@PolicyHC

SELECT mc.Name AS Name,
		    mc.Email AS Email1,
		    @@HP AS Phone1,
		    @@HP2 AS Phone2,
		    mc.Cust_Id AS CustIDAAB,
		    CASE WHEN mpr.product_type = 6 
		    THEN 
		    CASE WHEN rn.broker_code is null THEN CASE WHEN @@UserIdOtosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(@@UserIdOtosales)) END 
		    ELSE rn.broker_code
		    END
		    ELSE
            CASE WHEN @@UserIdOtosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(@@UserIdOtosales)) END 
		    END AS SalesOfficerID,
		    rn.Salesman_Dealer_Code AS SalesDealer,
		    rn.Dealer_Code AS DealerCode,
		    rn.Branch_distribution AS BranchCode,
		    CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany, 
		    isnull(@@IdCard,'') AS IdentityNo,
		    CASE WHEN isnull(@@Sex , '') = '' THEN '' 
		    WHEN isnull(@@Sex , '') = '1' THEN 'M'
		    ELSE 'F' END AS Gender,
		    @@ProspectID AS ProspectID,
		    mc.Birth_Date AS BirthDate,
		    CASE WHEN mc.Cust_Type = 2 THEN mc.Office_Address ELSE mc.Home_Address END AS [Address],
		    CASE WHEN mc.Cust_Type = 2 THEN mc.Office_PostCode ELSE mc.Home_PostCode END AS PostCode,
		    isnull(@@NPWPAddress,'') AS CompanyNPWPAddress,
		    isnull(@@NPWPNo,'') AS CompanyNPWPNo,
		    CASE WHEN @@NPWPDate is not null AND @@NPWPDate <> '' 
				THEN SUBSTRING(@@NPWPDate, 7, 4)+'-'+SUBSTRING(@@NPWPDate, 4, 2)+'-'+SUBSTRING(@@NPWPDate, 1, 2) 
				ELSE NULL 
			END  AS CompanyNPWPDate,
			CASE WHEN mc.Cust_Type = 2 THEN ISNULL(@@PICNo,'') 
			ELSE '' END AS PICPhone,
			CASE WHEN mc.Cust_Type = 2 THEN ISNULL(@@PICName,'') 
			ELSE '' END AS PIC,
            a.order_no AS OrderNo
    FROM Policy AS a WITH(NOLOCK)
    INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
    INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
    INNER JOIN mst_product mpr WITH ( NOLOCK ) ON mpr.product_code = rn.new_product_code
    WHERE a.policy_no = @0
		    AND a.Status = 'A'
            AND a.ORDER_STATUS IN ( '11','9' )
            AND a.RENEWAL_STATUS = 0
            AND rn.Status IN ('0', '1' )
            AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            custInfo = db1.FirstOrDefault<Otosales.Models.vWeb2.CustomerInfo>(qGetCustRN, PolicyNo);

                        }
                        else
                        {
                            string qGetCustp = @";
DECLARE @@PolicyHC VARCHAR(11)
DECLARE @@ProspectID VARCHAR(11)
DECLARE @@UserIdOtosales VARCHAR(20)
DECLARE @@NPWPAddress VARCHAR(255)
DECLARE @@NPWPNo VARCHAR(255)
DECLARE @@NPWPDate VARCHAR(255)
DECLARE @@PICNo VARCHAR(255)
DECLARE @@PICName VARCHAR(255)
DECLARE @@HP VARCHAR(16)
DECLARE @@HP2 VARCHAR(16)
DECLARE @@IdCard VARCHAR(16)
DECLARE @@Sex VARCHAR(1)
DECLARE @@BrokerCode VARCHAR(11)
DECLARE @@SalesDealer VARCHAR(11)
DECLARE @@NewBranchID VARCHAR(3)
DECLARE @@ProductCode VARCHAR(5)
DECLARE @@BrnahcID VARCHAR(3)
DECLARE @@SegmentCode VARCHAR(6)

SELECT @@PolicyHC=Policy_Holder_Code,@@BrokerCode=Broker_Code FROM dbo.Policy WHERE Policy_No = @0
SELECT @@ProspectID=Prospect_Id FROM dbo.Mst_Prospect WHERE Cust_Id = @@PolicyHC
SELECT @@UserIdOtosales=User_Id_Otosales FROM dbo.Renewal_Notice rn
INNER JOIN dbo.Mst_Salesman ms
ON ms.Salesman_Id = rn.Salesman_Id
WHERE Policy_No = @0
SELECT @@HP=HP, @@HP2=HP_2, @@IdCard=id_card, @@Sex=Sex
FROM dbo.Mst_Cust_Personal WHERE Cust_Id = @@PolicyHC
SELECT @@NPWPAddress=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'NPAD'
SELECT @@NPWPNo=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'NPWN'
SELECT @@NPWPDate=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'NPDT'
SELECT @@PICNo=AdditionalInfoValue FROM dbo.Mst_Customer_AdditionalInfo WHERE CustID = @@PolicyHC AND AdditionalCode = 'MPC1'
SELECT @@PICName=Name FROM dbo.Mst_Cust_Pic mpc WHERE Cust_Id = @@PolicyHC
SELECT @@SalesDealer=mc2.Cust_Id FROM dbo.ComSales_Salesman AS css
INNER JOIN dbo.Mst_Customer AS mc2 ON css.Salesman_Code = mc2.Cust_Id
where css.Salesman_Code = @@BrokerCode
SELECT @@NewBranchID=New_Branch_ID FROM renewal_map rm WHERE rm.Product_Code = @@ProductCode
                                                          AND rm.status = 1
                                                          AND ( rm.old_branch_id = ''
                                                              OR rm.old_branch_id = @@BrnahcID
                                                              )
                                                          AND ( rm.old_segment_code = ''
                                                              OR rm.old_segment_code = @@SegmentCode
                                                              )

SELECT mc.Name AS Name,
        mc.Email AS Email1,
        @@HP AS Phone1,
		@@HP2 AS Phone2,
        mc.Cust_Id AS CustIDAAB,
        CASE WHEN @@UserIdOtosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(@@UserIdOtosales)) END  AS SalesOfficerID,
        a.Broker_Code AS DealerCode,
        @@SalesDealer AS SalesDealer,
        COALESCE(@@NewBranchID, a.branch_id) AS BranchCode,
        CASE WHEN mc.Cust_Type = 2 THEN 1 ELSE 0 END AS isCompany,
        isnull(@@IdCard, '') AS IdentityNo,
        CASE WHEN isnull(@@Sex, '') = '' THEN ''
        WHEN isnull(@@Sex, '') = '1' THEN 'M'
        ELSE 'F' END AS Gender,
		mc.Prospect_Id AS ProspectID,
		mc.Birth_Date AS BirthDate,
		mc.Home_Address AS [Address],
		mc.Home_PostCode AS PostCode,
		isnull(@@NPWPAddress,'') AS CompanyNPWPAddress,
		isnull(@@NPWPNo,'') AS CompanyNPWPNo,
		CASE WHEN @@NPWPDate is not null AND @@NPWPDate <> ''
        THEN SUBSTRING(@@NPWPDate, 7, 4)+'-'+SUBSTRING(@@NPWPDate, 4, 2)+'-'+SUBSTRING(@@NPWPDate, 1, 2) 
        ELSE NULL END  AS CompanyNPWPDate,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(@@PICNo,'') 
		ELSE '' END AS PICPhone,
		CASE WHEN mc.Cust_Type = 2 THEN ISNULL(@@PICName,'') 
		ELSE '' END AS PIC
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc WITH(NOLOCK) ON a.policy_holder_code = mc.cust_id
INNER JOIN mst_product mp ON mp.product_code = a.product_code
WHERE a.policy_no = @0
AND a.Status = 'A'
AND a.ORDER_STATUS IN ( '11','9' )
AND a.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            custInfo = db1.FirstOrDefault<Otosales.Models.vWeb2.CustomerInfo>(qGetCustp, PolicyNo);
                        }
                        #endregion

                        #region Sales Dealer dan Dealer Code
                        string querySalesman = "SELECT SalesmanCode FROM dbo.SalesmanDealer WHERE CustID = @0";
                        string queryDealer = "SELECT DealerCode FROM dbo.Dealer WHERE CustID = @0";
                        a2isDBHelper.Database mdb1 = new a2isDBHelper.Database("AABMobile");
                        if (custInfo.SalesDealer != null)
                            salesmancode = mdb1.First<int>(querySalesman, custInfo.SalesDealer);
                        else
                            salesmancode = 0;
                        int value = 0;
                        if (custInfo.DealerCode != null && int.TryParse(custInfo.DealerCode, out value))
                            dealercode = mdb1.First<int>(queryDealer, custInfo.DealerCode);
                        else
                            dealercode = 0;

                        SalesmanCode = Convert.ToInt32(salesmancode);
                        DealerCode = Convert.ToInt32(dealercode);

                        #endregion
                    }
                    catch (Exception e)
                    {

                        throw (e);
                    }
                });

                Thread tGetOrder = new Thread(() =>
                {
                    try
                    {
                        #region Query Order Simulation
                        //Update 105 Hotfix - ganti period policy ke range 1 tahun
                        a2isDBHelper.Database db2 = new a2isDBHelper.Database("AAB");
                        if (isRenot) {
                            string qGetOSRN = @";
DECLARE @@UserIdOtosales VARCHAR(20)
DECLARE @@CvgId1 VARCHAR(6)
DECLARE @@CvgId2 VARCHAR(6)
DECLARE @@PolicyFee NUMERIC
DECLARE @@GrossPremium NUMERIC
DECLARE @@ProductCode VARCHAR(6)
DECLARE @@PDTProductCode VARCHAR(6)
DECLARE @@Address VARCHAR(512)
DECLARE @@PostalCode VARCHAR(8)
DECLARE @@DeliveryCode VARCHAR(6)
DECLARE @@ContactPerson VARCHAR(30)

SELECT @@UserIdOtosales=User_Id_Otosales,@@ProductCode=rn.New_Product_Code FROM dbo.Renewal_Notice rn
INNER JOIN dbo.Mst_Salesman ms
ON ms.Salesman_Id = rn.Salesman_Id
WHERE Policy_No = @0
SELECT @@CvgId1=Coverage_Id FROM dbo.Ren_Dtl_Coverage WITH ( NOLOCK ) WHERE Policy_No = @0
AND Interest_Id = 'CASCO' AND Coverage_Id = 'TLO'
SELECT @@CvgId2=Coverage_Id FROM dbo.Ren_Dtl_Coverage WITH ( NOLOCK ) WHERE Policy_No = @0
AND Interest_Id = 'CASCO' AND Coverage_Id = 'ALLRIK'
SELECT @@PolicyFee=Policy_Fee, @@GrossPremium=Gross_Premium FROM dbo.Ren_Premium WHERE Policy_No = @0
SELECT @@PDTProductCode=Product_Code FROM dbo.Partner_Document_Template WHERE Product_Code = @@ProductCode AND Partner_ID = 'LEX' 
SELECT @@Address=Address, @@UserIdOtosales=Postal_Code, @@DeliveryCode=Delivery_Code, @@ContactPerson=ContactPerson FROM dbo.Policy a
INNER JOIN dbo.Dtl_Address da on da.Policy_Id = a.Policy_Id AND da.Address_Type ='DELIVR'
WHERE Policy_No = @0

SELECT 
		NULL AS NewPolicyId,
		rn.New_Policy_No AS NewPolicyNo,
		rn.VANumber AS VANumber,
		'' AS QuotationNo,
		0 AS MultiYearF,
		1 AS YearCoverage,
		CASE WHEN @@CvgId1 is null THEN 0 ELSE 1 END AS TLOPeriod,
		CASE WHEN @@CvgId2 is null THEN 0 ELSE 1 END AS ComprePeriod,
		rn.Branch_distribution AS BranchCode,
		CASE WHEN mp.product_type = 6 
		THEN 
		CASE WHEN rn.broker_code is null THEN CASE WHEN @@UserIdOtosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(@@UserIdOtosales)) END 
		ELSE rn.broker_code
		END
		ELSE
        CASE WHEN @@UserIdOtosales is null THEN LTRIM(RTRIM(rn.Salesman_Id)) ELSE LTRIM(RTRIM(@@UserIdOtosales)) END 
		END AS SalesOfficerID,
		'' AS PhoneSales,
		rn.Dealer_Code AS DealerCode,
		rn.Salesman_Dealer_Code AS SalesDealer,
		'GARDAOTO' AS ProductTypeCode,
		@@PolicyFee AS AdminFee,
		@@GrossPremium AS TotalPremium,
		mc.Home_Phone1 AS Phone1,
		mc.Home_Phone2 AS Phone2,
		mc.Email AS Email1,
		'' AS Email2,
		0 AS SendStatus,
		CASE WHEN mp.cob_id = '404' THEN 2 
		WHEN @@PDTProductCode is not null THEN 3 
        WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
        ELSE 1 END AS InsuranceType,
		1 AS ApplyF,
		0 AS SendF,
		(SELECT top 1 Interest_no from Ren_Dtl_Interest where policy_no = @0) AS LastInterestNo,
		(SELECT top 1 coverage_no from Ren_Dtl_Coverage where policy_no = @0) AS LastCoverageNo,
		rn.New_Product_code AS ProductCode,
		CASE WHEN rn.Old_Product_Code = rn.New_Product_code
			THEN rn.MOU_ID
			ELSE rn.New_Product_code
		END MouID,
        rn.Due_Date AS PeriodFrom,
        DATEADD(YEAR, 1,rn.Due_Date) AS PeriodTo,
        rn.segment_code AS SegmentCode,
@@Address AS PolicyDeliverAddress,
@@PostalCode AS PolicyDeliverPostalCode,
@@DeliveryCode AS PolicyDeliveryType,
@@ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN mst_customer mc WITH ( NOLOCK ) ON a.policy_holder_code = mc.cust_id
INNER JOIN mst_product mp on mp.product_code = rn.new_product_code AND mp.Status = 1
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )
";
                            orderSimulation = db2.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(qGetOSRN, PolicyNo);
                        }
                        else
                        {
                            string qGetOSP = @";
DECLARE @@UserIdOtosales VARCHAR(20)
DECLARE @@CvgId1 VARCHAR(6)
DECLARE @@CvgId2 VARCHAR(6)
DECLARE @@PolicyFee NUMERIC
DECLARE @@GrossPremium NUMERIC
DECLARE @@ProductCode VARCHAR(6)
DECLARE @@PDTProductCode VARCHAR(6)
DECLARE @@Address VARCHAR(512)
DECLARE @@PostalCode VARCHAR(8)
DECLARE @@DeliveryCode VARCHAR(6)
DECLARE @@ContactPerson VARCHAR(30)

SELECT @@UserIdOtosales=User_Id_Otosales,@@ProductCode=rn.New_Product_Code FROM dbo.Renewal_Notice rn
INNER JOIN dbo.Mst_Salesman ms
ON ms.Salesman_Id = rn.Salesman_Id
WHERE Policy_No = @0
SELECT @@CvgId1=Coverage_Id FROM dbo.Ren_Dtl_Coverage WITH ( NOLOCK ) WHERE Policy_No = @0
AND Interest_Id = 'CASCO' AND Coverage_Id = 'TLO'
SELECT @@CvgId2=Coverage_Id FROM dbo.Ren_Dtl_Coverage WITH ( NOLOCK ) WHERE Policy_No = @0
AND Interest_Id = 'CASCO' AND Coverage_Id = 'ALLRIK'
SELECT @@PolicyFee=Policy_Fee, @@GrossPremium=Gross_Premium FROM dbo.Ren_Premium WHERE Policy_No = @0
SELECT @@PDTProductCode=Product_Code FROM dbo.Partner_Document_Template WHERE Product_Code = @@ProductCode AND Partner_ID = 'LEX' 
SELECT @@Address=Address, @@PostalCode=Postal_Code, @@DeliveryCode=Delivery_Code, @@ContactPerson=ContactPerson FROM dbo.Policy a
INNER JOIN dbo.Dtl_Address da on da.Policy_Id = a.Policy_Id AND da.Address_Type ='DELIVR'
WHERE Policy_No = @0

SELECT TOP 1
NULL AS NewPolicyId,
@1 AS NewPolicyNo,
@2 AS VANumber,
'' AS QuotationNo,
0 AS MultiYearF,
1 AS YearCoverage,
CASE WHEN @@CvgId1 is null THEN 0 ELSE 1 END AS TLOPeriod,
CASE WHEN @@CvgId2 is null THEN 0 ELSE 1 END AS ComprePeriod,
a.Branch_Id AS BranchCode,
CASE WHEN @@UserIdOtosales is null THEN LTRIM(RTRIM(a.Salesman_Id)) ELSE LTRIM(RTRIM(@@UserIdOtosales)) END  AS SalesOfficerID,
'' AS PhoneSales,
a.Broker_Code AS DealerCode,
        mc.Cust_Id AS SalesDealer,
'GARDAOTO' AS ProductTypeCode,
@@PolicyFee AS AdminFee,
@@GrossPremium AS TotalPremium,
mc.Home_Phone1 AS Phone1,
mc.Home_Phone2 AS Phone2,
mc.Email AS Email1,
'' AS Email2,
0 AS SendStatus,
CASE WHEN mp.cob_id = '404' THEN 2 
WHEN @@PDTProductCode is not null THEN 3 
WHEN mp.biz_type = 2 AND mp.cob_id = '403' THEN 4
ELSE 1 END AS InsuranceType,
1 AS ApplyF,
0 AS SendF,
(SELECT max(Interest_no) from Dtl_Interest where policy_id = a.policy_id) AS LastInterestNo,
(SELECT max(coverage_no) from Dtl_Coverage where policy_id = a.policy_id) AS LastCoverageNo,
a.Product_Code [ProductCode],
		a.Mou_ID MouID,
        a.Period_To AS PeriodFrom,
        DATEADD(YEAR,1,a.Period_To) AS PeriodTo,
a.Segment_Code [SegmentCode],
@@Address AS PolicyDeliverAddress,
@@PostalCode AS PolicyDeliverPostalCode,
@@DeliveryCode AS PolicyDeliveryType,
@@ContactPerson AS PolicyDeliveryName
FROM Policy AS a WITH(NOLOCK)
INNER JOIN mst_customer mc  ON a.policy_holder_code = mc.cust_id
INNER JOIN mst_product mp on mp.product_code = a.product_code AND mp.Status = 1
WHERE a.policy_no = @0
AND a.Status = 'A'
AND a.ORDER_STATUS IN ( '11','9' )
AND a.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            string VANumber = "", NewPolicyNo = "";
                            List<dynamic> listRen = db2.Fetch<dynamic>(@"SELECT VANumber, Policy_No FROM dbo.Renewal_Notice WHERE Policy_No = @0 AND Status IN ('0', '1' )", PolicyNo);
                            if (listRen.Count > 0)
                            {
                                VANumber = listRen.First().VANumber;
                                NewPolicyNo = listRen.First().Policy_No;
                            }
                            else
                            {
                                NewPolicyNo = Otosales.Logic.CommonLogic.GetPolicyNo();
                            }
                            orderSimulation = db2.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationRenot>(qGetOSP, PolicyNo, NewPolicyNo, VANumber);
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {

                        throw (e);
                    }
                });

                Thread tGetMV = new Thread(() =>
                {
                    try
                    {
                        #region Query Order Simulation MV
                        a2isDBHelper.Database db3 = new a2isDBHelper.Database("AAB");
                        if (isRenot)
                        {
                            string qGetMVRN = @"SELECT rdm.Object_No AS ObjectNo,
		'GARDAOTO' AS ProductTypeCode,
		rdm.Vehicle_Code AS VehicleCode,
		rdm.Brand_Code AS BrandCode,
		rdm.Model_Code AS ModelCode,
		rdm.Series  AS Series,
		rdm.Vehicle_Type AS [Type],
		rdm.Sitting_Capacity AS Sitting,
		rdm.Mfg_Year AS [Year],
		rdm.Geo_Area_Code AS CityCode,
		rdm.Usage_Code AS UsageCode,
		rdi.Sum_Insured AS SumInsured,
		ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Ren_Dtl_Non_Standard_Accessories] where policy_no = @0),0) AS AccessSI,
		rdm.Registration_Number AS RegistrationNumber,
		rdm.Engine_Number AS EngineNumber,
		rdm.Chasis_Number AS ChasisNumber,
		rdm.New_Car AS IsNew,
        rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_no = rn.Policy_No
INNER JOIN Ren_Dtl_Interest rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.policy_no 
AND rdi.Object_no = rdm.object_no
AND rdi.interest_id = 'CASCO'
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            orderSimulationMV = db3.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(qGetMVRN, PolicyNo);
                        }
                        else
                        {
                            string qGetMVP = @"SELECT rdm.Object_No AS ObjectNo,
'GARDAOTO' AS ProductTypeCode,
rdm.Vehicle_Code AS VehicleCode,
rdm.Brand_Code AS BrandCode,
rdm.Model_Code AS ModelCode,
rdm.Series  AS Series,
rdm.Vehicle_Type AS [Type],
rdm.Sitting_Capacity AS Sitting,
rdm.Mfg_Year AS [Year],
rdm.Geo_Area_Code AS CityCode,
rdm.Usage_Code AS UsageCode,
rdm.Market_Price AS SumInsured,
ISNULL((SELECT SUM(Sum_Insured) FROM [dbo].[Dtl_Non_Standard_Accessories] where policy_id = a.policy_id),0) AS AccessSI,
rdm.Registration_Number AS RegistrationNumber,
rdm.Engine_Number AS EngineNumber,
rdm.Chasis_Number AS ChasisNumber,
rdm.New_Car AS IsNew,
rdm.Color_On_BPKB AS Color
FROM Policy AS a WITH(NOLOCK)
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'
AND a.ORDER_STATUS IN ( '11','9' )
AND a.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            orderSimulationMV = db3.FirstOrDefault<Otosales.Models.vWeb2.OrderSimulationMVRenot>(qGetMVP, PolicyNo);
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {

                        throw (e);
                    }
                });

                Thread tGetInterest = new Thread(() =>
                {
                    try
                    {
                        #region Query Order Simulation Interest
                        //Update 105 Hotfix - ganti period policy ke range 1 tahun
                        a2isDBHelper.Database db4 = new a2isDBHelper.Database("AAB");
                        if (isRenot)
                        {
                            string qGetInterestRN = @"SELECT rdi.Object_No AS ObjectNo,
		rdi.Interest_No AS InterestNo,
		rdi.Interest_Id AS InterestID,
		1 AS [Year],
		(SELECT SUM(rdc.net_premium) FROM [dbo].[Ren_Dtl_Coverage] rdc WHERE rdc.policy_no = @0 
		and rdc.interest_no = rdi.Interest_No) AS Premium,
        rn.Due_Date AS PeriodFrom,
        DATEADD(YEAR,1,rn.Due_Date) AS PeriodTo,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND a.ORDER_STATUS IN ( '11','9' )
        AND a.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            listOSInterest = db4.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(qGetInterestRN, PolicyNo);
                        }
                        else
                        {
                            string qGetInterestP = @"SELECT rdi.Object_No AS ObjectNo,
rdi.Interest_No AS InterestNo,
rdi.Interest_Id AS InterestID,
1 AS [Year],
(SELECT SUM(rdc.net_premium) FROM [dbo].[Dtl_Coverage] rdc WHERE rdc.policy_id = a.policy_id 
and rdc.interest_no = rdi.Interest_No) AS Premium,
        a.Period_To AS PeriodFrom,
        DATEADD(YEAR,1,a.Period_To) AS PeriodTo,
rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a 
INNER JOIN [Dtl_Interest] rdi ON rdi.policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'
AND A.ORDER_STATUS IN ( '11','9' )
AND A.RENEWAL_STATUS = 0
AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            listOSInterest = db4.Fetch<Models.vWeb2.OrderSimulationInterestRenot>(qGetInterestP, PolicyNo);
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {

                        throw (e);
                    }
                });

                Thread tGetCoverage = new Thread(() =>
                {
                    try
                    {
                        #region Query Order Simulation Coverage
                        //Update 105 Hotfix - ganti period policy ke range 1 tahun
                        a2isDBHelper.Database db5 = new a2isDBHelper.Database("AAB");
                        if (isRenot)
                        {
                            string qGetCvgRN = @"SELECT rdc.Object_No AS ObjectNo,
		rdc.Interest_No AS InterestNo,
		rdc.Coverage_no AS CoverageNo,
		rdc.Coverage_ID AS CoverageID,
		rdc.Rate AS Rate,
		rdi.sum_insured AS SumInsured,
		rdc.Loading AS LoadingRate,
		(rdc.Loading*rdc.net_premium)/100 AS Loading,
		rdc.net_premium AS Premium,
		rn.Due_Date AS BeginDate,
		DATEADD(YEAR, 1,rn.Due_Date) AS EndDate,
		rdc.IsBundlingF AS IsBundling,
		100 AS EntryPct,
		DATEDIFF(day, rn.Due_Date, DATEADD(YEAR, 1,rn.Due_Date)) AS Ndays,
		rdc.Excess_Rate AS ExcessRate,
		CASE WHEN DATEDIFF(MONTH, rn.Due_Date, DATEADD(YEAR, 1,rn.Due_Date)) < 12 
		THEN 1 ELSE 0 END CalcMethod,
		rdc.Cover_Premium AS CoverPremium,
		rdc.Gross_Premium AS GrossPremium,
		rdc.Max_SI AS MaxSI,
		rdc.Net1 AS Net1,
		rdc.Net2 AS Net2,
		rdc.Net3 AS Net3,
		rdi.Deductible_Code AS DeductibleCode
FROM Policy AS a WITH(NOLOCK)
INNER JOIN renewal_notice rn WITH ( NOLOCK ) ON a.policy_no = rn.Policy_No
INNER JOIN [Ren_dtl_Interest] rdi WITH ( NOLOCK ) ON rdi.policy_no = rn.Policy_No
INNER JOIN [Ren_Dtl_Coverage] rdc WITH ( NOLOCK ) ON rdc.policy_no = rn.Policy_No AND rdi.interest_no = rdc.interest_no
WHERE a.policy_no = @0
		AND a.Status = 'A'
        AND A.ORDER_STATUS IN ( '11','9' )
        AND A.RENEWAL_STATUS = 0
        AND rn.Status IN ('0', '1' )
        AND ((a.endorsement_no = 0 and A.ORDER_STATUS IN ('11','9')) or a.endorsement_no <> 0 )";
                            listOSCoverage = db5.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(qGetCvgRN, PolicyNo);
                        }
                        else
                        {
                            string qGetCvgP = @"SELECT rdc.Object_No AS ObjectNo,
rdc.Interest_No AS InterestNo,
rdc.Coverage_no AS CoverageNo,
rdc.Coverage_Id AS CoverageID,
rdc.Rate AS Rate,
rdc.Sum_Insured  AS SumInsured,
rdc.Loading AS LoadingRate,
(rdc.Loading*rdc.net_premium)/100 AS Loading,
rdc.net_premium AS Premium,
        a.Period_To AS BeginDate,
        DATEADD(YEAR,1,a.Period_To) AS EndDate,
CASE WHEN rdc.net_premium = 0 OR rdc.net_premium is null THEN 1 ELSE 0 END AS IsBundling,
rdc.Entry_Pct [EntryPct],
rdc.NDays [NDays],
Net1,
Net2,
Net3,
Gross_Premium [GrossPremium],
Cover_Premium [CoverPremium],
rdc.Calc_Method [CalcMethod],
Excess_Rate [ExcessRate],
Max_si [MaxSi],
Deductible_Code [DeductibleCode]
FROM Policy AS a 
INNER JOIN [Dtl_Coverage] rdc ON rdc.Policy_id = a.Policy_id
INNER JOIN Dtl_Mv rdm WITH ( NOLOCK ) ON rdm.Policy_id = a.Policy_id
WHERE a.policy_no = @0
AND a.Status = 'A'";
                            listOSCoverage = db5.Fetch<Models.vWeb2.OrderSimulationCoverageRenot>(qGetCvgP, PolicyNo);
                            foreach (Models.vWeb2.OrderSimulationCoverageRenot osC in listOSCoverage)
                            {
                                int IsBundling = 0;
                                string interestCode = "";
                                interestCode = db5.ExecuteScalar<string>(@"DECLARE @@PolicyID VARCHAR(20)
SELECT @@PolicyID=Policy_Id FROM dbo.Policy where Policy_No = @0
DECLARE @@InterestID VARCHAR(10) = ''
SELECT @@InterestID = COALESCE(Interest_Id,'') FROM dbo.Dtl_Interest WHERE Policy_Id = @@PolicyID AND Interest_No = @1
IF (@@InterestID <> '' AND @@InterestID IS NOT NULL)
BEGIN 
	SELECT @@InterestID
END
ELSE 
BEGIN
	SELECT @@InterestID = ''
END", PolicyNo, osC.InterestNo);
                                if (interestCode.Contains("CASCO") || interestCode.Contains("ACCESS"))
                                {
                                    string squery = @"SELECT a.Coverage_Id, a.Scoring_Code, b.Auto_Apply, 1 Year  FROM 
                                         Prd_Interest_Coverage a INNER JOIN B2B_Interest_Coverage b ON 
                                         a.Coverage_Id=b.Coverage_Id AND a.Interest_ID=b.Interest_Id AND 
                                         a.Product_Code=b.Product_Code WHERE a.Product_Code=@0
										 AND a.Coverage_Id = @1 
										 AND b.Vehicle_Type='ALL' 
										 AND b.Interest_Id='CASCO'";
                                    List<dynamic> ListTmp = db5.Fetch<dynamic>(squery, orderSimulation.ProductCode, osC.CoverageID);
                                    if (ListTmp.Count > 0)
                                    {
                                        int AutoApply = Convert.ToInt32(ListTmp.First().Auto_Apply);
                                        if (AutoApply > 0)
                                        {
                                            IsBundling = 1;
                                        }
                                    }
                                }
                                osC.IsBundling = IsBundling;
                            }
                        }
                        #endregion
                    }
                    catch (Exception e)
                    {

                        throw (e);
                    }
                });

                Thread tGetImage = new Thread(() =>
                {
                    try
                    {
                        string qImg = @"DELETE FROM [dbo].[ImageData] WHERE DeviceID = @0
                INSERT INTO [dbo].[ImageData]
                ([ImageID],[DeviceID],[SalesOfficerID],[PathFile]
                ,[EntryDate],[LastUpdatedTime],[RowStatus],[Data]
                ,[ThumbnailData],[CoreImage_ID])
                SELECT tid.ImageID,tid.FollowUpNo,tid.SalesOfficerID,tid.PathFile,
                GETDATE(),GETDATE(),1,tid.Data,tid.ThumbnailData,NULL
                FROM dbo.TempImageData tid
                WHERE tid.FollowUpNo = @0";

                        a2isDBHelper.Database dbImg = new a2isDBHelper.Database("AABMobile");
                        dbImg.Execute(qImg, followUpID);

                        /*FORMRENEWAL
                        RENEWALNOTICE
                        BUKTIBAYAR
                        KTP
                        STNK
                        SURVEY
                        DOCNSA1
                        DOCNSA2*/

                    }
                    catch (Exception e)
                    {

                        throw (e);
                    }
                });

                tGetCustomer.Start();
                tGetOrder.Start();
                tGetMV.Start();
                tGetInterest.Start();
                tGetCoverage.Start();
                tGetImage.Start();

                tGetCustomer.Join();
                tGetOrder.Join();
                tGetMV.Join();
                tGetInterest.Join();
                tGetCoverage.Join();
                tGetImage.Join();

                string query = @"";
                #region InserCustFU
                using (a2isDBHelper.Database db11 = new a2isDBHelper.Database("AABMobile"))
                {

                    #region Query Insert Prospect Customer
                    query = @";
INSERT INTO ProspectCustomer([CustID]
      , [Name] --*
      , [Phone1] --*
      , [Phone2]
      , [Email1] --*
      , [Email2]
      , [CustIDAAB] --*
      , [SalesOfficerID] --*
      , [DealerCode] --*
      , [SalesDealer] --*
      , [BranchCode] --*
      , [isCompany] --*
      , [EntryDate]
      , [LastUpdatedTime]
      , [RowStatus]
      , [IdentityNo]
      , [CustGender]      
      , [ProspectID]
      , [CustBirthDay]
      , [CustAddress]
      , [PostalCode])
      SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,GETDATE(),GETDATE(),1,@12,@13,@14,@15,@16,@17

IF(@100=1)
BEGIN
    INSERT INTO ProspectCompany ([CustID]
          ,[CompanyName] --*
          ,[FollowUpNo] --*
          ,[Email] --*
          ,[ModifiedDate]
          ,[RowStatus]
          ,[NPWPno]
          ,[NPWPdate]
          ,[NPWPaddress]
          ,[OfficeAddress]
          ,[PostalCode]
          ,[PICPhoneNo]
          ,[PICname]) VALUES(@18,@19,@20,@21,GETDATE(),1,@22,@23,@24,@25,@26,@27,@28)
END

INSERT INTO FollowUp ([FollowUpNo]
      ,[LastSeqNo]
      ,[CustID]
      ,[ProspectName]
      ,[Phone1]
      ,[Phone2]
      ,[SalesOfficerID]
      ,[EntryDate]
      ,[FollowUpName]
      ,[FollowUpStatus]
      ,[FollowUpInfo]
      ,[RemarkToSA]
      ,[BranchCode]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsRenewal]) 
SELECT @29,@30,@31,@32,@33,@34,@35,GETDATE(),@36,@37,@38,@39,@40,GETDATE(),1,1

INSERT INTO OrderSimulation ([OrderNo]
      ,[CustID]
      ,[FollowUpNo]
      ,[QuotationNo]
      ,[MultiYearF]
      ,[YearCoverage]
      ,[TLOPeriod]
      ,[ComprePeriod]
      ,[BranchCode]
      ,[SalesOfficerID]
      ,[PhoneSales]
      ,[DealerCode]
      ,[SalesDealer]
      ,[ProductTypeCode]
      ,[AdminFee]
      ,[TotalPremium]
      ,[Phone1]
      ,[Phone2]
      ,[Email1]
      ,[Email2]
      ,[SendStatus]
      ,[SendDate]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[InsuranceType]
      ,[ApplyF]
      ,[SendF]
      ,[LastInterestNo]
      ,[LastCoverageNo]
      ,[OldPolicyNo]
      ,[PolicyNo]
      ,[PolicyID]
      ,[VANumber]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[ProductCode]
      ,[SegmentCode]
      ,[PolicyDeliveryAddress]
      ,[PolicyDeliveryPostalCode]
      ,[PolicySentTo]
      ,[PolicyDeliveryName]
	  ,IsPolicyIssuedBeforePaying
	  ,MouID
        ) 
  SELECT @41,@42,@43,@44,@45,@46,@47,@48,@49,@50,@51, @52,@53,
        @54,@55,@56,@57,@58,@59,@60,@61,NULL,GETDATE(),GETDATE(),1,@62,@63,@64,@65,@66,@67,@68,@69,@70,@71,@72,@73,@74,@75,@76,@77,@78,@79,@80

DELETE FROM OrdersimulationMV WHERE OrderNo=@0
    INSERT INTO OrdersimulationMV ( [OrderNo]
      ,[ObjectNo]
      ,[ProductTypeCode]
      ,[VehicleCode]
      ,[BrandCode]
      ,[ModelCode]
      ,[Series]
      ,[Type]
      ,[Sitting]
      ,[Year]
      ,[CityCode]
      ,[UsageCode]
      ,[SumInsured]
      ,[AccessSI]
      ,[RegistrationNumber]
      ,[EngineNumber]
      ,[ChasisNumber]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsNew]
      ,[ColorOnBPKB]) 
  SELECT @81,@82,@83,@84,@85,@86,@87,@88,@89,@90,@91,@92,@93,@94,@95,@96,@97,GETDATE(),1,@98,@99";
                    #endregion

                    //Update 105 Hotfix - Ganti branch code data polis lama / renot ke branch code SO yang login
                    db11.Execute(query, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2, custInfo.Email1
                        , "", custInfo.CustIDAAB, salesOfficerID, DealerCode, SalesmanCode, SOBranchID
                        , custInfo.isCompany, custInfo.IdentityNo, custInfo.Gender, custInfo.ProspectID, custInfo.BirthDate
                        , custInfo.Address, custInfo.PostCode

                        //ProspectCompany
                        , custID, custInfo.Name, followUpID, custInfo.Email1
                        , custInfo.CompanyNPWPNo, custInfo.CompanyNPWPDate, custInfo.CompanyNPWPAddress, custInfo.Address, custInfo.PostCode
                        , custInfo.PICPhone, custInfo.PIC

                        //FollowUp
                        , followUpID, 0, custID, custInfo.Name, custInfo.Phone1, custInfo.Phone2
                        , salesOfficerID, custInfo.Name, 1, 1, remarkToSA, SOBranchID

                        //OrderSimulation
                        , orderID, custID, followUpID, orderSimulation.QuotationNo, orderSimulation.MultiYearF
                        , orderSimulation.YearCoverage, orderSimulation.TLOPeriod, orderSimulation.ComprePeriod, SOBranchID
                        , salesOfficerID, orderSimulation.PhoneSales, DealerCode, SalesmanCode
                        , orderSimulation.ProductTypeCode, orderSimulation.AdminFee, orderSimulation.TotalPremium, orderSimulation.Phone1
                        , orderSimulation.Phone2, orderSimulation.Email1, orderSimulation.Email2, orderSimulation.SendStatus
                        , orderSimulation.InsuranceType, orderSimulation.ApplyF, orderSimulation.SendF, orderSimulation.LastInterestNo
                        , orderSimulation.LastCoverageNo, PolicyNo, orderSimulation.NewPolicyNo, orderSimulation.NewPolicyId
                        , orderSimulation.VANumber, orderSimulation.PeriodFrom, orderSimulation.PeriodTo, orderSimulation.ProductCode
                        , orderSimulation.SegmentCode, orderSimulation.PolicyDeliverAddress, orderSimulation.PolicyDeliverPostalCode
                        , orderSimulation.PolicyDeliveryType, orderSimulation.PolicyDeliveryName
                        , isPSTB == 1 ? true : false, orderSimulation.MouID

                        //OrderSimulationMV
                        , orderID, orderSimulationMV.ObjectNo, orderSimulationMV.ProductTypeCode
                        , orderSimulationMV.VehicleCode, orderSimulationMV.BrandCode, orderSimulationMV.ModelCode
                        , orderSimulationMV.Series, orderSimulationMV.Type, orderSimulationMV.Sitting
                        , orderSimulationMV.Year, orderSimulationMV.CityCode, orderSimulationMV.UsageCode
                        , orderSimulationMV.SumInsured, orderSimulationMV.AccessSI, orderSimulationMV.RegistrationNumber
                        , orderSimulationMV.EngineNumber, orderSimulationMV.ChasisNumber, orderSimulationMV.IsNew, orderSimulationMV.Color

                        //IsCompany
                        , custInfo.isCompany
                        );

                }
                #endregion

                #region Insert Order Simulation Interest
                query = @"
DELETE FROM OrderSimulationInterest WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2
INSERT INTO OrderSimulationInterest ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[InterestID]
      ,[Year]
      ,[Premium]
      ,[PeriodFrom]
      ,[PeriodTo]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,GETDATE(),1,@8";
                using (a2isDBHelper.Database db14 = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationInterestRenot osI in listOSInterest)
                    {
                        db14.Execute(query, orderID, osI.ObjectNo, osI.InterestNo, osI.InterestID, osI.Year
                            , osI.Premium, osI.PeriodFrom, osI.PeriodTo, osI.DeductibleCode);
                    }
                }
                #endregion

                #region Insert Order Simulation Coverage
                query = @"DELETE FROM OrderSimulationCoverage WHERE OrderNo = @0 AND ObjectNo = @1 AND InterestNo = @2 AND CoverageNo = @3
    INSERT INTO OrderSimulationCoverage ([OrderNo]
      ,[ObjectNo]
      ,[InterestNo]
      ,[CoverageNo]
      ,[CoverageID]
      ,[Rate]
      ,[SumInsured]
      ,[LoadingRate]
      ,[Loading]
      ,[Premium]
      ,[BeginDate]
      ,[EndDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[IsBundling]
      ,[Entry_Pct]
      ,[Ndays]
      ,[Excess_Rate]
      ,[Calc_Method]
      ,[Cover_Premium]
      ,[Gross_Premium]
      ,[Max_si]
      ,[Net1]
      ,[Net2]
      ,[Net3]
      ,[Deductible_Code]) 
	  SELECT @0,@1,@2,@3,@4,@5,@6,@7,@8,@9,@22,@23,GETDATE(),1,@10,
            @11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21";
                using (a2isDBHelper.Database db15 = new a2isDBHelper.Database("AABMobile"))
                {
                    foreach (Models.vWeb2.OrderSimulationCoverageRenot osC in listOSCoverage)
                    {
                        db15.Execute(query, orderID, osC.ObjectNo, osC.InterestNo, osC.CoverageNo
                            , osC.CoverageID, osC.Rate, osC.SumInsured, osC.LoadingRate, osC.Loading
                            , osC.Premium, osC.IsBundling, osC.EntryPct, osC.NDays, osC.ExcessRate
                            , osC.CalcMethod, osC.CoverPremium, osC.GrossPremium, osC.MaxSi
                            , osC.Net1, osC.Net2, osC.Net3, osC.DeductibleCode, osC.BeginDate, osC.EndDate);
                    }
                }
                #endregion

                Otosales.Repository.v0213URF2019.MobileRepository.UpdateImageFollowUp(followUpID);

                query = @"UPDATE FollowUp
                          SET [FollowUpStatus] = 2
                          ,[FollowUpInfo] = 61
                           WHERE FollowUpNo = @0";
                using (a2isDBHelper.Database db16 = new a2isDBHelper.Database("AABMobile"))
                {
                    db16.Execute(query, followUpID);
                }

                #region Update Status Exclude Renewal Policy
                query = @"UPDATE [Excluded_Renewal_Policy] SET IsSendToSA=1 WHERE Policy_No=@0";
                using (a2isDBHelper.Database db17 = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                {
                    db17.Execute(query, PolicyNo);
                }
                #endregion
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        public dynamic GetSurveyScheduleSurveyManagement(string cityid, string zipcode, DateTime d)
        {
            dynamic result = null;
            var AABMobileDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            var AABDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB);
            string qGetSurveyAddress = @"SELECT AreaCode GeoAreaCode FROM SurveyManagement.dbo.MappingSurveyAreaZipCode where ZipCode = @0 AND RowStatus = 0";
            List<dynamic> addresses = AABDB.Fetch<dynamic>(qGetSurveyAddress, zipcode);
            if (addresses.Count > 0)
            {
                string GeoAreaCode = addresses.First().GeoAreaCode;
                if (!string.IsNullOrEmpty(GeoAreaCode))
                {
                    string QueryGetGAScheduleWithDate = @";SELECT  @0 AS SurveyDate
        INTO    #SurveyDate

            SELECT  CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
						 WHEN tc.TimeCategoryCode = 'TC2' THEN 40
						 WHEN tc.TimeCategoryCode = 'TC3' THEN 41
					END ScheduleTime ,
					ZipCode ,
					msast.AreaCode SurveyareaCode ,
					CityId ,
					TC.TimeCategoryDescription ScheduleTimeTextID ,
					CASE WHEN tc.TimeCategoryCode = 'TC1'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
						 WHEN tc.TimeCategoryCode = 'TC2'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
						 WHEN tc.TimeCategoryCode = 'TC3'
						 THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
					END ScheduleTimeTextEN ,
                    msast.Quota ,
                    ( SELECT    COUNT(SurveyOrderID)
                      FROM      SurveyManagement.dbo.SurveyOrder AS so
                      WHERE     so.RowStatus = 0
                                AND so.IsRescheduleF = 0
                                AND so.SurveyDate = sd.SurveyDate
                                AND so.AreaCode = msast.AreaCode
                                AND so.SurveyTime >= msast.Start
                                AND so.SurveyTime < msast.[End]
                    ) AS slotbooked
            INTO    #DateSlot
            FROM    #SurveyDate AS sd
                    FULL JOIN SurveyManagement.dbo.MappingSurveyAreaSlotTime AS msast ON 1 = 1
					INNER JOIN ( SELECT DISTINCT
                            PostalCode ZipCode,
                            CityID,
							AreaCode
                     FROM   BEYONDMOSS.Asuransiastra.dbo.Postal p
					 INNER JOIN SurveyManagement.dbo.MappingSurveyAreaZipCode msazc
					 ON p.PostalCode = msazc.ZipCode AND p.RowStatus = 0
                   ) c ON c.AreaCode = msast.AreaCode
				   LEFT JOIN BEYONDMOSS.GardaAkses.GA.TimeCategory tc ON TC.ScheduleTime =  SUBSTRING(CONVERT(VARCHAR, msast.Start), 1, 5)
            WHERE   msast.AreaCode = @1
                    AND msast.RowStatus = 0
					AND c.ZipCode = @2

			IF (CAST(@0 AS DATE) = CAST(GETDATE() AS DATE))
			BEGIN
				SELECT * FROM #DateSlot
			END
			ELSE
			BEGIN
				SELECT * FROM #DateSlot
				WHERE Quota - slotbooked >0
			END
    DROP TABLE #SurveyDate
    DROP TABLE #DateSlot";
                    using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
                    {
                        string AvailableDate = d.ToString("yyyy-MM-dd");
                        result = db.Fetch<dynamic>(QueryGetGAScheduleWithDate, AvailableDate, GeoAreaCode, zipcode);
                    }
                }
            }

            return result;

        }

        public dynamic GetScheduleSurveyDalam(List<string> TimeCodeSurveyDalam)
        {
            dynamic result = null;

            using (var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_GARDAAKSES))
            {
                string queryGetScheduleSurveyDalam = @"
SELECT 
        CASE WHEN tc.TimeCategoryCode = 'TC1' THEN 39
             WHEN tc.TimeCategoryCode = 'TC2' THEN 40
             WHEN tc.TimeCategoryCode = 'TC3' THEN 41
             WHEN tc.TimeCategoryCode = 'TC4' THEN 42
        END ScheduleTime ,
        TC.TimeCategoryDescription ScheduleTimeTextID ,
        CASE WHEN tc.TimeCategoryCode = 'TC1'
             THEN REPLACE(TC.TimeCategoryDescription, 'Pagi', 'Morning')
             WHEN tc.TimeCategoryCode = 'TC2'
             THEN REPLACE(TC.TimeCategoryDescription, 'Siang', 'Afternoon')
             WHEN tc.TimeCategoryCode = 'TC3'
             THEN REPLACE(TC.TimeCategoryDescription, 'Sore', 'Noon')
             WHEN tc.TimeCategoryCode = 'TC4'
             THEN REPLACE(TC.TimeCategoryDescription, 'Malam', 'Night')
        END ScheduleTimeTextEN
FROM    GA.TimeCategory TC WHERE  timecategorycode in (@0)";
                result = db.Fetch<dynamic>(queryGetScheduleSurveyDalam, TimeCodeSurveyDalam.Count > 0 ? TimeCodeSurveyDalam : new List<string>(new string[] { "0" }));
                return result;
            }
        }

        public List<string> getSurveyCategoryInternalSurvey(string BranchID, DateTime d)
        {
            List<string> timeCat = new List<string>();
            string qGetTimeCat = @";DECLARE @@dayWeekNumber INT = -1, @@DateAvailabe DATETIME = @1

SET @@dayWeekNumber = (
CASE WHEN (DATEPART(dw, @@DateAvailabe)-1) = 0 THEN 7 -- make Monday = 1, Sunday = 7
ELSE (DATEPART(dw, @@DateAvailabe)-1)
END 
)

select part from GODigital.Fn_SplitString
((SELECT TimeCategory FROM Asuransiastra.dbo.BranchOperatingHour 
WHERE BranchID = @0
AND DayStart <= @@dayWeekNumber
AND DayEnd >= @@dayWeekNumber
),',')";
            using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB))
            {
                string AvailableDate = d.ToString("yyyy-MM-dd");
                timeCat = db.Fetch<string>(qGetTimeCat, BranchID, AvailableDate);
            }
            return timeCat;
        }

        public List<string> GetSurveyDays(string BranchID)
        {
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            try
            {
                if (!string.IsNullOrEmpty(BranchID))
                {
                    int svyMinDate = 1, svyMaxDate = 1;
                    Int32.TryParse(Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALES-MIN-SURVEY-DATEADD")[0], out svyMinDate);
                    Int32.TryParse(Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALES-MAX-SURVEY-DATEADD")[0], out svyMaxDate);
                    CultureInfo ci = new CultureInfo("en-US");
                    DateTime datefrom = DateTime.Now.AddDays(svyMinDate);
                    DateTime dateto = DateTime.Now.AddDays(svyMaxDate);
                    int days = (dateto - datefrom).Days;

                    string mindate = datefrom.ToString("dd-MMM-yyyy", ci);
                    string maxdate = dateto.ToString("yyyy/MM/dd", ci);

                    string qDayWeekBranchID = @";SELECT COALESCE((
SELECT MIN(DayStart) FROM Asuransiastra.dbo.BranchOperatingHour WHERE BranchID = @0
),0) DayStart, 
 COALESCE((
SELECT MAX(DayEnd) FROM Asuransiastra.dbo.BranchOperatingHour WHERE BranchID = @0
), 0) DayEnd";
                    dynamic startEndDayWeek = db.FirstOrDefault<dynamic>(qDayWeekBranchID, BranchID);

                    List<string> dates = new List<string>();
                    for (int cal = 0; cal <= days; cal++)
                    {
                        DateTime dateTimeSurvey = datefrom.AddDays(cal);
                        int dayWeek = (int)dateTimeSurvey.DayOfWeek == 0 ? 7 : (int)dateTimeSurvey.DayOfWeek; // -- make Monday = 1, Sunday = 7
                        if (Int32.Parse(BranchID) != 0
                            && ((int)startEndDayWeek.DayStart <= dayWeek
                            && (int)startEndDayWeek.DayEnd >= dayWeek))
                        {
                            dates.Add(dateTimeSurvey.ToString("dd-MM-yyyy", ci));
                        }

                        if (Int32.Parse(BranchID) == 0)
                        {
                            dates.Add(dateTimeSurvey.ToString("dd-MM-yyyy", ci));
                        }

                    }

                    var asuransiastraDB = new a2isDBHelper.Database(Util.CONNECTION_STRING_ASURANSIASTRADB);
                    string qGetBranchType = @"SELECT [Type] FROM dbo.Branch WHERE ID = @0";
                    //                string qGetBranchType = @"SELECT DISTINCT b.Type
                    //                                FROM Branch b
                    //                                INNER JOIN city c ON b.CITYID = c.ID
                    //							    WHERE CityId = @0
                    //                                AND b.type IN ( 2, 5, 6 )					
                    //                                AND b.ISDELETED = 0";
                    List<dynamic> ls = asuransiastraDB.Fetch<dynamic>(qGetBranchType, BranchID);
                    int type = 0;
                    if (ls.Count > 0)
                    {
                        type = Convert.ToInt32(ls.First().Type);
                    }

                    List<string> DateConfig = Repository.vWeb2.MobileRepository.GetApplicationParametersValue("OTOSALES-SURVEYDATE-ALLOWHOLIDAY");
                    string[] listBranchType = DateConfig[0].Split(',');
                    string[] listFlagAllow = DateConfig[1].Split(',');
                    bool isAllowHoliday = true;
                    for (int i = 0; i < listBranchType.Length; i++)
                    {
                        if (Convert.ToInt32(listBranchType[i]) == type)
                        {
                            if (Convert.ToInt32(listFlagAllow[i]) == 1)
                            {
                                isAllowHoliday = true;
                            }
                        }
                    }
                    isAllowHoliday = true;
                    if (!isAllowHoliday)
                    {
                        List<dynamic> offDates = Repository.vWeb2.MobileRepository.GetOffDayDate(datefrom, dateto);
                        foreach (dynamic offDate in offDates)
                        {
                            if (dates.Contains(offDate.OffDate.ToString()))
                            {

                                dates.Remove(offDate.OffDate.ToString());
                            }
                        }
                    }

                    return dates;

                }
                return null;
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace + "," + e.ToString());
                throw e;
            }
        }


        public void ClearFailedData(string custID, string followUpID, string orderID, string PolicyNo)
        {
            #region Query Delete Fail GT
            string qDeleteFailGT = @"
                                               DELETE FROM OrderSimulationCoverage WHERE OrderNo=@2
                                               DELETE FROM OrderSimulationInterest WHERE OrderNo=@2
                                               DELETE FROM OrdersimulationMV WHERE OrderNo=@2
                                               DELETE FROM OrderSimulation WHERE CustId=@0 AND FollowUpNo=@1 AND OrderNo=@2
                                               DELETE FROM FollowUp WHERE CustId=@0 AND FollowUpNo=@1
                                               DELETE FROM ProspectCompany WHERE CustId=@0
                                               DELETE FROM ProspectCustomer WHERE CustId=@0";
            #endregion
            using (a2isDBHelper.Database db = new a2isDBHelper.Database("AABMobile"))
            {
                db.Execute(qDeleteFailGT, custID, followUpID, orderID);
            }
            #region Update Status Exclude Renewal Policy
            string query = @"UPDATE [Excluded_Renewal_Policy] SET IsSendToSA=1 WHERE Policy_No=@0";
            using (a2isDBHelper.Database db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AAB))
            {
                db.Execute(query, PolicyNo);
            }
            #endregion
        }

        public void SaveImageProspect(string FollowUpNo)
        {
            var db = new a2isDBHelper.Database(Util.CONNECTION_STRING_AABMOBILE);
            string actionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string query = @";
SELECT [ImageID]
      ,[FollowUpNo]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData] FROM [TempImageData] WHERE FollowUpNo=@0 AND RowStatus=1;";
                List<dynamic> ImageData = db.Fetch<dynamic>(query, FollowUpNo);
                foreach (dynamic id in ImageData)
                {
                    query = @"IF NOT EXISTS(SELECT * FROM ImageData where ImageId=@0)
                                        BEGIN
                                        INSERT INTO ImageData ([ImageID]
      ,[DeviceID]
      ,[SalesOfficerID]
      ,[PathFile]
      ,[EntryDate]
      ,[LastUpdatedTime]
      ,[RowStatus]
      ,[Data]
      ,[ThumbnailData]) VALUES(@0,@1,@2,@3,@4,@5,1,@6,@7)
END";
                    db.Execute(query, id.ImageID, id.DeviceID
    , id.SalesOfficerID
    , id.PathFile
    , id.EntryDate
    , id.LastUpdatedTime
    , id.Data
    , id.ThumbnailData);
                    db.Execute("UPDATE dbo.TempImageData SET RowStatus=0 WHERE FollowUpNo=@0 AND PathFile=@1", id.FollowUpNo, id.PathFile);
                }
            }
            catch (Exception e)
            {
                logger.Error("Error at Function " + actionName + " :" + e.StackTrace);
                throw e;
            }
        }

    }
}