﻿using System;
using Xunit;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Globalization;
using Moq;
using System.Dynamic;
using Otosales.Models;
using PremiumCalculation.Model;
using A2isMessaging;

namespace Otosales.UnitTest
{
    //[TestClass]
    public class OtosalesUnitTest
    {
        #region Helpers
        private string POSTurlencoded(string route, string param)
        {
            HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://publicapi-qc.gardaoto.com/Otosales/api/");
            client.BaseAddress = new Uri("http://localhost/Otosales/api/");
            //client.DefaultRequestHeaders.Add("content-type", "application/x-www-form-urlencoded");
            return client.PostAsync(route, new StringContent(param, Encoding.UTF8, "application/x-www-form-urlencoded")).Result.Content.ReadAsStringAsync().Result;
        }
        private string POSTurlencodedWithBearer(string route, string param)
        {
            HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://publicapi-qc.gardaoto.com/Otosales/api/");
            client.BaseAddress = new Uri("http://localhost/Otosales/api/");
            string json = Login("ENU", "P@ssw0rd");
            JObject obj = JObject.Parse(json);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", obj["Token"].ToString());
            //client.DefaultRequestHeaders.Add("content-type", "application/x-www-form-urlencoded");
            return client.PostAsync(route, new StringContent(param, Encoding.UTF8, "application/x-www-form-urlencoded")).Result.Content.ReadAsStringAsync().Result;
        }
        private string POSTjsondWithBearer(string route, string param)
        {
            HttpClient client = new HttpClient();
            //client.BaseAddress = new Uri("https://publicapi-qc.gardaoto.com/Otosales/api/");
            client.BaseAddress = new Uri("http://localhost/Otosales/api/");
            string json = Login("ENU", "P@ssw0rd");
            JObject obj = JObject.Parse(json);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", obj["Token"].ToString());
            //client.DefaultRequestHeaders.Add("content-type", "application/x-www-form-urlencoded");
            return client.PostAsync(route, new StringContent(param, Encoding.UTF8, "application/json")).Result.Content.ReadAsStringAsync().Result;
        }
        public string Login(string username, string password)
        {
            string json = POSTurlencoded("vWeb2/Authentication/login/", "userID=" + username + "&password=" + password);
            return json;
        }
        #endregion

        [Theory(DisplayName = "Login_ReturnLoginSuccess")]
        [InlineData(new object[] { "idl", "P@ssw0rd"})]
        [InlineData(new object[] { "xxlinge@gomitra.com", "Password95" })]
        public void Login_ReturnLoginSuccess(string userID, string password)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("userID", userID),
                new KeyValuePair<string, string>("password", password)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.AuthenticationController();
            var result = controller.Login(form);
            dynamic content = result as System.Web.Http.Results.JsonResult<LoginResult<UserPrivilege>>;

            Assert.True(((Otosales.Models.LoginStatus)(content.Content)).IsAuthenticated);
            Assert.NotEmpty(content.Content.UserInfo.User.Name);
        }
        [Fact(DisplayName = "Login_ReturnLoginFailed")]
        public void Login_ReturnLoginFailed()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string userID = "rmf"; string password = "123442";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("userID", userID),
                new KeyValuePair<string, string>("password", password)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.AuthenticationController();
            var result = controller.Login(form);
            dynamic content = result as System.Web.Http.Results.JsonResult<LoginResult<UserPrivilege>>;

            Assert.False(((Otosales.Models.LoginStatus)(content.Content)).IsAuthenticated);
            Assert.Null(content.Content.UserInfo);
        }

        #region OldLogin
        //[Fact(DisplayName = "Login - AO")]
        //public void testLoginAO()
        //{
        //    string o = Login("ENU", "P@ssw0rd");
        //    JObject obj = JObject.Parse(o);

        //    Assert.True(Convert.ToInt32(obj["IsAuthenticated"]) == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "Login - MGO")]
        //public void testLoginMGO()
        //{
        //    string o = Login("xxlinge@gomitra.com", "P@ssw0rd");
        //    JObject obj = JObject.Parse(o);

        //    Assert.True(Convert.ToInt32(obj["IsAuthenticated"]) == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "Login - Fail")]
        //public void testLoginFail()
        //{
        //    string o = Login("ENU", "Password95");
        //    JObject obj = JObject.Parse(o);
        //    Assert.True(Convert.ToInt32(obj["IsAuthenticated"]) == 0);
        //} 
        #endregion
        //[TestMethod]
        #region GetTaskListNew
        [Theory(DisplayName = "GetTaskList_ReturnTaskList")]
        [InlineData(new object[] { "ESP", 1, "Status", 1, false,
            true, true, true, true, true, false,
            true, true, false, true, "2020-4-8 23:59:59.725", false,
            false, false, true, "", 1, 20, true, true })]
        [InlineData(new object[] { "yih", 1, "Status", 1, true,
            true, true, true, true, true, false,
            true, true, false, false, "", false,
            false, false, true, "", 1, 20, true, true })]
        [InlineData(new object[] { "LING0001", 1, "Name", 1, false,
            true, true, true, true, true, true,
            true, true, true, true, "2020-4-8 23:59:59.725", false,
            false, false, true, "", 2, 20, false, true })]
        [InlineData(new object[] { "KNA", 1, "LastFollowUpdate", 1, false,
            true, true, true, true, true, true,
            true, true, true, true, "2020-4-8 23:59:59.725", false,
            true, false, true, "RGK8888JN", 1, 20, false, false })]
        [InlineData(new object[] { "", 1, "Status", 1, false,
            true, true, true, true, true, false,
            true, true, false, true, "2020-4-8 23:59:59.725", false,
            false, false, true, "sdfsdf", 1, 20, true, true })]
        [InlineData(new object[] { "LING0001", 1, "EntryDate", 0, false,
            true, true, true, true, true, false,
            true, true, true, true, "2020-4-8 23:59:59.725", true,
            false, false, true, "ENGEND1", 1, 20, true, true })]
        [InlineData(new object[] { "GAI", 1, "NextFollowUpdate", 0, false,
            true, true, true, true, true, false,
            true, true, true, true, "2020-4-8 23:59:59.725", false,
            false, true, true, "2000534857", 1, 20, true, true })]
        [InlineData(new object[] { "LING0001", 0, "NextFollowUpdate", 0, false,
            true, true, true, true, true, false,
            true, true, true, true, "2020-4-8 23:59:59.725", false,
            false, false, true, "JSW", 1, 20, true, true })]
        [InlineData(new object[] { "LING0001", 0, "NextFollowUpdate", 0, false,
            false, false, false, false, true, false,
            false, false, false, true, "2020-4-8 23:59:59.725", false,
            false, false, false, "", 1, 20, true, true })]
        public void GetTaskList_ReturnTaskList(string salesofficerid, int allowSearchOthers, string indexOrderByItem, int indexOrderByTypeItem, bool chFollowUpMonthlyChecked
            , bool chNeedFUChecked, bool chPotentialChecked, bool chCallLaterChecked, bool chCollectDocChecked, bool chOrderRejectedChecked, bool chNotDealChecked
            , bool chSentToSAChecked, bool chBackToAOChecked, bool chPolicyCreatedChecked, bool chFollowUpDateChecked, string getFollowUpDate, bool chIsSearchByEngNo
            , bool chIsSearchByChasNo, bool chIsSearchByPolicyNo, bool chIsSearchByProsName, string search, int currentPage, int pageSize, bool chNewChecked, bool chRenewableChecked)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("currentPage", currentPage.ToString()),
                new KeyValuePair<string, string>("pageSize", pageSize.ToString()),
                new KeyValuePair<string, string>("chIsSearchByChasNo", chIsSearchByChasNo.ToString()),
                new KeyValuePair<string, string>("chIsSearchByEngNo", chIsSearchByEngNo.ToString()),
                new KeyValuePair<string, string>("chIsSearchByPolicyNo", chIsSearchByPolicyNo.ToString()),
                new KeyValuePair<string, string>("chIsSearchByProsName", chIsSearchByProsName.ToString()),
                new KeyValuePair<string, string>("salesofficerid", salesofficerid),
                new KeyValuePair<string, string>("search", search),
                new KeyValuePair<string, string>("indexOrderByItem", indexOrderByItem),
                new KeyValuePair<string, string>("indexOrderByTypeItem", indexOrderByTypeItem.ToString()),
                new KeyValuePair<string, string>("chFollowUpMonthlyChecked", chFollowUpMonthlyChecked.ToString()),
                new KeyValuePair<string, string>("chFollowUpDateChecked", chFollowUpDateChecked.ToString()),
                new KeyValuePair<string, string>("chNeedFUChecked", chNeedFUChecked.ToString()),
                new KeyValuePair<string, string>("chPotentialChecked", chPotentialChecked.ToString()),
                new KeyValuePair<string, string>("chCallLaterChecked", chCallLaterChecked.ToString()),
                new KeyValuePair<string, string>("chCollectDocChecked", chCollectDocChecked.ToString()),
                new KeyValuePair<string, string>("chNotDealChecked", chNotDealChecked.ToString()),
                new KeyValuePair<string, string>("chSentToSAChecked", chSentToSAChecked.ToString()),
                new KeyValuePair<string, string>("chBackToAOChecked", chBackToAOChecked.ToString()),
                new KeyValuePair<string, string>("chPolicyCreatedChecked", chPolicyCreatedChecked.ToString()),
                new KeyValuePair<string, string>("chOrderRejectedChecked", chOrderRejectedChecked.ToString()),
                new KeyValuePair<string, string>("chNewChecked", chNewChecked.ToString()),
                new KeyValuePair<string, string>("chRenewableChecked", chRenewableChecked.ToString()),
                new KeyValuePair<string, string>("getFollowUpDate", getFollowUpDate.ToString()),
                new KeyValuePair<string, string>("allowSearchOthers", allowSearchOthers.ToString())
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0063URF2020.DataReactController();
            var result = controller.getTaskList(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            if (salesofficerid == null || salesofficerid == "")
            {
                Assert.False(status);
                Assert.Equal("Parameter is null", message);
                Assert.Equal("null", data);
            }
            else
            {
                Assert.True(status);
                Assert.Equal("OK", message);
                if (!chNewChecked && !chRenewableChecked)
                {
                    Assert.True(data.Items.Count == 0);
                }
                else
                {
                    Assert.True(data.Items.Count > 0);
                }
            }
        }
        [Fact(DisplayName = " GetTaskList_ReturnException")]
        public void GetTaskList_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string salesofficerid = ""; int allowSearchOthers = 0; string indexOrderByItem = ""; int indexOrderByTypeItem = 0; bool chFollowUpMonthlyChecked = false;
            bool chNeedFUChecked = false; bool chPotentialChecked = false; bool chCallLaterChecked = false; bool chCollectDocChecked = false; bool chOrderRejectedChecked = false;
            bool chNotDealChecked = false; bool chSentToSAChecked = false; bool chBackToAOChecked = false; bool chPolicyCreatedChecked = false; bool chFollowUpDateChecked = false;
            string getFollowUpDate = ""; bool chIsSearchByEngNo = false; string chIsSearchByChasNo = "jhfskj"; bool chIsSearchByPolicyNo = false; bool chIsSearchByProsName = false;
            string search = ""; int currentPage = 0; int pageSize = 20; bool chNewChecked = false; bool chRenewableChecked = false;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("currentPage", currentPage.ToString()),
                new KeyValuePair<string, string>("pageSize", pageSize.ToString()),
                new KeyValuePair<string, string>("chIsSearchByChasNo", chIsSearchByChasNo.ToString()),
                new KeyValuePair<string, string>("chIsSearchByEngNo", chIsSearchByEngNo.ToString()),
                new KeyValuePair<string, string>("chIsSearchByPolicyNo", chIsSearchByPolicyNo.ToString()),
                new KeyValuePair<string, string>("chIsSearchByProsName", chIsSearchByProsName.ToString()),
                new KeyValuePair<string, string>("salesofficerid", salesofficerid),
                new KeyValuePair<string, string>("search", search),
                new KeyValuePair<string, string>("indexOrderByItem", indexOrderByItem),
                new KeyValuePair<string, string>("indexOrderByTypeItem", indexOrderByTypeItem.ToString()),
                new KeyValuePair<string, string>("chFollowUpMonthlyChecked", chFollowUpMonthlyChecked.ToString()),
                new KeyValuePair<string, string>("chFollowUpDateChecked", chFollowUpDateChecked.ToString()),
                new KeyValuePair<string, string>("chNeedFUChecked", chNeedFUChecked.ToString()),
                new KeyValuePair<string, string>("chPotentialChecked", chPotentialChecked.ToString()),
                new KeyValuePair<string, string>("chCallLaterChecked", chCallLaterChecked.ToString()),
                new KeyValuePair<string, string>("chCollectDocChecked", chCollectDocChecked.ToString()),
                new KeyValuePair<string, string>("chNotDealChecked", chNotDealChecked.ToString()),
                new KeyValuePair<string, string>("chSentToSAChecked", chSentToSAChecked.ToString()),
                new KeyValuePair<string, string>("chBackToAOChecked", chBackToAOChecked.ToString()),
                new KeyValuePair<string, string>("chPolicyCreatedChecked", chPolicyCreatedChecked.ToString()),
                new KeyValuePair<string, string>("chOrderRejectedChecked", chOrderRejectedChecked.ToString()),
                new KeyValuePair<string, string>("chNewChecked", chNewChecked.ToString()),
                new KeyValuePair<string, string>("chRenewableChecked", chRenewableChecked.ToString()),
                new KeyValuePair<string, string>("getFollowUpDate", getFollowUpDate.ToString()),
                new KeyValuePair<string, string>("allowSearchOthers", allowSearchOthers.ToString())
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0063URF2020.DataReactController();
            var result = controller.getTaskList(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
            //Assert.Equal("null", data);
        }
        #endregion
        #region GetTasklistOld
        //[Fact(DisplayName = "GetTaskList - Basic")]
        //public void GetTaskListBasic()
        //{
        //    try
        //    {
        //        string param = string.Empty;
        //        string userid = "ESP";
        //        param = "salesofficerid=" + userid + "&allowSearchOthers=0&indexOrderByItem=Status&indexOrderByTypeItem=1&chFollowUpMonthlyChecked=false&chNeedFUChecked=true&chPotentialChecked=true&chCallLaterChecked=true&chCollectDocChecked=true&chOrderRejectedChecked=true&chNotDealChecked=false&chSentToSAChecked=true&chBackToAOChecked=true&chPolicyCreatedChecked=false&chFollowUpDateChecked=true&getFollowUpDate=2020-3-12 23:59:59.725&chIsSearchbyEngNo=false&chIsSearchbyChasNo=false&chIsSearchbyPolicyNo=false&chIsSearchbyProsName=true&search=&currentPage=1&PageSize=20&PageSizeLead=20&chNewChecked=true&chRenewableChecked=true";
        //        string json = POSTurlencodedWithBearer("v0001URF2020/DataReact/getTaskList/", param);
        //        JObject obj1 = JObject.Parse(json);
        //        //string s = obj1["data"].ToString();
        //        JObject data = JObject.Parse(obj1["data"].ToString());
        //        JArray Items = JArray.Parse(data["Items"].ToString());
        //        Assert.NotNull(Items);

        //    }
        //    catch (Exception e)
        //    {
        //        Assert.NotNull(null);
        //    }

        //}
        //public void GetTaskListSearch(string username, string searchparam, string searchbychano
        //    , string seacrchbyengno, string searchbypolicyno, string searchbyname
        //    , string chNeedFUChecked, string chPotentialChecked, string chCallLaterChecked, string chCollectDocChecked, string chOrderRejectedChecked
        //    , string chNotDealChecked, string chSentToSAChecked, string chBackToAOChecked, string chPolicyCreatedChecked
        //    , string chFollowUpDateChecked, string FollowUpDate, bool IsSuccess)
        //{
        //    try
        //    {
        //        string param = string.Empty;
        //        param = "salesofficerid=" + username + "&allowSearchOthers=0&indexOrderByItem=Status&indexOrderByTypeItem=1&chFollowUpMonthlyChecked=false&chNeedFUChecked=" + chNeedFUChecked
        //            + "&chPotentialChecked=" + chPotentialChecked + "&chCallLaterChecked=" + chCallLaterChecked + "&chCollectDocChecked=" + chCollectDocChecked + "&chOrderRejectedChecked=" + chOrderRejectedChecked
        //            + "&chNotDealChecked=" + chNotDealChecked + "&chSentToSAChecked=" + chSentToSAChecked + "&chBackToAOChecked=" + chBackToAOChecked + "&chPolicyCreatedChecked=" + chPolicyCreatedChecked
        //            + "&chFollowUpDateChecked=" + chFollowUpDateChecked + "&getFollowUpDate=" + FollowUpDate + "&chIsSearchbyEngNo=" + searchbychano + "&chIsSearchbyChasNo=" + seacrchbyengno
        //            + "&chIsSearchbyPolicyNo=" + searchbypolicyno + "&chIsSearchbyProsName=" + searchbyname + "&search=" + searchparam + "&currentPage=1&PageSize=20&PageSizeLead=20&chNewChecked=true&chRenewableChecked=true";
        //        string json = POSTurlencodedWithBearer("v0001URF2020/DataReact/getTaskList/", param);
        //        JObject obj1 = JObject.Parse(json);
        //        //string s = obj1["data"].ToString();
        //        if (IsSuccess)
        //        {
        //            JObject data = JObject.Parse(obj1["data"].ToString());
        //            JArray Items = JArray.Parse(data["Items"].ToString());
        //            Assert.NotNull(Items);
        //        }
        //        else
        //        {
        //            Assert.False(Convert.ToBoolean(obj1["status"]));
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Assert.NotNull(null);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - SearchEngineNo")]
        //public void GetTaskListSearchEngineNo()
        //{
        //    GetTaskListSearch("ESP", "1NZZ315155", "true", "false", "false", "false", //seachflag
        //        "true", "true", "true", "true", "true", "false", "true", "true", "false", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", true);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - SearchChasisNo")]
        //public void GetTaskListSearchChasisNo()
        //{
        //    GetTaskListSearch("ESP", "MHFKT9F3XF6063574", "false", "true", "false", "false", //seachflag
        //        "true", "true", "true", "true", "false", "true", "true", "false", "true", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", true);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - SearchPolicyNo")]
        //public void GetTaskListSearchPolicyNo()
        //{
        //    GetTaskListSearch("ESP", "1900662529", "false", "false", "true", "false", //seachflag
        //        "true", "true", "true", "true", "true", "false", "true", "true", "false", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", true);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - SearchName")]
        //public void GetTaskListSearchName()
        //{
        //    GetTaskListSearch("ENU", "ENU RENEW", "false", "false", "false", "true", //seachflag
        //        "true", "true", "true", "true", "true", "false", "true", "true", "false", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", true);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - SearchFail")]
        //public void GetTaskListSearchFail()
        //{
        //    GetTaskListSearch("ESP", "ENU", "false", "false", "false", "false", //seachflag
        //        "true", "true", "true", "true", "true", "false", "true", "true", "false", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", false);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - PotentialOnly")]
        //public void GetTaskListPotentialOnly()
        //{
        //    GetTaskListSearch("ESP", "", "false", "false", "false", "false", //seachflag
        //        "true", "false", "false", "false", "false", "false", "false", "false", "false", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", true);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - SendToSAandPolicyCreated")]
        //public void GetTaskListSendToSAandPolicyCreated()
        //{
        //    GetTaskListSearch("ESP", "", "false", "false", "false", "false", //seachflag
        //        "false", "false", "false", "false", "false", "false", "true", "false", "true", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", true);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetTaskList - AllFilter")]
        //public void GetTaskListAllFilter()
        //{
        //    GetTaskListSearch("ESP", "", "false", "false", "false", "false", //seachflag
        //        "true", "true", "true", "true", "true", "true", "true", "true", "true", //filterflag 
        //        "true", "2020-3-12 23:59:59.725", true);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "PopulateOrderApproval - Succes")]
        //public void PopulateOrderApprovalSucces()
        //{
        //    string userid = "ENU";
        //    string json = POSTurlencodedWithBearer("v2/OrderApproval/populateOrderApproval", "userID=" + userid);
        //    JObject obj = JObject.Parse(json);
        //    Assert.True(Convert.ToInt32(obj["Status"]) == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "PopulateOrderApproval - Fail")]
        //public void PopulateOrderApprovalFail()
        //{
        //    string userid = "";
        //    string json = POSTurlencodedWithBearer("v2/OrderApproval/populateOrderApproval", "userID=" + userid);
        //    JObject obj = JObject.Parse(json);
        //    Assert.True(Convert.ToInt32(obj["Status"]) == 0);
        //}

        #endregion

        ////[TestMethod]
        //public void SaveProspect()
        //{
        //    string OrderNo = "";
        //    string CustId = ""; //"a63a7952-cf2e-488a-adb3-a37f2a3518d6";
        //    string FollowUpNo = ""; //"0f84ce89-d297-41ee-9d51-c1554bedf309";
        //    string GuidTempPenawaran = "292b1bd48415485e9def0c0f6c118bf15ce4b8e9da01497972fdf5f1512232f4";
        //    Otosales.Models.v0219URF2019.ProspectCustomer pc = new Otosales.Models.v0219URF2019.ProspectCustomer();
        //    string name = "UnitTest3";
        //    string phone1 = "087826598723";
        //    string email1 = "hjaf@mail.com";
        //    string SOID = "ENU";
        //    string BranchId = "016";
        //    int dealercode = 3951;
        //    int salesdealercode = 110254;
        //    string BrandCode = "B25";
        //    string ModelCode = "M01048";
        //    string Type = "T00011";
        //    int sitting = 7;
        //    string CityCode = "000020";
        //    string UsageCode = "U00002";
        //    string Year = "2019";
        //    string productctypeode = "GARDAOTO";
        //    string VehicleCode = "V07952";
        //    string RegistrationNumber = "B0983IL";
        //    string Series = "GRAND NEW G 1.3 A/T";
        //    string EngineNumber = "WERTHJDFHSf";
        //    string ChasisNumber = "HJFHFGSKHLJS";
        //    pc.CustID = ""; pc.Name = name; pc.Phone1 = phone1; pc.Phone2 = ""; pc.Email1 = email1; pc.Email2 = "";
        //    pc.SalesOfficerID = SOID; pc.DealerCode = dealercode; pc.SalesDealer = salesdealercode; pc.BranchCode = BranchId; pc.isCompany = false;
        //    string jsonProspectCust = JsonConvert.SerializeObject(pc);
        //    Otosales.Models.v0219URF2019.FollowUp f = new Otosales.Models.v0219URF2019.FollowUp();
        //    f.FollowUpNo = ""; f.LastSeqNo = 0; f.CustID = ""; f.ProspectName = name; f.Phone1 = phone1; f.Phone2 = ""; f.SalesOfficerID = SOID;
        //    f.FollowUpName = name; f.FollowUpStatus = 1; f.FollowUpInfo = 1; f.Remark = ""; f.BranchCode = BranchId; f.PolicyId = ""; f.PolicyNo = "";
        //    f.TransactionNo = ""; f.IdentityCard = ""; f.STNK = ""; f.SPPAKB = ""; f.BuktiBayar = "";
        //    string jsonFollowUp = JsonConvert.SerializeObject(f);
        //    string paramProspect = "State=PROSPECT&GuidTempPenawaran=&isMvGodig=false&isNonMvGodig=true&ProspectCustomer=" + jsonProspectCust + "&FollowUp=" + jsonFollowUp;
        //    string jsonprospect = POSTurlencodedWithBearer("v0219URF2019/DataReact/SaveProspect/", paramProspect);
        //    JObject obj = JObject.Parse(jsonprospect);
        //    OrderNo = obj["OrderNo"].ToString();
        //    CustId = obj["CustId"].ToString();
        //    FollowUpNo = obj["FollowUpNo"].ToString();
        //    GuidTempPenawaran = obj["GuidTempPenawaran"].ToString();
        //    Otosales.Models.v0219URF2019.OrderSimulation os = new Otosales.Models.v0219URF2019.OrderSimulation();
        //    os.OrderNo = OrderNo; os.CustID = CustId; os.ProductTypeCode = productctypeode; os.FollowUpNo = FollowUpNo; os.TLOPeriod = 0; os.ComprePeriod = 0; os.BranchCode = BranchId;
        //    os.SalesOfficerID = SOID; os.DealerCode = dealercode; os.SalesDealer = salesdealercode; os.ProductCode = "GDN52"; os.Phone1 = phone1;
        //    os.Phone2 = ""; os.Email1 = email1; os.Email2 = ""; os.InsuranceType = 1; os.ApplyF = true; os.LastInterestNo = 0; os.LastCoverageNo = 0;
        //    string stringOS = JsonConvert.SerializeObject(os);
        //    Otosales.Models.v0219URF2019.OrderSimulationMV omv = new Otosales.Models.v0219URF2019.OrderSimulationMV();
        //    omv.OrderNo = OrderNo; omv.ObjectNo = 1; omv.ProductTypeCode = productctypeode; omv.VehicleCode = VehicleCode; omv.BrandCode = BrandCode;
        //    omv.ModelCode = ModelCode; omv.Series = Series; omv.Type = Type; omv.Sitting = sitting; omv.Year = Year; omv.CityCode = CityCode;
        //    omv.UsageCode = UsageCode; omv.SumInsured = 219650000; omv.RegistrationNumber = RegistrationNumber; omv.EngineNumber = EngineNumber;
        //    omv.ChasisNumber = ChasisNumber; omv.IsNew = false;
        //    string stringOMV = JsonConvert.SerializeObject(omv);
        //    string paramVehicle = "State=VEHICLE&GuidTempPenawaran=" + GuidTempPenawaran + "&isMvGodig=false&isNonMvGodig=true&OrderSimulation=" + stringOS + "&OrderSimulationMV=" + stringOMV;
        //    string jsonvehicle = POSTurlencodedWithBearer("v0219URF2019/DataReact/SaveProspect/", paramVehicle);
        //    JObject objVehicle = JObject.Parse(jsonvehicle);
        //    int TLOPeriod = 0; int ComprePeriod = 2;
        //    OrderNo = objVehicle["OrderNo"].ToString();
        //    CustId = objVehicle["CustId"].ToString();
        //    FollowUpNo = objVehicle["FollowUpNo"].ToString();
        //    string sperfrom = "2020-03-12"; string sperto = "2022-03-12";
        //    DateTime periodfrom = DateTime.ParseExact(sperfrom, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //    DateTime periodto = DateTime.ParseExact(sperto, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //    os.OrderNo = OrderNo; os.CustID = CustId; os.FollowUpNo = FollowUpNo; os.ProductTypeCode = productctypeode; os.TLOPeriod = TLOPeriod; os.ComprePeriod = ComprePeriod; os.BranchCode = BranchId;
        //    os.SalesOfficerID = SOID; os.DealerCode = dealercode; os.SalesDealer = salesdealercode; os.ProductCode = "GDN52"; os.Phone1 = phone1;
        //    os.Phone2 = ""; os.Email1 = email1; os.Email2 = ""; os.InsuranceType = 1; os.ApplyF = true; os.LastInterestNo = 0; os.LastCoverageNo = 0;
        //    os.PeriodFrom = periodfrom; os.PeriodTo = periodto;
        //    stringOS = JsonConvert.SerializeObject(os);
        //    omv.OrderNo = OrderNo; omv.ObjectNo = 1; omv.ProductTypeCode = productctypeode; omv.VehicleCode = VehicleCode; omv.BrandCode = BrandCode;
        //    omv.ModelCode = ModelCode; omv.Series = Series; omv.Type = Type; omv.Sitting = sitting; omv.Year = Year; omv.CityCode = CityCode;
        //    omv.UsageCode = UsageCode; omv.SumInsured = 219650000; omv.RegistrationNumber = RegistrationNumber; omv.EngineNumber = EngineNumber;
        //    omv.ChasisNumber = ChasisNumber; omv.IsNew = false;
        //    stringOMV = JsonConvert.SerializeObject(omv);
        //    //State=COVER&GuidTempPenawaran=48c141b775744d598a9e722141ba7ab2c5934ae9341507a617c0a92fb9112b48&isMvGodig=false&isNonMvGodig=true&OrderSimulation={"OrderNo":"c4f5286b-7db5-4e90-ab46-205b4e3d4e43","CustID":"1706d1f9-192a-409e-9aa8-3eb3280f5a9a","FollowUpNo":"0908ee9a-0b88-4e73-85f3-6f6b84e76534","MultiYearF":false,"YearCoverage":1,"TLOPeriod":0,"ComprePeriod":1,"BranchCode":"016","SalesOfficerID":"enu","DealerCode":"3951","SalesDealer":"110254","ProductTypeCode":"GARDAOTO","ProductCode":"GDN52","AdminFee":50000,"TotalPremium":5651075,"Phone1":"086435737537","Phone2":"","Email1":"hsdf@mail.com","Email2":"","InsuranceType":1,"ApplyF":"true","SendF":"false","LastInterestNo":0,"LastCoverageNo":0,"PeriodTo":"2021-03-16 12:45:55","PeriodFrom":"2020-03-16 12:45:55"}&OrderSimulationMV={"OrderNo":"c4f5286b-7db5-4e90-ab46-205b4e3d4e43","ObjectNo":"1","ProductTypeCode":"GARDAOTO","VehicleCode":"V07952","BrandCode":"B25   ","ModelCode":"T00011","Series":"GRAND NEW G 1.3 A/T","Type":"T00011","Sitting":7,"Year":"2019","CityCode":"000020","UsageCode":"U00002","SumInsured":219650000,"RegistrationNumber":"B898Io","IsNew":false,"AccessSI":0,"searchVehicle":"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T"}&calculatedPremiItems=[{"InterestID":"CASCO","CoverageID":"ALLRIK","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":2.25,"ExcessRate":2.25,"Premium":4942125,"CoverPremium":4942125,"GrossPremium":4942125,"MaxSI":99999999999,"DeductibleCode":"FLT003","EntryPrecentage":100,"Ndays":365,"Net1":4942125,"Net2":4942125,"Net3":4942125,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":0,"AutoApply":"0","CoverType":null,"CalcMethod":0},{"InterestID":"CASCO","CoverageID":"ETV   ","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":0.125,"ExcessRate":0.125,"Premium":274562.5,"CoverPremium":274562.5,"GrossPremium":274562.5,"MaxSI":99999999999,"DeductibleCode":"CL0001","EntryPrecentage":100,"Ndays":365,"Net1":274562.5,"Net2":274562.5,"Net3":274562.5,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":1,"AutoApply":"1","CoverType":null,"CalcMethod":0},{"InterestID":"CASCO","CoverageID":"FLD   ","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":0.1,"ExcessRate":0.1,"Premium":219649.99999999997,"CoverPremium":219650,"GrossPremium":219650,"MaxSI":99999999999,"DeductibleCode":"CL0001","EntryPrecentage":100,"Ndays":365,"Net1":219650,"Net2":219650,"Net3":219650,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":1,"AutoApply":"1","CoverType":null,"CalcMethod":0},{"InterestID":"CASCO","CoverageID":"SRCC  ","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":0.075,"ExcessRate":0.075,"Premium":164737.5,"CoverPremium":164737.5,"GrossPremium":164737.5,"MaxSI":99999999999,"DeductibleCode":"CL0001","EntryPrecentage":100,"Ndays":365,"Net1":164737.5,"Net2":164737.5,"Net3":164737.5,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":1,"AutoApply":"1","CoverType":null,"CalcMethod":0}]&PremiumModel={"status":1,"message":"OK","ProductCode":"GDN52","CalculatedPremiItems":[{"InterestID":"CASCO","CoverageID":"ALLRIK","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":2.25,"ExcessRate":2.25,"Premium":4942125,"CoverPremium":4942125,"GrossPremium":4942125,"MaxSI":99999999999,"DeductibleCode":"FLT003","EntryPrecentage":100,"Ndays":365,"Net1":4942125,"Net2":4942125,"Net3":4942125,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":0,"AutoApply":"0","CoverType":null,"CalcMethod":0},{"InterestID":"CASCO","CoverageID":"ETV   ","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":0.125,"ExcessRate":0.125,"Premium":274562.5,"CoverPremium":274562.5,"GrossPremium":274562.5,"MaxSI":99999999999,"DeductibleCode":"CL0001","EntryPrecentage":100,"Ndays":365,"Net1":274562.5,"Net2":274562.5,"Net3":274562.5,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":1,"AutoApply":"1","CoverType":null,"CalcMethod":0},{"InterestID":"CASCO","CoverageID":"FLD   ","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":0.1,"ExcessRate":0.1,"Premium":219649.99999999997,"CoverPremium":219650,"GrossPremium":219650,"MaxSI":99999999999,"DeductibleCode":"CL0001","EntryPrecentage":100,"Ndays":365,"Net1":219650,"Net2":219650,"Net3":219650,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":1,"AutoApply":"1","CoverType":null,"CalcMethod":0},{"InterestID":"CASCO","CoverageID":"SRCC  ","Year":1,"PeriodFrom":"2020-03-16 00:00:00.000","PeriodTo":"2021-03-16 00:00:00.000","Rate":0.075,"ExcessRate":0.075,"Premium":164737.5,"CoverPremium":164737.5,"GrossPremium":164737.5,"MaxSI":99999999999,"DeductibleCode":"CL0001","EntryPrecentage":100,"Ndays":365,"Net1":164737.5,"Net2":164737.5,"Net3":164737.5,"SumInsured":219650000,"Loading":0,"LoadingRate":0,"IsBundling":1,"AutoApply":"1","CoverType":null,"CalcMethod":0}],"IsTPLEnabled":1,"IsTPLChecked":0,"IsTPLSIEnabled":0,"IsSRCCChecked":1,"IsSRCCEnabled":1,"IsFLDChecked":1,"IsFLDEnabled":1,"IsETVChecked":1,"IsETVEnabled":1,"IsTSChecked":0,"IsTSEnabled":1,"IsPADRVRChecked":0,"IsPADRVREnabled":1,"IsPADRVRSIEnabled":0,"IsPASSEnabled":0,"IsPAPASSSIEnabled":0,"IsPAPASSEnabled":1,"IsPAPASSChecked":0,"IsACCESSChecked":0,"IsACCESSEnabled":1,"IsACCESSSIEnabled":0,"SRCCPremi":164737.5,"FLDPremi":219649.99999999997,"ETVPremi":274562.5,"TSPremi":0,"PADRVRPremi":0,"PAPASSPremi":0,"TPLPremi":0,"ACCESSPremi":0,"AdminFee":50000,"TotalPremi":5651075,"GrossPremi":5601075,"NoClaimBonus":0,"DiscountPremi":0}
        //    List<Otosales.Models.v0219URF2019.CalculatedPremi> calculatedPremiItems = BasicPremiCalculation(ComprePeriod.ToString(), TLOPeriod.ToString(), sperfrom, sperto, Type, UsageCode, CityCode, Year, sitting.ToString(), "GDN52", "219650000"
        //        , "219650000", "ALLRIK", "", "366", BrandCode, ModelCode, "0");
        //    string sCalculadPremiList = JsonConvert.SerializeObject(calculatedPremiItems);
        //    string paramCover = "State=COVER&GuidTempPenawaran=" + GuidTempPenawaran + "&isMvGodig=false&isNonMvGodig=true&OrderSimulation=" + stringOS + "&OrderSimulationMV=" + stringOMV + "&calculatedPremiItems=" + sCalculadPremiList + "&PremiumModel=";
        //    string jsonCover = POSTurlencodedWithBearer("v0219URF2019/DataReact/SaveProspect/", paramCover);
        //    JObject objCover = JObject.Parse(jsonCover);
        //    UpdateTaskDetailNoSurvey(CustId, FollowUpNo, pc, null, os, omv, sCalculadPremiList);
        //    Assert.True(Convert.ToBoolean(obj["status"]));
        //}
        ////[TestMethod]
        //public void UpdateTaskDetailNoSurvey(string CustID, string FollowUpNo, Otosales.Models.v0219URF2019.ProspectCustomer pc, Otosales.Models.v0219URF2019.ProspectCompany pcy, Otosales.Models.v0219URF2019.OrderSimulation os, Otosales.Models.v0219URF2019.OrderSimulationMV omv, string CalculadPremiList)
        //{
        //    Otosales.Models.vWeb2.PersonalData pd = new Otosales.Models.vWeb2.PersonalData();
        //    pd.Name = pc.Name;
        //    pd.CustBirthDay = DateTime.ParseExact("1995-03-16", "yyyy-MM-dd", CultureInfo.InvariantCulture);
        //    pd.CustGender = "M"; pd.CustAddress = "edawefsrthfyuj"; pd.PostalCode = "10140"; pd.Email1 = pc.Email1; pd.Phone1 = pc.Phone1;
        //    pd.IdentityNo = "jhi45394857t34"; pd.IsPayerCompany = false; pd.isNeedDocRep = false; pd.isCompany = false; pd.SalesOfficerID = pc.SalesOfficerID;
        //    Otosales.Models.vWeb2.VehicleData vd = new Otosales.Models.vWeb2.VehicleData();
        //    vd.Vehicle = ""; vd.ProductTypeCode = omv.ProductTypeCode; vd.ProductCode = os.ProductCode;
        //    vd.VehicleCode = omv.VehicleCode; vd.BrandCode = omv.BrandCode; vd.ModelCode = omv.ModelCode;
        //    vd.Series = omv.Series; vd.Type = omv.Type; vd.Sitting = omv.Sitting; vd.Year = omv.Year;
        //    vd.CityCode = omv.VehicleCode; vd.InsuranceType = os.InsuranceType; vd.SumInsured = omv.SumInsured;
        //    vd.SegmentCode = "P1C100"; vd.UsageCode = omv.UsageCode; vd.AccessSI = 0; vd.RegistrationNumber = omv.RegistrationNumber;
        //    vd.EngineNumber = omv.EngineNumber; vd.ChasisNumber = omv.ChasisNumber; vd.IsNew = false; vd.VANumber = ""; vd.Remarks = "";
        //    vd.IsPolicyIssuedBeforePaying = false; vd.IsORDefectsRepair = false; vd.IsAccessoriesChange = false; vd.DealerCode = os.DealerCode;
        //    vd.SalesDealer = os.SalesDealer; vd.ColorOnBPKB = "sgdfhj"; vd.PeriodFrom = os.PeriodFrom; vd.PeriodTo = os.PeriodTo;
        //    Otosales.Models.vWeb2.PolicyAddress pa = new Otosales.Models.vWeb2.PolicyAddress();
        //    pa.SentTo = "BR"; pa.Name = "ERLIN NURSANTY"; pa.Address = "jl Raya Barat Boulevard LC 6 No.21-22 KGD Jakarta"; pa.PostalCode = "14240";
        //    Otosales.Models.vWeb2.SurveySchedule ss = new Otosales.Models.vWeb2.SurveySchedule();
        //    ss.CityCode = ""; ss.LocationCode = ""; ss.SurveyAddress = ""; ss.SurveyPostalCode = ""; ss.SurveyDate = null; ss.ScheduleTimeID = null;
        //    ss.IsNeedSurvey = false; ss.IsNSASkipSurvey = false; ss.IsManualSurvey = false;
        //    List<Otosales.Models.vWeb2.ImageDataTaskDetail> id = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.ImageDataTaskDetail>>("[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"e732394b-2752-4c71-a61d-a404d52033b0.png\"},{\"ImageType\":\"STNK\",\"PathFile\":\"3b349dde-064a-459c-beb2-49164b113a86.png\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"29e245af-8f5c-411d-88e4-25c315ba2169.png\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"3cff00cb-247a-4457-b489-c8d260b188f1.png\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}]");
        //    //string sparam = "{\"CustID\":\""+CustID+"\",\"FollowUpNo\":\""+FollowUpNo+"\",\"PersonalData\":{\"Name\":\"UnitTest2\",\"CustBirthDay\":\"2020-03-16 12:00:00\",\"CustGender\":\"M\",\"CustAddress\":\"edawefsrthfyuj\",\"PostalCode\":\"10140\",\"Email1\":\"hjaf@mail.com\",\"Phone1\":\"087826598723\",\"IdentityNo\":\"jhi45394857t34\",\"IsPayerCompany\":false,\"isNeedDocRep\":0,\"isCompany\":false,\"SalesOfficerID\":\"ENU\"},\"CompanyData\":{\"CompanyName\":\"\",\"NPWPno\":\"\",\"NPWPdate\":null,\"NPWPaddres\":\"\",\"SIUPno\":\"\",\"PKPno\":\"\",\"PKPdate\":null,\"OfficeAddress\":\"\",\"PostalCode\":\"\",\"OfficePhoneNo\":\"\",\"PICPhoneNo\":\"\",\"PICname\":\"\",\"PICEmail\":\"\"},\"VehicleData\":{\"Vehicle\":\"\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GDN52\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"InsuranceType\":1,\"SumInsured\":219650000,\"SegmentCode\":\"P1C100\",\"UsageCode\":\"U00002\",\"AccessSI\":0,\"RegistrationNumber\":\"B0983IL\",\"EngineNumber\":\"WERTHJDFHSF\",\"ChasisNumber\":\"HJFHFGSKHLJS\",\"IsNew\":false,\"BankID\":\"\",\"VANumber\":\"\",\"Remarks\":\"\",\"IsPolicyIssuedBeforePaying\":0,\"IsORDefectsRepair\":0,\"IsAccessoriesChange\":0,\"DealerCode\":0,\"SalesDealer\":0,\"ColorOnBPKB\":\"sgdfhj\",\"PeriodFrom\":\"2020-03-12 12:00:00\",\"PeriodTo\":\"2022-03-12 12:00:00\"},\"PolicyAddress\":{\"SentTo\":\"BR\",\"Name\":\"ERLIN NURSANTY\",\"Address\":\"jl Raya Barat Boulevard LC 6 No. 21-22  KGD Jakarta\",\"PostalCode\":\"14240\"},\"SurveySchedule\":{\"CityCode\":\"\",\"LocationCode\":\"\",\"SurveyAddress\":\"\",\"SurveyPostalCode\":\"\",\"SurveyDate\":null,\"ScheduleTimeID\":null,\"IsNeedSurvey\":false,\"IsNSASkipSurvey\":false,\"IsManualSurvey\":false},\"ImageData\":[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"e732394b-2752-4c71-a61d-a404d52033b0.png\"},{\"ImageType\":\"STNK\",\"PathFile\":\"3b349dde-064a-459c-beb2-49164b113a86.png\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"29e245af-8f5c-411d-88e4-25c315ba2169.png\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"3cff00cb-247a-4457-b489-c8d260b188f1.png\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}],\"Remarks\":{\"RemarkToSA\":null},\"OSData\":\"\",\"OSMVData\":\"\",\"calculatedPremiItems\":{\"ListCalculatePremi\":[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":2,\"PeriodFrom\":\"2020-03-12 00:00:00.000\",\"PeriodTo\":\"2021-03-12 00:00:00.000\",\"Rate\":4.97,\"ExcessRate\":4.97,\"Premium\":10916605,\"CoverPremium\":10916605,\"GrossPremium\":10916605,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":10916605,\"Net2\":10916605,\"Net3\":10916605,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":2,\"PeriodFrom\":\"2021-03-12 00:00:00.000\",\"PeriodTo\":\"2022-03-12 00:00:00.000\",\"Rate\":4.97,\"ExcessRate\":4.97,\"Premium\":8733284,\"CoverPremium\":8733284,\"GrossPremium\":8733284,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":8733284,\"Net2\":8733284,\"Net3\":8733284,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"TPLPER\",\"CoverageID\":\"MVTP02\",\"Year\":1,\"PeriodFrom\":\"2020-03-12 00:00:00.000\",\"PeriodTo\":\"2022-03-12 00:00:00.000\",\"Rate\":1,\"ExcessRate\":1,\"Premium\":199999.99999999997,\"CoverPremium\":200000,\"GrossPremium\":200000,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":200,\"Ndays\":730,\"Net1\":200000,\"Net2\":200000,\"Net3\":200000,\"SumInsured\":10000000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0}],\"OSData\":{\"OrderNo\":\""+OrderNo+"\",\"CustID\":\"11cb9ea4-576a-4bc0-9459-0d402b4d22cc\",\"FollowUpNo\":\"6badd5b5-363e-4530-b1b5-b27dac5b508b\",\"TLOPeriod\":0,\"ComprePeriod\":2,\"BranchCode\":\"016\",\"SalesOfficerID\":\"ENU\",\"PhoneSales\":\"\",\"SalesDealer\":0,\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GDN52\",\"Phone1\":\"087826598723\",\"Email1\":\"hjaf@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"AdminFee\":50000,\"MultiYearF\":true,\"TotalPremi\":19899889,\"YearCoverage\":2,\"LastInterestNo\":0,\"LastCoverageNo\":0},\"OSMVData\":{\"OrderNo\":\""+OrderNo+"\",\"ObjectNo\":1,\"AccessSI\":0,\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B0983IL\",\"EngineNumber\":\"WERTHJDFHSF\",\"ChasisNumber\":\"HJFHFGSKHLJS\",\"IsNew\":false}},\"FollowUpStatus\":1}";
        //    Otosales.Models.vWeb2.CalculatePremiModel cpm = new Otosales.Models.vWeb2.CalculatePremiModel();
        //    cpm.ListCalculatePremi = JsonConvert.DeserializeObject<List<Otosales.Models.vWeb2.CalculatedPremiItems>>(CalculadPremiList);
        //    Otosales.Models.vWeb2.OSData osd = new Otosales.Models.vWeb2.OSData();
        //    osd.OrderNo = os.OrderNo; osd.TLOPeriod = os.TLOPeriod; osd.ComprePeriod = os.ComprePeriod; osd.ProductTypeCode = os.ProductTypeCode;
        //    osd.ProductCode = os.ProductCode; osd.InsuranceType = os.InsuranceType; osd.AdminFee = os.AdminFee; osd.MultiYearF = os.MultiYearF;
        //    osd.YearCoverage = os.YearCoverage; osd.LastInterestNo = os.LastInterestNo; osd.LastCoverageNo = os.LastCoverageNo;
        //    Otosales.Models.vWeb2.OSMVData omvd = new Otosales.Models.vWeb2.OSMVData();
        //    omvd.OrderNo = omv.OrderNo; omvd.ProductTypeCode = omv.ProductTypeCode; omvd.SumInsured = omv.SumInsured; omvd.AccessSI = omv.AccessSI;
        //    cpm.OSData = osd; cpm.OSMVData = omvd;
        //    Otosales.Models.v0213URF2019.UpdateTaskDetailParam param = new Otosales.Models.v0213URF2019.UpdateTaskDetailParam();
        //    param.CustID = CustID;
        //    param.FollowUpNo = FollowUpNo;
        //    param.PersonalData = pd;
        //    param.CompanyData = new Otosales.Models.vWeb2.CompanyData();
        //    param.VehicleData = vd;
        //    param.policyAddress = pa;
        //    param.surveySchedule = ss;
        //    param.imageData = id;
        //    param.calculatedPremiItems = cpm;
        //    param.FollowUpStatus = "1";
        //    string jsonParam = JsonConvert.SerializeObject(param);
        //    string json = POSTjsondWithBearer("v0213URF2019/DataReact/UpdateTaskDetail/", jsonParam);
        //    JObject obj = JObject.Parse(json);
        //}
        ////[TestMethod]
        //public List<Otosales.Models.v0219URF2019.CalculatedPremi> BasicPremiCalculation(string ComprePeriod, string TLOPeriod, string PeriodFrom, string PeriodTo
        //    , string vtype, string vcitycode, string vusagecode, string vyear, string vsitting, string vProductCode
        //    , string vTSInterest, string vPrimarySI, string vCoverageId, string pQuotationNo, string Ndays, string vBrand, string vModel, string isNew)
        //{
        //    //"ComprePeriod=2&TLOPeriod=0&PeriodFrom=2020-03-12&PeriodTo=2022-03-12&vtype=T00011&vcitycode=000020&vusagecode=U00002&vyear=2019&vsitting=7&vProductCode=GDN52&vTSInterest=219650000&vPrimarySI=219650000&vCoverageId=ALLRIK&pQuotationNo=&Ndays=366&vBrand=B25&vModel=M01048&isNew=0"
        //    string param = "";
        //    param = "ComprePeriod=" + ComprePeriod + "&TLOPeriod=" + TLOPeriod + "&PeriodFrom=" + PeriodFrom + "&PeriodTo=" + PeriodTo + "&vtype=" + vtype
        //        + "&vcitycode=" + vcitycode + "&vusagecode=" + vusagecode + "&vyear=" + vyear + "&vsitting=" + vsitting + "&vProductCode=" + vProductCode
        //        + "&vTSInterest=" + vTSInterest + "&vPrimarySI=" + vPrimarySI + "&vCoverageId=" + vCoverageId + "&pQuotationNo=" + pQuotationNo
        //        + "&Ndays=" + Ndays + "&vBrand=" + vBrand + "&vModel=" + vModel + "&isNew=" + isNew;
        //    string json = POSTurlencodedWithBearer("vWeb2/DataReact/BasicPremiCalculation/", param);
        //    JObject res = JObject.Parse(json);
        //    List<Otosales.Models.v0219URF2019.CalculatedPremi> cal = JsonConvert.DeserializeObject<List<Otosales.Models.v0219URF2019.CalculatedPremi>>(res["CalculatedPremiItems"].ToString());
        //    return cal;
        //}

        #region NewGetFollowUpStatus
        [Theory(DisplayName = "GetFollowUpStatus_ReturnFollowUpStatus")]
        [InlineData(new object[] { "1", "1", "0", "2b3a713d-1180-40f8-92fb-b316c2312dc4" })]
        public void GetFollowUpStatus_ReturnFollowUpStatus(string followupstatus, string followupstatusinfo, string isRenewal, string followupno)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("followupstatus", followupstatus),
                new KeyValuePair<string, string>("followupstatusinfo", followupstatusinfo),
                new KeyValuePair<string, string>("isRenewal", isRenewal),
                new KeyValuePair<string, string>("followupno", followupno)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0001URF2020.DataReactController();
            var result = controller.getFollowUpStatus(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            foreach (dynamic dt in data)
            {
                Assert.True(!string.IsNullOrEmpty(dt.Description));
            }
        }
        [Fact(DisplayName = "GetFollowUpStatus_ReturnNullParam")]
        public void GetFollowUpStatus_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string followupstatus = ""; string followupstatusinfo = ""; string isRenewal = ""; string followupno = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("followupstatus", followupstatus),
                new KeyValuePair<string, string>("followupstatusinfo", followupstatusinfo),
                new KeyValuePair<string, string>("isRenewal", isRenewal),
                new KeyValuePair<string, string>("followupno", followupno)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0001URF2020.DataReactController();
            var result = controller.getFollowUpStatus(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Param is null", message);
            Assert.Equal("No Data", data);
        }
        [Fact(DisplayName = "GetFollowUpStatus_ReturnNullParam")]
        public void GetFollowUpStatus_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string followupstatus = "1"; string followupstatusinfo = "1"; string isRenewal = "1"; string followupno = "0";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("followupstatus", followupstatus),
                new KeyValuePair<string, string>("followupstatusinfo", followupstatusinfo),
                new KeyValuePair<string, string>("isRenewal", isRenewal),
                new KeyValuePair<string, string>("followupno", followupno)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            var mock = new Mock<Repository.v0001URF2020.IMobileRepository>();
            mock.Setup(o => o.getFollowUpStatus(followupstatus, followupstatusinfo, isRenewal, followupno)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0001URF2020.DataReactController(mock.Object);
            var result = controller.getFollowUpStatus(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
            //Assert.Equal("null", data);
        }
        #endregion
        #region OldGetFollowUpStatus
        //        public JObject GetFollowUpStatus(string followupstatus, string followupstatusinfo, string followupno, string isRenewal
        //, string followupstatusbefore, string followupstatusinfobefore, string isMVGodig)
        //        {
        //            try
        //            {
        //                string param = string.Empty;
        //                param = "followupstatus=" + followupstatus + "&followupstatusinfo=" + followupstatusinfo + "&followupno=" + followupno + "&isRenewal=" + isRenewal + "&followupstatusbefore=" + followupstatusbefore + "&followupstatusinfobefore=" + followupstatusinfobefore + "&isMVGodig=" + isMVGodig;
        //                string json = POSTurlencodedWithBearer("v0001URF2020/DataReact/getFollowUpStatus/", param);
        //                JObject obj1 = JObject.Parse(json);
        //                return obj1;
        //            }
        //            catch (Exception e)
        //            {
        //                return null;
        //            }
        //        }

        //        //[TestMethod]
        //        [Fact(DisplayName = "GetFollowUpStatus - FUSurveyResult")]
        //        public void GetFollowUpStatusFUSurveyResult()
        //        {
        //            try
        //            {
        //                JObject res = GetFollowUpStatus("2", "59", "7e3e02b3-931e-4c63-abe2-3ac0e49970b0", "0", "-1", "-1", "0");
        //                if (res["status"].ToString() == "1")
        //                {
        //                    JArray datas = JArray.Parse(res["data"].ToString());
        //                    Assert.True(datas.Count == 3);

        //                    string[] listStatus = { "2", "5", "3" };
        //                    foreach (var data in datas)
        //                    {
        //                        int pos = Array.IndexOf(listStatus, (string)data["StatusCode"]);
        //                        Assert.True(pos >= 0);
        //                    }
        //                }
        //                else
        //                {
        //                    Assert.False(Convert.ToBoolean(res["status"]));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Assert.NotNull(null);
        //            }
        //        }
        //        //[TestMethod]
        //        [Fact(DisplayName = "GetFollowUpStatus - SendToSurveyor")]
        //        public void GetFollowUpStatusSendToSurveyor()
        //        {
        //            try
        //            {
        //                JObject res = GetFollowUpStatus("2", "58", "cbc967f1-26ae-44a6-be09-fab8033ae5ae", "0", "-1", "-1", "0");
        //                if (res["status"].ToString() == "1")
        //                {
        //                    JArray datas = JArray.Parse(res["data"].ToString());
        //                    Assert.True(datas.Count == 1);

        //                    string[] listStatus = { "3" };
        //                    foreach (var data in datas)
        //                    {
        //                        int pos = Array.IndexOf(listStatus, (string)data["StatusCode"]);
        //                        Assert.True(pos >= 0);
        //                    }
        //                }
        //                else
        //                {
        //                    Assert.False(Convert.ToBoolean(res["status"]));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Assert.NotNull(null);
        //            }
        //        }
        //        //[TestMethod]
        //        [Fact(DisplayName = "GetFollowUpStatus - SendToSA")]
        //        public void GetFollowUpStatusSendToSA()
        //        {
        //            try
        //            {
        //                JObject res = GetFollowUpStatus("2", "61", "cbc967f1-26ae-44a6-be09-fab8033ae5ae", "0", "-1", "-1", "0");
        //                if (res["status"].ToString() == "1")
        //                {
        //                    JArray datas = JArray.Parse(res["data"].ToString());
        //                    Assert.True(datas.Count == 1);

        //                    string[] listStatus = { "3" };
        //                    foreach (var data in datas)
        //                    {
        //                        int pos = Array.IndexOf(listStatus, (string)data["StatusCode"]);
        //                        Assert.True(pos >= 0);
        //                    }
        //                }
        //                else
        //                {
        //                    Assert.False(Convert.ToBoolean(res["status"]));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Assert.NotNull(null);
        //            }
        //        }
        //        //[TestMethod]
        //        [Fact(DisplayName = "GetFollowUpStatus - SendToSurveyor")]
        //        public void GetFollowUpStatusPolicyCreated()
        //        {
        //            try
        //            {
        //                JObject res = GetFollowUpStatus("2", "51", "cbc967f1-26ae-44a6-be09-fab8033ae5ae", "0", "-1", "-1", "0");
        //                if (res["status"].ToString() == "1")
        //                {
        //                    JArray datas = JArray.Parse(res["data"].ToString());
        //                    Assert.True(datas.Count == 1);

        //                    string[] listStatus = { "3" };
        //                    foreach (var data in datas)
        //                    {
        //                        int pos = Array.IndexOf(listStatus, (string)data["StatusCode"]);
        //                        Assert.True(pos >= 0);
        //                    }
        //                }
        //                else
        //                {
        //                    Assert.False(Convert.ToBoolean(res["status"]));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Assert.NotNull(null);
        //            }
        //        }
        //        //[TestMethod]
        //        [Fact(DisplayName = "GetFollowUpStatus - PolicyApproved")]
        //        public void GetFollowUpStatusPolicyApproved()
        //        {
        //            try
        //            {
        //                JObject res = GetFollowUpStatus("13", "50", "136906e4-ff5f-441b-909e-b15b4b2ca698", "0", "-1", "-1", "0");
        //                if (res["status"].ToString() == "1")
        //                {
        //                    JArray datas = JArray.Parse(res["data"].ToString());
        //                    Assert.True(datas.Count == 1);

        //                    string[] listStatus = { "3" };
        //                    foreach (var data in datas)
        //                    {
        //                        int pos = Array.IndexOf(listStatus, (string)data["StatusCode"]);
        //                        Assert.True(pos >= 0);
        //                    }
        //                }
        //                else
        //                {
        //                    Assert.False(Convert.ToBoolean(res["status"]));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Assert.NotNull(null);
        //            }
        //        }
        //        //[TestMethod]
        //        [Fact(DisplayName = "GetFollowUpStatus - SurveyReject")]
        //        public void GetFollowUpStatusSurveyReject()
        //        {
        //            try
        //            {
        //                JObject res = GetFollowUpStatus("5", "38", "00193303-731a-44ab-85bd-9672aaf03d18", "0", "-1", "-1", "0");
        //                if (res["status"].ToString() == "1")
        //                {
        //                    JArray datas = JArray.Parse(res["data"].ToString());
        //                    Assert.True(datas.Count == 1);

        //                    string[] listStatus = { "5" };
        //                    foreach (var data in datas)
        //                    {
        //                        int pos = Array.IndexOf(listStatus, (string)data["StatusCode"]);
        //                        Assert.True(pos >= 0);
        //                    }
        //                }
        //                else
        //                {
        //                    Assert.False(Convert.ToBoolean(res["status"]));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Assert.NotNull(null);
        //            }
        //        }
        //        //[TestMethod]
        //        [Fact(DisplayName = "GetFollowUpStatus - FUPembayaran")]
        //        public void GetFollowUpStatusFUPembayaran()
        //        {
        //            try
        //            {
        //                JObject res = GetFollowUpStatus("2", "51", "cbc967f1-26ae-44a6-be09-fab8033ae5ae", "0", "-1", "-1", "0");
        //                if (res["status"].ToString() == "1")
        //                {
        //                    JArray datas = JArray.Parse(res["data"].ToString());
        //                    Assert.True(datas.Count == 1);

        //                    string[] listStatus = { "3" };
        //                    foreach (var data in datas)
        //                    {
        //                        int pos = Array.IndexOf(listStatus, (string)data["StatusCode"]);
        //                        Assert.True(pos >= 0);
        //                    }
        //                }
        //                else
        //                {
        //                    Assert.False(Convert.ToBoolean(res["status"]));
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                Assert.NotNull(null);
        //            }
        //        } 
        #endregion

        #region NewGetFollowUpStatusInfo
        [Theory(DisplayName = "GetFollowUpStatusInfo_ReturnTaskFollowUpStatusInfo")]
        [InlineData(new object[] { "2", "3", "46", "0"
            , "0", "0", "e9e7e1a7-8ab0-4739-ba69-d6ca0f718422", "0" })]
        public void GetFollowUpStatusInfo_ReturnFollowUpStatusInfo(string statuscode, string statuscodebefore, string statusinfocodebefore, string IsRenewal
            , string NeedSurvey, string NeedNSA, string followUpNo, string isMVGodig)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("statuscode", statuscode),
                new KeyValuePair<string, string>("statuscodebefore", statuscodebefore),
                new KeyValuePair<string, string>("statusinfocodebefore", statusinfocodebefore),
                new KeyValuePair<string, string>("IsRenewal", IsRenewal),
                new KeyValuePair<string, string>("NeedSurvey", NeedSurvey),
                new KeyValuePair<string, string>("NeedNSA", NeedNSA),
                new KeyValuePair<string, string>("followUpNo", followUpNo),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0219URF2019.DataReactController();
            var result = controller.getFollowUpDetailsStatusInfo(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.True(data[0].InfoCode != null);
        }
        [Fact(DisplayName = "GetFollowUpStatusInfo_ReturnException")]
        public void GetFollowUpStatusInfo_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string statuscode = "2"; string statuscodebefore = "3"; string statusinfocodebefore = "46";
            string IsRenewal = "0"; string NeedSurvey = "0"; string NeedNSA = "0";
            string followUpNo = "e9e7e1a7-8ab0-4739-ba69-d6ca0f718422"; string isMVGodig = "jhgfsk";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("statuscode", statuscode),
                new KeyValuePair<string, string>("statuscodebefore", statuscodebefore),
                new KeyValuePair<string, string>("statusinfocodebefore", statusinfocodebefore),
                new KeyValuePair<string, string>("IsRenewal", IsRenewal),
                new KeyValuePair<string, string>("NeedSurvey", NeedSurvey),
                new KeyValuePair<string, string>("NeedNSA", NeedNSA),
                new KeyValuePair<string, string>("followUpNo", followUpNo),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0219URF2019.DataReactController();
            var result = controller.getFollowUpDetailsStatusInfo(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion
        #region OldGetFollowUpStatusInfo
        //    public JObject GetFollowUpStatusInfo(string statuscode, string statuscodebefore, string statusinfocodebefore, string isRenewal
        //, string NeedSurvey, string NeedNSA, string followUpNo, string isMVGodig)
        //    {
        //        try
        //        {
        //            string param = string.Empty;
        //            param = "statuscode=" + statuscode + "&statuscodebefore=" + statuscodebefore + "&statusinfocodebefore=" + statusinfocodebefore + "&isRenewal=" + isRenewal + "&NeedSurvey=" + NeedSurvey + "&NeedNSA=" + NeedNSA + "&followUpNo=" + followUpNo + "&isMVGodig=" + isMVGodig;
        //            string json = POSTurlencodedWithBearer("v0219URF2019/DataReact/getFollowUpDetailsStatusInfo/", param);
        //            JObject obj1 = JObject.Parse(json);
        //            return obj1;
        //        }
        //        catch (Exception e)
        //        {
        //            return null;
        //        }
        //    }
        //    //[TestMethod]
        //    [Fact(DisplayName = "GetFollowUpDetailsStatusInfo - CallLaterFUPembayaranNoReason")]
        //    public void GetFollowUpDetailsStatusInfoCallLaterFUPembayaranNoReason()
        //    {
        //        try
        //        {
        //            JObject res = GetFollowUpStatusInfo("3", "2", "57", "0", "1", "0", "26ed9ac6-2f29-459b-a279-55ff2d37971e", "0");
        //            if (res["status"].ToString() == "1")
        //            {
        //                JArray datas = JArray.Parse(res["data"].ToString());
        //                Assert.True(datas.Count == 0);
        //            }
        //            else
        //            {
        //                Assert.False(Convert.ToBoolean(res["status"]));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Assert.NotNull(null);
        //        }
        //    }
        //    //[TestMethod]
        //    [Fact(DisplayName = "GetFollowUpDetailsStatusInfo - CallLaterSendToSANoReason")]
        //    public void GetFollowUpDetailsStatusInfoCallLaterSendToSANoReason()
        //    {
        //        try
        //        {
        //            JObject res = GetFollowUpStatusInfo("3", "2", "61", "0", "0", "0", "2d9fdbae-36a8-4b13-b651-26f60a5915a4", "0");
        //            if (res["status"].ToString() == "1")
        //            {
        //                JArray datas = JArray.Parse(res["data"].ToString());
        //                Assert.True(datas.Count == 0);
        //            }
        //            else
        //            {
        //                Assert.False(Convert.ToBoolean(res["status"]));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Assert.NotNull(null);
        //        }
        //    }
        //    //[TestMethod]
        //    [Fact(DisplayName = "GetFollowUpDetailsStatusInfo  -CallLaterSendToSurveyorNoReason")]
        //    public void GetFollowUpDetailsStatusInfoCallLaterSendToSurveyorNoReason()
        //    {
        //        try
        //        {
        //            JObject res = GetFollowUpStatusInfo("3", "2", "58", "0", "1", "0", "a00a9988-f053-435f-8036-13fb47eae164", "0");
        //            if (res["status"].ToString() == "1")
        //            {
        //                JArray datas = JArray.Parse(res["data"].ToString());
        //                Assert.True(datas.Count == 0);
        //            }
        //            else
        //            {
        //                Assert.False(Convert.ToBoolean(res["status"]));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Assert.NotNull(null);
        //        }
        //    }
        //    //[TestMethod]
        //    [Fact(DisplayName = "GetFollowUpDetailsStatusInfo - CallLaterPolicyApprovedNoReason")]
        //    public void GetFollowUpDetailsStatusInfoCallLaterPolicyApprovedNoReason()
        //    {
        //        try
        //        {
        //            JObject res = GetFollowUpStatusInfo("3", "13", "50", "1", "0", "0", "479d667d-62c2-44a4-a0ed-4c499c48bd51", "0");
        //            if (res["status"].ToString() == "1")
        //            {
        //                JArray datas = JArray.Parse(res["data"].ToString());
        //                Assert.True(datas.Count == 0);
        //            }
        //            else
        //            {
        //                Assert.False(Convert.ToBoolean(res["status"]));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Assert.NotNull(null);
        //        }
        //    }
        //    //[TestMethod]
        //    [Fact(DisplayName = "GetFollowUpDetailsStatusInfo - CallLaterFUSurveyResultNSAAproval")]
        //    public void GetFollowUpDetailsStatusInfoCallLaterFUSurveyResultNSAAproval()
        //    {
        //        try
        //        {
        //            JObject res = GetFollowUpStatusInfo("3", "2", "59", "0", "1", "0", "03fa823d-4b84-463f-9dd1-edb373124cac", "0");
        //            if (res["status"].ToString() == "1")
        //            {
        //                JArray datas = JArray.Parse(res["data"].ToString());
        //                Assert.True(datas.Count == 1);
        //                Assert.True(Convert.ToInt32(datas[0]["InfoCode"]) == 46);
        //            }
        //            else
        //            {
        //                Assert.False(Convert.ToBoolean(res["status"]));
        //            }
        //        }
        //        catch (Exception e)
        //        {
        //            Assert.NotNull(null);
        //        }
        //    } 
        #endregion

        #region newBasicCoverUnitTest
        [Theory(DisplayName = "GetBasicCover_ReturnListBasicCover")]
        [InlineData(new object[] { "2018", "False" })]
        [InlineData(new object[] { "2005", "False" })]
        [InlineData(new object[] { "2015", "False" })]
        [InlineData(new object[] { "2015", "false", "true" })]        
        //[InlineData(new object[] { "" })]
        public void GetBasicCover_ReturnListBasicCover(string year, string isBasic, string isMVGodig = "false")
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("vyear", year),
                new KeyValuePair<string, string>("isBasic", isBasic),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0063URF2020.DataReactController();
            var result = controller.getBasicCover(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic BasicCover = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("BasicCover").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.True(BasicCover.Count > 0);
        }
        [Theory(DisplayName = "GetBasicCover_ReturnListBasicCover_WithMock")]
        [InlineData(new object[] { "2018", "False" })]
        [InlineData(new object[] { "2005", "False" })]
        [InlineData(new object[] { "2015", "False" })]
        [InlineData(new object[] { "2015", "false", "true" })]
        public void GetBasicCover_ReturnListBasicCover_WithMock(string year, string isBasic, string isMVGodig = "false")
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("vyear", year),
                new KeyValuePair<string, string>("isBasic", isBasic),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            List<Otosales.Models.v0219URF2019.BasicCover> ListBasicCover = new List<Otosales.Models.v0219URF2019.BasicCover>();
            ListBasicCover.Add(new Otosales.Models.v0219URF2019.BasicCover()
            {
                Id = 0,
                CoverageId = "",
                ComprePeriod = 0,
                TLOPeriod = 0,
                Description = ""
            });

            var mock = new Mock<Repository.v0063URF2020.IMobileRepository>();
            mock.Setup(o => o.GetBasicCover(Convert.ToInt32(year), Convert.ToBoolean(isBasic), Convert.ToBoolean(isMVGodig))).Returns(ListBasicCover);

            // Act
            var controller = new Controllers.v0063URF2020.DataReactController(mock.Object);
            var result = controller.getBasicCover(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic BasicCover = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("BasicCover").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.Equal(ListBasicCover, BasicCover);
        }
        [Fact(DisplayName = "GetBasicCover_ReturnException")]
        public void GetBasicCover_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string vyear = "2019", isBasic = "false", isMVGodig = "false";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("isBasic", isBasic),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            //var exception = FormatterServices.GetUninitializedObject(typeof(SqlException)) as SqlException;

            var mock = new Mock<Repository.v0063URF2020.IMobileRepository>();
            mock.Setup(o => o.GetBasicCover(Convert.ToInt32(vyear), Convert.ToBoolean(isBasic), Convert.ToBoolean(isMVGodig))).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0063URF2020.DataReactController(mock.Object);
            var result = controller.getBasicCover(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("null", data);
        }
        #endregion
        #region oldBasicCoverUnitTest
        //public List<Models.v0219URF2019.BasicCover> GetBasicCover(string year, string isBasic, string isMVGodig = "false")
        //{
        //    string json = POSTurlencodedWithBearer("v0063URF2020/DataReact/getBasicCover", "vYear=" + year + "&isBasic=" + isBasic + "&isMvGodig=" + isMVGodig);
        //    JObject obj = JObject.Parse(json);
        //    List<Models.v0219URF2019.BasicCover> res = JsonConvert.DeserializeObject<List<Models.v0219URF2019.BasicCover>>(obj["BasicCover"].ToString());
        //    return res;
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBasicCover - VAgeLessThan5Year")]
        //public void GetBasicWithVAgeLessThan5Year()
        //{
        //    List<Models.v0219URF2019.BasicCover> res = GetBasicCover("2018", "false");

        //    Assert.True(res.Count == 9);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBasicCover - VAgeMoreThan5Year")]
        //public void GetBasicWithVAgeMoreThan5Year()
        //{
        //    List<Models.v0219URF2019.BasicCover> res = GetBasicCover("2005", "false");

        //    Assert.True(res.Count == 2);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBasicCover - VAge5Year")]
        //public void GetBasicWithVAge5Year()
        //{
        //    List<Models.v0219URF2019.BasicCover> res = GetBasicCover("2015", "false");

        //    Assert.True(res.Count == 9);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBasicCover - VAge5Year")]
        //public void GetBasicCoverAll()
        //{
        //    List<Models.v0219URF2019.BasicCover> res = GetBasicCover("2015", "true");

        //    Assert.True(res.Count == 22);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBasicCover - Godig")]
        //public void GetBasicCoverGodig()
        //{
        //    List<Models.v0219URF2019.BasicCover> res = GetBasicCover("2015", "false", "true");

        //    Assert.True(res.Count == 2);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBasicCover - WrongParam")]
        //public void GetBasicCoverWrongParam()
        //{
        //    string json = POSTurlencodedWithBearer("v0063URF2020/DataReact/getBasicCover", "vYear=2015isBasic=3&isMvGodig=3");
        //    JObject obj = JObject.Parse(json);
        //    Assert.True(Convert.ToBoolean(obj["status"]) == false && obj["data"].ToString().Equals("null"));
        //}
        #endregion

        #region NewGetBankInformation
        [Fact(DisplayName = "GetBankInformation_ReturnBankInformation")]
        public void GetBankInformation_ReturnBankInformation()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string SalesmanCode = "110254";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("SalesmanCode", SalesmanCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.GetBankInformation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            Assert.NotEmpty(data.AccountNo);
        }
        [Fact(DisplayName = "GetBankInformation_ReturnErrorMessage")]
        public void GetBankInformation_ReturnErrorMessage()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string SalesmanCode = "110257";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("SalesmanCode", SalesmanCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.GetBankInformation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            Assert.NotEmpty(data.ErrorMessage);
        }
        [Fact(DisplayName = "GetBankInformation_ReturnException")]
        public void GetBankInformation_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string SalesmanCode = "110257";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("SalesmanCode", SalesmanCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IMobileRepository>();
            mock.Setup(o => o.GetBankInformation(SalesmanCode)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.GetBankInformation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion
        #region OldGetBankInformation
        //public string GetBankInformation(string SalesmanCode)
        //{
        //    string json = POSTurlencodedWithBearer("vWeb2/DataReact/GetBankInformation/", "SalesmanCode=" + SalesmanCode);
        //    JObject obj = JObject.Parse(json);
        //    string res = obj["data"].ToString();

        //    return res;
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBankInformation - Succes")]
        //public void GetBankInformationSucces()
        //{
        //    Otosales.Models.vWeb2.BankInformationResult bankinfo = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.BankInformationResult>(GetBankInformation("26351"));

        //    Assert.True(!string.IsNullOrEmpty(bankinfo.BankName) && !string.IsNullOrEmpty(bankinfo.AccountNo) && string.IsNullOrEmpty(bankinfo.ErrorMessage));
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBankInformation - NotRegisteredSalesmanCode")]
        //public void GetBankWithNotRegisteredSalesmanCode()
        //{
        //    Otosales.Models.vWeb2.BankInformationResult bankinfo = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.BankInformationResult>(GetBankInformation("26394"));

        //    Assert.True(string.IsNullOrEmpty(bankinfo.BankName) && string.IsNullOrEmpty(bankinfo.AccountNo) && !string.IsNullOrEmpty(bankinfo.ErrorMessage));
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBankInformation - WrongSalesmanCode")]
        //public void GetBankWithWrongSalesmanCode()
        //{
        //    Otosales.Models.vWeb2.BankInformationResult bankinfo = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.BankInformationResult>(GetBankInformation("21234"));

        //    Assert.True(string.IsNullOrEmpty(bankinfo.BankName) && string.IsNullOrEmpty(bankinfo.AccountNo) && string.IsNullOrEmpty(bankinfo.ErrorMessage));
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBankInformation - WrongParameter")]
        //public void GetBankWithWrongParameter()
        //{
        //    string error = GetBankInformation("wurei");

        //    Assert.Contains("Error", error);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBankInformation - NullParam")]
        //public void GetBankWithNullParam()
        //{
        //    string json = POSTjsondWithBearer("vWeb2/DataReact/GetBankInformation/", "");
        //    JObject obj = JObject.Parse(json);
        //    Assert.Equal("Input Required", obj["Message"].ToString());
        //} 
        #endregion
        [Fact(DisplayName = "GetDealerName_ReturnDealerName")]
        public void GetDealerName_ReturnDealerName()
        {
            // AAA : Assign, Act, Assert
            // Assign

            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.getDealerName();
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            foreach (dynamic dt in data)
            {
                Assert.True(dt.DealerCode != 0);
            }
        }
        #region NewGetSalesmanDealerName
        [Fact(DisplayName = "GetSalesmanDealerName_ReturnSalesmanDealerName")]
        public void GetSalesmanDealerName_ReturnSalesmanDealerName()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string dealercode = "3953";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("dealercode", dealercode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.getSalesmanDealerName(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            foreach (dynamic dt in data)
            {
                Assert.True(dt.DealerCode != 0);
            }
        }
        [Fact(DisplayName = "GetSalesmanDealerName_ReturnNullParam")]
        public void GetSalesmanDealerName_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("dealercode", null)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.getSalesmanDealerName(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Parameter is null", message);
        }
        [Fact(DisplayName = "GetSalesmanDealerName_ReturnException")]
        public void GetSalesmanDealerName_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string dealercode = "3953";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("dealercode", dealercode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IMobileRepository>();
            mock.Setup(o => o.getSalesmanDealerName(dealercode)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.getSalesmanDealerName(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion
        #region OldGetSalesmanDealerName
        //public JObject getSalesmanDealerName(string dealercode)
        //{
        //    string json = POSTurlencodedWithBearer("vWeb2/DataReact/getSalesmanDealerName/", "dealercode=" + dealercode);
        //    JObject obj = JObject.Parse(json);
        //    return obj;
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBankInformation - NullParam")]
        //public void getSalesmanDealerNameSuccess()
        //{
        //    JObject obj = getSalesmanDealerName("3341");
        //    string message = obj["message"].ToString();
        //    List<dynamic> data = JsonConvert.DeserializeObject<List<dynamic>>(obj["data"].ToString());

        //    Assert.True(data.Count > 0 && message.Equals("OK"));
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "GetBankInformation - NullParam")]
        //public void getSalesmanDealerNameWithWrongDealerCode()
        //{
        //    JObject obj = getSalesmanDealerName("1213");
        //    string message = obj["message"].ToString();
        //    List<dynamic> data = JsonConvert.DeserializeObject<List<dynamic>>(obj["data"].ToString());

        //    Assert.True(data.Count == 0 && message.Equals("OK"));
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getSalesmanDealerName - EmptyParameter")]
        //public void getSalesmanDealerNameWithEmptyParameter()
        //{
        //    JObject obj = getSalesmanDealerName("");
        //    string message = obj["message"].ToString();

        //    Assert.True(obj["data"].ToString().Equals("null") && message.Equals("Parameter is null"));
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getSalesmanDealerName - EmptyParameter")]
        //public void getSalesmanDealerNameWithWrongParameter()
        //{
        //    JObject obj = getSalesmanDealerName("jhgj");
        //    string message = obj["message"].ToString();

        //    Assert.True(Convert.ToBoolean(obj["status"]) == false && message.Contains("Error"));
        //} 
        #endregion
        #region NewGetProductType
        [Theory(DisplayName = "GetProductType_ReturnEnableProductType")]
        [InlineData(new object[] { false, "UMA" })]
        [InlineData(new object[] { true, "UMA" })]
        [InlineData(new object[] { false, "LING0001" })]
        public void GetProductType_ReturnEnableProductType(bool isMvGodig, string salesOfficerId)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("isMvGodig", isMvGodig.ToString()),
                new KeyValuePair<string, string>("salesOfficerId", salesOfficerId)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }


            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0063URF2020.DataReactController();
            var result = controller.getProductType(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.True(data.Count > 0);
        }
        [Fact(DisplayName = "GetProductType_ReturnException")]
        public void GetProductType_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param, isMvGodig = "false", salesOfficerId = "UMA";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("isMvGodig", isMvGodig),
                new KeyValuePair<string, string>("salesOfficerId", salesOfficerId)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            //var exception = FormatterServices.GetUninitializedObject(typeof(SqlException)) as SqlException;

            var mock = new Mock<Repository.v0063URF2020.IMobileRepository>();
            mock.Setup(o => o.GetProductType(salesOfficerId, Convert.ToBoolean(isMvGodig))).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0063URF2020.DataReactController(mock.Object);
            var result = controller.getProductType(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion
        #region OldGetProductType
        //public JObject getProductType(string SalesOfficerID, string isMvGodig = "false")
        //{
        //    string json = POSTurlencodedWithBearer("v0219URF2019/DataReact/getProductType/", "salesofficerid=" + SalesOfficerID + "&isMvGodig=" + isMvGodig);
        //    JObject obj = JObject.Parse(json);
        //    return obj;
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductType - All")]
        //public void getProductTypeAll()
        //{
        //    JObject obj = getProductType("UMA");
        //    List<dynamic> data = JsonConvert.DeserializeObject<List<dynamic>>(obj["data"].ToString());
        //    Assert.True(data.Count == 4);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductType - NonSharia")]
        //public void getProductTypeNonSharia()
        //{
        //    JObject obj = getProductType("LING0001");
        //    List<dynamic> data = JsonConvert.DeserializeObject<List<dynamic>>(obj["data"].ToString());
        //    Assert.True(data.Count == 3);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductType - MVGodig")]
        //public void getProductTypeMVGodig()
        //{
        //    JObject obj = getProductType("KNA", "true");
        //    List<dynamic> data = JsonConvert.DeserializeObject<List<dynamic>>(obj["data"].ToString());
        //    Assert.True(data.Count == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductType - WrongParam")]
        //public void getProductTypeWithWrongParam()
        //{
        //    JObject obj = getProductType("KNA", "erewrer23");
        //    Assert.True(Convert.ToBoolean(obj["status"]) == false && obj["message"].ToString().Contains("Error"));
        //}

        #endregion
        #region NewGetProductCode
        [Theory(DisplayName = "GetProductCode_ReturnProductCode")]
        [InlineData(new object[] { "4", "UMA", "" , 0 })]
        [InlineData(new object[] { "1", "UMA", "RFN10001ABC", 0 })]
        [InlineData(new object[] { "2", "UMA", "", 0 })]
        [InlineData(new object[] { "3", "UMA", "", 0 })]
        [InlineData(new object[] { "1", "UMA", "", 1 })]
        [InlineData(new object[] { "1", "UMA", "", 1, true, true })]
        [InlineData(new object[] { "1", "UMA", "", 1, false, true })]
        [InlineData(new object[] { "1", "LING0001", "", 1, true, true })]
        [InlineData(new object[] { "1", "LING0001", "", 0, false, false })]
        [InlineData(new object[] { "1", "LING0001", "", 0, false, true })]
        public void GetProductCode_ReturnProductCode(string insurancetype, string salesofficerid, string searchParam, int isRenewal,bool isNew = false, bool isMVGodig = false)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("insurancetype", insurancetype),
                new KeyValuePair<string, string>("salesofficerid", salesofficerid),
                new KeyValuePair<string, string>("searchParam", searchParam),
                new KeyValuePair<string, string>("isRenewal", isRenewal.ToString()),
                new KeyValuePair<string, string>("isNew", isNew.ToString()),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig.ToString())
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.getProductCode(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            foreach (dynamic dt in data)
            {
                Assert.NotEmpty(dt.ProductCode);
            }
        }
        [Fact(DisplayName = "GetProductCode_ReturnNullParam")]
        public void GetProductCode_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string insurancetype = null; string salesofficerid = "idl"; int isRenewal = 0;
            bool isNew = false, isMVGodig = false;
            string searchParam = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("insurancetype", insurancetype),
                new KeyValuePair<string, string>("salesofficerid", salesofficerid),
                new KeyValuePair<string, string>("searchParam", searchParam),
                new KeyValuePair<string, string>("isRenewal", isRenewal.ToString()),
                new KeyValuePair<string, string>("isNew", isNew.ToString()),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig.ToString())
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.getProductCode(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Parameter is null", message);
            Assert.Equal("null", data);
        }
        [Fact(DisplayName = "GetProductCode_ReturnException")]
        public void GetProductCode_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string insurancetype = "1"; string salesofficerid = "idl"; int isRenewal = 0;
            bool isNew = false, isMVGodig = false;
            string searchParam = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("insurancetype", insurancetype),
                new KeyValuePair<string, string>("salesofficerid", salesofficerid),
                new KeyValuePair<string, string>("searchParam", searchParam),
                new KeyValuePair<string, string>("isRenewal", isRenewal.ToString()),
                new KeyValuePair<string, string>("isNew", isNew.ToString()),
                new KeyValuePair<string, string>("isMVGodig", isMVGodig.ToString())
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            mock.Setup(o => o.GetProduct(insurancetype, salesofficerid, searchParam, isRenewal, isNew, isMVGodig)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.getProductCode(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion
        #region OldProductCode
        //public JObject getProductCode(string insurancetype, string isNew, string salesofficerid, string isRenewal, string isMvGodig)
        //{
        //    string json = POSTurlencodedWithBearer("v0219URF2019/DataReact/getProductCode/", "producttypecode=GARDAOTO&insurancetype=" + insurancetype + "&isNew=" + isNew + "&salesofficerid=" + salesofficerid + "&isRenewal=" + isRenewal + "&isMvGodig=" + isMvGodig);

        //    return JObject.Parse(json);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - GO")]
        //public void getProductCodeGO()
        //{
        //    JObject obj = getProductCode("1", "false", "UMA", "0", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            if (Convert.ToInt32(item["InsuranceType"]) == 1)
        //            {
        //                Assert.True(true);
        //            }
        //            else
        //            {
        //                Assert.True(false);
        //                break;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - TI")]
        //public void getProductCodeTI()
        //{
        //    JObject obj = getProductCode("2", "false", "UMA", "0", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            if (Convert.ToInt32(item["InsuranceType"]) == 2)
        //            {
        //                Assert.True(true);
        //            }
        //            else
        //            {
        //                Assert.True(false);
        //                break;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - LI")]
        //public void getProductCodeLI()
        //{
        //    JObject obj = getProductCode("3", "false", "UMA", "0", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            if (Convert.ToInt32(item["InsuranceType"]) == 3)
        //            {
        //                Assert.True(true);
        //            }
        //            else
        //            {
        //                Assert.True(false);
        //                break;
        //            }

        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - Sharia")]
        //public void getProductCodeSharia()
        //{
        //    JObject obj = getProductCode("4", "false", "UMA", "0", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            if (Convert.ToInt32(item["InsuranceType"]) == 4)
        //            {
        //                Assert.True(true);
        //            }
        //            else
        //            {
        //                Assert.True(false);
        //                break;
        //            }

        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - Agency")]
        //public void getProductCodeGOAgency()
        //{
        //    JObject obj = getProductCode("1", "false", "LING0001", "0", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            if (Convert.ToInt32(item["InsuranceType"]) == 1)
        //            {
        //                Assert.True(true);
        //            }
        //            else
        //            {
        //                Assert.True(false);
        //                break;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - TIAgency")]
        //public void getProductCodeTIAgency()
        //{
        //    JObject obj = getProductCode("2", "false", "LING0001", "0", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 0);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - LIAgency")]
        //public void getProductCodeLIAgency()
        //{
        //    JObject obj = getProductCode("3", "false", "LING0001", "0", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 0);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - GORenew")]
        //public void getProductCodeGORenew()
        //{
        //    JObject obj = getProductCode("1", "false", "UMA", "1", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            if (Convert.ToInt32(item["InsuranceType"]) == 1)
        //            {
        //                Assert.True(true);
        //            }
        //            else
        //            {
        //                Assert.True(false);
        //                break;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - TIRenew")]
        //public void getProductCodeTIRenew()
        //{
        //    JObject obj = getProductCode("2", "false", "UMA", "1", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            Assert.True(Convert.ToInt32(item["InsuranceType"]) == 2);
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - LIRenew")]
        //public void getProductCodeLIRenew()
        //{
        //    JObject obj = getProductCode("3", "false", "UMA", "1", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            Assert.True(Convert.ToInt32(item["InsuranceType"]) == 3);
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - ShariaRenew")]
        //public void getProductCodeShariaRenew()
        //{
        //    JObject obj = getProductCode("4", "false", "UMA", "1", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            Assert.True(Convert.ToInt32(item["InsuranceType"]) == 4);
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - AgencyRenew")]
        //public void getProductCodeGOAgencyRenew()
        //{
        //    JObject obj = getProductCode("1", "false", "LING0001", "1", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    if (Convert.ToBoolean(obj["status"]) == true && arr.Count > 0)
        //    {
        //        foreach (var item in arr)
        //        {
        //            Assert.True(Convert.ToInt32(item["InsuranceType"]) == 1);
        //        }
        //    }
        //    else
        //    {
        //        Assert.True(false);
        //    }
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - TIAgencyRenew")]
        //public void getProductCodeTIAgencyRenew()
        //{
        //    JObject obj = getProductCode("2", "false", "LING0001", "1", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 0);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - LIAgencyRenew")]
        //public void getProductCodeLIAgencyRenew()
        //{
        //    JObject obj = getProductCode("3", "false", "LING0001", "1", "false");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 0);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - MVGodigNew")]
        //public void getProductCodeMVGodigNew()
        //{
        //    JObject obj = getProductCode("1", "true", "KNA", "0", "true");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - MVGodigNotNew")]
        //public void getProductCodeMVGodigNotNew()
        //{
        //    JObject obj = getProductCode("1", "false", "KNA", "0", "true");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - MVGodigNewAgency")]
        //public void getProductCodeMVGodigNewAgency()
        //{
        //    JObject obj = getProductCode("1", "true", "LING0001", "0", "true");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - MVGodigNotNewAgency")]
        //public void getProductCodeMVGodigNotNewAgency()
        //{
        //    JObject obj = getProductCode("1", "false", "LING0001", "0", "true");
        //    JArray arr = JArray.Parse(obj["data"].ToString());
        //    Assert.True(Convert.ToBoolean(obj["status"]) == true && arr.Count == 1);
        //}
        ////[TestMethod]
        //[Fact(DisplayName = "getProductCode - WrongParameter")]
        //public void getProductCodeWithWrongParameter()
        //{
        //    JObject obj = getProductCode("1", "false", "LING0001", "true", "true");

        //    Assert.True(Convert.ToBoolean(obj["status"]) == false && obj["data"].ToString().Equals("null"));
        //} 
        #endregion
        #region NewGetEnableDisableSalesmanDealer
        [Theory(DisplayName = "GetEnableDisableSalesmanDealer_ReturnEnableDisableSalesmanDealer")]
        [InlineData(new object[] { "GDN52", "023", "GDN52" })]
        [InlineData(new object[] { "GDN52", "022", "GDN52" })]
        [InlineData(new object[] { "GDU6S", "022", "GDU6S" })]
        [InlineData(new object[] { "RFN10", "022", "RFN10001ABC" })]
        public void GetEnableDisableSalesmanDealer_ReturnEnableDisableSalesmanDealer(string ProductCode, string BranchCode, string MouID)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ProductCode", ProductCode),
                new KeyValuePair<string, string>("BranchCode", BranchCode),
                new KeyValuePair<string, string>("MouID", MouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }


            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.GetEnableDisableSalesmanDealer(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic IsDealerEnable = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("IsDealerEnable").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic IsSalesmanInfoEnable = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("IsSalesmanInfoEnable").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            Assert.True((IsDealerEnable == true || IsDealerEnable == false));
            Assert.True((IsSalesmanInfoEnable == true || IsSalesmanInfoEnable == false));
        }
        [Fact(DisplayName = "GetEnableDisableSalesmanDealer_Return_Exception")]
        public void GetEnableDisableSalesmanDealer_Return_Exception()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param, ProductCode = "GDN52", BranchCode = "022", MouID = "RFN10001ABC";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ProductCode", ProductCode),
                new KeyValuePair<string, string>("BranchCode", BranchCode),
                new KeyValuePair<string, string>("MouID", MouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            var mock = new Mock<Repository.v0107URF2020.IAABRepository>();
            mock.Setup(o => o.GetEnableDisableSalesmanDealer(ProductCode, BranchCode, MouID)).Returns(null); 

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.GetEnableDisableSalesmanDealer(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        [Theory(DisplayName = "GetEnableDisableSalesmanDealer_Return_InputRequired")]
        [InlineData(new object[] { "", "", "" })]
        [InlineData(new object[] { null, null, null })]
        public void GetEnableDisableSalesmanDealer_Return_InputRequired(string ProductCode, string BranchCode, string MouID)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ProductCode", ProductCode),
                new KeyValuePair<string, string>("BranchCode", BranchCode),
                new KeyValuePair<string, string>("MouID", MouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            FormDataCollection form;
            if (ProductCode != null && BranchCode != null && MouID != null)
            {
                form = new FormDataCollection(param);
            }
            else {
                form = null;
            }

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.GetEnableDisableSalesmanDealer(form);
            dynamic message = result.GetType().GetProperty("Message").GetValue(result);

            Assert.Equal("Input Required", message);
        }
        #endregion

        #region NewUpdateUserActive
        [Theory(DisplayName = "UpdateUserActive_ReturnSuccess")]
        [InlineData(new object[] { "IDL", 1 })]
        [InlineData(new object[] { "UMA", 0 })]
        public void UpdateUserActive_ReturnSuccess(string SalesOfficerID, int IsLogin)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("SalesOfficerID", SalesOfficerID),
                new KeyValuePair<string, string>("IsLogin", IsLogin.ToString())
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }


            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0219URF2019.DataReactController();
            var result = controller.updateUserIsActive(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
        }
        [Fact(DisplayName = "UpdateUserActive_ReturnException")]
        public void UpdateUserActive_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string SalesOfficerID = "ESP"; int IsLogin = 0;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("SalesOfficerID", SalesOfficerID),
                new KeyValuePair<string, string>("IsLogin", IsLogin.ToString())
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.v0219URF2019.IMobileRepository>();
            mock.Setup(o => o.UpdateUserActive(SalesOfficerID, IsLogin)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0219URF2019.DataReactController(mock.Object);
            var result = controller.updateUserIsActive(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion

        #region NewGetApplicationParameter
        [Theory(DisplayName = "GetApplicationParameter_ReturnApplicationParameter")]
        [InlineData(new object[] { "ROLECTI" })]
        [InlineData(new object[] { "PREFIX-PHONENO" })]
        public void GetApplicationParameter_ReturnApplicationParameter(string ParamName)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ParamName", ParamName)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0213URF2019.DataReactController();
            var result = controller.GetApplicationParameter(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.True(data.Count > 0);
        }
        [Fact(DisplayName = "GetApplicationParameter_ReturnNullParam")]
        public void GetApplicationParameter_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string ParamName = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ParamName", ParamName)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0213URF2019.DataReactController();
            var result = controller.GetApplicationParameter(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Param can't be null", message);
        }
        [Fact(DisplayName = "GetApplicationParameter_ReturnNullParam")]
        public void GetApplicationParameter_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string ParamName = "ROLECTI";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ParamName", ParamName)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.v0213URF2019.IMobileRepository>();
            mock.Setup(o => o.GetApplicationParametersValue(ParamName)).Throws(new OutOfMemoryException());
            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0213URF2019.DataReactController(mock.Object);
            var result = controller.GetApplicationParameter(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion

        #region NewCheckPremiItems
        [Theory(DisplayName = "CheckPremiItems_ReturnPremiItemsExistence")]
        [InlineData(new object[] { "205ba8ff-e8ac-41aa-87f6-1cdc6a5f1ea6" })]
        [InlineData(new object[] { "004dc42b-4453-4756-95cc-22caa900e69a" })]
        public void CheckPremiItems_ReturnPremiItemsExistence(string OrderNo)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0001URF2020.DataReactController();
            var result = controller.CheckPremiItems(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic IsPremiItemExist = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("IsPremiItemExist").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            Assert.True(IsPremiItemExist == false || IsPremiItemExist == true);
        }
        [Fact(DisplayName = "CheckPremiItems_ReturnNullParam")]
        public void CheckPremiItems_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0001URF2020.DataReactController();
            var result = controller.CheckPremiItems(form);
            dynamic message = result.GetType().GetProperties()[0].GetValue(result);

            Assert.Equal("Input Required", message);
        }
        [Fact(DisplayName = "CheckPremiItems_ReturnNullForm")]
        public void CheckPremiItems_ReturnNullForm()
        {
            // AAA : Assign, Act, Assert
            // Assign

            // Act
            var controller = new Controllers.v0001URF2020.DataReactController();
            var result = controller.CheckPremiItems(null);
            dynamic message = result.GetType().GetProperties()[0].GetValue(result);

            Assert.Equal("Input Required", message);
        }
        [Fact(DisplayName = "CheckPremiItems_ReturnException")]
        public void CheckPremiItems_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo = "004dc42b-4453-4756-95cc-22caa900e69a";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            var mock = new Mock<Repository.v0001URF2020.IMobileRepository>();
            mock.Setup(o => o.CheckPremiItems(OrderNo)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0001URF2020.DataReactController(mock.Object);
            var result = controller.CheckPremiItems(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion

        #region NewGetBranchList
        [Theory(DisplayName = "GetBranchList_ReturnBranchList")]
        [InlineData(new object[] { "" })]
        [InlineData(new object[] { "a" })]
        public void GetBranchList_ReturnBranchList(string search)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("search", search)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.getBranchList(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.True(data.Count > 0);
        }
        [Fact(DisplayName = "GetBranchList_ReturnException")]
        public void GetBranchList_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string search = "jkdlksad";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("search", search)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            //var mock = new Mock<Repository.vWeb2.IMobileRepository>();
            //mock.Setup(o => o.getBranchList(search)).Throws(new OutOfMemoryException());
            //FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.getBranchList(null);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion

        #region NewGetMappingVehiclePlateGeoArea
        [Theory(DisplayName = "GetMappingVehiclePlateGeoArea_ReturnVehiclePlateGeoArea")]
        [InlineData(new object[] { "B" })]
        [InlineData(new object[] { "AD" })]
        public void GetMappingVehiclePlateGeoArea_ReturnVehiclePlateGeoArea(string VehiclePlateCode)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("VehiclePlateCode", VehiclePlateCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.GetMappingVehiclePlateGeoArea(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            foreach (dynamic dt in data)
            {
                Assert.NotEmpty(dt.VehiclePlateCode);
            }
        }
        [Fact(DisplayName = "GetMappingVehiclePlateGeoArea_ReturnVehiclePlateGeoArea_WithMock")]
        public void GetMappingVehiclePlateGeoArea_ReturnVehiclePlateGeoArea_WithMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string VehiclePlateCode = "B";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("VehiclePlateCode", VehiclePlateCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IMobileRepository>();
            mock.Setup(o => o.GetMappingVehiclePlateGeoArea(VehiclePlateCode)).Returns(new List<dynamic>());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.GetMappingVehiclePlateGeoArea(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.Equal(new List<dynamic>(), data);
        }
        [Fact(DisplayName = "GetMappingVehiclePlateGeoArea_ReturnException")]
        public void GetMappingVehiclePlateGeoArea_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string VehiclePlateCode = "B";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("VehiclePlateCode", VehiclePlateCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IMobileRepository>();
            mock.Setup(o => o.GetMappingVehiclePlateGeoArea(VehiclePlateCode)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.GetMappingVehiclePlateGeoArea(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion

        #region NewGetSegmentCode
        [Theory(DisplayName = "GetSegmentCode_ReturnSegmentCode")]
        [InlineData(new object[] { "GDN12" })]
        [InlineData(new object[] { "GDU6S" })]
        public void GetSegmentCode_ReturnSegmentCode(string ProductCode)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ProductCode", ProductCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.GetSegmentCode(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            foreach (dynamic dt in data)
            {
                Assert.NotEmpty(dt.SOB_ID);
            }
        }
        [Fact(DisplayName = "GetSegmentCode_ReturnSegmentCode_WithMock")]
        public void GetSegmentCode_ReturnSegmentCode_WithMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string ProductCode = "GDN52";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ProductCode", ProductCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IAABRepository>();
            mock.Setup(o => o.GetSegmentCode(ProductCode)).Returns(new List<dynamic>());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.GetSegmentCode(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            Assert.Equal(new List<dynamic>(), data);
        }
        [Fact(DisplayName = "GetSegmentCode_ReturnNullParam")]
        public void GetSegmentCode_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.GetSegmentCode(null);
            dynamic message = ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message;
            
            Assert.Equal("Input Required", message);
        }
        [Fact(DisplayName = "GetSegmentCode_ReturnException")]
        public void GetSegmentCode_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string ProductCode = "GDN52";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ProductCode", ProductCode)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IAABRepository>();
            mock.Setup(o => o.GetSegmentCode(ProductCode)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.GetSegmentCode(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion

        #region newGetPolicyDeliveryName
        [Theory(DisplayName = "GetPolicyDeliveryName_ReturnPolicyDeliveryName")]
        [InlineData(new object[] { "BR", "YIH" })]
        [InlineData(new object[] { "CS", "46132b99-de22-4380-b83f-6e3e073ab625" })]
        [InlineData(new object[] { "GC", "b" })]
        public void GetPolicyDeliveryName_ReturnPolicyDeliveryName(string PolicySentToID, string ParamSearch)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("PolicySentToID", PolicySentToID),
                new KeyValuePair<string, string>("ParamSearch", ParamSearch)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController();
            var result = controller.GetPolicyDeliveryName(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            foreach (dynamic dt in data)
            {
                Assert.NotEmpty(dt.Name);
            }
        }
        [Theory(DisplayName = "GetPolicyDeliveryName_ReturnPolicyDeliveryName_WithMock")]
        [InlineData(new object[] { "BR", "YIH" })]
        [InlineData(new object[] { "CS", "46132b99-de22-4380-b83f-6e3e073ab625" })]
        [InlineData(new object[] { "GC", "b" })]
        public void GetPolicyDeliveryName_ReturnPolicyDeliveryName_WithMock(string PolicySentToID, string ParamSearch)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("PolicySentToID", PolicySentToID),
                new KeyValuePair<string, string>("ParamSearch", ParamSearch)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IMobileRepository>();
            mock.Setup(o => o.GetPolicyDeliveryName(PolicySentToID, ParamSearch)).Returns(new List<dynamic>());
            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.GetPolicyDeliveryName(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            Assert.Equal(new List<dynamic>(), data);
        }
        [Fact(DisplayName = "GetPolicyDeliveryName_ReturnException")]
        public void GetPolicyDeliveryName_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string PolicySentToID = "BR"; string ParamSearch = "YIH";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("PolicySentToID", PolicySentToID),
                new KeyValuePair<string, string>("ParamSearch", ParamSearch)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.vWeb2.IMobileRepository>();
            mock.Setup(o => o.GetPolicyDeliveryName(PolicySentToID, ParamSearch)).Throws(new OutOfMemoryException());
            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.vWeb2.DataReactController(mock.Object);
            var result = controller.GetPolicyDeliveryName(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        #endregion

        [Theory(DisplayName = "GetTaskDetail_ReturnTaskDetail")]
        [InlineData(new object[] { "3e256bf9-25a7-4d53-9040-5ee4059f9949", "479d667d-62c2-44a4-a0ed-4c499c48bd51" })]
        [InlineData(new object[] { "06b05d1f-3115-4b70-9a62-0c800c34dd8d", "4749ddbe-91f4-4d9a-9deb-2456eb7ef7b9" })]
        [InlineData(new object[] { "5b3c685f-9f72-4d82-917c-15caa3ac038b", "36f1dbf7-fcae-438d-8be5-63d21e68c905" })]
        [InlineData(new object[] { "15657089-13f1-436e-856f-c45680c6bb15", "12d49bad-da91-4d52-8bd6-386485a5809c" })]
        public void GetTaskDetail_ReturnTaskDetail(string CustID, string FollowUpNo)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("CustID", CustID),
                new KeyValuePair<string, string>("FollowUpNo", FollowUpNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.getTaskDetail(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.True(!string.IsNullOrEmpty(data.PersonalData.Name));
        }
        [Fact(DisplayName = "GetTaskDetail_ReturnException")]
        public void GetTaskDetail_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string CustID = "", FollowUpNo = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("CustID", CustID),
                new KeyValuePair<string, string>("FollowUpNo", FollowUpNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            mock.Setup(o => o.GetTaksListDetail(CustID, FollowUpNo)).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.getTaskDetail(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
            //Assert.Equal("null", data);
        }

        [Theory(DisplayName = "GetTaskDetailDocument_ReturnTaskDetailDocument")]
        [InlineData(new object[] { "3e256bf9-25a7-4d53-9040-5ee4059f9949", "479d667d-62c2-44a4-a0ed-4c499c48bd51", "COMPANY" })]
        [InlineData(new object[] { "5b3c685f-9f72-4d82-917c-15caa3ac038b", "36f1dbf7-fcae-438d-8be5-63d21e68c905", "PERSONAL" })]
        [InlineData(new object[] { "15657089-13f1-436e-856f-c45680c6bb15", "12d49bad-da91-4d52-8bd6-386485a5809c", "PROSPECT" })]
        [InlineData(new object[] { "985877f4-cca4-490a-8e53-4ee095b3b5ed", "794b1c50-7586-44e3-af2f-1a37f3ee9355", "PROSPECT" })]
        [InlineData(new object[] { "9c2fdaec-afe6-43bd-9cc5-73cbabbb520b", "adede4ea-7a40-49e9-9af8-f91c2039fd8c", "PROSPECT" })]
        public void GetTaskDetailDocument_ReturnTaskDetailDocument(string CustID, string FollowUpNo, string Type)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("CustID", CustID),
                new KeyValuePair<string, string>("FollowUpNo", FollowUpNo),
                new KeyValuePair<string, string>("Type", Type)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0063URF2020.DataReactController();
            var result = controller.getTaskDetailDocument(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.True(data.Count > 0);
        }
        [Fact(DisplayName = "GetTaskDetailDocument_ReturnTaskBadType")]        
        public void GetTaskDetailDocument_ReturnTaskBadType()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string CustID = "9c2fdaec-afe6-43bd-9cc5-73cbabbb520b";
            string FollowUpNo = "adede4ea-7a40-49e9-9af8-f91c2039fd8c";
            string Type = "jgufuty";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("CustID", CustID),
                new KeyValuePair<string, string>("FollowUpNo", FollowUpNo),
                new KeyValuePair<string, string>("Type", Type)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0063URF2020.DataReactController();
            var result = controller.getTaskDetailDocument(form);
            dynamic message = result.GetType().GetProperties()[0].GetValue(result);

            Assert.Equal("Bad Type!", message);
        }
        [Fact(DisplayName = "GetTaskDetailDocument_ReturnTaskBadRequest")]
        public void GetTaskDetailDocument_ReturnTaskBadRequest()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string CustID = "";
            string FollowUpNo = "";
            string Type = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("CustID", CustID),
                new KeyValuePair<string, string>("FollowUpNo", FollowUpNo),
                new KeyValuePair<string, string>("Type", Type)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0063URF2020.DataReactController();
            var result = controller.getTaskDetailDocument(form);
            dynamic message = result.GetType().GetProperties()[0].GetValue(result);

            Assert.Equal("Bad Request!", message);
        }
        [Fact(DisplayName = "GetTaskDetailDocument_ReturnTaskException")]
        public void GetTaskDetailDocument_ReturnTaskException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string CustID = "9c2fdaec-afe6-43bd-9cc5-73cbabbb520b";
            string FollowUpNo = "adede4ea-7a40-49e9-9af8-f91c2039fd8c";
            string Type = "PERSONAL";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("CustID", CustID),
                new KeyValuePair<string, string>("FollowUpNo", FollowUpNo),
                new KeyValuePair<string, string>("Type", Type)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0063URF2020.IMobileRepository>();
            mock.Setup(o => o.GetTaksListDetailDocumentPersonal(CustID, FollowUpNo)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0063URF2020.DataReactController(mock.Object);
            var result = controller.getTaskDetailDocument(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
            //Assert.Equal("null", data);
        }

        //0090URF2020
        [Theory(DisplayName = "GenerateShortenLinkPaymentOtosales_ReturnShortenLink")]
        [InlineData(new object[] { "a455a6aa-a6a5-40b3-aa9c-1cc437a3d862", "True" })]
        [InlineData(new object[] { "b5165921-32e7-45b5-9c90-6fb0f55cff95", "False" })]
        [InlineData(new object[] { "109efb77-07c4-4937-b601-b740325609ac", "True" })]
        [InlineData(new object[] { "8a707837-5fa3-4258-a447-58caff9793b4", "False" })]
        [InlineData(new object[] { "501325b6-69f9-4ab3-ac51-1f89b3a74fb5", "False" })]
        public void GenerateShortenLinkPaymentOtosales_ReturnShortenLink(string OrderNo, string IsInstallment)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("IsInstallment", IsInstallment)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.GenerateShortenLinkPaymentOtosales(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            if (data.IsNeedApproval)
            {
                Assert.Empty(data.ShortenURL);
            }
            else {
                Assert.NotEmpty(data.ShortenURL);
            }
        }
        [Fact(DisplayName = "GenerateShortenLinkPaymentOtosales_ReturnNullParam")]
        public void GenerateShortenLinkPaymentOtosales_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param, OrderNo = "", IsInstallment = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("IsInstallment", IsInstallment)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.GenerateShortenLinkPaymentOtosales(form);
            dynamic message = ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message;

            Assert.Equal("Param Required", message);
        }
        [Fact(DisplayName = "GenerateShortenLinkPaymentOtosales_Exception")]
        public void GenerateShortenLinkPaymentOtosales_Exception()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param, OrderNo = "109efb77-07c4-4937-b601-b740325609ac", IsInstallment = "True";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("IsInstallment", IsInstallment)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.GenerateLinkPayment(OrderNo, Convert.ToBoolean(IsInstallment))).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.GenerateShortenLinkPaymentOtosales(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        [Fact(DisplayName = "GenerateShortenLinkPaymentOtosales_ReturnMock")]
        public void GenerateShortenLinkPaymentOtosales_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param, OrderNo = "109efb77-07c4-4937-b601-b740325609ac", IsInstallment = "True";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("IsInstallment", IsInstallment)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.GenerateLinkPayment(OrderNo, Convert.ToBoolean(IsInstallment))).Returns(null);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.GenerateShortenLinkPaymentOtosales(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.Null(data);
        }

        [Theory(DisplayName = "GetApprovalPaymentStatus_ReturnApprovalPaymentStatus")]
        [InlineData(new object[] { "52230cd4-eae1-41cf-b41a-82ef18ce9368" })]
        [InlineData(new object[] { "4cf3fe6b-37ab-416d-8330-c4d36d4279c7" })]
        [InlineData(new object[] { "b5165921-32e7-45b5-9c90-6fb0f55cff95" })]
        [InlineData(new object[] { "8b597dfc-9413-4c24-a790-8e95db8409d3" })]
        [InlineData(new object[] { "f61e7a6d-2a64-4c6c-9a9d-5b09ada69373" })]
        [InlineData(new object[] { "be505cff-8de1-4221-8766-ef8bb67b986d" })]        
        public void GetApprovalPaymentStatus_ReturnApprovalPaymentStatus(string OrderNo)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.GetApprovalPaymentStatus(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.NotNull(data.ApprovalStatus);
        }
        [Fact(DisplayName = "GetApprovalPaymentStatus_ReturnNullParam")]
        public void GetApprovalPaymentStatus_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.GetApprovalPaymentStatus(form);
            dynamic message = ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message;

            Assert.Equal("Param Required", message);
        }
        [Fact(DisplayName = "GetApprovalPaymentStatus_ReturnException")]
        public void GetApprovalPaymentStatus_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo = "52230cd4-eae1-41cf-b41a-82ef18ce9368";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.GetApprovalPaymentStatus(OrderNo)).Throws(new OutOfMemoryException());
            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.GetApprovalPaymentStatus(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        [Fact(DisplayName = "GetApprovalPaymentStatus_ReturnMock")]
        public void GetApprovalPaymentStatus_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo = "be505cff-8de1-4221-8766-ef8bb67b986d";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.GetApprovalPaymentStatus(OrderNo)).Returns(null);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.GetApprovalPaymentStatus(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.Null(data);
        }

        [Theory(DisplayName = "populateOrderApproval_ReturnOrderApproval")]
        [InlineData(new object[] { "srw", "" })]
        [InlineData(new object[] { "lls", "1" })]
        [InlineData(new object[] { "srw", "1" })]
        [InlineData(new object[] { "bmy", "2" })]
        [InlineData(new object[] { "uma", "2" })]
        public void populateOrderApproval_ReturnOrderApproval(string UserID, string ReqType)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.populateOrderApproval(form);

            Assert.True(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("Success!", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
            Assert.Equal("1", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.NotNull(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "populateOrderApproval_ReturnNullParam")]
        public void populateOrderApproval_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string UserID = ""; string ReqType = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.populateOrderApproval(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("User cannot be Empty!", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
            Assert.Equal("6661", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "populateOrderApproval_ReturnException")]
        public void populateOrderApproval_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string UserID = "JPA"; string ReqType = "1";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.PopulateOrderMobileApproval(UserID, Convert.ToInt32(ReqType))).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.populateOrderApproval(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("666", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "populateOrderApproval_ReturnNullParam")]
        public void populateOrderApproval_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string UserID = "srw"; string ReqType = "1";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IAABRepository>();
            OtosalesAPIResult res = new OtosalesAPIResult();
            res.Status = true;
            mock.Setup(o => o.PopulateOrderApproval(UserID, Convert.ToInt32(ReqType))).Returns(res);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.populateOrderApproval(form);

            Assert.Equal(res, ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content);
        }

        [Theory(DisplayName = "populatePaymentApproval_ReturnPaymentApproval")]
        [InlineData(new object[] { "bmy", "" })]
        [InlineData(new object[] { "jpa", "1" })]
        [InlineData(new object[] { "jpa", "2" })]
        public void populatePaymentApproval_ReturnPaymentApproval(string UserID, string ReqType)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.populatePaymentApproval(form);

            Assert.True(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("Success!", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
            Assert.Equal("1", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.NotNull(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "populatePaymentApproval_ReturnNullParam")]
        public void populatePaymentApproval_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string UserID = null; string ReqType = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.populatePaymentApproval(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("User cannot be Empty!", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
            Assert.Equal("6661", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "populatePaymentApproval_ReturnException")]
        public void populatePaymentApproval_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string UserID = "JPA"; string ReqType = "1";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IAABRepository>();
            mock.Setup(o => o.PopulateOrderApproval(UserID, Convert.ToInt32(ReqType))).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.populatePaymentApproval(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("666", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "populatePaymentApproval_ReturnMock")]
        public void populatePaymentApproval_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string UserID = "jpa"; string ReqType = "1";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("UserID", UserID),
                new KeyValuePair<string, string>("ReqType", ReqType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            OtosalesAPIResult res = new OtosalesAPIResult();
            res.Status = true;
            mock.Setup(o => o.PopulateOrderMobileApproval(UserID, Convert.ToInt32(ReqType))).Returns(res);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.populatePaymentApproval(form);

            Assert.Equal(res, ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content);
        }

        [Theory(DisplayName = "ProcessApprovalPayment_ReturnApprovalPaymentStatus")]
        [InlineData(new object[] { "1200016398", "BMY", "PMT1", "1" })]
        [InlineData(new object[] { "1200016398", "JPA", "PMT2", "1" })]
        [InlineData(new object[] { "1200038197", "JPA", "PMT2", "2" })]
        public void ProcessApprovalPayment_ReturnApprovalPaymentStatus(string OrderNo, string UserId
            , string ApprovalType, string ActionType)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("UserId", UserId),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("ActionType", ActionType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.ProcessApprovalPayment(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.NotNull(data);
        }
        [Fact(DisplayName = "ProcessApprovalPayment_ReturnNullParam")]
        public void ProcessApprovalPayment_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo= ""; string UserId="";
            string ApprovalType=""; string ActionType="";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("UserId", UserId),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("ActionType", ActionType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.ProcessApprovalPayment(form);
            dynamic message = ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message;

            Assert.Equal("Param Required", message);
        }
        [Fact(DisplayName = "ProcessApprovalPayment_ReturnException")]
        public void ProcessApprovalPayment_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo = "1200016398"; string UserId = "BMY";
            string ApprovalType = "PMT1"; string ActionType = "1";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("UserId", UserId),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("ActionType", ActionType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.ProcessApprovalPayment(OrderNo, UserId, ApprovalType, ActionType)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.ProcessApprovalPayment(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        [Fact(DisplayName = "ProcessApprovalPayment_ReturnMock")]
        public void ProcessApprovalPayment_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderNo = "1200038197"; string UserId = "JPA";
            string ApprovalType = "PMT1"; string ActionType = "1";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("UserId", UserId),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("ActionType", ActionType)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.ProcessApprovalPayment(OrderNo, UserId, ApprovalType, ActionType)).Returns(string.Empty);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.ProcessApprovalPayment(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.Empty(data);
        }

        [Theory(DisplayName = "fetchDetailApproval_ReturnDetailApproval")]
        [InlineData(new object[] { "381300", "1190021897", "ADJUST", "BRANCHMGR" })]
        [InlineData(new object[] { "0", "1200038584", "PMT1", "BRANCHMGR" })]
        [InlineData(new object[] { "0", "1200038603", "PMT2", "REGIONALMGR" })]
        [InlineData(new object[] { "389840", "1190023821", "NEXTLMT", "REGIONALMGR" })]
        [InlineData(new object[] { "388410", "1190022522", "COMSAMD", "AO" })]
        [InlineData(new object[] { "464431", "1200038595", "KOMISI", "MGO" })]
        public void fetchDetailApproval_ReturnDetailApproval(string OrderID, string OrderNo, string ApprovalType, string Role)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderID", OrderID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("Role", Role)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.fetchDetailApproval(form);

            Assert.True(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Contains("Success", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
            Assert.Equal("1", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.NotNull(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "fetchDetailApproval_ReturnUnknownApprovalType")]
        public void fetchDetailApproval_ReturnUnknownApprovalType()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderID = "464431"; string OrderNo = "1200038595"; string ApprovalType = "Unknown"; string Role = "AO";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderID", OrderID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("Role", Role)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.fetchDetailApproval(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Contains("Unknown Order Type", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
            Assert.Equal("1", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "fetchDetailApproval_ReturnException")]
        public void fetchDetailApproval_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderID = "464433"; string OrderNo = "12000385X9"; string ApprovalType = "NEXTLMT"; string Role = "AO";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderID", OrderID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("Role", Role)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IAABRepository>();
            mock.Setup(o => o.detailApprovalTsiLimit(OrderID, OrderNo)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.fetchDetailApproval(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("666", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.ResponseCode);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "fetchDetailApproval_ReturnMock")]
        public void fetchDetailApproval_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string OrderID = "464433"; string OrderNo = "12000385X9"; string ApprovalType = "NEXTLMT"; string Role = "AO";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderID", OrderID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("ApprovalType", ApprovalType),
                new KeyValuePair<string, string>("Role", Role)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IAABRepository>();
            OtosalesAPIResult res = new OtosalesAPIResult();
            res.Status = true;
            mock.Setup(o => o.detailApprovalTsiLimit(OrderID, OrderNo)).Returns(res);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.fetchDetailApproval(form);

            Assert.Equal(res, ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content);
        }

        [Theory(DisplayName = "Penawaran_ReturnListPenawaran")]
        [InlineData(new object[] { "LING0001" })]
        [InlineData(new object[] { "HERI2012" })]
        [InlineData(new object[] { "ALVI0754" })]
        [InlineData(new object[] { "ANDR3299" })]
        [InlineData(new object[] { "ARIF1406" })]
        [InlineData(new object[] { "ASTR2287" })]
        [InlineData(new object[] { "BONA0896" })]
        [InlineData(new object[] { "CIND0597" })]
        [InlineData(new object[] { "DANN0096" })]
        [InlineData(new object[] { "FIRM5555" })]
        public void Penawaran_ReturnListPenawaran(string userID)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("userID", userID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.Penawaran(form);

            Assert.True(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.NotNull(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
        }
        [Fact(DisplayName = "Penawaran_ReturnNotAuthorized")]
        public void Penawaran_ReturnNotAuthorized()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string userID = "AGUS44786";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("userID", userID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.Penawaran(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
            Assert.Equal("You are not authorized to perform this action.", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
            
        }
        [Fact(DisplayName = "Penawaran_ReturnNullParam")]
        public void Penawaran_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string userID = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("userID", userID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.Penawaran(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Null(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Data);
            Assert.Equal("Param can't be null", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
        }
        [Fact(DisplayName = "Penawaran_ReturnMock")]
        public void Penawaran_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string userID = "DANN0096";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("userID", userID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            OtosalesAPIResult res = new OtosalesAPIResult();
            res.Status = true;
            mock.Setup(o => o.GetPenawaranList(userID)).Returns(res);
            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.Penawaran(form);

            Assert.Equal(res, ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content);
        }
        [Fact(DisplayName = "Penawaran_ReturnException")]
        public void Penawaran_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string userID = "DANN0096";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("userID", userID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.GetPenawaranList(userID)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.Penawaran(form);

            Assert.False(((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Status);
            Assert.Equal("Unable to process your input please try again or contact administrator", ((System.Web.Http.Results.JsonResult<OtosalesAPIResult>)result).Content.Message);
        }

        [Theory(DisplayName = "InsertUpdatePhoneNo_ReturnPhoneList")]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"nip\",\"OrderNo\":\"bdc7b433-f2bf-40fe-b019-5cd27e985a4e\",\"PhoneNoParam\":[{\"PhoneNo\":\"08634356422\",\"ColumnName\":\"\",\"Relation\":\"\",\"IsPrimary\":true,\"IsWrongNumber\":false,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"wulan jkj\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"nip\",\"OrderNo\":\"bdc7b433-f2bf-40fe-b019-5cd27e985a4e\",\"PhoneNoParam\":[{\"PhoneNo\":\"08634356422\",\"ColumnName\":\"MPC1\",\"Relation\":0,\"IsPrimary\":false,\"IsWrongNumber\":true,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"nip\",\"OrderNo\":\"eeffe769-d0ba-43da-a071-20f408e7ffd2\",\"PhoneNoParam\":[{\"PhoneNo\":\"085764987654\",\"ColumnName\":\"\",\"Relation\":\"6\",\"IsPrimary\":false,\"IsWrongNumber\":false,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"ewqrt\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"nip\",\"OrderNo\":\"eeffe769-d0ba-43da-a071-20f408e7ffd2\",\"PhoneNoParam\":[{\"PhoneNo\":\"085764987654\",\"ColumnName\":\"HP_2\",\"Relation\":6,\"IsPrimary\":false,\"IsWrongNumber\":true,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"ewqrt\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"kna\",\"OrderNo\":\"3d698089-5db5-4aeb-8fc7-064cb49ab8a1\",\"PhoneNoParam\":[{\"PhoneNo\":\"086876908765\",\"ColumnName\":\"\",\"Relation\":\"5\",\"IsPrimary\":false,\"IsWrongNumber\":false,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"wsdefe\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"kna\",\"OrderNo\":\"3d698089-5db5-4aeb-8fc7-064cb49ab8a1\",\"PhoneNoParam\":[{\"PhoneNo\":\"086876908765\",\"ColumnName\":\"HP\",\"Relation\":\"5\",\"IsPrimary\":false,\"IsWrongNumber\":true,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"wsdefe\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"kna\",\"OrderNo\":\"68bc3ee5-df8d-46c9-974e-dd1080ebbe75\",\"PhoneNoParam\":[{\"PhoneNo\":\"08276777722\",\"ColumnName\":\"\",\"Relation\":\"7\",\"IsPrimary\":false,\"IsWrongNumber\":false,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"unit test jangan diubah\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"kna\",\"OrderNo\":\"68bc3ee5-df8d-46c9-974e-dd1080ebbe75\",\"PhoneNoParam\":[{\"PhoneNo\":\"08276777722\",\"ColumnName\":\"HP\",\"Relation\":\"0\",\"IsPrimary\":false,\"IsWrongNumber\":true,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"wsdefe\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"kna\",\"OrderNo\":\"26878454-1505-44d5-bf88-d870d1108bc4\",\"PhoneNoParam\":[{\"PhoneNo\":\"08276777722\",\"ColumnName\":\"\",\"Relation\":\"7\",\"IsPrimary\":false,\"IsWrongNumber\":false,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"unit test jangan diubah\",\"CallRejectReasonId\":0}]}" })]
        [InlineData(new object[] { "{\"SalesOfficerID\":\"kna\",\"OrderNo\":\"26878454-1505-44d5-bf88-d870d1108bc4\",\"PhoneNoParam\":[{\"PhoneNo\":\"08276777722\",\"ColumnName\":\"HP\",\"Relation\":\"0\",\"IsPrimary\":false,\"IsWrongNumber\":true,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"wsdefe\",\"CallRejectReasonId\":0}]}" })]
        public void InsertUpdatePhoneNo_ReturnPhoneList(string json)
        {
            // AAA : Assign, Act, Assert
            // Assign

            Otosales.Models.vWeb2.InsertUpdatePhoneNoParam form = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.InsertUpdatePhoneNoParam>(json);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.InsertUpdatePhoneNo(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));


            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.NotNull(data.Count >= 0);
        }
        [Fact(DisplayName = "InsertUpdatePhoneNo_ReturnPhoneExist")]
        public void InsertUpdatePhoneNo_ReturnPhoneExist()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"SalesOfficerID\":\"nip\",\"OrderNo\":\"2c1d7bd5-303a-4d1f-b728-e3550c584d6b\",\"PhoneNoParam\":[{\"PhoneNo\":\"085434567324\",\"ColumnName\":\"\",\"Relation\":\"\",\"IsPrimary\":true,\"IsWrongNumber\":false,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"dgdfg\",\"CallRejectReasonId\":0}]}";
            Otosales.Models.vWeb2.InsertUpdatePhoneNoParam form = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.InsertUpdatePhoneNoParam>(json);

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController();
            var result = controller.InsertUpdatePhoneNo(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Already exist", message);
        }
        [Fact(DisplayName = "BasicPremiCalculation_ReturnPremiCalculation")]
        public void InsertUpdatePhoneNo_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"SalesOfficerID\":\"nip\",\"OrderNo\":\"728e4ea7-3537-426a-830c-969fe94000cf\",\"PhoneNoParam\":[{\"PhoneNo\":\"085876908124\",\"ColumnName\":\"\",\"Relation\":\"\",\"IsPrimary\":true,\"IsWrongNumber\":false,\"IsLandLine\":false,\"isConnected\":false,\"PICName\":\"Wulan jagan dipake nip\",\"CallRejectReasonId\":0}]}";
            Otosales.Models.vWeb2.InsertUpdatePhoneNoParam form = JsonConvert.DeserializeObject<Otosales.Models.vWeb2.InsertUpdatePhoneNoParam>(json);
            var mock = new Mock<Repository.v0090URF2020.IAABRepository>();
            mock.Setup(o => o.GetClaimeePhone("", "")).Returns(new Models.vWeb2.PhoneNoModel());
            mock.Setup(o => o.InsertUpdatePhoneNo(form, "")).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.InsertUpdatePhoneNo(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        [Theory(DisplayName = "BasicPremiCalculation_ReturnPremiCalculation")]
        [InlineData(new object[] { 1, 0, "2020-06-10", "2021-06-10", "T00011", "000020", "U00002"
                    , "2019" ,"7", "GDN52", 219650000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "GDN52" })]
        [InlineData(new object[] { 1, 0, "2020-06-10", "2021-06-10", "T00011", "000020", "U00002"
                    , "2019" ,"7", "RFN10", 219650000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC" })]
        [InlineData(new object[] { 0, 1, "2020-06-10", "2021-06-10", "T00010", "000020", "U00002"
                    , "0" ,"7", "GDN52", 527850000, 527850000, "TLO", "", "366", "B25"
                    , "M01110", "1", "", "" })]
        [InlineData(new object[] { 1, 1, "2019-09-23", "2020-09-23", "T00012", "000020", "U00002"
                    , "2015" ,"5", "TDU53", 90000000, 90000000, "ALLRIK,TLO", "", "366", "B25"
                    , "M01759", "0", "1503029831", "" })]
        [InlineData(new object[] { 1, 0, "2019-11-30", "2020-11-30", "T00011", "000020", "U00002"
                    , "2018" ,"7", "GEU10", 299500000, 299500000, "ALLRIK", "", "366", "B25"
                    , "M01167", "0", "1902077062", "" })]
        [InlineData(new object[] { 0, 0, "2020-06-10", "2021-03-10", "T00011", "000020", "U00002"
                    , "2019" ,"7", "GDN52", 219650000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "GDN52" })]
        public void BasicPremiCalculation_ReturnPremiCalculation(int ComprePeriod, int TLOPeriod, string PeriodFrom, string PeriodTo, string vtype, string vcitycode, string vusagecode
            , string vyear , string vsitting, string vProductCode, decimal vTSInterest, decimal vPrimarySI, string vCoverageId, string pQuotationNo, string Ndays, string vBrand
            , string vModel, string isNew, string OldPolicyNo, string vMouID)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.BasicPremiCalculation(form);
            dynamic CalculatedPremiItems = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("CalculatedPremiItems").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic GrossPremi = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("GrossPremi").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic TotalPremi = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("TotalPremi").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(CalculatedPremiItems.Count > 0);
            Assert.True(GrossPremi > 0);
            Assert.True(TotalPremi > 0);
        }
        [Fact(DisplayName = "BasicPremiCalculation_ReturnEmptyParam")]
        public void BasicPremiCalculation_ReturnEmptyParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            int ComprePeriod=1; int TLOPeriod=0; string PeriodFrom= "2019-11-30"; string PeriodTo= "2020-11-30"; string vtype= "T00011"; string vcitycode= "000020"; string vusagecode = "U00002";
            string vyear= "2018"; string vsitting= "7"; string vProductCode= "GEU10"; decimal vTSInterest= 299500000; decimal vPrimarySI= 299500000; string vCoverageId= "ALLRIK"; string pQuotationNo=""; string Ndays= ""; string vBrand
            = "B25"; string vModel= "M01167"; string isNew= "0"; string OldPolicyNo= "1902077062"; string vMouID = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.BasicPremiCalculation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Param Not Completed", message);
        }
        [Fact(DisplayName = "BasicPremiCalculation_ReturnProductNoSegment")]
        public void BasicPremiCalculation_ReturnProductNoSegment()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            int ComprePeriod = 1; int TLOPeriod = 0; string PeriodFrom = "2019-11-30"; string PeriodTo = "2020-11-30"; string vtype = "T00011"; string vcitycode = "000020"; string vusagecode = "U00002";
            string vyear = "2018"; string vsitting = "7"; string vProductCode = "asdf"; decimal vTSInterest = 299500000; decimal vPrimarySI = 299500000; string vCoverageId = "ALLRIK"; string pQuotationNo = ""; string Ndays = "366"; string vBrand
                     = "B25"; string vModel = "M01167"; string isNew = "0"; string OldPolicyNo = "1902077062"; string vMouID = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.BasicPremiCalculation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Product Has Not Mapping Segment", message);
        }
        [Fact(DisplayName = "BasicPremiCalculation_ReturnException")]
        public void BasicPremiCalculation_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            int ComprePeriod = 1; int TLOPeriod = 0; string PeriodFrom = "2019-11-30"; string PeriodTo = "2020-11-30"; string vtype = "T00010"; string vcitycode = "000020"; string vusagecode = "U00002";
            string vyear = "2018"; string vsitting = "7"; string vProductCode = "TATN7"; decimal vTSInterest = 408000000; decimal vPrimarySI = 408000000; string vCoverageId = "ALLRIK"; string pQuotationNo = ""; string Ndays = "365";
            string vBrand  = "B11"; string vModel = "M01818"; string isNew = "0"; string OldPolicyNo = ""; string vMouID = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            List<decimal> lsD = new List<decimal>();
            bool oBool = false;
            mock.Setup(o => o.CalculateBasicPremi(new List<CoverageParam>(), "", "", "", "", "", "", "", "", "", "", DateTime.Now, DateTime.Now, "", 0, 0
                , "", "", out lsD, out oBool)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.BasicPremiCalculation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        [Theory(DisplayName = "PremiumCalculation_ReturnPremiCalculation")]
        [InlineData(new object[] { 1, 0, "2020-06-10", "2021-06-10", "T00011", "000020", "U00002"
                    , "2019" ,"7", "GDN52", 219650000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "GDN52", "9da1a91e-dce5-4d67-bbd8-d68b6b5d2377", "", "", "" })]
        [InlineData(new object[] { 1, 0, "2020-06-10", "2021-06-10", "T00011", "000020", "U00002"
                    , "2019" ,"7", "RFN10", 5000000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "TPLPER"
                    , "[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-09 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}]"
                    , "MVTP01"})]
        [InlineData(new object[] { 1, 0, null, null, "T00011", "000020", "U00002"
                    , "0" ,"7", "RFN10", 5000000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "TPLPER"
                    , "[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}]"
                    , "MVTP01"})]
        [InlineData(new object[] { 1, 1, "2020-06-12", "2020-06-12", "T00011", "000020", "U00002"
                    , "0" ,"7", "RFN10", 10, 219650000, "ALLRIK,TLO", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "ACCESS"
                    , "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"TLO   \",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":1247612,\"CoverPremium\":1247612,\"GrossPremium\":1247612,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":1247612,\"Net2\":1247612,\"Net3\":1247612,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETVTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLDTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":70288,\"CoverPremium\":70288,\"GrossPremium\":70288,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":70288,\"Net2\":70288,\"Net3\":70288,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , ""})]
        [InlineData(new object[] { 1, 0, "", "", "T00011", "000020", "U00002"
                    , "2019" ,"7", "RFN10", 5000000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "PADRVR"
                    , "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"PADRVR\",\"CoverageID\":\"PAD1  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.5,\"ExcessRate\":0.5,\"Premium\":49999.99999999999,\"CoverPremium\":50000,\"GrossPremium\":50000,\"MaxSI\":99999999999999,\"DeductibleCode\":\"FLT0  \",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":50000,\"Net2\":50000,\"Net3\":50000,\"SumInsured\":10000000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"PAPASS\",\"CoverageID\":\"PAP1  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.6,\"ExcessRate\":0.1,\"Premium\":30000,\"CoverPremium\":30000,\"GrossPremium\":30000,\"MaxSI\":99999999999999,\"DeductibleCode\":\"FLT0  \",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":30000,\"Net2\":30000,\"Net3\":30000,\"SumInsured\":5000000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , "MVTP01"})]
        [InlineData(new object[] { 1, 0, "", "", "T00011", "000020", "U00002"
                    , "2019" ,"7", "RFN10", 5000000, 219650000, "ALLRIK", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "SRCC"
                    , "[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-09 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}]"
                    , "MVTP01"})]
        [InlineData(new object[] { 1, 1, "", "", "T00011", "000020", "U00002"
                    , "0" ,"7", "RFN10", 219650000, 219650000, "ALLRIK,TLO", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "TS"
                    , "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"TLO   \",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":1247612,\"CoverPremium\":1247612,\"GrossPremium\":1247612,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":1247612,\"Net2\":1247612,\"Net3\":1247612,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETVTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLDTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":70288,\"CoverPremium\":70288,\"GrossPremium\":70288,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":70288,\"Net2\":70288,\"Net3\":70288,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ALLRIK\",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":0.215,\"CoverPremium\":0.215,\"GrossPremium\":0.215,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.215,\"Net2\":0.215,\"Net3\":0.215,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETV   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":0.0125,\"CoverPremium\":0.0125,\"GrossPremium\":0.0125,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0125,\"Net2\":0.0125,\"Net3\":0.0125,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLD   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.01,\"CoverPremium\":0.01,\"GrossPremium\":0.01,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.01,\"Net2\":0.01,\"Net3\":0.01,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCC  \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":0.0075,\"CoverPremium\":0.0075,\"GrossPremium\":0.0075,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0075,\"Net2\":0.0075,\"Net3\":0.0075,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"TLO   \",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":0.0568,\"CoverPremium\":0.0568,\"GrossPremium\":0.0568,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0568,\"Net2\":0.0568,\"Net3\":0.0568,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETVTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLDTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":0.0032,\"CoverPremium\":0.0032,\"GrossPremium\":0.0032,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0032,\"Net2\":0.0032,\"Net3\":0.0032,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , ""})]
        [InlineData(new object[] { 1, 1, "2020-06-12", "2020-06-12", "T00011", "000020", "U00002"
                    , "0" ,"7", "RFN10", 219650000, 219650000, "ALLRIK,TLO", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "ACCESS"
                    , "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"TLO   \",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":1247612,\"CoverPremium\":1247612,\"GrossPremium\":1247612,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":1247612,\"Net2\":1247612,\"Net3\":1247612,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETVTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLDTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":70288,\"CoverPremium\":70288,\"GrossPremium\":70288,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":70288,\"Net2\":70288,\"Net3\":70288,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ALLRIK\",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":0.215,\"CoverPremium\":0.215,\"GrossPremium\":0.215,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.215,\"Net2\":0.215,\"Net3\":0.215,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETV   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":0.0125,\"CoverPremium\":0.0125,\"GrossPremium\":0.0125,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0125,\"Net2\":0.0125,\"Net3\":0.0125,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLD   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.01,\"CoverPremium\":0.01,\"GrossPremium\":0.01,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.01,\"Net2\":0.01,\"Net3\":0.01,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCC  \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":0.0075,\"CoverPremium\":0.0075,\"GrossPremium\":0.0075,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0075,\"Net2\":0.0075,\"Net3\":0.0075,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"TLO   \",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":0.0568,\"CoverPremium\":0.0568,\"GrossPremium\":0.0568,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0568,\"Net2\":0.0568,\"Net3\":0.0568,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETVTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLDTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":0.0032,\"CoverPremium\":0.0032,\"GrossPremium\":0.0032,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0032,\"Net2\":0.0032,\"Net3\":0.0032,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , ""})]
        [InlineData(new object[] { 1, 1, "", "", "T00011", "000020", "U00002"
                    , "0" ,"7", "RFN10", 219650000, 219650000, "TLO", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "SRCC"
                    , "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"TLO   \",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":1247612,\"CoverPremium\":1247612,\"GrossPremium\":1247612,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":1247612,\"Net2\":1247612,\"Net3\":1247612,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETVTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLDTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":70288,\"CoverPremium\":70288,\"GrossPremium\":70288,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":70288,\"Net2\":70288,\"Net3\":70288,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , ""})]
        [InlineData(new object[] { 1, 1, "2020-06-12", "2020-06-12", "T00011", "000020", "U00002"
                    , "0" ,"7", "RFN10", 219650000, 219650000, "TLO", "", "366", "B25"
                    , "M01048", "0", "", "RFN10001ABC", "", "TS"
                    , "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"TLO   \",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":1247612,\"CoverPremium\":1247612,\"GrossPremium\":1247612,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":1247612,\"Net2\":1247612,\"Net3\":1247612,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETVTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLDTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":70288,\"CoverPremium\":70288,\"GrossPremium\":70288,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":70288,\"Net2\":70288,\"Net3\":70288,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ALLRIK\",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":0.215,\"CoverPremium\":0.215,\"GrossPremium\":0.215,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.215,\"Net2\":0.215,\"Net3\":0.215,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETV   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":0.0125,\"CoverPremium\":0.0125,\"GrossPremium\":0.0125,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0125,\"Net2\":0.0125,\"Net3\":0.0125,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLD   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.01,\"CoverPremium\":0.01,\"GrossPremium\":0.01,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.01,\"Net2\":0.01,\"Net3\":0.01,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCC  \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":0.0075,\"CoverPremium\":0.0075,\"GrossPremium\":0.0075,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0075,\"Net2\":0.0075,\"Net3\":0.0075,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"TLO   \",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":0.0568,\"CoverPremium\":0.0568,\"GrossPremium\":0.0568,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0568,\"Net2\":0.0568,\"Net3\":0.0568,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETVTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLDTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":0.0032,\"CoverPremium\":0.0032,\"GrossPremium\":0.0032,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0032,\"Net2\":0.0032,\"Net3\":0.0032,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , ""})]
        [InlineData(new object[] { 1, 0, "", "", "T00011", "000020", "U00002"
                    , "2018" ,"7", "GWU50", 248350000, 248350000, "ALLRIK", "", "366", "B18"
                    , "M01934", "0", "2001716498", "RFN10001ABC", "", "TS"
                    , "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"TLO   \",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":1247612,\"CoverPremium\":1247612,\"GrossPremium\":1247612,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":1247612,\"Net2\":1247612,\"Net3\":1247612,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETVTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLDTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":175720,\"CoverPremium\":175720,\"GrossPremium\":175720,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":175720,\"Net2\":175720,\"Net3\":175720,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCTLO\",\"Year\":1,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":70288,\"CoverPremium\":70288,\"GrossPremium\":70288,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":70288,\"Net2\":70288,\"Net3\":70288,\"SumInsured\":175720000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ALLRIK\",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":0.215,\"CoverPremium\":0.215,\"GrossPremium\":0.215,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.215,\"Net2\":0.215,\"Net3\":0.215,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETV   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":0.0125,\"CoverPremium\":0.0125,\"GrossPremium\":0.0125,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0125,\"Net2\":0.0125,\"Net3\":0.0125,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLD   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.01,\"CoverPremium\":0.01,\"GrossPremium\":0.01,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.01,\"Net2\":0.01,\"Net3\":0.01,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCC  \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":0.0075,\"CoverPremium\":0.0075,\"GrossPremium\":0.0075,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0075,\"Net2\":0.0075,\"Net3\":0.0075,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"TLO   \",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":0.0568,\"CoverPremium\":0.0568,\"GrossPremium\":0.0568,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0568,\"Net2\":0.0568,\"Net3\":0.0568,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETVTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLDTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":0.0032,\"CoverPremium\":0.0032,\"GrossPremium\":0.0032,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0032,\"Net2\":0.0032,\"Net3\":0.0032,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , "9ac6b54e-f119-4e57-81fd-73eff22c754a"})]
        public void PremiumCalculation_ReturnPremiCalculation(int ComprePeriod, int TLOPeriod, string PeriodFrom, string PeriodTo, string vtype, string vcitycode, string vusagecode
    , string vyear, string vsitting, string vProductCode, decimal vTSInterest, decimal vPrimarySI, string vCoverageId, string pQuotationNo, string Ndays, string vBrand
    , string vModel, string isNew, string OldPolicyNo, string vMouID, string OrderNo, string chItem, string calculatedPremiItems, string TPLCoverageId)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("chItem", chItem),
                new KeyValuePair<string, string>("CalculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("TPLCoverageId", TPLCoverageId)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.PremiumCalculation(form);
            dynamic CalculatedPremiItems = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("CalculatedPremiItems").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic GrossPremi = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("GrossPremi").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic TotalPremi = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("TotalPremi").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(CalculatedPremiItems.Count > 0);
            Assert.True(GrossPremi > 0);
            Assert.True(TotalPremi > 0);
        }

        [Fact(DisplayName = "PremiumCalculation_ReturnParamNotComplete")]
        public void PremiumCalculation_ReturnParamNotComplete()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            int ComprePeriod=1; int TLOPeriod=0; string PeriodFrom=""; string PeriodTo= ""; string vtype= ""; string vcitycode= ""; string vusagecode
= ""; string vyear= ""; string vsitting= ""; string vProductCode= ""; decimal vTSInterest= 0; decimal vPrimarySI=0; string vCoverageId= ""; string pQuotationNo= ""; string Ndays= ""; string vBrand
= ""; string vModel= ""; string isNew= ""; string OldPolicyNo= ""; string vMouID= ""; string OrderNo= ""; string chItem=""; string calculatedPremiItems= ""; string TPLCoverageId = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("chItem", chItem),
                new KeyValuePair<string, string>("CalculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("TPLCoverageId", TPLCoverageId)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.PremiumCalculation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Param Not Completed", message);
        }

        [Fact(DisplayName = "PremiumCalculation_ReturnBasicCoverNotApplied")]
        public void PremiumCalculation_ReturnBasicCoverNotApplied()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            int ComprePeriod = 1; int TLOPeriod = 0; string PeriodFrom = "2020-06-12"; string PeriodTo = "2021-06-12"; string vtype = "T00011"; string vcitycode = "000020"; string vusagecode
         = "U00002"; string vyear = "2018"; string vsitting = "7"; string vProductCode = "GWU50"; decimal vTSInterest = 248350000; decimal vPrimarySI = 248350000; string vCoverageId = "ALLRIK"; string pQuotationNo = ""; string Ndays = "366"; string vBrand
                  = "B18"; string vModel = "M01934"; string isNew = "0"; string OldPolicyNo = "2001716498"; string vMouID = ""; string OrderNo = ""; string chItem = "";
            string calculatedPremiItems = "[{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ALLRIK\",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":0.215,\"CoverPremium\":0.215,\"GrossPremium\":0.215,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.215,\"Net2\":0.215,\"Net3\":0.215,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETV   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":0.0125,\"CoverPremium\":0.0125,\"GrossPremium\":0.0125,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0125,\"Net2\":0.0125,\"Net3\":0.0125,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLD   \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.01,\"CoverPremium\":0.01,\"GrossPremium\":0.01,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.01,\"Net2\":0.01,\"Net3\":0.01,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCC  \",\"Year\":0,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":0.0075,\"CoverPremium\":0.0075,\"GrossPremium\":0.0075,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0075,\"Net2\":0.0075,\"Net3\":0.0075,\"SumInsured\":10,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"TLO   \",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.71,\"ExcessRate\":0.71,\"Premium\":0.0568,\"CoverPremium\":0.0568,\"GrossPremium\":0.0568,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0568,\"Net2\":0.0568,\"Net3\":0.0568,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"ETVTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"FLDTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":0.008,\"CoverPremium\":0.008,\"GrossPremium\":0.008,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.008,\"Net2\":0.008,\"Net3\":0.008,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"ACCESS\",\"CoverageID\":\"SRCTLO\",\"Year\":0,\"PeriodFrom\":\"2021-06-12 00:00:00.000\",\"PeriodTo\":\"2022-06-12 00:00:00.000\",\"Rate\":0.04,\"ExcessRate\":0.04,\"Premium\":0.0032,\"CoverPremium\":0.0032,\"GrossPremium\":0.0032,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":0.0032,\"Net2\":0.0032,\"Net3\":0.0032,\"SumInsured\":8,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"2\",\"CoverType\":null,\"CalcMethod\":0}]";
            string TPLCoverageId = "ACCESS";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("chItem", chItem),
                new KeyValuePair<string, string>("CalculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("TPLCoverageId", TPLCoverageId)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.PremiumCalculation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Please Apply Basic Cover first!", message);
        }
        [Fact(DisplayName = "PremiumCalculation_ReturnDisasterNotApplied")]
        public void PremiumCalculation_ReturnDisasterNotApplied()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            int ComprePeriod = 1; int TLOPeriod = 0; string PeriodFrom = "2020-06-12"; string PeriodTo = "2021-06-12"; string vtype = "T00011"; string vcitycode = "000020"; string vusagecode
         = "U00002"; string vyear = "2018"; string vsitting = "7"; string vProductCode = "GWU50"; decimal vTSInterest = 248350000; decimal vPrimarySI = 248350000; string vCoverageId = "ALLRIK"; string pQuotationNo = ""; string Ndays = "366"; string vBrand
                  = "B18"; string vModel = "M01934"; string isNew = "0"; string OldPolicyNo = "2001716498"; string vMouID = ""; string OrderNo = ""; string chItem = "TS";
            string calculatedPremiItems = "[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-09 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}]";
            string TPLCoverageId = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("chItem", chItem),
                new KeyValuePair<string, string>("CalculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("TPLCoverageId", TPLCoverageId)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.PremiumCalculation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Please Apply SRCC/FLD/ETV first!", message);
        }
        [Fact(DisplayName = "PremiumCalculation_ReturnException")]
        public void PremiumCalculation_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            int ComprePeriod = 1; int TLOPeriod = 0; string PeriodFrom = "2020-06-12"; string PeriodTo = "2021-06-12"; string vtype = "T00011"; string vcitycode = "000020"; string vusagecode
         = "U00002"; string vyear = "2018"; string vsitting = "7"; string vProductCode = "GWU50"; decimal vTSInterest = 248350000; decimal vPrimarySI = 248350000; string vCoverageId = "ALLRIK"; string pQuotationNo = ""; string Ndays = "366"; string vBrand
                  = "B18"; string vModel = "M01934"; string isNew = "0"; string OldPolicyNo = "2001716498"; string vMouID = ""; string OrderNo = ""; string chItem = "";
            string calculatedPremiItems = "[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-09 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}]";
            string TPLCoverageId = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ComprePeriod", ComprePeriod.ToString()),
                new KeyValuePair<string, string>("TLOPeriod", TLOPeriod.ToString()),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("vTSInterest", vTSInterest.ToString()),
                new KeyValuePair<string, string>("vPrimarySI", vPrimarySI.ToString()),
                new KeyValuePair<string, string>("vCoverageId", vCoverageId),
                new KeyValuePair<string, string>("pQuotationNo", pQuotationNo),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("isNew", isNew),
                new KeyValuePair<string, string>("OldPolicyNo", OldPolicyNo),
                new KeyValuePair<string, string>("vMouID", vMouID),
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("chItem", chItem),
                new KeyValuePair<string, string>("CalculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("TPLCoverageId", TPLCoverageId)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            List<decimal> a = new List<decimal>();
            List<decimal> b = new List<decimal>();
            List<CalculatedPremi> lp = new List<CalculatedPremi>();
            List<DetailScoring> ld = new List<DetailScoring>();
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            mock.Setup(o => o.CalculateACCESS(0,0,"","",DateTime.Now,DateTime.Now,"","","","","",lp,ld,"",out a, out b)).Throws(null);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.PremiumCalculation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        [Theory(DisplayName = "getOrderDetailCoverage_ReturnOrderDetailCoverage")]
        [InlineData(new object[] { "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , "50000", "T00011", "000020", "U00002", "2019", "7", "M01048", "B25", "0", "366", "2020-06-12", "2021-06-12", "RFN10", "RFN10001ABC"})]
        [InlineData(new object[] { "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":1.7,\"ExcessRate\":1.7,\"Premium\":8973450,\"CoverPremium\":8973450,\"GrossPremium\":8973450,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":8973450,\"Net2\":8973450,\"Net3\":8973450,\"SumInsured\":527850000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":659812.5,\"CoverPremium\":659812.5,\"GrossPremium\":659812.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":659812.5,\"Net2\":659812.5,\"Net3\":659812.5,\"SumInsured\":527850000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":527850,\"CoverPremium\":527850,\"GrossPremium\":527850,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":527850,\"Net2\":527850,\"Net3\":527850,\"SumInsured\":527850000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":395887.50000000006,\"CoverPremium\":395887.5,\"GrossPremium\":395887.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":395887.5,\"Net2\":395887.5,\"Net3\":395887.5,\"SumInsured\":527850000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}]"
                    , "50000", "T00010", "000020", "U00002", "2020", "7", "M01110", "B25", "1", "366", "2020-06-12", "2021-06-12", "RFN10", "RFN10001ABC"})]
        public void getOrderDetailCoverage_ReturnOrderDetailCoverage(string calculatedPremi, string AdminFee, string vtype, string vcitycode, string vusagecode, string vyear,
            string vsitting, string vModel, string vBrand, string vIsNew, string Ndays, string PeriodFrom, string PeriodTo, string vProductCode, string vMouID)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("calculatedPremi", calculatedPremi),
                new KeyValuePair<string, string>("AdminFee", AdminFee),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("IsNew", vIsNew),
                new KeyValuePair<string, string>("vMouID", vMouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.getOrderDetailCoverage(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.True(data.dtlCoverage.Count > 0);
        }
        [Fact(DisplayName = "getOrderDetailCoverage_ReturnMock")]
        public void getOrderDetailCoverage_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string calculatedPremi= "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-12 00:00:00.000\",\"PeriodTo\":\"2021-06-12 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}]";
            string AdminFee= "50000"; string vtype= "T00010"; string vcitycode= "000020"; string vusagecode= "U00002"; string vyear= "2020"; string vsitting= "7"; string vModel= "M01110";
            string vBrand= "B25"; string vIsNew="1"; string Ndays="266"; string PeriodFrom= "2020-06-12"; string PeriodTo= "2020-06-12"; string vProductCode= "RFN10"; string vMouID = "RFN10001ABC";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("calculatedPremi", calculatedPremi),
                new KeyValuePair<string, string>("AdminFee", AdminFee),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("IsNew", vIsNew),
                new KeyValuePair<string, string>("vMouID", vMouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            List<decimal> a = new List<decimal>();
            List<decimal> b = new List<decimal>();
            List<CalculatedPremi> lp = new List<CalculatedPremi>();
            List<DetailScoring> ld = new List<DetailScoring>();

            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            DetailCoverageSummary ob = new DetailCoverageSummary();
            double disc = 0;
            mock.Setup(o => o.GetDetailCoverageSummary("","",ld,lp,0,"","",0,DateTime.Now,DateTime.Now,"","","", out disc)).Returns(ob);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.getOrderDetailCoverage(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("OK", message);
            Assert.Null(data);
        }
        [Fact(DisplayName = "getOrderDetailCoverage_ReturnException")]
        public void getOrderDetailCoverage_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            string calculatedPremi = "";
            string AdminFee = ""; string vtype = ""; string vcitycode = ""; string vusagecode = ""; string vyear = ""; string vsitting = ""; string vModel = "";
            string vBrand = ""; string vIsNew = ""; string Ndays = ""; string PeriodFrom = ""; string PeriodTo = "asd"; string vProductCode = ""; string vMouID = "";
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("calculatedPremi", calculatedPremi),
                new KeyValuePair<string, string>("AdminFee", AdminFee),
                new KeyValuePair<string, string>("PeriodFrom", PeriodFrom),
                new KeyValuePair<string, string>("PeriodTo", PeriodTo),
                new KeyValuePair<string, string>("vtype", vtype),
                new KeyValuePair<string, string>("vcitycode", vcitycode),
                new KeyValuePair<string, string>("vusagecode", vusagecode),
                new KeyValuePair<string, string>("vyear", vyear),
                new KeyValuePair<string, string>("vsitting", vsitting),
                new KeyValuePair<string, string>("vProductCode", vProductCode),
                new KeyValuePair<string, string>("Ndays", Ndays),
                new KeyValuePair<string, string>("vBrand", vBrand),
                new KeyValuePair<string, string>("vModel", vModel),
                new KeyValuePair<string, string>("IsNew", vIsNew),
                new KeyValuePair<string, string>("vMouID", vMouID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            List<decimal> a = new List<decimal>();
            List<decimal> b = new List<decimal>();
            List<CalculatedPremi> lp = new List<CalculatedPremi>();
            List<DetailScoring> ld = new List<DetailScoring>();

            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            DetailCoverageSummary ob = new DetailCoverageSummary();
            double disc = 0;

            mock.Setup(o => o.GetDetailCoverageSummary("", "", ld, lp, 0, "", "", 0, DateTime.Now, DateTime.Now, "", "", "", out disc)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.getOrderDetailCoverage(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        [Theory(DisplayName = "UpdateTaskDetail_ReturnOrderNo")]
        [InlineData(new object[] { "{\"CustID\":\"fa2b4cb7-e296-4f0b-b4dd-c110f3556477\",\"FollowUpNo\":\"e8a94b44-ccb5-4813-ac93-af01bc26cd03\",\"PersonalData\":{\"Name\":\"RMFS0702\",\"CustBirthDay\":\"1970-06-25 12:00:00\",\"CustGender\":\"M\",\"CustAddress\":\"sdfgfsht\",\"PostalCode\":\"10110\",\"Email1\":\"dhgasj@mail.com\",\"Phone1\":\"089864324374\",\"IdentityNo\":\"356fd34563t5ffr56635353\",\"AmountRep\":0,\"IsPayerCompany\":false,\"isNeedDocRep\":0,\"isCompany\":false,\"SalesOfficerID\":\"rei\"},\"CompanyData\":{\"CompanyName\":\"\",\"NPWPno\":\"\",\"NPWPdate\":null,\"NPWPaddres\":\"\",\"SIUPno\":\"\",\"PKPno\":\"\",\"PKPdate\":null,\"OfficeAddress\":\"\",\"PostalCode\":\"\",\"OfficePhoneNo\":\"\",\"PICPhoneNo\":\"\",\"PICname\":\"\",\"PICEmail\":\"\"},\"VehicleData\":{\"Vehicle\":\"\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"MouID\":\"RFN10001ABC\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"InsuranceType\":1,\"SumInsured\":219650000,\"SegmentCode\":\"P1C100\",\"UsageCode\":\"U00002\",\"AccessSI\":0,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false,\"BankID\":\"\",\"VANumber\":\"\",\"Remarks\":\"\",\"IsPolicyIssuedBeforePaying\":0,\"IsORDefectsRepair\":0,\"IsAccessoriesChange\":0,\"DealerCode\":0,\"SalesDealer\":0,\"ColorOnBPKB\":\"jambon\",\"PeriodFrom\":\"2020-06-10 07:35:00\",\"PeriodTo\":\"2021-06-10 07:35:00\"},\"PolicyAddress\":{\"SentTo\":\"\",\"Name\":\"\",\"Address\":\"\",\"PostalCode\":\"\"},\"SurveySchedule\":{\"CityCode\":\"\",\"LocationCode\":\"\",\"SurveyAddress\":\"\",\"SurveyPostalCode\":\"\",\"SurveyDate\":null,\"ScheduleTimeID\":null,\"IsNeedSurvey\":false,\"IsNSASkipSurvey\":false,\"IsManualSurvey\":false},\"ImageData\":[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"466d732e-97f6-4268-9fb2-1fba22a0c933.png\"},{\"ImageType\":\"STNK\",\"PathFile\":\"\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"6e055388-ee18-412e-9dca-05ea9b57a746.png\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}],\"Remarks\":{\"RemarkToSA\":null},\"OSData\":\"\",\"OSMVData\":\"\",\"calculatedPremiItems\":{\"ListCalculatePremi\":[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}],\"OSData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"CustID\":\"fa2b4cb7-e296-4f0b-b4dd-c110f3556477\",\"FollowUpNo\":\"e8a94b44-ccb5-4813-ac93-af01bc26cd03\",\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"PhoneSales\":\"\",\"SalesDealer\":0,\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"Phone1\":\"089864324374\",\"Email1\":\"dhgasj@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"AdminFee\":50000,\"MultiYearF\":false,\"TotalPremi\":5431425,\"YearCoverage\":1,\"LastInterestNo\":0,\"LastCoverageNo\":0},\"OSMVData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"ObjectNo\":1,\"AccessSI\":0,\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false}},\"FollowUpStatus\":1}" })]
        [InlineData(new object[] { "{\"CustID\":\"700a964e-8e6c-4eb1-b05c-6253a92b6428\",\"FollowUpNo\":\"eec75467-b781-4f89-8850-52892edeb980\",\"PersonalData\":{\"Name\":\"RMFS0718\",\"CustBirthDay\":null,\"CustGender\":null,\"CustAddress\":\"\",\"PostalCode\":null,\"Email1\":\"kusd@mail.com\",\"Phone1\":\"085232423242\",\"IdentityNo\":\"\",\"AmountRep\":0,\"IsPayerCompany\":false,\"isNeedDocRep\":0,\"isCompany\":false,\"SalesOfficerID\":\"rei\"},\"CompanyData\":{\"CompanyName\":\"\",\"NPWPno\":\"\",\"NPWPdate\":null,\"NPWPaddres\":\"\",\"SIUPno\":\"\",\"PKPno\":\"\",\"PKPdate\":null,\"OfficeAddress\":\"\",\"PostalCode\":\"\",\"OfficePhoneNo\":\"\",\"PICPhoneNo\":\"\",\"PICname\":\"\",\"PICEmail\":\"\"},\"VehicleData\":{\"Vehicle\":\"\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GDN52\",\"MouID\":\"GDN52\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"InsuranceType\":1,\"SumInsured\":219650000,\"SegmentCode\":\"CND100\",\"UsageCode\":\"U00002\",\"AccessSI\":0,\"RegistrationNumber\":\"\",\"EngineNumber\":\"\",\"ChasisNumber\":\"\",\"IsNew\":false,\"BankID\":\"\",\"VANumber\":\"\",\"Remarks\":\"\",\"IsPolicyIssuedBeforePaying\":0,\"IsORDefectsRepair\":0,\"IsAccessoriesChange\":0,\"DealerCode\":0,\"SalesDealer\":0,\"ColorOnBPKB\":\"\",\"PeriodFrom\":\"2020-06-19 03:08:59\",\"PeriodTo\":\"2021-06-19 03:08:59\"},\"PolicyAddress\":{\"SentTo\":null,\"Name\":null,\"Address\":null,\"PostalCode\":null},\"SurveySchedule\":{\"CityCode\":\"\",\"LocationCode\":\"\",\"SurveyAddress\":\"\",\"SurveyPostalCode\":\"\",\"SurveyDate\":null,\"ScheduleTimeID\":0,\"IsNeedSurvey\":true,\"IsNSASkipSurvey\":false,\"IsManualSurvey\":false},\"ImageData\":[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"\"},{\"ImageType\":\"STNK\",\"PathFile\":\"\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}],\"Remarks\":{\"RemarkToSA\":null},\"OSData\":\"\",\"OSMVData\":\"\",\"calculatedPremiItems\":{\"ListCalculatePremi\":[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}],\"OSData\":{\"OrderNo\":\"7867b5ba-5afc-46eb-9550-bd0dba6f91d0\",\"CustID\":\"700a964e-8e6c-4eb1-b05c-6253a92b6428\",\"FollowUpNo\":\"eec75467-b781-4f89-8850-52892edeb980\",\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"PhoneSales\":\"\",\"SalesDealer\":0,\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GDN52\",\"Phone1\":\"085232423242\",\"Email1\":\"kusd@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"AdminFee\":50000,\"MultiYearF\":false,\"TotalPremi\":5651075,\"YearCoverage\":1,\"LastInterestNo\":0,\"LastCoverageNo\":0},\"OSMVData\":{\"OrderNo\":\"7867b5ba-5afc-46eb-9550-bd0dba6f91d0\",\"ObjectNo\":1,\"AccessSI\":0,\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"\",\"EngineNumber\":\"\",\"ChasisNumber\":\"\",\"IsNew\":false}},\"FollowUpStatus\":1}" })]
        [InlineData(new object[] { "{\"CustID\":\"700a964e-8e6c-4eb1-b05c-6253a92b6428\",\"FollowUpNo\":\"eec75467-b781-4f89-8850-52892edeb980\",\"PersonalData\":{\"Name\":\"RMFS0718\",\"CustBirthDay\":null,\"CustGender\":null,\"CustAddress\":\"daas\",\"PostalCode\":null,\"Email1\":\"kusd@mail.com\",\"Phone1\":\"085232423242\",\"IdentityNo\":\"\",\"AmountRep\":0,\"IsPayerCompany\":false,\"isNeedDocRep\":0,\"isCompany\":false,\"SalesOfficerID\":\"rei\"},\"CompanyData\":{\"CompanyName\":\"\",\"NPWPno\":\"\",\"NPWPdate\":null,\"NPWPaddres\":\"\",\"SIUPno\":\"\",\"PKPno\":\"\",\"PKPdate\":null,\"OfficeAddress\":\"\",\"PostalCode\":\"\",\"OfficePhoneNo\":\"\",\"PICPhoneNo\":\"\",\"PICname\":\"\",\"PICEmail\":\"\"},\"VehicleData\":{\"Vehicle\":\"\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GDN52\",\"MouID\":\"GDN52\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"InsuranceType\":1,\"SumInsured\":219650000,\"SegmentCode\":\"CND100\",\"UsageCode\":\"U00002\",\"AccessSI\":0,\"RegistrationNumber\":\"\",\"EngineNumber\":\"\",\"ChasisNumber\":\"\",\"IsNew\":false,\"BankID\":\"\",\"VANumber\":\"\",\"Remarks\":\"\",\"IsPolicyIssuedBeforePaying\":0,\"IsORDefectsRepair\":0,\"IsAccessoriesChange\":0,\"DealerCode\":0,\"SalesDealer\":0,\"ColorOnBPKB\":\"\",\"PeriodFrom\":\"2020-06-19 03:08:59\",\"PeriodTo\":\"2021-06-19 03:08:59\"},\"PolicyAddress\":{\"SentTo\":null,\"Name\":null,\"Address\":null,\"PostalCode\":null},\"SurveySchedule\":{\"CityCode\":\"\",\"LocationCode\":\"\",\"SurveyAddress\":\"\",\"SurveyPostalCode\":\"\",\"SurveyDate\":null,\"ScheduleTimeID\":0,\"IsNeedSurvey\":true,\"IsNSASkipSurvey\":false,\"IsManualSurvey\":false},\"ImageData\":[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"\"},{\"ImageType\":\"STNK\",\"PathFile\":\"\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}],\"Remarks\":{\"RemarkToSA\":null},\"OSData\":\"\",\"OSMVData\":\"\",\"calculatedPremiItems\":{\"ListCalculatePremi\":[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-19 00:00:00.000\",\"PeriodTo\":\"2021-06-19 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}],\"OSData\":{\"OrderNo\":\"7867b5ba-5afc-46eb-9550-bd0dba6f91d0\",\"CustID\":\"700a964e-8e6c-4eb1-b05c-6253a92b6428\",\"FollowUpNo\":\"eec75467-b781-4f89-8850-52892edeb980\",\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"PhoneSales\":\"\",\"SalesDealer\":0,\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GDN52\",\"Phone1\":\"085232423242\",\"Email1\":\"kusd@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"AdminFee\":50000,\"MultiYearF\":false,\"TotalPremi\":5651075,\"YearCoverage\":1,\"LastInterestNo\":0,\"LastCoverageNo\":0},\"OSMVData\":{\"OrderNo\":\"7867b5ba-5afc-46eb-9550-bd0dba6f91d0\",\"ObjectNo\":1,\"AccessSI\":0,\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"\",\"EngineNumber\":\"\",\"ChasisNumber\":\"\",\"IsNew\":false}},\"FollowUpStatus\":1}" })]
        public void UpdateTaskDetail_ReturnOrderNo(string json)
        {
            // AAA : Assign, Act, Assert
            // Assign

            Models.v0213URF2019.UpdateTaskDetailParam param = JsonConvert.DeserializeObject<Models.v0213URF2019.UpdateTaskDetailParam> (json);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.UpdateTaskDetail(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Update Success.", message);
            Assert.NotNull(data);
        }
        [Fact(DisplayName = "UpdateTaskDetail_ReturnMock")]
        public void UpdateTaskDetail_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"CustID\":\"fa2b4cb7-e296-4f0b-b4dd-c110f3556477\",\"FollowUpNo\":\"e8a94b44-ccb5-4813-ac93-af01bc26cd03\",\"PersonalData\":{\"Name\":\"RMFS0702\",\"CustBirthDay\":\"1970-06-25 12:00:00\",\"CustGender\":\"M\",\"CustAddress\":\"sdfgfsht\",\"PostalCode\":\"10110\",\"Email1\":\"dhgasj@mail.com\",\"Phone1\":\"089864324374\",\"IdentityNo\":\"356fd34563t5ffr56635353\",\"AmountRep\":0,\"IsPayerCompany\":false,\"isNeedDocRep\":0,\"isCompany\":false,\"SalesOfficerID\":\"rei\"},\"CompanyData\":{\"CompanyName\":\"\",\"NPWPno\":\"\",\"NPWPdate\":null,\"NPWPaddres\":\"\",\"SIUPno\":\"\",\"PKPno\":\"\",\"PKPdate\":null,\"OfficeAddress\":\"\",\"PostalCode\":\"\",\"OfficePhoneNo\":\"\",\"PICPhoneNo\":\"\",\"PICname\":\"\",\"PICEmail\":\"\"},\"VehicleData\":{\"Vehicle\":\"\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"MouID\":\"RFN10001ABC\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"InsuranceType\":1,\"SumInsured\":219650000,\"SegmentCode\":\"P1C100\",\"UsageCode\":\"U00002\",\"AccessSI\":0,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false,\"BankID\":\"\",\"VANumber\":\"\",\"Remarks\":\"\",\"IsPolicyIssuedBeforePaying\":0,\"IsORDefectsRepair\":0,\"IsAccessoriesChange\":0,\"DealerCode\":0,\"SalesDealer\":0,\"ColorOnBPKB\":\"jambon\",\"PeriodFrom\":\"2020-06-10 07:35:00\",\"PeriodTo\":\"2021-06-10 07:35:00\"},\"PolicyAddress\":{\"SentTo\":\"\",\"Name\":\"\",\"Address\":\"\",\"PostalCode\":\"\"},\"SurveySchedule\":{\"CityCode\":\"\",\"LocationCode\":\"\",\"SurveyAddress\":\"\",\"SurveyPostalCode\":\"\",\"SurveyDate\":null,\"ScheduleTimeID\":null,\"IsNeedSurvey\":false,\"IsNSASkipSurvey\":false,\"IsManualSurvey\":false},\"ImageData\":[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"466d732e-97f6-4268-9fb2-1fba22a0c933.png\"},{\"ImageType\":\"STNK\",\"PathFile\":\"\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"6e055388-ee18-412e-9dca-05ea9b57a746.png\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}],\"Remarks\":{\"RemarkToSA\":null},\"OSData\":\"\",\"OSMVData\":\"\",\"calculatedPremiItems\":{\"ListCalculatePremi\":[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}],\"OSData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"CustID\":\"fa2b4cb7-e296-4f0b-b4dd-c110f3556477\",\"FollowUpNo\":\"e8a94b44-ccb5-4813-ac93-af01bc26cd03\",\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"PhoneSales\":\"\",\"SalesDealer\":0,\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"Phone1\":\"089864324374\",\"Email1\":\"dhgasj@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"AdminFee\":50000,\"MultiYearF\":false,\"TotalPremi\":5431425,\"YearCoverage\":1,\"LastInterestNo\":0,\"LastCoverageNo\":0},\"OSMVData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"ObjectNo\":1,\"AccessSI\":0,\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false}},\"FollowUpStatus\":1}";
            Models.v0213URF2019.UpdateTaskDetailParam param = JsonConvert.DeserializeObject<Models.v0213URF2019.UpdateTaskDetailParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            DetailCoverageSummary ob = new DetailCoverageSummary();
            mock.Setup(o => o.UpdateTaskListDetail(param.CustID, param.FollowUpNo, param.PersonalData, param.CompanyData, param.VehicleData, param.policyAddress, param.surveySchedule,
                param.imageData, param.OSData, param.OSMVData, param.calculatedPremiItems, param.FollowUpStatus, param.Remarks)).Returns("");
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.UpdateTaskDetail(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Update Success.", message);
            Assert.NotNull(data);
        }
        [Fact(DisplayName = "UpdateTaskDetail_ReturnNullParam")]
        public void UpdateTaskDetail_ReturnNullParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"CustID\":\"\",\"FollowUpNo\":\"\",\"PersonalData\":{\"Name\":\"RMFS0702\",\"CustBirthDay\":\"1970-06-25 12:00:00\",\"CustGender\":\"M\",\"CustAddress\":\"sdfgfsht\",\"PostalCode\":\"10110\",\"Email1\":\"dhgasj@mail.com\",\"Phone1\":\"089864324374\",\"IdentityNo\":\"356fd34563t5ffr56635353\",\"AmountRep\":0,\"IsPayerCompany\":false,\"isNeedDocRep\":0,\"isCompany\":false,\"SalesOfficerID\":\"rei\"},\"CompanyData\":{\"CompanyName\":\"\",\"NPWPno\":\"\",\"NPWPdate\":null,\"NPWPaddres\":\"\",\"SIUPno\":\"\",\"PKPno\":\"\",\"PKPdate\":null,\"OfficeAddress\":\"\",\"PostalCode\":\"\",\"OfficePhoneNo\":\"\",\"PICPhoneNo\":\"\",\"PICname\":\"\",\"PICEmail\":\"\"},\"VehicleData\":{\"Vehicle\":\"\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"MouID\":\"RFN10001ABC\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"InsuranceType\":1,\"SumInsured\":219650000,\"SegmentCode\":\"P1C100\",\"UsageCode\":\"U00002\",\"AccessSI\":0,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false,\"BankID\":\"\",\"VANumber\":\"\",\"Remarks\":\"\",\"IsPolicyIssuedBeforePaying\":0,\"IsORDefectsRepair\":0,\"IsAccessoriesChange\":0,\"DealerCode\":0,\"SalesDealer\":0,\"ColorOnBPKB\":\"jambon\",\"PeriodFrom\":\"2020-06-10 07:35:00\",\"PeriodTo\":\"2021-06-10 07:35:00\"},\"PolicyAddress\":{\"SentTo\":\"\",\"Name\":\"\",\"Address\":\"\",\"PostalCode\":\"\"},\"SurveySchedule\":{\"CityCode\":\"\",\"LocationCode\":\"\",\"SurveyAddress\":\"\",\"SurveyPostalCode\":\"\",\"SurveyDate\":null,\"ScheduleTimeID\":null,\"IsNeedSurvey\":false,\"IsNSASkipSurvey\":false,\"IsManualSurvey\":false},\"ImageData\":[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"466d732e-97f6-4268-9fb2-1fba22a0c933.png\"},{\"ImageType\":\"STNK\",\"PathFile\":\"\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"6e055388-ee18-412e-9dca-05ea9b57a746.png\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}],\"Remarks\":{\"RemarkToSA\":null},\"OSData\":\"\",\"OSMVData\":\"\",\"calculatedPremiItems\":{\"ListCalculatePremi\":[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}],\"OSData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"CustID\":\"fa2b4cb7-e296-4f0b-b4dd-c110f3556477\",\"FollowUpNo\":\"e8a94b44-ccb5-4813-ac93-af01bc26cd03\",\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"PhoneSales\":\"\",\"SalesDealer\":0,\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"Phone1\":\"089864324374\",\"Email1\":\"dhgasj@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"AdminFee\":50000,\"MultiYearF\":false,\"TotalPremi\":5431425,\"YearCoverage\":1,\"LastInterestNo\":0,\"LastCoverageNo\":0},\"OSMVData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"ObjectNo\":1,\"AccessSI\":0,\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false}},\"FollowUpStatus\":1}";
            Models.v0213URF2019.UpdateTaskDetailParam param = JsonConvert.DeserializeObject<Models.v0213URF2019.UpdateTaskDetailParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            DetailCoverageSummary ob = new DetailCoverageSummary();
            mock.Setup(o => o.UpdateTaskListDetail(param.CustID, param.FollowUpNo, param.PersonalData, param.CompanyData, param.VehicleData, param.policyAddress, param.surveySchedule,
                param.imageData, param.OSData, param.OSMVData, param.calculatedPremiItems, param.FollowUpStatus, param.Remarks)).Returns("");
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.UpdateTaskDetail(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            

            Assert.False(status);
            Assert.Equal("Param can't be null", message);
        }
        [Fact(DisplayName = "UpdateTaskDetail_ReturnException")]
        public void UpdateTaskDetail_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"CustID\":\"fa2b4cb7-e296-4f0b-b4dd-c110f3556477\",\"FollowUpNo\":\"e8a94b44-ccb5-4813-ac93-af01bc26cd03\",\"PersonalData\":{\"Name\":\"RMFS0702\",\"CustBirthDay\":\"1970-06-25 12:00:00\",\"CustGender\":\"M\",\"CustAddress\":\"sdfgfsht\",\"PostalCode\":\"10110\",\"Email1\":\"dhgasj@mail.com\",\"Phone1\":\"089864324374\",\"IdentityNo\":\"356fd34563t5ffr56635353\",\"AmountRep\":0,\"IsPayerCompany\":false,\"isNeedDocRep\":0,\"isCompany\":false,\"SalesOfficerID\":\"rei\"},\"CompanyData\":{\"CompanyName\":\"\",\"NPWPno\":\"\",\"NPWPdate\":null,\"NPWPaddres\":\"\",\"SIUPno\":\"\",\"PKPno\":\"\",\"PKPdate\":null,\"OfficeAddress\":\"\",\"PostalCode\":\"\",\"OfficePhoneNo\":\"\",\"PICPhoneNo\":\"\",\"PICname\":\"\",\"PICEmail\":\"\"},\"VehicleData\":{\"Vehicle\":\"\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"MouID\":\"RFN10001ABC\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"InsuranceType\":1,\"SumInsured\":219650000,\"SegmentCode\":\"P1C100\",\"UsageCode\":\"U00002\",\"AccessSI\":0,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false,\"BankID\":\"\",\"VANumber\":\"\",\"Remarks\":\"\",\"IsPolicyIssuedBeforePaying\":0,\"IsORDefectsRepair\":0,\"IsAccessoriesChange\":0,\"DealerCode\":0,\"SalesDealer\":0,\"ColorOnBPKB\":\"jambon\",\"PeriodFrom\":\"2020-06-10 07:35:00\",\"PeriodTo\":\"2021-06-10 07:35:00\"},\"PolicyAddress\":{\"SentTo\":\"\",\"Name\":\"\",\"Address\":\"\",\"PostalCode\":\"\"},\"SurveySchedule\":{\"CityCode\":\"\",\"LocationCode\":\"\",\"SurveyAddress\":\"\",\"SurveyPostalCode\":\"\",\"SurveyDate\":null,\"ScheduleTimeID\":null,\"IsNeedSurvey\":false,\"IsNSASkipSurvey\":false,\"IsManualSurvey\":false},\"ImageData\":[{\"ImageType\":\"IDENTITYCARD\",\"PathFile\":\"466d732e-97f6-4268-9fb2-1fba22a0c933.png\"},{\"ImageType\":\"STNK\",\"PathFile\":\"\"},{\"ImageType\":\"BSTB\",\"PathFile\":\"6e055388-ee18-412e-9dca-05ea9b57a746.png\"},{\"ImageType\":\"FAKTUR\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA1\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA2\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA3\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA4\",\"PathFile\":\"\"},{\"ImageType\":\"DOCNSA5\",\"PathFile\":\"\"},{\"ImageType\":\"KONFIRMASICUST\",\"PathFile\":\"\"},{\"ImageType\":\"SPPAKB\",\"PathFile\":\"\"},{\"ImageType\":\"DOCREP\",\"PathFile\":\"\"},{\"ImageType\":\"NPWP\",\"PathFile\":\"\"},{\"ImageType\":\"SIUP\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR2\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR3\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR4\",\"PathFile\":\"\"},{\"ImageType\":\"BUKTIBAYAR5\",\"PathFile\":\"\"}],\"Remarks\":{\"RemarkToSA\":null},\"OSData\":\"\",\"OSMVData\":\"\",\"calculatedPremiItems\":{\"ListCalculatePremi\":[{\"InterestID\":\"CASCO \",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":0,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219650,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO \",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-10 00:00:00.000\",\"PeriodTo\":\"2021-06-10 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":0,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":null,\"CoverType\":null,\"CalcMethod\":0}],\"OSData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"CustID\":\"fa2b4cb7-e296-4f0b-b4dd-c110f3556477\",\"FollowUpNo\":\"e8a94b44-ccb5-4813-ac93-af01bc26cd03\",\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"PhoneSales\":\"\",\"SalesDealer\":0,\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"Phone1\":\"089864324374\",\"Email1\":\"dhgasj@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"AdminFee\":50000,\"MultiYearF\":false,\"TotalPremi\":5431425,\"YearCoverage\":1,\"LastInterestNo\":0,\"LastCoverageNo\":0},\"OSMVData\":{\"OrderNo\":\"2eb7d84a-137e-4ea3-9f3d-ecd761fb18bb\",\"ObjectNo\":1,\"AccessSI\":0,\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25\",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B8493IO\",\"EngineNumber\":\"534543\",\"ChasisNumber\":\"WERWER\",\"IsNew\":false}},\"FollowUpStatus\":1}";
            Models.v0213URF2019.UpdateTaskDetailParam param = JsonConvert.DeserializeObject<Models.v0213URF2019.UpdateTaskDetailParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            DetailCoverageSummary ob = new DetailCoverageSummary();
            mock.Setup(o => o.UpdateTaskListDetail(param.CustID, param.FollowUpNo, param.PersonalData, param.CompanyData, param.VehicleData, param.policyAddress, param.surveySchedule,
                param.imageData, param.OSData, param.OSMVData, param.calculatedPremiItems, param.FollowUpStatus, param.Remarks)).Throws(new OutOfMemoryException());
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.UpdateTaskDetail(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));


            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        [Theory(DisplayName = "SaveProspect_ReturnProspectInfo")]
        [InlineData(new object[] { "1", "PROSPECT", "", "false", "true", "{\"CustID\":\"\",\"Name\":\"RMFUnitTest\",\"Phone1\":\"085783482343\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":null,\"SalesOfficerID\":\"rei\",\"DealerCode\":\"3951\",\"SalesDealer\":\"110254\",\"BranchCode\":\"023\",\"isCompany\":false}",
        "{\"FollowUpNo\":\"701a4c61-5284-4369-8c90-2ebc5f93f97c\",\"LastSeqNo\":\"0\",\"CustID\":\"\",\"ProspectName\":\"RMFUnitTest\",\"Phone1\":\"085783482343\",\"Phone2\":\"\",\"SalesOfficerID\":\"rei\",\"FollowUpName\":\"RMFUnitTest\",\"NextFollowUpDate\":\"\",\"LastFollowUpDate\":\"\",\"FollowUpStatus\":\"1\",\"FollowUpInfo\":\"1\",\"Remark\":\"\",\"BranchCode\":\"023\",\"PolicyId\":\"\",\"PolicyNo\":\"\",\"TransactionNo\":\"\",\"SalesAdminID\":null,\"SendDocDate\":null,\"IdentityCard\":\"fa33ab18-e833-4e18-947b-e1abc6a59ba0.png\",\"STNK\":\"\",\"SPPAKB\":\"\",\"BSTB1\":\"\",\"BUKTIBAYAR\":\"\"}",
            "", "", "", "" })]
        [InlineData(new object[] { "2", "VEHICLE", "dc2906dcde0a4d6db94db30bfa361a4e159fc249207cc45fd95a7e17308aaba1", "false", "true", "", "",
            "{\"OrderNo\":\"\",\"CustID\":\"11c1a5e8-635d-46a2-a308-ec8e275aca8b\",\"FollowUpNo\":\"701a4c61-5284-4369-8c90-2ebc5f93f97c\",\"TLOPeriod\":0,\"ComprePeriod\":0,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"DealerCode\":\"3951\",\"SalesDealer\":\"110254\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"\",\"MouID\":\"\",\"Phone1\":\"085783482343\",\"Phone2\":\"\",\"Email1\":\"sfdds@mail.com\",\"Email2\":\"\",\"InsuranceType\":0,\"ApplyF\":\"true\",\"SendF\":\"false\",\"LastInterestNo\":0,\"LastCoverageNo\":0,\"PeriodFrom\":null,\"PeriodTo\":null}",
            "{\"OrderNo\":\"\",\"ObjectNo\":\"1\",\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25   \",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B784Lj\",\"EngineNumber\":\"ENGRMFS07S10\",\"ChasisNumber\":\"CHARMFS07S10\",\"IsNew\":false,\"searchVehicle\":\"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T\"}",
            "", "" })]
        [InlineData(new object[] { "3", "COVER", "dc2906dcde0a4d6db94db30bfa361a4e159fc249207cc45fd95a7e17308aaba1", "false", "true", "", "",
            "{\"OrderNo\":\"98453ab1-81db-4c5a-aa49-27ce436dce63\",\"CustID\":\"11c1a5e8-635d-46a2-a308-ec8e275aca8b\",\"FollowUpNo\":\"701a4c61-5284-4369-8c90-2ebc5f93f97c\",\"MultiYearF\":false,\"YearCoverage\":1,\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"DealerCode\":\"3951\",\"SalesDealer\":\"110254\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"RFN10\",\"MouID\":\"RFN10001ABC\",\"AdminFee\":50000,\"TotalPremium\":5431425,\"Phone1\":\"085783482343\",\"Phone2\":\"\",\"Email1\":\"sfdds@mail.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"LastInterestNo\":0,\"LastCoverageNo\":0,\"PeriodTo\":\"2021-06-15 10:34:12\",\"PeriodFrom\":\"2020-06-15 10:34:12\"}",
            "{\"OrderNo\":\"98453ab1-81db-4c5a-aa49-27ce436dce63\",\"ObjectNo\":\"1\",\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25   \",\"ModelCode\":\"T00011\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B784Lj\",\"IsNew\":false,\"AccessSI\":0,\"searchVehicle\":\"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T\"}",
            "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}]",
            "{\"status\":1,\"message\":\"OK\",\"ProductCode\":\"RFN10\",\"CalculatedPremiItems\":[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.15,\"ExcessRate\":2.15,\"Premium\":4722475,\"CoverPremium\":4722475,\"GrossPremium\":4722475,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4722475,\"Net2\":4722475,\"Net3\":4722475,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}],\"IsTPLEnabled\":1,\"IsTPLChecked\":0,\"IsTPLSIEnabled\":0,\"IsSRCCChecked\":1,\"IsSRCCEnabled\":1,\"IsFLDChecked\":1,\"IsFLDEnabled\":1,\"IsETVChecked\":1,\"IsETVEnabled\":1,\"IsTSChecked\":0,\"IsTSEnabled\":1,\"IsPADRVRChecked\":0,\"IsPADRVREnabled\":1,\"IsPADRVRSIEnabled\":0,\"IsPASSEnabled\":0,\"IsPAPASSSIEnabled\":0,\"IsPAPASSEnabled\":1,\"IsPAPASSChecked\":0,\"IsACCESSChecked\":0,\"IsACCESSEnabled\":1,\"IsACCESSSIEnabled\":0,\"SRCCPremi\":164737.5,\"FLDPremi\":219649.99999999997,\"ETVPremi\":274562.5,\"TSPremi\":0,\"PADRVRPremi\":0,\"PAPASSPremi\":0,\"TPLPremi\":0,\"ACCESSPremi\":0,\"AdminFee\":50000,\"TotalPremi\":5431425,\"GrossPremi\":5381425,\"NoClaimBonus\":0,\"DiscountPremi\":0}" })]
        [InlineData(new object[] { "4", "PROSPECT", "", "true", "false", "{\"CustID\":\"\",\"Name\":\"RMFUnitTestGodig\",\"Phone1\":\"085652525252\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":null,\"SalesOfficerID\":\"rei\",\"DealerCode\":\"0\",\"SalesDealer\":\"0\",\"BranchCode\":\"023\",\"isCompany\":false}",
        "{\"FollowUpNo\":\"cd8c2fbf-c6cf-4296-a783-6cdafb4984de\",\"LastSeqNo\":\"0\",\"CustID\":\"\",\"ProspectName\":\"RMFUnitTestGodig\",\"Phone1\":\"085652525252\",\"Phone2\":\"\",\"SalesOfficerID\":\"rei\",\"FollowUpName\":\"RMFUnitTestGodig\",\"NextFollowUpDate\":\"\",\"LastFollowUpDate\":\"\",\"FollowUpStatus\":\"1\",\"FollowUpInfo\":\"1\",\"Remark\":\"\",\"BranchCode\":\"023\",\"PolicyId\":\"\",\"PolicyNo\":\"\",\"TransactionNo\":\"\",\"SalesAdminID\":null,\"SendDocDate\":null,\"IdentityCard\":\"44a43df4-6d74-4569-8feb-c605f3679019.png\",\"STNK\":\"\",\"SPPAKB\":\"\",\"BSTB1\":\"\",\"BUKTIBAYAR\":\"\"}",
            "", "", "", "" })]
        [InlineData(new object[] { "5", "VEHICLE", "ef02a51095e641dea31998936aee1df48b836b9276e806fc2f8458b2bc05f8fc", "true", "false", "", "",
            "{\"OrderNo\":\"\",\"CustID\":\"23f2f5a1-4efc-4e2b-96d3-b77721a4786f\",\"FollowUpNo\":\"cd8c2fbf-c6cf-4296-a783-6cdafb4984de\",\"TLOPeriod\":0,\"ComprePeriod\":0,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"DealerCode\":\"0\",\"SalesDealer\":\"0\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"\",\"MouID\":\"\",\"Phone1\":\"085652525252\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"LastInterestNo\":0,\"LastCoverageNo\":0,\"PeriodFrom\":null,\"PeriodTo\":null}",
            "{\"OrderNo\":\"\",\"ObjectNo\":\"1\",\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25   \",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B233Dv\",\"EngineNumber\":\"ENGRMFS0711\",\"ChasisNumber\":\"CHARMFS0711\",\"IsNew\":false,\"searchVehicle\":\"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T\"}",
            "", "" })]
        [InlineData(new object[] { "6", "COVER", "ef02a51095e641dea31998936aee1df48b836b9276e806fc2f8458b2bc05f8fc", "true", "false", "", "",
            "{\"OrderNo\":\"da8456de-d134-4061-8e98-f4ab6e36fd77\",\"CustID\":\"23f2f5a1-4efc-4e2b-96d3-b77721a4786f\",\"FollowUpNo\":\"cd8c2fbf-c6cf-4296-a783-6cdafb4984de\",\"MultiYearF\":false,\"YearCoverage\":0,\"TLOPeriod\":0,\"ComprePeriod\":0,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"DealerCode\":\"0\",\"SalesDealer\":\"0\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GEU10\",\"MouID\":\"GEU10\",\"AdminFee\":50000,\"TotalPremium\":5651075,\"Phone1\":\"085652525252\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"LastInterestNo\":0,\"LastCoverageNo\":0,\"PeriodTo\":\"2021-06-15 11:13:28\",\"PeriodFrom\":\"2020-06-15 11:13:28\"}",
            "{\"OrderNo\":\"da8456de-d134-4061-8e98-f4ab6e36fd77\",\"ObjectNo\":\"1\",\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25   \",\"ModelCode\":\"T00011\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B233Dv\",\"IsNew\":false,\"AccessSI\":0,\"searchVehicle\":\"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T\"}",
            "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}]",
            "{\"status\":1,\"message\":\"OK\",\"ProductCode\":\"GEU10\",\"CalculatedPremiItems\":[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}],\"IsTPLEnabled\":1,\"IsTPLChecked\":0,\"IsTPLSIEnabled\":0,\"IsSRCCChecked\":1,\"IsSRCCEnabled\":1,\"IsFLDChecked\":1,\"IsFLDEnabled\":1,\"IsETVChecked\":1,\"IsETVEnabled\":1,\"IsTSChecked\":0,\"IsTSEnabled\":1,\"IsPADRVRChecked\":0,\"IsPADRVREnabled\":1,\"IsPADRVRSIEnabled\":0,\"IsPASSEnabled\":0,\"IsPAPASSSIEnabled\":0,\"IsPAPASSEnabled\":1,\"IsPAPASSChecked\":0,\"IsACCESSChecked\":0,\"IsACCESSEnabled\":1,\"IsACCESSSIEnabled\":0,\"SRCCPremi\":164737.5,\"FLDPremi\":219649.99999999997,\"ETVPremi\":274562.5,\"TSPremi\":0,\"PADRVRPremi\":0,\"PAPASSPremi\":0,\"TPLPremi\":0,\"ACCESSPremi\":0,\"AdminFee\":50000,\"TotalPremi\":5651075,\"GrossPremi\":5601075,\"NoClaimBonus\":0,\"DiscountPremi\":0}" })]
        [InlineData(new object[] { "7", "PROSPECT", "", "false", "true", "{\"CustID\":\"9c1cdfe0-1390-42f4-b80b-25fe8c271a63\",\"Name\":\"RMFUnitTestCompany\",\"Phone1\":\"081565362537\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":null,\"SalesOfficerID\":\"rei\",\"DealerCode\":\"0\",\"SalesDealer\":\"0\",\"BranchCode\":\"023\",\"isCompany\":true}",
        "{\"FollowUpNo\":\"c4eacec2-5885-4075-8c6a-bee1ed2a894b\",\"LastSeqNo\":\"0\",\"CustID\":\"9c1cdfe0-1390-42f4-b80b-25fe8c271a63\",\"ProspectName\":\"RMFUnitTestCompany\",\"Phone1\":\"081565362537\",\"Phone2\":\"\",\"SalesOfficerID\":\"rei\",\"FollowUpName\":\"RMFUnitTestCompany\",\"NextFollowUpDate\":\"\",\"LastFollowUpDate\":\"\",\"FollowUpStatus\":\"1\",\"FollowUpInfo\":\"1\",\"Remark\":\"\",\"BranchCode\":\"023\",\"PolicyId\":\"\",\"PolicyNo\":\"\",\"TransactionNo\":\"\",\"SalesAdminID\":null,\"SendDocDate\":null,\"IdentityCard\":\"\",\"STNK\":\"\",\"SPPAKB\":\"\",\"BSTB1\":\"92cd999e-52b0-46e4-98c5-5aa7bc1ccb43.png\",\"BUKTIBAYAR\":\"\"}",
            "", "", "", "" })]
        [InlineData(new object[] { "8", "VEHICLE", "65b63c49f29149dc87c0b34f9dc48f0c5f9e5ab9fb9870e880f101af42a46b23", "false", "true", "", "",
            "{\"OrderNo\":\"\",\"CustID\":\"9c1cdfe0-1390-42f4-b80b-25fe8c271a63\",\"FollowUpNo\":\"c4eacec2-5885-4075-8c6a-bee1ed2a894b\",\"TLOPeriod\":0,\"ComprePeriod\":0,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"DealerCode\":\"0\",\"SalesDealer\":\"0\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"\",\"MouID\":\"\",\"Phone1\":\"081565362537\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":\"\",\"InsuranceType\":0,\"ApplyF\":\"true\",\"SendF\":\"false\",\"LastInterestNo\":0,\"LastCoverageNo\":0,\"PeriodFrom\":null,\"PeriodTo\":null}",
            "{\"OrderNo\":\"\",\"ObjectNo\":\"1\",\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25   \",\"ModelCode\":\"M01048\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B234Wt\",\"EngineNumber\":\"ENGRMFS0712\",\"ChasisNumber\":\"CHARMFS0712\",\"IsNew\":false,\"searchVehicle\":\"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T\"}",
            "", "" })]
        [InlineData(new object[] { "9", "COVER", "65b63c49f29149dc87c0b34f9dc48f0c5f9e5ab9fb9870e880f101af42a46b23", "false", "true", "", "",
            "{\"OrderNo\":\"5a513ce1-ce44-4314-a6cf-a6f2a089a3ba\",\"CustID\":\"9c1cdfe0-1390-42f4-b80b-25fe8c271a63\",\"FollowUpNo\":\"c4eacec2-5885-4075-8c6a-bee1ed2a894b\",\"MultiYearF\":false,\"YearCoverage\":1,\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"DealerCode\":\"0\",\"SalesDealer\":\"0\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GDN52\",\"MouID\":\"GDN52\",\"AdminFee\":50000,\"TotalPremium\":5651075,\"Phone1\":\"081565362537\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"LastInterestNo\":0,\"LastCoverageNo\":0,\"PeriodTo\":\"2021-06-15 11:35:20\",\"PeriodFrom\":\"2020-06-15 11:35:20\"}",
            "{\"OrderNo\":\"5a513ce1-ce44-4314-a6cf-a6f2a089a3ba\",\"ObjectNo\":\"1\",\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25   \",\"ModelCode\":\"T00011\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B234Wt\",\"IsNew\":false,\"AccessSI\":0,\"searchVehicle\":\"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T\"}",
            "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}]",
            "{\"status\":1,\"message\":\"OK\",\"ProductCode\":\"GDN52\",\"CalculatedPremiItems\":[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}],\"IsTPLEnabled\":1,\"IsTPLChecked\":0,\"IsTPLSIEnabled\":0,\"IsSRCCChecked\":1,\"IsSRCCEnabled\":1,\"IsFLDChecked\":1,\"IsFLDEnabled\":1,\"IsETVChecked\":1,\"IsETVEnabled\":1,\"IsTSChecked\":0,\"IsTSEnabled\":1,\"IsPADRVRChecked\":0,\"IsPADRVREnabled\":1,\"IsPADRVRSIEnabled\":0,\"IsPASSEnabled\":0,\"IsPAPASSSIEnabled\":0,\"IsPAPASSEnabled\":1,\"IsPAPASSChecked\":0,\"IsACCESSChecked\":0,\"IsACCESSEnabled\":1,\"IsACCESSSIEnabled\":0,\"SRCCPremi\":164737.5,\"FLDPremi\":219649.99999999997,\"ETVPremi\":274562.5,\"TSPremi\":0,\"PADRVRPremi\":0,\"PAPASSPremi\":0,\"TPLPremi\":0,\"ACCESSPremi\":0,\"AdminFee\":50000,\"TotalPremi\":5651075,\"GrossPremi\":5601075,\"NoClaimBonus\":0,\"DiscountPremi\":0}" })]
        public void SaveProspect_ReturnProspectInfo(string SeqNo, string State, string GuidTempPenawaran, string isMvGodig, string isNonMvGodig, string ProspectCustomer, string followUp,
            string OrderSimulation, string OrderSimulationMV, string calculatedPremiItems, string PremiumModel)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("State", State),
                new KeyValuePair<string, string>("GuidTempPenawaran", GuidTempPenawaran),
                new KeyValuePair<string, string>("isMvGodig", isMvGodig),
                new KeyValuePair<string, string>("isNonMvGodig", isNonMvGodig),
                new KeyValuePair<string, string>("ProspectCustomer", ProspectCustomer),
                new KeyValuePair<string, string>("FollowUp", followUp),
                new KeyValuePair<string, string>("OrderSimulation", OrderSimulation),
                new KeyValuePair<string, string>("OrderSimulationMV", OrderSimulationMV),
                new KeyValuePair<string, string>("calculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("PremiumModel", PremiumModel)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.SaveProspect(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic CustId = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("CustId").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic FollowUpNo = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("FollowUpNo").GetValue(result.GetType().GetProperties()[0].GetValue(result)); 

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.NotEmpty(CustId);
            Assert.NotEmpty(FollowUpNo);
        }
        [Fact(DisplayName = "SaveProspect_ReturnBasicCoverNotApplied")]
        public void SaveProspect_ReturnBasicCoverNotApplied()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string State= "COVER"; string GuidTempPenawaran= "65b63c49f29149dc87c0b34f9dc48f0c5f9e5ab9fb9870e880f101af42a46b23"; string isMvGodig= "false"; string isNonMvGodig= "true";
            string ProspectCustomer=""; string followUp="";
            string OrderSimulation= "{\"OrderNo\":\"5a513ce1-ce44-4314-a6cf-a6f2a089a3ba\",\"CustID\":\"9c1cdfe0-1390-42f4-b80b-25fe8c271a63\",\"FollowUpNo\":\"c4eacec2-5885-4075-8c6a-bee1ed2a894b\",\"MultiYearF\":false,\"YearCoverage\":1,\"TLOPeriod\":0,\"ComprePeriod\":1,\"BranchCode\":\"023\",\"SalesOfficerID\":\"rei\",\"DealerCode\":\"0\",\"SalesDealer\":\"0\",\"ProductTypeCode\":\"GARDAOTO\",\"ProductCode\":\"GNAB0\",\"MouID\":\"GNAB0\",\"AdminFee\":50000,\"TotalPremium\":5651075,\"Phone1\":\"081565362537\",\"Phone2\":\"\",\"Email1\":\"unit@test.com\",\"Email2\":\"\",\"InsuranceType\":1,\"ApplyF\":\"true\",\"SendF\":\"false\",\"LastInterestNo\":0,\"LastCoverageNo\":0,\"PeriodTo\":\"2021-06-15 11:35:20\",\"PeriodFrom\":\"2020-06-15 11:35:20\"}";
            string OrderSimulationMV= "{\"OrderNo\":\"5a513ce1-ce44-4314-a6cf-a6f2a089a3ba\",\"ObjectNo\":\"1\",\"ProductTypeCode\":\"GARDAOTO\",\"VehicleCode\":\"V07952\",\"BrandCode\":\"B25   \",\"ModelCode\":\"T00011\",\"Series\":\"GRAND NEW G 1.3 A/T\",\"Type\":\"T00011\",\"Sitting\":7,\"Year\":\"2019\",\"CityCode\":\"000020\",\"UsageCode\":\"U00002\",\"SumInsured\":219650000,\"RegistrationNumber\":\"B234Wt\",\"IsNew\":false,\"AccessSI\":0,\"searchVehicle\":\"TOYOTA AVANZA 2019 GRAND NEW G 1.3 A/T\"}";
            string calculatedPremiItems= "[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0}]";
            string PremiumModel = "{\"status\":1,\"message\":\"OK\",\"ProductCode\":\"GNAB0\",\"CalculatedPremiItems\":[{\"InterestID\":\"CASCO\",\"CoverageID\":\"ALLRIK\",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":2.25,\"ExcessRate\":2.25,\"Premium\":4942125,\"CoverPremium\":4942125,\"GrossPremium\":4942125,\"MaxSI\":99999999999,\"DeductibleCode\":\"FLT003\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":4942125,\"Net2\":4942125,\"Net3\":4942125,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":0,\"AutoApply\":\"0\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"ETV   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.125,\"ExcessRate\":0.125,\"Premium\":274562.5,\"CoverPremium\":274562.5,\"GrossPremium\":274562.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":274562.5,\"Net2\":274562.5,\"Net3\":274562.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"FLD   \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.1,\"ExcessRate\":0.1,\"Premium\":219649.99999999997,\"CoverPremium\":219650,\"GrossPremium\":219650,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":219650,\"Net2\":219650,\"Net3\":219650,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0},{\"InterestID\":\"CASCO\",\"CoverageID\":\"SRCC  \",\"Year\":1,\"PeriodFrom\":\"2020-06-15 00:00:00.000\",\"PeriodTo\":\"2021-06-15 00:00:00.000\",\"Rate\":0.075,\"ExcessRate\":0.075,\"Premium\":164737.5,\"CoverPremium\":164737.5,\"GrossPremium\":164737.5,\"MaxSI\":99999999999,\"DeductibleCode\":\"CL0001\",\"EntryPrecentage\":100,\"Ndays\":365,\"Net1\":164737.5,\"Net2\":164737.5,\"Net3\":164737.5,\"SumInsured\":219650000,\"Loading\":0,\"LoadingRate\":0,\"IsBundling\":1,\"AutoApply\":\"1\",\"CoverType\":null,\"CalcMethod\":0}],\"IsTPLEnabled\":1,\"IsTPLChecked\":0,\"IsTPLSIEnabled\":0,\"IsSRCCChecked\":1,\"IsSRCCEnabled\":1,\"IsFLDChecked\":1,\"IsFLDEnabled\":1,\"IsETVChecked\":1,\"IsETVEnabled\":1,\"IsTSChecked\":0,\"IsTSEnabled\":1,\"IsPADRVRChecked\":0,\"IsPADRVREnabled\":1,\"IsPADRVRSIEnabled\":0,\"IsPASSEnabled\":0,\"IsPAPASSSIEnabled\":0,\"IsPAPASSEnabled\":1,\"IsPAPASSChecked\":0,\"IsACCESSChecked\":0,\"IsACCESSEnabled\":1,\"IsACCESSSIEnabled\":0,\"SRCCPremi\":164737.5,\"FLDPremi\":219649.99999999997,\"ETVPremi\":274562.5,\"TSPremi\":0,\"PADRVRPremi\":0,\"PAPASSPremi\":0,\"TPLPremi\":0,\"ACCESSPremi\":0,\"AdminFee\":50000,\"TotalPremi\":5651075,\"GrossPremi\":5601075,\"NoClaimBonus\":0,\"DiscountPremi\":0}";
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("State", State),
                new KeyValuePair<string, string>("GuidTempPenawaran", GuidTempPenawaran),
                new KeyValuePair<string, string>("isMvGodig", isMvGodig),
                new KeyValuePair<string, string>("isNonMvGodig", isNonMvGodig),
                new KeyValuePair<string, string>("ProspectCustomer", ProspectCustomer),
                new KeyValuePair<string, string>("FollowUp", followUp),
                new KeyValuePair<string, string>("OrderSimulation", OrderSimulation),
                new KeyValuePair<string, string>("OrderSimulationMV", OrderSimulationMV),
                new KeyValuePair<string, string>("calculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("PremiumModel", PremiumModel)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.SaveProspect(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Basic Cover Not Applied", message);
        }
        [Fact(DisplayName = "SaveProspect_ReturnException")]
        public void SaveProspect_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string State = "PROSPECT"; string GuidTempPenawaran = ""; string isMvGodig = "false"; string isNonMvGodig = "true";
            string ProspectCustomer = "jkfsdhfj";
            string followUp = "{\"FollowUpNo\":\"c4eacec2-5885-4075-8c6a-bee1ed2a894b\",\"LastSeqNo\":\"0\",\"CustID\":\"9c1cdfe0-1390-42f4-b80b-25fe8c271a63\",\"ProspectName\":\"RMFUnitTestCompany\",\"Phone1\":\"081565362537\",\"Phone2\":\"\",\"SalesOfficerID\":\"rei\",\"FollowUpName\":\"RMFUnitTestCompany\",\"NextFollowUpDate\":\"\",\"LastFollowUpDate\":\"\",\"FollowUpStatus\":\"1\",\"FollowUpInfo\":\"1\",\"Remark\":\"\",\"BranchCode\":\"023\",\"PolicyId\":\"\",\"PolicyNo\":\"\",\"TransactionNo\":\"\",\"SalesAdminID\":null,\"SendDocDate\":null,\"IdentityCard\":\"\",\"STNK\":\"\",\"SPPAKB\":\"\",\"BSTB1\":\"92cd999e-52b0-46e4-98c5-5aa7bc1ccb43.png\",\"BUKTIBAYAR\":\"\"}";
            string OrderSimulation = "";
            string OrderSimulationMV = "";
            string calculatedPremiItems = "";
            string PremiumModel = "";
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("State", State),
                new KeyValuePair<string, string>("GuidTempPenawaran", GuidTempPenawaran),
                new KeyValuePair<string, string>("isMvGodig", isMvGodig),
                new KeyValuePair<string, string>("isNonMvGodig", isNonMvGodig),
                new KeyValuePair<string, string>("ProspectCustomer", ProspectCustomer),
                new KeyValuePair<string, string>("FollowUp", followUp),
                new KeyValuePair<string, string>("OrderSimulation", OrderSimulation),
                new KeyValuePair<string, string>("OrderSimulationMV", OrderSimulationMV),
                new KeyValuePair<string, string>("calculatedPremiItems", calculatedPremiItems),
                new KeyValuePair<string, string>("PremiumModel", PremiumModel)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.SaveProspect(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        //[Theory(DisplayName = "DownloadQuotation_ReturnQuotation")]
        //[InlineData(new object[] { "98453ab1-81db-4c5a-aa49-27ce436dce63" })]
        //[InlineData(new object[] { "4cf3fe6b-37ab-416d-8330-c4d36d4279c7" })]
        //public void DownloadQuotation_ReturnQuotation(string OrderNo)
        //{
        //    // AAA : Assign, Act, Assert
        //    // Assign

        //    string param;
        //    using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
        //        new KeyValuePair<string, string>("OrderNo", OrderNo)
        //    }))
        //    {
        //        param = body.ReadAsStringAsync().Result;
        //    }

        //    FormDataCollection form = new FormDataCollection(param);
        //    // Act
        //    var controller = new Controllers.v0107URF2020.DataReactController();
        //    var result = controller.DownloadQuotation(form);
        //    dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

        //    Assert.True(status);
        //    Assert.Equal("success", message);
        //    Assert.NotNull(data.Bytes);
        //}
        [Fact(DisplayName = "DownloadQuotation_ReturnMock")]
        public void DownloadQuotation_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string OrderNo = "98453ab1-81db-4c5a-aa49-27ce436dce63";
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            Models.v0219URF2019.DownloadQuotationResult res = new Models.v0219URF2019.DownloadQuotationResult();
            res.Bytes = null;
            res.FileName = "";
            mock.Setup(o => o.GetByteReport(OrderNo)).Returns(res);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.DownloadQuotation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("success", message);
            Assert.Equal(res, data);
        }
        [Fact(DisplayName = "DownloadQuotation_ReturnException")]
        public void DownloadQuotation_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string OrderNo = "98453ab1-81db-4c5a-aa49-27ce436dce63";
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            mock.Setup(o => o.GetByteReport(OrderNo)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.DownloadQuotation(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        //[Theory(DisplayName = "SendQuotationEmail_ReturnSuccess")]
        //[InlineData(new object[] { "afathurrahman@asuransiastra.com", "98453ab1-81db-4c5a-aa49-27ce436dce63" })]
        //[InlineData(new object[] { "afathurrahman@asuransiastra.com", "9da1a91e-dce5-4d67-bbd8-d68b6b5d2377" })]
        //public void SendQuotationEmail_ReturnSuccess(string Email, string OrderNo)
        //{
        //    // AAA : Assign, Act, Assert
        //    // Assign

        //    string param;
        //    using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
        //        new KeyValuePair<string, string>("OrderNo", OrderNo),
        //        new KeyValuePair<string, string>("Email", Email)
        //    }))
        //    {
        //        param = body.ReadAsStringAsync().Result;
        //    }

        //    FormDataCollection form = new FormDataCollection(param);
        //    // Act
        //    var controller = new Controllers.v0107URF2020.DataReactController();
        //    var result = controller.SendQuotationEmail(form);
        //    dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

        //    Assert.True(status);
        //    Assert.Equal("Success", message);
        //}
        [Fact(DisplayName = "SendQuotationEmail_ReturnFailed")]
        public void SendQuotationEmail_ReturnFailed()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string Email = "afathurrahman@asuransiastra.com"; string OrderNo = "98453ab1-81db-4c5a-aa49-27ce436dce63";
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("Email", Email)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            A2isCommonResult res = new A2isCommonResult();
            res.ResultCode = false;
            mock.Setup(o => o.SendQuotationEmail(OrderNo, Email)).Returns(res);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.SendQuotationEmail(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Contains("Failed Send Email", message);
        }
        [Fact(DisplayName = "SendQuotationEmail_ReturnException")]
        public void SendQuotationEmail_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string Email = "afathurrahman@asuransiastra.com"; string OrderNo = "98453ab1-81db-4c5a-aa49-27ce436dce63";
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("OrderNo", OrderNo),
                new KeyValuePair<string, string>("Email", Email)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            A2isCommonResult res = new A2isCommonResult();
            res.ResultCode = false;
            mock.Setup(o => o.SendQuotationEmail(OrderNo, Email)).Throws(new OutOfMemoryException());
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.SendQuotationEmail(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Contains("Unable to process your input please try again or contact administrator", message);
        }

        //[Theory(DisplayName = "SendViaWA_ReturnSuccess")]
        //[InlineData(new object[] { "{\"WAMessage\":\"Message Unit Test\",\"PhoneNo\":\"08136362736278\",\"OrderNo\":\"193e4bf1-0c87-4337-90a1-5ba8ba4ade1d\",\"ListAtt\":[],\"IsAddAtt\":true}" })]
        //[InlineData(new object[] { "{\"WAMessage\":\"Message Unit Test\",\"PhoneNo\":\"08136362736123\",\"OrderNo\":\"193e4bf1-0c87-4337-90a1-5ba8ba4ade1d\",\"ListAtt\":[],\"IsAddAtt\":true}" })]
        //public void SendViaWA_ReturnSuccess(string json)
        //{
        //    // AAA : Assign, Act, Assert
        //    // Assign

        //    Models.v0219URF2019.SendViaWAParam param = JsonConvert.DeserializeObject<Models.v0219URF2019.SendViaWAParam>(json);

        //    // Act
        //    var controller = new Controllers.v0107URF2020.DataReactController();
        //    var result = controller.SendViaWA(param);
        //    dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

        //    Assert.True(status);
        //    Assert.Equal("Success", message);
        //}
        [Fact(DisplayName = "SendViaWA_ReturnFailed")]
        public void SendViaWA_ReturnFailed()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"WAMessage\":\"Message Unit Test\",\"PhoneNo\":\"08136362736278\",\"OrderNo\":\"193e4bf1-0c87-4337-90a1-5ba8ba4ade1d\",\"ListAtt\":[],\"IsAddAtt\":true}";
            Models.v0219URF2019.SendViaWAParam param = JsonConvert.DeserializeObject<Models.v0219URF2019.SendViaWAParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();           
            mock.Setup(o => o.SendViaWA(param)).Returns(false);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.SendViaWA(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Failed Send WA", message);
        }
        [Fact(DisplayName = "SendViaWA_ReturnEmptyParam")]
        public void SendViaWA_ReturnEmptyParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"WAMessage\":\"\",\"PhoneNo\":\"08136362736278\",\"OrderNo\":\"\",\"ListAtt\":[],\"IsAddAtt\":true}";
            Models.v0219URF2019.SendViaWAParam param = JsonConvert.DeserializeObject<Models.v0219URF2019.SendViaWAParam>(json);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.SendViaWA(param);

            Assert.Equal("Input Required", ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message);
        }
        [Fact(DisplayName = "SendViaWA_ReturnException")]
        public void SendViaWA_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"WAMessage\":\"Message Unit Test\",\"PhoneNo\":\"08136362736278\",\"OrderNo\":\"193e4bf1-0c87-4337-90a1-5ba8ba4ade1d\",\"ListAtt\":[],\"IsAddAtt\":true}";
            Models.v0219URF2019.SendViaWAParam param = JsonConvert.DeserializeObject<Models.v0219URF2019.SendViaWAParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            mock.Setup(o => o.SendViaWA(param)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.SendViaWA(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        //[Theory(DisplayName = "SendViaEmail_ReturnSuccess")]
        //[InlineData(new object[] { "{\"EmailTo\":\"rmanihuruk@asuransiastra.com\",\"EmailCC\":\"\",\"EmailBCC\":\"\",\"Subject\":\"Penawaran Polis Asuransi Lexus Insurance an cek vehicle\",\"Body\":\"Message Unit Test\",\"OrderNo\":\"193e4bf1-0c87-4337-90a1-5ba8ba4ade1d\",\"ListAtt\":[],\"IsAddAtt\":true}" })]
        //[InlineData(new object[] { "{\"EmailTo\":\"afathurrahman@asuransiastra.com\",\"EmailCC\":\"\",\"EmailBCC\":\"\",\"Subject\":\"Penawaran Polis Asuransi Lexus Insurance an cek vehicle\",\"Body\":\"Message Unit Test\",\"OrderNo\":\"98453ab1-81db-4c5a-aa49-27ce436dce63\",\"ListAtt\":[],\"IsAddAtt\":true}" })]
        //public void SendViaEmail_ReturnSuccess(string json)
        //{
        //    // AAA : Assign, Act, Assert
        //    // Assign

        //    Models.vWeb2.SendViaEmailParam param = JsonConvert.DeserializeObject<Models.vWeb2.SendViaEmailParam>(json);

        //    // Act
        //    var controller = new Controllers.v0107URF2020.DataReactController();
        //    var result = controller.SendViaEmail(param);
        //    dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

        //    Assert.True(status);
        //    Assert.Equal("Success", message);
        //}
        [Fact(DisplayName = "SendViaEmail_ReturnFailed")]
        public void SendViaEmail_ReturnFailed()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"EmailTo\":\"rmanihuruk@asuransiastra.com\",\"EmailCC\":\"\",\"EmailBCC\":\"\",\"Subject\":\"Penawaran Polis Asuransi Lexus Insurance an cek vehicle\",\"Body\":\"Message Unit Test\",\"OrderNo\":\"193e4bf1-0c87-4337-90a1-5ba8ba4ade1d\",\"ListAtt\":[],\"IsAddAtt\":true}";
            Models.vWeb2.SendViaEmailParam param = JsonConvert.DeserializeObject<Models.vWeb2.SendViaEmailParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            A2isCommonResult res = new A2isCommonResult();
            res.ResultCode = false;
            mock.Setup(o => o.SendViaEmail(param.EmailTo, param.EmailCC, param.EmailBCC, param.Body, param.Subject, param.ListAtt, param.OrderNo, param.IsAddAtt)).Returns(res);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.SendViaEmail(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Contains("Failed Send Email", message);
        }
        [Fact(DisplayName = "SendViaEmail_ReturnEmptyParam")]
        public void SendViaEmail_ReturnEmptyParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"EmailTo\":\"\",\"EmailCC\":\"\",\"EmailBCC\":\"\",\"Subject\":\"Penawaran Polis Asuransi Lexus Insurance an cek vehicle\",\"Body\":\"\",\"OrderNo\":\"\",\"ListAtt\":[],\"IsAddAtt\":true}";
            Models.vWeb2.SendViaEmailParam param = JsonConvert.DeserializeObject<Models.vWeb2.SendViaEmailParam>(json);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.SendViaEmail(param);

            Assert.Equal("Input Required", ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message);
        }
        [Fact(DisplayName = "SendViaEmail_ReturnException")]
        public void SendViaEmail_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"EmailTo\":\"rmanihuruk@asuransiastra.com\",\"EmailCC\":\"\",\"EmailBCC\":\"\",\"Subject\":\"Penawaran Polis Asuransi Lexus Insurance an cek vehicle\",\"Body\":\"Message Unit Test\",\"OrderNo\":\"193e4bf1-0c87-4337-90a1-5ba8ba4ade1d\",\"ListAtt\":[],\"IsAddAtt\":true}";
            Models.vWeb2.SendViaEmailParam param = JsonConvert.DeserializeObject<Models.vWeb2.SendViaEmailParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            A2isCommonResult res = new A2isCommonResult();
            res.ResultCode = false;
            mock.Setup(o => o.SendViaEmail(param.EmailTo, param.EmailCC, param.EmailBCC, param.Body, param.Subject, param.ListAtt, param.OrderNo, param.IsAddAtt)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.SendViaEmail(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Contains("Unable to process your input please try again or contact administrator", message);
        }

        //[Theory(DisplayName = "sendToSARenewalNonRenNot_ReturnSuccess")]
        //[InlineData(new object[] { "{\"PolicyNo\":\"2001716924\",\"FollowUpNo\":\"8867c013-89e2-4b48-b9b8-180b58753e21\",\"RemarksToSA\":\"sds\",\"SalesOfficerID\":\"rei\",\"isPTSB\":0}" })]
        //public void sendToSARenewalNonRenNot_ReturnSuccess(string json)
        //{
        //    // AAA : Assign, Act, Assert
        //    // Assign

        //    Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam param = JsonConvert.DeserializeObject<Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam>(json);
        //    // Act
        //    var controller = new Controllers.v0107URF2020.DataReactController();
        //    var result = controller.sendToSARenewalNonRenNot(param);
        //    dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic orderID = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("orderID").GetValue(result.GetType().GetProperties()[0].GetValue(result));

        //    Assert.True(status);
        //    Assert.Equal("Update Success.", message);
        //    Assert.NotEmpty(orderID);
        //}
        [Fact(DisplayName = "sendToSARenewalNonRenNot_ReturnDoubleInsured")]
        public void sendToSARenewalNonRenNot_ReturnDoubleInsured()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"PolicyNo\":\"1500488181\",\"FollowUpNo\":\"929a9508-abb9-4fa6-a0a3-d7db9caafcc2\",\"RemarksToSA\":\"sds\",\"SalesOfficerID\":\"rei\",\"isPTSB\":0}";
            Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam param = JsonConvert.DeserializeObject<Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam>(json);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.sendToSARenewalNonRenNot(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Double Insured.", message);
            Assert.NotNull(data);
        }
        [Fact(DisplayName = "sendToSARenewalNonRenNot_ReturnEmptyParam")]
        public void sendToSARenewalNonRenNot_ReturnEmptyParam()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"PolicyNo\":\"1500488181\",\"FollowUpNo\":\"\",\"RemarksToSA\":\"sds\",\"SalesOfficerID\":\"rei\",\"isPTSB\":0}";
            Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam param = JsonConvert.DeserializeObject<Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam>(json);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.sendToSARenewalNonRenNot(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Param can't be null", message);
        }
        [Fact(DisplayName = "sendToSARenewalNonRenNot_ReturnException")]
        public void sendToSARenewalNonRenNot_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string json = "{\"PolicyNo\":\"150048818\",\"FollowUpNo\":\"929a9508-abb9-4fa6-a0a3-d7db9caafcc2\",\"RemarksToSA\":\"sds\",\"SalesOfficerID\":\"rei\",\"isPTSB\":0}";
            Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam param = JsonConvert.DeserializeObject<Otosales.Models.v0232URF2019.SendToSARenewalNonRenNotParam>(json);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            mock.Setup(o => o.GenerateOrderSimulation(param.SalesOfficerID, "", param.FollowUpNo, "", param.PolicyNo, param.RemarksToSA, param.isPTSB)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.sendToSARenewalNonRenNot(param);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }
        [Theory(DisplayName = "GetSlotTimeBooking_ReturnSlotTimeBooking")]
        [InlineData(new object[] { "", "60", "5", "17-Jun-2020" })]
        [InlineData(new object[] { "", "97", "9", "22-Jun-2020" })]
        [InlineData(new object[] { "16431", "97", "", "20-Jun-2020" })]
        public void GetSlotTimeBooking_ReturnSlotTimeBooking(string ZipCode, string CityId, string BranchID, string AvailableDate)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ZipCode", ZipCode),
                new KeyValuePair<string, string>("CityId", CityId),
                new KeyValuePair<string, string>("BranchID", BranchID),
                new KeyValuePair<string, string>("AvailableDate", AvailableDate)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.GetSlotTimeBooking(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Fetch success", message);
            Assert.True(data.Count > 0);
        }
        [Fact(DisplayName = "GetSlotTimeBooking_ReturnIncorectDateFormat")]
        public void GetSlotTimeBooking_ReturnIncorectDateFormat()
        {
            // AAA : Assign, Act, Assert
            string ZipCode = "16431"; string CityId = "97"; string BranchID = ""; string AvailableDate = "ads";
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ZipCode", ZipCode),
                new KeyValuePair<string, string>("CityId", CityId),
                new KeyValuePair<string, string>("BranchID", BranchID),
                new KeyValuePair<string, string>("AvailableDate", AvailableDate)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.GetSlotTimeBooking(form);

            Assert.Equal("Incorrect Date format", ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message);
        }
        [Theory(DisplayName = "GetSlotTimeBooking_ReturnBadRequest")]
        [InlineData(new object[] { "", "", "", "17-Jun-2020" })]
        [InlineData(new object[] { "", "", "", "" })]
        public void GetSlotTimeBooking_ReturnBadRequest(string ZipCode, string CityId, string BranchID, string AvailableDate)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            FormDataCollection form = null;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ZipCode", ZipCode),
                new KeyValuePair<string, string>("CityId", CityId),
                new KeyValuePair<string, string>("BranchID", BranchID),
                new KeyValuePair<string, string>("AvailableDate", AvailableDate)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            if (!string.IsNullOrEmpty(AvailableDate)) {
                form = new FormDataCollection(param);
            }

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.GetSlotTimeBooking(form);

            Assert.Equal("Bad Request", ((System.Web.Http.Results.BadRequestErrorMessageResult)(result)).Message);
        }
        [Fact(DisplayName = "GetSlotTimeBooking_ReturnException")]
        public void GetSlotTimeBooking_ReturnException()
        {
            // AAA : Assign, Act, Assert
            string ZipCode = "16431"; string CityId = "97"; string BranchID = ""; string AvailableDate = "17-Jun-2020";
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ZipCode", ZipCode),
                new KeyValuePair<string, string>("CityId", CityId),
                new KeyValuePair<string, string>("BranchID", BranchID),
                new KeyValuePair<string, string>("AvailableDate", AvailableDate)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            DateTime d;
            DateTime.TryParse(AvailableDate, CultureInfo.CreateSpecificCulture("en-US"), DateTimeStyles.None, out d);
            mock.Setup(o => o.GetSurveyScheduleSurveyManagement(CityId, ZipCode, d)).Returns(false);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.GetSlotTimeBooking(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        [Theory(DisplayName = "GetSlotTimeBooking_ReturnSlotTimeBooking")]
        [InlineData(new object[] { "59" })]
        [InlineData(new object[] { "5" })]
        [InlineData(new object[] { "9" })]
        public void GetSurveyDays_ReturnSurveyDays(string BranchID)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("BranchID", BranchID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            var result = controller.GetSurveyDays(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.NotNull(data);
        }
        [Fact(DisplayName = "GetSurveyDays_ReturnMock")]
        public void GetSurveyDays_ReturnMock()
        {
            // AAA : Assign, Act, Assert
            string BranchID = "59";
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("BranchID", BranchID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            List<string> ls = new List<string>();
            ls.Add("");
            mock.Setup(o => o.GetSurveyDays(BranchID)).Returns(ls);

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.GetSurveyDays(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.True(status);
            Assert.Equal("Success", message);
            Assert.Equal(ls, data);
        }
        [Fact(DisplayName = "GetSurveyDays_ReturnException")]
        public void GetSurveyDays_ReturnException()
        {
            // AAA : Assign, Act, Assert
            string BranchID = "59";
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("BranchID", BranchID)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
            List<string> ls = new List<string>();
            ls.Add("");
            mock.Setup(o => o.GetSurveyDays(BranchID)).Throws(new OutOfMemoryException());

            // Act
            var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
            var result = controller.GetSurveyDays(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic data = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("data").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("Unable to process your input please try again or contact administrator", message);
        }

        [Theory(DisplayName = "GetSlotTimeBooking_ReturnSlotTimeBookingParamEmpty")]
        [InlineData(new object[] { "", "", "", ""})]
        public void GetSlotTimeBooking_ReturnSlotTimeBookingParamEmpty(string ZipCode, string CityId, string BranchID, string AvailableDate)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ZipCode", ZipCode),
                new KeyValuePair<string, string>("CityId", CityId),
                new KeyValuePair<string, string>("BranchID", BranchID),
                new KeyValuePair<string, string>("AvailableDate", AvailableDate)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            dynamic result = controller.GetSlotTimeBooking(form);
            dynamic message = result.Message;

            Assert.Equal("Incorrect Date format", message);
        }

        [Theory(DisplayName = "GetSlotTimeBooking_ReturnSlotTimeBookingEmptyZipeCode")]
        [InlineData(new object[] { "", "", "", "14-Jun-2020" })]
        public void GetSlotTimeBooking_ReturnSlotTimeBookingEmptyZipeCode(string ZipCode, string CityId, string BranchID, string AvailableDate)
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("ZipCode", ZipCode),
                new KeyValuePair<string, string>("CityId", CityId),
                new KeyValuePair<string, string>("BranchID", BranchID),
                new KeyValuePair<string, string>("AvailableDate", AvailableDate)
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0107URF2020.DataReactController();
            dynamic result = controller.GetSlotTimeBooking(form);
            dynamic message = result.Message;
            
            Assert.Equal("Bad Request", message);
        }

        [Theory(DisplayName = "GetSurveyDays_GetDateSurvey")]
        [InlineData(new object[] { "26" })]
        public void GetSurveyDays_GetDateSurvey(string BranchID)
        {
            // AAA : Assign, Act, Assert
            // Assign
                       
            // Act
            var repository = new Repository.v0107URF2020.MobileRepository();
            var result = repository.GetSurveyDays(BranchID);

            Assert.True(result.Count > 0);

        }

        [Theory(DisplayName = "GetSurveyDays_GetDateSurveyEmptyBranchID")]
        [InlineData(new object[] { "" })]
        public void GetSurveyDays_GetDateSurveyEmptyBranchID(string BranchID)
        {
            // AAA : Assign, Act, Assert
            // Assign
            
            // Act
            var repository = new Repository.v0107URF2020.MobileRepository();
            var result = repository.GetSurveyDays(BranchID);

            Assert.True(result == null);

        }

        //[Fact(DisplayName = "GetSurveyDays_ThrowError")]
        //public void GetSurveyDays_ThrowError()
        //{
        //    // AAA : Assign, Act, Assert
        //    // Assign
        //    string param;
        //    using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
        //        new KeyValuePair<string, string>("BranchID", "")
        //    }))
        //    {
        //        param = body.ReadAsStringAsync().Result;
        //    }
        //    var mock = new Mock<Repository.v0107URF2020.IMobileRepository>();
        //    mock.Setup(o => o.GetSurveyDays("")).Throws(new OutOfMemoryException());

        //    FormDataCollection form = new FormDataCollection(param);
        //    // Act
        //    var controller = new Controllers.v0107URF2020.DataReactController(mock.Object);
        //    var result = controller.GetSurveyDays(form);
        //    dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
        //    dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

        //    Assert.False(status);
        //    Assert.Equal("Error", message);
        //}


        [Theory(DisplayName = "GetBasicCover_MVGodigGOLite")]
        [InlineData(new object[] { "", true, true, "ELN01" })]
        [InlineData(new object[] { "", true, true, "ELU01" })]
        [InlineData(new object[] { "", true, true, "ALN65" })]
        [InlineData(new object[] { "", true, true, "ALU65" })]
        public void GetBasicCover_MVGodigGOLite(string vyear, bool isBasic, bool isMvGodig, string productCode)
        {
            // AAA : Assign, Act, Assert
            // Assign

            // Act
            var repository = new Repository.v0090URF2020.MobileRepository();
            List<Models.v0219URF2019.BasicCover> result = new List<Models.v0219URF2019.BasicCover>();
            result = repository.GetBasicCover(string.IsNullOrEmpty(vyear) ? DateTime.Now.Year : Convert.ToInt32(vyear), isBasic, isMvGodig, productCode);

            Assert.True(result.Count == 1);

        }

        [Fact(DisplayName = "GetBasicCoverv0090_ReturnException")]
        public void GetBasicCoverv0090_ReturnException()
        {
            // AAA : Assign, Act, Assert
            // Assign
            string param;
            using (var body = new FormUrlEncodedContent(new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("vyear", ""),
                new KeyValuePair<string, string>("isBasic", ""),
                new KeyValuePair<string, string>("isMvGodig", ""),
                new KeyValuePair<string, string>("productCode", "")
            }))
            {
                param = body.ReadAsStringAsync().Result;
            }
            var mock = new Mock<Repository.v0090URF2020.IMobileRepository>();
            mock.Setup(o => o.GetBasicCover(DateTime.Now.Year, true, true, "")).Throws(new OutOfMemoryException());

            FormDataCollection form = new FormDataCollection(param);
            // Act
            var controller = new Controllers.v0090URF2020.DataReactController(mock.Object);
            var result = controller.getBasicCover(form);
            dynamic status = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("status").GetValue(result.GetType().GetProperties()[0].GetValue(result));
            dynamic message = result.GetType().GetProperties()[0].GetValue(result).GetType().GetProperty("message").GetValue(result.GetType().GetProperties()[0].GetValue(result));

            Assert.False(status);
            Assert.Equal("String was not recognized as a valid Boolean.", message);
        }
    }
}
